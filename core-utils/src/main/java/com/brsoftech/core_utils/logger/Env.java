package com.brsoftech.core_utils.logger;
import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by RAMNIVAS SINGH 05-01-2016.
 */
public class Env {
    public static Context appContext;
        public static AppCompatActivity currentActivity;
//    public static DBHelper dbHelper;
    public static String logFilePath;
    public static boolean isDebugMode;
    public static void init(Context appContext, String logFilePath, boolean isDebugMode) {
        Env.appContext = appContext;
//        Env.dbHelper = dbHelper;
        Env.logFilePath = logFilePath;
        Env.isDebugMode = isDebugMode;
    }
}

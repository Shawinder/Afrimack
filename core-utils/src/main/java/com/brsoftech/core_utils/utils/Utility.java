package com.brsoftech.core_utils.utils;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.brsoftech.core_utils.R;
import com.brsoftech.core_utils.logger.Env;

/**
 * Author: Kajal Mittal
 * Date: 27/12/16
 * Time: 4:04 PM
 */

public class Utility {
   static ProgressDialog mLoadingDialog;
   static AlertDialog mErrorDialog;

    public static void displayLoadingDialog(boolean isCancellable) {
        try {
            mLoadingDialog = new ProgressDialog(Env.currentActivity);
            mLoadingDialog.setTitle(R.string.loading);
            mLoadingDialog.setMessage(Env.currentActivity.getString(R.string.please_wait));
            mLoadingDialog.setIndeterminate(true);
            mLoadingDialog.setCancelable(isCancellable);
            mLoadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissLoadingDialog() {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public static void displayErrorDialog(String  title,  String content) {
        mErrorDialog = new AlertDialog.Builder(Env.currentActivity)
                .setTitle(title)
                .setMessage(content)
                .setIcon(ContextCompat.getDrawable(Env.currentActivity, R.drawable.ic_error_24dp))
                .setCancelable(false)
                .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create();
        mErrorDialog.show();
    }


}

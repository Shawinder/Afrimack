package com.brsoftech.core_utils.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;

import com.brsoftech.core_utils.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImagePickerUtil {
    public static final int RC_PICK_IMAGE_CAMERA = 456;
    public static final int RC_PICK_IMAGE_STORAGE = 789;

    private final static int CAMERA_PERMISSION_RC = 1337;

    private static final String TAG = "ImagePickerUtil";

    private Context mContext;
    private Fragment mFragment;
    private OnImagePickerListener mOnImagePickerListener;
    private String mCurrentPhotoPath;
    private boolean mPickFromCamera;

    public ImagePickerUtil(Context context) {
        this.mContext = context;
    }

    public ImagePickerUtil(Fragment fragment) {
        mContext = fragment.getContext();
        mFragment = fragment;
    }

    public void pickImage(OnImagePickerListener onImagePickerListener) {
        mOnImagePickerListener = onImagePickerListener;
        new AlertDialog.Builder(mContext)
                .setTitle(R.string.pick_image)
                .setSingleChoiceItems(R.array.pick_image_types, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                pickImageFromCamera();
                                break;
                            case 1:
                                pickImageFromStorage();
                                break;
                        }
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.choose, null)
                .show();
    }

    public void pickImageFromGallery(OnImagePickerListener onImagePickerListener) {
        mOnImagePickerListener = onImagePickerListener;
        pickImageFromStorage();


    }

    public void pickImageFromCamera(OnImagePickerListener onImagePickerListener) {
        mOnImagePickerListener = onImagePickerListener;
        pickImageFromCamera();

    }


    public void pickImageFromCamera() {
        mPickFromCamera = true;

        //  Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

       /* File photo = new File(getOutputMediaFileUri(MEDIA_TYPE_IMAGE).getPath());

        photoUri = Uri.fromFile(photo);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);*/

        ((Activity) mContext).startActivityForResult(intent, RC_PICK_IMAGE_CAMERA);

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.e("CurrentPhotoPath", mCurrentPhotoPath);
        return image;
    }

    public void pickImageFromStorage() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (mFragment == null) {
            ((Activity) mContext).startActivityForResult(
                    Intent.createChooser(intent, "Select File"),
                    RC_PICK_IMAGE_STORAGE);
        } else {
            mFragment.startActivityForResult(
                    Intent.createChooser(intent, "Select File"),
                    RC_PICK_IMAGE_STORAGE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case RC_PICK_IMAGE_CAMERA:
//                    Uri photoUrri = (Uri) data.getExtras().get("data");
//                    Uri file = (Uri) data.getExtras().get("data");
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    Uri mUri = getImageUri(mContext, photo);
                    mOnImagePickerListener.onImagePickerSuccess(mUri, true);
                    break;
                case RC_PICK_IMAGE_STORAGE:
                    Uri photoUri = data.getData();
                    mOnImagePickerListener.onImagePickerSuccess(photoUri, false);
                    break;
            }
        }
    }

    public void onDestroy() {
        if (mPickFromCamera) {
            new FileUtil().deleteFile(mCurrentPhotoPath);
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, System.currentTimeMillis() + "", null);
        return Uri.parse(path);
    }

    public interface OnImagePickerListener {
        void onImagePickerSuccess(String imgPath, boolean fromCamera);

        void onImagePickerSuccess(Uri imgPath, boolean fromCamera);

        void onImagePickerFailure(String message);
    }
}


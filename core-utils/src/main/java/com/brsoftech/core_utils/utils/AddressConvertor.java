package com.brsoftech.core_utils.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

/**
 * Created by ubuntu on 2/5/16.
 */
public class AddressConvertor {

    Double latude, longtude;
    public static Context context;
    private static AddressConvertor instance;
    private NotifyLocation Notify;

    private AddressConvertor() {

    }

    public static AddressConvertor getInstance(Context ctx) {
        context = ctx;


        if (instance == null) {
            instance = new AddressConvertor();
        }
        return instance;
    }

    public void Getaddress(Location dragPosition,NotifyLocation onNotify) {

        Notify=onNotify;
//        if (dragPosition == null) {
//            dragPosition = LocationMgr.getInstance(context).getCurrentLocation(true);
//        }
        if(dragPosition!=null) {
            double dragLat = dragPosition.getLatitude();
            double dragLong = dragPosition.getLongitude();


            latude = dragPosition.getLatitude();
            longtude = dragPosition.getLongitude();

            String url = "https://maps.googleapis.com/maps/api/geocode/json?";
            String address = "latlng=" + latude + "," + longtude;
            String sensor = "sensor=false";

            url = url + address + "&" + sensor;

            DownloadTask downloadTask = new DownloadTask();

            downloadTask.execute(url);
        }

    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class DownloadTask extends AsyncTask<String, Integer, String> {

        String data = null;
        ProgressDialog proDialog;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result) {

//            Log.w("ADDRESS RESULT", result);

            JSONObject jsonObj;
            try {
                jsonObj = new JSONObject(result);

                if (jsonObj.getString("status").equalsIgnoreCase("OK")) {
                    JSONArray arryResult = jsonObj.getJSONArray("results");
                    if (arryResult.length() > 0) {
                        JSONObject objaddress = arryResult.getJSONObject(0);
                        JSONObject objgeometry = objaddress.getJSONObject("geometry");
                        JSONObject objlocation = objgeometry.getJSONObject("location");
                        String formatted_address = objaddress.getString("formatted_address");
                        String[] bits = formatted_address.split(",");
                        int count = bits.length;
                        AddressConvertorModel address= new AddressConvertorModel();
                         String country = bits[(bits.length - 1)];
                        String  state = bits[(bits.length - 2)];
                        String[] bitsd = state.trim().split(" ");
                        state = bitsd[0];
                        String     city = bits[(bits.length - 3)];
                        String    pin = bitsd[1];
                        Log.e("FORMETED Address", formatted_address);
                        String lat = objlocation.getString("lat");
                        String lng = objlocation.getString("lng");
                        address.country=country;
                        address.state=state;
                        address.city=city;
                        address.pin=pin;
                        address.lat=lat;
                        address.lng=lng;
                        Notify.notifyData(address, false);

                       // Utils.displayToast(context, lat + " " + lng);


                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("ERROR", "" + e);

            }

        }

    }

   public class AddressConvertorModel
   {
       public   String country;
       public   String  state;
       public   String     city;
       public   String    pin;
       public   String lat;
       public  String lng;
   }
}

package com.brsoftech.timerprogressview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

public class TimerProgressBar extends View {

    public static final int DEFAULT_BG_COLOR = 0xffffffff;
    public static final int DEFAULT_PROGRESS_COLOR = 0xff00ff00;
    public static final float DEFAULT_PROGRESS_STROCK_WIDTH = 10.0f;

    public static int totaltimercount = 0;
    public static int timercount = 0;
    public static int updateIntervalmilisec = 0;
    public static float newangle = 0;
    public static float incrementangle = 0;
    TimerProgressListner timerProgressListner;

    public TimerProgressListner getTimerProgressListner () {
        return timerProgressListner;
    }

    public void setTimerProgressListner (TimerProgressListner timerProgressListner) {
        this.timerProgressListner = timerProgressListner;
    }

    Paint bg_paint;
    Paint pg_paint;
    Rect viewRect;
    float viewRadius = 0.0f;
    float progressStrokeWidth = 0.0f;


    Handler handler = new Handler();

    Runnable runnable = new Runnable() {
        @Override
        public void run () {
            newangle += incrementangle;
            timercount -= updateIntervalmilisec;
            if (timercount >= 0) {
                triggerTimerProgress();
                invalidate();
                if (timercount > 0) {
                    handler.postDelayed(this, updateIntervalmilisec);
                } else {
                    triggerTimerEnd();
                }
                return;
            }
            triggerTimerEnd();

        }
    };


    public void resetTimerProgress () {
        newangle = 0;
        timercount = 0;
        incrementangle = 0;
        updateIntervalmilisec = 0;
        totaltimercount = 0;
    }

    public void pauseTimerProgress () {
        handler.removeCallbacks(runnable);
    }

    public void resumeTimerProgress () {
        pauseTimerProgress();
        handler.postDelayed(runnable, 1000);
    }

    public void stratTimerProgress (int totalTimermilisec, int updateIntervalmilisec) {
        resetTimerProgress();
        this.updateIntervalmilisec = updateIntervalmilisec;
        this.totaltimercount = totalTimermilisec;
        this.timercount = totalTimermilisec;
        this.incrementangle = (float) 360 / (totalTimermilisec / updateIntervalmilisec);
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, updateIntervalmilisec);
        triggerTimerStart();
    }

    @Override
    protected void onDetachedFromWindow () {
        super.onDetachedFromWindow();
        handler.removeCallbacks(runnable);
    }

    public TimerProgressBar (Context context) {
        // TODO Auto-generated constructor stub
        super(context);
        inIt(null);
    }

    public TimerProgressBar (Context context, AttributeSet attrs) {
        super(context, attrs);
        inIt(attrs);
    }

    public TimerProgressBar (Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inIt(attrs);
    }

    private void inIt (AttributeSet attrs) {
        newangle = 0;
        timercount = 0;
        incrementangle = 0;
        progressStrokeWidth = dpToPx(getContext(), DEFAULT_PROGRESS_STROCK_WIDTH);

        viewRect = new Rect(0, 0, 0, 0);

        bg_paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bg_paint.setDither(true);
        bg_paint.setStyle(Paint.Style.FILL);
        bg_paint.setColor(DEFAULT_BG_COLOR);

        pg_paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        pg_paint.setStrokeCap(Paint.Cap.ROUND);
        pg_paint.setStrokeJoin(Paint.Join.ROUND);
        pg_paint.setDither(true);
        pg_paint.setStyle(Paint.Style.STROKE);
        pg_paint.setStrokeWidth(progressStrokeWidth);
        pg_paint.setColor(DEFAULT_PROGRESS_COLOR);

        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.timerProgressBar);
            final int N = a.getIndexCount();
            for (int i = 0; i < N; ++i) {
                int attr = a.getIndex(i);
                if (attr == R.styleable.timerProgressBar_timer_background_color) {
                    bg_paint.setColor(a.getColor(attr, DEFAULT_BG_COLOR));
                }
                if (attr == R.styleable.timerProgressBar_timer_progress_color) {
                    pg_paint.setColor(a.getColor(attr, DEFAULT_PROGRESS_COLOR));
                }
                if (attr == R.styleable.timerProgressBar_timer_progress_stroke_width) {
                    pg_paint.setStrokeWidth(a.getDimension(attr, progressStrokeWidth));

                }
            }

        }
    }

    @Override
    protected void onSizeChanged (int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        viewRect = new Rect(0, 0, w, h);
        if (viewRect.width() > viewRect.height()) {
            viewRadius = viewRect.height() * 0.5f;
        } else {
            viewRadius = viewRect.width() * 0.5f;
        }
    }

    @Override
    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);

        final RectF rect = new RectF(viewRect);
        rect.inset(progressStrokeWidth, progressStrokeWidth);
        final float width = rect.width();
        final float height = rect.height();
        final float radius = viewRadius - progressStrokeWidth;
        PointF center = new PointF(rect.centerX(), rect.centerY());
        Path path = new Path();
        path.addCircle(center.x, center.y, radius, Path.Direction.CW);
        canvas.drawPath(path, bg_paint);
        if (isInEditMode()) {
            newangle = 90;
        }
        if (newangle != 0) {
            path.reset();
            path.addArc(rect, -90, newangle);
            canvas.drawPath(path, pg_paint);
        }
        triggerTimerDraw(canvas);
    }

    public float dpToPx (Context ctx, float dp) {
        DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
        float px = dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    private void triggerTimerStart () {
        if (getTimerProgressListner() != null) {
            getTimerProgressListner().onTimerProgressStart();
            getTimerProgressListner().onTimerProgress(totaltimercount, timercount);
        }
    }

    private void triggerTimerEnd () {
        if (getTimerProgressListner() != null) {
            getTimerProgressListner().onTimerProgressEnd();
        }
    }

    private void triggerTimerProgress () {
        if (getTimerProgressListner() != null) {
            getTimerProgressListner().onTimerProgress(totaltimercount, timercount);
        }
    }

    private void triggerTimerDraw (Canvas canvas) {
        if (getTimerProgressListner() != null) {
            getTimerProgressListner().onTimerProgressDraw(canvas);
        }
    }

    public interface TimerProgressListner {
        void onTimerProgressStart ();

        void onTimerProgressEnd ();

        void onTimerProgress (int totalTimeMillisec, int remainTimeMillisec);

        void onTimerProgressDraw (Canvas canvas);
    }
}

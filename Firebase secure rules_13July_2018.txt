{
  "rules": {
   "test": {
      ".indexOn": ["email","password"]
    },
  "users": {
      ".indexOn": "email"
    },
      "message":{
        ".indexOn":["idSender","isRead","isImage"]
      },
    ".read": "auth != null",
    ".write": "auth != null"
  }
}
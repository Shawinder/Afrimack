package com.chat.appfolders;

import java.io.File;

/**
 * Created by ubuntu on 30/12/16.
 */

public class InternalImageFolder {

    public static final String TAG = "InternalImageFolder";

    public static final String FOLDER_NAME = "/Images/";

    static InternalImageFolder instance;

    static {
        instance = new InternalImageFolder(InternalAppFolder.getInstance());
    }

    InternalAppFolder parentDir;
    File dir;

    private InternalImageFolder(InternalAppFolder parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static InternalImageFolder getInstance() {
        return instance;
    }

    public File getDir() {
        return dir;
    }

    public String getNewFilePath() {
        String fileName = "IMG-" + System.currentTimeMillis() + ".jpg";
        return new File(dir, fileName).getAbsolutePath();

    }
}

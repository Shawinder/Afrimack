package com.chat.appfolders;

import java.io.File;

/**
 * Created by ubuntu on 3/1/17.
 */

public class VideoFolder {

    public static final String TAG = "VideoFolder";

    public static final String FOLDER_NAME = "/Videos/";

    static VideoFolder instance;

    static {
        instance = new VideoFolder(MediaFolder.getInstance());
    }

    MediaFolder parentDir;
    File dir;

    private VideoFolder(MediaFolder parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static VideoFolder getInstance() {
        return instance;
    }

    public File getDir() {
        return dir;
    }

    public String getNewFilePath() {
        String fileName = "VID-" + System.currentTimeMillis() + ".mp4";
        return new File(dir, fileName).getAbsolutePath();

    }
}

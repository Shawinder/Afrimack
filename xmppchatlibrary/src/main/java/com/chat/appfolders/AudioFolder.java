package com.chat.appfolders;

import java.io.File;

/**
 * Created by ubuntu on 21/12/16.
 */

public class AudioFolder {

    public static final String TAG = "AudioFolder";

    public static final String FOLDER_NAME = "/Audio/";

    static AudioFolder instance;

    static {
        instance = new AudioFolder(MediaFolder.getInstance());
    }

    MediaFolder parentDir;
    File dir;

    private AudioFolder(MediaFolder parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static AudioFolder getInstance() {
        return instance;
    }

    public File getDir() {
        return dir;
    }

    public String getNewFilePath() {
        String fileName = "AUD-" + System.currentTimeMillis() + ".mp3";
        return new File(dir, fileName).getAbsolutePath();

    }
}

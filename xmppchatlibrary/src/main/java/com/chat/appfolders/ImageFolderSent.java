package com.chat.appfolders;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;


import com.chat.Utils.ThumbnailHandler;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by ubuntu on 16/12/16.
 */

public class ImageFolderSent {


    public static final String TAG = "ImageFolderSent";
    public static final String FOLDER_NAME = "/Sent/";
    private static final int TARGET_SIZE_IMAGE = 1024;
    private static final int MAX_NUM_PIXELS_IMAGE = 1024 * 1024;
    static ImageFolderSent instance;

    static {
        instance = new ImageFolderSent(ImagesFolder.getInstance());
    }

    ImagesFolder parentDir;
    File dir;

    private ImageFolderSent(ImagesFolder parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static ImageFolderSent getInstance() {
        return instance;
    }

    public static int getMaxPixels(double w, double h) {
        double targetWidth = w;
        double targetHeight = h;
        double ratio = 0.0;
        if (w > h && w > 1024) {
            ratio = w / h;
            targetWidth = 1024;
            targetHeight = (int) (targetWidth / ratio);
        } else if (w < h && h > 1024) {
            ratio = h / w;
            targetHeight = 1024;
            targetWidth = (int) (targetHeight / ratio);
        } else {
            if (w > 1024 && h > 1024) {
                targetWidth = 1024;
                targetHeight = 1024;
            }
        }
        return (int) (targetWidth * targetHeight);
    }

    public static Bitmap convertImage(String filePath) {
        // boolean wantMini = (kind == MediaStore.Images.Thumbnails.MINI_KIND);
        int targetSize = TARGET_SIZE_IMAGE;

        Bitmap bitmap = null;
        if (bitmap == null) {
            FileInputStream stream = null;
            try {
                stream = new FileInputStream(filePath);
                FileDescriptor fd = stream.getFD();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFileDescriptor(fd, null, options);
                if (options.mCancel || options.outWidth == -1
                        || options.outHeight == -1) {
                    return null;
                }
                double w = options.outWidth;
                double h = options.outHeight;
                int maxPixels = getMaxPixels(w, h);
                options.inSampleSize = ThumbnailHandler.computeSampleSize(
                        options, targetSize, maxPixels);
                options.inJustDecodeBounds = false;

                options.inDither = false;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                bitmap = BitmapFactory.decodeFileDescriptor(fd, null, options);
            } catch (IOException ex) {
                Log.e(TAG, "", ex);
            } catch (OutOfMemoryError oom) {
                Log.e(TAG, "Unable to decode file " + filePath + ". OutOfMemoryError.", oom);
            } finally {
                try {
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException ex) {
                    Log.e(TAG, "", ex);
                }
            }

        }
        return bitmap;
    }

    public File getDir() {
        return dir;
    }

    public synchronized String copyFile(String fromPath) throws IOException {
        Bitmap bitmap = convertImage(fromPath);
        if (bitmap == null) {
            return null;
        }
        String outputFileName = "IMG-" + System.currentTimeMillis() + ".jpg";

        OutputStream out = null;

        //create output directory if it doesn't exist
        File dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        File outFile = new File(dir, outputFileName);
        out = new FileOutputStream(outFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        out.flush();
        out.close();
        out = null;
        return outFile.getAbsolutePath();
    }
}

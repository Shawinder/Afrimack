package com.chat.appfolders;

import com.chat.xmpp.XmppUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by ubuntu on 2/1/17.
 */

public class DocumentFolderSent {

    public static final String TAG = "DocumentFolderSent";

    public static final String FOLDER_NAME = "/Sent/";

    static DocumentFolderSent instance;

    static {
        instance = new DocumentFolderSent(DocumentFolder.getInstance());
    }

    DocumentFolder parentDir;
    File dir;

    private DocumentFolderSent(DocumentFolder parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static DocumentFolderSent getInstance() {
        return instance;
    }

    public synchronized static String getFilenameWithOutExtensionFromPath(String filePath) {
        if (!XmppUtils.checkVaildString(filePath)) {
            return null;
        }
        int index = filePath.lastIndexOf("/");
        int index2 = filePath.lastIndexOf(".");
        if (filePath.length() <= index) {
            return null;
        }
        return filePath.substring(index + 1, index2);
    }

    public synchronized static String getFileExtensionFromPath(String filePath) {
        if (!XmppUtils.checkVaildString(filePath)) {
            return null;
        }
        int index = filePath.lastIndexOf(".");
        if (filePath.length() <= index) {
            return null;
        }
        return filePath.substring(index + 1, filePath.length());
    }

    public File getDir() {
        return dir;
    }

    public synchronized String copyFile(String fromPath) throws IOException {

        InputStream in = null;
        OutputStream out = null;
        String outputFileName = getFilenameWithOutExtensionFromPath(fromPath);
        String outputFileExt = getFileExtensionFromPath(fromPath);
        String outputFileFullName = outputFileName + "." + outputFileExt;
        File dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String parentDirPath = new File(fromPath).getParent();
        if (parentDirPath.equals(dir.getAbsolutePath())) {
            return fromPath;
        }

        File outFile = new File(dir, outputFileFullName);
        int count = 1;
        while (outFile.exists()) {
            outputFileFullName = outputFileName + " " + count + "." + outputFileExt;
            outFile = new File(dir, outputFileFullName);
            count++;
        }


        in = new FileInputStream(fromPath);
        out = new FileOutputStream(outFile);

        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        in = null;
        out.flush();
        out.close();
        out = null;
        return outFile.getAbsolutePath();
    }


}

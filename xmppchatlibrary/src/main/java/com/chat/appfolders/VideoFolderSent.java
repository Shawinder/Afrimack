package com.chat.appfolders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Created by ubuntu on 3/1/17.
 */

public class VideoFolderSent {


    public static final String TAG = "VideoFolderSent";

    public static final String FOLDER_NAME = "/Sent/";

    static VideoFolderSent instance;

    static {
        instance = new VideoFolderSent(VideoFolder.getInstance());
    }

    VideoFolder parentDir;
    File dir;

    private VideoFolderSent(VideoFolder parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static VideoFolderSent getInstance() {
        return instance;
    }

    public File getDir() {
        return dir;
    }

    public String getNewFilePath() {
        File dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String fileName = "VID-" + System.currentTimeMillis() + ".mp4";
        return new File(dir, fileName).getAbsolutePath();

    }

    public synchronized String copyFile(String fromPath) throws IOException {
        InputStream in = null;
        OutputStream out = null;

        String outputFileFullName = "VID-" + System.currentTimeMillis() + ".mp4";
        File dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String parentDirPath = new File(fromPath).getParent();
        if (parentDirPath.equals(dir.getAbsolutePath())) {
            return fromPath;
        }

        File outFile = new File(dir, outputFileFullName);


        in = new FileInputStream(fromPath);
        out = new FileOutputStream(outFile);

        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        in = null;
        out.flush();
        out.close();
        out = null;
        return outFile.getAbsolutePath();
    }

}

package com.chat.appfolders;

import android.os.Environment;

import com.chat.xmpp.media.MediaType;

import java.io.File;

/**
 * Created by ubuntu on 16/12/16.
 */

public class FoldersHandler {

    public static final String TAG = "FoldersHandler";

    public static final String ROOT_FOLDER_NAME = "/AfrimackApp/";
    public static final File EXT_DIR = Environment.getExternalStorageDirectory();


    static FoldersHandler instance;

    static {
        instance = new FoldersHandler();
    }

    File dir;

    private FoldersHandler() {
        dir = new File(EXT_DIR, ROOT_FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static FoldersHandler getInstance() {
        return instance;
    }

    public File getDir() {
        return dir;
    }

    public String getFilePathForDownload(MediaType mediaType, String url, String caption) {
        if (mediaType == null) {
            return null;
        }
        if (mediaType == MediaType.image) {
            return ImagesFolder.getInstance().getNewFilePath();
        } else if (mediaType == MediaType.audio) {
            return AudioFolder.getInstance().getNewFilePath();
        } else if (mediaType == MediaType.document) {
            return DocumentFolder.getInstance().getNewFilePath(url, caption);
        } else if (mediaType == MediaType.video) {
            return VideoFolder.getInstance().getNewFilePath();
        }

        return null;

    }
}

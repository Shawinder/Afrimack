package com.chat.appfolders;

import java.io.File;

/**
 * Created by ubuntu on 16/12/16.
 */

public class ImagesFolder {

    public static final String TAG = "ImagesFolder";

    public static final String FOLDER_NAME = "/Images/";

    static ImagesFolder instance;

    static {
        instance = new ImagesFolder(MediaFolder.getInstance());
    }

    MediaFolder parentDir;
    File dir;

    private ImagesFolder(MediaFolder parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static ImagesFolder getInstance() {
        return instance;
    }

    public File getDir() {
        return dir;
    }

    public String getNewFilePath() {
        String fileName = "IMG-" + System.currentTimeMillis() + ".jpg";
        return new File(dir, fileName).getAbsolutePath();

    }
}

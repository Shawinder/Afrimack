package com.chat.appfolders;

import java.io.File;

/**
 * Created by ubuntu on 16/12/16.
 */

public class MediaFolder {

    public static final String TAG = "MediaFolder";

    public static final String FOLDER_NAME = "/Media/";

    static MediaFolder instance;

    static {
        instance = new MediaFolder(FoldersHandler.getInstance());
    }

    FoldersHandler parentDir;
    File dir;

    private MediaFolder(FoldersHandler parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static MediaFolder getInstance() {
        return instance;
    }

    public File getDir() {
        return dir;
    }
}

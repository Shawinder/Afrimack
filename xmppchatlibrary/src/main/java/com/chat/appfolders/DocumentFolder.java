package com.chat.appfolders;

import com.chat.xmpp.XmppUtils;

import java.io.File;

/**
 * Created by ubuntu on 2/1/17.
 */

public class DocumentFolder {
    public static final String TAG = "DocumentFolder";

    public static final String FOLDER_NAME = "/Documents/";

    static DocumentFolder instance;

    static {
        instance = new DocumentFolder(MediaFolder.getInstance());
    }

    MediaFolder parentDir;
    File dir;

    private DocumentFolder(MediaFolder parentDir) {
        this.parentDir = parentDir;
        dir = new File(parentDir.getDir(), FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static DocumentFolder getInstance() {
        return instance;
    }

    public synchronized static String getFilenameWithOutExtensionFromPath(String filePath) {
        if (!XmppUtils.checkVaildString(filePath)) {
            return null;
        }
        int index = filePath.lastIndexOf("/");
        int index2 = filePath.lastIndexOf(".");
        if (filePath.length() <= index) {
            return null;
        }
        return filePath.substring(index + 1, index2);
    }

    public synchronized static String getFileExtensionFromPath(String filePath) {
        if (!XmppUtils.checkVaildString(filePath)) {
            return null;
        }
        int index = filePath.lastIndexOf(".");
        if (filePath.length() <= index) {
            return null;
        }
        return filePath.substring(index + 1, filePath.length());
    }

    public File getDir() {
        return dir;
    }

    public String getNewFilePath(String url, String name) {
        String outFileName = name;
        String outFileExt = getFileExtensionFromPath(url);
        String outputFileFullName = outFileName + "." + outFileExt;
        File outFile = new File(dir, outputFileFullName);
        int count = 1;
        while (outFile.exists()) {
            outputFileFullName = outFileName + " " + count + "." + outFileExt;
            outFile = new File(dir, outputFileFullName);
            count++;
        }
        return outFile.getAbsolutePath();

    }
}

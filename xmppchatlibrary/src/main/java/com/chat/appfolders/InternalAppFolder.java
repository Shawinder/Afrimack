package com.chat.appfolders;

import com.chat.ChatApplication;
import com.chat.xmpp.media.MediaType;

import java.io.File;

/**
 * Created by ubuntu on 30/12/16.
 */

public class InternalAppFolder {

    public static final String TAG = "InternalAppFolder";

    public static final String ROOT_FOLDER_NAME = "/MyCache/";
    public static File EXT_DIR;

    static InternalAppFolder instance;

    static {
        EXT_DIR = new File(ChatApplication.getInstance().getApplicationInfo().dataDir);
        instance = new InternalAppFolder();
    }

    File dir;

    private InternalAppFolder() {
        dir = new File(EXT_DIR, ROOT_FOLDER_NAME);
        if (!dir.exists() || !dir.isDirectory()) {
            dir.mkdirs();
        }
    }

    public static InternalAppFolder getInstance() {
        return instance;
    }

    public File getDir() {
        return dir;
    }

    public String getFilePathForDownload(MediaType mediaType) {
        if (mediaType == null) {
            return null;
        }
        if (mediaType == MediaType.location) {
            return InternalImageFolder.getInstance().getNewFilePath();
        }

        return null;

    }

}

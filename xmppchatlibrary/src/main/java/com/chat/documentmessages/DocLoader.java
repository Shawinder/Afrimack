package com.chat.documentmessages;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;

import com.chat.ChatApplication;
import com.chat.xmpp.media.MediaUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ubuntu on 23/12/16.
 */

public class DocLoader {
    public static final String TAG = "DocLoader";

    static DocLoader instance;

    static {
        instance = new DocLoader();

    }

    ContentResolver cr;
    List<DocModal> docModalList;
    DocTask docTask;
    Handler handler = new Handler();

    private DocLoader() {
        cr = ChatApplication.getInstance().getContentResolver();
        docModalList = new ArrayList<>();
    }

    public static DocLoader getInstance() {
        return instance;
    }

    public List<DocModal> getDocModalList() {
        return docModalList;
    }

    public void getDocs(DocsLoaderListner docsLoaderListner) {
        if (docTask != null) {
            if (docTask.isRunning()) {
                docTask.setCancelled();
            }
        }
        docTask = new DocTask();
        docTask.execute(docsLoaderListner);
    }

    public interface DocsLoaderListner {
        void loadingStart();

        void loadingEnd();
    }

    public static class DocMimeType {
        public static final String MIME_PDF = "application/pdf";
        public static final String MIME_PPT = "application/vnd.ms-powerpointtd>";
        public static final String MIME_PPTX = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
        public static final String MIME_XLS = "application/vnd.ms-excel";
        public static final String MIME_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public static final String MIME_TXT = "text/plain";

        public static final String docTypes = "'" + MIME_PDF + "'" + "," +
                "'" + MIME_PPT + "'" + "," +
                "'" + MIME_PPTX + "'" + "," +
                "'" + MIME_XLS + "'" + "," +
                "'" + MIME_XLSX + "'" + "," +
                "'" + MIME_TXT + "'";
        static DocMimeType instance;

        static {
            instance = new DocMimeType();
        }

        HashMap<String, DocModal.DocType> dataMap;

        private DocMimeType() {
            dataMap = new HashMap<>();
            dataMap.put(MIME_PDF, DocModal.DocType.pdf);
            dataMap.put(MIME_PPT, DocModal.DocType.ppt);
            dataMap.put(MIME_PPTX, DocModal.DocType.ppt);
            dataMap.put(MIME_XLS, DocModal.DocType.xls);
            dataMap.put(MIME_XLSX, DocModal.DocType.xls);
            dataMap.put(MIME_TXT, DocModal.DocType.txt);
        }

        public static DocMimeType getInstance() {
            return instance;
        }

        public DocModal.DocType getDocType(String mimeType) {
            if (mimeType == null) {
                return null;
            }
            if (dataMap.containsKey(mimeType)) {
                return dataMap.get(mimeType);
            }
            return null;
        }
    }

    public class DocTask extends AsyncTask<DocsLoaderListner, Void, List<DocModal>> {
        DocsLoaderListner docsLoaderListner;
        boolean cancelled = false;
        boolean running = false;

        public boolean isRunning() {
            return running;
        }

        public void setCancelled() {
            cancelled = true;
        }

        @Override
        protected void onPostExecute(List<DocModal> musicModals) {
            super.onPostExecute(musicModals);
            if (!cancelled) {
                docModalList.clear();
                docModalList.addAll(musicModals);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (docsLoaderListner != null) {
                            docsLoaderListner.loadingEnd();
                        }
                    }
                });
            }
            running = false;
        }

        @Override
        protected List<DocModal> doInBackground(DocsLoaderListner... params) {
            running = true;
            docsLoaderListner = params[0];
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (docsLoaderListner != null) {
                        docsLoaderListner.loadingStart();
                    }
                }
            });
            File appsDir = new File(Environment.getExternalStorageDirectory(), "Android");
            String blockedPath = appsDir.getAbsolutePath();

            List<DocModal> dataList = new ArrayList<>();
            Uri uri = MediaStore.Files.getContentUri("external");
            String selection = MediaStore.Files.FileColumns.MIME_TYPE + " IN (" + DocMimeType.docTypes + ") AND " +
                    MediaStore.Files.FileColumns.DATA + " NOT LIKE '" + blockedPath + "%'" +
                    " AND " + MediaStore.Files.FileColumns.DATA + " NOT LIKE '%/.%'";

            String sortOrder = MediaStore.Files.FileColumns.DATE_ADDED + " DESC";
            Cursor cur = cr.query(uri, null, selection, null, sortOrder);
            int count = 0;
            if (cur != null) {
                count = cur.getCount();
                if (count > 0) {
                    while (cur.moveToNext() && !cancelled) {
                        String data = cur.getString(cur.getColumnIndex(MediaStore.Files.FileColumns.DATA));
                        long size = cur.getLong(cur.getColumnIndex(MediaStore.Files.FileColumns.SIZE));
                        long created = cur.getLong(cur.getColumnIndex(MediaStore.Files.FileColumns.DATE_ADDED));
                        String mimeType = cur.getString(cur.getColumnIndex(MediaStore.Files.FileColumns.MIME_TYPE));

                        DocModal.DocType docType = DocMimeType.getInstance().getDocType(mimeType);
                        if (docType != null) {
                            DocModal docModal = new DocModal();
                            docModal.setFilePath(data);
                            String title = MediaUtil.getFilenameFromUrl(docModal.getFilePath());
                            docModal.setTitle(title);
                            docModal.setSize(size);
                            created *= 1000;
                            docModal.setCreated(created);
                            docModal.setMimeType(mimeType);
                            docModal.setDocType(docType);
                            dataList.add(docModal);
                        }

                    }

                }
                cur.close();

            }
            return dataList;
        }
    }
}

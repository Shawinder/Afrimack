package com.chat;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.chat.Utils.PermissionsHelper;
import com.chat.contacts.ContactHelper;
import com.chat.contacts.ContactModal;
import com.chat.contacts.ContactUris;
import com.chat.database.tables.ContactTable;
import com.chat.database.tables.GroupMembers;
import com.chat.database.tables.GroupMessageReceipts;
import com.chat.database.tables.MessageAndCallTable;
import com.chat.database.tables.RecentTable;
import com.chat.modals.MessageModal;
import com.chat.modals.RecentModal;
import com.chat.modals.StatusModal;
import com.chat.prefences.ChatPrefs;
import com.chat.xmpp.MyXMPP;
import com.chat.xmpp.XmppListners;
import com.chat.xmpp.XmppLogger;
import com.chat.xmpp.listners.MyXmppConnectionListner;
import com.chat.xmpp.listners.OnClearListener;
import com.chat.xmpp.listners.OnLoadListner;
import com.chat.xmpp.listners.OnMediaDownloadListner;
import com.chat.xmpp.listners.OnMediaUploadListner;
import com.chat.xmpp.listners.OnMessageUpdateListner;
import com.chat.xmpp.listners.VideoCompressionListner;
import com.chat.xmpp.media.downloadUtils.DownloadRequest;
import com.chat.xmpp.media.uploadUtils.UploadRequest;
import com.chat.xmpp.modals.XmppAccount;
import com.chat.xmpp.services.XmppConnectionService;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by ubuntu on 23/11/16.
 */

public class ChatApplication extends Application implements MyXmppConnectionListner,
        OnMediaUploadListner, OnMediaDownloadListner, VideoCompressionListner, OnMessageUpdateListner
//        , OnGroupListner
{


    public static final String TAG = "ChatApplication";
    static ChatApplication instance;
    private final ExecutorService backgroundExecutor;
    private final ExecutorService vCardBackgroundExecutor;
    private final ExecutorService rosterBackgroundExecutor;
    private final ExecutorService receiptBackgroundExecutor;
    private final ExecutorService databaseBackgroundExecutor;
    XmppConnectionService xmppConnectionService;
    ContactHelper contactHelper;
    XmppListners xmppListners;
    Handler handler;
    Handler bitmapLoadHandler;
    Handler videoCompressHandler;
    Handler serviceHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                doStartChatService();

            } else if (msg.what == 2) {
                if (xmppConnectionService != null) {
                    xmppConnectionService.getXmpp().contactChangeDoSync();
                }
            }
        }
    };
    MyContentObserver contactObserver = new MyContentObserver();


    public ChatApplication() {
        instance = ChatApplication.this;
        handler = new Handler();
        bitmapLoadHandler = new Handler();
        videoCompressHandler = new Handler();
        backgroundExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "Chat backgroundExecutor");
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        });
        databaseBackgroundExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "Chat databaseBackgroundExecutor");
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        });
        vCardBackgroundExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "Chat vCardBackgroundExecutor");
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        });
        rosterBackgroundExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "Chat rosterBackgroundExecutor");
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        });
        receiptBackgroundExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "Chat receiptBackgroundExecutor");
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        });
    }

    public static ChatApplication getInstance() {
        return instance;
    }

    public static void showToast(String msg) {
        Toast.makeText(instance, msg, Toast.LENGTH_SHORT).show();
    }

    public XmppListners getXmppListners() {
        return xmppListners;
    }

    public ContactHelper getContactHelper() {
        return contactHelper;
    }

    public XmppConnectionService getXmppConnectionService() {
        return xmppConnectionService;

    }

    public MyXMPP getXmpp() {
        if (xmppConnectionService != null) {
            return xmppConnectionService.getXmpp();
        }
        return null;
    }

    public void serviceCreatedDoWork() {
        XmppLogger.printLog(TAG + " serviceCreatedDoWork");
        triggerOnLoadListner();
        xmppConnectionService = XmppConnectionService.getInstance();
        XmppAccount xmppAccount = ChatPrefs.getSavedChatAccount(instance);
        xmppConnectionService.getXmpp().setXmppAccount(xmppAccount);
        xmppConnectionService.getXmpp().setXmppConnectionService(xmppConnectionService);
        try {
            xmppConnectionService.getXmpp().startConnection();
        } catch (IllegalStateException e) {
            XmppLogger.printLog(TAG + " IllegalStateException=" + e.getMessage());
        }
    }

    public void connectToOpenFire() throws IllegalStateException {
        try {
            if (xmppConnectionService != null) {
                xmppConnectionService.getXmpp().startConnection();
            } else {
                XmppLogger.printLog(TAG + " connectToOpenFire xmppConnectionService is null");
                doStartChatService();
            }
        } catch (IllegalStateException e) {
            XmppLogger.printLog(TAG + " IllegalStateException=" + e.getMessage());
            throw e;
        }
    }

    private void triggerOnLoadListner() {
        ArrayList<OnLoadListner> onLoadListnerArrayList = getXmppListners().getOnLoadListnerArrayList();
        for (OnLoadListner onLoadListner : onLoadListnerArrayList) {
            onLoadListner.onLoad();
        }
    }

    private void triggerOnClearListner() {
        ArrayList<OnClearListener> onClearListenerArrayList = getXmppListners().getOnClearListenerArrayList();
        for (OnClearListener onClearListener : onClearListenerArrayList) {
            onClearListener.onClear();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        XmppLogger.printLog(TAG + " onCreate");
        xmppListners = XmppListners.getInstance();
        ContactTable.getInstance();
        MessageAndCallTable.getInstance();
        RecentTable.getInstance();
        GroupMembers.getInstance();
        GroupMessageReceipts.getInstance();
        xmppListners.addUiListner(this);
        serviceHandler.sendEmptyMessageDelayed(1, 2000);
       /* FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                    XmppLogger.printLog("ffmpeg.loadBinary onStart");
                }

                @Override
                public void onFailure() {
                    XmppLogger.printLog("ffmpeg.loadBinary onFailure");
                }

                @Override
                public void onSuccess() {
                    XmppLogger.printLog("ffmpeg.loadBinary onSuccess");
                }

                @Override
                public void onFinish() {
                    XmppLogger.printLog("ffmpeg.loadBinary onFinish");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
            XmppLogger.printLog("ffmpeg.loadBinary FFmpegNotSupportedException");
        }*/
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        XmppLogger.printLog(TAG + " onTerminate");
    }

    public synchronized boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service.process.equals(getPackageName()) && serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void doStartChatService() {
        if (!isMyServiceRunning(XmppConnectionService.class)) {
            if (PermissionsHelper.areExplicitPermissionsRequired()) {
                List<String> required = PermissionsHelper.isAllPremissiongranted(this);
                if (required != null && required.size() > 0) {
                    XmppLogger.printLog(TAG + " doStartService fail required permission");
                    return;
                }

            }
            startService(XmppConnectionService.createIntent(this));
        } else {
            XmppLogger.printLog(TAG + " doStartService service is already running.");
        }
    }

    public void netWorkConnectivityChange() {
        if (isNetworkAvailable()) {
            XmppLogger.printLog(TAG + " Network Available.");
            if (xmppConnectionService != null) {
                xmppConnectionService.getXmpp().startConnection();
            }

        } else {
            XmppLogger.printLog(TAG + " Network Unavailable.");
        }
    }

    public boolean isNetworkAvailable() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo infoAvailableNetworks = cm.getActiveNetworkInfo();

        return infoAvailableNetworks != null;
    }

    public void runInBackground(Runnable runnable) {
        XmppLogger.printLog(TAG + " runInBackground");
        backgroundExecutor.submit(runnable);
    }

    public void runInDatabaseBackground(Runnable runnable) {
        XmppLogger.printLog(TAG + " runInDatabaseBackground");
        databaseBackgroundExecutor.submit(runnable);
    }

    public void runInVcardBackground(Runnable runnable) {
        XmppLogger.printLog(TAG + " runInVCardBackground");
        vCardBackgroundExecutor.submit(runnable);
    }

    public void runInRosterBackground(Runnable runnable) {
        XmppLogger.printLog(TAG + " runInVCardBackground");
        rosterBackgroundExecutor.submit(runnable);
    }

    public void runInReceiptBackground(Runnable runnable) {
        XmppLogger.printLog(TAG + " runInVCardBackground");
        receiptBackgroundExecutor.submit(runnable);
    }

    public void runOnUiThread(Runnable runnable) {
        synchronized (handler) {
            handler.post(runnable);
        }
    }

    public void runOnBitmapLoadHandlerThread(Runnable runnable) {
        synchronized (bitmapLoadHandler) {
            bitmapLoadHandler.post(runnable);
        }
    }

    public void runOnVideoCompressHandlerThread(Runnable runnable) {
        synchronized (videoCompressHandler) {
            videoCompressHandler.post(runnable);
        }
    }

    public void removeFromUiThread(Runnable runnable) {
        synchronized (handler) {
            handler.removeCallbacks(runnable);
        }
    }

    public void registerContactObserver() {
        getContentResolver().registerContentObserver(ContactUris.PHONE_CONTENT_URI, true, contactObserver);
    }

    public void unRegisterContactObserver() {
        getContentResolver().unregisterContentObserver(contactObserver);
    }

    public void setChatAccount(XmppAccount xmppAccount) {
        ChatPrefs.setChatAccountDetailInPrefs(instance, xmppAccount);
    }

    public XmppAccount getSavedChatAccount() {
        XmppAccount xmppAccount = ChatPrefs.getSavedChatAccount(instance);
        return xmppAccount;
    }

    public void clearChatAccount() {
        ChatPrefs.clearPrefsdata(instance);
        try {
            getXmpp().setXmppAccount(null);
        } catch (NullPointerException ignored) {
        }

    }

    public void saveDefaultStatus() {
        String[] defaults = getResources().getStringArray(R.array.defaultstatus);
        StatusModal statusModal = new StatusModal();
        for (String st : defaults) {
            statusModal.addNewStatus(st);
        }
        updateStatusList(statusModal);
    }

    public void updateStatusList(StatusModal statusModal) {
        ChatPrefs.saveDefaultStatus(instance, statusModal);
    }

    public StatusModal getDefaultStatus() {
        return ChatPrefs.getDefaultStatus(instance);
    }

    /*public void ClearDb() {
        RecentTable.getInstance().clearAllData();
        ContactTable.getInstance().clearAllData();
        MessageAndCallTable.getInstance().clearAllData();
    }*/


    @Override
    public void xmppConnected() {
        XmppLogger.printLog(TAG + " myXmppConnectionListner xmppConnected");
        if (!getXmpp().isWasAuthenticated()) {
            try {
                getXmpp().doLoginIfUserExist();
            } catch (IllegalStateException e) {
                e.printStackTrace();
                XmppLogger.printLog(TAG + " IllegalStateException=" + e.getMessage());
            }
        }

    }

    @Override
    public void xmppDisconnected() {
        XmppLogger.printLog(TAG + " myXmppConnectionListner xmppDisconnected");
    }

    @Override
    public void xmppLogedIn() {
        XmppLogger.printLog(TAG + " myXmppConnectionListner xmppLogedIn");
    }

    @Override
    public void xmppConnectError(Exception e) {
        XmppLogger.printLog(TAG + " myXmppConnectionListner xmppConnectError");
    }

    @Override
    public void xmppLoginInError(Exception e) {
        XmppLogger.printLog(TAG + " myXmppConnectionListner xmppLoginInError");
    }

    @Override
    public void onMediaUploadStart(UploadRequest uploadRequest) {

    }

    @Override
    public void onMediaUploadProgress(UploadRequest uploadRequest) {

    }

    @Override
    public void onMediaUploadEnd(UploadRequest uploadRequest) {
        getXmpp().handleMediaUploadEnd(uploadRequest);
    }

    @Override
    public void onMediaDownloadStart(DownloadRequest downloadRequest) {

    }

    @Override
    public void onMediaDownloadProgress(DownloadRequest downloadRequest) {

    }

    @Override
    public void onMediaDownloadEnd(DownloadRequest downloadRequest) {
        getXmpp().handleMediaDownloadEnd(downloadRequest);
    }

    @Override
    public void videoCompressStart(MessageModal messageModal) {

    }

    @Override
    public void videoCompressProgress(MessageModal messageModal) {

    }

    @Override
    public void videoCompressEnd(MessageModal messageModal, boolean success) {
        getXmpp().handleVideoCompressEnd(messageModal, success);
    }

    @Override
    public void onMessageAdded(MessageModal messageModal) {
        genratedNotification(messageModal);
    }

    @Override
    public void onMediaMessageAdded(MessageModal messageModal) {
        genratedNotification(messageModal);
    }

    @Override
    public void onMessageSent(MessageModal messageModal) {

    }

    @Override
    public void onMessageDelivered(MessageModal messageModal) {

    }

    @Override
    public void onMesssageDisplayed(MessageModal messageModal) {

    }

    @Override
    public void onMesssageDeliverySent(MessageModal messageModal) {

    }

    @Override
    public void onMesssageDisplaySent(MessageModal messageModal) {

    }

    public void sendComposingChatState(String jid) {
        if (getXmpp() != null)
            getXmpp().sendComposingChatState(jid);
    }

    public int deleteXmppAccount() {
        if (getXmpp() != null)
            return getXmpp().deleteXmppAccount();
        return 1;
    }

    public void updateAllPrivacyList() {
        if (getXmpp() != null)
            getXmpp().updateAllPrivacyList();
    }

    public boolean unBlockFriend(String jid) {
        if (getXmpp() != null)
            return getXmpp().unBlockFriend(jid);
        return false;
    }

    public XmppAccount getXmppAccount() {
        if (getXmpp() != null)
            return getXmpp().getXmppAccount();
        return null;
    }

    public void setXmppAccount(XmppAccount xmppAccount) {
        if (getXmpp() != null)
            getXmpp().setXmppAccount(xmppAccount);
    }

    public void setInitialVcard(XmppAccount xmppAccount) {
        if (getXmpp() != null)
            getXmpp().setInitialVcard(xmppAccount);
    }

    public void triggerOnRecentUpdate(RecentModal recentModal) {
        if (getXmpp() != null)
            getXmpp().triggerOnRecentUpdate(recentModal);
    }

    public void triggerOnMessageUpdateListner(int i, MessageModal messageModal) {
        if (getXmpp() != null)
            getXmpp().triggerOnMessageUpdateListner(i, messageModal);
    }

    public void sentDeliveryReceiptForMessage(MessageModal messageModal, org.jivesoftware.smack.packet.Message.Type chat) {
        if (getXmpp() != null)
            getXmpp().sentDeliveryReceiptForMessage(messageModal, chat);
    }

    public MyXMPP.ConnectionStates getConnectionStates() {
        if (getXmpp() != null)
            getXmpp().getConnectionStates();
        return null;
    }

    public void getAllXmppUsers() {
        if (getXmpp() != null)
            getXmpp().getAllXmppUsers();
    }

    public void loadMyAndRegisteredUserVcard() {
        if (getXmpp() != null)
            getXmpp().loadMyAndRegisteredUserVcard();
    }

    public void triggerContactListner(int i, ContactModal contactModal, List<ContactModal> o, List<ContactModal> o1) {
        if (getXmpp() != null)
            getXmpp().triggerContactListner(i, contactModal, o, o1);
    }

    public void disconnect() {
        if (getXmpp() != null)
            getXmpp().disconnect();

    }

    public void loadRegisteredContactVcards() {
        if (getXmpp() != null)
            getXmpp().loadRegisteredContactVcards();
    }

    public boolean isConnectionDisconnected() {
        if (getXmpp() != null)
            return getXmpp().isConnectionDisconnected();
        return false;
    }

    public void sendPauseChatState(String jid) {
        if (getXmpp() != null && jid != null)
            getXmpp().sendPauseChatState(jid);
    }

    public void retryMediaMessage(MessageModal messageModal) {
        if (getXmpp() != null && messageModal != null)
            getXmpp().retryMediaMessage(messageModal);
    }

    public void downloadMediaFromServer(MessageModal messageModal) {
        if (getXmpp() != null && messageModal != null)
            getXmpp().downloadMediaFromServer(messageModal);
    }

    public void sentDisplayReceiptForMessage(MessageModal messageModal, org.jivesoftware.smack.packet.Message.Type chat) {
        if (getXmpp() != null)
            getXmpp().sentDisplayReceiptForMessage(messageModal, chat);
    }

    public void getUserPresence(String jid) {
        if (getXmpp() != null && jid != null)
            getXmpp().getUserPresence(jid);
    }

    public void getLastActivityOfUser(String jid) {
        if (getXmpp() != null && jid != null)
            getXmpp().getLastActivityOfUser(jid);
    }

    public void sendImageMedia(ContactModal contactModal, String imgPath, String o, int i) throws FileNotFoundException {
        if (getXmpp() != null)
            getXmpp().sendImageMedia(contactModal, imgPath, null, i);
    }

    public void blockFriend(String jid) {
        if (getXmpp() != null && jid != null)
            getXmpp().blockFriend(jid);
    }

    public void sendUserDetailsMessage(String jid, String loginUserName, String loginProfileImg) {
        if (getXmpp() != null)
            getXmpp().sendUserDetailsMessage(jid, loginUserName, loginProfileImg);
    }

    public void AddContectToRoster(String jid) {
        if (getXmpp() != null)
            getXmpp().AddContectToRoster(jid);
    }

    public boolean isAuthenticated() {
        if (getXmpp() != null)
            return getXmpp().isAuthenticated();
        return false;
    }

    public void doLoginMannually(XmppAccount xmppAccount) {
        if (getXmpp() != null)
            getXmpp().doLoginMannually(xmppAccount);
    }

    public void registerNewUser(XmppAccount xmppAccount) {
        if (getXmpp() != null)
            getXmpp().registerNewUser(xmppAccount);
    }

   /* @Override
    public void newGroupCreated(ContactModal contactModal) {

    }

    @Override
    public void groupMemberAdded(ContactModal contactModal) {
        getXmpp().enterInGroup(contactModal);

    }*/

   /* @Override
    public void newGroupCreationError(ContactModal contactModal) {

    }

    @Override
    public void onEnterInGroup(ContactModal contactModal, boolean isSuccess) {
        if (isSuccess) {
            if (contactModal.getStatus().equals(ContactTable.STATUS_GROUP_NOT_CREATE + "") ||
                    contactModal.getStatus().equals(ContactTable.STATUS_GROUP_CREATE_FAILED + "")) {
//                getXmpp().sendConfigrationFormOfGroup(contactModal);
            } else if (contactModal.getStatus().equals(ContactTable.STATUS_GROUP_CREATED + "")) {
//                getXmpp().addMembersInGroup(contactModal);
            }
        }

    }

    @Override
    public void onGroupConfigPush(ContactModal contactModal, boolean isSuccess) {
        if (isSuccess) {
//            getXmpp().addMembersInGroup(contactModal);
        }

    }

    @Override
    public void onGroupMemberAdded(ContactModal contactModal, boolean isSuccess) {
        if (isSuccess) {
            getXmpp().sendInvitationToGroupMembers(contactModal);
        }
    }*/

    private class MyContentObserver extends ContentObserver {

        public MyContentObserver() {
            super(serviceHandler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            XmppLogger.printLog(TAG + " MyContentObserver onChange selfChange=" + selfChange);
            serviceHandler.removeMessages(2);
            serviceHandler.sendEmptyMessageDelayed(2, 2000);

        }

    }

    public void genratedNotification(MessageModal messageModal) {
        XmppLogger.printLog("Notification = " + messageModal.getData());
    }


}

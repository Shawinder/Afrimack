package com.chat.audiomessages;

import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by ubuntu on 22/12/16.
 */

public class AudioPlayHelper implements MediaPlayer.OnCompletionListener {
    public static final long PROGRESS_UPDATE_INTERVAL = 100;
    private static final String TAG = "AudioRecorderHelper";
    AudioPlayHelperListner audioPlayHelperListner;
    File playingFile;
    Handler handler = new Handler();
    private long MAX_DURATION_MILLI = 0;
    private MediaPlayer mediaPlayer = null;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (isPlaying()) {
                long second = mediaPlayer.getCurrentPosition();
                long remainSecond = MAX_DURATION_MILLI - second;
                if (audioPlayHelperListner != null) {
                    audioPlayHelperListner.onPlayerTimerChange(second, remainSecond);
                }
                updateTimer();
            }
        }
    };

    public AudioPlayHelper(AudioPlayHelperListner audioPlayHelperListner) {
        this.audioPlayHelperListner = audioPlayHelperListner;

    }

    public long getMAX_DURATION_MILLI() {
        return MAX_DURATION_MILLI;
    }

    public void updateTimer() {
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, PROGRESS_UPDATE_INTERVAL);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (audioPlayHelperListner != null) {

            audioPlayHelperListner.onPlayerEnd();
        }
    }

    public void setAudioPlayHelperListner(AudioPlayHelperListner audioPlayHelperListner) {
        this.audioPlayHelperListner = audioPlayHelperListner;
    }

    private void setPlayingFile(File playingFile) {
        this.playingFile = playingFile;
    }

    private boolean setPlayingFile(String playingFilePath) {
        if (playingFile == null || !playingFile.getAbsolutePath().equals(playingFilePath)) {
            stopPlayer();
            setPlayingFile(new File(playingFilePath));
            return true;
        }
        return false;
    }

    private boolean initializePlayer() {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(playingFile.getAbsolutePath());
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.prepare();
        } catch (IOException e) {
            Log.e(TAG, "mediaPlayer prepare() failed");
            mediaPlayer = null;
            return false;
        }
        return true;
    }

    public boolean isPlaying() {
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    public boolean startPlaying(String filePath) {
        try {
            if (setPlayingFile(filePath)) {
                initializePlayer();
            }
            if (mediaPlayer != null) {
                mediaPlayer.start();
                MAX_DURATION_MILLI = mediaPlayer.getDuration();
                updateTimer();
                if (audioPlayHelperListner != null) {
                    audioPlayHelperListner.onPlayerStart();
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            mediaPlayer = null;
        }
        return false;

    }

    public void pausePlayer() {
        if (null != mediaPlayer && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            if (audioPlayHelperListner != null) {
                audioPlayHelperListner.onPlayerPause();
            }
        }
    }

    public void stopPlayer() {
        if (null != mediaPlayer) {
            try {
                mediaPlayer.stop();
                mediaPlayer.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mediaPlayer = null;
            if (audioPlayHelperListner != null) {
                audioPlayHelperListner.onPlayerEnd();
            }
        }
    }


    public interface AudioPlayHelperListner {

        void onPlayerTimerChange(long secondInMilli, long remainSecondInMilli);

        void onPlayerStart();

        void onPlayerEnd();

        void onPlayerPause();


    }
}

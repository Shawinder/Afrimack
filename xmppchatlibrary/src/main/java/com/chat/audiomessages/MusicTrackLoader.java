package com.chat.audiomessages;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;


import com.chat.ChatApplication;

import org.jivesoftware.smack.chat.Chat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 23/12/16.
 */

public class MusicTrackLoader {
    static MusicTrackLoader instance;

    static {
        instance = new MusicTrackLoader();

    }

    ContentResolver cr;
    List<MusicModal> musicModalList;
    MusicTask musicTask;
    Handler handler = new Handler();

    private MusicTrackLoader() {
        cr = ChatApplication.getInstance().getContentResolver();
        musicModalList = new ArrayList<>();
    }

    public static MusicTrackLoader getInstance() {
        return instance;
    }

    public List<MusicModal> getMusicModalList() {
        return musicModalList;
    }

    public void getMusicTracks(MusicTrackLoaderListner musicTrackLoaderListner) {
        if (musicTask != null) {
            if (musicTask.isRunning()) {
                musicTask.setCancelled();
            }
        }
        musicTask = new MusicTask();
        musicTask.execute(musicTrackLoaderListner);
    }

    public interface MusicTrackLoaderListner {
        void loadingStart();

        void loadingEnd();
    }

    public class MusicTask extends AsyncTask<MusicTrackLoaderListner, Void, List<MusicModal>> {
        MusicTrackLoaderListner musicTrackLoaderListner;
        boolean cancelled = false;
        boolean running = false;

        public boolean isRunning() {
            return running;
        }

        public void setCancelled() {
            cancelled = true;
        }

        @Override
        protected void onPostExecute(List<MusicModal> musicModals) {
            super.onPostExecute(musicModals);
            if (!cancelled) {
                musicModalList.clear();
                musicModalList.addAll(musicModals);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (musicTrackLoaderListner != null) {
                            musicTrackLoaderListner.loadingEnd();
                        }
                    }
                });
            }
            running = false;
        }

        @Override
        protected List<MusicModal> doInBackground(MusicTrackLoaderListner... params) {
            running = true;
            musicTrackLoaderListner = params[0];
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (musicTrackLoaderListner != null) {
                        musicTrackLoaderListner.loadingStart();
                    }
                }
            });
            List<MusicModal> dataList = new ArrayList<>();
            Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
            String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
            Cursor cur = cr.query(uri, null, selection, null, sortOrder);
            int count = 0;
            if (cur != null) {
                count = cur.getCount();
                if (count > 0) {
                    while (cur.moveToNext() && !cancelled) {
                        String data = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA));
                        String title = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE));
                        String album = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                        String artist = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                        long duration = cur.getLong(cur.getColumnIndex(MediaStore.Audio.Media.DURATION));

                        MusicModal musicModal = new MusicModal();
                        musicModal.setMusicPath(data);
                        musicModal.setTitle(title);
                        musicModal.setAlbum(album);
                        musicModal.setArtist(artist);
                        musicModal.setDuration(duration);
                        dataList.add(musicModal);
                    }

                }
                cur.close();

            }
            return dataList;
        }
    }
}

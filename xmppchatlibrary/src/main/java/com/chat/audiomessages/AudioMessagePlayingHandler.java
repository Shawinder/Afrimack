package com.chat.audiomessages;

import com.chat.ChatApplication;
import com.chat.modals.MessageModal;
import com.chat.xmpp.listners.AudioMessagePlayingListner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by ubuntu on 22/12/16.
 */

public class AudioMessagePlayingHandler {

    static AudioMessagePlayingHandler instance;

    static {
        instance = new AudioMessagePlayingHandler();
    }

    HashMap<String, AudioMessagePlayHelper> mapData;
    AudioMessagePlayHelper currentAudioPlayHelper;

    private AudioMessagePlayingHandler() {
        mapData = new HashMap<>();
    }

    public static AudioMessagePlayingHandler getInstance() {
        return instance;
    }

    public AudioMessagePlayHelper getAudioMessageHelper(String messageId) {
        return mapData.get(messageId);
    }

    public void clearAudioMessagePlayingHandler() {
        if (currentAudioPlayHelper != null) {
            currentAudioPlayHelper.stopPlayer();
        }
        Iterator<Map.Entry<String, AudioMessagePlayHelper>> iterator = mapData.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, AudioMessagePlayHelper> entry = iterator.next();
            AudioMessagePlayHelper audioMessagePlayHelper = entry.getValue();
            audioMessagePlayHelper.stopPlayer();
        }
        mapData.clear();
    }

    public void playPauseAudio(MessageModal messageModal) {
        boolean samePause = false;
        if (currentAudioPlayHelper != null) {
            if (currentAudioPlayHelper.isPlaying()) {
                currentAudioPlayHelper.pausePlayer();
                samePause = true;
            }
        }
        AudioMessagePlayHelper audioMessagePlayHelper = getAudioMessageHelper(messageModal.getMessage_id());
        if (audioMessagePlayHelper == null) {
            audioMessagePlayHelper = new AudioMessagePlayHelper(messageModal.getTo_jid(),
                    messageModal.getMessage_id(),
                    messageModal.getMedia_name());
            mapData.put(messageModal.getMessage_id(), audioMessagePlayHelper);
        }
        if (audioMessagePlayHelper.isPlaying()) {
            audioMessagePlayHelper.pausePlayer();
        } else if (!samePause || currentAudioPlayHelper != audioMessagePlayHelper) {
            audioMessagePlayHelper.startPlay();
        }

    }

    /**
     * @param tag       1=> startPlay
     *                  2=>pausePlay
     *                  3=>progressPlay
     *                  4=>endPlay
     * @param messageId
     */

    public void triggerAudioMessagePlayingListner(int tag, String messageId, String toJid) {
        final ArrayList<AudioMessagePlayingListner> list = (ArrayList<AudioMessagePlayingListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(AudioMessagePlayingListner.class);

        switch (tag) {
            case 1:
                for (AudioMessagePlayingListner audioMessagePlayingListner : list) {
                    audioMessagePlayingListner.startPlay(toJid, messageId);
                }
                break;
            case 2:
                for (AudioMessagePlayingListner audioMessagePlayingListner : list) {
                    audioMessagePlayingListner.pausePlay(toJid, messageId);
                }
                break;
            case 3:
                for (AudioMessagePlayingListner audioMessagePlayingListner : list) {
                    audioMessagePlayingListner.progressPlay(toJid, messageId);
                }
                break;
            case 4:
                for (AudioMessagePlayingListner audioMessagePlayingListner : list) {
                    audioMessagePlayingListner.stopPlay(toJid, messageId);
                }
                break;
        }
    }

    public class AudioMessagePlayHelper extends AudioPlayHelper implements AudioPlayHelper.AudioPlayHelperListner {
        String messageId;
        String toJid;
        String filePath;
        long maxProgress;
        long currentProgress;
        long remainProgress;

        public AudioMessagePlayHelper(String toJid, String messageId, String filePath) {
            super(null);
            setAudioPlayHelperListner(this);
            this.toJid = toJid;
            this.messageId = messageId;
            this.filePath = filePath;
        }

        private String getSecond(long secondInMilli) {
            long second = secondInMilli / 1000;
            long min = second / 60;
            long sec = second - (min * 60);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(min < 10 ? "0" + min : min);
            stringBuilder.append(":");
            stringBuilder.append(sec < 10 ? "0" + sec : sec);
            return stringBuilder.toString();
        }

        public long getMaxProgress() {
            return maxProgress;
        }

        public long getCurrentProgress() {
            return currentProgress;
        }

        public long getRemainProgress() {
            return remainProgress;
        }

        public String getCurrentTime() {
            return getSecond(currentProgress);
        }

        public String getRemainTime() {
            return getSecond(remainProgress);
        }

        public void startPlay() {
            startPlaying(filePath);
        }

        @Override
        public void onPlayerTimerChange(long secondInMilli, long remainSecondInMilli) {
            currentProgress = secondInMilli;
            remainProgress = remainSecondInMilli;
            triggerAudioMessagePlayingListner(3, messageId, toJid);
        }

        @Override
        public void onPlayerStart() {
            currentAudioPlayHelper = this;
            maxProgress = getMAX_DURATION_MILLI();
            triggerAudioMessagePlayingListner(1, messageId, toJid);
        }

        @Override
        public void onPlayerEnd() {
            currentProgress = 0;
            remainProgress = 0;
            triggerAudioMessagePlayingListner(4, messageId, toJid);
        }

        @Override
        public void onPlayerPause() {
            triggerAudioMessagePlayingListner(2, messageId, toJid);
        }
    }
}

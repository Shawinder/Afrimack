package com.chat.database.listners;

/**
 * Created by ubuntu on 18/11/16.
 */

public interface OnMigrationListener {

    void onMigrate(int oldeVersion);
}

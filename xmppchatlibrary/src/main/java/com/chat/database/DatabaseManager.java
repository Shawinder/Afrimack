package com.chat.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.chat.ChatApplication;
import com.chat.database.listners.OnMigrationListener;
import com.chat.database.tables.ContactTable;
import com.chat.database.tables.MessageAndCallTable;
import com.chat.database.tables.RecentTable;
import com.chat.xmpp.XmppLogger;
import com.chat.xmpp.listners.OnClearListener;
import com.chat.xmpp.listners.OnLoadListner;
import com.chat.xmpp.listners.OnVcardLoadListner;

import org.jivesoftware.smackx.vcardtemp.packet.VCard;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by ubuntu on 18/11/16.
 */

public class DatabaseManager extends SQLiteOpenHelper implements OnLoadListner, OnClearListener, OnVcardLoadListner {
    public static final int DATABASE_VERSION = 13;
    private static final String TAG = "DatabaseManager";
    public static String DATABASE_NAME = "afrimackchattest.db";
    private static final SQLiteException DOWNGRAD_EXCEPTION = new SQLiteException(
            "Database file was deleted");

    private final static DatabaseManager instance;

    static {
        instance = new DatabaseManager();
        ChatApplication.getInstance().getXmppListners().addOnLoadListner(instance);
        ChatApplication.getInstance().getXmppListners().addOnClearListner(instance);
        ChatApplication.getInstance().getXmppListners().addUiListner(instance);
    }

    private final ArrayList<DatabaseTable> registeredTables;

    public DatabaseManager() {
        super(ChatApplication.getInstance(), DATABASE_NAME, null, DATABASE_VERSION);
        registeredTables = new ArrayList<DatabaseTable>();
    }

    public static DatabaseManager getInstance() {
        return instance;
    }

    public static void execSQL(SQLiteDatabase db, String sql) {
        XmppLogger.printLog(TAG + " execSQL=" + sql);
        db.execSQL(sql);
    }

    public static void dropTable(SQLiteDatabase db, String table) {
        XmppLogger.printLog(TAG + " dropTable=" + table);
        execSQL(db, "DROP TABLE IF EXISTS " + table + ";");
    }

    public static void renameTable(SQLiteDatabase db, String table,
                                   String newTable) {
        XmppLogger.printLog(TAG + " renameTable table" + table + " to newTable=" + newTable);
        execSQL(db, "ALTER TABLE " + table + " RENAME TO " + newTable + ";");
    }

    public void addTable(DatabaseTable table) {
        XmppLogger.printLog(TAG + " addTable=" + table);
        registeredTables.add(table);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        XmppLogger.printLog(TAG + " onCreate");
        for (DatabaseTable table : registeredTables)
            table.create(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        XmppLogger.printLog(TAG + " onUpgrade");
        if (oldVersion > newVersion) {
            XmppLogger.printLog(TAG + " Downgrading database from version "
                    + oldVersion + " to " + newVersion);
            File file = new File(db.getPath());
            file.delete();
            XmppLogger.printLog(TAG + " Database file was deleted");
            throw DOWNGRAD_EXCEPTION;
        } else {
            XmppLogger.printLog(TAG + " Upgrading database from version " + oldVersion
                    + " to " + newVersion);
            // while (oldVersion < newVersion) {
            oldVersion = newVersion;
            XmppLogger.printLog(TAG + " Migrate to version " + oldVersion);
            migrate(db, oldVersion);
            for (DatabaseTable table : registeredTables)
                table.migrate(db, oldVersion);
            for (OnMigrationListener listener : ChatApplication.getInstance()
                    .getXmppListners().getOnMigrationListenerArrayList())
                listener.onMigrate(oldVersion);
            // }
        }
    }

    private void migrate(SQLiteDatabase db, int toVersion) {
        XmppLogger.printLog(TAG + " migrate toVersion=" + toVersion);
        switch (toVersion) {
            case 6:
                RecentTable.getInstance().create(db);
                MessageAndCallTable.getInstance().create(db);
                break;

            default:
                break;
        }
    }

    @Override
    public void onLoad() {
        XmppLogger.printLog(TAG + " onLoad");
        try {
            XmppLogger.printLog(TAG + " onLoad getWritableDatabase");
            getWritableDatabase(); // Force onCreate or onUpgrade
        } catch (SQLiteException e) {
            XmppLogger.printLog(TAG + " onLoad SQLiteException=" + e.getMessage());
            if (e == DOWNGRAD_EXCEPTION) {
                // Downgrade occured
            } else {
                throw e;
            }
        }
    }

    @Override
    public void onClear() {
        XmppLogger.printLog(TAG + " onClear");
        for (DatabaseTable table : registeredTables)
            table.clear();
    }


    @Override
    public void vcardLoad(final String jid, final VCard vCard, Exception e) {
        if (e == null) {
            if (vCard == null) return;
           /* ChatApplication.getInstance().runInDatabaseBackground(new Runnable() {
                @Override
                public void run() {
                    ContactTable.getInstance().updateContactInfoUsingVcard(jid, vCard);
                }
            });*/
        }
    }
}

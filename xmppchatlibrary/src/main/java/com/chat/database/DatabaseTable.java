package com.chat.database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by ubuntu on 18/11/16.
 */

public interface DatabaseTable {

    void create(SQLiteDatabase db);

    void migrate(SQLiteDatabase db, int toVersion);

    void clear();

}

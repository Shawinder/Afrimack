package com.chat.database.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.chat.database.DatabaseManager;
import com.chat.database.DatabaseTable;
import com.chat.xmpp.XmppLogger;

/**
 * Created by ubuntu on 18/11/16.
 */

public abstract class AbstractTable implements DatabaseTable {

    public static final String TAG = "AbstraceTable";

    protected abstract String getTableName();

    protected abstract String[] getProjection();

    protected String getListOrder() {
        return null;
    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        XmppLogger.printLog(TAG + " " + getTableName() + " migrate toVersion=" + toVersion);
    }


    public Cursor listFromQuery(String query) {
        SQLiteDatabase db = DatabaseManager.getInstance().getReadableDatabase();
        return db.rawQuery(query, null);
    }

    public Cursor list() {
        SQLiteDatabase db = DatabaseManager.getInstance().getReadableDatabase();
        return db.query(getTableName(), getProjection(), null, null, null,
                null, getListOrder());
    }

    /*public int clearData(String TableName) {
        SQLiteDatabase db = DatabaseManager.getInstance().getWritableDatabase();
        return db.delete(TableName, null, null);
    }*/

    public int clearChat(String tableName, String whereClause) {
        SQLiteDatabase db = DatabaseManager.getInstance().getWritableDatabase();
        return db.delete(tableName, whereClause, null);
    }

    @Override
    public void clear() {
        SQLiteDatabase db = DatabaseManager.getInstance().getWritableDatabase();
        XmppLogger.printLog(TAG + " " + getTableName() + " clear");
        db.delete(getTableName(), null, null);
    }
}

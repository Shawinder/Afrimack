package com.chat.database.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.chat.contacts.ContactModal;
import com.chat.database.DatabaseManager;
import com.chat.xmpp.XmppConstant;
import com.chat.xmpp.XmppLogger;
import com.chat.xmpp.XmppUtils;

import org.jivesoftware.smackx.vcardtemp.packet.VCard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ubuntu on 18/11/16.
 */

public class ContactTable extends AbstractTable {
    public static final int STATUS_GROUP_NOT_CREATE = 1;
    public static final int STATUS_GROUP_CREATE_RUNNING = 2;
    public static final int STATUS_GROUP_CREATED = 3;
    public static final int STATUS_GROUP_CREATE_FAILED = 4;
    public static final int STATUS_GROUP_SETTLED = 5;
    public static final String TAG = "ContactTable";
    private static final String NAME = "contacts";
    private static final String[] PROJECTION = new String[]{Fields.RAW_CONTACT_ID,
            Fields.JID, Fields.TYPE, Fields.NUMBER, Fields.FORMATTED_NUMBER,
            Fields.DEVICE_NAME,
            Fields.SERVER_NAME, Fields.REGISTERED, Fields.STRANGER,
            Fields.STATUS, Fields.AVATAR};
    private final static ContactTable instance;

    static {
        instance = new ContactTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }

    private final DatabaseManager databaseManager;

    private ContactTable(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public static ContactTable getInstance() {
        return instance;
    }

    public static long getRawContactId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.RAW_CONTACT_ID));
    }

    public static int getType(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.TYPE));
    }

    public static String getJid(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.JID));
    }

    public static String getNumber(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.NUMBER));
    }

    public static String getFormattedNumber(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.FORMATTED_NUMBER));
    }

    public static String getDeviceName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.DEVICE_NAME));
    }

    public static String getServerName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.SERVER_NAME));
    }

    public static boolean isRegistered(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.REGISTERED)) != 0;
    }

    /*Blocked*/
    public static int isBlocked(String jid) {
        String query = "SELECT * FROM "
                + NAME + " WHERE " + Fields.JID + "='" + jid + "' AND " + Fields.BLOCKED + " = 1 ";

        SQLiteDatabase db = DatabaseManager.getInstance().getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int cursorCount = cursor.getCount();
        if (cursor != null) {
            if (cursorCount > 0) {
                while (cursor.moveToNext()) {
                    return cursor.getInt(cursor.getColumnIndex(Fields.BLOCKED));
                }
            }
        }
        cursor.close();
        return 0;
    }

    public void setBlockedStatus(String jid, int blockedStatus) {
        if (jid == null) {
            return;
        }
        String sql = "UPDATE " + NAME + " SET " + Fields.BLOCKED + " = " + blockedStatus + " WHERE " + Fields.JID + " = '" + jid + "' ;";
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        DatabaseManager.execSQL(db, sql);

    }

    public static boolean isStranger(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.STRANGER)) != 0;
    }

    public static String getStatus(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.STATUS));
    }

    public static byte[] getAvatar(Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndex(Fields.AVATAR));
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void create(SQLiteDatabase db) {

        DatabaseManager.execSQL(db, generateCreateSql());
    }

    private String generateCreateSql() {
        return "CREATE TABLE " + NAME + " ("
                + Fields.JID + " TEXT PRIMARY KEY,"
                + Fields.TYPE + " INTEGER,"
                + Fields.RAW_CONTACT_ID + " INTEGER,"
                + Fields.NUMBER + " TEXT,"
                + Fields.FORMATTED_NUMBER + " TEXT,"
                + Fields.DEVICE_NAME + " TEXT,"
                + Fields.SERVER_NAME + " TEXT,"
                + Fields.STATUS + " TEXT,"
                + Fields.REGISTERED + " INTEGER,"
                + Fields.STRANGER + " INTEGER,"
                + Fields.BLOCKED + " INTEGER DEFAULT 0,"
                + Fields.AVATAR + " BLOB);";
    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        XmppLogger.printLog(TAG + " " + getTableName() + " migrate toVersion=" + toVersion);
        String sql;
        switch (toVersion) {
            case 2:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.STRANGER + " INTEGER;";
                DatabaseManager.execSQL(db, sql);
                sql = "UPDATE " + NAME + " SET " + Fields.STRANGER + " = 0;";
                DatabaseManager.execSQL(db, sql);
                break;
            case 3:
                DatabaseManager.renameTable(db, NAME, NAME + "_");
                sql = generateCreateSql();
                DatabaseManager.execSQL(db, sql);
                sql = "INSERT INTO " + NAME + " SELECT " +
                        Fields.JID + "," + 0 + "," + Fields.NUMBER + "," + Fields.DEVICE_NAME + "," +
                        Fields.SERVER_NAME + "," + Fields.REGISTERED + "," +
                        Fields.STRANGER + " FROM " + NAME + "_";
                DatabaseManager.execSQL(db, sql);
                DatabaseManager.dropTable(db, NAME + "_");
            case 4:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.FORMATTED_NUMBER + " TEXT;";
                DatabaseManager.execSQL(db, sql);
                sql = "UPDATE " + NAME + " SET " + Fields.FORMATTED_NUMBER + " = 0;";
                DatabaseManager.execSQL(db, sql);
                break;
            case 5:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.STATUS + " TEXT;";
                DatabaseManager.execSQL(db, sql);
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.AVATAR + " BLOB DEFAULT NULL;";
                DatabaseManager.execSQL(db, sql);
                break;
            case 9:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.TYPE + " INTEGER DEFAULT " + RecentTable.TYPE_SINGLE_CHAT + ";";
                DatabaseManager.execSQL(db, sql);
                sql = "UPDATE " + NAME + " SET " + Fields.TYPE + " = " + RecentTable.TYPE_SINGLE_CHAT + ";";
                DatabaseManager.execSQL(db, sql);

                break;

            case 13:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.BLOCKED + " INTEGER DEFAULT 0;";
                DatabaseManager.execSQL(db, sql);
                sql = "UPDATE " + NAME + " SET " + Fields.BLOCKED + " = 0;";
                DatabaseManager.execSQL(db, sql);

                break;

            default:
                break;
        }
    }

    public void writeList(ArrayList<ContactModal> list, ArrayList<ContactModal> addedContacts,
                          ArrayList<ContactModal> updatedContacts) {
        XmppLogger.printLog(TAG + " writeList");
        if (list == null) {
            XmppLogger.printLog(TAG + " writeList list is null");
            return;
        }
        for (ContactModal contactModal : list) {
            boolean add = write(null, contactModal);
            if (add) {
                addedContacts.add(contactModal);
            } else {
                updatedContacts.add(contactModal);
            }
        }
    }

    public boolean write(String jid, ContactModal contactModal) {
        /*if (contactModal.isAlreadyLocal()) {
            jid = contactModal.getJid();
        }*/
        ContentValues values = new ContentValues();
//        values.put(Fields.RAW_CONTACT_ID, contactModal.getRawContactId());
        values.put(Fields.JID, contactModal.getJid());
        values.put(Fields.TYPE, contactModal.getType());

       /* if (contactModal.getDeviceNumber() == null) {
            values.putNull(Fields.NUMBER);
        } else {
            values.put(Fields.NUMBER, contactModal.getDeviceNumber());
        }
        if (contactModal.getFormattedNumber() == null) {
            values.putNull(Fields.FORMATTED_NUMBER);
        } else {
            values.put(Fields.FORMATTED_NUMBER, contactModal.getFormattedNumber());
        }*/
        if (contactModal.getDisplayName() == null) {
            values.putNull(Fields.DEVICE_NAME);
        } else {
            values.put(Fields.DEVICE_NAME, contactModal.getDisplayName());
        }

        if (contactModal.getStatus() != null) {
            values.put(Fields.STATUS, contactModal.getStatus());
        }

        if (contactModal.getVcardAvatar() != null) {
            values.put(Fields.AVATAR, contactModal.getVcardAvatar());
        }

//        values.put(Fields.REGISTERED, contactModal.isRegistered() ? 1 : 0);
//        values.put(Fields.STRANGER, contactModal.isStranger() ? 1 : 0);

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        if (jid == null) {
            XmppLogger.printLog(TAG + " write values=" + contactModal.toString());
            try {
                db.insert(NAME, Fields.JID, values);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        }
        XmppLogger.printLog(TAG + " update where jid=" + jid + " and values=" + contactModal.toString());
        db.update(NAME, values, Fields.JID + " = ?", new String[]{String.valueOf(jid)});
        return false;
    }

    public void writeStrangerList(ArrayList<ContactModal> list, ArrayList<ContactModal> updatedContacts) {
        XmppLogger.printLog(TAG + " writeStrangerList");
        if (list == null) {
            XmppLogger.printLog(TAG + " writeStrangerList list is null");
            return;
        }
        for (ContactModal contactModal : list) {
            long update = writeStranger(contactModal.getJid(), contactModal);
            if (update == 1) {
                updatedContacts.add(contactModal);
            }
        }
    }

    public long writeStranger(String jid, ContactModal contactModal) {
        /*if (contactModal.isRegistered()) {
            if (contactModal.isStranger()) {
                XmppLogger.printLog(TAG + " writeStranger contact is already stranger jid=" + jid);
                return -1;
            }
//            contactModal.setRawContactId(0);
            contactModal.setDisplayName(null);
//            contactModal.setStranger(true);

            ContentValues values = new ContentValues();
            values.put(Fields.RAW_CONTACT_ID, 0);
            values.putNull(Fields.DEVICE_NAME);
            values.put(Fields.STRANGER, 1);
            SQLiteDatabase db = databaseManager.getWritableDatabase();
            XmppLogger.printLog(TAG + " update stranger where jid=" + jid + " and values=" + contactModal.toString());
            db.update(NAME, values, Fields.JID + " = ?", new String[]{String.valueOf(jid)});
            return 1;
        }*/
        XmppLogger.printLog(TAG + " remove stranger jid=" + jid);
        remove(jid);
        return 2;

    }

    public int updateContactInfoUsingVcard(String jid, VCard vCard) {
        ContentValues values = new ContentValues();
        values.put(Fields.SERVER_NAME, vCard.getNickName());
        values.put(Fields.DEVICE_NAME, vCard.getNickName());
        values.put(Fields.AVATAR, vCard.getAvatar());
        values.put(Fields.STATUS, vCard.getField(XmppConstant.VCARD_USER_IMG));
        SQLiteDatabase db = databaseManager.getWritableDatabase();

        XmppLogger.printLog(TAG + " updateContactInfoUsingVcard where jid=" + jid);
        return db.update(NAME, values, Fields.JID + " = ?", new String[]{String.valueOf(jid)});
    }


    public int updateContactInfoUsingVcard(String jid, String userName, String userImg) {
        ContentValues values = new ContentValues();
        values.put(Fields.DEVICE_NAME, userName);
        values.put(Fields.STATUS, userImg);
        SQLiteDatabase db = databaseManager.getWritableDatabase();

        XmppLogger.printLog(TAG + " updateContactInfoUsingVcard where jid=" + jid);
        return db.update(NAME, values, Fields.JID + " = ?", new String[]{String.valueOf(jid)});
    }

    public ArrayList<ContactModal> getAllSingleContacts() {
        String query = "SELECT * FROM "
                + NAME
                + " WHERE " + Fields.TYPE + "=" + RecentTable.TYPE_SINGLE_CHAT;
        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult(cursor);
    }

    public ArrayList<ContactModal> getAllContacts() {
        String query = "SELECT * FROM " + NAME;
        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult(cursor);
    }

    public HashMap<String, ContactModal> getAllContactMap() {
        HashMap<String, ContactModal> contactMap = new HashMap<>();
        ArrayList<ContactModal> list = getAllContacts();
        for (ContactModal contactModal : list) {
            contactMap.put(contactModal.getJid(), contactModal);
        }
        return contactMap;
    }

    public ArrayList<String> getAllNonStrangersContacts() {
        String query = "SELECT * FROM (SELECT "
                + Fields.JID + "," + Fields.REGISTERED + " FROM "
                + NAME + " WHERE " + Fields.STRANGER + "!=1"
                + " AND " + Fields.TYPE + "=" + RecentTable.TYPE_SINGLE_CHAT
                + " ORDER BY " + Fields.DEVICE_NAME + " ASC) ORDER BY "
                + Fields.REGISTERED + " DESC";

        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult_as_StringList(cursor);
    }

    public ArrayList<ContactModal> getAllRegisteredContacts() {
        String query = "SELECT * FROM "
                + NAME + " WHERE " + Fields.REGISTERED + "=1"
                + " AND " + Fields.TYPE + "=" + RecentTable.TYPE_SINGLE_CHAT;
        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult(cursor);
    }

    public ArrayList<String> getAllRegisteredContactsJid() {
        String query = "SELECT " + Fields.JID + " FROM "
                + NAME + " WHERE " + Fields.REGISTERED + "=1"
                + " AND " + Fields.TYPE + "=" + RecentTable.TYPE_SINGLE_CHAT;
        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult_as_StringList(cursor);
    }

    public ArrayList<String> getAllRegisteredNonStrangerContactsJid() {
        String query = "SELECT " + Fields.JID + " FROM "
                + NAME + " WHERE " + Fields.REGISTERED + "=1"
                + " AND " + Fields.STRANGER + "=0"
                + " AND " + Fields.TYPE + "=" + RecentTable.TYPE_SINGLE_CHAT;
        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult_as_StringList(cursor);
    }

    public ContactModal getContactDetail(String jid) {
        String query = "SELECT * FROM "
                + NAME + " WHERE " + Fields.JID + "='" + jid + "'";
        Cursor cursor = super.listFromQuery(query);
        ArrayList<ContactModal> list = parseCursorResult(cursor);
        if (list.size() > 0) {
            cursor.close();
            return list.get(0);
        }
        return null;
    }

    /*Blocked Contact List*/
    public List<ContactModal> getBlockedContactDetail() {
        String query = "SELECT * FROM "
                + NAME + " WHERE " + Fields.BLOCKED + "=" + 1;
        Cursor cursor = super.listFromQuery(query);
        ArrayList<ContactModal> list = parseCursorResult(cursor);
        if (list.size() > 0) {
            cursor.close();
            return list;
        }
        return null;
    }

    private synchronized ArrayList<ContactModal> parseCursorResult(Cursor cursor) {
        ArrayList<ContactModal> list = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    ContactModal contactModal = new ContactModal(XmppConstant.serviceName);
//                    contactModal.setRawContactId(getRawContactId(cursor));
//                    contactModal.setDeviceNumber(getNumber(cursor));
//                    contactModal.setFormattedNumber(getFormattedNumber(cursor));
                    contactModal.setJid(getJid(cursor));
                    contactModal.setType(getType(cursor));
                    contactModal.setDisplayName(getDeviceName(cursor));
//                    contactModal.setServerDisplayName(getServerName(cursor));
//                    contactModal.setRegistered(isRegistered(cursor));
//                    contactModal.setStranger(isStranger(cursor));
                    contactModal.setStatus(getStatus(cursor));
                    contactModal.setVcardAvatar(getAvatar(cursor));
                    contactModal.setStatus_for_ui(contactModal.getStatus_for_ui());
                    list.add(contactModal);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    private synchronized ArrayList<String> parseCursorResult_as_StringList(Cursor cursor) {
        ArrayList<String> list = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    list.add(getJid(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public int remove(String jid) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        XmppLogger.printLog(TAG + " remove jid=" + jid);
        return db.delete(NAME, Fields.JID + " = ?", new String[]{String.valueOf(jid)});
    }

    /*public int clearAllData(){
        return  super.clearData(NAME);
    }*/

    public boolean checkContactNumberRegistered(String number) {
        String toJid = XmppUtils.getJidFromNumber(number);
        if (toJid == null) {
            return false;
        }
        String query = "SELECT * FROM "
                + NAME + " WHERE " + Fields.JID + "='" + toJid + "' AND "
                + Fields.REGISTERED + "=1"
                + " AND " + Fields.TYPE + "=" + RecentTable.TYPE_SINGLE_CHAT;

        Cursor cursor = super.listFromQuery(query);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                return true;
            }
        }
        return false;
    }

    public ContactModal checkContactExist(String jid) {
        String query = "SELECT * FROM "
                + NAME + " WHERE " + Fields.JID + "='" + jid + "'";

        Cursor cursor = super.listFromQuery(query);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ContactModal contactModal = new ContactModal(XmppConstant.serviceName);
                contactModal.setDisplayName(getDeviceName(cursor));
                contactModal.setJid(getJid(cursor));
                contactModal.setType(getType(cursor));
                contactModal.setStatus(getStatus(cursor));
                contactModal.setStatus_for_ui(getStatus(cursor));
//                contactModal.setFormattedNumber(getFormattedNumber(cursor));
                contactModal.setVcardAvatar(getAvatar(cursor));
                return contactModal;
            }
        }
        return null;
    }

    public ContactModal addContactIfNotExist(String jid) {
        ContactModal contactModal = checkContactExist(jid);
        if (contactModal == null) {
            contactModal = new ContactModal(XmppConstant.serviceName);
            contactModal.setJid(jid);
//            contactModal.setFormattedNumber("+" + jid.split("@")[0]);
//            contactModal.setDeviceNumber("+" + jid.split("@")[0]);
//            contactModal.setRawContactId(0);
//            contactModal.setStranger(true);
//            contactModal.setRegistered(true);
            write(null, contactModal);
            contactModal.setNewAdded(true);
        }
        return contactModal;
    }

    /*Add all contact user by server api*/
    public ContactModal addContactIfNotExist(ContactModal modal) {
        XmppLogger.printLog("AddContactIfNotExist"+modal.getDisplayName());
        ContactModal contactModal = checkContactExist(modal.getJid());
        if (contactModal == null) {
            boolean insertContactStatus = write(null, modal);
            XmppLogger.printLog("Insert Contact Status " + insertContactStatus);
            modal.setNewAdded(true);
            return modal;
        }
        return contactModal;
    }

    public ContactModal addGroupIfNotExist(String jid) {
        ContactModal contactModal = checkContactExist(jid);
        if (contactModal == null) {
            contactModal = new ContactModal(XmppConstant.serviceName);
            contactModal.setJid(jid);
            contactModal.setType(RecentTable.TYPE_MULTI_CHAT);
//            contactModal.setRawContactId(0);
//            contactModal.setStranger(false);
//            contactModal.setRegistered(true);
            write(null, contactModal);
            contactModal.setNewAdded(true);
        }
        return contactModal;
    }

    public int updateGroupStatus(String jid, int group_status) {
        ContentValues values = new ContentValues();
        values.put(Fields.STATUS, group_status + "");
        SQLiteDatabase db = databaseManager.getWritableDatabase();

        XmppLogger.printLog(TAG + " updateGroupStatus where jid=" + jid);
        return db.update(NAME, values, Fields.JID + " = ?", new String[]{String.valueOf(jid)});
    }

    public int updateGroupSubjectAndAvatar(String jid, String subject, byte[] avatar) {
        ContentValues values = new ContentValues();
        if (subject == null) {
            values.putNull(Fields.SERVER_NAME);
            values.putNull(Fields.DEVICE_NAME);
        } else {
            values.put(Fields.SERVER_NAME, subject);
            values.put(Fields.DEVICE_NAME, subject);
        }
        if (avatar == null) {
            values.putNull(Fields.AVATAR);
        } else {
            values.put(Fields.AVATAR, avatar);
        }
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        XmppLogger.printLog(TAG + " updateGroupSubjectAndAvatar where jid=" + jid);
        return db.update(NAME, values, Fields.JID + " = ?", new String[]{String.valueOf(jid)});
    }

    public static final class Fields implements BaseColumns {
        public static final String RAW_CONTACT_ID = "raw_contact_id";
        public static final String JID = "jid";
        public static final String TYPE = "type";
        public static final String NUMBER = "number";
        public static final String FORMATTED_NUMBER = "formatted_number";
        public static final String DEVICE_NAME = "device_name";
        public static final String SERVER_NAME = "server_name";
        public static final String STATUS = "status";
        public static final String REGISTERED = "registered";
        public static final String STRANGER = "stranger";
        public static final String AVATAR = "avatar";
        public static final String BLOCKED = "blocked";

        private Fields() {
        }
    }
}

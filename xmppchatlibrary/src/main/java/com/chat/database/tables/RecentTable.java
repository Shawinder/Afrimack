package com.chat.database.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.chat.database.DatabaseManager;
import com.chat.modals.MessageModal;
import com.chat.modals.RecentModal;
import com.chat.xmpp.XmppConstant;
import com.chat.xmpp.XmppLogger;

import java.util.ArrayList;


/**
 * Created by ubuntu on 1/12/16.
 */

public class RecentTable extends AbstractTable {

    public static final int TYPE_SINGLE_CHAT = 1;
    public static final int TYPE_MULTI_CHAT = 2;
    public static final int TYPE_VIDEO_CALL = 3;
    public static final int TYPE_AUDIO_CALL = 4;
    public static final String TAG = "RecentTable";
    private static final String NAME = "recent";
    private static final String[] PROJECTION = new String[]{Fields.ID,
            Fields.TO_JID, Fields.MESSAGE_CALL_TABLE_ID, Fields.TYPE,
            Fields.SUBJECT,
            Fields.CREATED_TIME, Fields.LAST_READ_MESSAGE_CALL_TABLE_ID,
            Fields.LAST_READ_RECEIPT_SENT_MESSAGE_CALL_TABLE_ID,
            Fields.UNSEEN_MESSAGE_COUNT, Fields.UNSEEN_MISCALL_COUNT,
            Fields.UNSEEN_ROW_COUNT};
    private final static RecentTable instance;

    static {
        instance = new RecentTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }

    private final DatabaseManager databaseManager;

    private RecentTable(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public static RecentTable getInstance() {
        return instance;
    }

    static long getID(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.ID));
    }

    static String getToJid(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.TO_JID));
    }

    static long getMessageCallTableId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.MESSAGE_CALL_TABLE_ID));
    }

    static int getTYPE(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.TYPE));
    }

    static String getSUBJECT(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.SUBJECT));
    }

    static long getCreatedTime(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.CREATED_TIME));
    }

    static long getLastReadMessageCallTableId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.LAST_READ_MESSAGE_CALL_TABLE_ID));
    }

    static long getLastReadReceiptSentMessageCallTableId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.LAST_READ_RECEIPT_SENT_MESSAGE_CALL_TABLE_ID));
    }

    static long getUnseenMessageCount(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.UNSEEN_MESSAGE_COUNT));
    }

    static long getUnseenMiscallCount(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.UNSEEN_MISCALL_COUNT));
    }

    static long getUnseenRowCount(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.UNSEEN_ROW_COUNT));
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void create(SQLiteDatabase db) {
        DatabaseManager.execSQL(db, generateCreateSql());
    }

    private String generateCreateSql() {
        return "CREATE TABLE " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.TO_JID + " TEXT UNIQUE,"
                + Fields.MESSAGE_CALL_TABLE_ID + " INTEGER,"
                + Fields.TYPE + " INTEGER,"
                + Fields.SUBJECT + " TEXT,"
                + Fields.CREATED_TIME + " INTEGER,"
                + Fields.LAST_READ_MESSAGE_CALL_TABLE_ID + " INTEGER,"
                + Fields.LAST_READ_RECEIPT_SENT_MESSAGE_CALL_TABLE_ID + " INTEGER,"
                + Fields.UNSEEN_MESSAGE_COUNT + " INTEGER,"
                + Fields.UNSEEN_MISCALL_COUNT + " INTEGER,"
                + Fields.UNSEEN_ROW_COUNT + " INTEGER);";
    }

    public boolean checkRecentExist(String jid, int type) {
        String query = "SELECT * FROM "
                + NAME + " WHERE " + Fields.TO_JID + "='" + jid + "' AND "
                + Fields.TYPE + "=" + type;

        Cursor cursor = super.listFromQuery(query);
        if (cursor != null) {
            return cursor.getCount() > 0;
        }
        return false;
    }

    public void resetUnseenMessageCount(String toJid, int type) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        String resetCount = "UPDATE " + NAME + " SET " + Fields.UNSEEN_MESSAGE_COUNT + "="
                + 0 + " WHERE " + Fields.TO_JID + "='" + toJid + "' AND "
                + Fields.TYPE + "=" + type;
        db.execSQL(resetCount);
    }

    public void increaseUnseenMessageCountbyOne(SQLiteDatabase db, String toJid) {
        String increaseCount = "UPDATE " + NAME + " SET " + Fields.UNSEEN_MESSAGE_COUNT + "="
                + Fields.UNSEEN_MESSAGE_COUNT + "+" + 1 + " WHERE " + Fields.TO_JID + "='" + toJid + "' AND "
                + Fields.TYPE + "=" + TYPE_SINGLE_CHAT;
        db.execSQL(increaseCount);
    }

    public RecentModal writeMessage(MessageModal messageModal) {
        String toJid = messageModal.getTo_jid();
        int type = TYPE_SINGLE_CHAT;
        if (messageModal.getTo_jid().contains(XmppConstant.group_domain)) {
            type = TYPE_MULTI_CHAT;
        }
        RecentModal recentModal = new RecentModal();
        recentModal.setTo_jid(toJid);
        recentModal.setType(type);
        recentModal.setMessage_call_table_id(messageModal.get_id());
        recentModal.setMessage_id(messageModal.getMessage_id());
        recentModal.setCreated(messageModal.getTimestamp());
        recentModal.setData(messageModal.getData());
        recentModal.setFromMe(messageModal.getFrom_me());
        recentModal.setMessageType(messageModal.getMessage_type());
        recentModal.setMsgStatus(messageModal.getStatus());


        recentModal.setMedia_type(messageModal.getMedia_type());
        recentModal.setMedia_url(messageModal.getMedia_url());
        recentModal.setMedia_caption(messageModal.getMedia_caption());
        recentModal.setTo_resource(messageModal.getTo_resource());

        ContentValues values = new ContentValues();
        SQLiteDatabase db = databaseManager.getWritableDatabase();

        if (checkRecentExist(toJid, type)) {
            values.put(Fields.MESSAGE_CALL_TABLE_ID, recentModal.getMessage_call_table_id());
            values.put(Fields.CREATED_TIME, recentModal.getCreated());
            db.update(NAME, values,
                    Fields.TO_JID + " = ? AND " + Fields.TYPE + " = ?",
                    new String[]{String.valueOf(toJid), String.valueOf(type)});
            if (messageModal.getFrom_me() == 0) {
                increaseUnseenMessageCountbyOne(db, toJid);
            }
        } else {
            if (messageModal.getFrom_me() == 0 && messageModal.getMsgUnReadCount() == 0) {
                values.put(Fields.UNSEEN_MESSAGE_COUNT, 1);
            }
            values.put(Fields.TO_JID, recentModal.getTo_jid());
            values.put(Fields.TYPE, recentModal.getType());
            values.put(Fields.MESSAGE_CALL_TABLE_ID, recentModal.getMessage_call_table_id());
            values.put(Fields.CREATED_TIME, recentModal.getCreated());

            if (messageModal.getMsgUnReadCount() != 0) {
                values.put(Fields.UNSEEN_MESSAGE_COUNT, messageModal.getMsgUnReadCount());
            }

            long row = db.insert(NAME, null, values);
            recentModal.set_id(row);
        }


        return recentModal;

    }

    public RecentModal writeRecentMessage(MessageModal messageModal) {
        String toJid = messageModal.getTo_jid();
        int type = TYPE_SINGLE_CHAT;
        if (messageModal.getTo_jid().contains(XmppConstant.group_domain)) {
            type = TYPE_MULTI_CHAT;
        }
        RecentModal recentModal = new RecentModal();
        recentModal.setTo_jid(toJid);
        recentModal.setType(type);
        recentModal.setMessage_call_table_id(messageModal.get_id());
        recentModal.setMessage_id(messageModal.getMessage_id());
        recentModal.setCreated(messageModal.getTimestamp());
        recentModal.setData(messageModal.getData());
        recentModal.setFromMe(messageModal.getFrom_me());
        recentModal.setMessageType(messageModal.getMessage_type());
        recentModal.setMsgStatus(messageModal.getStatus());


        recentModal.setMedia_type(messageModal.getMedia_type());
        recentModal.setMedia_url(messageModal.getMedia_url());
        recentModal.setMedia_caption(messageModal.getMedia_caption());
        recentModal.setTo_resource(messageModal.getTo_resource());

        ContentValues values = new ContentValues();
        SQLiteDatabase db = databaseManager.getWritableDatabase();

        if (checkRecentExist(toJid, type)) {
            values.put(Fields.UNSEEN_MESSAGE_COUNT, messageModal.getMsgUnReadCount());
            values.put(Fields.MESSAGE_CALL_TABLE_ID, recentModal.getMessage_call_table_id());
            values.put(Fields.CREATED_TIME, recentModal.getCreated());
            XmppLogger.printLog("Count in DB =" + values.get(Fields.UNSEEN_MESSAGE_COUNT));
            db.update(NAME, values,
                    Fields.TO_JID + " = ? AND " + Fields.TYPE + " = ?",
                    new String[]{String.valueOf(toJid), String.valueOf(type)});

        } else {
            values.put(Fields.UNSEEN_MESSAGE_COUNT, messageModal.getMsgUnReadCount());
            values.put(Fields.TO_JID, recentModal.getTo_jid());
            values.put(Fields.TYPE, recentModal.getType());
            values.put(Fields.MESSAGE_CALL_TABLE_ID, recentModal.getMessage_call_table_id());
            values.put(Fields.CREATED_TIME, recentModal.getCreated());
            XmppLogger.printLog("Count in DB =" + values.get(Fields.UNSEEN_MESSAGE_COUNT));

            long row = db.insert(NAME, null, values);
            recentModal.set_id(row);
        }


        return recentModal;

    }


    public ArrayList<RecentModal> getRecentMessages() {
        MessageAndCallTable messageAndCallTable = MessageAndCallTable.getInstance();

        String RT = "r";
        String MCT = "mc";

        String query = "SELECT " + RT + ".*,"
                + MCT + "." + MessageAndCallTable.Fields.DATA + ","
                + MCT + "." + MessageAndCallTable.Fields.STATUS + ","
                + MCT + "." + MessageAndCallTable.Fields.MESSAGE_ID + ","
                + MCT + "." + MessageAndCallTable.Fields.MEDIA_URL + ","
                + MCT + "." + MessageAndCallTable.Fields.MEDIA_TYPE + ","
                + MCT + "." + MessageAndCallTable.Fields.MEDIA_CAPTION + ","
                + MCT + "." + MessageAndCallTable.Fields.TO_RESOURCE + ","
                + MCT + "." + MessageAndCallTable.Fields.MESSAGE_TYPE + ","
                + MCT + "." + MessageAndCallTable.Fields.FROM_ME
                + " FROM "
                + getTableName() + " as " + RT + ", "
                + messageAndCallTable.getTableName() + " as " + MCT
                + " WHERE "
                + RT + "." + Fields.MESSAGE_CALL_TABLE_ID + "=" + MCT + "." + MessageAndCallTable.Fields.ID
                + " AND ("
                + RT + "." + Fields.TYPE + "=" + TYPE_SINGLE_CHAT + " OR "
                + RT + "." + Fields.TYPE + "=" + TYPE_MULTI_CHAT + " )"
                + " ORDER BY "
                + RT + "." + Fields.MESSAGE_CALL_TABLE_ID + " DESC";

        Cursor cursor = super.listFromQuery(query);
        ArrayList<RecentModal> list = new ArrayList<>();
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    RecentModal recentModal = new RecentModal();
                    recentModal.set_id(getID(cursor));
                    recentModal.setTo_jid(getToJid(cursor));
                    recentModal.setMessage_call_table_id(getMessageCallTableId(cursor));
                    recentModal.setType(getTYPE(cursor));
                    recentModal.setCreated(getCreatedTime(cursor));

                    recentModal.setData(MessageAndCallTable.getDATA(cursor));
                    recentModal.setFromMe(MessageAndCallTable.getFromMe(cursor));
                    recentModal.setMessageType(MessageAndCallTable.getMessageType(cursor));
                    recentModal.setMsgStatus(MessageAndCallTable.getStatus(cursor));
                    recentModal.setMessage_id(MessageAndCallTable.getMessageId(cursor));
                    recentModal.setMedia_url(MessageAndCallTable.getMediaUrl(cursor));
                    recentModal.setMedia_type(MessageAndCallTable.getMediaType(cursor));
                    recentModal.setMedia_caption(MessageAndCallTable.getMediaCaption(cursor));
                    recentModal.setTo_resource(MessageAndCallTable.getToResource(cursor));

                    recentModal.setUnseen_message_count(getUnseenMessageCount(cursor));
                    list.add(recentModal);
                }

            }
            cursor.close();

        }
        return list;

    }

    public String getAllUnReadMessage() {

        String query = "SELECT " + Fields.TO_JID + ", " + Fields.UNSEEN_MESSAGE_COUNT + " FROM " + NAME + " WHERE " + Fields.UNSEEN_MESSAGE_COUNT + " > 0";

        String queryMsg = "SELECT count(" + Fields.TO_JID + ") as users, total( " + Fields.UNSEEN_MESSAGE_COUNT + ") as unseenmsg FROM " + NAME + " WHERE unseenmsg > 0 GROUP BY " + Fields.TO_JID;

        SQLiteDatabase db = DatabaseManager.getInstance().getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        int cursorCount = cursor.getCount();
        long countUnReadMessage = 0;
        if (cursor != null) {
            if (cursorCount > 0) {
                while (cursor.moveToNext()) {
                    countUnReadMessage += getUnseenMessageCount(cursor);
                }
            }
        }
        if (cursorCount > 1) {
            return countUnReadMessage + " messages from " + cursorCount + " chat";
        }
        cursor.close();
        return null;
    }

    private static final class Fields implements BaseColumns {
        static final String ID = Fields._ID;
        static final String TO_JID = "to_jid";
        static final String MESSAGE_CALL_TABLE_ID = "message_call_table_id";
        static final String TYPE = "type";
        static final String SUBJECT = "subject";
        static final String CREATED_TIME = "created";
        static final String LAST_READ_MESSAGE_CALL_TABLE_ID = "last_read_message_call_table_id";
        static final String LAST_READ_RECEIPT_SENT_MESSAGE_CALL_TABLE_ID = "last_read_receipt_sent_message_call_table_id";
        static final String UNSEEN_MESSAGE_COUNT = "unseen_message_count";
        static final String UNSEEN_MISCALL_COUNT = "unseen_misscall_count";
        static final String UNSEEN_ROW_COUNT = "unseen_row_count";

        private Fields() {
        }

    }
}

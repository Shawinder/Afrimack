package com.chat.database.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.chat.database.DatabaseManager;
import com.chat.xmpp.XmppLogger;

/**
 * Created by ubuntu on 23/1/17.
 */

public class GroupMessageReceipts extends AbstractTable {

    public static final String TAG = "GroupMessageReceipts";
    private static final String NAME = "group_message_receipts";
    private static final String[] PROJECTION = new String[]{Fields.ROOM_JID,
            Fields.MEM_JID, Fields.MESSAGE_ID, Fields.DELIVER_TIMESTAMP,
            Fields.DISPLAY_TIMESTAMP};
    private final static GroupMessageReceipts instance;

    static {
        instance = new GroupMessageReceipts(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }

    private final DatabaseManager databaseManager;

    private GroupMessageReceipts(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public static GroupMessageReceipts getInstance() {
        return instance;
    }


    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void create(SQLiteDatabase db) {
        DatabaseManager.execSQL(db, generateCreateSql());
    }

    private String generateCreateSql() {
        return "CREATE TABLE " + NAME + " ("
                + Fields.ROOM_JID + " TEXT,"
                + Fields.MEM_JID + " TEXT,"
                + Fields.MESSAGE_ID + " TEXT,"
                + Fields.DELIVER_TIMESTAMP + " INTEGER,"
                + Fields.DISPLAY_TIMESTAMP + " INTEGER"
                + ");";
    }


    public static final class Fields implements BaseColumns {
        public static final String ROOM_JID = "room_jid";
        public static final String MEM_JID = "mem_jid";
        public static final String MESSAGE_ID = "message_id";
        public static final String DELIVER_TIMESTAMP = "deliver_timestamp";
        public static final String DISPLAY_TIMESTAMP = "display_timestamp";

        private Fields() {
        }
    }
}

package com.chat.database.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.chat.database.DatabaseManager;
import com.chat.modals.MessageModal;
import com.chat.xmpp.XmppLogger;
import com.chat.xmpp.XmppUtils;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 1/12/16.
 */

public class MessageAndCallTable extends AbstractTable {

    public static final int TYPE_MESSAGE_GROUP_LOCAL_CREATE = -1;
    public static final int TYPE_MESSAGE_NORMAL = 0;
    public static final int TYPE_MESSAGE_GROUP_CREATE = 1;
    public static final int TYPE_MESSAGE_ADDED_IN_GROUP = 2;
    public static final int TYPE_MESSAGE_REMOVE_FROM_GROUP = 3;
    public static final String TAG = "MessageAndCallTable";
    private static final String NAME = "message_call";
    private static final String[] PROJECTION = new String[]{Fields.ID,
            Fields.TO_JID, Fields.FROM_ME, Fields.MESSAGE_TYPE,
            Fields.MESSAGE_ID, Fields.STATUS,
            Fields.DATA, Fields.MEDIA_URL, Fields.MEDIA_HASH,
            Fields.MEDIA_MIME_TYPE,
            Fields.MEDIA_TYPE, Fields.MEDIA_SIZE, Fields.MEDIA_NAME,
            Fields.MEDIA_CAPTION, Fields.MEDIA_DURATION, Fields.MEDIA_ORIGIN,
            Fields.LATITUDE, Fields.LONGITUDE, Fields.THUMB_IMAGE,
            Fields.TO_RESOURCE, Fields.TIMESTAMP, Fields.TIMESTAMP_RECEIVED,
            Fields.TIMESTAMP_SEND, Fields.TIMESTAMP_RECEIPT_SERVER,
            Fields.TIMESTAMP_RECEIPT_DEVICE, Fields.TIMESTAMP_READ_DEVICE,
            Fields.RAW_DATA, Fields.RECIPIENT_COUNT, Fields.STARRED,
            Fields.MENTIONED_JIDS};
    private final static MessageAndCallTable instance;

    static {
        instance = new MessageAndCallTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }

    private final DatabaseManager databaseManager;

    private MessageAndCallTable(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public static MessageAndCallTable getInstance() {
        return instance;
    }

    public static long getID(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.ID));
    }

    public static String getToJid(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.TO_JID));
    }

    public static int getFromMe(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.FROM_ME));
    }

    public static int getMessageType(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.MESSAGE_TYPE));
    }

    public static int getStatus(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.STATUS));
    }

    public static String getMessageId(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MESSAGE_ID));
    }

    public static String getDATA(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.DATA));
    }

    public static String getMediaUrl(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEDIA_URL));
    }

    public static String getMediaVideoCmd(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEDIA_VIDEO_CMD));
    }

    public static String getMediaHash(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEDIA_HASH));
    }

    public static String getMediaMimeType(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEDIA_MIME_TYPE));
    }

    public static String getMediaType(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEDIA_TYPE));
    }

    public static long getMediaSize(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.MEDIA_SIZE));
    }

    public static String getMediaName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEDIA_NAME));
    }

    public static String getMediaCaption(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEDIA_CAPTION));
    }

    public static long getMediaDuration(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.MEDIA_DURATION));
    }

    public static int getMediaOrigin(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.MEDIA_ORIGIN));
    }

    public static double getLATITUDE(Cursor cursor) {
        return cursor.getDouble(cursor.getColumnIndex(Fields.LATITUDE));
    }

    public static double getLONGITUDE(Cursor cursor) {
        return cursor.getDouble(cursor.getColumnIndex(Fields.LONGITUDE));
    }

    public static String getThumbImage(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.THUMB_IMAGE));
    }

    public static String getToResource(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.TO_RESOURCE));
    }

    public static long getTIMESTAMP(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.TIMESTAMP));
    }

    public static long getTimestampReceived(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.TIMESTAMP_RECEIVED));
    }

    public static long getTimestampSend(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.TIMESTAMP_SEND));
    }

    public static long getTimestampReceiptServer(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.TIMESTAMP_RECEIPT_SERVER));
    }

    public static long getTimestampReceiptDevice(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.TIMESTAMP_RECEIPT_DEVICE));
    }

    public static long getTimestampReadDevice(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(Fields.TIMESTAMP_READ_DEVICE));
    }

    public static byte[] getRawData(Cursor cursor) {
        return cursor.getBlob(cursor.getColumnIndex(Fields.RAW_DATA));
    }

    public static int getRecipientCount(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.RECIPIENT_COUNT));
    }

    public static int getSTARRED(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.RECIPIENT_COUNT));
    }

    public static String getMentionedJids(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MENTIONED_JIDS));
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void create(SQLiteDatabase db) {
        DatabaseManager.execSQL(db, generateCreateSql());
    }

    private String generateCreateSql() {
        return "CREATE TABLE " + NAME + " ("
                + Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Fields.TO_JID + " TEXT NOT NULL,"
                + Fields.FROM_ME + " INTEGER,"
                + Fields.MESSAGE_TYPE + " INTEGER DEFAULT " + TYPE_MESSAGE_NORMAL + ","
                + Fields.MESSAGE_ID + " TEXT NOT NULL,"
                + Fields.STATUS + " INTEGER,"
                + Fields.DATA + " TEXT,"
                + Fields.MEDIA_URL + " TEXT,"
                + Fields.MEDIA_VIDEO_CMD + " TEXT,"
                + Fields.MEDIA_HASH + " TEXT,"
                + Fields.MEDIA_MIME_TYPE + " TEXT,"
                + Fields.MEDIA_TYPE + " TEXT,"
                + Fields.MEDIA_SIZE + " INTEGER,"
                + Fields.MEDIA_NAME + " TEXT,"
                + Fields.MEDIA_CAPTION + " TEXT,"
                + Fields.MEDIA_DURATION + " INTEGER,"
                + Fields.MEDIA_ORIGIN + " INTEGER,"
                + Fields.LATITUDE + " REAL,"
                + Fields.LONGITUDE + " REAL,"
                + Fields.THUMB_IMAGE + " TEXT,"
                + Fields.TO_RESOURCE + " TEXT,"
                + Fields.TIMESTAMP + " INTEGER,"
                + Fields.TIMESTAMP_RECEIVED + " INTEGER,"
                + Fields.TIMESTAMP_SEND + " INTEGER,"
                + Fields.TIMESTAMP_RECEIPT_SERVER + " INTEGER,"
                + Fields.TIMESTAMP_RECEIPT_DEVICE + " INTEGER,"
                + Fields.TIMESTAMP_READ_DEVICE + " INTEGER,"
                + Fields.RECIPIENT_COUNT + " INTEGER,"
                + Fields.RAW_DATA + " BLOB,"
                + Fields.STARRED + " INTEGER,"
                + Fields.MENTIONED_JIDS + " TEXT);";
    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        XmppLogger.printLog(TAG + " " + getTableName() + " migrate toVersion=" + toVersion);
        String sql;
        switch (toVersion) {
            case 7:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.MEDIA_HASH + " TEXT;";
                DatabaseManager.execSQL(db, sql);
                break;
            case 8:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.MEDIA_VIDEO_CMD + " TEXT;";
                DatabaseManager.execSQL(db, sql);
                break;
            case 11:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN "
                        + Fields.MESSAGE_TYPE + " INTEGER DEFAULT " + TYPE_MESSAGE_NORMAL + ";";
                DatabaseManager.execSQL(db, sql);
                sql = "UPDATE " + NAME + " SET "
                        + Fields.MESSAGE_TYPE + " = " + TYPE_MESSAGE_NORMAL + ";";
                DatabaseManager.execSQL(db, sql);
                break;
        }

    }

    public long writeMessage(MessageModal messageModal, boolean isMedia) {
        ContentValues values = new ContentValues();
        values.put(Fields.TO_JID, messageModal.getTo_jid());
        values.put(Fields.FROM_ME, messageModal.getFrom_me());
        values.put(Fields.MESSAGE_TYPE, messageModal.getMessage_type());
        values.put(Fields.MESSAGE_ID, messageModal.getMessage_id());
        values.put(Fields.STATUS, messageModal.getStatus());
        values.put(Fields.DATA, messageModal.getData());
        values.put(Fields.TIMESTAMP, messageModal.getTimestamp());
        if (messageModal.getFrom_me() == 1) {
            values.put(Fields.TIMESTAMP_SEND, messageModal.getTimestamp_send());
        } else if (messageModal.getFrom_me() == 2) {
            values.put(Fields.TIMESTAMP_SEND, messageModal.getTimestamp_send());
        } else {
            values.put(Fields.TIMESTAMP_RECEIVED, messageModal.getTimestamp_send());
        }

        if (XmppUtils.checkVaildString(messageModal.getTo_resource())) {
            values.put(Fields.TO_RESOURCE, messageModal.getTo_resource());
        } else {
            values.putNull(Fields.TO_RESOURCE);
        }
        values.put(Fields.RECIPIENT_COUNT, messageModal.getRecipient_count());
        if (isMedia) {
            if (XmppUtils.checkVaildString(messageModal.getMedia_url())) {
                values.put(Fields.MEDIA_URL, messageModal.getMedia_url());
            } else {
                values.putNull(Fields.MEDIA_URL);
            }
            if (messageModal.getVideoCmddata() != null && messageModal.getVideoCmddata().length > 0) {
                String[] cmd = messageModal.getVideoCmddata();
                String jsoncmd = new Gson().toJson(cmd);
                values.put(Fields.MEDIA_VIDEO_CMD, jsoncmd);
            } else {
                values.putNull(Fields.MEDIA_VIDEO_CMD);
            }
            if (XmppUtils.checkVaildString(messageModal.getMedia_hash())) {
                values.put(Fields.MEDIA_HASH, messageModal.getMedia_hash());
            } else {
                values.putNull(Fields.MEDIA_HASH);
            }
            if (XmppUtils.checkVaildString(messageModal.getMedia_mime_type())) {
                values.put(Fields.MEDIA_MIME_TYPE, messageModal.getMedia_mime_type());
            } else {
                values.putNull(Fields.MEDIA_MIME_TYPE);
            }
            if (XmppUtils.checkVaildString(messageModal.getMedia_type())) {
                values.put(Fields.MEDIA_TYPE, messageModal.getMedia_type());
            } else {
                values.putNull(Fields.MEDIA_TYPE);
            }
            if (XmppUtils.checkVaildString(messageModal.getMedia_name())) {
                values.put(Fields.MEDIA_NAME, messageModal.getMedia_name());
            } else {
                values.putNull(Fields.MEDIA_NAME);
            }
            if (XmppUtils.checkVaildString(messageModal.getMedia_caption())) {
                values.put(Fields.MEDIA_CAPTION, messageModal.getMedia_caption());
            } else {
                values.putNull(Fields.MEDIA_CAPTION);
            }
            if (XmppUtils.checkVaildString(messageModal.getThumb_image())) {
                values.put(Fields.THUMB_IMAGE, messageModal.getThumb_image());
            } else {
                values.putNull(Fields.THUMB_IMAGE);
            }

            values.put(Fields.MEDIA_SIZE, messageModal.getMedia_size());
            values.put(Fields.MEDIA_DURATION, messageModal.getMedia_duration());
            values.put(Fields.MEDIA_ORIGIN, messageModal.getMedia_origin());
            values.put(Fields.LATITUDE, messageModal.getLatitude());
            values.put(Fields.LONGITUDE, messageModal.getLongitude());
        }
        XmppLogger.printLog("writeMessage = " + messageModal.getMessage_id() + " message body " + messageModal.getData());
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        return db.insert(NAME, null, values);
    }

    public int updateMediaMessageUrlAndStatus(MessageModal messageModal) {
        String toJid = messageModal.getTo_jid();
        String messageId = messageModal.getMessage_id();

        ContentValues values = new ContentValues();
        values.put(Fields.STATUS, messageModal.getStatus());
        if (XmppUtils.checkVaildString(messageModal.getMedia_url())) {
            values.put(Fields.MEDIA_URL, messageModal.getMedia_url());
        } else {
            values.putNull(Fields.MEDIA_URL);
        }

        SQLiteDatabase db = databaseManager.getWritableDatabase();

        return db.update(NAME, values,
                Fields.TO_JID + " = ? AND " + Fields.MESSAGE_ID + " = ?",
                new String[]{toJid, messageId});
    }

    public int updateMediaMessageNameAndStatus(MessageModal messageModal) {
        String toJid = messageModal.getTo_jid();
        String messageId = messageModal.getMessage_id();

        ContentValues values = new ContentValues();
        values.put(Fields.STATUS, messageModal.getStatus());
        if (XmppUtils.checkVaildString(messageModal.getMedia_name())) {
            values.put(Fields.MEDIA_NAME, messageModal.getMedia_name());
        } else {
            values.putNull(Fields.MEDIA_NAME);
        }


        SQLiteDatabase db = databaseManager.getWritableDatabase();

        return db.update(NAME, values,
                Fields.TO_JID + " = ? AND " + Fields.MESSAGE_ID + " = ?",
                new String[]{toJid, messageId});
    }

    public int updateMediaMessageNameAndStatusAfterCompress(MessageModal messageModal) {
        String toJid = messageModal.getTo_jid();
        String messageId = messageModal.getMessage_id();

        ContentValues values = new ContentValues();
        values.put(Fields.STATUS, messageModal.getStatus());
        if (XmppUtils.checkVaildString(messageModal.getMedia_name())) {
            values.put(Fields.MEDIA_NAME, messageModal.getMedia_name());
        } else {
            values.putNull(Fields.MEDIA_NAME);
        }
        if (messageModal.getVideoCmddata() != null && messageModal.getVideoCmddata().length > 0) {
            values.put(Fields.MEDIA_VIDEO_CMD, new Gson().toJson(messageModal.getVideoCmddata()));
        } else {
            values.putNull(Fields.MEDIA_VIDEO_CMD);
        }
        if (XmppUtils.checkVaildString(messageModal.getMedia_hash())) {
            values.put(Fields.MEDIA_HASH, messageModal.getMedia_hash());
        } else {
            values.putNull(Fields.MEDIA_HASH);
        }
        if (messageModal.getMedia_size() > 0) {
            values.put(Fields.MEDIA_SIZE, messageModal.getMedia_size());
        } else {
            values.put(Fields.MEDIA_SIZE, 0);
        }


        SQLiteDatabase db = databaseManager.getWritableDatabase();

        return db.update(NAME, values,
                Fields.TO_JID + " = ? AND " + Fields.MESSAGE_ID + " = ?",
                new String[]{toJid, messageId});
    }

    public MessageModal getMessageDetail(String messageId) {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.MESSAGE_ID + "='" + messageId + "'";

        Cursor cursor = super.listFromQuery(query);
        List<MessageModal> list = parseCursorResult(cursor);
        if (list.size() == 0) {
            return null;
        }
        return list.get(0);
    }


    /*Get Message existes according to user*/
    public boolean isMessageExists(String messageId, String jid) {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.MESSAGE_ID + "='" + messageId + "'"
                + " AND " + Fields.TO_JID + "='" + jid + "'";

        Cursor cursor = super.listFromQuery(query);
        List<MessageModal> list = parseCursorResult(cursor);
        if (list.size() == 0) {
            XmppLogger.printLog("isMessageExists = " + messageId + " Status " + false);
            return false;
        }
        XmppLogger.printLog("isMessageExists = " + messageId + " Status " + true);
        return true;
    }

    public boolean clearChatMessage(String jid) {
        if (jid == null) {
            return false;
        }
        String queryDelete = "DELETE FROM " + NAME + " WHERE " + Fields.TO_JID + "='" + jid + "'";
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        db.execSQL(queryDelete);

        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.TO_JID + "='" + jid + "'";

        Cursor cursor = super.listFromQuery(query);
        List<MessageModal> list = parseCursorResult(cursor);
        if (list.size() == 0) {
            return true;
        }
        return false;
    }

    public void updateMessageSent(MessageModal messageModal) {
        String updateSent = "UPDATE " + NAME + " SET "
                + Fields.TIMESTAMP_SEND + "=" + messageModal.getTimestamp_send() + ","
                + Fields.STATUS + "= CASE WHEN "
                + "("
                + Fields.STATUS + "=" + MessageModal.STATUS_NOT_SENT
                + " OR "
                + Fields.STATUS + "=" + MessageModal.STATUS_MEDIA_END
                + ")"
                + " THEN " + MessageModal.STATUS_SENT
                + " ELSE " + Fields.STATUS
                + " END WHERE " + Fields.MESSAGE_ID + "='" + messageModal.getMessage_id() + "'";

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        db.execSQL(updateSent);
    }

    public void updateMessageDelivered(MessageModal messageModal) {

        String updateDelivered = "UPDATE " + NAME + " SET "
                + Fields.TIMESTAMP_RECEIPT_DEVICE + "=" + messageModal.getTimestamp_receipt_device() + ","
                + Fields.STATUS
                + "= CASE WHEN " + Fields.STATUS + "<=" + MessageModal.STATUS_SENT
                + " THEN " + MessageModal.STATUS_DELIVERED
                + " ELSE " + Fields.STATUS
                + " END WHERE " + Fields.MESSAGE_ID + "='" + messageModal.getMessage_id() + "'";

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        db.execSQL(updateDelivered);
    }

    public void updateMessageDisplayed(MessageModal messageModal) {
        String updateDelivered = "UPDATE " + NAME + " SET "
                + Fields.TIMESTAMP_READ_DEVICE + "=" + messageModal.getTimestamp_read_device() + ","
                + Fields.STATUS
                + "= CASE WHEN " + Fields.STATUS + "<=" + MessageModal.STATUS_DELIVERED
                + " THEN " + MessageModal.STATUS_DISPLAYED
                + " ELSE " + Fields.STATUS
                + " END WHERE " + Fields.MESSAGE_ID + "='" + messageModal.getMessage_id() + "'";

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        db.execSQL(updateDelivered);
    }

    public void updateSentMessageDelivered(MessageModal messageModal) {
        String updateDelivered = "UPDATE " + NAME + " SET " + Fields.STATUS
                + "= CASE WHEN " + Fields.STATUS + "=" + MessageModal.STATUS_RECEIVED
                + " THEN " + MessageModal.STATUS_DELIVERY_RECEIPT_SENT
                + " ELSE " + Fields.STATUS
                + " END WHERE " + Fields.MESSAGE_ID + "='" + messageModal.getMessage_id() + "'";

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        db.execSQL(updateDelivered);
    }

    public void updateSentMessageDisplayed(MessageModal messageModal) {
        String updateDelivered = "UPDATE " + NAME + " SET " + Fields.STATUS
                + "= CASE WHEN " + Fields.STATUS + "<=" + MessageModal.STATUS_DELIVERY_RECEIPT_SENT
                + " THEN " + MessageModal.STATUS_DISPLAY_RECEIPT_SENT
                + " ELSE " + Fields.STATUS
                + " END WHERE " + Fields.MESSAGE_ID + "='" + messageModal.getMessage_id() + "'";

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        db.execSQL(updateDelivered);
    }

    public ArrayList<MessageModal> getUserMessages(String toJid) {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.TO_JID + "='" + toJid + "'"
                + " AND " + Fields.MESSAGE_TYPE + ">=0";

        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult(cursor);
    }

    public ArrayList<MessageModal> getAllImageMessages() {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.MEDIA_NAME + " not null";

        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult(cursor);
    }

    public ArrayList<MessageModal> getUserImageMessages(String toJid) {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.TO_JID + "='" + toJid + "'"
                + " AND " + Fields.MEDIA_URL + " not null";

        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult(cursor);
    }


    private ArrayList<MessageModal> parseCursorResult(Cursor cursor) {
        ArrayList<MessageModal> list = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    MessageModal messageModal = new MessageModal();
                    messageModal.set_id(getID(cursor));
                    messageModal.setMessage_id(getMessageId(cursor));
                    messageModal.setTo_jid(getToJid(cursor));
                    messageModal.setStatus(getStatus(cursor));
                    messageModal.setFrom_me(getFromMe(cursor));
                    messageModal.setMessage_type(getMessageType(cursor));
                    messageModal.setData(getDATA(cursor));
                    messageModal.setTimestamp(getTIMESTAMP(cursor));
                    messageModal.setTimestamp_send(getTimestampSend(cursor));
                    messageModal.setTimestamp_received(getTimestampReceived(cursor));
                    messageModal.setStatus(getStatus(cursor));
                    messageModal.setTo_resource(getToResource(cursor));

                    messageModal.setMedia_url(getMediaUrl(cursor));
                    String videoCmd = getMediaVideoCmd(cursor);
                    if (XmppUtils.checkVaildString(videoCmd)) {
                        String[] cmd = new Gson().fromJson(videoCmd, String[].class);
                        messageModal.setVideoCmddata(cmd);
                    }
                    messageModal.setMedia_hash(getMediaHash(cursor));
                    messageModal.setMedia_mime_type(getMediaMimeType(cursor));
                    messageModal.setMedia_type(getMediaType(cursor));
                    messageModal.setMedia_name(getMediaName(cursor));
                    messageModal.setMedia_caption(getMediaCaption(cursor));
                    messageModal.setThumb_image(getThumbImage(cursor));
                    messageModal.setMedia_size(getMediaSize(cursor));
                    messageModal.setMedia_duration(getMediaDuration(cursor));
                    messageModal.setMedia_origin(getMediaOrigin(cursor));
                    messageModal.setLatitude(getLATITUDE(cursor));
                    messageModal.setLongitude(getLONGITUDE(cursor));

                    list.add(messageModal);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public synchronized MessageModal getMessageModalForHashUrlNotNull(String hash) {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.MEDIA_URL + " NOT NULL AND "
                + Fields.MEDIA_HASH + "='" + hash + "'";

        Cursor cursor = super.listFromQuery(query);
        List<MessageModal> list = parseCursorResult(cursor);
        if (list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    public synchronized List<MessageModal> getMessageModalForHashPathNotNull(String hash) {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.MEDIA_NAME + " NOT NULL AND "
                + Fields.MEDIA_HASH + "='" + hash + "'";

        Cursor cursor = super.listFromQuery(query);
        List<MessageModal> list = parseCursorResult(cursor);

        return list;
    }


    public ArrayList<MessageModal> getLastMessages(String toJid) {

        String query = "SELECT * FROM " + NAME + " WHERE " + Fields._ID + " = (SELECT MAX(" + Fields._ID + ") FROM " + NAME + " where " + Fields.TO_JID + " = '" + toJid + "' );";

        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult(cursor);
    }


    public static final class Fields implements BaseColumns {
        static final String ID = Fields._ID;
        static final String TO_JID = "to_jid";
        static final String FROM_ME = "from_me";
        static final String MESSAGE_TYPE = "message_type";
        static final String MESSAGE_ID = "message_id";
        static final String STATUS = "status";
        static final String DATA = "data";
        static final String MEDIA_URL = "media_url";
        static final String MEDIA_VIDEO_CMD = "media_video_cmd";
        static final String MEDIA_HASH = "media_hash";
        static final String MEDIA_MIME_TYPE = "media_mime_type";
        static final String MEDIA_TYPE = "media_type";
        static final String MEDIA_SIZE = "media_size";
        static final String MEDIA_NAME = "media_name";
        static final String MEDIA_CAPTION = "media_caption";
        static final String MEDIA_DURATION = "media_duration";
        static final String MEDIA_ORIGIN = "media_origin";
        static final String LATITUDE = "latitude";
        static final String LONGITUDE = "longitude";
        static final String THUMB_IMAGE = "thumb_image";
        static final String TO_RESOURCE = "to_resource";
        static final String TIMESTAMP = "timestamp";
        static final String TIMESTAMP_RECEIVED = "timestamp_received";
        static final String TIMESTAMP_SEND = "timestamp_send";
        static final String TIMESTAMP_RECEIPT_SERVER = "timestamp_receipt_server";
        static final String TIMESTAMP_RECEIPT_DEVICE = "timestamp_receipt_device";
        static final String TIMESTAMP_READ_DEVICE = "timestamp_read_device";
        static final String RAW_DATA = "raw_data";
        static final String RECIPIENT_COUNT = "recipient_count";
        static final String STARRED = "starred";
        static final String MENTIONED_JIDS = "mentioned_jids";

        private Fields() {
        }

    }

}

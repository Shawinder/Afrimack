package com.chat.database.tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.chat.contacts.GroupMemberModal;
import com.chat.database.DatabaseManager;
import com.chat.modals.MessageModal;
import com.chat.xmpp.XmppLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 17/1/17.
 */

public class GroupMembers extends AbstractTable {
    public static final int MEMBER_TYPE_OWNER = 1;
    public static final int MEMBER_TYPE_MEMBER = 2;

    public static final int MEMBER_INVITE_NOT_SENT = 1;
    public static final int MEMBER_INVITE_SENT = 2;
    public static final String TAG = "GroupMembers";
    private static final String NAME = "group_members";
    private static final String[] PROJECTION = new String[]{Fields.ROOM_JID,
            Fields.MEM_JID, Fields.MEM_TYPE, Fields.MEM_INVITE_SENT,
            Fields.MEM_INVITE_ID};
    private final static GroupMembers instance;

    static {
        instance = new GroupMembers(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }

    private final DatabaseManager databaseManager;

    private GroupMembers(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public static GroupMembers getInstance() {
        return instance;
    }

    public static String getRoomJid(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.ROOM_JID));
    }

    public static String getMemJid(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEM_JID));
    }

    public static String getMemInviteId(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.MEM_INVITE_ID));
    }

    public static int getMemtype(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.MEM_TYPE));
    }

    public static int getMemInviteSent(Cursor cursor) {
        return cursor.getInt(cursor.getColumnIndex(Fields.MEM_INVITE_SENT));
    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void create(SQLiteDatabase db) {
        DatabaseManager.execSQL(db, generateCreateSql());
    }

    private String generateCreateSql() {
        return "CREATE TABLE " + NAME + " ("
                + Fields.ROOM_JID + " TEXT,"
                + Fields.MEM_JID + " TEXT,"
                + Fields.MEM_TYPE + " INTEGER DEFAULT " + MEMBER_TYPE_MEMBER + ","
                + Fields.MEM_INVITE_SENT + " INTEGER DEFAULT " + MEMBER_INVITE_NOT_SENT + ","
                + Fields.MEM_INVITE_ID + " TEXT"
                + ");";
    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        XmppLogger.printLog(TAG + " " + getTableName() + " migrate toVersion=" + toVersion);
        String sql;
        switch (toVersion) {
            case 10:
                sql = "ALTER TABLE " + NAME + " ADD COLUMN " + Fields.MEM_INVITE_ID + " TEXT;";
                DatabaseManager.execSQL(db, sql);
                break;
        }
    }

    public long write(String roomJid, String mem_jid, int mem_type, int mem_invite) {
        boolean already = checkIfExist(roomJid, mem_jid);
        ContentValues values = new ContentValues();

        values.put(Fields.MEM_TYPE, mem_type);
        values.put(Fields.MEM_INVITE_SENT, mem_invite);

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        if (already) {
//            String where = Fields.ROOM_JID + " = ? AND " + Fields.MEM_JID + " = ?";
//            String[] whereArgs = new String[]{roomJid, mem_jid};
//            XmppLogger.printLog(TAG + " update roomJid=" + roomJid);
//            return db.update(NAME, values, where, whereArgs);
            return 0;
        } else {
            values.put(Fields.ROOM_JID, roomJid);
            values.put(Fields.MEM_JID, mem_jid);
            XmppLogger.printLog(TAG + " write roomJid=" + roomJid);
            return db.insert(NAME, null, values);
        }

    }

    public boolean checkIfExist(String roomJid, String memJid) {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.ROOM_JID + "='" + roomJid + "'"
                + " AND " + Fields.MEM_JID + "='" + memJid + "'";
        Cursor cursor = super.listFromQuery(query);
        boolean already = false;
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                already = true;
            }
            cursor.close();
        }
        return already;
    }

    public List<GroupMemberModal> getGroupMembers(String roomJid) {
        String query = "SELECT * FROM " + NAME
                + " WHERE " + Fields.ROOM_JID + "='" + roomJid + "'";

        Cursor cursor = super.listFromQuery(query);
        return parseCursorResult(cursor);
    }

    private ArrayList<GroupMemberModal> parseCursorResult(Cursor cursor) {
        ArrayList<GroupMemberModal> list = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    GroupMemberModal groupMemberModal = new GroupMemberModal();
                    groupMemberModal.setMem_jid(getMemJid(cursor));
                    groupMemberModal.setMem_type(getMemtype(cursor));
                    groupMemberModal.setMem_invite(getMemInviteSent(cursor));
                    groupMemberModal.setMem_invite_id(getMemInviteId(cursor));
                    list.add(groupMemberModal);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return list;
    }

    public void updateMemInviteId(String roomJid, String inviteId, List<String> members) {
        if (members.size() == 0) {
            return;
        }

        String updateQuery = "UPDATE " + NAME + " SET " + Fields.MEM_INVITE_ID + "='"
                + inviteId + "' WHERE " + Fields.ROOM_JID + "='" + roomJid + "'";
        String memQuery = "";
        for (int i = 0; i < members.size(); i++) {
            String memJid = members.get(i);
            memQuery += Fields.MEM_JID + "='" + memJid + "'";
            if (i != members.size() - 1) {
                memQuery += " OR ";
            }
        }
        updateQuery += " AND ( " + memQuery + " )";
        XmppLogger.printLog(TAG + " updateQuery=" + updateQuery);
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        db.execSQL(updateQuery);
    }

    public void updateMemInviteSent(MessageModal messageModal) {
        String updateQuery = "UPDATE " + NAME + " SET " + Fields.MEM_INVITE_SENT + "="
                + messageModal.getStatus() + " WHERE " + Fields.MEM_INVITE_ID + "='" + messageModal.getMessage_id() + "'";
        XmppLogger.printLog(TAG + " updateQuery=" + updateQuery);
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        db.execSQL(updateQuery);
    }

    public static final class Fields implements BaseColumns {
        public static final String ROOM_JID = "room_jid";
        public static final String MEM_JID = "mem_jid";
        public static final String MEM_TYPE = "mem_type";
        public static final String MEM_INVITE_SENT = "mem_invite";
        public static final String MEM_INVITE_ID = "mem_invite_id";

        private Fields() {
        }
    }
}

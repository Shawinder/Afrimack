package com.chat.prefences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.chat.modals.StatusModal;
import com.chat.xmpp.XmppConstant;
import com.chat.xmpp.XmppUtils;
import com.chat.xmpp.modals.XmppAccount;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class ChatPrefs {

    public static final String Prefsname = "chatPrefs";


    public static final String KEY_CHAT_USER = "xmppaccount";
    public static final String KEY_DEFAULT_STAUS = "savedstatus";


    public static void clearPrefsdata(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(Prefsname,
                Context.MODE_PRIVATE);
        prefs.edit().clear().commit();
    }


    public static void setChatAccountDetailInPrefs(Context ctx, XmppAccount xmppAccount) {
        if (!XmppUtils.checkVaildString(xmppAccount.getUsername()) || !XmppUtils.checkVaildString(xmppAccount.getPassword())) {
            throw new IllegalStateException("username or password required.");
        }
        if (!XmppUtils.checkVaildString(xmppAccount.getResource())) {
            xmppAccount.setResource(XmppConstant.defaultResource);
        }
        if (!XmppUtils.checkVaildString(xmppAccount.getCountryPhoneCode())) {
            xmppAccount.setCountryPhoneCode(XmppConstant.defaultCountryPhoneCode);
        }
        SharedPreferences prefs = ctx.getSharedPreferences(Prefsname,
                Context.MODE_PRIVATE);
        Editor editor = prefs.edit();
        editor.putString(KEY_CHAT_USER, new Gson().toJson(xmppAccount));
        editor.apply();
    }


    public static XmppAccount getSavedChatAccount(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(Prefsname,
                Context.MODE_PRIVATE);
        String detail = prefs.getString(KEY_CHAT_USER, "");

        if (!XmppUtils.checkVaildString(detail)) {
            return null;
        }
        try {
            return new Gson().fromJson(detail, XmppAccount.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveDefaultStatus(Context ctx, StatusModal statusModal) {
        SharedPreferences prefs = ctx.getSharedPreferences(Prefsname,
                Context.MODE_PRIVATE);
        Editor editor = prefs.edit();
        editor.putString(KEY_DEFAULT_STAUS, new Gson().toJson(statusModal));
        editor.commit();

    }

    public static StatusModal getDefaultStatus(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(Prefsname,
                Context.MODE_PRIVATE);
        String status = prefs.getString(KEY_DEFAULT_STAUS, "");
        if (!XmppUtils.checkVaildString(status)) {
            return null;
        }
        return new Gson().fromJson(status, StatusModal.class);

    }


}

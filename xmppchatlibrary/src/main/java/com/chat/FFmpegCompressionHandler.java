package com.chat;

import android.util.Log;

import com.chat.modals.MessageModal;
import com.chat.xmpp.listners.VideoCompressionListner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 7/1/17.
 */

public class FFmpegCompressionHandler {

    public static final String TAG = FFmpegCompressionHandler.class.getSimpleName();

    static FFmpegCompressionHandler instance;

    static {
        instance = new FFmpegCompressionHandler();
    }

    List<MessageModal> compressionModalList = new ArrayList<>();

    private FFmpegCompressionHandler() {

    }

    public static FFmpegCompressionHandler getInstance() {
        return instance;
    }

    private void printLog(String msg) {
        Log.e(TAG, msg);
    }

    public void addForCompression(MessageModal messageModal) {
        compressionModalList.add(messageModal);
        runNextCompression();
    }


    private void compressVideo(MessageModal messageModal) {
       /* FFmpeg ffmpeg = FFmpeg.getInstance(ChatApplication.getInstance());
        if (ffmpeg.isFFmpegCommandRunning()) {
            compressionModalList.add(messageModal);
        } else {
            final MessageModal taskModal = messageModal;

            try {

                String[] complexCmd = taskModal.getVideoCmddata();
                final String outputPath = complexCmd[complexCmd.length - 1];
                if (new File(outputPath).exists()) {
                    new File(outputPath).delete();
                }
                ffmpeg.execute(complexCmd, new ExecuteBinaryResponseHandler() {

                    @Override
                    public void onStart() {
                        printLog("ffmpeg.execute onStart");
                        triggerVideoCompression(1, taskModal);
                    }

                    @Override
                    public void onProgress(String message) {
                        printLog("ffmpeg.execute onProgress message=" + message);
                        triggerVideoCompression(2, taskModal);
                    }

                    @Override
                    public void onFailure(String message) {
                        printLog("ffmpeg.execute onFailure message=" + message);
                        File outFile = new File(outputPath);
                        if (outFile.exists()) {
                            outFile.delete();
                        }
                        triggerVideoCompression(4, taskModal);

                    }

                    @Override
                    public void onSuccess(String message) {
                        printLog("ffmpeg.execute onSuccess message=" + message);
                        triggerVideoCompression(3, taskModal);
                    }

                    @Override
                    public void onFinish() {
                        printLog("ffmpeg.execute onFinish");
                        runNextCompression();
                    }
                });
            } catch (FFmpegCommandAlreadyRunningException e) {
                printLog("ffmpeg.execute FFmpegCommandAlreadyRunningException");
                compressionModalList.add(taskModal);
            }

        }*/

    }

    private void runNextCompression() {
        if (compressionModalList != null && compressionModalList.size() > 0) {
            MessageModal messageModal = compressionModalList.remove(0);
            compressVideo(messageModal);
        }
    }

    /**
     * @param tag          1=>Start
     *                     2=>progress
     *                     3=>success
     *                     4=>failure
     * @param messageModal
     */
    private void triggerVideoCompression(final int tag, final MessageModal messageModal) {
        ChatApplication.getInstance().runOnVideoCompressHandlerThread(new Runnable() {
            @Override
            public void run() {
                final List<VideoCompressionListner> uiListnerList = (List<VideoCompressionListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(VideoCompressionListner.class);
                switch (tag) {
                    case 1:
                        messageModal.setStatus(MessageModal.STATUS_NOT_SENT);
                        for (VideoCompressionListner videoCompressionListner : uiListnerList) {
                            videoCompressionListner.videoCompressStart(messageModal);
                        }

                        break;
                    case 2:
                        messageModal.setStatus(MessageModal.STATUS_NOT_SENT);
                        for (VideoCompressionListner videoCompressionListner : uiListnerList) {
                            videoCompressionListner.videoCompressProgress(messageModal);
                        }

                        break;
                    case 3:
                        messageModal.setStatus(MessageModal.STATUS_MEDIA_COMPRESSED);
                        for (VideoCompressionListner videoCompressionListner : uiListnerList) {
                            videoCompressionListner.videoCompressEnd(messageModal, true);
                        }
                        break;
                    case 4:
                        messageModal.setStatus(MessageModal.STATUS_MEDIA_FAILED);
                        for (VideoCompressionListner videoCompressionListner : uiListnerList) {
                            videoCompressionListner.videoCompressEnd(messageModal, false);
                        }
                        break;
                }
            }
        });


    }
}

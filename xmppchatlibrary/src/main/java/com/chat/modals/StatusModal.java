package com.chat.modals;

import java.util.ArrayList;

/**
 * Created by ubuntu on 29/11/16.
 */

public class StatusModal {
    ArrayList<String> statusList = new ArrayList<>();

    public void addNewStatus(String status) {
        statusList.add(status);
    }

    public ArrayList<String> getStatusList() {
        return statusList;
    }

    public void setStatusList(ArrayList<String> statusList) {
        this.statusList = statusList;
    }
}

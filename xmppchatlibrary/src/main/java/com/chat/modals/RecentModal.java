package com.chat.modals;

import com.chat.xmpp.XmppUtils;
import com.chat.xmpp.media.MediaType;

import org.jivesoftware.smackx.chatstates.ChatState;

/**
 * Created by ubuntu on 1/12/16.
 */

public class RecentModal {


    long _id;
    String to_jid;
    long message_call_table_id;
    int type;
    String subject;
    long created;
    long last_read_message_call_table_id;
    long last_read_receipt_sent_message_call_table_id;
    long unseen_message_count;
    long unseen_misscall_count;
    long unseen_row_count;

    String data;
    String media_type;
    String media_url;
    String media_caption;
    int fromMe;
    int messageType;
    int msgStatus;
    String message_id;
    String to_resource;
    String chatStateResource;
    ChatState chatState = ChatState.paused;

    public ChatState getChatState() {
        return chatState;
    }

    public void setChatState(ChatState chatState) {
        this.chatState = chatState;
    }

    public String getTo_resource() {
        return to_resource;
    }

    public void setTo_resource(String to_resource) {
        this.to_resource = to_resource;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public MediaType getMediaType() {
        MediaType mediaType = null;
        if (XmppUtils.checkVaildString(media_type)) {
            if (media_type.equals(MessageModal.MEDIA_TYPE_LINK)) {
                mediaType = MediaType.link;
            } else if (media_type.equals(MessageModal.MEDIA_TYPE_IMAGE)) {
                mediaType = MediaType.image;
            } else if (media_type.equals(MessageModal.MEDIA_TYPE_VIDEO)) {
                mediaType = MediaType.video;
            } else if (media_type.equals(MessageModal.MEDIA_TYPE_AUDIO)) {
                mediaType = MediaType.audio;
            } else if (media_type.equals(MessageModal.MEDIA_TYPE_LOCATION)) {
                mediaType = MediaType.location;
            } else if (media_type.equals(MessageModal.MEDIA_TYPE_CONTACT)) {
                mediaType = MediaType.contact;
            } else if (media_type.equals(MessageModal.MEDIA_TYPE_DOCUMENT)) {
                mediaType = MediaType.document;
            }

        }
        return mediaType;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getTo_jid() {
        return to_jid;
    }

    public void setTo_jid(String to_jid) {
        this.to_jid = to_jid;
    }

    public long getMessage_call_table_id() {
        return message_call_table_id;
    }

    public void setMessage_call_table_id(long message_call_table_id) {
        this.message_call_table_id = message_call_table_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getLast_read_message_call_table_id() {
        return last_read_message_call_table_id;
    }

    public void setLast_read_message_call_table_id(long last_read_message_call_table_id) {
        this.last_read_message_call_table_id = last_read_message_call_table_id;
    }

    public long getLast_read_receipt_sent_message_call_table_id() {
        return last_read_receipt_sent_message_call_table_id;
    }

    public void setLast_read_receipt_sent_message_call_table_id(long last_read_receipt_sent_message_call_table_id) {
        this.last_read_receipt_sent_message_call_table_id = last_read_receipt_sent_message_call_table_id;
    }

    public long getUnseen_message_count() {
        return unseen_message_count;
    }

    public void setUnseen_message_count(long unseen_message_count) {
        this.unseen_message_count = unseen_message_count;
    }

    public long getUnseen_misscall_count() {
        return unseen_misscall_count;
    }

    public void setUnseen_misscall_count(long unseen_misscall_count) {
        this.unseen_misscall_count = unseen_misscall_count;
    }

    public long getUnseen_row_count() {
        return unseen_row_count;
    }

    public void setUnseen_row_count(long unseen_row_count) {
        this.unseen_row_count = unseen_row_count;
    }


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getFromMe() {
        return fromMe;
    }

    public void setFromMe(int fromMe) {
        this.fromMe = fromMe;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getMsgStatus() {
        return msgStatus;
    }

    public void setMsgStatus(int msgStatus) {
        this.msgStatus = msgStatus;
    }

    public String getMedia_caption() {
        return media_caption;
    }

    public void setMedia_caption(String media_caption) {
        this.media_caption = media_caption;
    }

    public String getChatStateResource() {
        return chatStateResource;
    }

    public void setChatStateResource(String chatStateResource) {
        this.chatStateResource = chatStateResource;
    }
}

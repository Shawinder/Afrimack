package com.chat.modals;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.lang.reflect.Modifier.TRANSIENT;

/**
 * Created by ubuntu on 23/12/16.
 */

public class ContactDetailModal {

    public static final int IS_NUMER_PHONE = 1;
    public static final int IS_NUMER_EMAIL = 2;
    public static final int IS_NUMER_EVENT = 3;

    String name;

    transient byte[] image;
    List<Detail> detailArrayList;
    transient Detail registeredDetail;

    public String toJson() {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(TRANSIENT).create();
        return gson.toJson(this);
    }

    @Override
    public String toString() {
        return "Name=" + name + "," + detailArrayList.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public List<Detail> getDetailArrayList() {
        return detailArrayList;
    }

    public void setDetailArrayList(List<Detail> detailArrayList) {
        this.detailArrayList = detailArrayList;
    }

    public void sortByPhoneNumber() {
        if (detailArrayList == null || detailArrayList.size() == 0) {
            return;
        }
        Collections.sort(detailArrayList, new Comparator<Detail>() {
            public int compare(Detail detail1, Detail detail2) {

                if (detail1.getIsNumber() < detail2.getIsNumber()) {
                    return -1;
                } else if (detail1.getIsNumber() > detail2.getIsNumber()) {
                    return 1;
                }
                return 0;
            }
        });
    }

    public Detail getRegisteredContact() {
        if (registeredDetail != null) {
            return registeredDetail;
        }
        if (detailArrayList != null && detailArrayList.size() > 0) {

            for (Detail detail : detailArrayList) {
                if ((detail.getIsNumber() == IS_NUMER_PHONE) && detail.isRegistered()) {
                    registeredDetail = detail;
                    break;
                }
            }
        }
        if (registeredDetail == null) {
            registeredDetail = new Detail();
            registeredDetail.setValue("nodetail");
        }
        return registeredDetail;
    }

    public static class Detail {
        String value;
        String valueType;
        int isNumber;
        transient boolean isSelected;
        boolean isRegistered;


        @Override
        public boolean equals(Object obj) {
            return getValue().equals(((Detail) obj).getValue());
        }

        @Override
        public String toString() {
            return "Value=" + value + ", ValueType=" + valueType;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getValueType() {
            return valueType;
        }

        public void setValueType(String valueType) {
            this.valueType = valueType;
        }

        public int getIsNumber() {
            return isNumber;
        }

        public void setIsNumber(int isNumber) {
            this.isNumber = isNumber;
        }

        public boolean isRegistered() {
            return isRegistered;
        }

        public void setRegistered(boolean registered) {
            isRegistered = registered;
        }
    }

    public ContactDetailModal getFilteredDetail() {
        ContactDetailModal contactDetailModal = new ContactDetailModal();
        contactDetailModal.setName(getName());
        List<Detail> list = new ArrayList<>();
        contactDetailModal.setDetailArrayList(list);
        for (Detail detail : getDetailArrayList()) {
            if (detail.isSelected()) {
                list.add(detail);
            }
        }
        return contactDetailModal;
    }

    public boolean checkAtleastOneNumberExist() {

        for (Detail detail : getDetailArrayList()) {
            if (detail.isSelected() && (detail.getIsNumber() == IS_NUMER_PHONE)) {
                return true;
            }
        }
        return false;
    }


}

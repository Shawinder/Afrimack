package com.chat.modals;

import com.chat.database.tables.MessageAndCallTable;
import com.chat.documentmessages.DocModal;
import com.chat.xmpp.XmppUtils;
import com.chat.xmpp.media.MediaType;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Created by ubuntu on 1/12/16.
 */

public class MessageModal {

    public static final int STATUS_NOT_SENT = 0;
    public static final int STATUS_SENT = 1;
    public static final int STATUS_DELIVERED = 2;
    public static final int STATUS_DISPLAYED = 3;
    public static final int STATUS_RECEIVED = 4;
    public static final int STATUS_DELIVERY_RECEIPT_SENT = 5;
    public static final int STATUS_DISPLAY_RECEIPT_SENT = 6;
    public static final int STATUS_MEDIA_START = 7;
    public static final int STATUS_MEDIA_PROGRESS = 8;
    public static final int STATUS_MEDIA_END = 9;
    public static final int STATUS_MEDIA_FAILED = 10;
    public static final int STATUS_MEDIA_COMPRESSED = 11;


    public static final String MEDIA_TYPE_LINK = "1";
    public static final String MEDIA_TYPE_IMAGE = "2";
    public static final String MEDIA_TYPE_VIDEO = "3";
    public static final String MEDIA_TYPE_AUDIO = "4";
    public static final String MEDIA_TYPE_LOCATION = "5";
    public static final String MEDIA_TYPE_CONTACT = "6";
    public static final String MEDIA_TYPE_DOCUMENT = "7";

    long _id;
    String to_jid;
    int from_me;
    int message_type = MessageAndCallTable.TYPE_MESSAGE_NORMAL;
    String message_id;
    int status;
    String data;
    String media_url;
    String media_hash;
    String media_mime_type;
    String media_type;
    long media_size;
    String media_name;
    String media_caption;
    long media_duration;
    int media_origin;
    double latitude;
    double longitude;
    String thumb_image;
    String to_resource;
    long timestamp;
    long timestamp_received;
    long timestamp_send;
    long timestamp_receipt_server;
    long timestamp_receipt_device;
    long timestamp_read_device;
    byte[] raw_data;
    int recipient_count;
    int starred;
    String mentioned_jids;

    int media_progress;

    int mediaWidth = -1;
    int mediaHeight = -1;

    DocModal.DocType docType;
    String docExt;

    ContactDetailModal contactDetailModal;
    String[] videoCmddata;

    int msgUnReadCount = 0;

    public String[] getVideoCmddata() {
        return videoCmddata;
    }

    public void setVideoCmddata(String[] videoCmddata) {
        this.videoCmddata = videoCmddata;
    }

    public int getMedia_progress() {
        return media_progress;
    }

    public void setMedia_progress(int media_progress) {
        this.media_progress = media_progress;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getTo_jid() {
        return to_jid;
    }

    public void setTo_jid(String to_jid) {
        this.to_jid = to_jid;
    }

    public int getFrom_me() {
        return from_me;
    }

    public void setFrom_me(int from_me) {
        this.from_me = from_me;
    }

    public int getMessage_type() {
        return message_type;
    }

    public void setMessage_type(int message_type) {
        this.message_type = message_type;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getMedia_hash() {
        return media_hash;
    }

    public void setMedia_hash(String media_hash) {
        this.media_hash = media_hash;
    }

    public String getMedia_mime_type() {
        return media_mime_type;
    }

    public void setMedia_mime_type(String media_mime_type) {
        this.media_mime_type = media_mime_type;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(MediaType media_type) {
        if (media_type == MediaType.link) {
            this.media_type = MEDIA_TYPE_LINK;
        } else if (media_type == MediaType.image) {
            this.media_type = MEDIA_TYPE_IMAGE;
        } else if (media_type == MediaType.video) {
            this.media_type = MEDIA_TYPE_VIDEO;
        } else if (media_type == MediaType.audio) {
            this.media_type = MEDIA_TYPE_AUDIO;
        } else if (media_type == MediaType.location) {
            this.media_type = MEDIA_TYPE_LOCATION;
        } else if (media_type == MediaType.contact) {
            this.media_type = MEDIA_TYPE_CONTACT;
        } else if (media_type == MediaType.document) {
            this.media_type = MEDIA_TYPE_DOCUMENT;
        }
    }

    public MediaType getMediaType() {
        MediaType mediaType = null;
        if (XmppUtils.checkVaildString(media_type)) {
            if (media_type.equals(MEDIA_TYPE_LINK)) {
                mediaType = MediaType.link;
            } else if (media_type.equals(MEDIA_TYPE_IMAGE)) {
                mediaType = MediaType.image;
            } else if (media_type.equals(MEDIA_TYPE_VIDEO)) {
                mediaType = MediaType.video;
            } else if (media_type.equals(MEDIA_TYPE_AUDIO)) {
                mediaType = MediaType.audio;
            } else if (media_type.equals(MEDIA_TYPE_LOCATION)) {
                mediaType = MediaType.location;
            } else if (media_type.equals(MEDIA_TYPE_CONTACT)) {
                mediaType = MediaType.contact;
            } else if (media_type.equals(MEDIA_TYPE_DOCUMENT)) {
                mediaType = MediaType.document;
            }

        }
        return mediaType;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public long getMedia_size() {
        return media_size;
    }

    public void setMedia_size(long media_size) {
        this.media_size = media_size;
    }

    public String getMedia_name() {
        return media_name;
    }

    public void setMedia_name(String media_name) {
        this.media_name = media_name;
    }

    public String getMedia_caption() {
        return media_caption;
    }

    public void setMedia_caption(String media_caption) {
        this.media_caption = media_caption;
    }

    public long getMedia_duration() {
        return media_duration;
    }

    public void setMedia_duration(long media_duration) {
        this.media_duration = media_duration;
    }

    public int getMedia_origin() {
        return media_origin;
    }

    public void setMedia_origin(int media_origin) {
        this.media_origin = media_origin;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getTo_resource() {
        return to_resource;
    }

    public void setTo_resource(String to_resource) {
        this.to_resource = to_resource;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp_received() {
        return timestamp_received;
    }

    public void setTimestamp_received(long timestamp_received) {
        this.timestamp_received = timestamp_received;
    }

    public long getTimestamp_send() {
        return timestamp_send;
    }

    public void setTimestamp_send(long timestamp_send) {
        this.timestamp_send = timestamp_send;
    }

    public long getTimestamp_receipt_server() {
        return timestamp_receipt_server;
    }

    public void setTimestamp_receipt_server(long timestamp_receipt_server) {
        this.timestamp_receipt_server = timestamp_receipt_server;
    }

    public long getTimestamp_receipt_device() {
        return timestamp_receipt_device;
    }

    public void setTimestamp_receipt_device(long timestamp_receipt_device) {
        this.timestamp_receipt_device = timestamp_receipt_device;
    }

    public long getTimestamp_read_device() {
        return timestamp_read_device;
    }

    public void setTimestamp_read_device(long timestamp_read_device) {
        this.timestamp_read_device = timestamp_read_device;
    }

    public byte[] getRaw_data() {
        return raw_data;
    }

    public void setRaw_data(byte[] raw_data) {
        this.raw_data = raw_data;
    }

    public int getRecipient_count() {
        return recipient_count;
    }

    public void setRecipient_count(int recipient_count) {
        this.recipient_count = recipient_count;
    }

    public int getStarred() {
        return starred;
    }

    public void setStarred(int starred) {
        this.starred = starred;
    }

    public String getMentioned_jids() {
        return mentioned_jids;
    }

    public void setMentioned_jids(String mentioned_jids) {
        this.mentioned_jids = mentioned_jids;
    }

    public int getMediaWidth() {
        return mediaWidth;
    }

    public void setMediaWidth(int mediaWidth) {
        this.mediaWidth = mediaWidth;
    }

    public int getMediaHeight() {
        return mediaHeight;
    }

    public void setMediaHeight(int mediaHeight) {
        this.mediaHeight = mediaHeight;
    }

    public ContactDetailModal getContactDetailModal() {
        if (contactDetailModal == null) {
            try {
                contactDetailModal = new Gson().fromJson(media_url, ContactDetailModal.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return contactDetailModal;
    }

    public DocModal.DocType getDocType() {
        return docType;
    }

    public void setDocType(DocModal.DocType docType) {
        this.docType = docType;
    }

    public String getDocExt() {
        return docExt;
    }

    public void setDocExt(String docExt) {
        this.docExt = docExt;
    }

    public int getMsgUnReadCount() {
        return msgUnReadCount;
    }

    public void setMsgUnReadCount(int msgUnReadCount) {
        this.msgUnReadCount = msgUnReadCount;
    }
}

package com.chat.xmpp.media.uploadUtils;

import com.chat.ChatApplication;
import com.chat.modals.MessageModal;
import com.chat.xmpp.XmppLogger;
import com.chat.xmpp.listners.OnMediaUploadListner;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ubuntu on 15/12/16.
 */

public class UploadMediaRunnable implements Runnable {

    public static final String TAG = "UploadMediaRunnable";

    final UploadRequest uploadRequest;
    final UploadTask uploadTask;
    final Runnable startRunnable = new Runnable() {
        @Override
        public void run() {
            final ArrayList<OnMediaUploadListner> mediaUploadListnerArrayList = (ArrayList<OnMediaUploadListner>) ChatApplication.getInstance().getXmppListners().
                    getUiListernsArrayList(OnMediaUploadListner.class);
            uploadRequest.getMessageModal().setStatus(MessageModal.STATUS_MEDIA_START);
            XmppLogger.printLog(TAG + " start for uploadId " + uploadRequest.getUploadId());
            for (OnMediaUploadListner onMediaUploadListner : mediaUploadListnerArrayList) {
                onMediaUploadListner.onMediaUploadStart(uploadRequest);
            }
        }
    };
    final Runnable progressRunnable = new Runnable() {
        @Override
        public void run() {
            final ArrayList<OnMediaUploadListner> mediaUploadListnerArrayList = (ArrayList<OnMediaUploadListner>) ChatApplication.getInstance().getXmppListners().
                    getUiListernsArrayList(OnMediaUploadListner.class);
            uploadRequest.getMessageModal().setStatus(MessageModal.STATUS_MEDIA_PROGRESS);
            XmppLogger.printLog(TAG + " progress=" + uploadRequest.progress + " for uploadId " + uploadRequest.getUploadId());
            for (OnMediaUploadListner onMediaUploadListner : mediaUploadListnerArrayList) {
                onMediaUploadListner.onMediaUploadProgress(uploadRequest);
            }
        }
    };
    final Runnable endRunnable = new Runnable() {
        @Override
        public void run() {
            final ArrayList<OnMediaUploadListner> mediaUploadListnerArrayList = (ArrayList<OnMediaUploadListner>) ChatApplication.getInstance().getXmppListners().
                    getUiListernsArrayList(OnMediaUploadListner.class);
            uploadRequest.getMessageModal().setStatus(MessageModal.STATUS_MEDIA_END);
            XmppLogger.printLog(TAG + " end for uploadId " + uploadRequest.getUploadId());
            for (OnMediaUploadListner onMediaUploadListner : mediaUploadListnerArrayList) {
                onMediaUploadListner.onMediaUploadEnd(uploadRequest);
            }
        }
    };
    final Runnable failedRunnable = new Runnable() {
        @Override
        public void run() {
            final ArrayList<OnMediaUploadListner> mediaUploadListnerArrayList = (ArrayList<OnMediaUploadListner>) ChatApplication.getInstance().getXmppListners().
                    getUiListernsArrayList(OnMediaUploadListner.class);
            uploadRequest.getMessageModal().setStatus(MessageModal.STATUS_MEDIA_FAILED);
            XmppLogger.printLog(TAG + " failed for uploadId " + uploadRequest.getUploadId());
            for (OnMediaUploadListner onMediaUploadListner : mediaUploadListnerArrayList) {
                onMediaUploadListner.onMediaUploadEnd(uploadRequest);
            }
        }
    };
    boolean isRunning = false;
    boolean isCancelled = false;


    public UploadMediaRunnable(UploadRequest uploadRequest) {
        this.uploadRequest = uploadRequest;
        this.uploadTask = new UploadTask(this);
    }

    public UploadRequest getUploadRequest() {
        return uploadRequest;
    }

    @Override
    public void run() {

        try {
            isRunning = true;
            uploadTask.calculateData();
            triggerOnMediaUploadListner(1);
            uploadTask.upload();
        } catch (IOException e) {
            e.printStackTrace();
            uploadRequest.exception = e;
            uploadRequest.response = null;
            triggerOnMediaUploadListner(4);
            return;
        }
        triggerOnMediaUploadListner(3);

    }

    public void cancelled() {
        uploadRequest.setException(new IOException("task is cancelled."));
        uploadRequest.response = null;
        triggerOnMediaUploadListner(4);
    }


    /**
     * @param triggerFor 1=> upload start
     *                   2=>upload progress
     *                   3=>upload completed
     *                   4=>upload failed
     */
    public void triggerOnMediaUploadListner(final int triggerFor) {
        if (triggerFor == 2) {
            ChatApplication.getInstance().removeFromUiThread(progressRunnable);
            ChatApplication.getInstance().runOnUiThread(progressRunnable);
        } else {
            switch (triggerFor) {
                case 1:
                    ChatApplication.getInstance().runOnUiThread(startRunnable);
                    break;
                case 3:
                    ChatApplication.getInstance().runOnUiThread(endRunnable);
                    break;
                case 4:
                    ChatApplication.getInstance().runOnUiThread(failedRunnable);
                    break;
            }
        }

    }


}

package com.chat.xmpp.media.uploadUtils;

import com.chat.ChatApplication;
import com.chat.xmpp.listners.OnMediaUploadListner;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ubuntu on 15/12/16.
 */


public class UploadMediaHandler implements OnMediaUploadListner {


    static UploadMediaHandler instance;


    static {
        instance = new UploadMediaHandler();
        ChatApplication.getInstance().getXmppListners().addUiListner(instance);
    }

    LinkedBlockingQueue<Runnable> POOL_QUEUE;
    ExecutorService THREAD_POOL_EXECUTOR;
    private HashMap<String, UploadMediaRunnable> uploadIds;
    private ThreadFactory sThreadFactory;
    UploadMediaRunnable uploadMediaRunnable;
    private UploadMediaHandler() {
        uploadIds = new HashMap<>();
        sThreadFactory = new ThreadFactory() {
            private final AtomicInteger mCount = new AtomicInteger(1);

            public Thread newThread(Runnable r) {

                Thread thread = new Thread(r, "UploadMediaHandler #" + mCount.getAndIncrement());
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        };
        POOL_QUEUE = new LinkedBlockingQueue<Runnable>();
        THREAD_POOL_EXECUTOR
                = new ThreadPoolExecutor(4, 4, 1,
                TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), sThreadFactory);
    }

    public static UploadMediaHandler getInstance() {
        return instance;
    }

    public boolean submit(UploadRequest uploadRequest) {
        if (!uploadIds.containsKey(uploadRequest.getUploadId())) {
            UploadMediaRunnable uploadMediaRunnable = new UploadMediaRunnable(uploadRequest);
            uploadIds.put(uploadRequest.getUploadId(), uploadMediaRunnable);
            THREAD_POOL_EXECUTOR.submit(uploadMediaRunnable);
            return true;
        }
        return false;
    }

    public void cancelUpload(String id) {
        if (uploadIds.containsKey(id)) {
            uploadMediaRunnable = uploadIds.get(id);
            if (uploadMediaRunnable.isRunning) {
                uploadMediaRunnable.isCancelled = true;
            } else {
                POOL_QUEUE.remove(uploadMediaRunnable);
                uploadMediaRunnable.cancelled();
            }
        }
    }


    @Override
    public void onMediaUploadStart(UploadRequest uploadRequest) {

    }

    @Override
    public void onMediaUploadProgress(UploadRequest uploadRequest) {

    }

    @Override
    public void onMediaUploadEnd(UploadRequest uploadRequest) {
        uploadIds.remove(uploadRequest.getUploadId());

    }
}

package com.chat.xmpp.media;

import com.chat.xmpp.XmppUtils;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;

/**
 * Created by linux on 2/2/17.
 */

public class UserInfoExtension implements ExtensionElement {

    public static final String NAMESPACE = "urn:chat:userInfo";
    public static final String ELEMENT = "userInfo";

    private String user_name;
    private String user_img;

    public UserInfoExtension() {

    }

    public static UserInfoExtension from(Message message) {
        return message.getExtension(ELEMENT, NAMESPACE);
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    @Override
    public CharSequence toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        if (XmppUtils.checkVaildString(user_name)) {
            xml.attribute("user_name", user_name);
        }
        if (XmppUtils.checkVaildString(user_img)) {
            xml.attribute("user_img", user_img);
        }

        xml.closeEmptyElement();
        return xml;
    }

    public static class Provider extends ExtensionElementProvider<UserInfoExtension> {

        @Override
        public UserInfoExtension parse(XmlPullParser parser, int initialDepth) {

            UserInfoExtension userInfoExtension = new UserInfoExtension();

            try {
                String user_name = parser.getAttributeValue("", "user_name");
                if (XmppUtils.checkVaildString(user_name)) {
                    userInfoExtension.setUser_name(user_name);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            try {
                String user_img = parser.getAttributeValue("", "user_img");
                if (XmppUtils.checkVaildString(user_img)) {
                    userInfoExtension.setUser_img(user_img);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            return userInfoExtension;
        }
    }


}

package com.chat.xmpp.media.BitmapCacheUtils;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;

import com.chat.ChatApplication;
import com.chat.xmpp.XmppUtils;
import com.chat.xmpp.listners.OnBitmapLoadedListner;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ubuntu on 14/12/16.
 */

public class BitmapLoaderHandler implements OnBitmapLoadedListner {


    private static final String TAG = "BitmapLoaderHandler";
    private static final int DEFAULT_MAX_WIDTH = 320;
    private static final int DEFAULT_MAX_HEIGHT = 320;
    private static final ArrayList<String> currentTask = new ArrayList<>();
    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {

            Thread thread = new Thread(r, "BitmapLoaderHandler #" + mCount.getAndIncrement());
            thread.setPriority(Thread.MIN_PRIORITY);
            thread.setDaemon(true);
            return thread;
        }
    };
    private static final ExecutorService THREAD_POOL_EXECUTOR
            = new ThreadPoolExecutor(4, 4, 1,
            TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), sThreadFactory);
    static BitmapLoaderHandler instance;

    static {
        instance = new BitmapLoaderHandler();
        ChatApplication.getInstance().getXmppListners().addUiListner(instance);
    }

    BitmapCache bitmapCache;

    private BitmapLoaderHandler() {
        if (bitmapCache == null) {
            final int memClass = ((ActivityManager) ChatApplication.getInstance().getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
            final int size = 1024 * 1024 * memClass / 8;
            bitmapCache = new BitmapCache(size);
        }

    }

    public static BitmapLoaderHandler getInstance() {
        return instance;
    }

    public Bitmap submit(String tojid, String bitmapKey, String filePath,
                         int width, int height, boolean fromVideo) {
        Bitmap bitmap = bitmapCache.loadBitmapfromcache(bitmapKey);
        if (bitmap == null && !currentTask.contains(bitmapKey)) {
            if (XmppUtils.checkVaildString(filePath)) {
                THREAD_POOL_EXECUTOR.submit(new BitmapLoadRunnable(tojid, bitmapKey, filePath, width, height, fromVideo));
            }

        }
        return bitmap;
    }

    public Bitmap submit(String bitmapKey, String filePath, int width, int height) {
        return submit(null, bitmapKey, filePath, width, height, false);

    }

    public Bitmap submitVideo(String tojid, String bitmapKey, String filePath) {
        return submit(tojid, bitmapKey, filePath, DEFAULT_MAX_WIDTH,
                DEFAULT_MAX_HEIGHT, true);
    }

    public Bitmap submit(String tojid, String bitmapKey, String filePath) {
        return submit(tojid, bitmapKey, filePath, DEFAULT_MAX_WIDTH,
                DEFAULT_MAX_HEIGHT, false);
    }

    public Bitmap submit(String bitmapKey, String filePath) {
        return submit(null, bitmapKey, filePath, DEFAULT_MAX_WIDTH,
                DEFAULT_MAX_HEIGHT, false);
    }


    @Override
    public void bitmapLoadedStart(String id) {

    }

    @Override
    public void bitmapLoadedEnd(String toJid, String id, Bitmap bitmap) {
        bitmapCache.addBitmapToCache(id, bitmap);
        currentTask.remove(id);
    }

    public void addToCache(String id, Bitmap bitmap) {
        if (bitmap == null) {
            return;
        }
        bitmapCache.addBitmapToCache(id, bitmap);
    }
}

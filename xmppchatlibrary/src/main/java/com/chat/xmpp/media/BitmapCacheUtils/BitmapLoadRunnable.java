package com.chat.xmpp.media.BitmapCacheUtils;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import com.chat.ChatApplication;
import com.chat.Utils.ThumbnailHandler;
import com.chat.xmpp.XmppLogger;
import com.chat.xmpp.XmppUtils;
import com.chat.xmpp.listners.OnBitmapLoadedListner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.util.ArrayList;

/**
 * Created by ubuntu on 17/12/16.
 */

public class BitmapLoadRunnable implements Runnable {
    public static final String TAG = "BitmapLoadRunnable";
    final String bitmapKey;
    final String filePath;
    final String toJid;
    final boolean fromVideo;
    final int MAX_WIDTH;
    final int MAX_HEIGHT;
    final ContentResolver contentResolver;

    public BitmapLoadRunnable(String toJid, String bitmapKey, String filePath,
                              int width, int height, boolean fromVideo) {
        this.bitmapKey = bitmapKey;
        this.filePath = filePath;
        this.fromVideo = fromVideo;
        this.toJid = toJid;
        this.MAX_WIDTH = width;
        this.MAX_HEIGHT = height;
        contentResolver = ChatApplication.getInstance().getContentResolver();
    }

    @Override
    public void run() {
        triggerOnBitmapLoadListner(1, null);
        if (XmppUtils.checkVaildString(filePath)) {
            try {
                Bitmap bitmap;
                if (fromVideo) {
                    bitmap = ThumbnailHandler.createVideoThumbnail(filePath,
                            MediaStore.Video.Thumbnails.MINI_KIND);
                } else {
                    bitmap = generateThumb();
                }

                triggerOnBitmapLoadListner(2, bitmap);
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        triggerOnBitmapLoadListner(2, null);


    }

    private Matrix getInformationFromFileSystem(Uri uri) throws IOException {
        String path = uri.getPath();
        if (path == null)
            return null;

        ExifInterface exif = new ExifInterface(path);
        int orientationTag = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        Matrix orientation = new Matrix();
        switch (orientationTag) {
            case ExifInterface.ORIENTATION_NORMAL:

                break;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                orientation.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                orientation.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                orientation.setScale(1, -1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                orientation.setRotate(90);
                orientation.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                orientation.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                orientation.setRotate(-90);
                orientation.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                orientation.setRotate(-90);
                break;

        }
        return orientation;
    }

    private int[] getStoredDimensions(Uri uri) throws IOException {

        InputStream input = contentResolver.openInputStream(uri);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory
                .decodeStream(contentResolver.openInputStream(uri), null, options);
        input.close();

        if (options.outHeight <= 0 || options.outWidth <= 0)
            return null;

        int storedHeight = options.outHeight;
        int storedWidth = options.outWidth;

        return new int[]{storedWidth, storedHeight};
    }

    private Bitmap generateThumb() throws IOException {
        Uri uri = Uri.fromFile(new File(filePath));
        Matrix orientation = getInformationFromFileSystem(uri);
        if (orientation == null) {
            throw new FileNotFoundException(TAG + " orientation not found.");
        }
        int[] storedInfo = getStoredDimensions(uri);
        if (storedInfo == null) {
            throw new InvalidObjectException(TAG + " stored info not found.");
        }
        RectF rect = new RectF(0, 0, storedInfo[0], storedInfo[1]);
        orientation.mapRect(rect);
        int width = (int) rect.width();
        int height = (int) rect.height();
        int subSample = 1;

        if (width > MAX_WIDTH || height > MAX_HEIGHT) {
            subSample = Math.round((float) width / (float) MAX_WIDTH);
            int i = Math.round((float) height / (float) MAX_HEIGHT);

            if (subSample >= i) {
                subSample = i;
            }
        }

        if (width == 0 || height == 0)
            throw new InvalidObjectException(null);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = subSample;
        Bitmap subSampled = BitmapFactory.decodeStream(
                contentResolver.openInputStream(uri), null, options);

        Bitmap picture;
        if (!orientation.isIdentity()) {
            picture = Bitmap.createBitmap(subSampled, 0, 0, options.outWidth,
                    options.outHeight, orientation, false);
            subSampled.recycle();
        } else {
            picture = subSampled;
        }

        return picture;
    }

    public void triggerOnBitmapLoadListner(final int triggerFor, final Bitmap bitmap) {
        ChatApplication.getInstance().runOnBitmapLoadHandlerThread(new Runnable() {
            @Override
            public void run() {
                final ArrayList<OnBitmapLoadedListner> bitmapLoadedListnerArrayList = (ArrayList<OnBitmapLoadedListner>) ChatApplication.getInstance().getXmppListners().
                        getUiListernsArrayList(OnBitmapLoadedListner.class);
                switch (triggerFor) {
                    case 1:
                        XmppLogger.printLog(TAG + " bitmapLoad start for path=" + filePath);
                        for (OnBitmapLoadedListner onBitmapLoadedListner : bitmapLoadedListnerArrayList) {
                            onBitmapLoadedListner.bitmapLoadedStart(bitmapKey);
                        }
                        break;
                    case 2:
                        XmppLogger.printLog(TAG + " bitmapLoad end for path=" + filePath);

                        for (OnBitmapLoadedListner onBitmapLoadedListner : bitmapLoadedListnerArrayList) {
                            onBitmapLoadedListner.bitmapLoadedEnd(toJid, bitmapKey, bitmap);
                        }
                        break;
                }
            }
        });

    }


}

package com.chat.xmpp.media;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;

import com.chat.ChatApplication;
import com.chat.Utils.ThumbnailHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;

/**
 * Created by ubuntu on 14/12/16.
 */

public class MediaThumbGenerator {

    public static final String TAG = "MediaThumbGenerator";

    static int MAX_WIDTH = 30;
    static int MAX_HEIGHT = 30;

    static MediaThumbGenerator instance;
    static ContentResolver contentResolver;

    static {
        instance = new MediaThumbGenerator();
        contentResolver = ChatApplication.getInstance().getContentResolver();
    }

    private MediaThumbGenerator() {

    }

    public static MediaThumbGenerator getInstance() {
        return instance;
    }

    private static Matrix getInformationFromFileSystem(Uri uri) throws IOException {
        String path = uri.getPath();
        if (path == null)
            return null;

        ExifInterface exif = new ExifInterface(path);
        int orientationTag = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        Matrix orientation = new Matrix();
        switch (orientationTag) {
            case ExifInterface.ORIENTATION_NORMAL:

                break;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                orientation.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                orientation.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                orientation.setScale(1, -1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                orientation.setRotate(90);
                orientation.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                orientation.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                orientation.setRotate(-90);
                orientation.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                orientation.setRotate(-90);
                break;

        }
        return orientation;
    }

    private static int[] getStoredDimensions(Uri uri) throws IOException {

        InputStream input = contentResolver.openInputStream(uri);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory
                .decodeStream(contentResolver.openInputStream(uri), null, options);
        input.close();

        if (options.outHeight <= 0 || options.outWidth <= 0)
            return null;

        int storedHeight = options.outHeight;
        int storedWidth = options.outWidth;

        return new int[]{storedWidth, storedHeight};
    }

    private static Bitmap generateThumb(Uri uri) throws IOException {

        Matrix orientation = getInformationFromFileSystem(uri);
        if (orientation == null) {
            throw new FileNotFoundException(TAG + " orientation not found.");
        }
        int[] storedInfo = getStoredDimensions(uri);
        if (storedInfo == null) {
            throw new InvalidObjectException(TAG + " stored info not found.");
        }
        RectF rect = new RectF(0, 0, storedInfo[0], storedInfo[1]);
        orientation.mapRect(rect);
        int width = (int) rect.width();
        int height = (int) rect.height();
        int subSample = 1;

        if (width > MAX_WIDTH || height > MAX_HEIGHT) {
            subSample = Math.round((float) width / (float) MAX_WIDTH);
            int i = Math.round((float) height / (float) MAX_HEIGHT);

            if (subSample >= i) {
                subSample = i;
            }
        }

        if (width == 0 || height == 0)
            throw new InvalidObjectException(null);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = subSample;
        Bitmap subSampled = BitmapFactory.decodeStream(
                contentResolver.openInputStream(uri), null, options);

        Bitmap picture;
        if (!orientation.isIdentity()) {
            picture = Bitmap.createBitmap(subSampled, 0, 0, options.outWidth,
                    options.outHeight, orientation, false);
            subSampled.recycle();
        } else {
            picture = subSampled;
        }

        return picture;
    }

    public static byte[] getThumbBytes(String filePath) throws IOException {
        Uri uri = Uri.fromFile(new File(filePath));

        Bitmap bitmap = generateThumb(uri);
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static String getThumbBase64(String filePath) throws IOException {
        byte[] data = getThumbBytes(filePath);
        if (data == null) {
            return null;
        }
        return Base64.encodeToString(data, Base64.DEFAULT);
    }


    public static byte[] getVideoThumbBytes(String filePath) throws IOException {

        Bitmap bitmap = ThumbnailHandler.createVideoThumbnail(filePath,
                MediaStore.Video.Thumbnails.MICRO_KIND);
        if (bitmap == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static String getVideoThumbBase64(String filePath) throws IOException {
        byte[] data = getVideoThumbBytes(filePath);
        if (data == null) {
            return null;
        }
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public synchronized static Bitmap getLocationBitmap(String filePath) throws FileNotFoundException {
        Uri uri = Uri.fromFile(new File(filePath));
        Bitmap subSampled = BitmapFactory.decodeStream(
                contentResolver.openInputStream(uri), null, null);
        return subSampled;

    }

    public synchronized static String getLocationThumbBase64(String filePath) throws FileNotFoundException {

        Bitmap subSampled = getLocationBitmap(filePath);
        if (subSampled == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        subSampled.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        byte[] byteArray = stream.toByteArray();
        if (byteArray == null) {
            return null;
        }
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
}

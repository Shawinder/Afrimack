package com.chat.xmpp.media;

import com.chat.xmpp.XmppUtils;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;

/**
 * Created by ubuntu on 13/12/16.
 */

public class MediaMessageExtension implements ExtensionElement {

    public static final String NAMESPACE = "urn:chat:media";
    public static final String ELEMENT = "file";

    private String media_url;
    private String media_hash;
    private String media_mime_type;
    private String media_type;
    private String media_name;
    private String media_caption;
    private String thumb_image;
    private long media_size = -1;
    private long media_duration = -1;
    private int media_origin = -1;
    private double latitude = -1;
    private double longitude = -1;


    public MediaMessageExtension() {

    }

    public static MediaMessageExtension from(Message message) {
        return message.getExtension(ELEMENT, NAMESPACE);
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getMedia_hash() {
        return media_hash;
    }

    public void setMedia_hash(String media_hash) {
        this.media_hash = media_hash;
    }

    public String getMedia_mime_type() {
        return media_mime_type;
    }

    public void setMedia_mime_type(String media_mime_type) {
        this.media_mime_type = media_mime_type;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getMedia_name() {
        return media_name;
    }

    public void setMedia_name(String media_name) {
        this.media_name = media_name;
    }

    public String getMedia_caption() {
        return media_caption;
    }

    public void setMedia_caption(String media_caption) {
        this.media_caption = media_caption;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public long getMedia_size() {
        return media_size;
    }

    public void setMedia_size(long media_size) {
        this.media_size = media_size;
    }

    public long getMedia_duration() {
        return media_duration;
    }

    public void setMedia_duration(long media_duration) {
        this.media_duration = media_duration;
    }

    public int getMedia_origin() {
        return media_origin;
    }

    public void setMedia_origin(int media_origin) {
        this.media_origin = media_origin;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public XmlStringBuilder toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        if (XmppUtils.checkVaildString(media_url)) {
            xml.attribute("media_url", media_url);
        }
        if (XmppUtils.checkVaildString(media_hash)) {
            xml.attribute("media_hash", media_hash);
        }
        if (XmppUtils.checkVaildString(media_mime_type)) {
            xml.attribute("media_mime_type", media_mime_type);
        }
        if (XmppUtils.checkVaildString(media_type)) {
            xml.attribute("media_type", media_type);
        }
        if (XmppUtils.checkVaildString(media_name)) {
            xml.attribute("media_name", media_name);
        }
        if (XmppUtils.checkVaildString(media_caption)) {
            xml.attribute("media_caption", media_caption);
        }
        if (XmppUtils.checkVaildString(thumb_image)) {
            xml.attribute("thumb_image", thumb_image);
        }
        if (media_size > -1) {
            xml.attribute("media_size", String.valueOf(media_size));
        }
        if (media_duration > -1) {
            xml.attribute("media_duration", String.valueOf(media_duration));
        }
        if (media_origin > -1) {
            xml.attribute("media_origin", String.valueOf(media_origin));
        }
        if (latitude > -1) {
            xml.attribute("latitude", String.valueOf(latitude));
        }
        if (longitude > -1) {
            xml.attribute("longitude", String.valueOf(longitude));
        }

        xml.closeEmptyElement();
        return xml;
    }

    public static class Provider extends ExtensionElementProvider<MediaMessageExtension> {

        @Override
        public MediaMessageExtension parse(XmlPullParser parser, int initialDepth) {

            MediaMessageExtension mediaMessageExtension = new MediaMessageExtension();
            String media_url = parser.getAttributeValue("", "media_url");
            if (XmppUtils.checkVaildString(media_url)) {
                mediaMessageExtension.setMedia_url(media_url);
            }
            String media_hash = parser.getAttributeValue("", "media_hash");
            if (XmppUtils.checkVaildString(media_hash)) {
                mediaMessageExtension.setMedia_hash(media_hash);
            }
            String media_mime_type = parser.getAttributeValue("", "media_mime_type");
            if (XmppUtils.checkVaildString(media_mime_type)) {
                mediaMessageExtension.setMedia_mime_type(media_mime_type);
            }
            String media_type = parser.getAttributeValue("", "media_type");
            if (XmppUtils.checkVaildString(media_type)) {
                mediaMessageExtension.setMedia_type(media_type);
            }
            String media_name = parser.getAttributeValue("", "media_name");
            if (XmppUtils.checkVaildString(media_name)) {
                mediaMessageExtension.setMedia_name(media_name);
            }
            String media_caption = parser.getAttributeValue("", "media_caption");
            if (XmppUtils.checkVaildString(media_caption)) {
                mediaMessageExtension.setMedia_caption(media_caption);
            }
            String thumb_image = parser.getAttributeValue("", "thumb_image");
            if (XmppUtils.checkVaildString(thumb_image)) {
                mediaMessageExtension.setThumb_image(thumb_image);
            }
            try {
                String media_size = parser.getAttributeValue("", "media_size");
                if (XmppUtils.checkVaildString(media_size)) {
                    Long size = Long.parseLong(media_size);
                    mediaMessageExtension.setMedia_size(size);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            try {
                String media_duration = parser.getAttributeValue("", "media_duration");
                if (XmppUtils.checkVaildString(media_duration)) {
                    Long duration = Long.parseLong(media_duration);
                    mediaMessageExtension.setMedia_duration(duration);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            try {
                String media_origin = parser.getAttributeValue("", "media_origin");
                if (XmppUtils.checkVaildString(media_origin)) {
                    int origin = Integer.parseInt(media_origin);
                    mediaMessageExtension.setMedia_origin(origin);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            try {
                String latitude = parser.getAttributeValue("", "latitude");
                if (XmppUtils.checkVaildString(latitude)) {
                    double lat = Double.parseDouble(latitude);
                    mediaMessageExtension.setLatitude(lat);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            try {
                String longitude = parser.getAttributeValue("", "longitude");
                if (XmppUtils.checkVaildString(longitude)) {
                    double lng = Double.parseDouble(longitude);
                    mediaMessageExtension.setLongitude(lng);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            return mediaMessageExtension;
        }
    }
}

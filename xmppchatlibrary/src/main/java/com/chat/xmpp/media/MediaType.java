package com.chat.xmpp.media;

/**
 * Created by ubuntu on 13/12/16.
 */

public enum MediaType {

    link,

    image,

    video,

    location,

    audio,

    contact,

    document,

    other
}

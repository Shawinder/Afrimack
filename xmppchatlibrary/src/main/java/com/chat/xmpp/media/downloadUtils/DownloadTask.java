package com.chat.xmpp.media.downloadUtils;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by ubuntu on 17/12/16.
 */

public class DownloadTask {
    private static final String USER_AGENT = "BRSOFTECH";
    private static final int BUFFER_SIZE = 2048;
    protected final String url;
    protected final String outputFilepath;
    protected final String method;
    protected final String customUserAgent;
    private final DownloadMediaRunnable downloadMediaRunnable;
    protected long totalBodyBytes;
    protected long downloadedBodyBytes;

    DownloadTask(DownloadMediaRunnable downloadMediaRunnable) {
        this.downloadMediaRunnable = downloadMediaRunnable;
        this.outputFilepath = downloadMediaRunnable.getDownloadRequest().getOutputFilepath();
        this.url = downloadMediaRunnable.getDownloadRequest().getUrl();
        this.method = downloadMediaRunnable.getDownloadRequest().getMethod();
        this.customUserAgent = USER_AGENT;
    }

    private void checkCancelled() throws IOException {
        if (downloadMediaRunnable.isCancelled) {
            throw new IOException("task is cancelled.");
        }
    }

    protected void download() {
        URLConnection connection = null;
        OutputStream requestStream = null;
        InputStream responseStream = null;
        try {
            File dir = new File(outputFilepath).getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }
            connection = getURLConnection();
            connection.connect();
            checkCancelled();
            totalBodyBytes = connection.getContentLength();
            downloadMediaRunnable.getDownloadRequest().setTotalBodyBytes(totalBodyBytes);
            responseStream = new BufferedInputStream(new URL(url).openStream());
            checkCancelled();
            requestStream = new FileOutputStream(outputFilepath);

            byte data[] = new byte[BUFFER_SIZE];
            downloadedBodyBytes = 0;
            int count;
            while ((count = responseStream.read(data)) != -1) {
                downloadedBodyBytes += count;
                requestStream.write(data, 0, count);
                broadcastProgress();
                checkCancelled();
            }
        } catch (Exception excp) {
            excp.printStackTrace();
            downloadMediaRunnable.getDownloadRequest().setException(excp);
        } finally {
            if (requestStream != null) {
                try {
                    requestStream.flush();
                    requestStream.close();
                } catch (Exception exc) {
                }
            }
            if (responseStream != null) {
                try {
                    responseStream.close();
                } catch (Exception exc) {
                }
            }

        }

    }

    protected URLConnection getURLConnection() throws IOException {

        URLConnection conn = new URL(url).openConnection();

        return conn;
    }

    private void broadcastProgress() {
        final int previosDownloadProgress = downloadMediaRunnable.getDownloadRequest().progress;
        downloadMediaRunnable.getDownloadRequest().setDownloadedBodyBytes(downloadedBodyBytes);
        final int nowDownloadProgress = downloadMediaRunnable.getDownloadRequest().progress;
        if (previosDownloadProgress != nowDownloadProgress) {
            downloadMediaRunnable.triggerOnMediaDownloadListner(2);
        }

    }


}

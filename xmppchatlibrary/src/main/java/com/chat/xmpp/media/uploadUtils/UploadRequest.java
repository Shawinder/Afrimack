package com.chat.xmpp.media.uploadUtils;

import android.util.Base64;

import com.chat.modals.MessageModal;

import java.util.ArrayList;

/**
 * Created by ubuntu on 15/12/16.
 */

public class UploadRequest {
    private static final String METHOD_POST = "POST";
    protected long totalBodyBytes;
    protected long uploadedBodyBytes;
    protected ArrayList<NameValue> headers;
    MessageModal messageModal;
    String uploadId;
    String url;
    String method = METHOD_POST;
    int progress = 0;
    int responseCode = 0;
    String response;
    Exception exception;
    private ArrayList<MultipartUploadFile> files;
    private ArrayList<NameValue> parameters;

    public UploadRequest(String url, MessageModal messageModal) {
        this.url = url;
        this.messageModal = messageModal;
        this.uploadId = messageModal.getMessage_id();
        this.files = new ArrayList<MultipartUploadFile>();
        this.parameters = new ArrayList<NameValue>();
        this.headers = new ArrayList<NameValue>();
    }

    public MessageModal getMessageModal() {
        return messageModal;
    }

    public int getProgress() {
        return progress;
    }

    public String getUploadId() {
        return uploadId;
    }

    public String getUrl() {
        return url;
    }

    public ArrayList<MultipartUploadFile> getFiles() {
        return files;
    }

    public ArrayList<NameValue> getParameters() {
        return parameters;
    }

    public ArrayList<NameValue> getHeaders() {
        return headers;
    }

    public String getMethod() {
        return method;
    }

    public void setTotalBodyBytes(long totalBodyBytes) {
        this.totalBodyBytes = totalBodyBytes;
    }

    public void setUploadedBodyBytes(long uploadedBodyBytes) {
        this.uploadedBodyBytes = uploadedBodyBytes;
        if (this.uploadedBodyBytes == 0) {
            progress = 0;
        } else {
            final int percentsProgress = (int) (uploadedBodyBytes * 100 / totalBodyBytes);
            progress = percentsProgress;
        }
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponse() {
        return response;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public void addFile(String param, String path) {
        MultipartUploadFile multipartUploadFile = new MultipartUploadFile(path, param);
        this.files.add(multipartUploadFile);
    }

    public void addHeader(String name, String value) {
        this.headers.add(new NameValue(name, value));
    }

    public void addParameters(String name, String value) {
        this.parameters.add(new NameValue(name, value));
    }

    public void setBasicAuth(String email, String password) {
        String basicAuth = "Basic " +
                Base64.encodeToString(String.format("%s:%s", email, password)
                        .getBytes(), Base64.NO_WRAP);
        this.headers.add(new NameValue("Authorization", basicAuth));
    }
}

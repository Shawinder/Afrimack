package com.chat.xmpp.media.BitmapCacheUtils;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.chat.xmpp.XmppLogger;

/**
 * Created by ubuntu on 17/12/16.
 */

public class BitmapCache {

    public static final String TAG = "BitmapCache";
    static BitmapCache instance;
    private LruCache<String, Bitmap> mBitmapCache;


    public BitmapCache(int size) {
        XmppLogger.printLog(TAG + " BitmapCache size=" + size);
        mBitmapCache = new LruCache<String, Bitmap>(size) {
            @Override
            protected int sizeOf(String key, Bitmap b) {
                return b.getHeight() * b.getWidth() * 4;
            }
        };
    }

    public BitmapCache getInstance() {
        return instance;
    }

    public void addBitmapToCache(String key, Bitmap bitmap) {
        if (getBitmapFromCache(key) == null && bitmap != null) {
            mBitmapCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromCache(String key) {
        return mBitmapCache.get(key);
    }

    public Bitmap loadBitmapfromcache(String imageKey) {
        final Bitmap bitmap = getBitmapFromCache(imageKey);

        if (bitmap != null) {

            return bitmap;
        }
        return null;
    }


}

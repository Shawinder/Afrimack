package com.chat.xmpp.media.downloadUtils;

import com.chat.modals.MessageModal;

/**
 * Created by ubuntu on 17/12/16.
 */

public class DownloadRequest {

    private static final String METHOD_GET = "GET";
    protected long totalBodyBytes;
    protected long downloadedBodyBytes;
    MessageModal messageModal;
    String dwonloadId;
    String url;
    String outputFilepath;
    String method = METHOD_GET;
    int progress = 0;
    int responseCode = 0;
    String response;
    Exception exception;

    public DownloadRequest(String url, String outputFilepath, MessageModal messageModal) {
        this.url = url;
        this.outputFilepath = outputFilepath;
        this.messageModal = messageModal;
        this.dwonloadId = messageModal.getMessage_id();
        setTotalBodyBytes(messageModal.getMedia_size());
    }

    public MessageModal getMessageModal() {
        return messageModal;
    }

    public int getProgress() {
        return progress;
    }

    public String getDwonloadId() {
        return dwonloadId;
    }

    public String getUrl() {
        return url;
    }

    public String getMethod() {
        return method;
    }

    public String getOutputFilepath() {
        return outputFilepath;
    }

    public void setTotalBodyBytes(long totalBodyBytes) {
        this.totalBodyBytes = totalBodyBytes;
    }

    public void setDownloadedBodyBytes(long downloadedBodyBytes) {
        this.downloadedBodyBytes = downloadedBodyBytes;
        if (this.downloadedBodyBytes == 0) {
            progress = 0;
        } else {
            final int percentsProgress = (int) (downloadedBodyBytes * 100 / totalBodyBytes);
            progress = percentsProgress;
        }
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponse() {
        return response;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}

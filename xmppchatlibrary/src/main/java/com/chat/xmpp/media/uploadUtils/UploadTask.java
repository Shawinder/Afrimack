package com.chat.xmpp.media.uploadUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by ubuntu on 15/12/16.
 */

public class UploadTask {


    private static final String USER_AGENT = "BRSOFTECH";
    private static final int BUFFER_SIZE = 2048;

    private static final String NEW_LINE = "\r\n";
    private static final String TWO_HYPHENS = "--";
    protected final ArrayList<NameValue> headers;
    protected final String url;
    protected final String method;
    protected final String customUserAgent;
    private final ArrayList<MultipartUploadFile> files;
    private final ArrayList<NameValue> parameters;
    private final UploadMediaRunnable uploadMediaRunnable;
    protected long totalBodyBytes;
    protected long uploadedBodyBytes;
    private String boundary;
    private byte[] boundaryBytes;
    private byte[] trailerBytes;

    UploadTask(UploadMediaRunnable uploadMediaRunnable) {
        this.uploadMediaRunnable = uploadMediaRunnable;
        this.url = uploadMediaRunnable.getUploadRequest().getUrl();
        this.method = uploadMediaRunnable.getUploadRequest().getMethod();
        this.customUserAgent = USER_AGENT;
        this.files = uploadMediaRunnable.getUploadRequest().getFiles();
        this.parameters = uploadMediaRunnable.getUploadRequest().getParameters();
        this.headers = uploadMediaRunnable.getUploadRequest().getHeaders();
        this.headers.add(new NameValue("User-Agent", customUserAgent));

    }

    protected void calculateData() throws IOException {
        boundary = getBoundary();
        boundaryBytes = getBoundaryBytes();
        trailerBytes = getTrailerBytes();
        totalBodyBytes = getBodyLength();
        uploadMediaRunnable.getUploadRequest().setTotalBodyBytes(totalBodyBytes);
        uploadMediaRunnable.getUploadRequest().setUploadedBodyBytes(0);
        uploadMediaRunnable.getUploadRequest().progress = 0;
        checkValidBytes();
        checkCancelled();
    }

    private void checkCancelled() throws IOException {
        if (uploadMediaRunnable.isCancelled) {
            throw new IOException("task is cancelled.");
        }
    }

    protected void upload() throws IOException {
        HttpURLConnection connection = null;
        OutputStream requestStream = null;
        InputStream responseStream = null;
        try {

            connection = getHttpURLConnection();

            setRequestHeaders(connection);
            if (android.os.Build.VERSION.SDK_INT >= 19) {
                connection.setFixedLengthStreamingMode(totalBodyBytes);
            } else {
                connection.setFixedLengthStreamingMode((int) totalBodyBytes);
            }
            requestStream = connection.getOutputStream();
            try {
                writeBody(requestStream);
            } finally {

            }
            final int serverResponseCode = connection.getResponseCode();
            uploadMediaRunnable.getUploadRequest().responseCode = serverResponseCode;

            if (serverResponseCode / 100 == 2) {
                responseStream = connection.getInputStream();
            } else {
                responseStream = connection.getErrorStream();
            }
            final String serverResponseMessage = getResponseBodyAsString(responseStream);
            uploadMediaRunnable.getUploadRequest().response = serverResponseMessage;
        } finally {
            if (requestStream != null) {
                try {
                    requestStream.flush();
                    requestStream.close();
                } catch (Exception exc) {
                }
            }
            if (responseStream != null) {
                try {
                    responseStream.close();
                } catch (Exception exc) {
                }
            }
            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception exc) {
                }
            }
        }

    }

    protected HttpURLConnection getHttpURLConnection() throws IOException {
        final HttpURLConnection conn = (HttpURLConnection) new URL(url)
                .openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setRequestMethod(method);
        if (files.size() <= 1) {
            conn.setRequestProperty("Connection", "close");
        } else {
            conn.setRequestProperty("Connection", "Keep-Alive");
        }
        conn.setRequestProperty("ENCTYPE", "multipart/form-data");
        conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
        return conn;
    }

    private void setRequestHeaders(HttpURLConnection connection) {
        if (!headers.isEmpty()) {
            for (final NameValue param : headers) {
                connection
                        .setRequestProperty(param.getName(), param.getValue());
            }
        }
    }

    protected void writeBody(OutputStream requestStream) throws IOException {
        writeRequestParameters(requestStream);
        checkCancelled();
        writeFiles(requestStream);
        requestStream.write(trailerBytes, 0, trailerBytes.length);
    }

    private void writeRequestParameters(OutputStream requestStream) throws IOException {
        if (!parameters.isEmpty()) {
            for (final NameValue parameter : parameters) {
                requestStream.write(boundaryBytes, 0, boundaryBytes.length);
                byte[] formItemBytes = parameter.getBytes();
                requestStream.write(formItemBytes, 0, formItemBytes.length);

                uploadedBodyBytes += boundaryBytes.length + formItemBytes.length;
                broadcastProgress();
            }
        }
    }

    private void writeFiles(OutputStream requestStream) throws IOException {
        for (MultipartUploadFile file : files) {

            requestStream.write(boundaryBytes, 0, boundaryBytes.length);
            byte[] headerBytes = file.getMultipartHeader();
            requestStream.write(headerBytes, 0, headerBytes.length);

            uploadedBodyBytes += boundaryBytes.length + headerBytes.length;
            broadcastProgress();

            final InputStream stream = file.getStream();
            writeStream(requestStream, stream);
            checkCancelled();
        }
    }

    protected void writeStream(OutputStream requestStream, InputStream stream) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead;

        while ((bytesRead = stream.read(buffer, 0, buffer.length)) > 0) {
            requestStream.write(buffer, 0, bytesRead);
            uploadedBodyBytes += bytesRead;
            broadcastProgress();
            checkCancelled();
        }
    }

    private String getResponseBodyAsString(final InputStream inputStream) {
        StringBuilder outString = new StringBuilder();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                outString.append(line).append("\n");
            }
        } catch (Exception exc) {
            try {
                if (reader != null)
                    reader.close();
            } catch (Exception readerExc) {
            }
        }

        return outString.toString();
    }

    private void checkValidBytes() throws IOException {
        if (android.os.Build.VERSION.SDK_INT < 19
                && totalBodyBytes > Integer.MAX_VALUE) {
            throw new IOException(
                    "You need Android API version 19 or newer to "
                            + "upload more than 2GB in a single request using "
                            + "fixed size content length. Try switching to "
                            + "chunked mode instead, but make sure your server side supports it!");
        }
    }

    private String getBoundary() {
        final StringBuilder builder = new StringBuilder();
        builder.append("---------------------------").append(System.currentTimeMillis());
        return builder.toString();
    }

    private byte[] getBoundaryBytes() throws UnsupportedEncodingException {
        final StringBuilder builder = new StringBuilder();
        builder.append(NEW_LINE).append(TWO_HYPHENS).append(boundary).append(NEW_LINE);
        return builder.toString().getBytes("US-ASCII");
    }

    private byte[] getTrailerBytes() throws UnsupportedEncodingException {
        final StringBuilder builder = new StringBuilder();
        builder.append(NEW_LINE).append(TWO_HYPHENS).append(boundary).append(TWO_HYPHENS).append(NEW_LINE);
        return builder.toString().getBytes("US-ASCII");
    }

    protected long getBodyLength() throws UnsupportedEncodingException {
        long parameterBytes = getRequestParametersLength();
        final long totalFileBytes = getFilesLength();

        final long bodyLength = parameterBytes + totalFileBytes + trailerBytes.length;
        return bodyLength;
    }


    private long getRequestParametersLength() throws UnsupportedEncodingException {
        long parametersBytes = 0;

        if (!parameters.isEmpty()) {
            for (final NameValue parameter : parameters) {
                parametersBytes += boundaryBytes.length + parameter.getBytes().length;
            }
        }

        return parametersBytes;
    }

    private long getFilesLength() throws UnsupportedEncodingException {
        long total = 0;

        for (MultipartUploadFile file : files) {
            total += file.getTotalMultipartBytes(boundaryBytes.length);
        }

        return total;
    }

    private void broadcastProgress() {
        final int previosUploadProgress = uploadMediaRunnable.getUploadRequest().progress;
        uploadMediaRunnable.getUploadRequest().setUploadedBodyBytes(uploadedBodyBytes);
        final int nowUploadProgress = uploadMediaRunnable.getUploadRequest().progress;
        if (previosUploadProgress != nowUploadProgress) {
            uploadMediaRunnable.triggerOnMediaUploadListner(2);
        }

    }


}

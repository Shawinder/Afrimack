package com.chat.xmpp.media.downloadUtils;

import com.chat.ChatApplication;
import com.chat.database.tables.MessageAndCallTable;
import com.chat.modals.MessageModal;
import com.chat.xmpp.XmppLogger;
import com.chat.xmpp.XmppUtils;
import com.chat.xmpp.listners.OnMediaDownloadListner;
import com.chat.xmpp.media.MediaType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 17/12/16.
 */

public class DownloadMediaRunnable implements Runnable {
    public static final String TAG = "DownloadMediaRunnable";

    final DownloadRequest downloadRequest;
    final DownloadTask downloadTask;
    final Runnable startRunnable = new Runnable() {
        @Override
        public void run() {
            final ArrayList<OnMediaDownloadListner> mediaUploadListnerArrayList =
                    (ArrayList<OnMediaDownloadListner>) ChatApplication.getInstance().getXmppListners().
                            getUiListernsArrayList(OnMediaDownloadListner.class);
            if (downloadRequest.getMessageModal().getMediaType() != MediaType.location) {
                downloadRequest.getMessageModal().setStatus(MessageModal.STATUS_MEDIA_START);
            }

            XmppLogger.printLog(TAG + " start for downloadId " + downloadRequest.getDwonloadId());
            for (OnMediaDownloadListner onMediaDownloadListner : mediaUploadListnerArrayList) {
                onMediaDownloadListner.onMediaDownloadStart(downloadRequest);
            }
        }
    };
    final Runnable progressRunnable = new Runnable() {
        @Override
        public void run() {
            final ArrayList<OnMediaDownloadListner> mediaUploadListnerArrayList =
                    (ArrayList<OnMediaDownloadListner>) ChatApplication.getInstance().getXmppListners().
                            getUiListernsArrayList(OnMediaDownloadListner.class);
            if (downloadRequest.getMessageModal().getMediaType() != MediaType.location) {
                downloadRequest.getMessageModal().setStatus(MessageModal.STATUS_MEDIA_PROGRESS);
            }
            XmppLogger.printLog(TAG + " progress=" + downloadRequest.progress + " for downloadId " + downloadRequest.getDwonloadId());
            for (OnMediaDownloadListner onMediaDownloadListner : mediaUploadListnerArrayList) {
                onMediaDownloadListner.onMediaDownloadProgress(downloadRequest);
            }
        }
    };
    final Runnable endRunnable = new Runnable() {
        @Override
        public void run() {
            final ArrayList<OnMediaDownloadListner> mediaUploadListnerArrayList =
                    (ArrayList<OnMediaDownloadListner>) ChatApplication.getInstance().getXmppListners().
                            getUiListernsArrayList(OnMediaDownloadListner.class);
            if (downloadRequest.getMessageModal().getMediaType() != MediaType.location) {
                downloadRequest.getMessageModal().setStatus(MessageModal.STATUS_MEDIA_END);
            }
            XmppLogger.printLog(TAG + " end for downloadId " + downloadRequest.getDwonloadId());
            for (OnMediaDownloadListner onMediaDownloadListner : mediaUploadListnerArrayList) {
                onMediaDownloadListner.onMediaDownloadEnd(downloadRequest);
            }
        }
    };
    final Runnable failedRunnable = new Runnable() {
        @Override
        public void run() {
            final ArrayList<OnMediaDownloadListner> mediaUploadListnerArrayList =
                    (ArrayList<OnMediaDownloadListner>) ChatApplication.getInstance().getXmppListners().
                            getUiListernsArrayList(OnMediaDownloadListner.class);
            if (downloadRequest.getMessageModal().getMediaType() != MediaType.location) {
                downloadRequest.getMessageModal().setStatus(MessageModal.STATUS_MEDIA_FAILED);

            }
            downloadRequest.outputFilepath = null;
            XmppLogger.printLog(TAG + " failed for downloadId " + downloadRequest.getDwonloadId());
            for (OnMediaDownloadListner onMediaDownloadListner : mediaUploadListnerArrayList) {
                onMediaDownloadListner.onMediaDownloadEnd(downloadRequest);
            }
        }
    };
    boolean isRunning = false;
    boolean isCancelled = false;


    public DownloadMediaRunnable(DownloadRequest downloadRequest) {
        this.downloadRequest = downloadRequest;
        this.downloadTask = new DownloadTask(this);
    }

    public DownloadRequest getDownloadRequest() {
        return downloadRequest;
    }

    @Override
    public void run() {

        isRunning = true;
        String alreadySavedFilePath = "";
        MessageModal messageModal = downloadRequest.getMessageModal();
        String media_hash = messageModal.getMedia_hash();
        if (XmppUtils.checkVaildString(media_hash)) {
            List<MessageModal> dbDataList = MessageAndCallTable.getInstance().getMessageModalForHashPathNotNull(media_hash);
            if (dbDataList.size() > 0) {
                for (MessageModal messageModal1 : dbDataList) {
                    String filePath = messageModal1.getMedia_name();
                    if (XmppUtils.checkVaildString(filePath)) {
                        File file = new File(filePath);
                        if (file.exists()) {
                            alreadySavedFilePath = filePath;
                            break;
                        }
                    }

                }
                if (XmppUtils.checkVaildString(alreadySavedFilePath)) {
                    try {
                        copyFile(alreadySavedFilePath, downloadRequest.getOutputFilepath());
                        triggerOnMediaDownloadListner(3);
                        return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        triggerOnMediaDownloadListner(1);
        downloadTask.download();

        if (downloadRequest.exception != null) {
            File f = new File(downloadRequest.getOutputFilepath());
            f.delete();
            triggerOnMediaDownloadListner(4);
            return;
        }
        triggerOnMediaDownloadListner(3);

    }

    public void cancelled() {
        downloadRequest.setException(new IOException("task is cancelled."));
        downloadRequest.response = null;
        File f = new File(downloadRequest.getOutputFilepath());
        f.delete();
        triggerOnMediaDownloadListner(4);
    }

    public String copyFile(String fromPath, String toPath) throws IOException {

        InputStream in = null;
        OutputStream out = null;
        File outPutFile = new File(toPath);
        if (!outPutFile.getParentFile().isDirectory()) {
            outPutFile.getParentFile().mkdirs();
        }

        in = new FileInputStream(fromPath);
        out = new FileOutputStream(outPutFile);

        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        in = null;
        out.flush();
        out.close();
        out = null;
        return outPutFile.getAbsolutePath();
    }

    /**
     * @param triggerFor 1=> download start
     *                   2=>download progress
     *                   3=>download completed
     *                   4=>download failed
     */
    public void triggerOnMediaDownloadListner(final int triggerFor) {
        if (triggerFor == 2) {
            ChatApplication.getInstance().removeFromUiThread(progressRunnable);
            ChatApplication.getInstance().runOnUiThread(progressRunnable);
        } else {
            switch (triggerFor) {
                case 1:
                    ChatApplication.getInstance().runOnUiThread(startRunnable);
                    break;
                case 3:
                    ChatApplication.getInstance().runOnUiThread(endRunnable);
                    break;
                case 4:
                    ChatApplication.getInstance().runOnUiThread(failedRunnable);
                    break;
            }
        }

    }
}

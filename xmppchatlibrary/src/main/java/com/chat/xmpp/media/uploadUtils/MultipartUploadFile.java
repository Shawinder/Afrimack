package com.chat.xmpp.media.uploadUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by ubuntu on 15/12/16.
 */

public class MultipartUploadFile {
    private static final String NEW_LINE = "\r\n";

    protected final File file;
    protected final String paramName;
    protected final String fileName;
    protected String contentType;

    public MultipartUploadFile(final String path, final String parameterName) {
        this.file = new File(path);
        this.paramName = parameterName;
        this.fileName = this.file.getName();
    }

    public byte[] getMultipartHeader() throws UnsupportedEncodingException {
        StringBuilder builder = new StringBuilder();

        builder.append("Content-Disposition: form-data; name=\"").append(paramName).append("\"; filename=\"")
                .append(fileName).append("\"").append(NEW_LINE);

//        contentType = "application/octet-stream";
        contentType = "image/jpeg";



        builder.append("Content-Type: ").append(contentType).append(NEW_LINE).append(NEW_LINE);
        return builder.toString().getBytes("US-ASCII");
    }

    public long getTotalMultipartBytes(long boundaryBytesLength) throws UnsupportedEncodingException {
        return boundaryBytesLength + getMultipartHeader().length + file.length();
    }

    public final InputStream getStream() throws FileNotFoundException {
        return new FileInputStream(file);
    }

}

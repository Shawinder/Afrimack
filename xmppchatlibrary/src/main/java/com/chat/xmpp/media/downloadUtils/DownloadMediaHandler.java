package com.chat.xmpp.media.downloadUtils;

import com.chat.ChatApplication;
import com.chat.xmpp.listners.OnMediaDownloadListner;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ubuntu on 17/12/16.
 */

public class DownloadMediaHandler implements OnMediaDownloadListner {

    static DownloadMediaHandler instance;


    static {
        instance = new DownloadMediaHandler();
        ChatApplication.getInstance().getXmppListners().addUiListner(instance);
    }

    LinkedBlockingQueue<Runnable> POOL_QUEUE;
    ExecutorService THREAD_POOL_EXECUTOR;
    private HashMap<String, DownloadMediaRunnable> downloadIds;
    private ThreadFactory sThreadFactory;

    private DownloadMediaHandler() {
        downloadIds = new HashMap<>();
        sThreadFactory = new ThreadFactory() {
            private final AtomicInteger mCount = new AtomicInteger(1);

            public Thread newThread(Runnable r) {

                Thread thread = new Thread(r, "DownloadMediaHandler #" + mCount.getAndIncrement());
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        };
        POOL_QUEUE = new LinkedBlockingQueue<Runnable>();
        THREAD_POOL_EXECUTOR
                = new ThreadPoolExecutor(4, 4, 1,
                TimeUnit.SECONDS, POOL_QUEUE, sThreadFactory);
    }

    public static DownloadMediaHandler getInstance() {
        return instance;
    }

    public boolean submit(DownloadRequest downloadRequest) {
        if (!downloadIds.containsKey(downloadRequest.getDwonloadId())) {
            DownloadMediaRunnable downloadMediaRunnable = new DownloadMediaRunnable(downloadRequest);
            downloadIds.put(downloadRequest.getDwonloadId(), downloadMediaRunnable);
            THREAD_POOL_EXECUTOR.submit(downloadMediaRunnable);
            return true;
        }
        return false;
    }

    public void cancelDownload(String id) {
        if (downloadIds.containsKey(id)) {
            DownloadMediaRunnable downloadMediaRunnable = downloadIds.get(id);
            if (downloadMediaRunnable.isRunning) {
                downloadMediaRunnable.isCancelled = true;
            } else {
                POOL_QUEUE.remove(downloadMediaRunnable);
                downloadMediaRunnable.cancelled();
            }
        }
    }


    @Override
    public void onMediaDownloadStart(DownloadRequest downloadRequest) {

    }

    @Override
    public void onMediaDownloadProgress(DownloadRequest downloadRequest) {

    }

    @Override
    public void onMediaDownloadEnd(DownloadRequest downloadRequest) {
        downloadIds.remove(downloadRequest.getDwonloadId());
    }
}

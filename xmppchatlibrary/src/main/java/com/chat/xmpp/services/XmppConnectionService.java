package com.chat.xmpp.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.chat.ChatApplication;
import com.chat.xmpp.MyXMPP;
import com.chat.xmpp.XmppLogger;

/**
 * Created by ubuntu on 15/11/16.
 */

public class XmppConnectionService extends Service {

    public static final String TAG = "XmppConnectionService";
    private static XmppConnectionService instance;
    private MyXMPP xmpp;
    private ChatApplication myApplication;

    public static XmppConnectionService getInstance() {
        return instance;
    }

    public static Intent createIntent(Context context) {
        return new Intent(context, XmppConnectionService.class);
    }

    public ChatApplication getMyApplication() {
        return myApplication;
    }

    public MyXMPP getXmpp() {
        return xmpp;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        XmppLogger.printLog(TAG + " onBind");
        return new LocalBinder<XmppConnectionService>(this);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        XmppLogger.printLog(TAG + " onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        XmppLogger.printLog(TAG + " onStartCommand");
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        myApplication = (ChatApplication) getApplication();
        XmppLogger.printLog(TAG + " onCreate");
        xmpp = MyXMPP.getInstance();
        myApplication.serviceCreatedDoWork();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        XmppLogger.printLog(TAG + " onDestroy");
        xmpp.disconnect();
        instance = null;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        XmppLogger.printLog(TAG + " onLowMemory");
    }


}

package com.chat.xmpp;

import android.util.Log;

/**
 * Created by ubuntu on 15/11/16.
 */

public class XmppLogger {
    public static final String TAG = "XmppLogger";

    public static void printLog(String msg) {
        Log.e(TAG, msg);
    }
}

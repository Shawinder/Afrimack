package com.chat.xmpp;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by ubuntu on 1/12/16.
 */

public class SingleChatManager {
    public static final String TAG = "SingleChatManager";
    static SingleChatManager instance;

    static {
        instance = new SingleChatManager();
    }

    HashMap<String, String> myBuddiesChat = new HashMap<>();

    private SingleChatManager() {

    }

    public static SingleChatManager getInstance() {
        return instance;
    }

    private String addBuddy(String jid) {
        String thread = UUID.randomUUID().toString();
        myBuddiesChat.put(jid, thread);
        XmppLogger.printLog(TAG + " addBuddy jid=" + jid);
        return thread;
    }

    public String getBuddyChat(String jid) {
        synchronized (myBuddiesChat) {
            if (myBuddiesChat.containsKey(jid)) {
                return myBuddiesChat.get(jid);
            } else {
                return addBuddy(jid);
            }
        }
    }

    public void clearBuddiesList() {
        myBuddiesChat.clear();
        XmppLogger.printLog(TAG + " clearBuddiesList");
    }
}

package com.chat.xmpp.modals;

import com.chat.documentmessages.DocModal;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by linux on 30/1/17.
 */

public class SelectedDocList {

    List<DocModal> docModals = new ArrayList<>();


    public DocModal get(int index) {
        return docModals.get(index);
    }

    public int getSize() {
        return docModals.size();
    }

    public void addDoc(DocModal docModal) {
        docModals.add(docModal);
    }

    public void removeDoc(DocModal docModal) {
        docModals.remove(docModal);
    }

    public void clear() {
        docModals.clear();
    }

    public List<DocModal> getDocModals() {
        return docModals;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public void reset() {
        for (DocModal docModal : docModals) {
            if (docModal.isSelected()) {
                docModal.setSelected(false);
            }
        }
        clear();
    }

}

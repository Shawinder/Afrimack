package com.chat.xmpp.modals;

import com.chat.xmpp.XmppConstant;

/**
 * Created by ubuntu on 15/11/16.
 */

public class XmppAccount {
    String serviceName;
    String host;
    int port = 5222;
    String jid;
    String username;
    String userId;
    String password;
    String resource;
    String countryPhoneCode;
    String name;
    String status = "";
    boolean forLogin = false;
    String avtarPath;
    byte[] vCardAvatar;
    boolean registeredCheck=false;

    public XmppAccount() {

    }

    public XmppAccount(String serviceName, String host, int port,
                       String username, String password, String resource, String countryPhoneCode) {
        this.serviceName = serviceName;
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.resource = resource;
        this.countryPhoneCode = countryPhoneCode;
    }


    public XmppAccount(String username){
        this.serviceName = XmppConstant.serviceName;
        this.host = XmppConstant.host;
        this.port = XmppConstant.port;
        this.username = username;
        this.resource = XmppConstant.defaultResource;
        this.password = XmppConstant.defaultPassword;
        this.countryPhoneCode = XmppConstant.defaultCountryPhoneCode;

    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getCountryPhoneCode() {
        return countryPhoneCode;
    }

    public void setCountryPhoneCode(String countryPhoneCode) {
        this.countryPhoneCode = countryPhoneCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isForLogin() {
        return forLogin;
    }

    public void setForLogin(boolean forLogin) {
        this.forLogin = forLogin;
    }

    public String getJid() {
        return username + "@" + serviceName;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }

    public String getAvtarPath() {
        return avtarPath;
    }

    public void setAvtarPath(String avtarPath) {
        this.avtarPath = avtarPath;
    }

    public byte[] getvCardAvatar() {
        return vCardAvatar;
    }

    public void setvCardAvatar(byte[] vCardAvatar) {
        this.vCardAvatar = vCardAvatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isRegisteredCheck() {
        return registeredCheck;
    }

    public void setRegisteredCheck(boolean registeredCheck) {
        this.registeredCheck = registeredCheck;
    }
}

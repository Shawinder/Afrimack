package com.chat.xmpp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.chat.ChatApplication;


/**
 * Created by ubuntu on 15/11/16.
 */

public class NetworkConnection extends BroadcastReceiver {
    public static final String TAG = "NetworkConnection";
    ChatApplication myApplication;

//    public NetworkConnection(ChatApplication myApplication) {
//        this.myApplication = myApplication;
//    }

    @Override
    public void onReceive(Context context, Intent intent) {
        myApplication = (ChatApplication) context.getApplicationContext();
        myApplication.netWorkConnectivityChange();
    }


}

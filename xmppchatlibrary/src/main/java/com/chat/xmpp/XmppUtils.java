package com.chat.xmpp;

import org.jivesoftware.smack.util.StringUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

/**
 * Created by ubuntu on 16/11/16.
 */

public class XmppUtils {

    public static final int STATIC_MAP_THUMB_ZOOM = 12;
    public static final int STATIC_MAP_NORMAL_ZOOM = 15;
    public static final int[] STATIC_MAP_THUMB_SIZE = new int[]{100, 56};
    public static final int[] STATIC_MAP_FULL_SIZE = new int[]{300, 168};
    static final long ONE_KB = 1024;
    static final long ONE_MB = ONE_KB * 1024;
    static final long ONE_DAY_MILLI = 24 * 60 * 60 * 1000;
    static final String STATIC_MAP_BASE_URL = "https://maps.googleapis.com/maps/api/staticmap?";
    static final String API_KEY = "AIzaSyBYVd5Pkv4w9hXYSjMYaUbdlhA-xqnqeGI";
    static Random r = new Random();
    static String messagedIdData = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz";

    /*Active privacy list name */
    public static final String ACTIVE_PRIVACY_LIST_NAME = "xmpp_privacy_list";

    public static boolean checkVaildString(String data) {
        return data != null && !data.trim().isEmpty();
    }

    public static boolean checkValidObject(Object data) {
        return data != null;
    }


    public static String removePlusfromStart(String data) {
        if (checkVaildString(data)) {
            if (data.length() > 0) {
                if (data.charAt(0) == '+') {
                    data = data.substring(1);
                }
            }
        }
        return data;
    }

    public synchronized static String getJidFromNumber(String number) {
        if (checkVaildString(number)) {
            number = removePlusfromStart(number);
            return number + "@" + XmppConstant.serviceName;
        }
        return null;
    }

    public static synchronized byte[] getFileBytes(File file) throws IOException, IllegalStateException {
        if (!file.exists()) {
            throw new IllegalStateException("file does not exist");
        }
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            int bytes = (int) file.length();
            byte[] buffer = new byte[bytes];
            int readBytes = bis.read(buffer);
            if (readBytes != buffer.length) {
                throw new IOException("Entire file not read");
            }
            return buffer;
        } finally {
            if (bis != null) {
                bis.close();
            }
        }
    }

    public static String generateMessageId(final int length) {
        synchronized (r) {
            Calendar cal = Calendar.getInstance();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++) {
                int pos = r.nextInt(messagedIdData.length());
                char c = messagedIdData.charAt(pos);
                sb.append(c);
            }
            sb.append(cal.getTimeInMillis());
            return sb.toString();
        }
    }

    public synchronized static Calendar getCurrentDayStartCalendar() {
        Calendar d = Calendar.getInstance();
        d.set(Calendar.HOUR_OF_DAY, 0);
        d.set(Calendar.MINUTE, 0);
        d.set(Calendar.SECOND, 0);
        d.set(Calendar.MILLISECOND, 0);
        return d;
    }

    public synchronized static String getTimeString(long timestamp) {
        Calendar d = Calendar.getInstance();
        d.setTimeInMillis(timestamp);
        if (d != null) {
            SimpleDateFormat daytimeformate = new SimpleDateFormat("hh:mm a");
            return daytimeformate.format(d.getTime());
        }
        return "";

    }

    public synchronized static long convertToDateOnlyTime(long timestamp) {
        Calendar d = Calendar.getInstance();
        d.setTimeInMillis(timestamp);
        d.set(Calendar.HOUR_OF_DAY, 0);
        d.set(Calendar.MINUTE, 0);
        d.set(Calendar.SECOND, 0);
        d.set(Calendar.MILLISECOND, 0);
        return d.getTimeInMillis();
    }

    public synchronized static String generateRecentTimeString(long timeStamp) {
        String timeFormate = "";
        Calendar today = getCurrentDayStartCalendar();
        long todayMilli = today.getTimeInMillis();
        long diff = todayMilli - timeStamp;
        if (diff > 0) {
            if (diff <= ONE_DAY_MILLI) {
                return "YESTERDAY";
            } else {
                timeFormate = "MM-dd-yyyy";
            }

        } else {
            if (Math.abs(diff) <= ONE_DAY_MILLI) {
                timeFormate = "hh:mm a";
            } else {
                timeFormate = "MM-dd-yyyy";
            }

        }

        Calendar d = Calendar.getInstance();
        d.setTimeInMillis(timeStamp);

        if (d != null) {
            SimpleDateFormat daytimeformate = new SimpleDateFormat(timeFormate);
            return daytimeformate.format(d.getTime());
        }
        return "";
    }

    public synchronized static String generateChatDateViewString(long timeStamp) {

        String dateFormate = "";
        Calendar today = getCurrentDayStartCalendar();
        long todayMilli = today.getTimeInMillis();
        long diff = todayMilli - timeStamp;
        if (diff > 0) {
            if (diff <= ONE_DAY_MILLI) {
                return "Yesterday";
            } else {
                dateFormate = "MMMM dd, yyyy";
            }

        } else {
            if (Math.abs(diff) <= ONE_DAY_MILLI) {
                return "Today";
            } else {
                dateFormate = "MMMM dd, yyyy";
            }

        }

        Calendar d = Calendar.getInstance();
        d.setTimeInMillis(timeStamp);

        if (d != null) {
            SimpleDateFormat daytimeformate = new SimpleDateFormat(dateFormate);
            return daytimeformate.format(d.getTime());
        }
        return "";
    }


    public synchronized static String generateLastSeenString(long timeStamp) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("last seen ");
        String dateFormate = "";
        Calendar today = getCurrentDayStartCalendar();
        long todayMilli = today.getTimeInMillis();
        long diff = todayMilli - timeStamp;
        if (diff > 0) {
            if (diff <= ONE_DAY_MILLI) {
                stringBuilder.append("yesterday at ");
                dateFormate = "hh:mm a";
            } else {
                dateFormate = "MM-dd-yyyy hh:mm a";
            }

        } else {
            if (Math.abs(diff) <= ONE_DAY_MILLI) {
                stringBuilder.append("today at ");
                dateFormate = "hh:mm a";
            } else {
                dateFormate = "MM-dd-yyyy hh:mm a";
            }

        }

        Calendar d = Calendar.getInstance();
        d.setTimeInMillis(timeStamp);

        if (d != null) {
            SimpleDateFormat daytimeformate = new SimpleDateFormat(dateFormate);
            String time = daytimeformate.format(d.getTime());
            stringBuilder.append(time);
        }
        return stringBuilder.toString();
    }

    public static synchronized String getFileSizeString(long fileSize) {


        double mb = (double) fileSize / ONE_MB;
        if (mb < 0.8d) {
            double kb = (double) fileSize / ONE_KB;
            return new DecimalFormat("##.##").format(kb) + " KB";
        }
        return new DecimalFormat("##.##").format(mb) + " MB";

    }

    public synchronized static String getLocationImageThumbUrl(double lat, double lng) {
        return getLocationImageUrl(lat, lng, "big", STATIC_MAP_THUMB_ZOOM, 1,
                STATIC_MAP_THUMB_SIZE[0], STATIC_MAP_THUMB_SIZE[1]);
    }

    public synchronized static String getLocationImageFullUrl(double lat, double lng) {
        return getLocationImageUrl(lat, lng, "small", STATIC_MAP_NORMAL_ZOOM, 1,
                STATIC_MAP_FULL_SIZE[0], STATIC_MAP_FULL_SIZE[1]);
    }

    public synchronized static String getLocationImageUrl(double latitude, double longitude,
                                                          String markerSize,
                                                          int zoom, int scale,
                                                          int width, int height) {
        StringBuilder builder = new StringBuilder();
        builder.append(STATIC_MAP_BASE_URL);
        builder.append("center=").append(latitude + "," + longitude);
        builder.append("&");
        builder.append("zoom=").append(zoom);
        builder.append("&");
        builder.append("scale=").append(scale);
        builder.append("&");
        builder.append("size=").append(width + "x" + height);
        builder.append("&");
        builder.append("maptype=").append("roadmap");
        builder.append("&");
        builder.append("markers=").append("size:" + markerSize).append("|")
                .append("color:red").append("|").append(latitude + "," + longitude);
        builder.append("&");
        builder.append("key=").append(API_KEY);
        return builder.toString();

    }

    public static String getFileHash(String path) {
        byte[] bytes = getBytes(path);
        if (bytes == null) {
            return null;
        }

        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
        digest.update(bytes);
        return StringUtils.encodeHex(digest.digest());
    }

    public static byte[] getBytes(String path) {
        final File file = new File(path);
        if (file.exists()) {
            try {
                return getFileBytes(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }


}

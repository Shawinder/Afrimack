package com.chat.xmpp;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;


/**
 * Created by ubuntu on 18/1/17.
 */

public class GroupChatInvite implements ExtensionElement {

    public static final String NAMESPACE = "urn:xmpp:groupchat";
    public static final String ELEMENT = "invite";


    private final String roomJid;
    private final String roomSubject;
    private final String roomAvatar;

    public GroupChatInvite(String roomJid, String roomSubject, String roomAvatar) {
        this.roomJid = roomJid;
        this.roomSubject = roomSubject;
        this.roomAvatar = roomAvatar;
    }

    @Deprecated
    public static GroupChatInvite getFrom(Message p) {
        return from(p);
    }

    public static GroupChatInvite from(Message message) {
        return message.getExtension(ELEMENT, NAMESPACE);
    }

    public String getRoomJid() {
        return roomJid;
    }

    public String getRoomSubject() {
        return roomSubject;
    }

    public String getRoomAvatar() {
        return roomAvatar;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public XmlStringBuilder toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        xml.attribute("roomJid", roomJid);
        xml.attribute("roomSubject", roomSubject);
        if (XmppUtils.checkVaildString(roomAvatar)) {
            xml.attribute("roomAvatar", roomAvatar);
        }
        xml.closeEmptyElement();
        return xml;
    }

    public static class Provider extends ExtensionElementProvider<GroupChatInvite> {

        @Override
        public GroupChatInvite parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {

            String roomJid = parser.getAttributeValue("", "roomJid");
            String roomSubject = parser.getAttributeValue("", "roomSubject");
            String roomAvatar = parser.getAttributeValue("", "roomAvatar");
            if (XmppUtils.checkVaildString(roomJid) && XmppUtils.checkVaildString(roomSubject)) {
                return new GroupChatInvite(roomJid, roomSubject, roomAvatar);
            }
            return null;

        }
    }
}

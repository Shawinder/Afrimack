package com.chat.xmpp;

import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.util.Base64;

import com.chat.ChatApplication;
import com.chat.FFmpegCompressionHandler;
import com.chat.appfolders.AudioFolderSent;
import com.chat.appfolders.DocumentFolderSent;
import com.chat.appfolders.ImageFolderSent;
import com.chat.appfolders.VideoFolderSent;
import com.chat.contacts.ContactModal;
import com.chat.contacts.GroupMemberModal;
import com.chat.database.tables.ContactTable;
import com.chat.database.tables.GroupMembers;
import com.chat.database.tables.MessageAndCallTable;
import com.chat.database.tables.RecentTable;
import com.chat.documentmessages.DocModal;
import com.chat.modals.ContactDetailModal;
import com.chat.modals.MediaModel;
import com.chat.modals.MessageModal;
import com.chat.modals.RecentModal;
import com.chat.xmpp.media.MediaMessageExtension;
import com.chat.xmpp.media.MediaThumbGenerator;
import com.chat.xmpp.media.MediaType;
import com.chat.xmpp.media.MediaUtil;
import com.chat.xmpp.modals.SelectedDocList;
import com.chat.xmpp.modals.XmppAccount;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.FromMatchesFilter;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.NotFilter;
import org.jivesoftware.smack.filter.StanzaExtensionFilter;
import org.jivesoftware.smack.filter.StanzaFilter;
import org.jivesoftware.smack.filter.StanzaTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smackx.address.packet.MultipleAddresses;
import org.jivesoftware.smackx.chatstates.packet.ChatStateExtension;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MUCAffiliation;
import org.jivesoftware.smackx.muc.packet.MUCAdmin;
import org.jivesoftware.smackx.muc.packet.MUCInitialPresence;
import org.jivesoftware.smackx.muc.packet.MUCItem;
import org.jivesoftware.smackx.muc.packet.MUCOwner;
import org.jivesoftware.smackx.muc.packet.MUCUser;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.jxmpp.util.XmppStringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ubuntu on 17/1/17.
 */

public class GroupChatManager {
    public static final String TAG = GroupChatManager.class.getSimpleName();
    static final StanzaFilter GROUP_INVITATION_FILTER = new AndFilter(
            MessageTypeFilter.NORMAL,
            new StanzaExtensionFilter(GroupChatInvite.ELEMENT, GroupChatInvite.NAMESPACE),
            new NotFilter(MessageTypeFilter.ERROR));
    static final StanzaFilter GROUP_DISPLAY_MESSAGE_FILTER = new AndFilter(MessageTypeFilter.GROUPCHAT,
            new StanzaExtensionFilter(DisplayReceipt.ELEMENT, DisplayReceipt.NAMESPACE));
    static final StanzaFilter GROUP_DELIVERY_MESSAGE_FILTER = new AndFilter(MessageTypeFilter.GROUPCHAT,
            new StanzaExtensionFilter(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE));
    static final StanzaFilter GROUP_CHAT_MESSAGE_FILTER = new AndFilter(MessageTypeFilter.GROUPCHAT,
            new NotFilter(new StanzaExtensionFilter(null, ChatStateExtension.NAMESPACE)),
            new NotFilter(new StanzaExtensionFilter(null, MediaMessageExtension.NAMESPACE)),
            new NotFilter(new StanzaExtensionFilter(null, DeliveryReceipt.NAMESPACE)));
    static final StanzaFilter GROUP_CHAT_MEDIA_MESSAGE_FILTER = new AndFilter(MessageTypeFilter.GROUPCHAT,
            new StanzaExtensionFilter(MediaMessageExtension.ELEMENT, MediaMessageExtension.NAMESPACE));
    static GroupChatManager instance;

    static {
        instance = new GroupChatManager(MyXMPP.getInstance());
    }

    MyXMPP myXMPP;
    HashMap<String, ContactModal> groupList;
    private StanzaListener groupInvitationListener;
    private StanzaListener deliveryReceiptListener;
    private StanzaListener displayReceiptListener;
    private StanzaListener groupChatMessageListner;
    private StanzaListener groupChatMediaMessageListner;

    /**
     * groupInvitationListener
     */ {
        groupInvitationListener = new StanzaListener() {
            @Override
            public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                XmppLogger.printLog(TAG + " groupInvitationListener=" + packet.toString());
                long timeStamp = Calendar.getInstance().getTimeInMillis();
                long delayTimeStamp = -1;

                Message message = (Message) packet;
                String fromJid = XmppStringUtils.parseBareJid(message.getFrom());
                String toJid = XmppStringUtils.parseBareJid(message.getTo());
                MessageModal messageModal = new MessageModal();
                messageModal.setMessage_id(message.getStanzaId());
                GroupChatInvite groupChatInvite = GroupChatInvite.from(message);
                if (groupChatInvite == null) {
                    return;
                }

                if (message.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                    DelayInformation delayInformation = message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
                    Date realDate = delayInformation.getStamp();
                    if (realDate != null) {
                        delayTimeStamp = realDate.getTime();
                    }
                }
                messageModal.setTo_jid(groupChatInvite.getRoomJid());
                if (delayTimeStamp > 0) {
                    messageModal.setTimestamp(delayTimeStamp);
                } else {
                    messageModal.setTimestamp(timeStamp);
                }
                messageModal.setTimestamp_received(timeStamp);

                if (fromJid.equals(toJid)) {
                    messageModal.setStatus(GroupMembers.MEMBER_INVITE_SENT);
                    if (delayTimeStamp > 0) {
                        messageModal.setTimestamp_send(delayTimeStamp);
                    } else {
                        messageModal.setTimestamp_send(timeStamp);
                    }
                    GroupMembers.getInstance().updateMemInviteSent(messageModal);
                    return;
                } else {

                    List<GroupMemberModal> list = new ArrayList<>();
                    GroupMemberModal groupOwnerModal = new GroupMemberModal();
                    groupOwnerModal.setMem_jid(fromJid);
                    groupOwnerModal.setMem_type(GroupMembers.MEMBER_TYPE_OWNER);
                    groupOwnerModal.setMem_invite(GroupMembers.MEMBER_INVITE_SENT);
                    list.add(groupOwnerModal);
                    {
                        ContactModal contactModal = ContactTable.getInstance().addContactIfNotExist(fromJid);
                        if (contactModal.isNewAdded()) {
                            myXMPP.triggerContactListner(1, contactModal, null, null);
                        }
                    }
                    MultipleAddresses multipleAddresses = (MultipleAddresses) message.getExtension(MultipleAddresses.NAMESPACE);
                    List<MultipleAddresses.Address> addressList = multipleAddresses.getAddressesOfType(MultipleAddresses.Type.to);
                    if (addressList != null && addressList.size() > 0) {
                        for (MultipleAddresses.Address address : addressList) {
                            GroupMemberModal groupMemberModal = new GroupMemberModal();
                            groupMemberModal.setMem_jid(address.getJid());
                            groupMemberModal.setMem_type(GroupMembers.MEMBER_TYPE_MEMBER);
                            groupMemberModal.setMem_invite(GroupMembers.MEMBER_INVITE_SENT);
                            list.add(groupMemberModal);
                            {
                                ContactModal contactModal = ContactTable.getInstance().addContactIfNotExist(address.getJid());
                                if (contactModal.isNewAdded()) {
                                    myXMPP.triggerContactListner(1, contactModal, null, null);
                                }
                            }
                        }
                    }

                    ContactModal groupModal = ContactTable.getInstance().addGroupIfNotExist(groupChatInvite.getRoomJid());
/*
                    if (groupModal.isNewAdded()) {
                        List<GroupMemberModal> memberList = groupModal.getGroupMemberModalList();
                        memberList.clear();
                        memberList.addAll(list);

                        for (GroupMemberModal groupMemberModal : memberList) {
                            GroupMembers.getInstance().write(groupModal.getJid(), groupMemberModal.getMem_jid(),
                                    groupMemberModal.getMem_type(),
                                    groupMemberModal.getMem_invite());
                        }

                        int groupStatus = ContactTable.STATUS_GROUP_SETTLED;
                        ContactTable.getInstance().updateGroupStatus(groupModal.getJid(), groupStatus);
                        byte[] avatar = null;
                        if (XmppUtils.checkVaildString(groupChatInvite.getRoomAvatar())) {
                            avatar = Base64.decode(groupChatInvite.getRoomAvatar(), Base64.DEFAULT);
                        }
                        ContactTable.getInstance().updateGroupSubjectAndAvatar(groupModal.getJid(), groupChatInvite.getRoomSubject(), avatar);
                        groupModal.setDisplayName(groupChatInvite.getRoomSubject());
                        groupModal.setServerDisplayName(groupChatInvite.getRoomSubject());
                        groupModal.setVcardAvatar(avatar);
                        groupModal.setStatus(groupStatus + "");


                        XmppAccount xmppAccount = myXMPP.getXmppAccount();


                        messageModal.setFrom_me(0);
                        messageModal.setMessage_id("create_" + message.getStanzaId());
                        messageModal.setMessage_type(MessageAndCallTable.TYPE_MESSAGE_GROUP_CREATE);
                        messageModal.setStatus(MessageModal.STATUS_DISPLAY_RECEIPT_SENT);
                        messageModal.setData(groupModal.getDisplayName());
                        messageModal.setTo_resource(fromJid);

                        long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(messageModal, false);
                        messageModal.set_id(message_call_table_id);


                        RecentTable.getInstance().writeMessage(messageModal);

                        messageModal.setFrom_me(0);
                        messageModal.setMessage_id("added_" + message.getStanzaId());
                        messageModal.setMessage_type(MessageAndCallTable.TYPE_MESSAGE_ADDED_IN_GROUP);
                        messageModal.setStatus(MessageModal.STATUS_DISPLAY_RECEIPT_SENT);
                        messageModal.setData(xmppAccount.getJid());
                        messageModal.setTo_resource(fromJid);

                        long message_call_table_id1 = MessageAndCallTable.getInstance().writeMessage(messageModal, false);
                        messageModal.set_id(message_call_table_id1);


                        RecentModal recentModal1 = RecentTable.getInstance().writeMessage(messageModal);

                        myXMPP.triggerGroupListner(2, groupModal);
                        myXMPP.triggerOnMessageUpdateListner(1, messageModal);
                        myXMPP.triggerOnRecentUpdate(recentModal1);
                    }
*/
                }
            }
        };
    }

    /**
     * displayReceiptListener
     */ {
        displayReceiptListener = new StanzaListener() {
            @Override
            public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                long timeStamp = Calendar.getInstance().getTimeInMillis();
                long delayTimeStamp = -1;

                XmppLogger.printLog(TAG + " displayReceiptListener=" + packet.toString());

            }
        };
    }

    /**
     * deliveryReceiptListener
     */ {
        deliveryReceiptListener = new StanzaListener() {
            @Override
            public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                long timeStamp = Calendar.getInstance().getTimeInMillis();
                long delayTimeStamp = -1;

                XmppLogger.printLog(TAG + " deliveryReceiptListener=" + packet.toString());

            }
        };
    }

    /**
     * groupChatMessageListner
     */ {
        groupChatMessageListner = new StanzaListener() {
            @Override
            public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                XmppLogger.printLog(TAG + " groupChatMessageListner=" + packet.toString());
                long timeStamp = Calendar.getInstance().getTimeInMillis();
                long delayTimeStamp = -1;

                Message message = (Message) packet;
                String fromJid = XmppStringUtils.parseBareJid(message.getFrom());
                String fromResource = XmppStringUtils.parseResource(message.getFrom());
                String fromResourceJid = fromResource + "@" + XmppConstant.serviceName;
                String toJid = XmppStringUtils.parseBareJid(message.getTo());
                MessageModal messageModal = new MessageModal();
                messageModal.setMessage_id(message.getStanzaId());

                if (message.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                    DelayInformation delayInformation = message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
                    Date realDate = delayInformation.getStamp();
                    if (realDate != null) {
                        delayTimeStamp = realDate.getTime();
                    }
                }

                if (fromResourceJid.equals(toJid)) {
                    messageModal.setTo_jid(fromJid);
                    messageModal.setStatus(MessageModal.STATUS_SENT);
                    if (delayTimeStamp > 0) {
                        messageModal.setTimestamp_send(delayTimeStamp);
                    } else {
                        messageModal.setTimestamp_send(timeStamp);
                    }
                    MessageAndCallTable.getInstance().updateMessageSent(messageModal);
                    myXMPP.triggerOnMessageUpdateListner(2, messageModal);
                    return;
                } else {
                    ContactModal contactModal = ContactTable.getInstance().addContactIfNotExist(fromResourceJid);
                    if (contactModal.isNewAdded()) {
                        myXMPP.triggerContactListner(1, contactModal, null, null);
                    }
                    if (message.getBody() != null) {

                        messageModal.setFrom_me(0);
                        messageModal.setStatus(MessageModal.STATUS_RECEIVED);
                        messageModal.setTo_jid(fromJid);
                        messageModal.setData(message.getBody());
                        messageModal.setTo_resource(fromResourceJid);
                        if (delayTimeStamp > 0) {
                            messageModal.setTimestamp(delayTimeStamp);
                        } else {
                            messageModal.setTimestamp(timeStamp);
                        }
                        messageModal.setTimestamp_received(timeStamp);
                        long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(messageModal, false);
                        messageModal.set_id(message_call_table_id);

                        RecentModal recentModal = RecentTable.getInstance().writeMessage(messageModal);

                        sentDeliveryReceiptForGroupMessage(messageModal, Message.Type.groupchat);

                        myXMPP.triggerOnRecentUpdate(recentModal);
                        myXMPP.triggerOnMessageUpdateListner(1, messageModal);

                    }
                }


            }
        };
    }

    /**
     * groupChatMediaMessageListner
     */ {
        groupChatMediaMessageListner = new StanzaListener() {
            @Override
            public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                XmppLogger.printLog(TAG + " groupChatMediaMessageListner=" + packet.toString());
                long timeStamp = Calendar.getInstance().getTimeInMillis();
                long delayTimeStamp = -1;

                Message message = (Message) packet;
                String fromJid = XmppStringUtils.parseBareJid(message.getFrom());
                String fromResource = XmppStringUtils.parseResource(message.getFrom());
                String fromResourceJid = fromResource + "@" + XmppConstant.serviceName;
                String toJid = XmppStringUtils.parseBareJid(message.getTo());
                MessageModal messageModal = new MessageModal();
                messageModal.setMessage_id(message.getStanzaId());

                MediaMessageExtension mediaMessageExtension;

                if (message.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                    DelayInformation delayInformation = message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
                    Date realDate = delayInformation.getStamp();
                    if (realDate != null) {
                        delayTimeStamp = realDate.getTime();
                    }
                }

                if (fromResourceJid.equals(toJid)) {
                    messageModal.setTo_jid(fromJid);
                    messageModal.setStatus(MessageModal.STATUS_SENT);
                    if (delayTimeStamp > 0) {
                        messageModal.setTimestamp_send(delayTimeStamp);
                    } else {
                        messageModal.setTimestamp_send(timeStamp);
                    }
                    MessageAndCallTable.getInstance().updateMessageSent(messageModal);
                    myXMPP.triggerOnMessageUpdateListner(2, messageModal);
                    return;
                } else {
                    mediaMessageExtension = MediaMessageExtension.from(message);
                    if (mediaMessageExtension == null) {
                        XmppLogger.printLog(TAG + " groupChatMediaMessageListner mediaExtention null");
                        return;
                    }
                    ContactModal contactModal = ContactTable.getInstance().addContactIfNotExist(fromResourceJid);
                    if (contactModal.isNewAdded()) {
                        myXMPP.triggerContactListner(1, contactModal, null, null);
                    }
                    if (message.getBody() != null) {

                        messageModal.setFrom_me(0);
                        messageModal.setStatus(MessageModal.STATUS_RECEIVED);
                        messageModal.setTo_jid(fromJid);
                        messageModal.setTo_resource(fromResourceJid);
                        messageModal.setData(message.getBody());
                        if (delayTimeStamp > 0) {
                            messageModal.setTimestamp(delayTimeStamp);
                        } else {
                            messageModal.setTimestamp(timeStamp);
                        }
                        messageModal.setTimestamp_received(timeStamp);

                        messageModal.setMedia_url(mediaMessageExtension.getMedia_url());
                        messageModal.setMedia_hash(mediaMessageExtension.getMedia_hash());
                        messageModal.setMedia_mime_type(mediaMessageExtension.getMedia_mime_type());
                        messageModal.setMedia_type(mediaMessageExtension.getMedia_type());
                        messageModal.setMedia_name(mediaMessageExtension.getMedia_name());
                        messageModal.setMedia_caption(mediaMessageExtension.getMedia_caption());
                        messageModal.setThumb_image(mediaMessageExtension.getThumb_image());
                        messageModal.setMedia_size(mediaMessageExtension.getMedia_size());
                        messageModal.setMedia_duration(mediaMessageExtension.getMedia_duration());
                        messageModal.setMedia_origin(mediaMessageExtension.getMedia_origin());
                        messageModal.setLatitude(mediaMessageExtension.getLatitude());
                        messageModal.setLongitude(mediaMessageExtension.getLongitude());


                        long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(messageModal, true);
                        messageModal.set_id(message_call_table_id);

                        RecentModal recentModal = RecentTable.getInstance().writeMessage(messageModal);

                        myXMPP.sentDeliveryReceiptForMessage(messageModal, Message.Type.chat);

                        myXMPP.triggerOnRecentUpdate(recentModal);
                        myXMPP.triggerOnMessageUpdateListner(7, messageModal);

                        MediaType mediaType = messageModal.getMediaType();
                        if (mediaType == MediaType.contact) {
                            ContactDetailModal contactDetailModal = messageModal.getContactDetailModal();
                            if (contactDetailModal == null) {
                                return;
                            }
                            for (ContactDetailModal.Detail detail : contactDetailModal.getDetailArrayList()) {
                                if ((detail.getIsNumber() == ContactDetailModal.IS_NUMER_PHONE) && detail.isRegistered()) {
                                    String newJid = XmppUtils.getJidFromNumber(detail.getValue());
                                    if (XmppUtils.checkVaildString(newJid)) {
                                        ContactModal contactModal1 = ContactTable.getInstance().addContactIfNotExist(newJid);
                                        /*if (contactModal1.isNewAdded()) {
                                            myXMPP.triggerContactListner(1, contactModal1, null, null);
                                        }*/
                                    }
                                }
                            }
                        }

                    }
                }


            }
        };
    }

    private GroupChatManager(MyXMPP myXMPP) {
        this.myXMPP = myXMPP;
        groupList = new HashMap<>();
    }

    public static GroupChatManager getInstance() {
        return instance;
    }

    public void addAllGroupListner() {
        this.myXMPP.getXmpptcpConnection().addSyncStanzaListener(groupInvitationListener, GROUP_INVITATION_FILTER);
        this.myXMPP.getXmpptcpConnection().addSyncStanzaListener(deliveryReceiptListener, GROUP_DELIVERY_MESSAGE_FILTER);
        this.myXMPP.getXmpptcpConnection().addSyncStanzaListener(displayReceiptListener, GROUP_DISPLAY_MESSAGE_FILTER);
        this.myXMPP.getXmpptcpConnection().addSyncStanzaListener(groupChatMessageListner, GROUP_CHAT_MESSAGE_FILTER);
        this.myXMPP.getXmpptcpConnection().addSyncStanzaListener(groupChatMediaMessageListner, GROUP_CHAT_MEDIA_MESSAGE_FILTER);
    }

    public void removeAllGroupListner() {
        this.myXMPP.getXmpptcpConnection().removeSyncStanzaListener(groupInvitationListener);
        this.myXMPP.getXmpptcpConnection().removeSyncStanzaListener(deliveryReceiptListener);
        this.myXMPP.getXmpptcpConnection().removeSyncStanzaListener(displayReceiptListener);
        this.myXMPP.getXmpptcpConnection().removeSyncStanzaListener(groupChatMessageListner);
        this.myXMPP.getXmpptcpConnection().removeSyncStanzaListener(groupChatMediaMessageListner);
    }

    public ContactModal CreateNewGroup(String roomJid, String subject, byte[] avatar) {
        ContactModal contactModal = ContactTable.getInstance().addGroupIfNotExist(roomJid);
        if (!contactModal.isNewAdded()) {
            return null;
        }
        int groupStatus = ContactTable.STATUS_GROUP_NOT_CREATE;
        ContactTable.getInstance().updateGroupStatus(roomJid, groupStatus);
        ContactTable.getInstance().updateGroupSubjectAndAvatar(roomJid, subject, avatar);
        contactModal.setDisplayName(subject);
//        contactModal.setServerDisplayName(subject);
        contactModal.setVcardAvatar(avatar);
        contactModal.setStatus(groupStatus + "");
        return contactModal;
    }

/*
    public void addGroupMembers(ContactModal contactModal) {
        String roomJid = contactModal.getJid();
        List<GroupMemberModal> list = contactModal.getGroupMemberModalList();
        for (GroupMemberModal groupMemberModal : list) {
            GroupMembers.getInstance().write(roomJid, groupMemberModal.getMem_jid(),
                    groupMemberModal.getMem_type(),
                    groupMemberModal.getMem_invite());
        }

    }
*/

/*
    public void enterInGroup(ContactModal contactModal, DiscussionHistory history,
                             long timeout) {
        XmppLogger.printLog(TAG + " enterInGroup groupJid=" + contactModal.getJid());
        String room = contactModal.getJid();
        if (!groupList.containsKey(room)) {
            groupList.put(room, contactModal);
        }

        XmppAccount xmppAccount = myXMPP.getXmppAccount();
        String nickname = xmppAccount.getUsername();
        Presence joinPresence = new Presence(Presence.Type.available);
        joinPresence.setTo(room + "/" + nickname);

        MUCInitialPresence mucInitialPresence = new MUCInitialPresence();
        if (history != null) {
            //    mucInitialPresence.setHistory(history.getMUCHistory());
        }
        joinPresence.addExtension(mucInitialPresence);

        StanzaFilter responseFilter = new AndFilter(FromMatchesFilter.createFull(room + "/"
                + nickname), new StanzaTypeFilter(Presence.class));

        Presence presence = null;
        try {
            MyXmppTcpConnection myXmppTcpConnection = myXMPP.getXmpptcpConnection();
            if (myXmppTcpConnection != null) {
                presence = myXmppTcpConnection.createPacketCollectorAndSend(responseFilter, joinPresence).nextResultOrThrow(timeout);
            }

        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            presence = null;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            presence = null;
        }
        if (presence != null) {
            MUCUser mucUser = MUCUser.from(presence);
            if (mucUser != null && mucUser.getStatus().contains(MUCUser.Status.ROOM_CREATED_201)) {
                myXMPP.triggerGroupListner(4, contactModal);
                XmppLogger.printLog(TAG + " enterInGroup success groupJid=" + contactModal.getJid());
                return;
            } else if (mucUser != null && (mucUser.getStatus().contains(MUCUser.Status.create(100)) ||
                    mucUser.getStatus().contains(MUCUser.Status.create(110)))) {
                myXMPP.triggerGroupListner(4, contactModal);
                XmppLogger.printLog(TAG + " enterInGroup success groupJid=" + contactModal.getJid());
                return;
            }
        }
        if (contactModal.getStatus().equals(ContactTable.STATUS_GROUP_NOT_CREATE) ||
                contactModal.getStatus_for_ui().equals(ContactTable.STATUS_GROUP_CREATE_RUNNING)) {
            int groupStatus = ContactTable.STATUS_GROUP_CREATE_FAILED;
            contactModal.setStatus(groupStatus + "");
            ContactTable.getInstance().updateGroupStatus(contactModal.getJid(), groupStatus);
        }
        myXMPP.triggerGroupListner(5, contactModal);
        XmppLogger.printLog(TAG + " enterInGroup failed groupJid=" + contactModal.getJid());

    }
*/

/*
    public void sendConfigrationFormOfGroup(ContactModal contactModal) {
        XmppLogger.printLog(TAG + " sendConfigrationFormOfGroup groupJid=" + contactModal.getJid());

        String roomJid = contactModal.getJid();
        String roomName = roomJid.split("@")[0];
        String subject = contactModal.getDisplayName();
//        List<String> ownerList = contactModal.getGroupOwners();

//        Form configForm = generateGroupConfigForm(roomName, subject, ownerList);

        MUCOwner iq = new MUCOwner();
        iq.setTo(contactModal.getJid());
        iq.setType(IQ.Type.set);
        iq.addExtension(configForm.getDataFormToSend());

        IQ iqAnswer = null;
        try {
            MyXmppTcpConnection myXmppTcpConnection = myXMPP.getXmpptcpConnection();
            if (myXmppTcpConnection != null) {
                iqAnswer = myXmppTcpConnection.createPacketCollectorAndSend(iq).nextResultOrThrow();
                XmppLogger.printLog(TAG + " iqAnswer=" + iqAnswer.toXML());
            }
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            iqAnswer = null;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            iqAnswer = null;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            iqAnswer = null;
        }

        if (iqAnswer != null) {
            if (iqAnswer.getType() == IQ.Type.result) {

                int groupStatus = ContactTable.STATUS_GROUP_CREATED;
                contactModal.setStatus(groupStatus + "");
                ContactTable.getInstance().updateGroupStatus(contactModal.getJid(), groupStatus);

                myXMPP.triggerGroupListner(6, contactModal);
                return;
            }
        }
        if (!contactModal.getStatus().equals(ContactTable.STATUS_GROUP_CREATE_FAILED)) {
            int groupStatus = ContactTable.STATUS_GROUP_CREATE_FAILED;
            contactModal.setStatus(groupStatus + "");
            ContactTable.getInstance().updateGroupStatus(contactModal.getJid(), groupStatus);
        }
        myXMPP.triggerGroupListner(7, contactModal);

    }
*/

/*
    public void addMembersInGroup(ContactModal contactModal) {
        XmppLogger.printLog(TAG + " addMembersInGroup groupJid=" + contactModal.getJid());
        MUCAdmin iq = new MUCAdmin();
        iq.setTo(contactModal.getJid());
        iq.setType(IQ.Type.set);
        Collection<String> jids = contactModal.getGroupMembers();
        for (String jid : jids) {
            MUCItem item = new MUCItem(MUCAffiliation.member, jid);
            iq.addItem(item);
        }

        IQ iqAnswer = null;
        try {
            MyXmppTcpConnection myXmppTcpConnection = myXMPP.getXmpptcpConnection();
            if (myXmppTcpConnection != null) {
                iqAnswer = myXmppTcpConnection.createPacketCollectorAndSend(iq).nextResultOrThrow();
                XmppLogger.printLog(TAG + " iqAnswer=" + iqAnswer.toXML());
            }
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            iqAnswer = null;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            iqAnswer = null;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            iqAnswer = null;
        }

        if (iqAnswer != null) {
            if (iqAnswer.getType() == IQ.Type.result) {
                XmppAccount xmppAccount = myXMPP.getXmppAccount();
                long timeStamp = Calendar.getInstance().getTimeInMillis();
                String stanzaId = XmppUtils.generateMessageId(XmppConstant.defaultMessageIdLength);
                final MessageModal dbModal = new MessageModal();
                dbModal.setTo_jid(contactModal.getJid());
                dbModal.setFrom_me(1);
                dbModal.setMessage_type(MessageAndCallTable.TYPE_MESSAGE_GROUP_CREATE);
                dbModal.setMessage_id(stanzaId);
                dbModal.setStatus(MessageModal.STATUS_DISPLAYED);
                dbModal.setData(contactModal.getDisplayName());
                dbModal.setTimestamp(timeStamp);
                dbModal.setTimestamp_send(timeStamp);
                dbModal.setTo_resource(xmppAccount.getJid());
                dbModal.setRecipient_count(jids.size());
                long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(dbModal, false);
                dbModal.set_id(message_call_table_id);


                RecentModal recentModal = RecentTable.getInstance().writeMessage(dbModal);


                int groupStatus = ContactTable.STATUS_GROUP_SETTLED;
                contactModal.setStatus(groupStatus + "");
                ContactTable.getInstance().updateGroupStatus(contactModal.getJid(), groupStatus);
                myXMPP.triggerOnMessageUpdateListner(1, dbModal);
                myXMPP.triggerOnRecentUpdate(recentModal);
                myXMPP.triggerGroupListner(8, contactModal);
                return;
            }
        }

        myXMPP.triggerGroupListner(9, contactModal);

    }
*/

/*
    public void sendInvitationToGroupMembers(ContactModal contactModal) {
        String stanzaId = XmppUtils.generateMessageId(XmppConstant.defaultMessageIdLength);


        XmppAccount xmppAccount = myXMPP.getXmppAccount();
        MultipleAddresses multipleAddresses = new MultipleAddresses();
        List<String> memberList = contactModal.getNotInvitedGroupMembers();
        if (memberList.size() == 0) {
            return;
        }
        GroupMembers.getInstance().updateMemInviteId(contactModal.getJid(), stanzaId, memberList);
        for (String member : memberList) {
            multipleAddresses.addAddress(MultipleAddresses.Type.to, member, null, null, false, null);
        }
        multipleAddresses.addAddress(MultipleAddresses.Type.replyto, xmppAccount.getJid(), null, null, false, null);

        Message message = new Message();
        message.setTo(XmppConstant.serviceName);
        message.setBody("Invite");
        message.setStanzaId(stanzaId);
        message.setType(Message.Type.normal);
        message.addExtension(multipleAddresses);
        String avatar = contactModal.getVcardAvatar() == null ?
                null : Base64.encodeToString(contactModal.getVcardAvatar(), Base64.DEFAULT);
        GroupChatInvite groupChatInvite = new GroupChatInvite(contactModal.getJid(),
                contactModal.getDisplayName(), avatar);
        message.addExtension(groupChatInvite);

        try {
            MyXmppTcpConnection myXmppTcpConnection = myXMPP.getXmpptcpConnection();
            if (myXmppTcpConnection != null) {
                myXmppTcpConnection.sendStanza(message);
            }
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            XmppLogger.printLog(TAG + " sendInvitationToGroupMembers NotConnectedException");
        }


    }
*/


    private Form generateGroupConfigForm(String roomName, String subject, List<String> owners) {
        Form form = new Form(DataForm.Type.submit);
        {
            FormField newField = new FormField("FORM_TYPE");
            newField.setType(FormField.Type.hidden);
            form.addField(newField);
            List<String> values = new ArrayList<String>();
            values.add("http://jabber.org/protocol/muc#roomconfig");
            form.setAnswer(newField.getVariable(), values);
        }

        {
            FormField newField = new FormField("muc#roomconfig_roomname");
            newField.setType(FormField.Type.text_single);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), roomName);
        }
        {
            FormField newField = new FormField("muc#roomconfig_roomdesc");
            newField.setType(FormField.Type.text_single);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), subject);
        }
        {
            FormField newField = new FormField("muc#roomconfig_changesubject");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), true);
        }
        {
            FormField newField = new FormField("muc#roomconfig_maxusers");
            newField.setType(FormField.Type.list_single);
            form.addField(newField);
            List<String> values = new ArrayList<String>();
            values.add("0");
            form.setAnswer(newField.getVariable(), values);
        }
        {
            FormField newField = new FormField("muc#roomconfig_presencebroadcast");
            newField.setType(FormField.Type.list_multi);
            form.addField(newField);
            List<String> values = new ArrayList<String>();
            values.add("moderator");
            values.add("participant");
            values.add("visitor");
            form.setAnswer(newField.getVariable(), values);
        }
        {
            FormField newField = new FormField("muc#roomconfig_publicroom");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), true);
        }
        {
            FormField newField = new FormField("muc#roomconfig_persistentroom");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), true);
        }
        {
            FormField newField = new FormField("muc#roomconfig_moderatedroom");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), false);
        }
        {
            FormField newField = new FormField("muc#roomconfig_membersonly");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), true);
        }
        {
            FormField newField = new FormField("muc#roomconfig_allowinvites");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), false);
        }
        {
            FormField newField = new FormField("muc#roomconfig_passwordprotectedroom");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), false);
        }
        {
            FormField newField = new FormField("muc#roomconfig_whois");
            newField.setType(FormField.Type.list_single);
            form.addField(newField);
            List<String> values = new ArrayList<String>();
            values.add("anyone");
            form.setAnswer(newField.getVariable(), values);
        }
        {
            FormField newField = new FormField("muc#roomconfig_enablelogging");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), false);
        }
        {
            FormField newField = new FormField("x-muc#roomconfig_reservednick");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), false);
        }
        {
            FormField newField = new FormField("x-muc#roomconfig_canchangenick");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), true);
        }
        {
            FormField newField = new FormField("x-muc#roomconfig_registration");
            newField.setType(FormField.Type.bool);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), true);
        }
        {
            FormField newField = new FormField("muc#roomconfig_roomadmins");
            newField.setType(FormField.Type.jid_multi);
            form.addField(newField);
            List<String> values = new ArrayList<String>();
            form.setAnswer(newField.getVariable(), values);
        }
        {
            FormField newField = new FormField("muc#roomconfig_roomowners");
            newField.setType(FormField.Type.jid_multi);
            form.addField(newField);
            form.setAnswer(newField.getVariable(), owners);
        }
        return form;
    }

    public void sentDeliveryReceiptForGroupMessage(final MessageModal dbMessageModal, final Message.Type type) {
        ChatApplication.getInstance().runInReceiptBackground(new Runnable() {
            @Override
            public void run() {
                String fullJid = dbMessageModal.getTo_jid();
                Message message = new Message(fullJid, type);
                message.addExtension(new DeliveryReceipt(dbMessageModal.getMessage_id()));
                try {
                    MyXmppTcpConnection myXmppTcpConnection = myXMPP.getXmpptcpConnection();
                    if (myXmppTcpConnection != null) {
                        myXmppTcpConnection.sendStanza(message);

                        MessageModal messageModal = new MessageModal();
                        messageModal.setTo_jid(dbMessageModal.getTo_jid());
                        messageModal.setMessage_id(dbMessageModal.getMessage_id());
                        messageModal.setStatus(MessageModal.STATUS_DELIVERY_RECEIPT_SENT);
                        MessageAndCallTable.getInstance().updateSentMessageDelivered(messageModal);
                        myXMPP.triggerOnMessageUpdateListner(5, messageModal);
                    }
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void sentDisplayReceiptForGroupMessage(final MessageModal dbMessageModal, final Message.Type type) {
        ChatApplication.getInstance().runInReceiptBackground(new Runnable() {
            @Override
            public void run() {
                String fullJid = dbMessageModal.getTo_jid();
                Message message = new Message(fullJid, type);
                message.addExtension(new DisplayReceipt(dbMessageModal.getMessage_id()));
                try {
                    MyXmppTcpConnection myXmppTcpConnection = myXMPP.getXmpptcpConnection();
                    if (myXmppTcpConnection != null) {
                        myXmppTcpConnection.sendStanza(message);
                        MessageModal messageModal = new MessageModal();
                        messageModal.setTo_jid(dbMessageModal.getTo_jid());
                        messageModal.setMessage_id(dbMessageModal.getMessage_id());
                        messageModal.setStatus(MessageModal.STATUS_DISPLAY_RECEIPT_SENT);
                        MessageAndCallTable.getInstance().updateSentMessageDisplayed(messageModal);
                        myXMPP.triggerOnMessageUpdateListner(6, messageModal);
                    }
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public void sendTextMessage(ContactModal contactModal, String msg) {
        XmppAccount xmppAccount = myXMPP.getXmppAccount();
        long timeStamp = Calendar.getInstance().getTimeInMillis();
        String stanzaId = XmppUtils.generateMessageId(XmppConstant.defaultMessageIdLength);
        final MessageModal dbModal = new MessageModal();
        dbModal.setTo_jid(contactModal.getJid());
        dbModal.setFrom_me(1);
        dbModal.setMessage_id(stanzaId);
        dbModal.setStatus(MessageModal.STATUS_NOT_SENT);
        dbModal.setData(msg);
        dbModal.setTimestamp(timeStamp);
        dbModal.setTimestamp_send(timeStamp);
        dbModal.setTo_resource(myXMPP.getXmppAccount().getJid());
//        dbModal.setRecipient_count(contactModal.getGroupMemberModalList().size() - 1);
        long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(dbModal, false);
        dbModal.set_id(message_call_table_id);
        myXMPP.triggerOnMessageUpdateListner(1, dbModal);

        RecentModal recentModal = RecentTable.getInstance().writeMessage(dbModal);
        myXMPP.triggerOnRecentUpdate(recentModal);
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {

                String threadID = SingleChatManager.getInstance().getBuddyChat(dbModal.getTo_jid());

                Message message = new Message();
                message.setTo(dbModal.getTo_jid());
                message.setBody(dbModal.getData());
                message.setStanzaId(dbModal.getMessage_id());
                message.setType(Message.Type.groupchat);
                message.setThread(threadID);


                try {
                    MyXmppTcpConnection myXmppTcpConnection = myXMPP.getXmpptcpConnection();
                    if (myXmppTcpConnection != null) {
                        myXmppTcpConnection.sendStanza(message);
                    }
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                    XmppLogger.printLog(TAG + " sendTextMessage NotConnectedException");
                }

            }
        });
    }

    public synchronized void sendAudioMedia(final ContactModal contactModal, final String filePath,
                                            final String caption, final int origin) throws FileNotFoundException {
        if (!XmppUtils.checkVaildString(filePath)) {
            throw new IllegalArgumentException("filepath can't be empty or null");
        }

        try {
            String newPath = AudioFolderSent.getInstance().copyFile(filePath);
            sendMediaMessage(contactModal,
                    MediaType.audio,
                    newPath, caption, -1.0, -1.0, origin, null);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public synchronized void sendContactMedia(final ContactModal contactModal, final String contactDetail, final int origin) {
        if (!XmppUtils.checkVaildString(contactDetail)) {
            return;
        }
        sendMediaMessage(contactModal,
                MediaType.contact,
                contactDetail, null, -1.0, -1.0, origin, null);

    }


    public synchronized void sendImageMedia(final ContactModal contactModal, final String filePath,
                                            final String caption, final int origin) throws FileNotFoundException {
        if (!XmppUtils.checkVaildString(filePath)) {
            throw new IllegalArgumentException("filepath can't be empty or null");
        }

        try {
            String newPath = ImageFolderSent.getInstance().copyFile(filePath);
            sendMediaMessage(contactModal,
                    MediaType.image,
                    newPath, caption, -1.0, -1.0, origin, null);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void sendImageMedia(final ContactModal contactModal, final List<MediaModel> list,
                               final int origin) throws FileNotFoundException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (MediaModel mediaModel : list) {
                    try {
                        sendImageMedia(contactModal, mediaModel.getUrl(), mediaModel.getCaption(), origin);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    public synchronized void sendVideoMedia(final ContactModal contactModal, final MediaModel mediaModel, final int origin) throws FileNotFoundException {
        final String filePath = mediaModel.getUrl();
        final String caption = mediaModel.getCaption();
        if (!XmppUtils.checkVaildString(filePath)) {
            throw new IllegalArgumentException("filepath can't be empty or null");
        }


        String newPath = filePath;
        sendMediaMessage(contactModal,
                MediaType.video,
                newPath, caption, -1.0, -1.0, origin, mediaModel);


    }

    public void sendVideoMedia(final ContactModal contactModal, final List<MediaModel> list,
                               final int origin) throws FileNotFoundException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (MediaModel mediaModel : list) {
                    try {
                        sendVideoMedia(contactModal, mediaModel, origin);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }


    public synchronized void sendDocumentMedia(final ContactModal contactModal,
                                               final DocModal docModal,
                                               final int origin) throws FileNotFoundException {
        String filePath = docModal.getFilePath();
        if (!XmppUtils.checkVaildString(filePath)) {
            throw new IllegalArgumentException("filepath can't be empty or null");
        }

        try {
            String realFileName = DocumentFolderSent.getFilenameWithOutExtensionFromPath(filePath);
            //  String realFileExt = DocumentFolderSent.getFileExtensionFromPath(filePath);
            String newPath = DocumentFolderSent.getInstance().copyFile(filePath);
//            XmppLogger.printLog(TAG + " realFileName=" + realFileName
//                    + "\n" + "realFileExt=" + realFileExt
//                    + "\n" + "newPath=" + newPath);
            sendMediaMessage(contactModal,
                    MediaType.document,
                    newPath, realFileName, -1.0, -1.0, origin, null);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void sendDocumentMedia(final ContactModal contactModal, final SelectedDocList selectedDocList,
                                  final int origin) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (DocModal docModal : selectedDocList.getDocModals()) {
                    try {
                        sendDocumentMedia(contactModal, docModal, origin);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    public synchronized void sendLocationMedia(final ContactModal contactModal, final Location location, final int origin) {
        if (!XmppUtils.checkValidObject(location)) {
            return;
        }
        String caption = null;
        String filePath = null;
        Bundle bundle = location.getExtras();
        if (bundle != null) {
            caption = bundle.getString("address");
            filePath = bundle.getString("thumbPath");
        }
        sendMediaMessage(contactModal,
                MediaType.location,
                filePath, caption, location.getLatitude(), location.getLongitude(), origin, null);

    }


    public void sendMediaMessage(final ContactModal contactModal,
                                 final MediaType mediaType, final String filePath,
                                 final String caption, final double latitude,
                                 final double longitude, final int origin,
                                 final MediaModel mediaModel) {


        try {
            if (filePath == null) {
                return;
            }
            long fileSize = 0;
            String mimeType = null;
            long duration = 0;
            String thumbImage = null;
            if (mediaType != MediaType.contact &&
                    mediaType != MediaType.link &&
                    mediaType != MediaType.location) {
                fileSize = MediaUtil.getfileSize(filePath);
                mimeType = MediaUtil.getMimeType(filePath);
            }
            if (mediaType == MediaType.contact) {
                mimeType = "contact/vcard";
            } else if (mediaType == MediaType.location) {
                mimeType = "map/location";
            }
            if (mediaType == MediaType.image) {
                thumbImage = MediaThumbGenerator.getThumbBase64(filePath);
            } else if (mediaType == MediaType.video) {
                thumbImage = MediaThumbGenerator.getVideoThumbBase64(filePath);
            } else if (mediaType == MediaType.location) {
                thumbImage = MediaThumbGenerator.getLocationThumbBase64(filePath);
                new File(filePath).delete();
            }

            long timeStamp = Calendar.getInstance().getTimeInMillis();
            String stanzaId = XmppUtils.generateMessageId(XmppConstant.defaultMessageIdLength);
            MessageModal dbModal = new MessageModal();
            dbModal.setTo_jid(contactModal.getJid());
            dbModal.setFrom_me(1);
            dbModal.setMessage_id(stanzaId);
            dbModal.setStatus(MessageModal.STATUS_NOT_SENT);
            dbModal.setData("media");
            dbModal.setTimestamp(timeStamp);
            dbModal.setTimestamp_send(timeStamp);
            dbModal.setTo_resource(myXMPP.getXmppAccount().getJid());
//            dbModal.setRecipient_count(contactModal.getGroupMemberModalList().size() - 1);

            dbModal.setMedia_type(mediaType);
            if (mediaType == MediaType.link ||
                    mediaType == MediaType.contact) {
                dbModal.setMedia_url(filePath);
            } else {
                dbModal.setMedia_name(filePath);
            }
            dbModal.setMedia_caption(caption);
            dbModal.setLatitude(latitude);
            dbModal.setLongitude(longitude);
            dbModal.setMedia_origin(origin);
            dbModal.setMedia_size(fileSize);
            dbModal.setMedia_mime_type(mimeType);
            dbModal.setMedia_duration(duration);
            dbModal.setThumb_image(thumbImage);
           /* if (mediaType == MediaType.video) {
                String outFile = VideoFolderSent.getInstance().getNewFilePath();
                String[] finalCmd = new String[mediaModel.getVideoFfmpegCmd().length + 1];
                int i = 0;
                for (String data : mediaModel.getVideoFfmpegCmd()) {
                    finalCmd[i] = data;
                    i++;
                }
                finalCmd[i] = outFile;
                dbModal.setVideoCmddata(finalCmd);
            }*/

            if (mediaType != MediaType.link &&
                    mediaType != MediaType.contact &&
                    mediaType != MediaType.location &&
                    mediaType != MediaType.video) {
                String fileHash = XmppUtils.getFileHash(filePath);
                if (fileHash == null) {
                    new File(filePath).delete();
                    return;
                }
                MessageModal hashModal = MessageAndCallTable.getInstance().getMessageModalForHashUrlNotNull(fileHash);
                if (hashModal != null) {
                    String oldFileUrl = hashModal.getMedia_url();
                    if (XmppUtils.checkVaildString(oldFileUrl)) {
                        dbModal.setMedia_url(oldFileUrl);
                    }
                }
                dbModal.setMedia_hash(fileHash);
            }
            long message_call_table_id = MessageAndCallTable.getInstance().
                    writeMessage(dbModal, true);
            dbModal.set_id(message_call_table_id);
            myXMPP.triggerOnMessageUpdateListner(7, dbModal);

            RecentModal recentModal = RecentTable.getInstance().writeMessage(dbModal);
            myXMPP.triggerOnRecentUpdate(recentModal);
            if (mediaType == MediaType.contact || mediaType == MediaType.location) {
                sendMediaMessageToXmpp(dbModal);
            } else {
                if (mediaType == MediaType.video) {
                    FFmpegCompressionHandler.getInstance().addForCompression(dbModal);
                } else if (XmppUtils.checkVaildString(dbModal.getMedia_url())) {
                    sendMediaMessageToXmpp(dbModal);
                } else {
                    myXMPP.uploadMediaToServer(dbModal);
                }

            }

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void retryVideoMediaMessage(MessageModal messageModal) {
        String[] cmd = messageModal.getVideoCmddata();
        if (cmd == null) {
            String fileHash = messageModal.getMedia_hash();
            if (fileHash == null) {
                return;
            }
            MessageModal hashModal = MessageAndCallTable.getInstance().getMessageModalForHashUrlNotNull(fileHash);
            if (hashModal != null) {
                String oldFileUrl = hashModal.getMedia_url();
                if (XmppUtils.checkVaildString(oldFileUrl)) {
                    messageModal.setMedia_url(oldFileUrl);
                }
            }
            messageModal.setMedia_hash(fileHash);

            if (XmppUtils.checkVaildString(messageModal.getMedia_url())) {
                messageModal.setStatus(MessageModal.STATUS_MEDIA_END);
                MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
                sendMediaMessageToXmpp(messageModal);
            } else {
                myXMPP.uploadMediaToServer(messageModal);
            }

        } else {
            FFmpegCompressionHandler.getInstance().addForCompression(messageModal);
        }

    }

    public void retryMediaMessage(MessageModal messageModal) {
        MediaType mediaType = messageModal.getMediaType();
        if (mediaType == MediaType.video) {
            retryVideoMediaMessage(messageModal);
            return;
        }
        if (mediaType != MediaType.link &&
                mediaType != MediaType.contact &&
                mediaType != MediaType.location) {
            String fileHash = messageModal.getMedia_hash();
            if (fileHash == null) {
                return;
            }
            MessageModal hashModal = MessageAndCallTable.getInstance().getMessageModalForHashUrlNotNull(fileHash);
            if (hashModal != null) {

                String oldFileUrl = hashModal.getMedia_url();

                if (XmppUtils.checkVaildString(oldFileUrl)) {
                    messageModal.setMedia_url(oldFileUrl);
                }
            }
        }
        if (XmppUtils.checkVaildString(messageModal.getMedia_url())) {
            messageModal.setStatus(MessageModal.STATUS_MEDIA_END);
            MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
            sendMediaMessageToXmpp(messageModal);
        } else {
            myXMPP.uploadMediaToServer(messageModal);
        }


    }

    public void sendMediaMessageToXmpp(final MessageModal dbModal) {
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {

                String threadID = SingleChatManager.getInstance().getBuddyChat(dbModal.getTo_jid());
                Message message = new Message();
                message.setTo(dbModal.getTo_jid());
                message.setBody(dbModal.getData());
                message.setStanzaId(dbModal.getMessage_id());
                message.setType(Message.Type.groupchat);
                message.setThread(threadID);
                MediaMessageExtension mediaMessageExtension = new MediaMessageExtension();

                mediaMessageExtension.setMedia_url(dbModal.getMedia_url());
                mediaMessageExtension.setMedia_hash(dbModal.getMedia_hash());
                mediaMessageExtension.setMedia_mime_type(dbModal.getMedia_mime_type());
                mediaMessageExtension.setMedia_type(dbModal.getMedia_type());
                mediaMessageExtension.setMedia_caption(dbModal.getMedia_caption());
                mediaMessageExtension.setThumb_image(dbModal.getThumb_image());
                mediaMessageExtension.setMedia_size(dbModal.getMedia_size());
                mediaMessageExtension.setMedia_duration(dbModal.getMedia_duration());
                mediaMessageExtension.setMedia_origin(dbModal.getMedia_origin());
                mediaMessageExtension.setLatitude(dbModal.getLatitude());
                mediaMessageExtension.setLongitude(dbModal.getLongitude());
                message.addExtension(mediaMessageExtension);
                try {
                    MyXmppTcpConnection myXmppTcpConnection = myXMPP.getXmpptcpConnection();
                    if (myXmppTcpConnection != null) {

                        myXmppTcpConnection.sendStanza(message);
                    }
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                    XmppLogger.printLog(TAG + " sendMediaMessageToXmpp NotConnectedException");
                }
            }
        });
    }


}

package com.chat.xmpp;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by ubuntu on 13/12/16.
 */

public class UserLastActivity extends IQ {

    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = "jabber:iq:last";

    public long lastActivity = -1;
    public long lastlogout = -1;
    public String message;

    public UserLastActivity() {
        super(ELEMENT, NAMESPACE);
        setType(IQ.Type.get);
    }

    public UserLastActivity(String to) {
        this();
        setTo(to);
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.optLongAttribute("seconds", lastActivity);
        xml.optLongAttribute("lastlogout", lastlogout);

        // We don't support adding the optional message attribute, because it is usually only added
        // by XMPP servers and not by client entities.
        xml.setEmptyElement();
        return xml;
    }


    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }

    private void setMessage(String message) {
        this.message = message;
    }

    /**
     * Returns number of seconds that have passed since the user last logged out.
     * If the user is offline, 0 will be returned.
     *
     * @return the number of seconds that have passed since the user last logged out.
     */
    public long getIdleTime() {
        return lastActivity;
    }

    public long getLastlogout() {
        return lastlogout;
    }

    public void setLastlogout(long lastlogout) {
        this.lastlogout = lastlogout;
    }

    /**
     * Returns the status message of the last unavailable presence received from the user.
     *
     * @return the status message of the last unavailable presence received from the user
     */
    public String getStatusMessage() {
        return message;
    }


    /**
     * The IQ Provider for LastActivity.
     *
     * @author Derek DeMoro
     */
    public static class Provider extends IQProvider<UserLastActivity> {

        @Override
        public UserLastActivity parse(XmlPullParser parser, int initialDepth) throws SmackException, XmlPullParserException {
            UserLastActivity lastActivity = new UserLastActivity();
            String seconds = parser.getAttributeValue("", "seconds");
            if (seconds != null) {
                try {
                    lastActivity.setLastActivity(Long.parseLong(seconds));
                } catch (NumberFormatException e) {
                    throw new SmackException("Could not parse last activity number", e);
                }
            }
            String lastlogout = parser.getAttributeValue("", "lastlogout");
            if (lastlogout != null) {
                try {
                    lastActivity.setLastlogout(Long.parseLong(lastlogout));
                } catch (NumberFormatException e) {
                    throw new SmackException("Could not parse last lastlogout number", e);
                }
            }
            try {
                lastActivity.setMessage(parser.nextText());
            } catch (IOException e) {
                throw new SmackException(e);
            }
            return lastActivity;
        }
    }
}

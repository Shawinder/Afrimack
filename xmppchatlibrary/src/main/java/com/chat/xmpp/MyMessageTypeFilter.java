package com.chat.xmpp;

import org.jivesoftware.smack.filter.FlexibleStanzaTypeFilter;
import org.jivesoftware.smack.packet.Message;

/**
 * Created by linux on 28/2/17.
 */

public class MyMessageTypeFilter extends FlexibleStanzaTypeFilter<Message> {

    private final Message.Type type;

    /**
     * Creates a new message type filter using the specified message type.
     *
     * @param type the message type.
     */
    public MyMessageTypeFilter(Message.Type type) {
        super(Message.class);
        this.type = type;
    }

    @Override
    protected boolean acceptSpecific(Message message) {
        return message.getType() == type;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": type=" + type;
    }
}

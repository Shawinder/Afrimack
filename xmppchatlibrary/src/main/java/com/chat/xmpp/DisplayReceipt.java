package com.chat.xmpp;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.provider.EmbeddedExtensionProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;

import java.util.List;
import java.util.Map;

/**
 * Created by ubuntu on 6/12/16.
 */

public class DisplayReceipt implements ExtensionElement {
    public static final String NAMESPACE = "urn:xmpp:receipts";
    public static final String ELEMENT = "display";

    /**
     * original ID of the display message
     */
    private final String id;

    public DisplayReceipt(String id) {
        this.id = id;
    }

    @Deprecated
    public static DisplayReceipt getFrom(Message p) {
        return from(p);
    }

    public static DisplayReceipt from(Message message) {
        return message.getExtension(ELEMENT, NAMESPACE);
    }

    public String getId() {
        return id;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public XmlStringBuilder toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        xml.attribute("id", id);
        xml.closeEmptyElement();
        return xml;
    }

    /**
     * This Provider parses and returns DisplayReceipt packets.
     */
    public static class Provider extends EmbeddedExtensionProvider<DisplayReceipt> {

        @Override
        protected DisplayReceipt createReturnExtension(String currentElement, String currentNamespace,
                                                       Map<String, String> attributeMap, List<? extends ExtensionElement> content) {
            return new DisplayReceipt(attributeMap.get("id"));
        }

    }
}

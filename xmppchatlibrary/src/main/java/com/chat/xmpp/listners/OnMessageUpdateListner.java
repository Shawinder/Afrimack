package com.chat.xmpp.listners;

import com.chat.modals.MessageModal;

/**
 * Created by ubuntu on 2/12/16.
 */

public interface OnMessageUpdateListner extends UiListner {

    void onMessageAdded(MessageModal messageModal);

    void onMediaMessageAdded(MessageModal messageModal);

    void onMessageSent(MessageModal messageModal);

    void onMessageDelivered(MessageModal messageModal);

    void onMesssageDisplayed(MessageModal messageModal);

    void onMesssageDeliverySent(MessageModal messageModal);

    void onMesssageDisplaySent(MessageModal messageModal);
}

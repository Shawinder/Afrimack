package com.chat.xmpp.listners;

import org.jivesoftware.smackx.vcardtemp.packet.VCard;

/**
 * Created by ubuntu on 29/11/16.
 */

public interface OnVcardLoadListner extends UiListner {
    void vcardLoad(String jid, VCard vCard, Exception e);

}

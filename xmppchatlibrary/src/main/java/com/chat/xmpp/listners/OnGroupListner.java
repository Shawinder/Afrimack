package com.chat.xmpp.listners;

import com.chat.contacts.ContactModal;

/**
 * Created by ubuntu on 18/1/17.
 */

public interface OnGroupListner extends UiListner {

    void newGroupCreated(ContactModal contactModal);

    void groupMemberAdded(ContactModal contactModal);

    void newGroupCreationError(ContactModal contactModal);

    void onEnterInGroup(ContactModal contactModal, boolean isSuccess);

    void onGroupConfigPush(ContactModal contactModal, boolean isSuccess);

    void onGroupMemberAdded(ContactModal contactModal, boolean isSuccess);
}

package com.chat.xmpp.listners;

/**
 * Created by linux on 4/3/17.
 */

public interface OnXmppDeleteAccountListner extends UiListner {
    void deleteXmppAccountStatus(int status);
}

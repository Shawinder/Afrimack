package com.chat.xmpp.listners;

import android.graphics.Bitmap;

/**
 * Created by ubuntu on 17/12/16.
 */

public interface OnBitmapLoadedListner extends UiListner {

    void bitmapLoadedStart(String id);

    void bitmapLoadedEnd(String toJid, String id, Bitmap bitmap);
}

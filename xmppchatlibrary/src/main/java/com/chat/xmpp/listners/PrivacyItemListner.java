package com.chat.xmpp.listners;

/**
 * Created by linux on 4/3/17.
 */

public interface PrivacyItemListner extends UiListner {

    void blockPrivacyUser(String jid, boolean status);

    void unBlockPrivacyuser(String jid, boolean status);

}

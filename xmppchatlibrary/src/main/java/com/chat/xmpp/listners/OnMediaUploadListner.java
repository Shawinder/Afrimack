package com.chat.xmpp.listners;

import com.chat.xmpp.media.uploadUtils.UploadRequest;

/**
 * Created by ubuntu on 15/12/16.
 */

public interface OnMediaUploadListner extends UiListner {

    void onMediaUploadStart(UploadRequest uploadRequest);

    void onMediaUploadProgress(UploadRequest uploadRequest);

    void onMediaUploadEnd(UploadRequest uploadRequest);
}

package com.chat.xmpp.listners;

import com.chat.xmpp.media.downloadUtils.DownloadRequest;

/**
 * Created by ubuntu on 17/12/16.
 */

public interface OnMediaDownloadListner extends UiListner {
    void onMediaDownloadStart(DownloadRequest downloadRequest);

    void onMediaDownloadProgress(DownloadRequest downloadRequest);

    void onMediaDownloadEnd(DownloadRequest downloadRequest);

}

package com.chat.xmpp.listners;

import org.jivesoftware.smackx.vcardtemp.packet.VCard;

/**
 * Created by ubuntu on 5/12/16.
 */

public interface OnMyVcardLoadListner extends UiListner {
    void onLoadMyVcard(VCard vCard, Exception e);
}

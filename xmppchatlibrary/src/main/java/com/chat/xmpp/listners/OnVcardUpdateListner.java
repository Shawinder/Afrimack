package com.chat.xmpp.listners;

import com.chat.xmpp.modals.XmppAccount;

import org.jivesoftware.smackx.vcardtemp.packet.VCard;

/**
 * Created by ubuntu on 25/11/16.
 */

public interface OnVcardUpdateListner extends UiListner {
    void vcardUpdateSuccess(XmppAccount xmppAccount, VCard vCard, Exception e);
}

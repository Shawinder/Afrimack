package com.chat.xmpp.listners;

import com.chat.xmpp.modals.XmppAccount;

/**
 * Created by ubuntu on 25/11/16.
 */

public interface OnUserRegisterListner extends UiListner {
    void xmppUserRegistered(XmppAccount xmppAccount, Exception e);

}

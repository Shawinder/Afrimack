package com.chat.xmpp.listners;

/**
 * Created by ubuntu on 16/11/16.
 */

public interface MyXmppConnectionListner extends UiListner {

    void xmppConnected();

    void xmppDisconnected();

    void xmppLogedIn();

    void xmppConnectError(Exception e);

    void xmppLoginInError(Exception e);


}

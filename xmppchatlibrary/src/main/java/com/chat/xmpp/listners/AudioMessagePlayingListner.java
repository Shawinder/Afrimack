package com.chat.xmpp.listners;

/**
 * Created by ubuntu on 22/12/16.
 */

public interface AudioMessagePlayingListner extends UiListner {

    void startPlay(String toJid, String messageId);

    void pausePlay(String toJid, String messageId);

    void progressPlay(String toJid, String messageId);

    void stopPlay(String toJid, String messageId);
}

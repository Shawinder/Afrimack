package com.chat.xmpp.listners;

import com.chat.contacts.ContactModal;

import java.util.List;

/**
 * Created by ubuntu on 3/12/16.
 */

public interface OnContactAddUpdateListner extends UiListner {

    void onNewContactAdded(ContactModal contactModal);

    void onContactUpdate(ContactModal contactModal);

    void onNewContactsAdded(List<ContactModal> contactModalList);

    void onContactsUpdated(List<ContactModal> contactModalList);

}

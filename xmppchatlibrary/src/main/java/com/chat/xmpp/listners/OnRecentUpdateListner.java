package com.chat.xmpp.listners;

import com.chat.modals.RecentModal;

/**
 * Created by ubuntu on 3/12/16.
 */

public interface OnRecentUpdateListner extends OnMessageUpdateListner {

    void onNewRecentAdded(RecentModal recentModal);

    void onRecentUpdate(RecentModal recentModal);
}

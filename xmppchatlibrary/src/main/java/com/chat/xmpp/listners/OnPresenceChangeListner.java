package com.chat.xmpp.listners;

import org.jivesoftware.smack.packet.Presence;

/**
 * Created by ubuntu on 13/12/16.
 */

public interface OnPresenceChangeListner extends UiListner {

    void onPresenceChanged(String jid, Presence.Type type, long lastSeen);
}

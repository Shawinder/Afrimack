package com.chat.xmpp.listners;

import org.jivesoftware.smackx.chatstates.ChatState;

/**
 * Created by ubuntu on 7/12/16.
 */

public interface OnChatStateChangeListner extends UiListner {

    void onChatStateChange(String jid, String fromResourceJid, ChatState chatState);
}

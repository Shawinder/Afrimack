package com.chat.xmpp.listners;

import com.chat.modals.MessageModal;

/**
 * Created by ubuntu on 7/1/17.
 */

public interface VideoCompressionListner extends UiListner {

    void videoCompressStart(MessageModal messageModal);

    void videoCompressProgress(MessageModal messageModal);

    void videoCompressEnd(MessageModal messageModal, boolean success);
}

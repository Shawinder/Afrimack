package com.chat.xmpp;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.provider.EmbeddedExtensionProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;

import java.util.List;
import java.util.Map;


/**
 * Created by linux on 21/2/17.
 */

public class UserDetailsExtension implements ExtensionElement {

    public static final String NAMESPACE = "urn:xmpp:userdetails";
    public static final String ELEMENT = "userdetails";

    /**
     * original ID of the display message
     */
    private final String id;
    private final String name;
    private final String profile_pic;

    public UserDetailsExtension(String id, String name, String profile_pic) {
        this.id = id;
        this.name = name;
        this.profile_pic = profile_pic;
    }

    @Deprecated
    public static UserDetailsExtension getFrom(Message p) {
        return from(p);
    }

    public static UserDetailsExtension from(Message message) {
        return message.getExtension(ELEMENT, NAMESPACE);
    }

    public String getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public XmlStringBuilder toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        xml.attribute("id", id);
        xml.attribute("name", name);
        xml.attribute("profile_pic", profile_pic);
        xml.closeEmptyElement();
        return xml;
    }

    /**
     * This Provider parses and returns DisplayReceipt packets.
     */
    public static class Provider extends EmbeddedExtensionProvider<UserDetailsExtension> {

        @Override
        protected UserDetailsExtension createReturnExtension(String currentElement, String currentNamespace,
                                                             Map<String, String> attributeMap, List<? extends ExtensionElement> content) {
            return new UserDetailsExtension(attributeMap.get("id"), attributeMap.get("name"), attributeMap.get("profile_pic"));
        }

    }
}

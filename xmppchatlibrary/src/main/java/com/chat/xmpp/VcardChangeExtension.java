package com.chat.xmpp;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.provider.EmbeddedExtensionProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;

import java.util.List;
import java.util.Map;

/**
 * Created by ubuntu on 13/12/16.
 */

public class VcardChangeExtension implements ExtensionElement {
    public static final String NAMESPACE = "urn:chat:vcard";
    public static final String ELEMENT = "change";

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public String getNamespace() {
        return NAMESPACE;
    }

    @Override
    public XmlStringBuilder toXML() {
        XmlStringBuilder xml = new XmlStringBuilder(this);
        xml.closeEmptyElement();
        return xml;
    }

    public static class Provider extends EmbeddedExtensionProvider<VcardChangeExtension> {

        @Override
        protected VcardChangeExtension createReturnExtension(String currentElement, String currentNamespace,
                                                             Map<String, String> attributeMap, List<? extends ExtensionElement> content) {
            return new VcardChangeExtension();
        }

    }

}

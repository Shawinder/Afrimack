package com.chat.xmpp;

import com.chat.database.listners.OnMigrationListener;
import com.chat.xmpp.listners.OnClearListener;
import com.chat.xmpp.listners.OnLoadListner;
import com.chat.xmpp.listners.UiListner;

import java.util.ArrayList;

/**
 * Created by ubuntu on 16/11/16.
 */

public class XmppListners {

    private static XmppListners instance;

    static {
        instance = new XmppListners();
    }

    private ArrayList<OnLoadListner> onLoadListnerArrayList;
    private ArrayList<OnClearListener> onClearListenerArrayList;
    private ArrayList<OnMigrationListener> onMigrationListenerArrayList;
    private ArrayList<UiListner> uiListnerArrayList;

    private XmppListners() {
        onLoadListnerArrayList = new ArrayList<>();
        onClearListenerArrayList = new ArrayList<>();
        onMigrationListenerArrayList = new ArrayList<>();
    }

    public static XmppListners getInstance() {
        return instance;
    }


    public ArrayList<OnLoadListner> getOnLoadListnerArrayList() {
        return onLoadListnerArrayList;
    }

    public ArrayList<OnClearListener> getOnClearListenerArrayList() {
        return onClearListenerArrayList;
    }

    public ArrayList<OnMigrationListener> getOnMigrationListenerArrayList() {
        return onMigrationListenerArrayList;
    }


    public void addOnLoadListner(OnLoadListner onLoadListner) {
        onLoadListnerArrayList.add(onLoadListner);
    }

    public void removeOnLoadListner(OnLoadListner onLoadListner) {
        onLoadListnerArrayList.remove(onLoadListner);
    }

    public void clearOnLoadListner() {
        onLoadListnerArrayList.clear();
    }

    public void addOnClearListner(OnClearListener onClearListener) {
        onClearListenerArrayList.add(onClearListener);
    }

    public void removeClearListner(OnClearListener onClearListener) {
        onClearListenerArrayList.remove(onClearListener);
    }

    public void clearOnClearListner() {
        onClearListenerArrayList.clear();
    }

    public void addOnMigrationListner(OnMigrationListener onMigrationListener) {
        onMigrationListenerArrayList.add(onMigrationListener);
    }

    public void removeOnMigrationListner(OnMigrationListener onMigrationListener) {
        onMigrationListenerArrayList.remove(onMigrationListener);
    }

    public void clearOnMigrationListner() {
        onMigrationListenerArrayList.clear();
    }


    public ArrayList<? extends UiListner> getUiListernsArrayList(Class<? extends UiListner> name) {
        ArrayList<UiListner> newList = new ArrayList<>();
        ArrayList<UiListner> uiListners = uiListnerArrayList;
        if (uiListners != null) {
            for (UiListner uiListner : uiListners) {
                if (name.isInstance(uiListner)) {
                    newList.add(uiListner);
                }
            }
        }

        return newList;
    }

    public void addUiListner(UiListner uiListner) {
        if (uiListnerArrayList == null) {
            uiListnerArrayList = new ArrayList<>();
        }
        uiListnerArrayList.add(uiListner);

    }

    public void removeUiListner(UiListner uiListner) {
        if (uiListnerArrayList != null && uiListnerArrayList.size() > 0) {
            uiListnerArrayList.remove(uiListner);
        }
    }


}

package com.chat.xmpp;

/**
 * Created by ubuntu on 15/11/16.
 */

import android.content.res.Resources.NotFoundException;
import android.location.Location;
import android.os.Bundle;

import com.chat.ChatApplication;
import com.chat.FFmpegCompressionHandler;
import com.chat.appfolders.AudioFolderSent;
import com.chat.appfolders.DocumentFolderSent;
import com.chat.appfolders.FoldersHandler;
import com.chat.appfolders.ImageFolderSent;
import com.chat.appfolders.InternalAppFolder;
import com.chat.contacts.ContactHelper;
import com.chat.contacts.ContactModal;
import com.chat.database.tables.ContactTable;
import com.chat.database.tables.MessageAndCallTable;
import com.chat.database.tables.RecentTable;
import com.chat.documentmessages.DocModal;
import com.chat.modals.ContactDetailModal;
import com.chat.modals.MediaModel;
import com.chat.modals.MessageModal;
import com.chat.modals.RecentModal;
import com.chat.xmpp.listners.MyXmppConnectionListner;
import com.chat.xmpp.listners.OnChatStateChangeListner;
import com.chat.xmpp.listners.OnContactAddUpdateListner;
import com.chat.xmpp.listners.OnGroupListner;
import com.chat.xmpp.listners.OnMessageUpdateListner;
import com.chat.xmpp.listners.OnMyVcardLoadListner;
import com.chat.xmpp.listners.OnPresenceChangeListner;
import com.chat.xmpp.listners.OnRecentUpdateListner;
import com.chat.xmpp.listners.OnUserRegisterListner;
import com.chat.xmpp.listners.OnVcardLoadListner;
import com.chat.xmpp.listners.OnVcardUpdateListner;
import com.chat.xmpp.listners.OnXmppDeleteAccountListner;
import com.chat.xmpp.listners.PrivacyItemListner;
import com.chat.xmpp.listners.UiListner;
import com.chat.xmpp.media.MediaMessageExtension;
import com.chat.xmpp.media.MediaThumbGenerator;
import com.chat.xmpp.media.MediaType;
import com.chat.xmpp.media.MediaUtil;
import com.chat.xmpp.media.downloadUtils.DownloadMediaHandler;
import com.chat.xmpp.media.downloadUtils.DownloadRequest;
import com.chat.xmpp.media.uploadUtils.UploadMediaHandler;
import com.chat.xmpp.media.uploadUtils.UploadRequest;
import com.chat.xmpp.modals.SelectedDocList;
import com.chat.xmpp.modals.XmppAccount;
import com.chat.xmpp.services.XmppConnectionService;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.NotFilter;
import org.jivesoftware.smack.filter.OrFilter;
import org.jivesoftware.smack.filter.StanzaExtensionFilter;
import org.jivesoftware.smack.filter.StanzaFilter;
import org.jivesoftware.smack.filter.StanzaTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.roster.RosterLoadedListener;
import org.jivesoftware.smack.roster.packet.RosterPacket;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.address.packet.MultipleAddresses;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.packet.ChatStateExtension;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smackx.privacy.PrivacyListManager;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.search.ReportedData;
import org.jivesoftware.smackx.search.UserSearch;
import org.jivesoftware.smackx.search.UserSearchManager;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.jivesoftware.smackx.xdata.Form;
import org.json.JSONException;
import org.json.JSONObject;
import org.jxmpp.util.XmppStringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MyXMPP {

    static final StanzaFilter CHAT_STATE_FILTER = new AndFilter(StanzaTypeFilter.MESSAGE,
            new NotFilter(MessageTypeFilter.ERROR),
            new NotFilter(new StanzaExtensionFilter(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)),
            new OrFilter(new StanzaExtensionFilter(ChatState.composing.name(), ChatStateExtension.NAMESPACE),
                    new StanzaExtensionFilter(ChatState.paused.name(), ChatStateExtension.NAMESPACE)));
    static final StanzaFilter DISPLAY_MESSAGE_FILTER = new AndFilter(MessageTypeFilter.CHAT,
            new StanzaExtensionFilter(DisplayReceipt.ELEMENT, DisplayReceipt.NAMESPACE));
    static final StanzaFilter DELIVERY_MESSAGE_FILTER = new AndFilter(MessageTypeFilter.CHAT,
            new StanzaExtensionFilter(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE));
    static final StanzaFilter LAST_ACTIVITY_IQ_FILTER = new AndFilter(StanzaTypeFilter.IQ,
            new StanzaTypeFilter(UserLastActivity.class));
    static final StanzaFilter SINGLE_CHAT_MESSAGE_FILTER = new AndFilter(MessageTypeFilter.CHAT,
            new NotFilter(new StanzaExtensionFilter(null, ChatStateExtension.NAMESPACE)),
            new NotFilter(new StanzaExtensionFilter(null, MediaMessageExtension.NAMESPACE)),
            new NotFilter(new StanzaExtensionFilter(null, UserDetailsExtension.NAMESPACE)),
            new NotFilter(new StanzaExtensionFilter(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE)),
            new NotFilter(new StanzaExtensionFilter(DisplayReceipt.ELEMENT, DeliveryReceipt.NAMESPACE)));
    static final StanzaFilter SINGLE_CHAT_MEDIA_MESSAGE_FILTER = new AndFilter(MessageTypeFilter.CHAT,
            new StanzaExtensionFilter(MediaMessageExtension.ELEMENT, MediaMessageExtension.NAMESPACE));
    static final StanzaFilter SINGLE_CHAT_USERDETAIL_FILTER = new AndFilter(MessageTypeFilter.CHAT,
            new StanzaExtensionFilter(UserDetailsExtension.ELEMENT, UserDetailsExtension.NAMESPACE));
    private static final String TAG = "MyXMPP";
    private static MyXMPP instance = null;
    private static GroupChatManager groupChatManager;

    static {
        instance = new MyXMPP();
        groupChatManager = GroupChatManager.getInstance();
    }

    RosterLoadedListener rosterLoadedListener = new RosterLoadedListener() {
        @Override
        public void onRosterLoaded(Roster roster) {
            XmppLogger.printLog(TAG + " rosterLoadedListener onRosterLoaded");
            XmppLogger.printLog(TAG + " rosterLoadedListener total entries=" + roster.getEntryCount());

        }
    };
    StanzaListener lastActivityListner = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            XmppLogger.printLog(TAG + " lastActivityListner=" + packet.toString());
            UserLastActivity message = (UserLastActivity) packet;
            String fromJid = XmppStringUtils.parseBareJid(message.getFrom());
            if (message.getLastlogout() == -2) {
                triggerOnPresenceChange(fromJid, Presence.Type.available, -1L);
                return;
            }
            triggerOnPresenceChange(fromJid, Presence.Type.unavailable, message.getLastlogout());
        }
    };
    StanzaListener displayReceiptListener = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            long timeStamp = Calendar.getInstance().getTimeInMillis();
            long delayTimeStamp = -1;

            XmppLogger.printLog(TAG + " displayReceiptListener=" + packet.toString());
            Message message = (Message) packet;
            DisplayReceipt dr = DisplayReceipt.from((Message) packet);
            if (message.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                DelayInformation delayInformation = message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
                Date realDate = delayInformation.getStamp();
                if (realDate != null) {
                    delayTimeStamp = realDate.getTime();
                }
            }
            String fromJid = XmppStringUtils.parseBareJid(message.getFrom());

            MessageModal messageModal = new MessageModal();
            messageModal.setTo_jid(fromJid);
            messageModal.setMessage_id(dr.getId());
            messageModal.setStatus(MessageModal.STATUS_DISPLAYED);
            if (delayTimeStamp > 0) {
                messageModal.setTimestamp_read_device(delayTimeStamp);
            } else {
                messageModal.setTimestamp_read_device(timeStamp);
            }

            MessageAndCallTable.getInstance().updateMessageDisplayed(messageModal);
            triggerOnMessageUpdateListner(4, messageModal);
        }
    };
    StanzaListener deliveryReceiptListener = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            long timeStamp = Calendar.getInstance().getTimeInMillis();
            long delayTimeStamp = -1;

            XmppLogger.printLog(TAG + " deliveryReceiptListener=" + packet.toString());
            Message message = (Message) packet;
            DeliveryReceipt dr = DeliveryReceipt.from((Message) packet);
            if (message.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                DelayInformation delayInformation = message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
                Date realDate = delayInformation.getStamp();
                if (realDate != null) {
                    delayTimeStamp = realDate.getTime();
                }
            }
            String fromJid = XmppStringUtils.parseBareJid(message.getFrom());

            MessageModal messageModal = new MessageModal();
            messageModal.setTo_jid(fromJid);
            messageModal.setMessage_id(dr.getId());
            messageModal.setStatus(MessageModal.STATUS_DELIVERED);
            if (delayTimeStamp > 0) {
                messageModal.setTimestamp_receipt_device(delayTimeStamp);
            } else {
                messageModal.setTimestamp_receipt_device(timeStamp);
            }

            MessageAndCallTable.getInstance().updateMessageDelivered(messageModal);
            triggerOnMessageUpdateListner(3, messageModal);
        }
    };
    private ConnectionStates connectionStates = ConnectionStates.Disconnected;
    private MyXmppTcpConnection xmpptcpConnection;
    StanzaListener singleChatMessageListner = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            XmppLogger.printLog(TAG + " singleChatMessageListner=" + packet.toString());
            long timeStamp = Calendar.getInstance().getTimeInMillis();
            long delayTimeStamp = -1;

            Message message = (Message) packet;
            String fromJid = XmppStringUtils.parseBareJid(message.getFrom());
            String toJid = XmppStringUtils.parseBareJid(message.getTo());
            MessageModal messageModal = new MessageModal();
            messageModal.setMessage_id(message.getStanzaId());

            if (message.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                DelayInformation delayInformation = message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
                Date realDate = delayInformation.getStamp();
                if (realDate != null) {
                    delayTimeStamp = realDate.getTime();
                }
            }

            if (fromJid.equals(toJid)) {
                MultipleAddresses multipleAddresses = (MultipleAddresses) message.getExtension(MultipleAddresses.NAMESPACE);
                MultipleAddresses.Address address = multipleAddresses.getAddressesOfType(MultipleAddresses.Type.to).get(0);
                messageModal.setTo_jid(address.getJid());
                messageModal.setStatus(MessageModal.STATUS_SENT);
                if (delayTimeStamp > 0) {
                    messageModal.setTimestamp_send(delayTimeStamp);
                } else {
                    messageModal.setTimestamp_send(timeStamp);
                }
                MessageAndCallTable.getInstance().updateMessageSent(messageModal);
                triggerOnMessageUpdateListner(2, messageModal);

                return;
            } else {
                ContactModal contactModal = ContactTable.getInstance().addContactIfNotExist(fromJid);
                if (contactModal.isNewAdded()) {
                    triggerContactListner(1, contactModal, null, null);
                }
                if (message.getBody() != null) {

                    messageModal.setFrom_me(0);
                    messageModal.setStatus(MessageModal.STATUS_RECEIVED);
                    messageModal.setTo_jid(fromJid);
                    messageModal.setData(message.getBody());
                    if (delayTimeStamp > 0) {
                        messageModal.setTimestamp(delayTimeStamp);
                    } else {
                        messageModal.setTimestamp(timeStamp);
                    }
                    messageModal.setTimestamp_received(timeStamp);
                    long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(messageModal, false);
                    messageModal.set_id(message_call_table_id);

                    RecentModal recentModal = RecentTable.getInstance().writeMessage(messageModal);

                    sentDeliveryReceiptForMessage(messageModal, Message.Type.chat);

                    triggerOnRecentUpdate(recentModal);
                    triggerOnMessageUpdateListner(1, messageModal);

                }
            }
        }
    };

    StanzaListener singleChatMediaMessageListner = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            XmppLogger.printLog(TAG + " singleChatMediaMessageListner=" + packet.toString());
            long timeStamp = Calendar.getInstance().getTimeInMillis();
            long delayTimeStamp = -1;

            Message message = (Message) packet;
            String fromJid = XmppStringUtils.parseBareJid(message.getFrom());
            String toJid = XmppStringUtils.parseBareJid(message.getTo());
            MessageModal messageModal = new MessageModal();
            messageModal.setMessage_id(message.getStanzaId());

            MediaMessageExtension mediaMessageExtension;

            if (message.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                DelayInformation delayInformation = message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
                Date realDate = delayInformation.getStamp();
                if (realDate != null) {
                    delayTimeStamp = realDate.getTime();
                }
            }

            if (fromJid.equals(toJid)) {
                MultipleAddresses multipleAddresses = (MultipleAddresses) message.getExtension(MultipleAddresses.NAMESPACE);
                MultipleAddresses.Address address = multipleAddresses.getAddressesOfType(MultipleAddresses.Type.to).get(0);
                messageModal.setTo_jid(address.getJid());
                messageModal.setStatus(MessageModal.STATUS_SENT);
                if (delayTimeStamp > 0) {
                    messageModal.setTimestamp_send(delayTimeStamp);
                } else {
                    messageModal.setTimestamp_send(timeStamp);
                }
                MessageAndCallTable.getInstance().updateMessageSent(messageModal);
                triggerOnMessageUpdateListner(2, messageModal);
                return;
            } else {
                mediaMessageExtension = MediaMessageExtension.from(message);
                if (mediaMessageExtension == null) {
                    XmppLogger.printLog(TAG + " singleChatMediaMessageListner mediaExtention null");
                    return;
                }
                ContactModal contactModal = ContactTable.getInstance().addContactIfNotExist(fromJid);
                if (contactModal.isNewAdded()) {
                    triggerContactListner(1, contactModal, null, null);
                }
                if (message.getBody() != null) {

                    messageModal.setFrom_me(0);
                    messageModal.setStatus(MessageModal.STATUS_RECEIVED);
                    messageModal.setTo_jid(fromJid);
                    messageModal.setData(message.getBody());
                    if (delayTimeStamp > 0) {
                        messageModal.setTimestamp(delayTimeStamp);
                    } else {
                        messageModal.setTimestamp(timeStamp);
                    }
                    messageModal.setTimestamp_received(timeStamp);

                    messageModal.setMedia_url(mediaMessageExtension.getMedia_url());
                    messageModal.setMedia_hash(mediaMessageExtension.getMedia_hash());
                    messageModal.setMedia_mime_type(mediaMessageExtension.getMedia_mime_type());
                    messageModal.setMedia_type(mediaMessageExtension.getMedia_type());
                    messageModal.setMedia_name(mediaMessageExtension.getMedia_name());
                    messageModal.setMedia_caption(mediaMessageExtension.getMedia_caption());
                    messageModal.setThumb_image(mediaMessageExtension.getThumb_image());
                    messageModal.setMedia_size(mediaMessageExtension.getMedia_size());
                    messageModal.setMedia_duration(mediaMessageExtension.getMedia_duration());
                    messageModal.setMedia_origin(mediaMessageExtension.getMedia_origin());
                    messageModal.setLatitude(mediaMessageExtension.getLatitude());
                    messageModal.setLongitude(mediaMessageExtension.getLongitude());


                    long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(messageModal, true);
                    messageModal.set_id(message_call_table_id);

                    RecentModal recentModal = RecentTable.getInstance().writeMessage(messageModal);

                    sentDeliveryReceiptForMessage(messageModal, Message.Type.chat);

                    triggerOnRecentUpdate(recentModal);
                    triggerOnMessageUpdateListner(7, messageModal);

                    MediaType mediaType = messageModal.getMediaType();
                    if (mediaType == MediaType.contact) {
                        ContactDetailModal contactDetailModal = messageModal.getContactDetailModal();
                        if (contactDetailModal == null) {
                            return;
                        }
                        for (ContactDetailModal.Detail detail : contactDetailModal.getDetailArrayList()) {
                            if ((detail.getIsNumber() == ContactDetailModal.IS_NUMER_PHONE) && detail.isRegistered()) {
                                String newJid = XmppUtils.getJidFromNumber(detail.getValue());
                                if (XmppUtils.checkVaildString(newJid)) {
                                    ContactModal contactModal1 = ContactTable.getInstance().addContactIfNotExist(newJid);
                                   /* if (contactModal1.isNewAdded()) {
                                        triggerContactListner(1, contactModal1, null, null);
                                    }*/
                                }
                            }
                        }
                    }

                }
            }
        }
    };


    StanzaListener singleChatUserDetailListner = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            XmppLogger.printLog(TAG + " singleChatUserDetailListner=" + packet.toString());
            long timeStamp = Calendar.getInstance().getTimeInMillis();
            long delayTimeStamp = -1;

            Message message = (Message) packet;
            String fromJid = XmppStringUtils.parseBareJid(message.getFrom());
            String toJid = XmppStringUtils.parseBareJid(message.getTo());
            MessageModal messageModal = new MessageModal();
            messageModal.setMessage_id(message.getStanzaId());

            UserDetailsExtension userDetailsExtension = UserDetailsExtension.from(message);
            if (userDetailsExtension == null)
                return;

            if (message.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                DelayInformation delayInformation = message.getExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE);
                Date realDate = delayInformation.getStamp();
                if (realDate != null) {
                    delayTimeStamp = realDate.getTime();
                }
            }

            if (fromJid.equals(toJid)) {


                return;
            } else {
                // userDetailsExtension
                ContactModal contactModal = ContactTable.getInstance().addContactIfNotExist(fromJid);
                // if (contactModal.isNewAdded()) {
                int update = ContactTable.getInstance().updateContactInfoUsingVcard(fromJid,
                        userDetailsExtension.getName(),
                        userDetailsExtension.getProfile_pic());
                XmppLogger.printLog(TAG + " singleChatUserDetailListner=" + update);

                if (contactModal.isNewAdded()) {
                    triggerContactListner(1, contactModal, null, null);
                } else {
                    triggerContactListner(2, contactModal, null, null);
                }

                // }


            }
        }
    };

    private UserSearchManager userSearchManager;
    private XmppAccount xmppAccount;
    StanzaListener chatStateListner = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
            XmppLogger.printLog(TAG + " chatStateListner=" + packet.toString());
            Message message = (Message) packet;
            if (message == null) return;
            String fromJid = XmppStringUtils.parseBareJid(message.getFrom());
            String fromResource = "";
            String fromResourceJid = "";
            if (fromJid.contains(XmppConstant.group_domain)) {
                fromResource = XmppStringUtils.parseResource(message.getFrom());
                fromResourceJid = fromResource + "@" + XmppConstant.serviceName;
                if (fromResourceJid.equals(xmppAccount.getJid())) {
                    return;
                }
            }
            if (message.getExtension(ChatStateExtension.NAMESPACE) != null) {
                ChatStateExtension chatStateExtension = (ChatStateExtension) message.getExtension(ChatStateExtension.NAMESPACE);
                ChatState chatState = chatStateExtension.getChatState();
                triggerOnChatStateChange(fromJid, fromResourceJid, chatState);
            }

        }
    };
    private SyncStatus syncStatus = SyncStatus.NOTRUNNING;
    ContactHelper.ContactLoadedListner contactLoadedListner = new ContactHelper.ContactLoadedListner() {
        @Override
        public void contactLoaded(final ArrayList<ContactModal> contactModalArrayList) {

            ChatApplication.getInstance().runInBackground(new Runnable() {
                @Override
                public void run() {

                    XmppLogger.printLog(TAG + " contactLoadedListner contactLoaded ");
                    ArrayList<ContactModal> localDbContacts = ContactTable.getInstance().getAllSingleContacts();
//                    ContactHelper.getInstance().filterLocalContacts(localDbContacts, contactModalArrayList);

                    /*if (serverContacts != null) {
                        ArrayList<ContactModal> registeredContact = ContactHelper.getInstance().setRegisteredContact(serverContacts, contactModalArrayList);
                        contactModalArrayList.clear();
                        contactModalArrayList.addAll(registeredContact);
                        XmppLogger.printLog(TAG + " contactLoadedListner contactLoaded filteredContact=" + registeredContact.toString());
                    }*/
                    final ArrayList<ContactModal> newContacts = new ArrayList<ContactModal>();
                    final ArrayList<ContactModal> updateContacts = new ArrayList<ContactModal>();
                    XmppLogger.printLog(TAG + " contactLoaded runnable run");
                    ContactTable.getInstance().writeList(contactModalArrayList, newContacts, updateContacts);
                    ContactTable.getInstance().writeStrangerList(localDbContacts, updateContacts);
                    syncStatus = SyncStatus.NOTRUNNING;

                    triggerContactListner(3, null, newContacts, updateContacts);

                }
            });

        }
    };
    private XmppConnectionService xmppConnectionService;
    RosterListener rosterListener = new RosterListener() {
        @Override
        public void entriesAdded(Collection<String> addresses) {
            XmppLogger.printLog(TAG + " rosterListener entriesAdded addresses=" + addresses);
        }

        @Override
        public void entriesUpdated(Collection<String> addresses) {
            XmppLogger.printLog(TAG + " rosterListener entriesUpdated addresses=" + addresses);
        }

        @Override
        public void entriesDeleted(Collection<String> addresses) {
            XmppLogger.printLog(TAG + " rosterListener entriesDeleted addresses=" + addresses);
        }

        @Override
        public void presenceChanged(Presence presence) {
            if (presence != null) {
                XmppLogger.printLog(TAG + " rosterListener presenceChanged presence=" + presence.toXML());
                String jid = presence.getFrom();
                if (jid == null) return;
                jid = XmppStringUtils.parseBareJid(jid);
                if (presence.hasExtension(VcardChangeExtension.ELEMENT, VcardChangeExtension.NAMESPACE)) {
                    loadUserVcard(jid, null);
                    return;
                }
                if (presence.getType() == Presence.Type.unavailable) {
                    getLastActivityOfUser(jid);
                    return;
                } else if (presence.getType() == Presence.Type.available) {
                    triggerOnPresenceChange(jid, presence.getType(), -1L);
                }
            }
        }
    };
    OnContactAddUpdateListner contactListner = new OnContactAddUpdateListner() {
        @Override
        public void onNewContactAdded(ContactModal contactModal) {
            loadUserVcard(contactModal.getJid(), null);
        }

        @Override
        public void onContactUpdate(ContactModal contactModal) {

        }

        @Override
        public void onNewContactsAdded(List<ContactModal> contactModalList) {

        }

        @Override
        public void onContactsUpdated(List<ContactModal> contactModalList) {

        }

    };
    private List<ReportedData.Row> serverContacts;

    private MyXMPP() {
        XmppLogger.printLog(TAG + " instance created.");
    }

    public static MyXMPP getInstance() {
        return instance;
    }

    public MyXmppTcpConnection getXmpptcpConnection() {
        return xmpptcpConnection;
    }

    public XmppAccount getXmppAccount() {
        return xmppAccount;
    }

    public void setXmppAccount(XmppAccount xmppAccount) {
        this.xmppAccount = xmppAccount;

    }

    public boolean isWasAuthenticated() {
        return xmpptcpConnection.isWasAuthenticated();
    }

    public ConnectionStates getConnectionStates() {
        return connectionStates;
    }

    public void setXmppConnectionService(XmppConnectionService xmppConnectionService) {
        this.xmppConnectionService = xmppConnectionService;
    }

    public void startConnection() throws IllegalStateException {
        XmppLogger.printLog(TAG + " startConnection");
        if (xmppConnectionService == null) {
            throw new IllegalStateException("Set xmppConnectionService First.");
        }
//        if (xmppAccount == null) {
//            throw new IllegalStateException("Set xmppAccount First.");
//        }

        initialiseConnection();

    }

    private void addNeededProvider() {
        ProviderManager.addExtensionProvider(DisplayReceipt.ELEMENT, DisplayReceipt.NAMESPACE,
                new DisplayReceipt.Provider());
        ProviderManager.addExtensionProvider(MediaMessageExtension.ELEMENT, MediaMessageExtension.NAMESPACE,
                new MediaMessageExtension.Provider());
        ProviderManager.addExtensionProvider(UserDetailsExtension.ELEMENT, UserDetailsExtension.NAMESPACE,
                new UserDetailsExtension.Provider());
        ProviderManager.addExtensionProvider(VcardChangeExtension.ELEMENT, VcardChangeExtension.NAMESPACE,
                new VcardChangeExtension.Provider());
        ProviderManager.addIQProvider(UserLastActivity.ELEMENT, UserLastActivity.NAMESPACE, new UserLastActivity.Provider());
        ProviderManager.addExtensionProvider(GroupChatInvite.ELEMENT, GroupChatInvite.NAMESPACE,
                new GroupChatInvite.Provider());
    }

    private void initialiseConnection() {
        XmppLogger.printLog(TAG + " initialiseConnection");
        if (xmpptcpConnection == null) {
            XmppLogger.printLog(TAG + " xmpptcpConnection is null make new connection");
//            Base64.setEncoder(AndroidBase64Encoder.getInstance());
//            Base64UrlSafeEncoder.setEncoder(AndroidBase64UrlSafeEncoder.getInstance());
            Roster.setDefaultSubscriptionMode(Roster.SubscriptionMode.accept_all);
            addNeededProvider();
            XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration
                    .builder();
            config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
            config.setConnectTimeout(XmppConstant.CONNECTION_TIMEOUT);
            config.setServiceName(XmppConstant.serviceName);
            config.setHost(XmppConstant.host);
            config.setPort(XmppConstant.port);
            config.setDebuggerEnabled(true);
            XMPPTCPConnection.setUseStreamManagementResumptiodDefault(true);
            XMPPTCPConnection.setUseStreamManagementDefault(true);
            xmpptcpConnection = new MyXmppTcpConnection(config.build());
            xmpptcpConnection.setPacketReplyTimeout(XmppConstant.PACKET_REPLY_TIMEOUT);
            XMPPConnectionListener connectionListener = new XMPPConnectionListener();
            xmpptcpConnection.addConnectionListener(connectionListener);
            connect(xmpptcpConnection);


        } else {
            XmppLogger.printLog(TAG + " xmpptcpConnection is not null use previos connection");
            // xmpptcpConnection.setWasAuthenticated(false);
            connect(xmpptcpConnection);
        }
    }

    public synchronized boolean isConnectionConnected() {
        return connectionStates == ConnectionStates.Connected;
    }

    public synchronized boolean isConnectionDisconnected() {
        return connectionStates == ConnectionStates.Disconnected || connectionStates == ConnectionStates.TimeOut;
    }

    public void disconnect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isConnectionConnected()) {
                    XmppLogger.printLog(TAG + " disconnect");
                    xmpptcpConnection.disconnect();
                    connectionStates = ConnectionStates.Disconnected;
                    xmppAccount = null;
                    return;
                }
                XmppLogger.printLog(TAG + " already disconnect");
            }
        }).start();
    }

    private void connect(final MyXmppTcpConnection connection) {
        XmppLogger.printLog(TAG + " connect");
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                ConnectionStates connectionStates = connectToOpenFire(connection);
                while (connectionStates == ConnectionStates.TimeOut) {
                    connectionStates = connectToOpenFire(connection);
                }

            }
        });

    }

    /*This method is always call from background thread*/
    private ConnectionStates connectToOpenFire(MyXmppTcpConnection connection) {

        XmppLogger.printLog(TAG + " connectToOpenFire");
        if (isConnectionConnected()) {
            XmppLogger.printLog(TAG + " connection already connected");
            return connectionStates;
        }
        if (isConnectionDisconnected()) {
            connectionStates = ConnectionStates.Connecting;
            try {
                XmppLogger.printLog(TAG + " connection connect call");

                connection.connect();
                connectionStates = ConnectionStates.Connected;
                return connectionStates;
            } catch (IOException e) {
                e.printStackTrace();
                XmppLogger.printLog(TAG + " connectionThread IOException " + e.getMessage());
                triggerMyXmpConnectionListner(4, e);
            } catch (SmackException e) {
                e.printStackTrace();
                if (e.getMessage() != null) {
                    XmppLogger.printLog(TAG + " connectionThread SmackException " + e.getMessage());
                    if (e.getMessage().contains("java.net.SocketException")) {
                        XmppLogger.printLog(TAG + " connectionThread Please Add Internet Permission.");
                    } else if (e.getMessage().contains("java.net.ConnectException")) {
                        XmppLogger.printLog(TAG + " connectionThread Please check your internet connection.");
                    } else if (e.getMessage().contains("java.net.SocketTimeoutException")) {
                        XmppLogger.printLog(TAG + " connectionThread Connection Timeout Try Again.");
                        connectionStates = ConnectionStates.TimeOut;
                        return connectionStates;
                    } else if (e instanceof SmackException.NoResponseException) {
                        XmppLogger.printLog(TAG + " connectionThread Connection NoResponseException Try Again.");
                        connectionStates = ConnectionStates.TimeOut;
                        return connectionStates;
                    }
                }
                triggerMyXmpConnectionListner(4, e);

            } catch (XMPPException e) {
                e.printStackTrace();
                XmppLogger.printLog(TAG + " connectionThread XMPPException " + e.getMessage());
                triggerMyXmpConnectionListner(4, e);
            }
            connectionStates = ConnectionStates.Disconnected;
            XmppLogger.printLog(TAG + "  connection connect error");
            return connectionStates;
        }
        XmppLogger.printLog(TAG + " connection already in connecting");
        return connectionStates;
    }

    public void doLoginIfUserExist() throws IllegalStateException {
        XmppLogger.printLog(TAG + " doLoginIfUserExist");
        doLoginMannually(xmppAccount);
    }

    public boolean doLoginMannually(XmppAccount xmppAccount) {
        XmppLogger.printLog(TAG + " doLoginMannually");
        if (xmppAccount == null) {
//            ChatApplication.showToast("Set xmppAccount First.");
            return false;
        }
        if (!XmppUtils.checkVaildString(xmppAccount.getUsername()) ||
                !XmppUtils.checkVaildString(xmppAccount.getPassword())) {
            ChatApplication.showToast("Set xmppAccount username or password.");
            return false;
        }
        login(xmppAccount);
        return true;
    }

    private void login(final XmppAccount xmppAccount) {
        XmppLogger.printLog(TAG + " login");
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                int status = callLoginToServer(xmppAccount);
                while (status == XmppConstant.LOGIN_TIMOUT) {
                    status = callLoginToServer(xmppAccount);
                }
            }
        });

    }

    private int callLoginToServer(XmppAccount xmppAccount) {

        try {
            XmppLogger.printLog(TAG + " callLoginToServer login call");
            xmpptcpConnection.login(xmppAccount.getUsername(),
                    xmppAccount.getPassword(), xmppAccount.getResource());
            this.xmppAccount = xmppAccount;
            setPrivacyUnableList();
            return XmppConstant.LOGIN_SUCCESS;
        } catch (IOException e) {
            e.printStackTrace();
            XmppLogger.printLog(TAG + " callLoginToServer IOException " + e.getMessage());
            triggerMyXmpConnectionListner(5, e);
        } catch (SmackException e) {
            e.printStackTrace();
            if (e instanceof SmackException.AlreadyLoggedInException) {
                XmppLogger.printLog(TAG + "  callLoginToServer AlreadyLoggedInException");
                this.xmppAccount = xmppAccount;
                triggerMyXmpConnectionListner(5, e);
                return XmppConstant.LOGIN_SUCCESS;
            } else if (e instanceof SmackException.NotConnectedException) {
                XmppLogger.printLog(TAG + " callLoginToServer login NotConnectedException Try to connect first.");
                triggerMyXmpConnectionListner(5, e);
                return XmppConstant.NOT_CONNECTED;

            } else if (e instanceof SmackException.NoResponseException) {
                XmppLogger.printLog(TAG + " callLoginToServer login NoResponseException Try Again.");
                return XmppConstant.LOGIN_TIMOUT;
            }
            XmppLogger.printLog(TAG + " callLoginToServer SmackException " + e.getMessage());

        } catch (XMPPException e) {
            e.printStackTrace();
            triggerMyXmpConnectionListner(5, e);
            XmppLogger.printLog(TAG + " callLoginToServer XMPPException " + e.getMessage());
        }
        XmppLogger.printLog(TAG + "  callLoginToServer login error");
        return XmppConstant.LOGIN_EXCPTION;

    }

    public boolean registerNewUser(XmppAccount xmppAccount) {
        XmppLogger.printLog(TAG + " registerNewUser");
        if (xmppAccount == null) {
            ChatApplication.showToast("xmppAccount is null.");
            return false;
        }
        if (!XmppUtils.checkVaildString(xmppAccount.getUsername()) ||
                !XmppUtils.checkVaildString(xmppAccount.getPassword())) {
            ChatApplication.showToast("Set xmppAccount username or password.");
            return false;
        }
        xmppAccount.setHost(XmppConstant.host);
        xmppAccount.setPort(XmppConstant.port);
        xmppAccount.setServiceName(XmppConstant.serviceName);
        if (!XmppUtils.checkVaildString(xmppAccount.getResource())) {
            xmppAccount.setResource(XmppConstant.defaultResource);
        }
        if (!XmppUtils.checkVaildString(xmppAccount.getCountryPhoneCode())) {
            xmppAccount.setCountryPhoneCode(XmppConstant.defaultCountryPhoneCode);
        }

        registerUser(xmppAccount);
        return true;
    }

    private void registerUser(final XmppAccount xmppAccount) {
        XmppLogger.printLog(TAG + " registerUser");
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                int status = registerUserToServer(xmppAccount);
                while (status == XmppConstant.RESULT_RETRY) {
                    status = registerUserToServer(xmppAccount);
                }
            }
        });
    }

    private synchronized int registerUserToServer(XmppAccount xmppAccount) {
        AccountManager accountManager = AccountManager.getInstance(xmpptcpConnection);
        accountManager.sensitiveOperationOverInsecureConnection(true);
        try {
            if (accountManager.supportsAccountCreation()) {
                String username = xmppAccount.getUsername();
                String password = xmppAccount.getPassword();
                String name = xmppAccount.getName();
                if (!XmppUtils.checkVaildString(name)) {
                    name = "";
                }
                Map<String, String> attributes = new HashMap<String, String>();

                for (String attributeName : accountManager.getAccountAttributes()) {
                    if (attributeName.equals("name")) {
                        attributes.put(attributeName, name);
                    } else {
                        attributes.put(attributeName, "");
                    }
                }
                accountManager.createAccount(username, password, attributes);
                triggerUserRegisterListner(xmppAccount, null);
                XmppLogger.printLog(TAG + " registerUserToServer success");
                return XmppConstant.RESULT_SUCCESS;
            } else {
                XmppLogger.printLog(TAG + " registerUserToServer not supported");
                triggerUserRegisterListner(xmppAccount,
                        new SmackException("Server not support account creation."));
                return XmppConstant.RESULT_NOT_SUPPORT;
            }
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            XmppLogger.printLog(TAG + " registerUserToServer no response");
            return XmppConstant.RESULT_RETRY;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            XmppLogger.printLog(TAG + " registerUserToServer XMPPErrorException=" + e.getMessage());
            triggerUserRegisterListner(xmppAccount, e);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            XmppLogger.printLog(TAG + " registerUserToServer NotConnectedException=" + e.getMessage());
            triggerUserRegisterListner(xmppAccount, e);
            return XmppConstant.NOT_CONNECTED;
        }
        return XmppConstant.RESULT_ERROR;
    }

    public void setInitialVcard(final XmppAccount xmppAccount) {
        if (xmpptcpConnection == null)
            throw new IllegalStateException("connection is null");
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                int status = updateVcard(xmppAccount);
                while (status == XmppConstant.RESULT_RETRY) {
                    status = updateVcard(xmppAccount);
                }
            }
        });

    }

    private int updateVcard(XmppAccount xmppAccount) {

        VCardManager vCardManager = VCardManager.getInstanceFor(xmpptcpConnection);
        VCard vCard = null;
        try {
            vCard = vCardManager.loadVCard();
            if (vCard == null) {
                vCard = new VCard();
            }
            vCard.setNickName(xmppAccount.getName());
            vCard.setJabberId(xmppAccount.getJid());
            String status = vCard.getField(XmppConstant.VCARD_STATUS_FIELD);
            /*if (!XmppUtils.checkVaildString(status)) {
                vCard.setField(XmppConstant.VCARD_STATUS_FIELD,
                        xmppAccount.getStatus(), false);
            }*/
            String statusImg = vCard.getField(XmppConstant.VCARD_USER_IMG);
            if (!XmppUtils.checkVaildString(statusImg)) {
                vCard.setField(XmppConstant.VCARD_USER_IMG, xmppAccount.getAvtarPath());
            }

            if (XmppUtils.checkVaildString(xmppAccount.getAvtarPath())) {
                try {
                    byte[] avtarByties = XmppUtils.getFileBytes(new File(xmppAccount.getAvtarPath()));
                    if (avtarByties != null) {
                        vCard.setAvatar(avtarByties);
                        XmppLogger.printLog(TAG + " setInitialVcard vcard set successfully");
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    XmppLogger.printLog(TAG + " setInitialVcard IllegalStateException");
                } catch (IOException e) {
                    e.printStackTrace();
                    XmppLogger.printLog(TAG + " setInitialVcard IOException");
                }
            }
            vCardManager.saveVCard(vCard);
            triggerVcardUpdateListner(xmppAccount, vCard, null);
            return XmppConstant.RESULT_SUCCESS;
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            return XmppConstant.RESULT_RETRY;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            triggerVcardUpdateListner(xmppAccount, vCard, e);
            return XmppConstant.RESULT_ERROR;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            triggerVcardUpdateListner(xmppAccount, vCard, e);
            return XmppConstant.NOT_CONNECTED;
        }

    }

    public boolean updateMyStatus(final String newStatus, final OnMyVcardLoadListner onMyVcardLoadListner) {
        if (xmpptcpConnection == null) {
            return false;
        }
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                int retry = 0;
                int status = updateMyStatusToServer(newStatus, onMyVcardLoadListner);
                while (status == XmppConstant.RESULT_RETRY && retry < 2) {
                    status = updateMyStatusToServer(newStatus, onMyVcardLoadListner);
                    retry++;
                }
            }
        });
        return true;


    }

    private void sendVcardChangePresence() {
        Presence presence = new Presence(Presence.Type.available);
        VcardChangeExtension vcardChangeExtension = new VcardChangeExtension();
        presence.addExtension(vcardChangeExtension);
        try {
            xmpptcpConnection.sendStanza(presence);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    private int updateMyStatusToServer(String newStatus, OnMyVcardLoadListner onMyVcardLoadListner) {

        VCardManager vCardManager = VCardManager.getInstanceFor(xmpptcpConnection);
        VCard vCard = null;
        try {
            vCard = vCardManager.loadVCard();
            if (vCard == null) {
                vCard = new VCard();
            }
            vCard.setField(XmppConstant.VCARD_STATUS_FIELD,
                    newStatus, false);
            vCardManager.saveVCard(vCard);
            sendVcardChangePresence();
            triggerVcardLoadListner(xmppAccount.getJid(), vCard, onMyVcardLoadListner, null);
            return XmppConstant.RESULT_SUCCESS;
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            return XmppConstant.RESULT_RETRY;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            triggerVcardLoadListner(xmppAccount.getJid(), null, onMyVcardLoadListner, e);
            return XmppConstant.RESULT_ERROR;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            triggerVcardLoadListner(xmppAccount.getJid(), null, onMyVcardLoadListner, e);
            return XmppConstant.NOT_CONNECTED;
        }

    }

    public void getUserPresence(String jid) {
        if (xmpptcpConnection == null) {
            return;
        }
        Roster roster = Roster.getInstanceFor(xmpptcpConnection);
        Presence presence = roster.getPresence(jid);
        if (presence.getType() == Presence.Type.unavailable) {
            getLastActivityOfUser(jid);
            return;
        }
        if (presence.getType() == Presence.Type.available) {
            triggerOnPresenceChange(jid, presence.getType(), -1L);
        }


    }

    public boolean getLastActivityOfUser(final String jid) {
        if (xmpptcpConnection == null) {
            return false;
        }
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                int retry = 0;
                int status = getLastActivityOfUserFromServer(jid);
                while (status == XmppConstant.RESULT_RETRY && retry < 2) {
                    status = getLastActivityOfUserFromServer(jid);
                    retry++;
                }
            }
        });
        return true;
    }

    private int getLastActivityOfUserFromServer(String jid) {

        try {
            UserLastActivity activity = new UserLastActivity(jid);
            xmpptcpConnection.createPacketCollectorAndSend(activity).nextResultOrThrow();
            return XmppConstant.RESULT_SUCCESS;
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            return XmppConstant.RESULT_RETRY;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            return XmppConstant.RESULT_ERROR;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            return XmppConstant.NOT_CONNECTED;
        }
    }

    public void sendComposingChatState(String toJid) {
        if (xmpptcpConnection == null || !xmpptcpConnection.isAuthenticated()) {
            return;
        }
        Message message = new Message();
        message.setTo(toJid);
        message.setStanzaId(XmppUtils.generateMessageId(5));
        ChatStateExtension extension = new ChatStateExtension(ChatState.composing);
        message.addExtension(extension);
        try {
            xmpptcpConnection.sendStanza(message);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    public void sendGroupComposingChatState(String toJid) {
        if (xmpptcpConnection == null || !xmpptcpConnection.isAuthenticated()) {
            return;
        }
        Message message = new Message();
        message.setTo(toJid);
        message.setType(Message.Type.groupchat);
        message.setStanzaId(XmppUtils.generateMessageId(5));
        ChatStateExtension extension = new ChatStateExtension(ChatState.composing);
        message.addExtension(extension);
        try {
            xmpptcpConnection.sendStanza(message);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    public void sendPauseChatState(String toJid) {
        if (xmpptcpConnection == null || !xmpptcpConnection.isAuthenticated() || toJid == null) {
            return;
        }
        Message message = new Message();
        message.setTo(toJid);
        message.setStanzaId(XmppUtils.generateMessageId(5));
        ChatStateExtension extension = new ChatStateExtension(ChatState.paused);
        message.addExtension(extension);
        try {
            xmpptcpConnection.sendStanza(message);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    public void sendGroupPauseChatState(String toJid) {
        if (xmpptcpConnection == null || !xmpptcpConnection.isAuthenticated()) {
            return;
        }
        Message message = new Message();
        message.setTo(toJid);
        message.setType(Message.Type.groupchat);
        message.setStanzaId(XmppUtils.generateMessageId(5));
        ChatStateExtension extension = new ChatStateExtension(ChatState.paused);
        message.addExtension(extension);
        try {
            xmpptcpConnection.sendStanza(message);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    private void removeNeededListners() {
        userSearchManager = null;
        xmpptcpConnection.removeSyncStanzaListener(lastActivityListner);
        xmpptcpConnection.removeSyncStanzaListener(chatStateListner);
        xmpptcpConnection.removeSyncStanzaListener(singleChatMessageListner);
        xmpptcpConnection.removeSyncStanzaListener(singleChatMediaMessageListner);
        xmpptcpConnection.removeSyncStanzaListener(singleChatUserDetailListner);
        xmpptcpConnection.removeSyncStanzaListener(deliveryReceiptListener);
        xmpptcpConnection.removeSyncStanzaListener(displayReceiptListener);
        if (groupChatManager != null) {
            groupChatManager.removeAllGroupListner();
        }
        ContactHelper.getInstance().removeContactLoadListner(contactLoadedListner);
        Roster.getInstanceFor(xmpptcpConnection).removeRosterLoadedListener(rosterLoadedListener);
        Roster.getInstanceFor(xmpptcpConnection).removeRosterListener(rosterListener);
        ChatApplication.getInstance().unRegisterContactObserver();
    }

    private void addNeededListners() {

        ChatApplication.getInstance().getXmppListners().addUiListner(contactListner);
        xmpptcpConnection.addSyncStanzaListener(lastActivityListner, LAST_ACTIVITY_IQ_FILTER);
        xmpptcpConnection.addSyncStanzaListener(chatStateListner, CHAT_STATE_FILTER);
        xmpptcpConnection.addSyncStanzaListener(singleChatMessageListner, SINGLE_CHAT_MESSAGE_FILTER);
        xmpptcpConnection.addSyncStanzaListener(singleChatMediaMessageListner, SINGLE_CHAT_MEDIA_MESSAGE_FILTER);
        xmpptcpConnection.addSyncStanzaListener(singleChatUserDetailListner, SINGLE_CHAT_USERDETAIL_FILTER);
        xmpptcpConnection.addSyncStanzaListener(deliveryReceiptListener, DELIVERY_MESSAGE_FILTER);
        xmpptcpConnection.addSyncStanzaListener(displayReceiptListener, DISPLAY_MESSAGE_FILTER);
        groupChatManager.addAllGroupListner();

        ContactHelper.getInstance().addContactLoadListner(contactLoadedListner);
        Roster.getInstanceFor(xmpptcpConnection).addRosterLoadedListener(rosterLoadedListener);
        Roster.getInstanceFor(xmpptcpConnection).addRosterListener(rosterListener);
        userSearchManager = new UserSearchManager(xmpptcpConnection);
        ChatApplication.getInstance().registerContactObserver();
    }

    public synchronized void getAllXmppUsers() {
        if (syncStatus == SyncStatus.RUNNING) {
            XmppLogger.printLog(TAG + " getAllXmppUsers sync is already running");
            return;
        }
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                int result = getUsersFromServer();
                while (result == XmppConstant.RESULT_RETRY) {
                    result = getUsersFromServer();
                }
                if (result == XmppConstant.RESULT_SUCCESS) {
                    if (xmppAccount != null) {
                        ChatApplication.getInstance().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    int countryPhoneCode = Integer.parseInt(xmppAccount.getCountryPhoneCode());
                                    ContactHelper.getInstance().getAllContact(xmppConnectionService.getApplicationContext(),
                                            null, countryPhoneCode);
                                } catch (NumberFormatException e) {
                                    XmppLogger.printLog(TAG + " NumberFormatException in countryphonecode");
                                    syncStatus = SyncStatus.NOTRUNNING;
                                }
                            }
                        });
                        return;
                    }
                    syncStatus = SyncStatus.NOTRUNNING;
                } else {
                    syncStatus = SyncStatus.NOTRUNNING;
                }

            }
        });
    }

    /*This method always call from background*/
    private int getUsersFromServer() {
        try {
            if (userSearchManager == null) {
                XmppLogger.printLog(TAG + " getAllXmppUsers userSearchManager is null");
                return XmppConstant.RESULT_ERROR;
            }
            if (!isConnectionConnected()) {
                XmppLogger.printLog(TAG + " getAllXmppUsers xmpptcpConnection is not connected.");
                return XmppConstant.RESULT_ERROR;
            }
            if (xmpptcpConnection == null || !xmpptcpConnection.isAuthenticated()) {
                XmppLogger.printLog(TAG + " getAllXmppUsers xmpptcpConnection is null or user not login");
                return XmppConstant.RESULT_ERROR;
            }
            syncStatus = SyncStatus.RUNNING;
//            List<String> searchServices = userSearchManager.getSearchServices();
//            for (String searchService : searchServices) {
//                XmppLogger.printLog(TAG + " searchService=" + searchService);
//            }

            String searchFormString = "search." + xmpptcpConnection.getServiceName();
            XmppLogger.printLog(TAG + " searchFormString=" + searchFormString);
            Form searchForm = null;
            searchForm = userSearchManager.getSearchForm(searchFormString);
            Form answerForm = searchForm.createAnswerForm();
            UserSearch userSearch = new UserSearch();
            answerForm.setAnswer("Username", true);
            answerForm.setAnswer("search", "*");
            ReportedData results = userSearch.sendSearchForm(xmpptcpConnection, answerForm, searchFormString);
            if (results != null) {
                List<ReportedData.Row> rows = results.getRows();
                if (rows == null) {
                    rows = Collections.unmodifiableList(new ArrayList<ReportedData.Row>());
                }
                XmppLogger.printLog(TAG + " getAllXmppUsers  result found totalUsers=" + rows.size());
                serverContacts = rows;
                return XmppConstant.RESULT_SUCCESS;
            } else {
                XmppLogger.printLog(TAG + " getAllXmppUsers no result found");
            }

        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            return XmppConstant.RESULT_RETRY;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
        return XmppConstant.RESULT_ERROR;
    }

    /*
    code =>1 connection connected
    code =>2 connection disconnected
    code =>3 user logined
    code =>4 connection connect excption
    code =>5 login excption
    * */
    private void triggerMyXmpConnectionListner(final int code, final Exception e) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                List<MyXmppConnectionListner> myXmppConnectionListnerArrayList =
                        (List<MyXmppConnectionListner>) xmppConnectionService.getMyApplication().getXmppListners().getUiListernsArrayList(MyXmppConnectionListner.class);

                switch (code) {
                    case 1:
                        for (MyXmppConnectionListner myXmppConnectionListner : myXmppConnectionListnerArrayList) {
                            myXmppConnectionListner.xmppConnected();
                        }
                        break;
                    case 2:
                        for (MyXmppConnectionListner myXmppConnectionListner : myXmppConnectionListnerArrayList) {
                            myXmppConnectionListner.xmppDisconnected();
                        }
                        if (xmpptcpConnection.isWasAuthenticated()) {

                            xmpptcpConnection = null;
                        }
                        if (e == null) {
                            initialiseConnection();
                            return;
                        }
                        String expMsg = e.getMessage();
                        if (expMsg != null && (expMsg.contains("recvfrom failed: ETIMEDOUT") ||
                                expMsg.contains("ECONNRESET (Connection reset by peer)") ||
                                expMsg.contains("Parser got END_DOCUMENT event"))) {
                            initialiseConnection();
                        }
                        break;
                    case 3:
                        for (MyXmppConnectionListner myXmppConnectionListner : myXmppConnectionListnerArrayList) {
                            myXmppConnectionListner.xmppLogedIn();
                        }
//                        getAllXmppUsers();
                        break;
                    case 4:
                        for (MyXmppConnectionListner myXmppConnectionListner : myXmppConnectionListnerArrayList) {
                            myXmppConnectionListner.xmppConnectError(e);
                        }

                        break;
                    case 5:
                        for (MyXmppConnectionListner myXmppConnectionListner : myXmppConnectionListnerArrayList) {
                            myXmppConnectionListner.xmppLoginInError(e);
                        }

                        break;
                    default:
                        break;
                }
            }
        });

    }

    /*
code =>1 registerSuccess
code =>2 register error
* */
    private void triggerUserRegisterListner(final XmppAccount xmppAccount, final Exception e) {

        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayList<OnUserRegisterListner> onUserRegisterListnerArrayList =
                        (ArrayList<OnUserRegisterListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnUserRegisterListner.class);
                for (OnUserRegisterListner onUserRegisterListner : onUserRegisterListnerArrayList) {
                    onUserRegisterListner.xmppUserRegistered(xmppAccount, e);
                }
            }

        });

    }

    private void triggerVcardUpdateListner(
            final XmppAccount xmppAccount, final VCard vCard,
            final Exception e) {

        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayList<OnVcardUpdateListner> onVcardUpdateListnerArrayList =
                        (ArrayList<OnVcardUpdateListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnVcardUpdateListner.class);
                for (OnVcardUpdateListner onVcardUpdateListner : onVcardUpdateListnerArrayList) {
                    onVcardUpdateListner.vcardUpdateSuccess(xmppAccount, vCard, e);
                }
            }

        });

    }

    public synchronized void contactChangeDoSync() {
        if (xmpptcpConnection == null || !xmpptcpConnection.isAuthenticated()) {
            XmppLogger.printLog(TAG + " contactChangeDoSync connection null or not login");
            return;
        }
        if (syncStatus == SyncStatus.RUNNING) {
            XmppLogger.printLog(TAG + " contactChangeDoSync Sync alredy running.");
            return;
        }
//        getAllXmppUsers();

    }

    public void loadMyAndRegisteredUserVcard() {
        loadMyVcard();
        loadRegisteredContactVcards();
    }

    public void loadMyVcard() {
        if (xmppAccount != null) {
            loadUserVcard(xmppAccount.getJid(), null);
        }
    }

    public void loadRegisteredContactVcards() {
        ChatApplication.getInstance().runInDatabaseBackground(new Runnable() {
            @Override
            public void run() {
                final ArrayList<ContactModal> list = ContactTable.getInstance().getAllRegisteredContacts();
                ChatApplication.getInstance().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (ContactModal contactModal : list) {
                            final String jid = contactModal.getJid();
                            loadUserVcard(jid, null);
                        }

                    }
                });
            }
        });
    }

    public void loadUserVcard(final String jid, final OnVcardLoadListner onVcardLoadListner) {
        if (xmpptcpConnection == null) {
            XmppLogger.printLog(TAG + " loadUserVcard jid=" + jid + " connection is null");
            return;
        }

        ChatApplication.getInstance().runInVcardBackground(new Runnable() {
            @Override
            public void run() {
                int retry = 0;
                Object[] status = loadVcard(jid);
                while ((int) status[0] == XmppConstant.RESULT_RETRY && retry < 2) {
                    status = loadVcard(jid);
                    retry++;
                }
                if ((int) status[0] == XmppConstant.RESULT_SUCCESS) {
                    VCard vCard = (VCard) status[1];
                    triggerVcardLoadListner(jid, vCard, onVcardLoadListner, null);
                } else {
                    XmppLogger.printLog("loadUserVcard jid=" + jid + " failed.");
                    Exception e = (Exception) status[1];
                    triggerVcardLoadListner(jid, null, onVcardLoadListner, e);
                }
            }
        });
    }

    private synchronized Object[] loadVcard(String jid) {

        VCardManager vCardManager = VCardManager.getInstanceFor(xmpptcpConnection);
        VCard vCard = null;
        Object[] response = new Object[2];
        try {
            vCard = vCardManager.loadVCard(jid);
            response[0] = XmppConstant.RESULT_SUCCESS;
            response[1] = vCard;
            return response;
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            response[0] = XmppConstant.RESULT_RETRY;
            response[1] = e;
            return response;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            response[0] = XmppConstant.RESULT_ERROR;
            response[1] = e;
            return response;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            response[0] = XmppConstant.NOT_CONNECTED;
            response[1] = e;
            return response;
        }

    }

    private void triggerVcardLoadListner(final String jid, final VCard vCard,
                                         final UiListner uiListner, final Exception e) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (uiListner != null) {
                    if (uiListner instanceof OnMyVcardLoadListner) {
                        ((OnMyVcardLoadListner) uiListner).onLoadMyVcard(vCard, e);
                    } else if (uiListner instanceof OnVcardLoadListner) {
                        ((OnVcardLoadListner) uiListner).vcardLoad(jid, vCard, e);
                    }

                }
                ArrayList<OnVcardLoadListner> onVcardLoadArrayList =
                        (ArrayList<OnVcardLoadListner>) xmppConnectionService.getMyApplication().getXmppListners().getUiListernsArrayList(OnVcardLoadListner.class);

                for (OnVcardLoadListner onVcardLoad1 : onVcardLoadArrayList) {
                    onVcardLoad1.vcardLoad(jid, vCard, e);
                }

                if (xmppAccount != null && xmppAccount.getJid().equals(jid)) {
                    ArrayList<OnMyVcardLoadListner> onMyVcardLoadListnerArrayList =
                            (ArrayList<OnMyVcardLoadListner>) xmppConnectionService.getMyApplication().getXmppListners().getUiListernsArrayList(OnMyVcardLoadListner.class);

                    for (OnMyVcardLoadListner onMyVcardLoadListner : onMyVcardLoadListnerArrayList) {
                        onMyVcardLoadListner.onLoadMyVcard(vCard, e);
                    }
                }
            }
        });


    }

    public void sentDeliveryReceiptForMessage(final MessageModal dbMessageModal, final Message.Type type) {
        ChatApplication.getInstance().runInReceiptBackground(new Runnable() {
            @Override
            public void run() {
                Message message = new Message(dbMessageModal.getTo_jid(), type);
                message.addExtension(new DeliveryReceipt(dbMessageModal.getMessage_id()));
                try {
                    xmpptcpConnection.sendStanza(message);

                    MessageModal messageModal = new MessageModal();
                    messageModal.setTo_jid(dbMessageModal.getTo_jid());
                    messageModal.setMessage_id(dbMessageModal.getMessage_id());
                    messageModal.setStatus(MessageModal.STATUS_DELIVERY_RECEIPT_SENT);
                    MessageAndCallTable.getInstance().updateSentMessageDelivered(messageModal);
                    triggerOnMessageUpdateListner(5, messageModal);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void sentDisplayReceiptForGroupMessage(MessageModal dbMessageModal, Message.Type type) {
        groupChatManager.sentDisplayReceiptForGroupMessage(dbMessageModal, type);
    }

    public void sentDisplayReceiptForMessage(final MessageModal dbMessageModal, final Message.Type type) {
        ChatApplication.getInstance().runInReceiptBackground(new Runnable() {
            @Override
            public void run() {
                Message message = new Message(dbMessageModal.getTo_jid(), type);
                message.addExtension(new DisplayReceipt(dbMessageModal.getMessage_id()));
                try {
                    xmpptcpConnection.sendStanza(message);
                    MessageModal messageModal = new MessageModal();
                    messageModal.setTo_jid(dbMessageModal.getTo_jid());
                    messageModal.setMessage_id(dbMessageModal.getMessage_id());
                    messageModal.setStatus(MessageModal.STATUS_DISPLAY_RECEIPT_SENT);
                    MessageAndCallTable.getInstance().updateSentMessageDisplayed(messageModal);
                    triggerOnMessageUpdateListner(6, messageModal);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void sendgroupTextMessage(ContactModal contactModal, String msg) {
        groupChatManager.sendTextMessage(contactModal, msg);
    }

    public void sendTextMessage(ContactModal contactModal, String msg) {


        long timeStamp = Calendar.getInstance().getTimeInMillis();
        String stanzaId = XmppUtils.generateMessageId(XmppConstant.defaultMessageIdLength);
        final MessageModal dbModal = new MessageModal();
        dbModal.setTo_jid(contactModal.getJid());
        dbModal.setFrom_me(1);
        dbModal.setMessage_id(stanzaId);
        dbModal.setStatus(MessageModal.STATUS_NOT_SENT);
        dbModal.setData(msg);
        dbModal.setTimestamp(timeStamp);
        dbModal.setTimestamp_send(timeStamp);
        long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(dbModal, false);
        dbModal.set_id(message_call_table_id);
        triggerOnMessageUpdateListner(1, dbModal);

        RecentModal recentModal = RecentTable.getInstance().writeMessage(dbModal);
        triggerOnRecentUpdate(recentModal);
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                MultipleAddresses multipleAddresses = new MultipleAddresses();
                multipleAddresses.addAddress(MultipleAddresses.Type.to, dbModal.getTo_jid(), null, null, false, null);
                multipleAddresses.addAddress(MultipleAddresses.Type.replyto, xmppAccount.getJid(), null, null, false, null);

                String threadID = SingleChatManager.getInstance().getBuddyChat(dbModal.getTo_jid());

                Message message = new Message();
                message.setTo(XmppConstant.serviceName);
                message.setBody(dbModal.getData());
                message.setStanzaId(dbModal.getMessage_id());
                message.setType(Message.Type.chat);
                message.setThread(threadID);
                message.addExtension(multipleAddresses);

                try {
                    xmpptcpConnection.sendStanza(message);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                    XmppLogger.printLog(TAG + " sendTextMessage NotConnectedException");
                }

            }
        });
    }


    public void sendUserDetailsMessage(final String tojid, final String userName, final String userImg) {

        /*AddContectToRoster(tojid);*/
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                MultipleAddresses multipleAddresses = new MultipleAddresses();
                multipleAddresses.addAddress(MultipleAddresses.Type.to, tojid, null, null, false, null);
                multipleAddresses.addAddress(MultipleAddresses.Type.replyto, xmppAccount.getJid(), null, null, false, null);
                String stanzaId = XmppUtils.generateMessageId(XmppConstant.defaultMessageIdLength);
                String threadID = SingleChatManager.getInstance().getBuddyChat(tojid);

                Message message = new Message();
                message.setTo(tojid);
                message.setBody("Userdetail");
                message.setStanzaId(stanzaId);
                message.setType(Message.Type.chat);
                message.setThread(threadID);
                // message.addExtension(multipleAddresses);

                UserDetailsExtension extension = new UserDetailsExtension(tojid, userName, userImg);
                message.addExtension(extension);
                try {
                    xmpptcpConnection.sendStanza(message);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                    XmppLogger.printLog(TAG + " sendTextMessage NotConnectedException");
                }

            }
        });
    }

    public synchronized void sendGroupAudioMedia(final ContactModal contactModal,
                                                 final String filePath,
                                                 final String caption,
                                                 final int origin) throws FileNotFoundException {
        groupChatManager.sendAudioMedia(contactModal, filePath, caption, origin);
    }

    public synchronized void sendAudioMedia(final ContactModal contactModal,
                                            final String filePath,
                                            final String caption,
                                            final int origin) throws FileNotFoundException {
        if (!XmppUtils.checkVaildString(filePath)) {
            throw new IllegalArgumentException("filepath can't be empty or null");
        }

        try {
            String newPath = AudioFolderSent.getInstance().copyFile(filePath);
            sendMediaMessage(contactModal,
                    MediaType.audio,
                    newPath, caption, -1.0, -1.0, origin, null);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public synchronized void sendGroupContactMedia(final ContactModal contactModal,
                                                   final String contactDetail, final int origin) {
        groupChatManager.sendContactMedia(contactModal, contactDetail, origin);
    }

    public synchronized void sendContactMedia(final ContactModal contactModal,
                                              final String contactDetail, final int origin) {
        if (!XmppUtils.checkVaildString(contactDetail)) {
            return;
        }
        sendMediaMessage(contactModal,
                MediaType.contact,
                contactDetail, null, -1.0, -1.0, origin, null);

    }

    public synchronized void sendGroupImageMedia(final ContactModal contactModal,
                                                 final String filePath,
                                                 final String caption, final int origin) throws FileNotFoundException {
        groupChatManager.sendImageMedia(contactModal, filePath, caption, origin);
    }

    public synchronized void sendImageMedia(final ContactModal contactModal,
                                            final String filePath,
                                            final String caption, final int origin) throws FileNotFoundException {
        if (!XmppUtils.checkVaildString(filePath)) {
            throw new IllegalArgumentException("filepath can't be empty or null");
        }

        try {
            String newPath = ImageFolderSent.getInstance().copyFile(filePath);
            sendMediaMessage(contactModal,
                    MediaType.image,
                    newPath, caption, -1.0, -1.0, origin, null);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void sendGroupImageMedia(final ContactModal contactModal, final List<MediaModel> list,
                                    final int origin) throws FileNotFoundException {
        groupChatManager.sendImageMedia(contactModal, list, origin);
    }

    public void sendImageMedia(final ContactModal contactModal, final List<MediaModel> list,
                               final int origin) throws FileNotFoundException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (MediaModel mediaModel : list) {
                    try {
                        sendImageMedia(contactModal, mediaModel.getUrl(), mediaModel.getCaption(), origin);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    public synchronized void sendGroupVideoMedia(final ContactModal contactModal,
                                                 final MediaModel mediaModel,
                                                 final int origin) throws FileNotFoundException {
        groupChatManager.sendVideoMedia(contactModal, mediaModel, origin);
    }

    private synchronized void sendVideoMedia(final ContactModal contactModal,
                                             final MediaModel mediaModel,
                                             final int origin) throws FileNotFoundException {
        final String filePath = mediaModel.getUrl();
        final String caption = mediaModel.getCaption();
        if (!XmppUtils.checkVaildString(filePath)) {
            throw new IllegalArgumentException("filepath can't be empty or null");
        }


        String newPath = filePath;
        sendMediaMessage(contactModal,
                MediaType.video,
                newPath, caption, -1.0, -1.0, origin, mediaModel);


    }

    public void sendGroupVideoMedia(final ContactModal contactModal, final List<MediaModel> list,
                                    final int origin) throws FileNotFoundException {
        groupChatManager.sendVideoMedia(contactModal, list, origin);
    }

    public void sendVideoMedia(final ContactModal contactModal, final List<MediaModel> list,
                               final int origin) throws FileNotFoundException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (MediaModel mediaModel : list) {
                    try {
                        sendVideoMedia(contactModal, mediaModel, origin);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    public synchronized void sendGroupDocumentMedia(final ContactModal contactModal,
                                                    final DocModal docModal,
                                                    final int origin) throws FileNotFoundException {
        groupChatManager.sendDocumentMedia(contactModal, docModal, origin);
    }

    private synchronized void sendDocumentMedia(final ContactModal contactModal,
                                                final DocModal docModal,
                                                final int origin) throws FileNotFoundException {
        String filePath = docModal.getFilePath();
        if (!XmppUtils.checkVaildString(filePath)) {
            throw new IllegalArgumentException("filepath can't be empty or null");
        }

        try {
            String realFileName = DocumentFolderSent.getFilenameWithOutExtensionFromPath(filePath);
            //  String realFileExt = DocumentFolderSent.getFileExtensionFromPath(filePath);
            String newPath = DocumentFolderSent.getInstance().copyFile(filePath);
//            XmppLogger.printLog(TAG + " realFileName=" + realFileName
//                    + "\n" + "realFileExt=" + realFileExt
//                    + "\n" + "newPath=" + newPath);
            sendMediaMessage(contactModal,
                    MediaType.document,
                    newPath, realFileName, -1.0, -1.0, origin, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendGroupDocumentMedia(final ContactModal contactModal, final SelectedDocList selectedDocList,
                                       final int origin) {
        groupChatManager.sendDocumentMedia(contactModal, selectedDocList, origin);
    }

    public void sendDocumentMedia(final ContactModal contactModal, final SelectedDocList selectedDocList,
                                  final int origin) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (DocModal docModal : selectedDocList.getDocModals()) {
                    try {
                        sendDocumentMedia(contactModal, docModal, origin);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    public synchronized void sendGroupLocationMedia(final ContactModal contactModal, final Location location, final int origin) {
        groupChatManager.sendLocationMedia(contactModal, location, origin);
    }

    public synchronized void sendLocationMedia(final ContactModal contactModal, final Location location, final int origin) {
        if (!XmppUtils.checkValidObject(location)) {
            return;
        }
        String caption = null;
        String filePath = null;
        Bundle bundle = location.getExtras();
        if (bundle != null) {
            caption = bundle.getString("address");
            filePath = bundle.getString("thumbPath");
        }
        sendMediaMessage(contactModal,
                MediaType.location,
                filePath, caption, location.getLatitude(), location.getLongitude(), origin, null);

    }

    private void sendMediaMessage(final ContactModal contactModal,
                                  final MediaType mediaType, final String filePath,
                                  final String caption, final double latitude,
                                  final double longitude, final int origin,
                                  final MediaModel mediaModel) {


        try {
            if (filePath == null) {
                return;
            }
            long fileSize = 0;
            String mimeType = null;
            long duration = 0;
            String thumbImage = null;
            if (mediaType != MediaType.contact &&
                    mediaType != MediaType.link &&
                    mediaType != MediaType.location) {
                fileSize = MediaUtil.getfileSize(filePath);
                mimeType = MediaUtil.getMimeType(filePath);
            }
            if (mediaType == MediaType.contact) {
                mimeType = "contact/vcard";
            } else if (mediaType == MediaType.location) {
                mimeType = "map/location";
            }
            if (mediaType == MediaType.image) {
                thumbImage = MediaThumbGenerator.getThumbBase64(filePath);
            } else if (mediaType == MediaType.video) {
                thumbImage = MediaThumbGenerator.getVideoThumbBase64(filePath);
            } else if (mediaType == MediaType.location) {
                thumbImage = MediaThumbGenerator.getLocationThumbBase64(filePath);
                new File(filePath).delete();
            }

            long timeStamp = Calendar.getInstance().getTimeInMillis();
            String stanzaId = XmppUtils.generateMessageId(XmppConstant.defaultMessageIdLength);
            MessageModal dbModal = new MessageModal();
            dbModal.setTo_jid(contactModal.getJid());
            dbModal.setFrom_me(1);
            dbModal.setMessage_id(stanzaId);
            dbModal.setStatus(MessageModal.STATUS_NOT_SENT);
            dbModal.setData("media");
            dbModal.setTimestamp(timeStamp);
            dbModal.setTimestamp_send(timeStamp);

            dbModal.setMedia_type(mediaType);
            if (mediaType == MediaType.link ||
                    mediaType == MediaType.contact) {
                dbModal.setMedia_url(filePath);
            } else {
                dbModal.setMedia_name(filePath);
            }
            dbModal.setMedia_caption(caption);
            dbModal.setLatitude(latitude);
            dbModal.setLongitude(longitude);
            dbModal.setMedia_origin(origin);
            dbModal.setMedia_size(fileSize);
            dbModal.setMedia_mime_type(mimeType);
            dbModal.setMedia_duration(duration);
            dbModal.setThumb_image(thumbImage);
           // dbModal.setThumb_image(filePath);
           /* if (mediaType == MediaType.video) {
                String outFile = VideoFolderSent.getInstance().getNewFilePath();
                String[] finalCmd = new String[mediaModel.getVideoFfmpegCmd().length + 1];
                int i = 0;
                for (String data : mediaModel.getVideoFfmpegCmd()) {
                    finalCmd[i] = data;
                    i++;
                }
                finalCmd[i] = outFile;
                dbModal.setVideoCmddata(finalCmd);
            }*/

            if (mediaType != MediaType.link &&
                    mediaType != MediaType.contact &&
                    mediaType != MediaType.location &&
                    mediaType != MediaType.video) {
                String fileHash = XmppUtils.getFileHash(filePath);
                if (fileHash == null) {
                    new File(filePath).delete();
                    return;
                }
                MessageModal hashModal = MessageAndCallTable.getInstance().getMessageModalForHashUrlNotNull(fileHash);
                if (hashModal != null) {
                    String oldFileUrl = hashModal.getMedia_url();
                    if (XmppUtils.checkVaildString(oldFileUrl)) {
                        dbModal.setMedia_url(oldFileUrl);
                    }
                }
                dbModal.setMedia_hash(fileHash);
            }
            long message_call_table_id = MessageAndCallTable.getInstance().
                    writeMessage(dbModal, true);
            dbModal.set_id(message_call_table_id);
            triggerOnMessageUpdateListner(7, dbModal);

            RecentModal recentModal = RecentTable.getInstance().writeMessage(dbModal);
            triggerOnRecentUpdate(recentModal);
            if (mediaType == MediaType.contact || mediaType == MediaType.location) {
                sendMediaMessageToXmpp(dbModal);
            } else {
                if (mediaType == MediaType.video) {
                    FFmpegCompressionHandler.getInstance().addForCompression(dbModal);
                } else if (XmppUtils.checkVaildString(dbModal.getMedia_url())) {
                    sendMediaMessageToXmpp(dbModal);
                } else {
                    uploadMediaToServer(dbModal);
                }

            }

        } catch (NotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void retryVideoMediaMessage(MessageModal messageModal) {
        String[] cmd = messageModal.getVideoCmddata();
        if (cmd == null) {
            String fileHash = messageModal.getMedia_hash();
            if (fileHash == null) {
                return;
            }
            MessageModal hashModal = MessageAndCallTable.getInstance().getMessageModalForHashUrlNotNull(fileHash);
            if (hashModal != null) {
                String oldFileUrl = hashModal.getMedia_url();
                if (XmppUtils.checkVaildString(oldFileUrl)) {
                    messageModal.setMedia_url(oldFileUrl);
                }
            }
            messageModal.setMedia_hash(fileHash);

            if (XmppUtils.checkVaildString(messageModal.getMedia_url())) {
                messageModal.setStatus(MessageModal.STATUS_MEDIA_END);
                MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
                sendMediaMessageToXmpp(messageModal);
            } else {
                uploadMediaToServer(messageModal);
            }

        } else {
            FFmpegCompressionHandler.getInstance().addForCompression(messageModal);
        }

    }

    public void retryGroupMediaMessage(MessageModal messageModal) {
        groupChatManager.retryMediaMessage(messageModal);
    }

    public void retryMediaMessage(MessageModal messageModal) {
        MediaType mediaType = messageModal.getMediaType();
        if (mediaType == MediaType.video) {
            retryVideoMediaMessage(messageModal);
            return;
        }
        if (mediaType != MediaType.link &&
                mediaType != MediaType.contact &&
                mediaType != MediaType.location) {
            String fileHash = messageModal.getMedia_hash();
            if (fileHash == null) {
                return;
            }
            MessageModal hashModal = MessageAndCallTable.getInstance().getMessageModalForHashUrlNotNull(fileHash);
            if (hashModal != null) {

                String oldFileUrl = hashModal.getMedia_url();

                if (XmppUtils.checkVaildString(oldFileUrl)) {
                    messageModal.setMedia_url(oldFileUrl);
                }
            }
        }
        if (XmppUtils.checkVaildString(messageModal.getMedia_url())) {
            messageModal.setStatus(MessageModal.STATUS_MEDIA_END);
            MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
            sendMediaMessageToXmpp(messageModal);
        } else {
            uploadMediaToServer(messageModal);
        }


    }

    /**
     * @param _case        - 1=>Add Message,
     *                     2=>Message Sent,
     *                     3=>Message Delivered,
     *                     4=>Message Displayed
     *                     5=>Message Delivery Sent
     *                     6=>Message Displayed Sent
     *                     7=>Add Media Message
     * @param messageModal
     */
    public synchronized void triggerOnMessageUpdateListner(final int _case, final MessageModal messageModal) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayList<OnMessageUpdateListner> list = (ArrayList<OnMessageUpdateListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnMessageUpdateListner.class);
                for (OnMessageUpdateListner onMessageUpdateListner : list) {
                    switch (_case) {
                        case 1:
                            onMessageUpdateListner.onMessageAdded(messageModal);
                            break;
                        case 2:
                            onMessageUpdateListner.onMessageSent(messageModal);
                            break;
                        case 3:
                            onMessageUpdateListner.onMessageDelivered(messageModal);
                            break;
                        case 4:
                            onMessageUpdateListner.onMesssageDisplayed(messageModal);
                            break;
                        case 5:
                            onMessageUpdateListner.onMesssageDeliverySent(messageModal);
                            break;
                        case 6:
                            onMessageUpdateListner.onMesssageDisplaySent(messageModal);
                            break;
                        case 7:
                            onMessageUpdateListner.onMediaMessageAdded(messageModal);
                            break;
                    }

                }
            }
        });

    }

    /**
     * @param _case        - 1=>Add New Contact in DB
     *                     2=>Update Contact in DB
     *                     3=>Add contact list or Update Contact List or both in DB
     * @param contactModal
     */
    public synchronized void triggerContactListner(final int _case,
                                                   final ContactModal contactModal,
                                                   final List<ContactModal> addedContacts,
                                                   final List<ContactModal> updatedContacts) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                XmppLogger.printLog(TAG + " triggerContactListner");
                SyncContactWithRoster();

                ArrayList<OnContactAddUpdateListner> list = (ArrayList<OnContactAddUpdateListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnContactAddUpdateListner.class);
                for (OnContactAddUpdateListner contactListner : list) {
                    switch (_case) {
                        case 1:
                            contactListner.onNewContactAdded(contactModal);
                            break;
                        case 2:
                            contactListner.onContactUpdate(contactModal);
                            break;
                        case 3:
                            if (addedContacts.size() > 0) {
                                contactListner.onNewContactsAdded(addedContacts);
                            }
                            if (updatedContacts.size() > 0) {
                                contactListner.onContactsUpdated(updatedContacts);
                            }
                            break;

                    }

                }


            }
        });

    }

    /**
     * @param recentModal
     */
    public synchronized void triggerOnRecentUpdate(final RecentModal recentModal) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayList<OnRecentUpdateListner> list = (ArrayList<OnRecentUpdateListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnRecentUpdateListner.class);
                for (OnRecentUpdateListner onRecentUpdateListner : list) {
                    if (recentModal.get_id() > 0) {
                        onRecentUpdateListner.onNewRecentAdded(recentModal);
                    } else {
                        onRecentUpdateListner.onRecentUpdate(recentModal);
                    }

                }
            }
        });

    }

    /**
     * @param toJid
     * @param chatState
     */
    private synchronized void triggerOnChatStateChange(final String toJid, final String fromResourceJid, final ChatState chatState) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayList<OnChatStateChangeListner> list = (ArrayList<OnChatStateChangeListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnChatStateChangeListner.class);
                for (OnChatStateChangeListner onChatStateChangeListner : list) {
                    onChatStateChangeListner.onChatStateChange(toJid, fromResourceJid, chatState);
                }
            }
        });

    }

    private synchronized void triggerOnPresenceChange(final String jid, final Presence.Type type, final long timeStamp) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayList<OnPresenceChangeListner> list = (ArrayList<OnPresenceChangeListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnPresenceChangeListner.class);
                for (OnPresenceChangeListner onPresenceChangeListner : list) {
                    onPresenceChangeListner.onPresenceChanged(jid, type, timeStamp);
                }
            }
        });
    }

    private synchronized void SyncContactWithRoster() {

        ChatApplication.getInstance().runInRosterBackground(new Runnable() {
            @Override
            public void run() {
                List<String> dbRegisterContacts = ContactTable.getInstance().getAllRegisteredContactsJid();
                XmppLogger.printLog(TAG + " SyncContactWithRoster dbRegisterContacts=" + dbRegisterContacts.toString());
                if (dbRegisterContacts.size() > 0) {
                    if (xmpptcpConnection == null) {
                        return;
                    }
                    final Roster roster = Roster.getInstanceFor(xmpptcpConnection);
                    if (roster.isLoaded()) {
                        for (String jid : dbRegisterContacts) {
                            if (!roster.contains(jid)) {
                                int result = createRosterEntry(roster, jid);
                                while (result == XmppConstant.RESULT_RETRY) {
                                    result = createRosterEntry(roster, jid);
                                }
                                continue;
                            }
                            RosterEntry rosterEntry = roster.getEntry(jid);
                            if (rosterEntry != null) {
                                RosterPacket.ItemType itemType = rosterEntry.getType();
                                if (itemType == RosterPacket.ItemType.none ||
                                        itemType == RosterPacket.ItemType.from) {
                                    sendSubscriptionToRoster(jid);
                                }
                            }
                        }
                    }
                }


            }
        });

    }

    private int createRosterEntry(Roster roster, String jid) {
        try {
            roster.createEntry(jid, null, null);
            XmppLogger.printLog(TAG + " createRosterEntry jid=" + jid);
        } catch (SmackException.NotLoggedInException e) {
            e.printStackTrace();
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            return XmppConstant.RESULT_RETRY;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            return XmppConstant.NOT_CONNECTED;
        }
        return XmppConstant.RESULT_ERROR;
    }

    private void sendSubscriptionToRoster(String jid) {
        try {
            Presence presencePacket = new Presence(Presence.Type.subscribe);
            presencePacket.setTo(jid);
            xmpptcpConnection.sendStanza(presencePacket);
            XmppLogger.printLog(TAG + " sendSubscriptionToRoster jid=" + jid);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    public void downloadMediaFromServer(MessageModal messageModal) {
        String outputFilePath = FoldersHandler.getInstance().getFilePathForDownload(messageModal.getMediaType(),
                messageModal.getMedia_url(), messageModal.getMedia_caption());
        if (outputFilePath == null) {
            return;
        }
        DownloadRequest downloadRequest = new DownloadRequest(messageModal.getMedia_url(),
                outputFilePath, messageModal);
        DownloadMediaHandler.getInstance().submit(downloadRequest);

    }

    public void downloadLocationMediaFromServer(MessageModal messageModal) {
        String outputFilePath = InternalAppFolder.getInstance().getFilePathForDownload(messageModal.getMediaType());
        if (outputFilePath == null) {
            return;
        }
        if (!XmppUtils.checkVaildString(messageModal.getMedia_url())) {
            String url = XmppUtils.getLocationImageFullUrl(messageModal.getLatitude(), messageModal.getLongitude());
            messageModal.setMedia_url(url);
        }
        DownloadRequest downloadRequest = new DownloadRequest(messageModal.getMedia_url(),
                outputFilePath, messageModal);
        DownloadMediaHandler.getInstance().submit(downloadRequest);

    }

    public void uploadMediaToServer(MessageModal messageModal) {
        UploadRequest uploadRequest = new UploadRequest(XmppConstant.MEDIA_FILE_URL,
                messageModal);
        uploadRequest.addParameters("messageId", messageModal.getMessage_id());
        uploadRequest.addParameters("name", "testfile.jpg");
        uploadRequest.addParameters("size", messageModal.getMedia_size() + "");
        uploadRequest.addParameters("duration", messageModal.getMedia_duration() + "");
        uploadRequest.addParameters("mime_type", messageModal.getMedia_mime_type());
        uploadRequest.addFile("file", messageModal.getMedia_name());
        UploadMediaHandler.getInstance().submit(uploadRequest);

    }

    public synchronized void handleMediaUploadEnd(UploadRequest uploadRequest) {
        MessageModal messageModal = uploadRequest.getMessageModal();
        if (uploadRequest.getException() != null) {
            XmppLogger.printLog(TAG + " handleMediaUploadEnd with excption=" + uploadRequest.getException().getMessage());
            messageModal.setMedia_url(null);
            messageModal.setStatus(MessageModal.STATUS_MEDIA_FAILED);
            MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
        } else {
            String response = uploadRequest.getResponse();
            XmppLogger.printLog(TAG + " file upload response=" + response);
            if (uploadRequest.getResponseCode() == 200) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.optInt("code", -1);
                    boolean error = jsonObject.optBoolean("error", true);
                    String message = jsonObject.optString("message", "");


                    if (code == 0 && !error) {
                        String fileUrl = message;

                        messageModal.setMedia_url(fileUrl);
                        messageModal.setStatus(MessageModal.STATUS_MEDIA_END);
                        MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
                        if (messageModal.getTo_jid().contains(XmppConstant.group_domain)) {
                            groupChatManager.sendMediaMessageToXmpp(messageModal);
                        } else {
                            sendMediaMessageToXmpp(messageModal);
                        }

                    } else {
                        messageModal.setStatus(MessageModal.STATUS_MEDIA_FAILED);
                        messageModal.setMedia_url(null);
                        MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
                    }

                    /*int code = jsonObject.getInt("code");
                    boolean error = jsonObject.getBoolean("error");
                    String msg = jsonObject.getString("message");
                    if (!error) {
                        JSONObject data = jsonObject.getJSONObject("data");
                        String fileurl = data.getString("file_name");
                        //   String fileName = MediaUtil.getFilenameFromUrl(fileurl);
                        messageModal.setMedia_url(fileurl);
                        messageModal.setStatus(MessageModal.STATUS_MEDIA_END);
                        MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
                        if (messageModal.getTo_jid().contains(XmppConstant.group_domain)) {
                            groupChatManager.sendMediaMessageToXmpp(messageModal);
                        } else {
                            sendMediaMessageToXmpp(messageModal);
                        }

                    } else {
                        messageModal.setStatus(MessageModal.STATUS_MEDIA_FAILED);
                        messageModal.setMedia_url(null);
                        MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
                    }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                messageModal.setStatus(MessageModal.STATUS_MEDIA_FAILED);
                messageModal.setMedia_url(null);
                MessageAndCallTable.getInstance().updateMediaMessageUrlAndStatus(messageModal);
            }
        }

    }

    private void sendMediaMessageToXmpp(final MessageModal dbModal) {
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                MultipleAddresses multipleAddresses = new MultipleAddresses();
                multipleAddresses.addAddress(MultipleAddresses.Type.to, dbModal.getTo_jid(), null, null, false, null);
                multipleAddresses.addAddress(MultipleAddresses.Type.replyto, xmppAccount.getJid(), null, null, false, null);

                String threadID = SingleChatManager.getInstance().getBuddyChat(dbModal.getTo_jid());

                Message message = new Message();
                message.setTo(XmppConstant.serviceName);
                message.setBody(dbModal.getData());
                message.setStanzaId(dbModal.getMessage_id());
                message.setType(Message.Type.chat);
                message.setThread(threadID);
                message.addExtension(multipleAddresses);
                MediaMessageExtension mediaMessageExtension = new MediaMessageExtension();

                mediaMessageExtension.setMedia_url(dbModal.getMedia_url());
                mediaMessageExtension.setMedia_hash(dbModal.getMedia_hash());
                mediaMessageExtension.setMedia_mime_type(dbModal.getMedia_mime_type());
                mediaMessageExtension.setMedia_type(dbModal.getMedia_type());
                mediaMessageExtension.setMedia_caption(dbModal.getMedia_caption());
                mediaMessageExtension.setThumb_image(dbModal.getThumb_image());
                mediaMessageExtension.setMedia_size(dbModal.getMedia_size());
                mediaMessageExtension.setMedia_duration(dbModal.getMedia_duration());
                mediaMessageExtension.setMedia_origin(dbModal.getMedia_origin());
                mediaMessageExtension.setLatitude(dbModal.getLatitude());
                mediaMessageExtension.setLongitude(dbModal.getLongitude());
                message.addExtension(mediaMessageExtension);
                try {
                    xmpptcpConnection.sendStanza(message);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                    XmppLogger.printLog(TAG + " sendMediaMessageToXmpp NotConnectedException");
                }
            }
        });
    }

    public synchronized void handleMediaDownloadEnd(final DownloadRequest downloadRequest) {
        final MessageModal messageModal = downloadRequest.getMessageModal();
        if (messageModal == null ||
                messageModal.getMediaType() == MediaType.location) {
            return;
        }

        ChatApplication.getInstance().runInDatabaseBackground(new Runnable() {
            @Override
            public void run() {

                if (downloadRequest.getException() != null) {
                    XmppLogger.printLog(TAG + " handleMediaDownloadEnd with excption=" + downloadRequest.getException().getMessage());
                    messageModal.setMedia_name(null);
                    messageModal.setStatus(MessageModal.STATUS_MEDIA_FAILED);
                } else {
                    messageModal.setMedia_name(downloadRequest.getOutputFilepath());
                    messageModal.setStatus(MessageModal.STATUS_MEDIA_END);
                }
                MessageAndCallTable.getInstance().updateMediaMessageNameAndStatus(messageModal);
            }
        });


    }

    public synchronized void handleVideoCompressEnd(final MessageModal messageModal, final boolean success) {
        ChatApplication.getInstance().runInDatabaseBackground(new Runnable() {
            @Override
            public void run() {
                String[] cmd = messageModal.getVideoCmddata();
                String outputFile = cmd[cmd.length - 1];
                String fileHash = XmppUtils.getFileHash(outputFile);
                long fileSize = -1;
                if (success) {
                    try {
                        fileSize = MediaUtil.getfileSize(outputFile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    if (fileSize > 0) {
                        messageModal.setMedia_size(fileSize);
                    }
                    messageModal.setMedia_name(outputFile);
                    messageModal.setVideoCmddata(null);
                    messageModal.setMedia_hash(fileHash);
                } else {
                    messageModal.setMedia_hash(null);
                }
                MessageAndCallTable.getInstance().updateMediaMessageNameAndStatusAfterCompress(messageModal);

                if (success) {

                    if (fileHash == null) {
                        new File(outputFile).delete();
                        return;
                    }
                    MessageModal hashModal = MessageAndCallTable.getInstance().getMessageModalForHashUrlNotNull(fileHash);
                    if (hashModal != null) {
                        String oldFileUrl = hashModal.getMedia_url();
                        if (XmppUtils.checkVaildString(oldFileUrl)) {
                            messageModal.setMedia_url(oldFileUrl);
                        }
                    }
                    messageModal.setMedia_hash(fileHash);


                    if (XmppUtils.checkVaildString(messageModal.getMedia_url())) {
                        if (messageModal.getTo_jid().contains(XmppConstant.group_domain)) {
                            groupChatManager.sendMediaMessageToXmpp(messageModal);
                        } else {
                            sendMediaMessageToXmpp(messageModal);
                        }

                    } else {
                        uploadMediaToServer(messageModal);
                    }


                }

            }
        });
    }

/*
    public void createNewGroup(final String groupJid,
                               final String subject,
                               final byte[] avatar, final List<String> mem_list) {
        ChatApplication.getInstance().runInDatabaseBackground(new Runnable() {
            @Override
            public void run() {
                XmppLogger.printLog(TAG + " createNewGroup groupJid=" + groupJid);
                XmppLogger.printLog(TAG + " subject=" + subject);
                XmppLogger.printLog(TAG + " mem_list=" + mem_list.toString());
                ContactModal contactModal = groupChatManager.CreateNewGroup(groupJid, subject, avatar);
                if (contactModal == null) {
                    ContactModal errorModal = new ContactModal(XmppConstant.serviceName);
                    errorModal.setJid(groupJid);
                    triggerGroupListner(3, errorModal);
                    XmppLogger.printLog(TAG + " createNewGroup failed groupJid=" + groupJid);
                    return;
                }
                triggerGroupListner(1, contactModal);
                XmppLogger.printLog(TAG + " createNewGroup success groupJid=" + groupJid);
                String owner_jid = xmppAccount.getJid();
                List<GroupMemberModal> list = contactModal.getGroupMemberModalList();
                list.clear();
                if (XmppUtils.checkVaildString(owner_jid)) {
                    GroupMemberModal groupMemberModal = new GroupMemberModal();
                    groupMemberModal.setMem_jid(owner_jid);
                    groupMemberModal.setMem_type(GroupMembers.MEMBER_TYPE_OWNER);
                    groupMemberModal.setMem_invite(GroupMembers.MEMBER_INVITE_SENT);
                    list.add(groupMemberModal);
                }
                for (String mem_jid : mem_list) {
                    if (XmppUtils.checkVaildString(mem_jid)) {
                        GroupMemberModal groupMemberModal = new GroupMemberModal();
                        groupMemberModal.setMem_jid(mem_jid);
                        groupMemberModal.setMem_type(GroupMembers.MEMBER_TYPE_MEMBER);
                        groupMemberModal.setMem_invite(GroupMembers.MEMBER_INVITE_NOT_SENT);
                        list.add(groupMemberModal);
                    }
                }

                groupChatManager.addGroupMembers(contactModal);

                long timeStamp = Calendar.getInstance().getTimeInMillis();
                String stanzaId = XmppUtils.generateMessageId(XmppConstant.defaultMessageIdLength);
                final MessageModal dbModal = new MessageModal();
                dbModal.setTo_jid(contactModal.getJid());
                dbModal.setFrom_me(1);
                dbModal.setMessage_type(MessageAndCallTable.TYPE_MESSAGE_GROUP_LOCAL_CREATE);
                dbModal.setMessage_id(stanzaId);
                dbModal.setStatus(MessageModal.STATUS_DISPLAYED);
                dbModal.setData("Group not created tap to retry.");
                dbModal.setTimestamp(timeStamp);
                dbModal.setTimestamp_send(timeStamp);
                dbModal.setTo_resource(xmppAccount.getJid());
                dbModal.setRecipient_count(contactModal.getGroupMemberModalList().size());
                long message_call_table_id = MessageAndCallTable.getInstance().writeMessage(dbModal, false);
                dbModal.set_id(message_call_table_id);


                RecentModal recentModal = RecentTable.getInstance().writeMessage(dbModal);

                triggerGroupListner(2, contactModal);
                triggerOnRecentUpdate(recentModal);

                XmppLogger.printLog(TAG + " createNewGroup addGroupMembers success groupJid=" + groupJid);
            }
        });

    }
*/

/*    public void retryGroupCreate(final ContactModal contactModal) {
        ChatApplication.getInstance().runInDatabaseBackground(new Runnable() {
            @Override
            public void run() {
                List<GroupMemberModal> list = GroupMembers.getInstance().getGroupMembers(contactModal.getJid());
                contactModal.getGroupMemberModalList().clear();
                if (list != null) {
                    contactModal.getGroupMemberModalList().addAll(list);
                }
                enterInGroup(contactModal);
            }
        });

    }*/

    /*public void enterInGroup(final ContactModal contactModal) {
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                XmppLogger.printLog(TAG + " enterInGroup");
                groupChatManager.enterInGroup(contactModal, null, XmppConstant.PACKET_REPLY_TIMEOUT);

            }
        });

    }*/

   /* public void sendConfigrationFormOfGroup(final ContactModal contactModal) {
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                XmppLogger.printLog(TAG + " sendConfigrationFormOfGroup");
                groupChatManager.sendConfigrationFormOfGroup(contactModal);

            }
        });

    }

    public void addMembersInGroup(final ContactModal contactModal) {
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                XmppLogger.printLog(TAG + " addMembersInGroup");
                groupChatManager.addMembersInGroup(contactModal);
            }
        });

    }

    public void sendInvitationToGroupMembers(final ContactModal contactModal) {
        ChatApplication.getInstance().runInBackground(new Runnable() {
            @Override
            public void run() {
                XmppLogger.printLog(TAG + " sendInvitationToGroupMembers");
                groupChatManager.sendInvitationToGroupMembers(contactModal);
            }
        });

    }*/

    /**
     * @param tag          1=>New group added
     *                     2=>Group Member Updated
     *                     3=>New Group Creation Error Local
     *                     4=>Enter in group success
     *                     5=>Enter in group not success
     * @param contactModal
     */
    public synchronized void triggerGroupListner(final int tag,
                                                 final ContactModal contactModal) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ArrayList<OnGroupListner> list = (ArrayList<OnGroupListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnGroupListner.class);

                switch (tag) {
                    case 1:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.newGroupCreated(contactModal);
                        }
                        break;
                    case 2:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.groupMemberAdded(contactModal);
                        }
                        break;
                    case 3:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.newGroupCreationError(contactModal);
                        }
                        break;
                    case 4:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.onEnterInGroup(contactModal, true);
                        }
                        break;
                    case 5:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.onEnterInGroup(contactModal, false);
                        }
                        break;
                    case 6:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.onGroupConfigPush(contactModal, true);
                        }
                        break;
                    case 7:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.onGroupConfigPush(contactModal, false);
                        }
                        break;
                    case 8:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.onGroupMemberAdded(contactModal, true);
                        }
                        break;
                    case 9:
                        for (OnGroupListner onGroupListner : list) {
                            onGroupListner.onGroupMemberAdded(contactModal, false);
                        }
                        break;
                }

            }
        });

    }

    public boolean isAuthenticated() {
        return xmpptcpConnection != null && xmpptcpConnection.isAuthenticated();
    }

    public synchronized void AddContectToRoster(final String jid) {

        if (jid == null) {
            return;
        }

        ChatApplication.getInstance().runInRosterBackground(new Runnable() {
            @Override
            public void run() {

                if (xmpptcpConnection == null) {
                    return;
                }
                final Roster roster = Roster.getInstanceFor(xmpptcpConnection);
                if (roster.isLoaded()) {

                    if (!roster.contains(jid)) {
                        int result = createRosterEntry(roster, jid);
                        while (result == XmppConstant.RESULT_RETRY) {
                            result = createRosterEntry(roster, jid);
                        }

                    }
                    RosterEntry rosterEntry = roster.getEntry(jid);
                    if (rosterEntry != null) {
                        RosterPacket.ItemType itemType = rosterEntry.getType();
                        if (itemType == RosterPacket.ItemType.none ||
                                itemType == RosterPacket.ItemType.from) {
                            sendSubscriptionToRoster(jid);
                        }
                    }
                }


            }
        });

    }

    public void updateAllPrivacyList() {
        if (xmpptcpConnection == null) {
            return;
        }

        PrivacyListManager privacyManager = PrivacyListManager.getInstanceFor(xmpptcpConnection);
        List<PrivacyItem> list = null;
        try {
            list = privacyManager.getPrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME).getItems();
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }

        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                ContactTable.getInstance().setBlockedStatus(list.get(i).getValue(), 1);
            }

        }


    }

    public boolean blockFriend(String jid) {
        if (jid == null || xmpptcpConnection == null) {
            return false;
        }

        PrivacyItem item = new PrivacyItem(PrivacyItem.Type.jid, jid, false, 7);
        PrivacyListManager privacyManager = PrivacyListManager.getInstanceFor(xmpptcpConnection);

        try {
            List<PrivacyItem> list = null;
            boolean checkPrivacyStatus = true;
            try {

                list = privacyManager.getPrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME).getItems();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();
                /*item_not_found*/
                XMPPError.Condition condition = e.getXMPPError().getCondition();
                if (condition != null && condition.name() != null && condition.name().equals("item_not_found")) {

                } else {
                    checkPrivacyStatus = false;
                }

            }
            if (checkPrivacyStatus) {

                if (list == null) {
                    list = new ArrayList<PrivacyItem>();
                }
                item.setFilterIQ(false);
                item.setFilterMessage(false);
                item.setFilterPresenceIn(false);
                item.setFilterPresenceOut(false);
                list.add(item);

                privacyManager.updatePrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME, list);
                privacyManager.setActiveListName(XmppUtils.ACTIVE_PRIVACY_LIST_NAME);
                ContactTable.getInstance().setBlockedStatus(jid, 1);
                triggerOnPrivacyIteamList(1, jid, true);
                return true;
            }
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();

        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();

        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();

        }
        triggerOnPrivacyIteamList(1, jid, false);
        return false;

    }

    public boolean unBlockFriend(String jid) {
        if (jid == null || xmpptcpConnection == null) {
            return false;
        }


       /* PrivacyItem item = new PrivacyItem(PrivacyItem.Type.jid, jid, true, 7);
        PrivacyListManager privacyManager = PrivacyListManager.getInstanceFor(xmpptcpConnection);
        List<PrivacyItem> list = new ArrayList<PrivacyItem>();
        list.add(item);

        try {
            privacyManager.updatePrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME, list);
            privacyManager.setActiveListName(XmppUtils.ACTIVE_PRIVACY_LIST_NAME);
            ContactTable.getInstance().setBlockedStatus(jid, 0);
            return true;
        } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
            e.printStackTrace();
            return false;
        }*/

        PrivacyItem item = new PrivacyItem(PrivacyItem.Type.jid, jid, true, 7);
        PrivacyListManager privacyManager = PrivacyListManager.getInstanceFor(xmpptcpConnection);

        try {
            List<PrivacyItem> list = null;
            boolean checkPrivacyStatus = true;
            try {

                list = privacyManager.getPrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME).getItems();
            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();
                /*item_not_found*/
                XMPPError.Condition condition = e.getXMPPError().getCondition();
                if (condition != null && condition.name() != null && condition.name().equals("item_not_found")) {

                } else {
                    checkPrivacyStatus = false;
                }

            }
            if (checkPrivacyStatus) {

                if (list == null) {
                    list = new ArrayList<PrivacyItem>();
                }

                for (int i = 0; i < list.size(); i++) {
                    String unBlockJID = list.get(i).getValue();
                    if (unBlockJID.equalsIgnoreCase(jid)) {
                        list.remove(list.get(i));
                    }
                }

                if (list.size() == 0) {
                    privacyManager.deletePrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME);
                } else {
                    privacyManager.updatePrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME, list);
                    privacyManager.setActiveListName(XmppUtils.ACTIVE_PRIVACY_LIST_NAME);
                }

                ContactTable.getInstance().setBlockedStatus(jid, 0);
                triggerOnPrivacyIteamList(2, jid, true);
                return true;
            }
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();

        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();

        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();

        }
        triggerOnPrivacyIteamList(2, jid, true);
        return false;

    }

    public void setPrivacyUnableList() {
        if (xmpptcpConnection != null) {
            PrivacyListManager privacyManager = PrivacyListManager.getInstanceFor(xmpptcpConnection);
            try {
                List<PrivacyItem> list = null;
                boolean checkPrivacyStatus = true;
                try {

                    list = privacyManager.getPrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME).getItems();
                } catch (XMPPException.XMPPErrorException e) {
                    e.printStackTrace();
                /*item_not_found*/
                    XMPPError.Condition condition = e.getXMPPError().getCondition();
                    if (condition != null && condition.name() != null && condition.name().equals("item_not_found")) {

                    } else {
                        checkPrivacyStatus = false;
                    }

                }
                if (checkPrivacyStatus) {

                    if (list == null) {
                        list = new ArrayList<PrivacyItem>();
                    }
                    privacyManager.updatePrivacyList(XmppUtils.ACTIVE_PRIVACY_LIST_NAME, list);
                    privacyManager.setActiveListName(XmppUtils.ACTIVE_PRIVACY_LIST_NAME);

                }
            } catch (SmackException.NoResponseException e) {
                e.printStackTrace();

            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();

            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();

            }


        }
    }

    public int deleteXmppAccount() {

        AccountManager accountManager = AccountManager.getInstance(xmpptcpConnection);
        try {
            accountManager.deleteAccount();
            triggerOnDeleteXmppAccount(XmppConstant.SUCCESS);
            return XmppConstant.SUCCESS;
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
            triggerOnDeleteXmppAccount(XmppConstant.NORESPONSE);
            return XmppConstant.NORESPONSE;
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
            triggerOnDeleteXmppAccount(XmppConstant.EXCEPTION);
            return XmppConstant.EXCEPTION;
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            triggerOnDeleteXmppAccount(XmppConstant.NOTCONNECTED);
            return XmppConstant.NOTCONNECTED;
        }

    }

    public synchronized void triggerOnPrivacyIteamList(final int _case, final String jid, final boolean status) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                XmppLogger.printLog(TAG + " triggerOnPrivacyIteamList");


                ArrayList<PrivacyItemListner> list = (ArrayList<PrivacyItemListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(PrivacyItemListner.class);
                for (PrivacyItemListner privacyItemListner : list) {
                    switch (_case) {
                        case 1:
                            privacyItemListner.blockPrivacyUser(jid, status);
                            break;
                        case 2:
                            privacyItemListner.unBlockPrivacyuser(jid, status);
                            break;

                    }

                }
            }
        });

    }

    public synchronized void triggerOnDeleteXmppAccount(final int status) {
        ChatApplication.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                XmppLogger.printLog(TAG + " triggerOnDeleteXmppAccount");

                ArrayList<OnXmppDeleteAccountListner> list = (ArrayList<OnXmppDeleteAccountListner>) ChatApplication.getInstance().getXmppListners().getUiListernsArrayList(OnXmppDeleteAccountListner.class);
                for (OnXmppDeleteAccountListner deleteAccountListner : list) {
                    XmppLogger.printLog("Delete Chat Account status = " + status);
                    deleteAccountListner.deleteXmppAccountStatus(status);
                }
            }
        });

    }


    public enum ConnectionStates {
        Connected, Connecting, Disconnected, TimeOut
    }


    public enum SyncStatus {
        RUNNING, NOTRUNNING
    }

    private class XMPPConnectionListener implements ConnectionListener {

        @Override
        public void connected(final XMPPConnection connection) {
            XmppLogger.printLog(TAG + "  XMPPConnectionListener connected");
            connectionStates = ConnectionStates.Connected;
            addNeededListners();
            triggerMyXmpConnectionListner(1, null);

        }

        @Override
        public void connectionClosed() {
            XmppLogger.printLog(TAG + "  XMPPConnectionListener connectionClosed");
            connectionStates = ConnectionStates.Disconnected;
            removeNeededListners();
            triggerMyXmpConnectionListner(2, null);


        }

        @Override
        public void connectionClosedOnError(Exception e) {
            connectionStates = ConnectionStates.Disconnected;
            removeNeededListners();
            e.printStackTrace();
            XmppLogger.printLog(TAG + "  XMPPConnectionListener connectionClosedOnError " + e.getMessage());
            triggerMyXmpConnectionListner(2, e);

        }

        @Override
        public void reconnectingIn(int arg0) {
            XmppLogger.printLog(TAG + "  XMPPConnectionListener reconnectingIn " + arg0);
            connectionStates = ConnectionStates.Connecting;
        }

        @Override
        public void reconnectionFailed(Exception arg0) {
            arg0.printStackTrace();
            XmppLogger.printLog(TAG + "  XMPPConnectionListener reconnectionFailed " + arg0.getMessage());
            connectionStates = ConnectionStates.Disconnected;
            triggerMyXmpConnectionListner(2, null);

        }

        @Override
        public void reconnectionSuccessful() {
            XmppLogger.printLog(TAG + "  XMPPConnectionListener reconnectionSuccessful");
            connectionStates = ConnectionStates.Connected;
            triggerMyXmpConnectionListner(1, null);
        }

        @Override
        public void authenticated(XMPPConnection arg0, boolean arg1) {
            XmppLogger.printLog(TAG + "  XMPPConnectionListener authenticated");
            triggerMyXmpConnectionListner(3, null);
        }

    }
}


package com.chat.xmpp;

import com.chat.xmpp.modals.XmppAccount;

/**
 * Created by ubuntu on 15/11/16.
 */

public class XmppConstant {

    /*OpenFireDetails
    * http://192.168.1.37:9090/
    * username: admin
     * pass = admin
    * */

    public static final String VCARD_STATUS_FIELD = "STATUS";



    //live
    public static final String serviceName = "78.46.108.2";
    public static final String host = "78.46.108.2";
    //me
   /* public static final String serviceName = "192.168.1.161";
    public static final String host = "192.168.1.161";*/

    public static final String serviceNameGroup = "g";
    public static final String group_domain = "@" + serviceNameGroup + "." + serviceName;


    //public static final String MEDIA_FILE_URL = "http://205.147.102.6/p/sites/afrimack/api/users/attachment.json";
    /*live url*/
    //public static final String MEDIA_FILE_URL = "http://78.46.108.2/api/users/attachment.json";
    public static final String MEDIA_FILE_URL = "https://afrimack.com/api/users/attachment.json";

    public static final int port = 5222;
    public static final String defaultResource = "afrimack";
    public static final String defaultCountryPhoneCode = "91";
    public static final int defaultMessageIdLength = 20;
    public static final String defaultPassword = "123456";
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int PACKET_REPLY_TIMEOUT = 10000;

    public static final int RESULT_SUCCESS = 1;
    public static final int RESULT_ERROR = 0;
    public static final int RESULT_RETRY = 2;
    public static final int RESULT_NOT_SUPPORT = 3;
    public static final int NOT_CONNECTED = -1;
    public static final int LOGIN_SUCCESS = 1;
    public static final int LOGIN_TIMOUT = 2;
    public static final int LOGIN_EXCPTION = 0;

    public static final String VCARD_USER_IMG = "USERIMG";

    public static final int SUCCESS = 0;
    public static final int NORESPONSE = 1;
    public static final int NOTCONNECTED = 2;
    public static final int EXCEPTION = 3;


    public static XmppAccount getTestAccount() {
        String testUsername = "918561887459";
        String testUserPasswors = "123456";
        XmppAccount xmppAccount = new XmppAccount(serviceName, host, port, testUsername, testUserPasswors, defaultResource, defaultCountryPhoneCode);
        xmppAccount.setName("rty");
        return xmppAccount;
    }

    public static XmppAccount getTestAccount1() {
        String testUsername = "918058601168";
        String testUserPasswors = "123456";
        XmppAccount xmppAccount = new XmppAccount(serviceName, host, port, testUsername, testUserPasswors, defaultResource, defaultCountryPhoneCode);
        xmppAccount.setName("Tehju");
        return xmppAccount;
    }
}

package com.chat.xmpp.androidencoder;

import android.util.Base64;

import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.stringencoder.StringEncoder;

import java.io.UnsupportedEncodingException;

public class AndroidBase64UrlSafeEncoder implements StringEncoder {
    private static final int BASE64_ENCODER_FLAGS = 10;
    private static AndroidBase64UrlSafeEncoder instance;

    static {
        instance = new AndroidBase64UrlSafeEncoder();
    }

    private AndroidBase64UrlSafeEncoder() {
    }

    public static AndroidBase64UrlSafeEncoder getInstance() {
        return instance;
    }

    public String encode(String string) {
        try {
            return Base64.encodeToString(string.getBytes(StringUtils.UTF8), BASE64_ENCODER_FLAGS);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("UTF-8 not supported", e);
        }
    }

    public String decode(String string) {
        try {
            return new String(Base64.decode(string, BASE64_ENCODER_FLAGS), StringUtils.UTF8);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("UTF-8 not supported", e);
        }
    }
}

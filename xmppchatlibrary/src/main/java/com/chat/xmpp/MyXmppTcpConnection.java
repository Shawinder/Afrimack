package com.chat.xmpp;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

/**
 * Created by ubuntu on 16/11/16.
 */

public class MyXmppTcpConnection extends XMPPTCPConnection {
    public MyXmppTcpConnection(XMPPTCPConnectionConfiguration config) {
        super(config);
    }

    public MyXmppTcpConnection(CharSequence jid, String password) {
        super(jid, password);
    }

    public MyXmppTcpConnection(CharSequence username, String password, String serviceName) {
        super(username, password, serviceName);
    }

    public boolean isWasAuthenticated() {
        return super.wasAuthenticated;
    }

}

package com.chat.contacts;

/**
 * Created by ubuntu on 18/1/17.
 */

public class GroupMemberModal {
    String mem_jid;
    int mem_type;
    int mem_invite;
    String mem_invite_id;

    public String getMem_jid() {
        return mem_jid;
    }

    public void setMem_jid(String mem_jid) {
        this.mem_jid = mem_jid;
    }

    public int getMem_type() {
        return mem_type;
    }

    public void setMem_type(int mem_type) {
        this.mem_type = mem_type;
    }

    public int getMem_invite() {
        return mem_invite;
    }

    public void setMem_invite(int mem_invite) {
        this.mem_invite = mem_invite;
    }

    public String getMem_invite_id() {
        return mem_invite_id;
    }

    public void setMem_invite_id(String mem_invite_id) {
        this.mem_invite_id = mem_invite_id;
    }
}

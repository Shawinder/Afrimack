package com.chat.contacts;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;

import com.chat.ChatApplication;
import com.chat.modals.ContactDetailModal;
import com.chat.xmpp.XmppConstant;
import com.chat.xmpp.XmppLogger;
import com.chat.xmpp.XmppUtils;
import com.chat.xmpp.modals.XmppAccount;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import org.jivesoftware.smackx.search.ReportedData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ubuntu on 18/11/16.
 */

public class ContactHelper {

    public static final String TAG = "ContactHelper";
    static ContactHelper instance;
    static PhoneNumberUtil phoneNumberUtil;

    static {
        instance = new ContactHelper();
        phoneNumberUtil = PhoneNumberUtil.getInstance();
    }

    ContactLoadStatus contactLoadStatus = ContactLoadStatus.NOTRUNNING;
    ArrayList<ContactLoadedListner> contactLoadedListnerArrayList;

    private ContactHelper() {

    }

    public static ContactHelper getInstance() {
        return instance;
    }

    public void addContactLoadListner(ContactLoadedListner contactLoadedListner) {
        if (contactLoadedListner == null) {
            return;
        }
        if (contactLoadedListnerArrayList == null) {
            contactLoadedListnerArrayList = new ArrayList<>();
        }
        contactLoadedListnerArrayList.add(contactLoadedListner);
    }

    public void removeContactLoadListner(ContactLoadedListner contactLoadedListner) {
        if (contactLoadedListner == null) {
            return;
        }
        if (contactLoadedListnerArrayList != null) {
            contactLoadedListnerArrayList.remove(contactLoadedListner);
        }
    }

    public void removeAllContactLoadListners() {
        if (contactLoadedListnerArrayList != null) {
            contactLoadedListnerArrayList.clear();
        }
    }

    public synchronized void getAllContact(final Context context,
                                           final ContactLoadedListner contactLoadedListner,
                                           final int countryPhoneCode) {
        XmppLogger.printLog(TAG + " getAllContact");
        if (contactLoadStatus == ContactLoadStatus.RUNNING) {
            addContactLoadListner(contactLoadedListner);
            XmppLogger.printLog(TAG + " getAllContact already running wait for result.");
            return;
        }
        AsyncTask<Void, Void, ArrayList<ContactModal>> contactLoadThread = new AsyncTask<Void, Void, ArrayList<ContactModal>>() {

            @Override
            protected void onPostExecute(ArrayList<ContactModal> contactModalArrayList) {
                super.onPostExecute(contactModalArrayList);
                XmppLogger.printLog(TAG + " contactLoadThread onPostExecute.");
                XmppLogger.printLog(TAG + " contactLoadThread result=" + contactModalArrayList.toString());
                contactLoadStatus = ContactLoadStatus.NOTRUNNING;
                if (contactLoadedListnerArrayList != null) {
                    for (ContactLoadedListner contactLoadedListner1 : contactLoadedListnerArrayList) {
                        contactLoadedListner1.contactLoaded(contactModalArrayList);
                    }
                }
                removeContactLoadListner(contactLoadedListner);
            }

            @Override
            protected ArrayList<ContactModal> doInBackground(Void... voids) {
                XmppLogger.printLog(TAG + " contactLoadThread doInBackground.");
                contactLoadStatus = ContactLoadStatus.RUNNING;
                addContactLoadListner(contactLoadedListner);
                return getDeviceContacts(context.getContentResolver(), countryPhoneCode);
            }
        };
        contactLoadThread.execute();

    }

    public synchronized ArrayList<ContactModal> getDeviceContacts(ContentResolver cr, int countryPhoneCode) {
        XmppAccount xmppAccount = ChatApplication.getInstance().getXmppConnectionService().getXmpp().getXmppAccount();
        String selfNumber = xmppAccount.getUsername();
        ArrayList<ContactModal> contacts = new ArrayList<>();
        String regionCode = phoneNumberUtil.getRegionCodeForCountryCode(countryPhoneCode);

        Cursor pCur = cr.query(
                ContactUris.PHONE_CONTENT_URI,
                new String[]{ContactUris.PHONE_NUMBER,
                        ContactUris.PHONE_DISPLAYNAME,
                        ContactUris.PHONE_CONTACT_ID,
                        ContactUris.PHONE_RAW_CONTACT_ID}, null, null,
                ContactUris.PHONE_RAW_CONTACT_ID + " ASC");
        if (pCur != null) {
            if (pCur.getCount() > 0) {
                HashMap<Long, ArrayList<ContactModal>> phones = new HashMap<>();
                HashMap<String, String> numbersMap = new HashMap<>();
                while (pCur.moveToNext()) {
                    long PHONE_CONTACT_ID = pCur.getLong(pCur.getColumnIndex(ContactUris.PHONE_CONTACT_ID));
                    long PHONE_RAW_CONTACT_ID = pCur.getLong(pCur.getColumnIndex(ContactUris.PHONE_RAW_CONTACT_ID));
                    String number = pCur.getString(pCur.getColumnIndex(ContactUris.PHONE_NUMBER));
                    String formattedNumber = "";
                    if (number.trim().isEmpty()) {
                        continue;
                    }
                    try {

                        Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(number, regionCode);
                        if (!phoneNumberUtil.isValidNumber(phoneNumber)) {
                            continue;
                        }
                        formattedNumber = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                        if (formattedNumber == null || formattedNumber.trim().isEmpty()) {
                            continue;
                        }
                    } catch (NumberParseException e) {
                        continue;
                    }
                    if (numbersMap.containsKey(formattedNumber)) {
                        continue;
                    }
                    String userNameofthis = XmppUtils.removePlusfromStart(formattedNumber);
                    if (selfNumber.equals(userNameofthis)) {
                        continue;
                    }


                    ArrayList<ContactModal> curPhones = new ArrayList<>();
                    if (phones.containsKey(PHONE_CONTACT_ID)) {
                        curPhones = phones.get(PHONE_CONTACT_ID);
                    }

                    ContactModal contactModal = new ContactModal(XmppConstant.serviceName);
//                    contactModal.setId(PHONE_CONTACT_ID);
//                    contactModal.setRawContactId(PHONE_RAW_CONTACT_ID);
//                    contactModal.setDeviceNumber(number);
//                    contactModal.setFormattedNumber(formattedNumber);
                    contactModal.setDisplayName(pCur.getString(pCur.getColumnIndex(ContactUris.PHONE_DISPLAYNAME)));
                    curPhones.add(contactModal);

                    phones.put(PHONE_CONTACT_ID, curPhones);
                    numbersMap.put(formattedNumber, formattedNumber);
                }
                Cursor cur = cr.query(
                        ContactUris.CONTACT_CONTENT_URI,
                        new String[]{ContactUris.CONTACT_ID, ContactUris.HAS_PHONE_NUMBER},
                        ContactUris.HAS_PHONE_NUMBER + " > 0",
                        null, "");
                if (cur != null) {
                    if (cur.getCount() > 0) {
                        while (cur.moveToNext()) {
                            long id = cur.getLong(cur.getColumnIndex(ContactUris.CONTACT_ID));
                            if (phones.containsKey(id)) {
                                contacts.addAll(phones.get(id));
                            }
                        }
                    }
                    cur.close();
                }
            } else {
                XmppLogger.printLog(TAG + " getDeviceContacts pCur count 0");
            }

            pCur.close();
        } else {
            XmppLogger.printLog(TAG + " getDeviceContacts pCur is null");
        }
        return contacts;
    }

    public ContactDetailModal getContactDetail(Uri contactUri, int countryPhoneCode) {
        String regionCode = phoneNumberUtil.getRegionCodeForCountryCode(countryPhoneCode);
        ContactDetailModal contactDetailModal = null;
        Cursor c = ChatApplication.getInstance().getContentResolver().query(contactUri, null, null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                contactDetailModal = new ContactDetailModal();
                List<ContactDetailModal.Detail> list = new ArrayList<>();
                contactDetailModal.setDetailArrayList(list);

                long id = c.getLong(c.getColumnIndex(ContactsContract.Contacts._ID));
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                contactDetailModal.setName(name);
                String[] projection = {
                        ContactsContract.Data.CONTACT_ID,
                        ContactsContract.Contacts.Photo.PHOTO,
                        ContactsContract.Contacts.Data.DATA1,
                        ContactsContract.Contacts.Data.DATA2,
                        ContactsContract.Contacts.Data.MIMETYPE};
                Cursor cursor = ChatApplication.getInstance().getContentResolver().query(
                        ContactsContract.Data.CONTENT_URI, projection,
                        ContactsContract.Data.CONTACT_ID + " = ?",
                        new String[]{String.valueOf(id)},
                        null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {


                        int mimeIdx = cursor.getColumnIndex(
                                ContactsContract.Contacts.Data.MIMETYPE);
                        int photoIdx = cursor.getColumnIndex(
                                ContactsContract.Contacts.Photo.PHOTO);
                        int dataIdx = cursor.getColumnIndex(
                                ContactsContract.Contacts.Data.DATA1);
                        int dataIdx2 = cursor.getColumnIndex(
                                ContactsContract.Contacts.Data.DATA2);


                        do {
                            ContactDetailModal.Detail detail = new ContactDetailModal.Detail();
                            boolean isAddNeed = false;
                            String mime = cursor.getString(mimeIdx);
                            String data1 = cursor.getString(dataIdx);
                            XmppLogger.printLog(TAG + " mime=" + mime + ", data1=" + data1);
                            if (mime.equalsIgnoreCase(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {
                                try {
                                    if (data1 != null && !data1.isEmpty()) {

                                        Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(data1, regionCode);
                                        if (phoneNumberUtil.isValidNumber(phoneNumber)) {
                                            data1 = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
                                            if (data1 != null && !data1.isEmpty()) {
                                                detail.setValue(data1);
                                                if (!list.contains(detail)) {
                                                    int data2 = cursor.getInt(dataIdx2);
                                                    String valuetye = getPhoneLabel(data2);
                                                    detail.setValueType(valuetye);
                                                    detail.setIsNumber(ContactDetailModal.IS_NUMER_PHONE);
                                                    isAddNeed = true;
                                                }
                                            }
                                        }
                                    }
                                } catch (NumberParseException e) {
                                    e.printStackTrace();
                                }

                            } else if (mime.equalsIgnoreCase(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)) {
                                detail.setValue(data1);
                                int data2 = cursor.getInt(dataIdx2);
                                String valuetye = getEmailLabel(data2);
                                detail.setValueType(valuetye);
                                detail.setIsNumber(ContactDetailModal.IS_NUMER_EMAIL);
                                isAddNeed = true;
                            } else if (mime.equalsIgnoreCase(ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE)) {
                                detail.setValue(data1);
                                Integer data2 = cursor.getInt(dataIdx2);
                                String valuetye = getEventLabel(data2);
                                detail.setValueType(valuetye);
                                detail.setIsNumber(ContactDetailModal.IS_NUMER_EVENT);
                                isAddNeed = true;
                            } else if (mime.equalsIgnoreCase(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)) {
                                byte[] photoData = cursor.getBlob(photoIdx);
                                contactDetailModal.setImage(photoData);
                            }
                            if (isAddNeed) {
                                detail.setSelected(true);
                                list.add(detail);
                            }
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                }
            }
            c.close();
        }

        return contactDetailModal;
    }


    public String getPhoneLabel(int phoneType) {
        return ContactsContract.CommonDataKinds.Phone.getTypeLabel(ChatApplication.getInstance().getResources(), phoneType, "Custom").toString();
    }

    public String getEmailLabel(int emailType) {
        return ContactsContract.CommonDataKinds.Email.getTypeLabel(ChatApplication.getInstance().getResources(), emailType, "Custom").toString();
    }

    public String getEventLabel(Integer eventType) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return ContactsContract.CommonDataKinds.Event.getTypeLabel(ChatApplication.getInstance().getResources(), eventType, "Custom").toString();
        } else {
            if (eventType == null) {
                return "Other";
            }
            switch (eventType) {
                case 1:
                    return "Anniversary";
                case 3:
                    return "Birthday";
                case 2:
                    return "Other";
                default:
                    return "Custom";
            }
        }
    }

    public ArrayList<ContentValues> generateContentValues(ContactDetailModal contactDetailModal) {
        ArrayList<ContentValues> data = new ArrayList<ContentValues>();

        ContentValues row;
        List<ContactDetailModal.Detail> detailArrayList = contactDetailModal.getDetailArrayList();
        if (detailArrayList != null && detailArrayList.size() > 0) {

            for (ContactDetailModal.Detail detail : detailArrayList) {
                row = new ContentValues();
                if (detail.getIsNumber() == ContactDetailModal.IS_NUMER_PHONE) {
                    row.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                    row.put(ContactsContract.CommonDataKinds.Phone.NUMBER, detail.getValue());

                    int phoneType = getPhoneResourceType(detail.getValueType());
                    row.put(ContactsContract.CommonDataKinds.Phone.TYPE, phoneType);
                    if (phoneType == ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM) {
                        row.put(ContactsContract.CommonDataKinds.Phone.LABEL, detail.getValueType());
                    }
                } else if (detail.getIsNumber() == ContactDetailModal.IS_NUMER_EMAIL) {
                    row.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE);
                    row.put(ContactsContract.CommonDataKinds.Email.ADDRESS, detail.getValue());

                    int emailType = getEmailResourceType(detail.getValueType());
                    row.put(ContactsContract.CommonDataKinds.Email.TYPE, emailType);
                    if (emailType == ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM) {
                        row.put(ContactsContract.CommonDataKinds.Email.LABEL, detail.getValueType());
                    }
                } else if (detail.getIsNumber() == ContactDetailModal.IS_NUMER_EVENT) {
                    row.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE);
                    row.put(ContactsContract.CommonDataKinds.Event.DATA, detail.getValue());

                    int eventType = getEventResourceType(detail.getValueType());
                    row.put(ContactsContract.CommonDataKinds.Event.DATA2, eventType);
                    if (eventType == ContactsContract.CommonDataKinds.Event.TYPE_CUSTOM) {
                        row.put(ContactsContract.CommonDataKinds.Event.LABEL, detail.getValueType());
                    }
                }
                if (row.size() > 0) {
                    data.add(row);
                }
            }
        }
        return data;
    }

/*    public synchronized void filterLocalContacts(ArrayList<ContactModal> dbContacts, ArrayList<ContactModal> deviceContacts) {

        for (int i = 0; i < deviceContacts.size(); i++) {
            ContactModal deviceContact = deviceContacts.get(i);
            String deviceNumber = deviceContact.getFormattedNumber();

            for (int j = 0; j < dbContacts.size(); j++) {
                ContactModal dbContact = dbContacts.get(j);
                String dbNumber = dbContact.getFormattedNumber();
                if (dbNumber.equals(deviceNumber)) {
                    deviceContact.setAlreadyLocal(true);
                    deviceContact.setTempContactModal(dbContacts.remove(j));
                    break;
                }
            }
            if (dbContacts.size() == 0) {
                break;
            }
        }
    }*/

   /* public synchronized ArrayList<ContactModal> setRegisteredContact(List<ReportedData.Row> serverContacts, ArrayList<ContactModal> deviceContacts) {
        ArrayList<ContactModal> registerdContact = new ArrayList<>();
        for (int i = 0; i < deviceContacts.size(); i++) {

            String deviceNumberBack = deviceContacts.get(i).getFormattedNumber();
            deviceNumberBack = XmppUtils.removePlusfromStart(deviceNumberBack);
            boolean alreadyAdded = false;
            for (int j = 0; j < serverContacts.size(); j++) {
                String serverUsernameBack = serverContacts.get(j).getValues("Username").get(0);

                if (serverUsernameBack.equals(deviceNumberBack)) {
                    deviceContacts.get(i).setRegistered(true);
                    if (deviceContacts.get(i).needChange()) {
                        registerdContact.add(deviceContacts.get(i));
                    }
                    alreadyAdded = true;
                    break;
                }
            }
            if (!alreadyAdded) {
                deviceContacts.get(i).setRegistered(false);
                if (deviceContacts.get(i).needChange()) {
                    registerdContact.add(deviceContacts.get(i));
                }
            }

        }

        return registerdContact;
    }*/

    public int getPhoneResourceType(String lable) {
        if (lable == null) {
            return ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM;
        }
        switch (lable) {
            case "Home":
                return ContactsContract.CommonDataKinds.Phone.TYPE_HOME;
            case "Mobile":
                return ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;
            case "Work":
                return ContactsContract.CommonDataKinds.Phone.TYPE_WORK;
            case "Work Fax":
                return ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK;
            case "Home Fax":
                return ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME;
            case "Pager":
                return ContactsContract.CommonDataKinds.Phone.TYPE_PAGER;
            case "Other":
                return ContactsContract.CommonDataKinds.Phone.TYPE_OTHER;
            case "Callback":
                return ContactsContract.CommonDataKinds.Phone.TYPE_CALLBACK;
            case "Car":
                return ContactsContract.CommonDataKinds.Phone.TYPE_CAR;
            case "Company Main":
                return ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN;
            case "ISDN":
                return ContactsContract.CommonDataKinds.Phone.TYPE_ISDN;
            case "Main":
                return ContactsContract.CommonDataKinds.Phone.TYPE_MAIN;
            case "Other Fax":
                return ContactsContract.CommonDataKinds.Phone.TYPE_OTHER_FAX;
            case "Radio":
                return ContactsContract.CommonDataKinds.Phone.TYPE_RADIO;
            case "Telex":
                return ContactsContract.CommonDataKinds.Phone.TYPE_TELEX;
            case "TTY TDD":
                return ContactsContract.CommonDataKinds.Phone.TYPE_TTY_TDD;
            case "Work Mobile":
                return ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE;
            case "Work Pager":
                return ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER;
            case "Assistant":
                return ContactsContract.CommonDataKinds.Phone.TYPE_ASSISTANT;
            case "MMS":
                return ContactsContract.CommonDataKinds.Phone.TYPE_MMS;
            default:
                return ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM;
        }
    }

    public int getEmailResourceType(String lable) {
        if (lable == null) {
            return ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM;
        }
        switch (lable) {
            case "Home":
                return ContactsContract.CommonDataKinds.Email.TYPE_HOME;
            case "Mobile":
                return ContactsContract.CommonDataKinds.Email.TYPE_MOBILE;
            case "Work":
                return ContactsContract.CommonDataKinds.Email.TYPE_WORK;
            case "Other":
                return ContactsContract.CommonDataKinds.Email.TYPE_OTHER;
            default:
                return ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM;
        }
    }

    public int getEventResourceType(String lable) {
        if (lable == null) {
            return ContactsContract.CommonDataKinds.Event.TYPE_CUSTOM;
        }
        switch (lable) {
            case "Birthday":
                return ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY;
            case "Anniversary":
                return ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY;
            case "Other":
                return ContactsContract.CommonDataKinds.Event.TYPE_OTHER;
            default:
                return ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM;
        }
    }

    public enum ContactLoadStatus {
        RUNNING, NOTRUNNING
    }

    public interface ContactLoadedListner {
        void contactLoaded(ArrayList<ContactModal> contactModalArrayList);
    }

}

package com.chat.contacts;

import com.chat.database.tables.GroupMembers;
import com.chat.database.tables.RecentTable;
import com.chat.xmpp.XmppUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ubuntu on 18/11/16.
 */

public class ContactModal implements Serializable {

//    long id;
//    long rawContactId;
    public static ContactModal contactModal;

    public static ContactModal getContactModal() {
        return contactModal;
    }

    public static void setContactModal(ContactModal contactModal) {
        ContactModal.contactModal = contactModal;
    }

    String userId;
    String displayName;
    //    String serverDisplayName;
//    String deviceNumber;
//    String formattedNumber;
    int type = RecentTable.TYPE_SINGLE_CHAT;
    String jid;
    String status;
    String status_for_ui;
    String serverName;
    byte[] vcardAvatar;
    //    boolean registered = false;
//    boolean stranger = false;
//    boolean alreadyLocal = false;
    boolean newAdded = false;

//    String groupJid;

//    ContactModal tempContactModal;
//    List<GroupMemberModal> groupMemberModalList = new ArrayList<>();

    public ContactModal(String serverName) {
        this.serverName = serverName;
    }

   /* public List<GroupMemberModal> getGroupMemberModalList() {
        return groupMemberModalList;
    }

    public void setGroupMemberModalList(List<GroupMemberModal> groupMemberModalList) {
        this.groupMemberModalList = groupMemberModalList;
    }

    public ContactModal getTempContactModal() {
        return tempContactModal;
    }

    public void setTempContactModal(ContactModal tempContactModal) {
        this.tempContactModal = tempContactModal;
    }

    public String getGroupJid() {
        return groupJid;
    }

    public void setGroupJid(String groupJid) {
        this.groupJid = groupJid;
    }

    public long getRawContactId() {
        return rawContactId;
    }

    public void setRawContactId(long rawContactId) {
        this.rawContactId = rawContactId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }*/

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        setJid(userId + "@" + serverName);
    }

    public String getDisplayName() {
        return displayName == null ? "" : displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /*public String getServerDisplayName() {
        return serverDisplayName;
    }

    public void setServerDisplayName(String serverDisplayName) {
        this.serverDisplayName = serverDisplayName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getFormattedNumber() {
        return formattedNumber;
    }

    public void setFormattedNumber(String formattedNumber) {
        this.formattedNumber = formattedNumber;
        setJid(XmppUtils.removePlusfromStart(formattedNumber) + "@" + serverName);
    }*/


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getJid() {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

   /* public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public boolean isStranger() {
        return stranger;
    }

    public void setStranger(boolean stranger) {
        this.stranger = stranger;
    }

    public boolean isAlreadyLocal() {
        return alreadyLocal;
    }

    public void setAlreadyLocal(boolean alreadyLocal) {
        this.alreadyLocal = alreadyLocal;
    }*/

    public byte[] getVcardAvatar() {
        return vcardAvatar;
    }

    public void setVcardAvatar(byte[] vcardAvatar) {
        this.vcardAvatar = vcardAvatar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isNewAdded() {
        return newAdded;
    }

    public void setNewAdded(boolean newAdded) {
        this.newAdded = newAdded;
    }

    public String getStatus_for_ui() {
        return status_for_ui;
    }

    public void setStatus_for_ui(String status_for_ui) {
        this.status_for_ui = status_for_ui;
    }

   /* public boolean needChange() {
        if (tempContactModal == null) {
            return true;
        }
        if (tempContactModal.getDisplayName() != null || getDisplayName() != null) {
            if (tempContactModal.getDisplayName() != null && getDisplayName() != null) {
                if (!tempContactModal.getDisplayName().equals(getDisplayName())) {
                    return true;
                }
            } else if (tempContactModal.getDisplayName() != null && getDisplayName() == null) {
                return true;
            } else if (tempContactModal.getDisplayName() == null && getDisplayName() != null) {
                return true;
            }
        }
        return tempContactModal.isRegistered() != isRegistered();
    }*/

    @Override
    public String toString() {
        return getJid();
    }

   /* public List<String> getGroupOwners() {
        List<String> ownersList = new ArrayList<>();
        synchronized (groupMemberModalList) {
            for (GroupMemberModal groupMemberModal : groupMemberModalList) {
                if (groupMemberModal.getMem_type() == GroupMembers.MEMBER_TYPE_OWNER) {
                    ownersList.add(groupMemberModal.getMem_jid());
                }
            }
        }
        return ownersList;
    }

    public List<String> getGroupMembers() {
        List<String> membersList = new ArrayList<>();
        synchronized (groupMemberModalList) {
            for (GroupMemberModal groupMemberModal : groupMemberModalList) {
                if (groupMemberModal.getMem_type() == GroupMembers.MEMBER_TYPE_MEMBER) {
                    membersList.add(groupMemberModal.getMem_jid());
                }
            }
        }
        return membersList;
    }

    public List<String> getNotInvitedGroupMembers() {
        List<String> membersList = new ArrayList<>();
        synchronized (groupMemberModalList) {
            for (GroupMemberModal groupMemberModal : groupMemberModalList) {
                if (groupMemberModal.getMem_type() == GroupMembers.MEMBER_TYPE_MEMBER
                        && groupMemberModal.getMem_invite() == GroupMembers.MEMBER_INVITE_NOT_SENT) {
                    membersList.add(groupMemberModal.getMem_jid());
                }
            }
        }
        return membersList;
    }*/
}

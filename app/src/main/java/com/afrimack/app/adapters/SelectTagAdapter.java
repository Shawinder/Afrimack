package com.afrimack.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.fragments.DiscoverHome;
import com.afrimack.app.models.HomeResponseModel;

import java.util.List;

public class SelectTagAdapter extends RecyclerView.Adapter<SelectTagAdapter.MyViewHolder> {

    public static List<HomeResponseModel.ResponseBean.CategoryBean> horizontalList;
    private Context context;
    private View.OnClickListener OnClickListener;

    public SelectTagAdapter(Context context, List<HomeResponseModel.ResponseBean.CategoryBean> category, DiscoverHome OnClickListener) {

        this.context = context;
        this.horizontalList = category;
        this.OnClickListener = OnClickListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_tag, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tv_set_select_name.setText(horizontalList.get(position).getCategory_name());

        holder.img_tag_colse.getTag(horizontalList.get(position).getId());
        holder.img_tag_colse.setTag(position);
        holder.img_tag_colse.setOnClickListener(OnClickListener);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_set_select_name;
        private ImageView img_tag_colse;

        public MyViewHolder(View view) {
            super(view);
            tv_set_select_name = (TextView) view.findViewById(R.id.tv_set_select_name);
            img_tag_colse = (ImageView) view.findViewById(R.id.img_tag_colse);

        }
    }
}

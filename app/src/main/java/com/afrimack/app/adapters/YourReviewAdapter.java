package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.OfferUserActivity;
import com.afrimack.app.models.ReviewsResponseModel;
import com.squareup.picasso.Picasso;

import java.util.List;


public class YourReviewAdapter extends RecyclerView.Adapter<YourReviewAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    private List<ReviewsResponseModel.ResponseBean> moviesLists;


    public YourReviewAdapter(Context context, List<ReviewsResponseModel.ResponseBean> response) {
        this.context = context;
        this.moviesLists = response;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviews_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tv_review_text.setVisibility(View.VISIBLE);
        holder.tv_user_name_re.setText(moviesLists.get(position).getShort_name());// + " "+moviesLists.get(position).getLast_name());
        holder.tv_post_date.setText(moviesLists.get(position).getPurchase_date());
        holder.post_titel.setText(moviesLists.get(position).getPost_title());
        holder.rating_bar_rev.setRating(Float.parseFloat(String.valueOf(moviesLists.get(position).getRating())));
        holder.tv_review_text.setText(moviesLists.get(position).getReview());
        Picasso.with(context)
                .load(moviesLists.get(position).getImage())
                .into(holder.img_user);
    }

    @Override
    public int getItemCount() {
        return moviesLists.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_user_name_re, tv_post_date, post_titel, tv_review_text;
        public RatingBar rating_bar_rev;
        public ImageView img_user;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tv_user_name_re = (TextView) itemView.findViewById(R.id.tv_user_name_re);
            tv_post_date = (TextView) itemView.findViewById(R.id.tv_post_date);
            tv_review_text = (TextView) itemView.findViewById(R.id.tv_review_text);

            rating_bar_rev = (RatingBar) itemView.findViewById(R.id.rating_bar_rev);
            post_titel = (TextView) itemView.findViewById(R.id.post_titel);
            img_user = (ImageView) itemView.findViewById(R.id.img_user);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, OfferUserActivity.class);
            String single_user_id = String.valueOf(moviesLists.get(getPosition()).getUser_id());
            intent.putExtra("other_user_id", single_user_id);
            context.startActivity(intent);
        }
    }
}


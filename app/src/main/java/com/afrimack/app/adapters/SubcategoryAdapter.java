package com.afrimack.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.appbeans.CategoryBean;
import com.afrimack.app.appbeans.SubCategoryBean;
import com.afrimack.app.customprefs.Config;

import java.util.List;

/**
 * Created by Ayush on 4/6/2018.
 */

public class SubcategoryAdapter extends RecyclerView.Adapter<SubcategoryAdapter.MyViewHolder> {

    Context context;
    LayoutInflater layoutInflater;
    static public List<SubCategoryBean> mLIst;

    public SubcategoryAdapter(Context context, List<SubCategoryBean> mLIst) {
        this.mLIst = mLIst;
        this.context = context;
    }


    @Override
    public SubcategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_category_row_item, parent, false);
        return new SubcategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SubcategoryAdapter.MyViewHolder holder, final int position) {

        holder.txt_subCategory.setText(mLIst.get(position).getCategory_name());
        holder.txt_subCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mLIst.get(position).getId()!=null) {
                    view.setBackground(context.getResources().getDrawable(R.drawable.categories_background_filled));
                    String sub_id = String.valueOf(mLIst.get(position).getId());
                    if(sub_id.equalsIgnoreCase("0"))
                    {
                        Config.setSortSubCategory("",context);
                    }
                    else
                    {
                        Config.setSortSubCategory(sub_id,context);
                    }

                    Intent intent = new Intent(context, ActivityHome.class);
                    context.startActivity(intent);
                    ((Activity) context).finish();

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mLIst.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_subCategory;

        public MyViewHolder(View itemView) {
            super(itemView);

            txt_subCategory = (TextView)itemView.findViewById(R.id.txt_subCategory);

        }
    }
}

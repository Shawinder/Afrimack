package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityDiscoverDetail;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.HomeResponseModel;

import java.util.List;

public class SolventViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView countryName, tv_grid_prige, sold, txt_swap_deal, txt_or, tv_premium;
    public ImageView countryPhoto;
    private List<HomeResponseModel.ResponseBean.PostsBean> List;
    Context context;
    public LinearLayout ll_fram_hot;
    public CardView mCardView;
    public LinearLayout mFramelayout;
    public ProgressBar progress_product;


    public SolventViewHolders(View itemView, Context context, List<HomeResponseModel.ResponseBean.PostsBean> itemList) {
        super(itemView);
        itemView.setOnClickListener(this);
        mFramelayout = (LinearLayout) itemView.findViewById(R.id.frame_staggered_grid);
        mCardView = (CardView) itemView.findViewById(R.id.card_view);
        countryName = (TextView) itemView.findViewById(R.id.country_name);
        countryPhoto = (ImageView) itemView.findViewById(R.id.country_photo);
        tv_grid_prige = (TextView) itemView.findViewById(R.id.tv_grid_prige);
        ll_fram_hot = (LinearLayout) itemView.findViewById(R.id.ll_fram_hot);
        sold = (TextView) itemView.findViewById(R.id.sold);
        txt_swap_deal = (TextView) itemView.findViewById(R.id.txt_swap_deal);
        txt_or = (TextView) itemView.findViewById(R.id.txt_or);
        tv_premium = (TextView) itemView.findViewById(R.id.tv_premium);
        progress_product = (ProgressBar) itemView.findViewById(R.id.progress_product);
        this.context = context;
        this.List = itemList;
    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent(context, ActivityDiscoverDetail.class);
        String post_id = String.valueOf(List.get(getPosition()).getPost_id());
        Config.setPostId(post_id, context);
        context.startActivity(intent);

    }
}

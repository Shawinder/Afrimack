package com.afrimack.app.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.ChatResponseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private Drawable itemPic;
    private List<ChatResponseModel.ResponseBean.ChatBean> chatList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;



    public ChatAdapter(Context context, List<ChatResponseModel.ResponseBean.ChatBean> chat, View.OnClickListener onClickListener) {
        this.chatList = chat;
        this.context = context;
        this.onClickListener = onClickListener;
    }


    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_offer_list, parent, false);
        return new ChatAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChatAdapter.MyViewHolder holder, int position) {

        String offeruser_id = String.valueOf(chatList.get(position).getUser_id());
        if(offeruser_id.equalsIgnoreCase(Config.getUserId(context))){
            holder.lay_me.setVisibility(View.VISIBLE);
            holder.lay_other.setVisibility(View.GONE);
            holder.me_offer.setText(chatList.get(position).getMessage());
            holder.me_date.setText(chatList.get(position).getDate());

            holder.me_profile_chat.setTag(position);
            holder.me_profile_chat.setOnClickListener(onClickListener);

            Picasso
                    .with(context)
                    .load(chatList.get(position).getImage())
                    .placeholder(R.drawable.dami_image2)
                    .into(holder.me_profile_chat);
        }else {

            holder.lay_other.setVisibility(View.VISIBLE);
            holder.lay_me.setVisibility(View.GONE);
            holder.other_offer.setText(chatList.get(position).getMessage());
            holder.other_date.setText(chatList.get(position).getDate());

            holder.other_profile_chat.setTag(position);
            holder.other_profile_chat.setOnClickListener(onClickListener);
            Picasso
                    .with(context)
                    .load(chatList.get(position).getImage())
                    .placeholder(R.drawable.dami_image2)
                    .into(holder.other_profile_chat);
        }



    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView me_profile_chat,other_profile_chat;
        private TextView me_offer,other_offer;

        private TextView me_date,other_date;
        private LinearLayout lay_me,lay_other;

        public MyViewHolder(View itemView) {
            super(itemView);
            me_profile_chat = (ImageView)itemView.findViewById(R.id.me_profile_chat);
            other_profile_chat = (ImageView)itemView.findViewById(R.id.other_profile_chat);

            me_offer = (TextView)itemView.findViewById(R.id.me_offer);
            other_offer = (TextView)itemView.findViewById(R.id.other_offer);

            me_date = (TextView)itemView.findViewById(R.id.me_date);
            other_date = (TextView)itemView.findViewById(R.id.other_date);

            lay_me= (LinearLayout) itemView.findViewById(R.id.lay_me);
            lay_other = (LinearLayout) itemView.findViewById(R.id.lay_other);

        }
    }


}

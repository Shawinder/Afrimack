package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityChatRequest;
import com.afrimack.app.activities.SingleChatActivity;
import com.brsoftech.emozi.EmojiconTextView;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

/**
 * Created by root on 15/12/17.
 */

public class ChatOverviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private boolean isLoading;
    Context context;
    View.OnClickListener onClickListener;
    private boolean loading;
    private List<com.afrimack.app.appbeans.Response> dataListFriend;

    public ChatOverviewAdapter(Context context, List<com.afrimack.app.appbeans.Response> dataListFriend) {
        this.context = context;
        this.dataListFriend = dataListFriend;

    }


    @Override
    public int getItemViewType(int position) {
        return dataListFriend.size() > 0 ? VIEW_ITEM : VIEW_PROG;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.chat_overview_list_item, parent, false);

            vh = new UserViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_loading, parent, false);

            vh = new LoadingViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final String name = dataListFriend.get(position).getUserDetails().getFirstName();
            final UserViewHolder holder1 = (UserViewHolder) holder;
            holder1.tv_user_name.setText(name);

            final int pos = position;
            try {
                Picasso.with(context)
                        .load(dataListFriend.get(pos).getUserDetails().getPicture())
                        .placeholder(R.drawable.dami_image2)
                        .into(holder1.ivProfilePic_chat);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (dataListFriend.get(pos).getType().equalsIgnoreCase("confirm_request")) {
                holder1.tv_message.setText("");
            } else {
                if (dataListFriend.get(pos).getLastMessage() != null) {
                    if (dataListFriend.get(pos).getLastMessage().getText().equalsIgnoreCase("")) {
                        holder1.tv_message.setText("Image");
                    } else {
                        String text = dataListFriend.get(pos).getLastMessage().getText();
                        holder1.tv_message.setText(dataListFriend.get(pos).getLastMessage().getText());

                    }

                    holder1.tv_message_date.setText(getDateCurrentTimeZone(dataListFriend.get(pos).getLastMessage().getTimestamp()));
                }
            }
            if (dataListFriend.get(pos).getUnreadMessage() == 0) {
                holder1.tv_unread_message_count.setVisibility(View.GONE);
            } else {
                holder1.tv_unread_message_count.setVisibility(View.VISIBLE);
                holder1.tv_unread_message_count.setText(String.valueOf(dataListFriend.get(pos).getUnreadMessage()));
            }
            holder1.notifiction_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (dataListFriend.get(pos).getType().equalsIgnoreCase("chat_request")) {
                        Intent mIntent = new Intent(context, ActivityChatRequest.class);
                        mIntent.putExtra("U_ID", dataListFriend.get(pos).getFirebaseUID());
                        mIntent.putExtra("time_stamp", dataListFriend.get(pos).getLastMessage().getTimestamp());
                        mIntent.putExtra("user_name", dataListFriend.get(pos).getUserDetails().getFirstName());
                        mIntent.putExtra("user_image", dataListFriend.get(pos).getUserDetails().getPicture());
                        mIntent.putExtra("other_user_id", dataListFriend.get(pos).getUserDetails().getUser_id());
                        mIntent.putExtra("blocked_status", dataListFriend.get(pos).getUserDetails().getBlockedStatus());
                        mIntent.putExtra("blocked_by", dataListFriend.get(pos).getUserDetails().getPhp_block_by());
                        mIntent.putExtra("php_block_id", dataListFriend.get(pos).getUserDetails().getPhp_block_id());
                        mIntent.putExtra("from", "from");
                        context.startActivity(mIntent);
                    } else {
                        Intent mIntent = new Intent(context, SingleChatActivity.class);
                        mIntent.putExtra("U_ID", dataListFriend.get(pos).getFirebaseUID());
                        mIntent.putExtra("user_image", dataListFriend.get(pos).getUserDetails().getPicture());
                        mIntent.putExtra("other_user_id", dataListFriend.get(pos).getUserDetails().getUser_id());
                        mIntent.putExtra("blocked_status", dataListFriend.get(pos).getUserDetails().getBlockedStatus());
                        mIntent.putExtra("blocked_by", dataListFriend.get(pos).getUserDetails().getPhp_block_by());
                        mIntent.putExtra("php_block_id", dataListFriend.get(pos).getUserDetails().getPhp_block_id());
                        mIntent.putExtra("from", "from");
                        context.startActivity(mIntent);
                    }

                }
            });

        } else if (holder instanceof LoadingViewHolder)

        {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }


    @Override
    public int getItemCount() {
        return dataListFriend.size();
    }

    public boolean isAlpha(String name) {
        return name.matches("[a-zA-Z]+");
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_user_name, tv_message_date, tv_unread_message_count;
        private ImageView ivProfilePic_chat;
        private EmojiconTextView tv_message, tv_message_emoji;
        private LinearLayout notifiction_main;

        public UserViewHolder(View itemView) {
            super(itemView);
            tv_user_name = (TextView) itemView.findViewById(R.id.tv_user_name);
            tv_message = (EmojiconTextView) itemView.findViewById(R.id.tv_message);
            tv_message_emoji = (EmojiconTextView) itemView.findViewById(R.id.tv_message_emoji);
            tv_message_date = (TextView) itemView.findViewById(R.id.tv_message_date);
            tv_unread_message_count = (TextView) itemView.findViewById(R.id.tv_unread_message_count);
            ivProfilePic_chat = (ImageView) itemView.findViewById(R.id.ivProfilePic_chat);
            notifiction_main = (LinearLayout) itemView.findViewById(R.id.notifiction_main);
        }
    }

    public String getDateCurrentTimeZone(long timestamp) {
        try {
            return DateFormat.format("dd.MM.yy", new Date(timestamp)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}

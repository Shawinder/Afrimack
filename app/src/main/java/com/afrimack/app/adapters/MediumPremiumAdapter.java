package com.afrimack.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.models.PremiumResponseModel;

import java.util.List;

public class MediumPremiumAdapter extends RecyclerView.Adapter<MediumPremiumAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    private List<PremiumResponseModel.ResponseBean.MEDIUMBean> mediumlist;
    View.OnClickListener onClickListener;

    public MediumPremiumAdapter(Context context, List<PremiumResponseModel.ResponseBean.MEDIUMBean> mediumlist, View.OnClickListener onClickListener) {

        this.mediumlist = mediumlist;
        this.context = context;
        this.onClickListener=onClickListener;
    }


    @Override
    public MediumPremiumAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.premium_list, parent, false);
        return new MediumPremiumAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MediumPremiumAdapter.MyViewHolder holder, int position) {

        if(mediumlist.get(position).getPackage_type().equalsIgnoreCase("HOT")){
            holder.flay_hot.setVisibility(View.VISIBLE);
            holder.flay_top.setVisibility(View.GONE);
            holder.flay_hot_top.setVisibility(View.GONE);
            holder.tv_hot_day.setText(mediumlist.get(position).getDays()+ " Days");
            holder.tv_hot_prise.setText("$ "+mediumlist.get(position).getPrice());
            holder.flay_hot.setTag(position);
            holder.flay_hot.setOnClickListener(onClickListener);
        }

        if(mediumlist.get(position).getPackage_type().equalsIgnoreCase("TOP")){
            holder.flay_hot.setVisibility(View.GONE);
            holder.flay_top.setVisibility(View.VISIBLE);
            holder.flay_hot_top.setVisibility(View.GONE);
            holder.tv_top_day.setText(mediumlist.get(position).getDays()+ " Days");
            holder.tv_top_prise.setText("$ "+mediumlist.get(position).getPrice());
            holder.flay_top.setTag(position);
            holder.flay_top.setOnClickListener(onClickListener);
        }

        if(mediumlist.get(position).getPackage_type().equalsIgnoreCase("HOT & TOP")){
            holder.flay_hot.setVisibility(View.GONE);
            holder.flay_top.setVisibility(View.GONE);
            holder.flay_hot_top.setVisibility(View.VISIBLE);
            holder.tv_hot_top_day.setText(mediumlist.get(position).getDays()+ " Days");
            holder.tv_hot_top_prise.setText("$ "+mediumlist.get(position).getPrice());
            holder.flay_hot_top.setTag(position);
            holder.flay_hot_top.setOnClickListener(onClickListener);
        }



    }

    @Override
    public int getItemCount() {
        return mediumlist.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private FrameLayout flay_hot,flay_top,flay_hot_top;
        private TextView tv_hot_day,tv_hot_prise;
        private TextView tv_top_day,tv_top_prise;
        private TextView tv_hot_top_day,tv_hot_top_prise;



        public MyViewHolder(View itemView) {
            super(itemView);

            flay_hot = (FrameLayout) itemView.findViewById(R.id.flay_hot);
            flay_top = (FrameLayout) itemView.findViewById(R.id.flay_top);
            flay_hot_top = (FrameLayout) itemView.findViewById(R.id.flay_hot_top);

            tv_hot_day = (TextView)itemView.findViewById(R.id.tv_hot_day);
            tv_hot_prise = (TextView)itemView.findViewById(R.id.tv_hot_prise);

            tv_top_day = (TextView)itemView.findViewById(R.id.tv_top_day);
            tv_top_prise = (TextView)itemView.findViewById(R.id.tv_top_prise);

            tv_hot_top_day = (TextView)itemView.findViewById(R.id.tv_hot_top_day);
            tv_hot_top_prise = (TextView)itemView.findViewById(R.id.tv_hot_top_prise);
        }


    }

}

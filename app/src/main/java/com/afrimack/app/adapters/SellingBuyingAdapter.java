package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityDiscoverDetail;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.SellingResponseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SellingBuyingAdapter extends RecyclerView.Adapter<SellingBuyingAdapter.MyViewHolder> {

    private Drawable itemPic;
    static public List<SellingResponseModel.ResponseBean.PostsBean> sellList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;

    public SellingBuyingAdapter(Context context, List<SellingResponseModel.ResponseBean.PostsBean> posts, View.OnClickListener onClickListener) {
        this.sellList = posts;
        this.context = context;
        this.onClickListener = onClickListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_selling_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvSellingProductName.setText(sellList.get(position).getTitle());
        try {
            String sub = sellList.get(position).getPrice();
            String s = sub.substring(0, sub.indexOf("."));
            if (sub.contains(".00")) {
                holder.tvSellingProductPrice.setText(s);
            } else {
                holder.tvSellingProductPrice.setText(sub);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        Picasso.with(context)
                .load(sellList.get(position).getImage())
                .placeholder(R.drawable.camera)
                .error(R.drawable.camera)
                .into(holder.ivSellingProductBg);
        holder.tvSellingHours.setText(sellList.get(position).getDateTime());
        holder.tvLikes_total.setText(String.valueOf(sellList.get(position).getFollowing()));

        holder.ivSellingItemClose.setTag(position);
        holder.ivSellingItemClose.setOnClickListener(onClickListener);
        if (sellList.get(position).getPost_status().equalsIgnoreCase("C")) {
            holder.sold.setVisibility(View.VISIBLE);
            holder.sold.setText(R.string.deal_done);
        }
        if (sellList.get(position).getPost_status().equalsIgnoreCase("P")) {
            holder.sold.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return sellList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivSellingProductBg;
        public TextView tvSellingProductName;
        public TextView tvSellingProductPrice;
        public TextView tvLikes_total;
        public TextView tvSellingHours;
        public ImageView ivSellingItemClose;
        private TextView sold;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ivSellingProductBg = (ImageView) itemView.findViewById(R.id.ivSellingProductBg);
            tvSellingProductName = (TextView) itemView.findViewById(R.id.tvSellingProductName);
            tvSellingProductPrice = (TextView) itemView.findViewById(R.id.tvSellingProductPrice);
            tvLikes_total = (TextView) itemView.findViewById(R.id.tvLikes_total);
            tvSellingHours = (TextView) itemView.findViewById(R.id.tvSellingHours);
            sold = (TextView) itemView.findViewById(R.id.sold);
            ivSellingItemClose = (ImageView) itemView.findViewById(R.id.ivSellingItemClose);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityDiscoverDetail.class);
            String post_id = String.valueOf(sellList.get(getPosition()).getPost_id());
            Config.setPostId(post_id, context);
            context.startActivity(intent);
        }
    }
}

package com.afrimack.app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.models.CurencyNameResponseModel;

import java.util.List;

/**
 * Created by sukhdeepkaur on 5/15/18.
 */

public class CustomSpinnerAdapterHours extends ArrayAdapter<String> {

        private Context context;
        List<String> mLIst;
        public CustomSpinnerAdapterHours(@NonNull Context context, int resource, List<String> mLIst) {
            super(context, resource);
            this.context=context;
            this.mLIst=mLIst;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater.from(context));
            View row = inflater.inflate(R.layout.spinner_item_layout, parent,
                    false);
            TextView make = (TextView) row.findViewById(R.id.txt_make);

            if(position==0 )
            {
//            mLIst.add(0,"Choose");
                make.setTextColor(context.getResources().getColor(R.color.color_white));
            }
            else
            {
                make.setTextColor(context.getResources().getColor(R.color.color_black));
            }
            make.setText(mLIst.get(position));
            return row;
        }

        @Override
        public int getCount() {
            return mLIst.size();
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater.from(context)) ;
            View row = inflater.inflate(R.layout.spinner_item_layout, parent,
                    false);
            TextView make = (TextView) row.findViewById(R.id.txt_make);
            if(position==0)
            {
                make.setVisibility(View.GONE);
            }
            else
            {
                make.setVisibility(View.VISIBLE);
            }
            make.setText(mLIst.get(position));
            make.setTextColor(context.getResources().getColor(R.color.color_black));
            return row;
        }


}

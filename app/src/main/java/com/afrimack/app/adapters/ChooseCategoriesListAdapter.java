package com.afrimack.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.afrimack.app.Interfaces.itemChoose;
import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.Subcategory;
import com.afrimack.app.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import static com.afrimack.app.utils.CommonUtils.CategoryName;

public class ChooseCategoriesListAdapter extends RecyclerView.Adapter<ChooseCategoriesListAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<String> category_list;
    int row_index;
    String status="0";
    private itemChoose itemChoose;

    public ChooseCategoriesListAdapter(Context context,ArrayList<String> category_list) {
        this.category_list = category_list;
        this.context=context;
        this. itemChoose = itemChoose;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_category_list_view, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.txtVwCategoryTitle.setText(category_list.get(position));

        holder.txtVwCategoryTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


              //  itemChoose.onClick(CommonUtils.getCategories().get(position));
                if (status.equalsIgnoreCase("0")){

                    if(CommonUtils.getSub_categories().get(position)!=null&& CommonUtils.getSub_categories().get(position).size()>0) {
                        holder.listVwSubCategory.setVisibility(View.VISIBLE);
                        String id = String.valueOf(CommonUtils.getCategory_id().get(position));
                      /*  Config.setSortSpecialCategory("", context);
                        Config.setSortCategory(id, context);*/
                        SubCategoryNewAdapter subcategory_list_adapter=new SubCategoryNewAdapter(context,position,CommonUtils.getCategories().get(position));

                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                      //  mLayoutManager.setReverseLayout(true);
                        holder.listVwSubCategory.setLayoutManager(mLayoutManager);
                        holder.listVwSubCategory.setAdapter(subcategory_list_adapter);
                    }
                    else{
                        CategoryName=CommonUtils.getCategories().get(position);
                        Intent i = new Intent("android.intent.action.CATEGORY");
                        i.putExtra("category_name",CommonUtils.getCategories().get(position));
                        context.sendBroadcast(i);
                        holder.listVwSubCategory.setVisibility(View.GONE);
                        String id = String.valueOf(CommonUtils.getCategory_id().get(position));
                       /* Config.setSortSpecialCategory("", context);
                        Config.setSortCategory(id, context);*/

                       CommonUtils.Id=id;
                        ((Activity) context).finish();
                     /*   Intent intent = new Intent(context, ActivityHome.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();*/
                    }

                   holder.listVwSubCategory.setVisibility(View.VISIBLE);
                   status="1";
                }
                else {
                    status="0";
                    CategoryName=CommonUtils.getCategories().get(position);
                    Intent i = new Intent("android.intent.action.CATEGORY");
                    i.putExtra("category_name",CommonUtils.getCategories().get(position));
                    context.sendBroadcast(i);
                    holder.listVwSubCategory.setVisibility(View.GONE);
                    String id = String.valueOf(CommonUtils.getCategory_id().get(position));
                       /* Config.setSortSpecialCategory("", context);
                        Config.setSortCategory(id, context);*/

                    CommonUtils.Id=id;

                    holder.listVwSubCategory.setVisibility(View.GONE);

                }
            }
        });




    }

    @Override
    public int getItemCount() {
        return CommonUtils.getCategories().size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtVwCategoryTitle;
        private RecyclerView listVwSubCategory;


        public MyViewHolder(View itemView) {
            super(itemView);

            txtVwCategoryTitle = (TextView) itemView.findViewById(R.id.txtVwCategoryTitle);
            listVwSubCategory = (RecyclerView) itemView.findViewById(R.id.listVwSubCategory);
        }
    }


}

package com.afrimack.app.adapters;

import android.content.Context;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.models.NotificationAllResponseModel;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading;
    Context context;
    public static List<NotificationAllResponseModel.ResponseBean.NotificationsBean> notificationList;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    View.OnClickListener onClickListener;
    private boolean loading;


    public ContactAdapter(RecyclerView recyclerNews, Context context, List<NotificationAllResponseModel.ResponseBean.NotificationsBean> notiList, View.OnClickListener onClickListener, OnLoadMoreListener mOnLoadMoreListener) {
        this.notificationList = notiList;
        this.context = context;
        this.onClickListener = onClickListener;


        if (recyclerNews.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerNews
                    .getLayoutManager();


            recyclerNews
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();

                            if (totalItemCount > 9) {
                                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                    // End has been reached
                                    // Do something
                                    if (onLoadMoreListener != null) {
                                        onLoadMoreListener.onLoadMore();
                                    }
                                    loading = true;
                                }
                            }
                        }
                    });
        }
    }


    @Override
    public int getItemViewType(int position) {
        return notificationList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.fragment_news_list_item, parent, false);

            vh = new UserViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_loading, parent, false);

            vh = new LoadingViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserViewHolder) {
            UserViewHolder holder1 = (UserViewHolder) holder;
            if (notificationList.get(position).getType().equalsIgnoreCase("admin")) {
                holder1.ll_admin.setVisibility(View.VISIBLE);
                holder1.ll_message.setVisibility(View.GONE);
                holder1.ll_profile.setVisibility(View.GONE);
                holder1.tv_admin_mess.setText(notificationList.get(position).getMessage());
                holder1.tv_admin_HoursAgo.setText(notificationList.get(position).getDuration());

                if (notificationList.get(position).getIs_read().equalsIgnoreCase("y")) {
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                } else {
                  //holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                  holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.light_color_gray));
                 //   holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                }

                holder1.notifiction_main.setTag(position);
                holder1.notifiction_main.setOnClickListener(onClickListener);
            } else if (notificationList.get(position).getType().equalsIgnoreCase("chat_message")) {
                holder1.ll_profile.setVisibility(View.VISIBLE);
                holder1.ll_message.setVisibility(View.GONE);
                holder1.ll_admin.setVisibility(View.GONE);
                holder1.tv_profile_mess.setText(notificationList.get(position).getMessage());
                holder1.tv_profile_HoursAgo.setText(notificationList.get(position).getDuration());
                holder1.img_chat_user_image.setVisibility(View.VISIBLE);
                holder1.img_profile_image.setVisibility(View.GONE);
                if (notificationList.get(position).getIs_read().equalsIgnoreCase("y")) {
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                } else {
                 // holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                  holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.light_color_gray));
                  //  holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                }
                holder1.notifiction_main.setTag(position);
                holder1.notifiction_main.setOnClickListener(onClickListener);
                holder1.img_profirle_image.setVisibility(View.GONE);
            } else if (notificationList.get(position).getType().equalsIgnoreCase("chat_request")) {

                holder1.ll_profile.setVisibility(View.VISIBLE);
                holder1.ll_message.setVisibility(View.GONE);
                holder1.ll_admin.setVisibility(View.GONE);
                holder1.tv_profile_mess.setText(notificationList.get(position).getMessage());
                holder1.tv_profile_HoursAgo.setText(notificationList.get(position).getDuration());
                holder1.img_chat_user_image.setVisibility(View.GONE);
                holder1.img_profile_image.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(notificationList.get(position).getUser_image())
                        .placeholder(R.drawable.dami_image2)
                        .into(holder1.img_profile_image);

                if (notificationList.get(position).getIs_read().equalsIgnoreCase("y")) {
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                } else {
                  // holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                   holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.light_color_gray));
                  //  holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                }
                holder1.notifiction_main.setTag(position);
                holder1.notifiction_main.setOnClickListener(onClickListener);
                holder1.img_profirle_image.setVisibility(View.GONE);
            } else if (notificationList.get(position).getType().equalsIgnoreCase("profile_online")) {
                holder1.ll_profile.setVisibility(View.VISIBLE);
                holder1.ll_message.setVisibility(View.GONE);
                holder1.ll_admin.setVisibility(View.GONE);
                holder1.tv_profile_mess.setText(notificationList.get(position).getMessage());
                holder1.tv_profile_HoursAgo.setText(notificationList.get(position).getDuration());
                if (notificationList.get(position).getIs_read().equalsIgnoreCase("y")) {
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                } else {
                  //  holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.light_color_gray));
                   // holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                }
                holder1.notifiction_main.setTag(position);
                holder1.notifiction_main.setOnClickListener(onClickListener);
                holder1.img_profirle_image.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(notificationList.get(position).getUser_image())
                        .placeholder(R.drawable.user_round)
                        .into(holder1.img_profirle_image);

            } else if (notificationList.get(position).getType().equalsIgnoreCase("block user")) {
                holder1.ll_message.setVisibility(View.VISIBLE);
                holder1.ll_admin.setVisibility(View.GONE);
                holder1.ll_profile.setVisibility(View.GONE);

                holder1.tvQuestionPosed.setText(notificationList.get(position).getMessage());
                holder1.tvHoursAgo.setText(notificationList.get(position).getDuration());


                Picasso.with(context)
                        .load(notificationList.get(position).getUser_image())
                        .placeholder(R.drawable.dami_image2)
                        .into(holder1.ivProfilePic);
                holder1.ivItemPic.setVisibility(View.GONE);

                if (notificationList.get(position).getIs_read().equalsIgnoreCase("y")) {
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                } else {
                   // holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.light_color_gray));
                    //holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                }

                holder1.notifiction_main.setTag(position);
                holder1.notifiction_main.setOnClickListener(onClickListener);
            } else if (notificationList.get(position).getType().equalsIgnoreCase("unblock user")) {
                holder1.ll_message.setVisibility(View.VISIBLE);
                holder1.ll_admin.setVisibility(View.GONE);
                holder1.ll_profile.setVisibility(View.GONE);

                holder1.tvQuestionPosed.setText(notificationList.get(position).getMessage());
                holder1.tvHoursAgo.setText(notificationList.get(position).getDuration());


                Picasso.with(context)
                        .load(notificationList.get(position).getUser_image())
                        .placeholder(R.drawable.dami_image2)
                        .into(holder1.ivProfilePic);
                holder1.ivItemPic.setVisibility(View.GONE);

                if (notificationList.get(position).getIs_read().equalsIgnoreCase("y")) {
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                } else {
                //   holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                   holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.light_color_gray));
                  //  holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                }

                holder1.notifiction_main.setTag(position);
                holder1.notifiction_main.setOnClickListener(onClickListener);
            } else {
                holder1.ll_message.setVisibility(View.VISIBLE);
                holder1.ll_admin.setVisibility(View.GONE);
                holder1.ll_profile.setVisibility(View.GONE);

                holder1.tvQuestionPosed.setText(notificationList.get(position).getMessage());
                holder1.tvHoursAgo.setText(notificationList.get(position).getDuration());


                Picasso.with(context)
                        .load(notificationList.get(position).getUser_image())
                        .placeholder(R.drawable.dami_image2)
                        .into(holder1.ivProfilePic);

                Picasso.with(context)
                        .load(notificationList.get(position).getPost_image())
                        .placeholder(R.drawable.no_image)
                        .into(holder1.ivItemPic);

                if (notificationList.get(position).getIs_read().equalsIgnoreCase("y")) {
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                } else {
                   // holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                    holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.light_color_gray));
                    //holder1.notifiction_main.setBackgroundColor(context.getResources().getColor(R.color.color_white));
                }

                holder1.notifiction_main.setTag(position);
                holder1.notifiction_main.setOnClickListener(onClickListener);
            }


        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }

    // "Normal item" ViewHolder
    private class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQuestionPosed;
        private TextView tvHoursAgo;
        private ImageView ivProfilePic, img_chat_user_image, img_profile_image;
        private ImageView ivItemPic, img_profirle_image;

        private LinearLayout ll_message, ll_admin, ll_profile;
        private TextView tv_admin_mess, tv_admin_HoursAgo;
        private TextView tv_profile_mess, tv_profile_HoursAgo;

        private LinearLayout notifiction_main;

        public UserViewHolder(View itemView) {
            super(itemView);
            tvQuestionPosed = (TextView) itemView.findViewById(R.id.tvQuestionPosed);
            tvHoursAgo = (TextView) itemView.findViewById(R.id.tvHoursAgo);
            ivItemPic = (ImageView) itemView.findViewById(R.id.ivItemPic);
            ivProfilePic = (ImageView) itemView.findViewById(R.id.ivProfilePic);
            img_chat_user_image = (ImageView) itemView.findViewById(R.id.img_chat_user_image);
            img_profile_image = (ImageView) itemView.findViewById(R.id.img_profile_image);


            notifiction_main = (LinearLayout) itemView.findViewById(R.id.notifiction_main);

            ll_message = (LinearLayout) itemView.findViewById(R.id.ll_message);
            ll_admin = (LinearLayout) itemView.findViewById(R.id.ll_admin);
            ll_profile = (LinearLayout) itemView.findViewById(R.id.ll_profile);

            tv_admin_mess = (TextView) itemView.findViewById(R.id.tv_admin_mess);
            tv_admin_HoursAgo = (TextView) itemView.findViewById(R.id.tv_admin_HoursAgo);
            tv_profile_mess = (TextView) itemView.findViewById(R.id.tv_profile_mess);
            tv_profile_HoursAgo = (TextView) itemView.findViewById(R.id.tv_profile_HoursAgo);
            img_profirle_image = (ImageView) itemView.findViewById(R.id.img_profirle_image);
        }
    }


}

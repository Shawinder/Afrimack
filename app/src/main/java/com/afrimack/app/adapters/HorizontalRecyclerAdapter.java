package com.afrimack.app.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.afrimack.app.R;
import com.afrimack.app.activities.SellOffer;
import com.afrimack.app.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by br on 20/5/17.
 */

public class HorizontalRecyclerAdapter extends RecyclerView.Adapter<HorizontalRecyclerAdapter.RecyclerViewHolder> {

    private static LayoutInflater inflater = null;
    public Resources res;
    int mposition;
    private Context context;
    private List<Bitmap> images = null;
    private List<String> imagesUri = null;
    private Integer closeButton = null;
    private ClickListener clickListener;
    private View.OnClickListener onClickListener;
    private String postType;


    public HorizontalRecyclerAdapter(Context context, List<Bitmap> images, List<String> imagesUri, Integer closeButton, View.OnClickListener onClickListener, String postType) {
        this.context = context;
        this.images = images;
        this.closeButton = closeButton;
        this.imagesUri = imagesUri;
        this.onClickListener = onClickListener;
        this.postType = postType;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View myView = LayoutInflater.from(context).inflate(R.layout.selceted_image_list_item, parent, false);
        RecyclerViewHolder myViewHolder = new RecyclerViewHolder(myView);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        if (postType.equalsIgnoreCase("")) {
            if (images.get(position) == null) {
                holder.ivCloseButton.setVisibility(View.GONE);
                try {
                    holder.image.setImageResource(R.drawable.camera);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                holder.image.setOnClickListener(onClickListener);

            } else {
                holder.image.setOnClickListener(null);
                holder.ivCloseButton.setVisibility(View.VISIBLE);
                try {
                    holder.image.setImageBitmap(images.get(position));
                    holder.ivCloseButton.setImageResource(closeButton);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                holder.ivCloseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (imagesUri.size() > 2) {
                            if (clickListener != null) {
                                clickListener.onClick(holder, position);
                            }
                        } else {
                            CommonUtils.getInstance().showAlertMessage(context, "At least one image is required");
                        }
                    }
                });
            }
        } else {
            if (imagesUri.get(position) == null) {
                holder.ivCloseButton.setVisibility(View.GONE);
                try {
                    holder.image.setImageResource(R.drawable.camera);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                holder.image.setOnClickListener(onClickListener);

            } else {
                holder.image.setOnClickListener(null);
                holder.ivCloseButton.setVisibility(View.VISIBLE);
                if (imagesUri.get(position).contains("http") || imagesUri.get(position).contains("https")) {
                    Picasso
                            .with(context)
                            .load(imagesUri.get(position))
                            .placeholder(R.drawable.camera)
                            .into(holder.image);

                } else {

                    try {
                        holder.image.setImageBitmap(images.get(position));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


                try {
                    holder.ivCloseButton.setImageResource(closeButton);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                holder.ivCloseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (imagesUri.size() > 2) {
                            if (clickListener != null) {
                                clickListener.onClick(holder, position);
                            }
                        } else {
                            CommonUtils.getInstance().showAlertMessage(context, "At least one image is required");
                        }
                    }
                });
            }
        }

    }

    @Override
    public int getItemCount() {

        if (postType.equalsIgnoreCase("")) {
            return images.size();
        } else {
            return imagesUri.size();
        }

    }

    public void setOnClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onClick(RecyclerViewHolder view, int position);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public ImageView ivCloseButton;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            this.image = (ImageView) itemView.findViewById(R.id.image);
            this.ivCloseButton = (ImageView) itemView.findViewById(R.id.ivCloseButton);
        }
    }
}
package com.afrimack.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afrimack.app.R;

import java.util.List;

public class PickImageAdapter extends RecyclerView.Adapter<PickImageAdapter.MyViewHolder> {

    private List<String> iamge;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtView;

        public MyViewHolder(View view) {
            super(view);

        }
    }


    public PickImageAdapter(List<String> horizontalList) {
        this.iamge = horizontalList;
    }

    @Override
    public PickImageAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_tag, parent, false);

        return new PickImageAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PickImageAdapter.MyViewHolder holder, final int position) {
    }

    @Override
    public int getItemCount() {
        return iamge.size();
    }

}


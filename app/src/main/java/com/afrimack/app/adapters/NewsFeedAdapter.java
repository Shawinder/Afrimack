package com.afrimack.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.NotificationAllResponseModel;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ubuntu on 4/5/17.
 */
public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.MyViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    Context context;
    LayoutInflater layoutInflater;
    private List<String> moviesLists;
    public static List<NotificationAllResponseModel.ResponseBean.NotificationsBean> notifationList;
    View.OnClickListener onClickListener;
    private OnLoadMoreListener onLoadMoreListener;

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public ProgressBar progressBar;


    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public NewsFeedAdapter(Context context, List<NotificationAllResponseModel.ResponseBean.NotificationsBean> notifications, View.OnClickListener onClickListener) {
        this.notifationList = notifications;
        this.context = context;
        this.onClickListener = onClickListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_news_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvQuestionPosed.setText(notifationList.get(position).getMessage());
        holder.tvHoursAgo.setText(notifationList.get(position).getDuration());

        Picasso.with(context)
                .load(notifationList.get(position).getUser_image())
                .fit()
                .placeholder(R.drawable.user_round)
                .error(R.drawable.user_round)
                .into(holder.ivProfilePic);
        Picasso.with(context)
                .load(notifationList.get(position).getPost_image())
                .fit()
                .placeholder(R.drawable.user_round)
                .error(R.drawable.user_round)
                .into(holder.ivItemPic);

        holder.notifiction_main.setTag(position);
        holder.notifiction_main.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return notifationList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQuestionPosed;
        private TextView tvHoursAgo;
        private ImageView ivProfilePic;
        private ImageView ivItemPic;

        private LinearLayout notifiction_main;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvQuestionPosed = (TextView) itemView.findViewById(R.id.tvQuestionPosed);
            tvHoursAgo = (TextView) itemView.findViewById(R.id.tvHoursAgo);
            ivItemPic = (ImageView)itemView.findViewById(R.id.ivItemPic);
            ivProfilePic = (ImageView)itemView.findViewById(R.id.ivProfilePic);

            notifiction_main = (LinearLayout) itemView.findViewById(R.id.notifiction_main);
        }
    }
}

package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityDiscoverDetail;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.HomeResponseModel;
import com.afrimack.app.models.OtherUserResponseModel;
import com.afrimack.app.utils.CommonUtils;

import java.util.List;

public class OtherViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView countryName, tv_grid_prige, sold, tv_premium;
    public ImageView countryPhoto;
    private List<OtherUserResponseModel.ResponseBean.SellingArrBean> List;
    Context context;
    public LinearLayout ll_fram_hot;
    public CardView mCardView;
    public LinearLayout mFramelayout;


    public OtherViewHolders(View itemView, Context context, List<OtherUserResponseModel.ResponseBean.SellingArrBean> itemList) {
        super(itemView);
        itemView.setOnClickListener(this);
        mFramelayout = (LinearLayout) itemView.findViewById(R.id.frame_staggered_grid);
        mCardView = (CardView) itemView.findViewById(R.id.card_view);
        countryName = (TextView) itemView.findViewById(R.id.country_name);
        countryPhoto = (ImageView) itemView.findViewById(R.id.country_photo);
        tv_grid_prige = (TextView) itemView.findViewById(R.id.tv_grid_prige);
        ll_fram_hot = (LinearLayout) itemView.findViewById(R.id.ll_fram_hot);
        sold = (TextView) itemView.findViewById(R.id.sold);
        tv_premium = (TextView) itemView.findViewById(R.id.tv_premium);
        this.context = context;
        this.List = itemList;
    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent(context, ActivityDiscoverDetail.class);
        String post_id = String.valueOf(List.get(getPosition()).getPost_id());
        Config.setPostId(post_id, context);
        context.startActivity(intent);
    }
}

package com.afrimack.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.afrimack.app.R;
import com.afrimack.app.models.HomeResponseModel;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.afrimack.app.utils.EndlessRecyclerOnScrollListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;


public class AdapterStaggreGrid extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static List<HomeResponseModel.ResponseBean.PostsBean> additemList;

    private Context context;

    private final int TYPE_ITEM = 0;
    private final int VIEW_PROG = 1;
    private int visibleThreshold = 2;
    private OnLoadMoreListener onLoadMoreListener;
    private Random mRandom = new Random();
    private boolean loading = true;
    private int pastVisibleItems, visibleItemCount, totalItemCount;


    public AdapterStaggreGrid(Context context, List<HomeResponseModel.ResponseBean.PostsBean> posts, RecyclerView recyclerView, OnLoadMoreListener mOnLoadMoreListener) {
        this.additemList = posts;
        this.context = context;
        this.onLoadMoreListener = mOnLoadMoreListener;
        Log.e("posts", posts.size() + "");

        final StaggeredGridLayoutManager linearLayoutManager = (StaggeredGridLayoutManager) recyclerView
                .getLayoutManager();
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                int[] firstVisibleItems = null;
                firstVisibleItems = linearLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);
                if (firstVisibleItems != null && firstVisibleItems.length > 0) {
                    pastVisibleItems = firstVisibleItems[0];
                }

                if (loading) {
                    if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                        loading = false;
                        Log.d("tag", "LOAD NEXT ITEM");
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                    }
                }
            }
        });
    }


    @Override
    public int getItemViewType(int position) {
        return (additemList.size() > 0 && additemList.get(position) == null) ? VIEW_PROG : TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_PROG) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        } else {

            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_list_new, null);
            return new SolventViewHolders(layoutView, context, additemList);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        if (holder1 instanceof SolventViewHolders) {

            final SolventViewHolders holder = (SolventViewHolders) holder1;

            String premium = additemList.get(position).getPremium();
            if (premium.equalsIgnoreCase("0")) {
                holder.tv_premium.setVisibility(View.GONE);
                holder.ll_fram_hot.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));
            } else if (premium.equalsIgnoreCase("1")) {
                holder.tv_premium.setVisibility(View.VISIBLE);
                holder.tv_premium.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
                holder.tv_premium.setText("HOT");
                holder.ll_fram_hot.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));

            } else if (premium.equalsIgnoreCase("2")) {
                holder.tv_premium.setVisibility(View.VISIBLE);
                holder.tv_premium.setBackgroundColor(context.getResources().getColor(R.color.color_green));
                holder.tv_premium.setText("TOP");
                holder.ll_fram_hot.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));
            } else if (premium.equalsIgnoreCase("3")) {
                holder.tv_premium.setVisibility(View.VISIBLE);
                holder.tv_premium.setBackgroundColor(context.getResources().getColor(R.color.color_red));
                holder.tv_premium.setText("HOT & TOP");
                holder.ll_fram_hot.setBackgroundColor(context.getResources().getColor(R.color.color_red));
            }

            if (additemList.get(position).getImage() != null) {
                Glide.with(context).load(additemList.get(position).getImage()).asBitmap().
                        into(new BitmapImageViewTarget(holder.countryPhoto) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                holder.countryPhoto.setImageBitmap(resource);
                                if (resource.getWidth() > resource.getHeight()) {
                                    holder.countryPhoto.getLayoutParams().height = 300;
                                    holder.countryPhoto.requestLayout();
                                }

                            }
                        });

            }


            holder.countryName.setText(additemList.get(position).getTitle());

            try {
                String sub = additemList.get(position).getPrice();
                String s = sub.substring(0, sub.indexOf("."));
                if (sub.contains(".00")) {
                    holder.tv_grid_prige.setText(s);
                } else {
                    holder.tv_grid_prige.setText(sub);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (additemList.get(position).getSwap_deal().equalsIgnoreCase("Y")) {
                holder.txt_or.setText("or");
                holder.txt_swap_deal.setText("Swap");
            } else {
                holder.txt_or.setText("");
                holder.txt_swap_deal.setText("");
            }

            if (additemList.get(position).getPost_status().equalsIgnoreCase("C")) {
                holder.sold.setVisibility(View.VISIBLE);
            }

        }
        if (holder1 instanceof LoadingViewHolder) {
            LoadingViewHolder holder = (LoadingViewHolder) holder1;

            holder.progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        if (additemList != null)
            return additemList.size();
        else
            return 0;
    }

    public void setLoaded() {
        loading = true;
    }


    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;


        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }


}

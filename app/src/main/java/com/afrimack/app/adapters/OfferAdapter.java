package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityDiscoverDetail;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.ChatResponseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.MyViewHolder> {

    private Drawable itemPic;
    static public List<ChatResponseModel.ResponseBean.OffersBean> offerList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;


    public OfferAdapter(Context context, List<ChatResponseModel.ResponseBean.OffersBean> offers, View.OnClickListener onClickListener) {
        this.offerList = offers;
        this.context = context;
        this.onClickListener = onClickListener;
    }


    @Override
    public OfferAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_list, parent, false);
        return new OfferAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OfferAdapter.MyViewHolder holder, final int position) {

        String offeruser_id = String.valueOf(offerList.get(position).getUser_id());
        if (offeruser_id.equalsIgnoreCase(Config.getUserId(context))) {
            holder.lay_me.setVisibility(View.VISIBLE);
            holder.lay_other.setVisibility(View.GONE);
            if (offerList.get(position).getOffer_type().equalsIgnoreCase("PRICE")) {
                holder.me_offer.setText(offerList.get(position).getTitle());
            } else if (offerList.get(position).getOffer_type().equalsIgnoreCase("POST")) {
                try {
                    String title = offerList.get(position).getTitle();

                    SpannableString content = new SpannableString(title);
                    if (title.contains("offered")) {
                        content.setSpan(new UnderlineSpan(), title.indexOf("offered") + 8, title.length(), 0);
                    } else if (title.contains("accepted")) {
                        content.setSpan(new UnderlineSpan(), title.indexOf("accepted") + 23, title.length(), 0);
                    } else {
                        content.setSpan(new UnderlineSpan(), title.indexOf("confirmed") + 19, title.length(), 0);
                    }
                    holder.me_offer.setText(content);
                    holder.me_offer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, ActivityDiscoverDetail.class);
                            intent.putExtra("post_id", String.valueOf(offerList.get(position).getOffer_value()));
                            context.startActivity(intent);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            holder.me_date.setText(offerList.get(position).getDate());
            holder.me_profile.setTag(position);
            holder.me_profile.setOnClickListener(onClickListener);
            Picasso
                    .with(context)
                    .load(offerList.get(position).getImage())
                    .placeholder(R.drawable.dami_image2)
                    .into(holder.me_profile);
        } else {

            holder.lay_other.setVisibility(View.VISIBLE);
            holder.lay_me.setVisibility(View.GONE);
            if (offerList.get(position).getOffer_type().equalsIgnoreCase("PRICE")) {
                holder.other_offer.setText(offerList.get(position).getTitle());
            } else if (offerList.get(position).getOffer_type().equalsIgnoreCase("POST")) {
                try {
                    String title = offerList.get(position).getTitle();

                    SpannableString content = new SpannableString(title);

                    if (title.contains("offered")) {
                        content.setSpan(new UnderlineSpan(), title.indexOf("offered") + 8, title.length(), 0);
                    } else if (title.contains("accepted")) {
                        content.setSpan(new UnderlineSpan(), title.indexOf("accepted") + 23, title.length(), 0);
                    } else {
                        content.setSpan(new UnderlineSpan(), title.indexOf("confirmed") + 19, title.length(), 0);
                    }


                    holder.other_offer.setText(content);
                    holder.other_offer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, ActivityDiscoverDetail.class);
                            intent.putExtra("post_id", String.valueOf(offerList.get(position).getOffer_value()));
                            context.startActivity(intent);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            holder.other_date.setText(offerList.get(position).getDate());

            holder.other_profile.setTag(position);
            holder.other_profile.setOnClickListener(onClickListener);
            Picasso
                    .with(context)
                    .load(offerList.get(position).getImage())
                    .placeholder(R.drawable.dami_image2)
                    .into(holder.other_profile);

            holder.lay_other.setTag(position);
            holder.lay_other.setOnClickListener(onClickListener);
        }


    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView me_profile, other_profile;
        public TextView me_offer, other_offer;

        public TextView me_date, other_date;
        private LinearLayout lay_me, lay_other;

        public MyViewHolder(View itemView) {
            super(itemView);
            me_profile = (ImageView) itemView.findViewById(R.id.me_profile);
            other_profile = (ImageView) itemView.findViewById(R.id.other_profile);

            me_offer = (TextView) itemView.findViewById(R.id.me_offer);
            other_offer = (TextView) itemView.findViewById(R.id.other_offer);

            me_date = (TextView) itemView.findViewById(R.id.me_date);
            other_date = (TextView) itemView.findViewById(R.id.other_date);

            lay_me = (LinearLayout) itemView.findViewById(R.id.lay_me);
            lay_other = (LinearLayout) itemView.findViewById(R.id.lay_other);

        }
    }


}

package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.BlogInfoActivity;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.BlogModel;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.squareup.picasso.Picasso;

import java.util.List;


public class BlogAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    LayoutInflater layoutInflater;
    public static List<BlogModel.ResponseBean> bloglist;
    private final int TYPE_ITEM = 0;
    private final int VIEW_PROG = 1;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    public BlogAdapter(Context context, List<BlogModel.ResponseBean> users, RecyclerView recyclerView,OnLoadMoreListener mOnLoadMoreListener) {
        this.context = context;
        this.bloglist =users;
        this.onLoadMoreListener =mOnLoadMoreListener;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                .getLayoutManager();
        recyclerView
                .addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView,
                                           int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            // End has been reached
                            // Do something
                            if (onLoadMoreListener != null) {
                                onLoadMoreListener.onLoadMore();
                            }
                            loading = true;
                        }

                    }
                });
    }
    @Override
    public int getItemViewType(int position) {
        return (bloglist.size()>0 &&bloglist.get(position)==null) ? VIEW_PROG : TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType==VIEW_PROG)
        {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.item_loading, parent, false);
            return new BlogAdapter.LoadingViewHolder(view);
        }
        else {


            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_list, null);
            return new BlogAdapter.MyViewHolder(layoutView);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        if(holder1 instanceof BlogAdapter.MyViewHolder) {
            BlogAdapter.MyViewHolder holder= (BlogAdapter.MyViewHolder) holder1;

            Picasso.with(context)
                    .load(bloglist.get(position).getImage())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(holder.img_blog);

            holder.tv_titel.setText(bloglist.get(position).getTitle());
            holder.tv_date.setText(bloglist.get(position).getCreated());
            holder.tv_info.setText(Html.fromHtml(bloglist.get(position).getDescription()));




        }if (holder1 instanceof AdapterStaggreGrid.LoadingViewHolder) {
            AdapterStaggreGrid.LoadingViewHolder holder= (AdapterStaggreGrid.LoadingViewHolder) holder1;

            holder.progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        if(bloglist!=null)
            return bloglist.size();
        else
            return 0;
    }

    public void setLoaded() {
        loading = false;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_titel,tv_info,tv_date;
        private ImageView img_blog;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tv_titel = (TextView) itemView.findViewById(R.id.tv_titel);
            tv_info = (TextView) itemView.findViewById(R.id.tv_info);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            img_blog = (ImageView) itemView.findViewById(R.id.img_blog);

        }

        @Override
        public void onClick(View v) {
            Intent intent =new Intent(context, BlogInfoActivity.class);
            String single_user_id = String.valueOf(bloglist.get(getPosition()).getId());
            Config.setBlogId(single_user_id,context);
            context.startActivity(intent);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }
}

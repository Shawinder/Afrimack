package com.afrimack.app.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afrimack.app.R;

import java.util.ArrayList;
import java.util.List;

public class AllCatogeryAdapter extends RecyclerView.Adapter<AllCatogeryAdapter.MyViewHolder> {

    private Drawable itemPic;
    static public List<String> allCatoList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;


    public AllCatogeryAdapter(Context context, ArrayList<String> categories, View.OnClickListener onClickListener) {
        this.allCatoList = categories;
        this.context = context;
        this.onClickListener = onClickListener;

    }


    @Override
    public AllCatogeryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.catoget_textview, parent, false);
        return new AllCatogeryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AllCatogeryAdapter.MyViewHolder holder, int position) {

        holder.catogety.setText(allCatoList.get(position).toString());
        holder.catogety.setTag(position);
        holder.catogety.setOnClickListener(onClickListener);

    }

    @Override
    public int getItemCount() {
        return allCatoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        public TextView catogety;


        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            catogety = (TextView)itemView.findViewById(R.id.catogety);

        }

        @Override
        public void onClick(View view) {
        }
    }
}

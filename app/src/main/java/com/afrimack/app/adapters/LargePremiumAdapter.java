package com.afrimack.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.models.PremiumResponseModel;

import java.util.List;

public class LargePremiumAdapter extends RecyclerView.Adapter<LargePremiumAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    private List<PremiumResponseModel.ResponseBean.LARGEBean> largelist;
    View.OnClickListener onClickListener;
    public LargePremiumAdapter(Context context, List<PremiumResponseModel.ResponseBean.LARGEBean> largelist, View.OnClickListener onClickListener) {

        this.largelist = largelist;
        this.context = context;
        this.onClickListener=onClickListener;
    }



    @Override
    public LargePremiumAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.premium_list, parent, false);
        return new LargePremiumAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LargePremiumAdapter.MyViewHolder holder, int position) {

        if(largelist.get(position).getPackage_type().equalsIgnoreCase("HOT")){
            holder.flay_hot.setVisibility(View.VISIBLE);
            holder.flay_top.setVisibility(View.GONE);
            holder.flay_hot_top.setVisibility(View.GONE);
            holder.tv_hot_day.setText(largelist.get(position).getDays()+ " Days");
            holder.tv_hot_prise.setText("$ "+largelist.get(position).getPrice());
            holder.flay_hot.setTag(position);
            holder.flay_hot.setOnClickListener(onClickListener);
        }

        if(largelist.get(position).getPackage_type().equalsIgnoreCase("TOP")){
            holder.flay_hot.setVisibility(View.GONE);
            holder.flay_top.setVisibility(View.VISIBLE);
            holder.flay_hot_top.setVisibility(View.GONE);
            holder.tv_top_day.setText(largelist.get(position).getDays()+" Days");
            holder.tv_top_prise.setText("$ "+largelist.get(position).getPrice());
            holder.flay_top.setTag(position);
            holder.flay_top.setOnClickListener(onClickListener);
        }

        if(largelist.get(position).getPackage_type().equalsIgnoreCase("HOT & TOP")){
            holder.flay_hot.setVisibility(View.GONE);
            holder.flay_top.setVisibility(View.GONE);
            holder.flay_hot_top.setVisibility(View.VISIBLE);
            holder.tv_hot_top_day.setText(largelist.get(position).getDays()+" Days");
            holder.tv_hot_top_prise.setText("$ "+largelist.get(position).getPrice());
            holder.flay_hot_top.setTag(position);
            holder.flay_hot_top.setOnClickListener(onClickListener);
        }



    }

    @Override
    public int getItemCount() {
        return largelist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private FrameLayout flay_hot,flay_top,flay_hot_top;
        private TextView tv_hot_day,tv_hot_prise;
        private TextView tv_top_day,tv_top_prise;
        private TextView tv_hot_top_day,tv_hot_top_prise;



        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            flay_hot = (FrameLayout) itemView.findViewById(R.id.flay_hot);
            flay_top = (FrameLayout) itemView.findViewById(R.id.flay_top);
            flay_hot_top = (FrameLayout) itemView.findViewById(R.id.flay_hot_top);

            tv_hot_day = (TextView)itemView.findViewById(R.id.tv_hot_day);
            tv_hot_prise = (TextView)itemView.findViewById(R.id.tv_hot_prise);

            tv_top_day = (TextView)itemView.findViewById(R.id.tv_top_day);
            tv_top_prise = (TextView)itemView.findViewById(R.id.tv_top_prise);

            tv_hot_top_day = (TextView)itemView.findViewById(R.id.tv_hot_top_day);
            tv_hot_top_prise = (TextView)itemView.findViewById(R.id.tv_hot_top_prise);
        }
        @Override
        public void onClick(View view) {

        }

    }
}

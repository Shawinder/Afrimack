package com.afrimack.app.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.models.SubQuation;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;

public class DetailQuestionAdapter extends RecyclerView.Adapter<DetailQuestionAdapter.MyViewHolder> {

    private Drawable itemPic;
    static public List<PostDetailResponseModel.ResponseBean.QuestionsBean> quetionList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;
    SubQutiionMessage subQutiionMessage;
    SubQuestionAdapter.SubQutiionDelete subQutiionDelete;

    public DetailQuestionAdapter(Context context, List<PostDetailResponseModel.ResponseBean.QuestionsBean> questions, View.OnClickListener onClickListener, SubQutiionMessage subQutiionMessage, SubQuestionAdapter.SubQutiionDelete subQutiionDelete) {
        this.quetionList = questions;
        this.context = context;
        this.onClickListener = onClickListener;
        this.subQutiionMessage = subQutiionMessage;
        this.subQutiionDelete = subQutiionDelete;
    }


    @Override
    public DetailQuestionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.nested_question_list, parent, false);
        return new DetailQuestionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DetailQuestionAdapter.MyViewHolder holder, final int position) {

        if (quetionList.get(position).getBlock_status() == 1) {
            holder.block.setVisibility(View.VISIBLE);
        } else {
            holder.block.setVisibility(View.VISIBLE);
        }
        if (quetionList.get(position).getCan_reply() == 1) {
            holder.block.setVisibility(View.VISIBLE);
        } else {
            holder.block.setVisibility(View.GONE);
        }
        if (quetionList.get(position).getMy_product() == 1) {
            holder.lay_sub_question.setVisibility(View.VISIBLE);
        } else {
            holder.lay_sub_question.setVisibility(View.GONE);
        }

        try {
            if (Config.getUserId(context).equalsIgnoreCase(quetionList.get(position).getUser_id())) {
                holder.block.setImageResource(R.drawable.ic_delete);
            } else {
                holder.block.setImageResource(R.drawable.dot);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.question_user_name.setText(quetionList.get(position).getUser());


        String new_val= StringEscapeUtils.unescapeJava(quetionList.get(position).getQuestion());

        holder.question_message.setText(new_val);

        holder.question_date.setText(quetionList.get(position).getDateTime());
        Picasso
                .with(context)
                .load(quetionList.get(position).getImage())
                .placeholder(R.drawable.dami_image2)
                .into(holder.question_profile);
        holder.question_profile.setTag(position);
        holder.question_profile.setOnClickListener(onClickListener);

        holder.block.setTag(position);
        holder.block.setOnClickListener(onClickListener);

        Picasso
                .with(context)
                .load(quetionList.get(position).getLogin_user_image())
                .placeholder(R.drawable.dami_image2)
                .into(holder.sub_qution_user_image);

        holder.img_sub_send_qution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubQuation subQuation = new SubQuation();
                String sub_message = String.valueOf(holder.sub_et_qution_list.getText());
                subQuation.setPostion(String.valueOf(position));
                subQuation.setQution_id(quetionList.get(position).getQuestion_id());
                subQuation.setSubqution(sub_message);
                subQutiionMessage.getSubMessage(subQuation);
            }
        });
        if (holder.childAdapter == null) {
            holder.childAdapter = new SubQuestionAdapter(context, quetionList.get(position).getSubquestion(), onClickListener, subQutiionDelete);
            holder.rv_subqution.setAdapter(holder.childAdapter);
        } else {
            holder.childAdapter.subquestion = quetionList.get(position).getSubquestion();
            holder.childAdapter.notifyDataSetChanged();
        }


    }


    @Override
    public int getItemCount() {
        return quetionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView question_profile, block;
        public TextView question_user_name, question_message, question_date;
        private RecyclerView rv_subqution;
        public SubQuestionAdapter childAdapter;

        /*subqution*/
        private ImageView img_sub_send_qution, sub_qution_user_image;
        private EditText sub_et_qution_list;
        private LinearLayout lay_sub_question;

        public MyViewHolder(View itemView) {
            super(itemView);
            question_profile = (ImageView) itemView.findViewById(R.id.question_profile);
            block = (ImageView) itemView.findViewById(R.id.block);

            question_user_name = (TextView) itemView.findViewById(R.id.question_user_name);
            question_message = (TextView) itemView.findViewById(R.id.question_message);
            question_date = (TextView) itemView.findViewById(R.id.question_date);

            rv_subqution = (RecyclerView) itemView.findViewById(R.id.rv_subqution);

            sub_qution_user_image = (ImageView) itemView.findViewById(R.id.sub_qution_user_image);
            img_sub_send_qution = (ImageView) itemView.findViewById(R.id.img_sub_send_qution);
            sub_et_qution_list = (EditText) itemView.findViewById(R.id.sub_et_qution_list);
            lay_sub_question = (LinearLayout) itemView.findViewById(R.id.lay_sub_question);
            rv_subqution.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        }
    }

    public interface SubQutiionMessage {
        public void getSubMessage(SubQuation sub_message);
    }

}

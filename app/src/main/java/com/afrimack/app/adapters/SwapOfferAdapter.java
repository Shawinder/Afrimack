package com.afrimack.app.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.SwapResponseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SwapOfferAdapter extends RecyclerView.Adapter<SwapOfferAdapter.MyViewHolder> {

    private Drawable itemPic;
    static public List<SwapResponseModel.ResponseBean.OffersBean> offerList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;



    public SwapOfferAdapter(Context context, List<SwapResponseModel.ResponseBean.OffersBean> offers) {
        this.offerList = offers;
        this.context = context;
    }



    @Override
    public SwapOfferAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_list, parent, false);
        return new SwapOfferAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SwapOfferAdapter.MyViewHolder holder, int position) {

        String offeruser_id = String.valueOf(offerList.get(position).getUser_id());
        if(offeruser_id.equalsIgnoreCase(Config.getUserId(context))){
            holder.lay_me.setVisibility(View.VISIBLE);
            holder.lay_other.setVisibility(View.GONE);
            holder.me_offer.setText(offerList.get(position).getTitle());
            holder.me_date.setText(offerList.get(position).getDate());
            Picasso.with(context)
                    .load(offerList.get(position).getImage())
                    .placeholder(R.drawable.user_round)
                    .error(R.drawable.user_round)
                    .into(holder.me_profile);
        }else {

            holder.lay_other.setVisibility(View.VISIBLE);
            holder.lay_me.setVisibility(View.GONE);
            holder.other_offer.setText(offerList.get(position).getTitle());
            holder.other_date.setText(offerList.get(position).getDate());
            Picasso.with(context)
                    .load(offerList.get(position).getImage())
                    .placeholder(R.drawable.user_round)
                    .error(R.drawable.user_round)
                    .into(holder.other_profile);
        }



    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView me_profile,other_profile;
        public TextView me_offer,other_offer;

        public TextView me_date,other_date;
        private LinearLayout lay_me,lay_other;

        public MyViewHolder(View itemView) {
            super(itemView);
            me_profile = (ImageView)itemView.findViewById(R.id.me_profile);
            other_profile = (ImageView)itemView.findViewById(R.id.other_profile);

            me_offer = (TextView)itemView.findViewById(R.id.me_offer);
            other_offer = (TextView)itemView.findViewById(R.id.other_offer);

            me_date = (TextView)itemView.findViewById(R.id.me_date);
            other_date = (TextView)itemView.findViewById(R.id.other_date);

            lay_me= (LinearLayout) itemView.findViewById(R.id.lay_me);
            lay_other = (LinearLayout) itemView.findViewById(R.id.lay_other);

        }
    }

}

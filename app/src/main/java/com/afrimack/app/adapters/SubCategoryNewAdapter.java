package com.afrimack.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;

import java.util.ArrayList;

public class SubCategoryNewAdapter extends RecyclerView.Adapter<SubCategoryNewAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<String> category_list;
    int row_index;
    int position;
    String category;

    public SubCategoryNewAdapter(Context context, int position,String cat) {
        this.category_list = category_list;
        this.context=context;
        this.position=position;
        this.category = cat;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategory_list_view, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int i) {
        holder.txtVwSubCategory.setText(CommonUtils.getSub_categories().get(position).get(i));

        holder.txtVwSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CommonUtils.getSub_categories().get(position)!=null) {

                    String sub_id = String.valueOf(CommonUtils.getSub_categories_Id().get(position).get(i));
                   /* Config.setSortSubCategory(sub_id,context);*/
                    CommonUtils.Id=sub_id;
                    String fullname = category+" | "+CommonUtils.getSub_categories().get(position).get(i);
                    Log.e("FULL_NAME",fullname);

                    Intent intent = new Intent("android.intent.action.CATEGORY");
                    intent.putExtra("category_name",fullname);
                    context.sendBroadcast(intent);
                 /*   Intent intent = new Intent(context, ActivityHome.class);
                    context.startActivity(intent);
*/
                    ((Activity) context).finish();
               }
            }
        });

    }

    @Override
    public int getItemCount() {
        return CommonUtils.getSub_categories().get(position).size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtVwSubCategory;
        public MyViewHolder(View itemView) {
            super(itemView);

            txtVwSubCategory=(TextView)itemView.findViewById(R.id.txtVwSubCategory);
        }
    }
}

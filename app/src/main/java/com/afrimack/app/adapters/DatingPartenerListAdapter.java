package com.afrimack.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.SingleDatingActivity;
import com.afrimack.app.activities.SplashLogin;
import com.afrimack.app.appbeans.PartnerBean;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by root on 6/11/17.
 */

public class DatingPartenerListAdapter extends RecyclerView.Adapter<DatingPartenerListAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    static public List<PartnerBean> mLIst;
    private Fragment frg;


    public DatingPartenerListAdapter(Context context, List<PartnerBean> mLIst) {
        this.mLIst = mLIst;
        this.context = context;
    }


    @Override
    public DatingPartenerListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.discover_category_item_layout, parent, false);
        return new DatingPartenerListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DatingPartenerListAdapter.MyViewHolder holder, final int position) {
        holder.imgLOgo.setColorFilter(ContextCompat.getColor(context, R.color.color_black), android.graphics.PorterDuff.Mode.MULTIPLY);


        Picasso.with(context)
                .load(mLIst.get(position).getIcon_logo())
//                .placeholder(R.drawable.dami_background)
//                .error(R.drawable.dami_background)
                .into(holder.imgLOgo);
        holder.txtName.setText(mLIst.get(position).getCategory_name());

        holder.lay_discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                view.setBackground(context.getResources().getDrawable(R.drawable.categories_background_filled));
                // holder.lay_discover.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));

                if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
                    Intent intent = new Intent(context, SingleDatingActivity.class);
                    if (mLIst.get(position).getCategory_name().equalsIgnoreCase("Men")) {
                        intent.putExtra("male", "M");
                        intent.putExtra("female", "");
                    } else {
                        intent.putExtra("male", "");
                        intent.putExtra("female", "F");
                    }
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, SplashLogin.class);
                    context.startActivity(intent);
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            view.setBackground(context.getResources().getDrawable(R.drawable.categories_background_gray));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 200);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLIst.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtName;
        private ImageView imgLOgo;
        private LinearLayout lay_discover;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            txtName = (TextView) itemView.findViewById(R.id.txt_title_name);
            imgLOgo = (ImageView) itemView.findViewById(R.id.img_logo);
            lay_discover = (LinearLayout) itemView.findViewById(R.id.lay_discover);
        }

        @Override
        public void onClick(View view) {
            final View mView = view;
            txtName.setTextColor(context.getResources().getColor(R.color.color_black));
            try {

//                view.setBackground(context.getResources().getDrawable(R.drawable.category_black_border));
//                imgLOgo.setColorFilter(ContextCompat.getColor(context, R.color.color_black), android.graphics.PorterDuff.Mode.MULTIPLY);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
//                Intent intent = new Intent(context, SingleDatingActivity.class);
//                if (mLIst.get(getPosition()).getCategory_name().equalsIgnoreCase("Men")) {
//                    intent.putExtra("male", "M");
//                    intent.putExtra("female", "");
//                } else {
//                    intent.putExtra("male", "");
//                    intent.putExtra("female", "F");
//                }
//                context.startActivity(intent);
//            } else {
//                Intent intent = new Intent(context, SplashLogin.class);
//                context.startActivity(intent);
//            }
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    try {
////                        txtName.setTextColor(context.getResources().getColor(R.color.color_white));
////                        mView.setBackgroundColor(context.getResources().getColor(R.color.color_black));
////                        imgLOgo.setColorFilter(ContextCompat.getColor(context, R.color.color_white), android.graphics.PorterDuff.Mode.MULTIPLY);
//                        mView.setBackground(context.getResources().getDrawable(R.drawable.categories_background_filled));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }, 200);
        }

    }

}

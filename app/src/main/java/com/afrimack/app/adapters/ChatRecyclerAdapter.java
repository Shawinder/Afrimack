package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityChatImageFull;
import com.afrimack.app.activities.SingleChatActivity;
import com.afrimack.app.customprefs.Config;
import com.brsoftech.emozi.EmojiconTextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.Date;


public class ChatRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIEW_TYPE_USER = 0;
    public static final int VIEW_TYPE_MY = 1;
    public static final int VIEW_TYPE_DATE = 2;
    public static final int VIEW_TYPE_USER_MEDIA_IMAGE = 3;
    public static final int VIEW_TYPE_MY_MEDIA_IMAGE = 4;


    private Context context;
    private String UID = "";


    public ChatRecyclerAdapter(Context context, String UID) {
        this.context = context;
        this.UID = UID;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        if (viewType == VIEW_TYPE_USER) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_user_message, parent, false);
            return new UserMessageViewHolder(view);
        } else if (viewType == VIEW_TYPE_MY) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_my_message, parent, false);
            return new MyMessageViewHolder(view);
        }
        return null;

    }


    @Override
    public int getItemCount() {
        return SingleChatActivity.messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        // return SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).idSender.equals(UID) ? VIEW_TYPE_MY : VIEW_TYPE_USER;
        return SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).idSender.equalsIgnoreCase(UID) ? VIEW_TYPE_MY : VIEW_TYPE_USER;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).setData(position);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void setData(int position) {

        }
    }

    public String getDateCurrentTimeZone(long timestamp) {
        try {
            return DateFormat.format("HH:mm", new Date(timestamp)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getDateFromTimeStamp(long timestamp) {
        try {
            return DateFormat.format("dd-MMM-yyyy", new Date(timestamp)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getPhoneCurrentDate() {
        try {
            return DateFormat.format("dd-MMM-yyyy", new Date()).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean isAlpha(String name) {
        return name.matches("[a-zA-Z]+");
    }

    private class MyMessageViewHolder extends ViewHolder {
        RelativeLayout rl_mainlayuser_msg;
        private EmojiconTextView tv_data;
        private TextView tv_time, tv_date;
        private ImageView me_profile_chat, iv_media_image;
        private ImageView iv_clock, iv_tick1, iv_tick2, img_my_message_read;
        ProgressBar chatuser_pb_progressbar;


        public MyMessageViewHolder(View itemView) {
            super(itemView);
            tv_data = (EmojiconTextView) itemView.findViewById(R.id.tv_data);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            iv_tick1 = (ImageView) itemView.findViewById(R.id.iv_tick1);
            iv_tick2 = (ImageView) itemView.findViewById(R.id.iv_tick2);
            tv_date = (TextView) itemView.findViewById(R.id.date_text_my);
            rl_mainlayuser_msg = (RelativeLayout) itemView.findViewById(R.id.rl_mainlayuser_msg);
            img_my_message_read = (ImageView) itemView.findViewById(R.id.img_my_message_read);
            iv_media_image = (ImageView) itemView.findViewById(R.id.iv_media_image);
            chatuser_pb_progressbar = (ProgressBar) itemView.findViewById(R.id.chatuser_pb_progressbar);

        }

        @Override
        public void setData(final int position) {

            if (SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).isRead.equalsIgnoreCase("1")) {
                img_my_message_read.setColorFilter(ContextCompat.getColor(context, R.color.color_yallow), android.graphics.PorterDuff.Mode.SRC_IN);
            } else {
                img_my_message_read.setColorFilter(ContextCompat.getColor(context, R.color.color_black), android.graphics.PorterDuff.Mode.SRC_IN);
            }
            long dateFRomDatabase = SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).timestamp;
            if (position == 0) {
                tv_date.setVisibility(View.VISIBLE);
                if (getDateFromTimeStamp(dateFRomDatabase).equalsIgnoreCase(getPhoneCurrentDate())) {
                    tv_date.setText("Today");

                } else {
                    tv_date.setText(getDateFromTimeStamp(dateFRomDatabase));
                }
            } else if (!getDateFromTimeStamp(dateFRomDatabase).equals(getDateFromTimeStamp(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position - 1)).timestamp))) {
                tv_date.setVisibility(View.VISIBLE);
                if (getDateFromTimeStamp(dateFRomDatabase).equalsIgnoreCase(getPhoneCurrentDate())) {
                    tv_date.setText("Today");

                } else {
                    tv_date.setText(getDateFromTimeStamp(dateFRomDatabase));
                }
            } else {
                tv_date.setVisibility(View.GONE);
            }

            if (SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).isImage.equalsIgnoreCase("1")) {
                tv_data.setVisibility(View.GONE);
                iv_media_image.setVisibility(View.VISIBLE);
                chatuser_pb_progressbar.setVisibility(View.VISIBLE);

                loadImage(context, SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).photoUrl, iv_media_image, chatuser_pb_progressbar);

                /*
                 *//*      Glide.with(context)
                            .load(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).photoUrl)
                           .asBitmap()
                            //.asGif()
                           // .placeholder(R.raw.loading_icon_new)
                            .placeholder(R.drawable.loadin_product)
                            .into(iv_media_image);
*/
                 /*
                try {         Glide.with(context)
                            .load(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).photoUrl)
                         //   .asGif()
                         //   .placeholder(R.drawable.loading_icon_new)
                            .crossFade()
                            .into(iv_media_image);


                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            } else {
                tv_data.setVisibility(View.VISIBLE);
                iv_media_image.setVisibility(View.GONE);
                chatuser_pb_progressbar.setVisibility(View.GONE);
                String text = SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).text;
                if (isAlpha(text)) {
                    tv_data.setText(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).text);
                } else {
                    tv_data.setText(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).text);
                }

            }

            long time = SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).timestamp;
            tv_time.setText(getDateCurrentTimeZone(time));
            iv_media_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final int pos = position;
                    Intent mIntent = new Intent(context, ActivityChatImageFull.class);
                    Config.setFullImageString(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(pos)).photoUrl, context);
                    context.startActivity(mIntent);
                }
            });
        }
    }

    public class UserMessageViewHolder extends ViewHolder {
        private EmojiconTextView tv_data;
        public TextView tv_time,tv_date;
        RelativeLayout rl_mainlayuser_msg;
        private ImageView other_profile_chat, img_message_read, iv_media_image;
        ProgressBar chatuser_pb_progressbar;

        public UserMessageViewHolder(View itemView) {
            super(itemView);
            tv_data = (EmojiconTextView) itemView.findViewById(R.id.tv_data);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_date=(TextView)itemView.findViewById(R.id.date_text);
            rl_mainlayuser_msg = (RelativeLayout) itemView.findViewById(R.id.rl_mainlayuser_msg);
            img_message_read = (ImageView) itemView.findViewById(R.id.img_message_read);
            iv_media_image = (ImageView) itemView.findViewById(R.id.iv_media_image);
            chatuser_pb_progressbar = (ProgressBar) itemView.findViewById(R.id.chatuser_pb_progressbar);
        }

        @Override
        public void setData(int position) {
            final int pos = position;
            if (SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).isRead.equalsIgnoreCase("1")) {
                img_message_read.setColorFilter(ContextCompat.getColor(context, R.color.color_yallow), android.graphics.PorterDuff.Mode.SRC_IN);
            } else {
                img_message_read.setColorFilter(ContextCompat.getColor(context, R.color.color_black), android.graphics.PorterDuff.Mode.SRC_IN);
            }
            long dateFRomDatabase = SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).timestamp;
            if (position == 0) {
                tv_date.setVisibility(View.VISIBLE);
                if (getDateFromTimeStamp(dateFRomDatabase).equalsIgnoreCase(getPhoneCurrentDate())) {
                    tv_date.setText("Today");

                } else {
                    tv_date.setText(getDateFromTimeStamp(dateFRomDatabase));
                }
            } else if (!getDateFromTimeStamp(dateFRomDatabase).equals(getDateFromTimeStamp(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position - 1)).timestamp))) {
                tv_date.setVisibility(View.VISIBLE);
                if (getDateFromTimeStamp(dateFRomDatabase).equalsIgnoreCase(getPhoneCurrentDate())) {
                    tv_date.setText("Today");

                } else {
                    tv_date.setText(getDateFromTimeStamp(dateFRomDatabase));
                }
            } else {
                tv_date.setVisibility(View.GONE);
            }



            if (SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).isImage.equalsIgnoreCase("1")) {
                tv_data.setVisibility(View.GONE);
                iv_media_image.setVisibility(View.VISIBLE);
                chatuser_pb_progressbar.setVisibility(View.VISIBLE);


                loadImage(context, SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).photoUrl, iv_media_image, chatuser_pb_progressbar);
                /*         try {
                 *//*  Glide.with(context)
                            .load(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).photoUrl)
                            .asBitmap()
                           // .asGif()
                           // .placeholder(R.raw.loading_icon_new)
                            .placeholder(R.drawable.loadin_product)
                            .into(iv_media_image);*//*


                    Glide.with(context)
                            .load(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).photoUrl)
                           // .asGif()
                            // .asGif()
                            // .placeholder(R.raw.loading_icon_new)
                           // .placeholder(R.drawable.loading_icon_new)
                            .into(iv_media_image);


                } catch (Exception e) {
                    e.printStackTrace();
                }*/

            } else {
                tv_data.setVisibility(View.VISIBLE);
                iv_media_image.setVisibility(View.GONE);
                chatuser_pb_progressbar.setVisibility(View.GONE);
                String text = SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).text;
                if (isAlpha(text)) {
                    tv_data.setText(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).text);
                } else {
                    tv_data.setText(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).text);
                }
            }


            long time = SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(position)).timestamp;
            tv_time.setText(getDateCurrentTimeZone(time));
            iv_media_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent mIntent = new Intent(context, ActivityChatImageFull.class);
                    Config.setFullImageString(SingleChatActivity.messages.get(SingleChatActivity.messageIds.get(pos)).photoUrl, context);
                    context.startActivity(mIntent);
                }
            });


        }
    }

    public void loadImage(Context context, final String url, ImageView img, final ProgressBar eProgressBar) {

//        eProgressBar.setVisibility(View.VISIBLE);

        Log.e("URL_IMAGE", url);
        Glide.with(context)
                .load(url)

                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        if (url.equalsIgnoreCase("")) {

                        } else {
                            eProgressBar.setVisibility(View.GONE);
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        if (url.equalsIgnoreCase("")) {

                        } else {
                            eProgressBar.setVisibility(View.GONE);
                        }
                        return false;
                    }
                })
                .crossFade()
                //  .error(R.drawable.placeholder_image)
                .into(img);
    }


}
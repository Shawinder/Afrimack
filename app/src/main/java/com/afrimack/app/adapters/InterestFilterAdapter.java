package com.afrimack.app.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.afrimack.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 13/3/18.
 */

public class InterestFilterAdapter extends RecyclerView.Adapter<InterestFilterAdapter.MyViewHolder> {

    private Drawable itemPic;
    static public List<String> allCatoList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;
    private InterestsAdapter.OnItemCheckListener onItemClick;
    List<String> currentSelectedItems;

    public InterestFilterAdapter(Context context, ArrayList<String> categories, List<String> currentSelectedItems, View.OnClickListener onClickListener, InterestsAdapter.OnItemCheckListener onItemClick) {
        this.allCatoList = categories;
        this.context = context;
        this.onClickListener = onClickListener;
        this.currentSelectedItems = currentSelectedItems;
        this.onItemClick = onItemClick;
    }


    @Override
    public InterestFilterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_list_items, parent, false);
        return new InterestFilterAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final InterestFilterAdapter.MyViewHolder holder, final int position) {

        holder.catogory_interest.setText(allCatoList.get(position).toString());
        holder.catogory_interest.setTag(position);
//        holder.catogory_interest.setOnClickListener(onClickListener);

        try {
            if (currentSelectedItems != null && currentSelectedItems.size() > 0) {
                for (int i = 0; i < currentSelectedItems.size(); i++) {

                    if (getCategoryPos(currentSelectedItems.get(i)) == position) {
                        holder.checkbox.setChecked(true);
                    }
                }

            } else {
                holder.checkbox.setChecked(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        ((InterestFilterAdapter.MyViewHolder) holder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InterestFilterAdapter.MyViewHolder) holder).checkbox.setChecked(
                        !((InterestFilterAdapter.MyViewHolder) holder).checkbox.isChecked());
                if (((InterestFilterAdapter.MyViewHolder) holder).checkbox.isChecked()) {
                    onItemClick.onItemCheck(allCatoList.get(position));
                } else {
                    onItemClick.onItemUncheck(allCatoList.get(position));
                }
            }
        });
    }

    private int getCategoryPos(String category) {
        return allCatoList.indexOf(category);
    }


    @Override
    public int getItemCount() {
        return allCatoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView catogory_interest;
        public CheckBox checkbox;


        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            catogory_interest = (TextView) itemView.findViewById(R.id.catogory_interest);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
            checkbox.setClickable(false);
        }

        @Override
        public void onClick(View view) {

        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }

    public interface OnItemCheckListener {
        void onItemCheck(String item);

        void onItemUncheck(String item);
    }
}

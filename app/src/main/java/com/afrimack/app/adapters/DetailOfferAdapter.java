package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityChat;
import com.afrimack.app.activities.OfferUserActivity;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.PostDetailResponseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DetailOfferAdapter extends RecyclerView.Adapter<DetailOfferAdapter.MyViewHolder> {

    private Drawable itemPic;
    private List<PostDetailResponseModel.ResponseBean.OffersBean> offerList = new ArrayList<>();
    Context context;
    private String post_id;
    private String post_user_id;
    String dealTepe;


    public DetailOfferAdapter(Context context, List<PostDetailResponseModel.ResponseBean.OffersBean> offers, String post_id, String follower_id, String dealTepe) {
        this.offerList = offers;
        this.context = context;
        this.post_id = post_id;
        this.post_user_id = follower_id;
        this.dealTepe = dealTepe;
    }


    @Override
    public DetailOfferAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.detailoffer_list, parent, false);
        return new DetailOfferAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DetailOfferAdapter.MyViewHolder holder, final int position) {
        String u_id = String.valueOf(offerList.get(position).getUser_id());
        holder.lay_offer_me.setVisibility(View.VISIBLE);
        holder.offer_user_me.setText(offerList.get(position).getOffer());

        Picasso
                .with(context)
                .load(offerList.get(position).getImage())
                .placeholder(R.drawable.dami_image2)
                .into(holder.offer_profile_me);

        holder.lay_offer_me.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String offer_user_id = String.valueOf(offerList.get(position).getUser_id());


                if (post_user_id.equalsIgnoreCase(Config.getUserId(context))) {
                    Intent intent = new Intent(context, ActivityChat.class);
                    Config.setOfferUserID(offer_user_id, context);
                    intent.putExtra("from", "details");
                    context.startActivity(intent);

                } else if (offer_user_id.equalsIgnoreCase(Config.getUserId(context))) {
                    Intent intent = new Intent(context, ActivityChat.class);
                    Config.setOfferUserID(offer_user_id, context);
                    intent.putExtra("from", "details");
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, OfferUserActivity.class);
                    intent.putExtra("other_user_id", offer_user_id);
                    Config.setOfferUserID(offer_user_id, context);
                    context.startActivity(intent);
                }
            }
        });

        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String offer_user_id = String.valueOf(offerList.get(position).getUser_id());

                if (post_user_id.equalsIgnoreCase(Config.getUserId(context))) {
                    Intent intent = new Intent(context, ActivityChat.class);
                    Config.setOfferUserID(offer_user_id, context);
                    intent.putExtra("from", "details");
                    context.startActivity(intent);

                } else if (offer_user_id.equalsIgnoreCase(Config.getUserId(context))) {
                    Intent intent = new Intent(context, ActivityChat.class);
                    Config.setOfferUserID(offer_user_id, context);
                    intent.putExtra("dealTepe", dealTepe);
                    intent.putExtra("from", "details");
                    context.startActivity(intent);

                } else {
                    Intent intent = new Intent(context, OfferUserActivity.class);
                     intent.putExtra("other_user_id",offer_user_id);
                    Config.setOfferUserID(offer_user_id, context);
                    context.startActivity(intent);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView offer_profile_me, offer_profile;
        public TextView offer_user_me, offer_user, tv_view;

        private LinearLayout lay_offer_me, lay_offer;

        public MyViewHolder(View itemView) {
            super(itemView);
            offer_profile_me = (ImageView) itemView.findViewById(R.id.offer_profile_me);
            offer_profile = (ImageView) itemView.findViewById(R.id.offer_profile);

            offer_user_me = (TextView) itemView.findViewById(R.id.offer_user_me);
            offer_user = (TextView) itemView.findViewById(R.id.offer_user);
            tv_view = (TextView) itemView.findViewById(R.id.tv_view);

            lay_offer_me = (LinearLayout) itemView.findViewById(R.id.lay_offer_me);
            lay_offer = (LinearLayout) itemView.findViewById(R.id.lay_offer);


        }
    }


}

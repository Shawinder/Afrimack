package com.afrimack.app.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;

import java.util.ArrayList;
import java.util.List;


public class InterestsAdapter extends RecyclerView.Adapter<InterestsAdapter.MyViewHolder> {

    private Drawable itemPic;
    static public List<String> allCatoList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;
    private OnItemCheckListener onItemClick;
    List<String> currentSelectedItems;

    public InterestsAdapter(Context context, ArrayList<String> categories, List<String> currentSelectedItems, View.OnClickListener onClickListener, OnItemCheckListener onItemClick) {
        this.allCatoList = categories;
        this.context = context;
        this.onClickListener = onClickListener;
        this.currentSelectedItems = currentSelectedItems;
        this.onItemClick = onItemClick;
    }


    @Override
    public InterestsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        return new InterestsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final InterestsAdapter.MyViewHolder holder, final int position) {

        holder.catogory_interest.setText(allCatoList.get(position).toString());
        holder.catogory_interest.setTag(position);
        final int color_gray = -3684409;

        try {
            if (currentSelectedItems != null && currentSelectedItems.size() > 0) {
                for (int i = 0; i < currentSelectedItems.size(); i++) {

                    if (getCategoryPos(currentSelectedItems.get(i)) == position) {
                        holder.checkbox.setChecked(true);
                        holder.main_layout.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));
                        holder.catogory_interest.setTextColor(context.getResources().getColor(R.color.color_black));
                    }
                }

            } else {
                holder.checkbox.setChecked(false);
                holder.main_layout.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                holder.catogory_interest.setTextColor(context.getResources().getColor(R.color.color_white));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ((MyViewHolder) holder).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                ColorDrawable buttonColor = (ColorDrawable) holder.main_layout.getBackground();
                int colorId = buttonColor.getColor();

                if (colorId == color_gray) {

                    holder.main_layout.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));
                    holder.catogory_interest.setTextColor(context.getResources().getColor(R.color.color_black));
                    holder.checkbox.setChecked(true);
                    holder.checkbox.setButtonTintList(context.getResources().getColorStateList(R.color.color_black));
                    if (!currentSelectedItems.contains(allCatoList.get(position))) {

                        onItemClick.onItemCheck(allCatoList.get(position));
                    }

                } else {
                    holder.checkbox.setChecked(false);
                    holder.checkbox.setButtonTintList(context.getResources().getColorStateList(R.color.color_white));

                    holder.main_layout.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                    holder.catogory_interest.setTextColor(context.getResources().getColor(R.color.color_white));
                    onItemClick.onItemUncheck(allCatoList.get(position));
                }
            }
        });


        holder.checkbox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                        if(b){
                            holder.main_layout.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));
                            holder.catogory_interest.setTextColor(context.getResources().getColor(R.color.color_black));
                            holder.checkbox.setChecked(true);
                            holder.checkbox.setButtonTintList(context.getResources().getColorStateList(R.color.color_black));
                            if (!currentSelectedItems.contains(allCatoList.get(position))) {

                                onItemClick.onItemCheck(allCatoList.get(position));
                            }
                        }
                        else{
                            holder.checkbox.setChecked(false);
                            holder.checkbox.setButtonTintList(context.getResources().getColorStateList(R.color.color_white));
                            holder.main_layout.setBackgroundColor(context.getResources().getColor(R.color.color_gray));
                            holder.catogory_interest.setTextColor(context.getResources().getColor(R.color.color_white));
                            onItemClick.onItemUncheck(allCatoList.get(position));
                        }
                    }
                }
        );
    }


    private int getCategoryPos(String category) {
        return allCatoList.indexOf(category);
    }

    @Override
    public int getItemCount() {
        return allCatoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView catogory_interest;
        public CheckBox checkbox;
        public LinearLayout main_layout;


        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            catogory_interest = (TextView) itemView.findViewById(R.id.catogory_interest);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
            main_layout = (LinearLayout) itemView.findViewById(R.id.main_layout);
            checkbox.setClickable(true);
            checkbox.setVisibility(View.VISIBLE);
        }

        @Override
        public void onClick(View view) {

        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }
    }

    public interface OnItemCheckListener {
        void onItemCheck(String item);

        void onItemUncheck(String item);
    }
}

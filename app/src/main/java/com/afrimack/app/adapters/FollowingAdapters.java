package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.OfferUserActivity;
import com.afrimack.app.models.FollowingResponseModel;
import com.squareup.picasso.Picasso;

import java.util.List;


public class FollowingAdapters extends RecyclerView.Adapter<FollowingAdapters.MyViewHolder> {

    private List<FollowingResponseModel.ResponseBean> sellList;
    Context context;
    View.OnClickListener onClickListener;

    public FollowingAdapters(Context context, List<FollowingResponseModel.ResponseBean> response, View.OnClickListener onClickListener) {
        this.sellList = response;
        this.context = context;
        this.onClickListener = onClickListener;

    }


    @Override
    public FollowingAdapters.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.following_list, parent, false);
        //View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviews_list, parent, false);
        return new FollowingAdapters.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FollowingAdapters.MyViewHolder holder, int position) {


        holder.tv_user_name_re.setText(sellList.get(position).getShort_name());
        holder.rating_bar_rev.setRating(Float.parseFloat(String.valueOf(sellList.get(position).getRating())));
        if (sellList.get(position).getSingle_registered().equalsIgnoreCase("Y")) {
            holder.img_heart.setVisibility(View.VISIBLE);
        } else {
            holder.img_heart.setVisibility(View.GONE);
        }


        holder.tv_unfollow.setTag(position);
        holder.tv_unfollow.setOnClickListener(onClickListener);
        Picasso.with(context)
                .load(sellList.get(position).getImage())
                .placeholder(R.drawable.user_round)
                .into(holder.img_user);

    }

    @Override
    public int getItemCount() {
        return sellList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_unfollow;
        private ImageView image_follow;
        private TextView tv_user_name_re, tv_post_date, post_titel;
        public RatingBar rating_bar_rev;
        public ImageView img_user, img_heart;
        private LinearLayout laya_following;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            tv_unfollow = (TextView) itemView.findViewById(R.id.tv_unfollow);

            tv_user_name_re = (TextView) itemView.findViewById(R.id.tv_user_name_re);

            rating_bar_rev = (RatingBar) itemView.findViewById(R.id.rating_bar_rev);
            img_user = (ImageView) itemView.findViewById(R.id.img_user);
            img_heart = (ImageView) itemView.findViewById(R.id.img_heart);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, OfferUserActivity.class);
            String single_user_id = String.valueOf(sellList.get(getPosition()).getFollower_id());
            intent.putExtra("other_user_id", single_user_id);
            context.startActivity(intent);
        }
    }

}

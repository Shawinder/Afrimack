package com.afrimack.app.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.models.SubQuation;
import com.squareup.picasso.Picasso;

import java.util.List;


public class SubQuestionAdapter extends RecyclerView.Adapter<SubQuestionAdapter.MyViewHolder> {

    private Drawable itemPic;
     public List<PostDetailResponseModel.ResponseBean.QuestionsBean.SubquestionBean> subquestion ;
    Context context;
    View.OnClickListener onClickListener;
    SubQutiionDelete subQutiionDelete;

    public SubQuestionAdapter(Context context, List<PostDetailResponseModel.ResponseBean.QuestionsBean.SubquestionBean> subquestion, View.OnClickListener onClickListener, SubQutiionDelete subQutiionDelete) {
        this.subquestion = subquestion;
        this.context = context;
        this.onClickListener = onClickListener;
        this.subQutiionDelete =subQutiionDelete;
    }


    @Override
    public SubQuestionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_list, parent, false);
        return new SubQuestionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubQuestionAdapter.MyViewHolder holder,final int position) {
        String user_id = String.valueOf(subquestion.get(position).getUser_id());
        if(user_id .equalsIgnoreCase(Config.getUserId(context))){
            holder.sub_block.setVisibility(View.VISIBLE);
        }else {
            holder.sub_block.setVisibility(View.GONE);
        }
        holder.question_user_name.setText(subquestion.get(position).getUser());
        holder.question_message.setText(subquestion.get(position).getQuestion());
        holder.question_date.setText(subquestion.get(position).getDateTime());
        Picasso
                .with(context)
                .load(subquestion.get(position).getImage())
                .placeholder(R.drawable.user_round)
                .into(holder.question_profilee);
        holder.question_profilee.setTag(position);
        holder.question_profilee.setOnClickListener(onClickListener);

        holder.sub_block.setTag(position);
        holder.sub_block.setOnClickListener(onClickListener);
        holder.sub_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SubQuation subQuation = new SubQuation();
                String sub_message = "";
                subQuation.setPostion(String.valueOf(position));
                subQuation.setQution_id(String.valueOf(subquestion.get(position).getQuestion_id()));
                subQuation.setSubqution(sub_message);
                subQutiionDelete.getSubdelete(subQuation);
            }
        });


    }



    @Override
    public int getItemCount() {
        return subquestion.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView question_profilee,sub_block;
        public TextView question_user_name,question_message,question_date;


        public MyViewHolder(View itemView) {
            super(itemView);
            question_profilee = (ImageView)itemView.findViewById(R.id.question_profilee);
            sub_block = (ImageView) itemView.findViewById(R.id.sub_block);

            question_user_name = (TextView)itemView.findViewById(R.id.question_user_name);
            question_message = (TextView)itemView.findViewById(R.id.question_message);
            question_date = (TextView)itemView.findViewById(R.id.question_date);




        }
    }
    public interface SubQutiionDelete{
        public void getSubdelete(SubQuation sub_message);
    }

}

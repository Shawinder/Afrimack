package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.SingleProfileViewActivity;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.OtherUserResponseModel;
import com.afrimack.app.models.SingleUserResponseModel;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SingleDationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    LayoutInflater layoutInflater;
    public static List<SingleUserResponseModel.ResponseBean.UsersBean> singlelist;
    private final int TYPE_ITEM = 0;
    private final int VIEW_PROG = 1;
    private int visibleThreshold = 2;
    //private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private int pastVisibleItems, visibleItemCount, totalItemCount;

    public SingleDationAdapter(Context context, List<SingleUserResponseModel.ResponseBean.UsersBean> users, RecyclerView recyclerView, OnLoadMoreListener mOnLoadMoreListener) {
        this.context = context;
        this.singlelist = users;
        this.onLoadMoreListener = mOnLoadMoreListener;

        final StaggeredGridLayoutManager linearLayoutManager = (StaggeredGridLayoutManager) recyclerView
                .getLayoutManager();
        recyclerView
                .addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView,
                                           int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        totalItemCount = linearLayoutManager.getItemCount();
//                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                            // End has been reached
//                            // Do something
//                            if (onLoadMoreListener != null) {
//                                onLoadMoreListener.onLoadMore();
//                            }
//
//                            loading = true;
//
//                        }


                        visibleItemCount = linearLayoutManager.getChildCount();
                        totalItemCount = linearLayoutManager.getItemCount();
                        int[] firstVisibleItems = null;
                        firstVisibleItems = linearLayoutManager.findFirstVisibleItemPositions(firstVisibleItems);
                        if (firstVisibleItems != null && firstVisibleItems.length > 0) {
                            pastVisibleItems = firstVisibleItems[0];
                        }

                        if (loading) {
                            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                                loading = false;
                                Log.d("tag", "LOAD NEXT ITEM");
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                            }
                        }

                    }
                });
    }


    @Override
    public int getItemViewType(int position) {
        return (singlelist.size() > 0 && singlelist.get(position) == null) ? VIEW_PROG : TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_PROG) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.item_loading, parent, false);
            return new SingleDationAdapter.LoadingViewHolder(view);
        } else {


            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_list_single_dating, parent, false);
            return new SingleDationAdapter.MyViewHolder(layoutView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        if (holder1 instanceof SingleDationAdapter.MyViewHolder) {
            SingleDationAdapter.MyViewHolder holder = (SingleDationAdapter.MyViewHolder) holder1;

            Picasso.with(context)
                    .load(singlelist.get(position).getImage())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(holder.countryPhoto);
            holder.countryName.setText(singlelist.get(position).getShort_name());//+" "+singlelist.get(position).getLast_name());
            holder.tv_grid_prige.setText(singlelist.get(position).getCountry_name());

        }
        if (holder1 instanceof AdapterStaggreGrid.LoadingViewHolder) {
            SingleDationAdapter.LoadingViewHolder holder = (SingleDationAdapter.LoadingViewHolder) holder1;

            holder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        if (singlelist != null)
            return singlelist.size();
        else
            return 0;
    }

    public void setLoaded() {

        loading = true;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {



        public TextView countryName, tv_grid_prige, sold;
        public ImageView countryPhoto;
        private List<OtherUserResponseModel.ResponseBean.SellingArrBean> List;
        public LinearLayout ll_fram_hot;
        public CardView mCardView;
        public LinearLayout mFramelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            mFramelayout = (LinearLayout) itemView.findViewById(R.id.frame_staggered_grid);
            mCardView = (CardView) itemView.findViewById(R.id.card_view);
            countryName = (TextView) itemView.findViewById(R.id.country_name);
            countryPhoto = (ImageView) itemView.findViewById(R.id.country_photo);
            tv_grid_prige = (TextView) itemView.findViewById(R.id.tv_grid_prige);
            ll_fram_hot = (LinearLayout) itemView.findViewById(R.id.ll_fram_hot);
            sold = (TextView) itemView.findViewById(R.id.sold);


        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, SingleProfileViewActivity.class);
            String single_user_id = String.valueOf(singlelist.get(getPosition()).getId());
            Config.setSingleChatId(single_user_id, context);
            intent.putExtra("from","SingleDation");
            context.startActivity(intent);
        }
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        }
    }
}

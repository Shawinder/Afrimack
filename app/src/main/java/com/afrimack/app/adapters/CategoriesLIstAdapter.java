package com.afrimack.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.appbeans.CategoryBean;
import com.afrimack.app.customprefs.Config;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import static com.facebook.FacebookSdk.getApplicationContext;


public class CategoriesLIstAdapter extends RecyclerView.Adapter<CategoriesLIstAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    static public List<CategoryBean> mLIst;
    private SubcategoryAdapter subcategoryAdapter;
    String status = "0";
    //    int prev_pos=-1;
    int clickedPosition = -1;
    List<MyViewHolder> listRecycler = new ArrayList<>();


    public CategoriesLIstAdapter(Context context, List<CategoryBean> mLIst) {
        this.mLIst = mLIst;
        this.context = context;
    }


    @Override
    public CategoriesLIstAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.discover_category_item_layout, parent, false);
        return new CategoriesLIstAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CategoriesLIstAdapter.MyViewHolder holder, final int position) {
        listRecycler.add(position, holder);
        Picasso.with(context)
                .load(mLIst.get(position).getIcon_logo())
                //.placeholder(R.drawable.dami_background)
                //.error(R.drawable.dami_background)
                .into(holder.imgLOgo);
        holder.txtName.setText(mLIst.get(position).getCategory_name());
        holder.imgLOgo.setColorFilter(ContextCompat.getColor(context, R.color.color_black), android.graphics.PorterDuff.Mode.MULTIPLY);

//        prev_pos=position;

        holder.lay_discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (status.equalsIgnoreCase("0")) {
                    clickedPosition = position;
                    status = "1";
                    if (mLIst.get(position).getSubCat() != null && mLIst.get(position).getSubCat().size() > 0) {
                        holder.recyclerView_subCategory.setVisibility(View.VISIBLE);
                        String id = String.valueOf(mLIst.get(position).getId());
                        Config.setSortSpecialCategory("", context);
                        Config.setSortCategory(id, context);
                        subcategoryAdapter = new SubcategoryAdapter(context, mLIst.get(position).getSubCat());
                        holder.recyclerView_subCategory.setAdapter(subcategoryAdapter);

                    } else {
                        holder.recyclerView_subCategory.setVisibility(View.GONE);
                        String id = String.valueOf(mLIst.get(position).getId());
                        Config.setSortSpecialCategory("", context);
                        Config.setSortCategory(id, context);
                        Intent intent = new Intent(context, ActivityHome.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();

                    }
                    view.setBackground(context.getResources().getDrawable(R.drawable.categories_background_filled));
                } else {
                    status = "0";
                    holder.recyclerView_subCategory.setVisibility(View.GONE);
                    view.setBackground(context.getResources().getDrawable(R.drawable.categories_background_gray));
                    notifyDataSetChanged();

                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (clickedPosition != position) {
                                if (mLIst.get(position).getSubCat() != null && mLIst.get(position).getSubCat().size() > 0) {
                                    listRecycler.get(position).recyclerView_subCategory.setVisibility(View.VISIBLE);
                                    notifyItemChanged(position, listRecycler.get(position).recyclerView_subCategory);
                                    String id = String.valueOf(mLIst.get(position).getId());
                                    Config.setSortSpecialCategory("", context);
                                    Config.setSortCategory(id, context);
                                    subcategoryAdapter = new SubcategoryAdapter(context, mLIst.get(position).getSubCat());
                                    holder.recyclerView_subCategory.setAdapter(subcategoryAdapter);


                                } else {
                                    holder.recyclerView_subCategory.setVisibility(View.GONE);
                                    String id = String.valueOf(mLIst.get(position).getId());
                                    Config.setSortSpecialCategory("", context);
                                    Config.setSortCategory(id, context);
                                    Intent intent = new Intent(context, ActivityHome.class);
                                    context.startActivity(intent);
                                    ((Activity) context).finish();

                                }
                                view.setBackground(context.getResources().getDrawable(R.drawable.categories_background_filled));
                            }
                        }
                    }, 500);


                    // holder.lay_discover.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));
                    holder.imgLOgo.setColorFilter(ContextCompat.getColor(context, R.color.color_black), android.graphics.PorterDuff.Mode.MULTIPLY);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mLIst.size();
    }


    public void notifyClickedPosition(int position) {
        MyViewHolder myViewHolder = listRecycler.get(position);
        myViewHolder.recyclerView_subCategory.setVisibility(View.GONE);
        notifyItemChanged(position, myViewHolder.recyclerView_subCategory);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtName;
        private ImageView imgLOgo;
        private LinearLayout lay_discover;
        private RecyclerView recyclerView_subCategory;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            txtName = (TextView) itemView.findViewById(R.id.txt_title_name);
            imgLOgo = (ImageView) itemView.findViewById(R.id.img_logo);
            lay_discover = (LinearLayout) itemView.findViewById(R.id.lay_discover);
            recyclerView_subCategory = (RecyclerView) itemView.findViewById(R.id.recyclerView_subCategory);
            try {
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView_subCategory.setLayoutManager(mLayoutManager);
                recyclerView_subCategory.setHasFixedSize(true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onClick(View view) {
            txtName.setTextColor(context.getResources().getColor(R.color.color_black));
//            try {
//                view.setBackground(context.getResources().getDrawable(R.drawable.categories_background));
//                imgLOgo.setColorFilter(ContextCompat.getColor(context, R.color.color_yallow), android.graphics.PorterDuff.Mode.MULTIPLY);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }


            String id = String.valueOf(mLIst.get(getPosition()).getId());
            Config.setSortSpecialCategory("", context);
            Config.setSortCategory(id, context);
//            Intent intent = new Intent(context, ActivityHome.class);
//            context.startActivity(intent);
//            ((Activity) context).finish();
//
        }

    }
}

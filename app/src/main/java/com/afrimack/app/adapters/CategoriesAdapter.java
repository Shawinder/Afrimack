package com.afrimack.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.afrimack.app.R;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    private List<String> moviesLists;
    int row_index;

    public CategoriesAdapter(List<String> movieList) {
        this.moviesLists = movieList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_list, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.radioButton.setText(moviesLists.get(position));

        holder.lay_cate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index=position;
                notifyDataSetChanged();
            }
        });
        if(row_index==position){

            if (!holder.radioButton.isChecked()) {
                holder.radioButton.setChecked(false);
            } else {
                holder.radioButton.setChecked(false);
            }
        }

    }

    @Override
    public int getItemCount() {
        return moviesLists.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private RadioButton radioButton;
        private LinearLayout lay_cate;

        public MyViewHolder(View itemView) {
            super(itemView);

            radioButton = (RadioButton) itemView.findViewById(R.id.rb_categries);
            lay_cate = (LinearLayout) itemView.findViewById(R.id.lay_cate);
        }
    }
}

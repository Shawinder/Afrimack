package com.afrimack.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.models.FilterResponseModel;

import java.util.List;

import static com.afrimack.app.activities.ActivityFilter.stList;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    private List<FilterResponseModel.ResponseBean.CategoryBean> itemList;
    int row_index;
    boolean checkclicl;
    private String listcheck;


    public FilterAdapter(Context context, List<FilterResponseModel.ResponseBean.CategoryBean> category, boolean b, String listcheck) {
        this.itemList = category;
        this.context = context;
        this.checkclicl = b;
        this.listcheck = listcheck;
    }


    @Override
    public FilterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_list, parent, false);

        return new FilterAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(FilterAdapter.MyViewHolder holder, int position) {


        final int pos = position;

        if (listcheck.equalsIgnoreCase("Yes")) {
            if (stList != null && stList.size() > 0) {
                if (stList.get(position).isSelected()) {
                    itemList.get(pos).setSelected(true);
                } else {
                    itemList.get(pos).setSelected(false);
                }

            }
        } else {
        }


        holder.check_list_filter.setTag(itemList.get(position));
        holder.check_list_filter.setText(itemList.get(position).getCategory_name());

        holder.lay_list_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(itemList.get(pos).isSelected())
                {
                    itemList.get(pos).setSelected(false);
                }
                else
                {
                    itemList.get(pos).setSelected(true);
                }
                row_index = pos;
                notifyDataSetChanged();
            }
        });
        holder.check_list_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listcheck = "NO";
                CheckBox cb = (CheckBox) v;
                FilterResponseModel.ResponseBean.CategoryBean contact = (FilterResponseModel.ResponseBean.CategoryBean) cb.getTag();

                contact.setSelected(cb.isChecked());

                itemList.get(pos).setSelected(cb.isChecked());
                row_index = pos;
                notifyDataSetChanged();
            }
        });

        if (itemList.get(pos).isSelected()) {
            holder.lay_list_filter.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));
            holder.check_list_filter.setChecked(true);
            holder.check_list_filter.setTextColor(context.getResources().getColor(R.color.color_black));
        } else {
            holder.lay_list_filter.setBackgroundColor(context.getResources().getColor(R.color.border_gray));
            holder.check_list_filter.setChecked(false);
            holder.check_list_filter.setTextColor(context.getResources().getColor(R.color.color_white));
        }


    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LinearLayout lay_list_filter;
        private CheckBox check_list_filter;

        public MyViewHolder(View itemView) {
            super(itemView);
            lay_list_filter = (LinearLayout) itemView.findViewById(R.id.lay_list_filter);
            check_list_filter = (CheckBox) itemView.findViewById(R.id.check_list_filter);

        }

        @Override
        public void onClick(View v) {
        }
    }


    public List<FilterResponseModel.ResponseBean.CategoryBean> getStudentist() {
        return itemList;
    }
}

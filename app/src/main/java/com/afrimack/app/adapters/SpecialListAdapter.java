package com.afrimack.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.activities.SingleDatingActivity;
import com.afrimack.app.activities.SplashLogin;
import com.afrimack.app.appbeans.SpecialBean;
import com.afrimack.app.customprefs.Config;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by root on 6/11/17.
 */

public class SpecialListAdapter extends RecyclerView.Adapter<SpecialListAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    static public List<SpecialBean> mLIst;
    private Fragment frg;


    public SpecialListAdapter(Context context, List<SpecialBean> mLIst) {
        this.mLIst = mLIst;
        this.context = context;
    }


    @Override
    public SpecialListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.discover_category_item_layout, parent, false);
        return new SpecialListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SpecialListAdapter.MyViewHolder holder, final int position) {

        Picasso.with(context)
                .load(mLIst.get(position).getIcon_logo())
//                .placeholder(R.drawable.dami_background)
//                .error(R.drawable.dami_background)
                .into(holder.imgLOgo);
        holder.txtName.setText(mLIst.get(position).getCategory_name());
        holder.imgLOgo.setColorFilter(ContextCompat.getColor(context, R.color.color_black), android.graphics.PorterDuff.Mode.MULTIPLY);
        holder.lay_discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                view.setBackground(context.getResources().getDrawable(R.drawable.categories_background_filled));
                String categoryname = String.valueOf(mLIst.get(position).getCategory_name());
                if (categoryname.equalsIgnoreCase("Friends")) {
                    if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
                        Intent intent = new Intent(context, SingleDatingActivity.class);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, SplashLogin.class);
                        context.startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(context, ActivityHome.class);
                    String id = String.valueOf(mLIst.get(position).getId());
                    Config.setSortCategory("", context);
                    Config.setSortSpecialCategory(id, context);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        return mLIst.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtName;
        private ImageView imgLOgo;
        private LinearLayout lay_discover;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_title_name);
            imgLOgo = (ImageView) itemView.findViewById(R.id.img_logo);
            lay_discover = (LinearLayout) itemView.findViewById(R.id.lay_discover);
        }



//        @Override
//        public void onClick(View view) {
//            txtName.setTextColor(context.getResources().getColor(R.color.color_black));
//            try {
//                view.setBackground(context.getResources().getDrawable(R.drawable.category_black_border));
//                imgLOgo.setColorFilter(ContextCompat.getColor(context, R.color.color_black), android.graphics.PorterDuff.Mode.MULTIPLY);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
////            String categoryname = String.valueOf(mLIst.get(getPosition()).getCategory_name());
////            if (categoryname.equalsIgnoreCase("Friends")) {
////                if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
////                    Intent intent = new Intent(context, SingleDatingActivity.class);
////                    context.startActivity(intent);
////                } else {
////                    Intent intent = new Intent(context, SplashLogin.class);
////                    context.startActivity(intent);
////                }
////            } else {
////                Intent intent = new Intent(context, ActivityHome.class);
////                String id = String.valueOf(mLIst.get(getPosition()).getId());
////                Config.setSortCategory("", context);
////                Config.setSortSpecialCategory(id, context);
////                context.startActivity(intent);
////                ((Activity) context).finish();
////            }
//
//        }

    }
}

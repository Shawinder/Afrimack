package com.afrimack.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afrimack.app.R;
import com.afrimack.app.models.OtherUserResponseModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OtherUserAdapter extends RecyclerView.Adapter<OtherViewHolders> {

    private List<OtherUserResponseModel.ResponseBean.SellingArrBean> itemList;
    private Context context;


    public OtherUserAdapter(Context context, List<OtherUserResponseModel.ResponseBean.SellingArrBean> selling_arr) {
        this.itemList = selling_arr;
        this.context = context;
    }


    @Override
    public OtherViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_list_new, null);
        OtherViewHolders rcv = new OtherViewHolders(layoutView, context, itemList);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final OtherViewHolders holder, int position) {
        try {
            if (itemList.get(position).getImage() != null) {
                Glide.with(context).load(itemList.get(position).getImage()).asBitmap().
                        into(new BitmapImageViewTarget(holder.countryPhoto) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                holder.countryPhoto.setImageBitmap(resource);
                                if (resource.getWidth() > resource.getHeight()) {
                                    holder.countryPhoto.getLayoutParams().height = 300;
                                    holder.countryPhoto.requestLayout();
                                }

                            }
                        });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.countryName.setText(itemList.get(position).getTitle());
        try {
            String sub = itemList.get(position).getPrice();
            String s = sub.substring(0, sub.indexOf("."));
            if (sub.contains(".00")) {
                holder.tv_grid_prige.setText(s);
            } else {
                holder.tv_grid_prige.setText(sub);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (itemList.get(position).getPost_status().equalsIgnoreCase("C")) {
            holder.tv_premium.setVisibility(View.VISIBLE);
            holder.tv_premium.setBackgroundColor(context.getResources().getColor(R.color.color_yallow));
            holder.tv_premium.setText(R.string.deal_done);
        }


    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}

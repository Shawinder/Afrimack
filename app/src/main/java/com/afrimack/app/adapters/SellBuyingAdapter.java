package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityDiscoverDetail;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.SellingResponseModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class SellBuyingAdapter extends RecyclerView.Adapter<SellBuyingAdapter.MyViewHolder> {

    private Drawable itemPic;
    static public List<SellingResponseModel.ResponseBean.PostsBean> selllList = new ArrayList<>();
    Context context;
    View.OnClickListener onClickListener;

    public SellBuyingAdapter(Context context, List<SellingResponseModel.ResponseBean.PostsBean> posts, View.OnClickListener onClickListener) {

        this.selllList = posts;
        this.context = context;
        this.onClickListener = onClickListener;
    }


    @Override
    public SellBuyingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_selling_list_item, parent, false);
        return new SellBuyingAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SellBuyingAdapter.MyViewHolder holder, int position) {

        holder.tvSellingProductName.setText(selllList.get(position).getTitle());
        try {
            String sub = selllList.get(position).getPrice();
            String s = sub.substring(0, sub.indexOf("."));
            if (sub.contains(".00")) {
                holder.tvSellingProductPrice.setText(s);
            } else {
                holder.tvSellingProductPrice.setText(sub);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        Picasso.with(context)
                .load(selllList.get(position).getImage())
                .placeholder(R.drawable.camera)
                .error(R.drawable.camera)
                .into(holder.ivSellingProductBg);
        holder.tvSellingHours.setText(selllList.get(position).getDateTime());
        holder.tvLikes_total.setText(String.valueOf(selllList.get(position).getFollowing()));

        holder.ivSellingItemClose.setTag(position);
        holder.ivSellingItemClose.setOnClickListener(onClickListener);
        if (selllList.get(position).getPost_status().equalsIgnoreCase("C")) {
            holder.sold.setVisibility(View.VISIBLE);
        }
        if (selllList.get(position).getPost_status().equalsIgnoreCase("P")) {
            holder.sold.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return selllList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivSellingProductBg;
        public TextView tvSellingProductName;
        public TextView tvSellingProductPrice;
        public TextView tvLikes_total;
        public TextView tvSellingHours;
        public ImageView ivSellingItemClose;
        private TextView sold;
        private TextView tv_edit;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ivSellingProductBg = (ImageView) itemView.findViewById(R.id.ivSellingProductBg);
            tvSellingProductName = (TextView) itemView.findViewById(R.id.tvSellingProductName);
            tvSellingProductPrice = (TextView) itemView.findViewById(R.id.tvSellingProductPrice);
            tvLikes_total = (TextView) itemView.findViewById(R.id.tvLikes_total);
            tvSellingHours = (TextView) itemView.findViewById(R.id.tvSellingHours);
            sold = (TextView) itemView.findViewById(R.id.sold);
            ivSellingItemClose = (ImageView) itemView.findViewById(R.id.ivSellingItemClose);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityDiscoverDetail.class);
            String post_id = String.valueOf(selllList.get(getPosition()).getPost_id());
            Config.setPostId(post_id, context);
            context.startActivity(intent);

        }
    }
}

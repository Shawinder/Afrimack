package com.afrimack.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.OfferUserActivity;
import com.afrimack.app.models.BlockUserModel;
import com.squareup.picasso.Picasso;

import java.util.List;


public class BlockUserAdapter extends RecyclerView.Adapter<BlockUserAdapter.MyViewHolder> {

    private List<BlockUserModel.ResponseBean> blovkUserlList;
    Context context;
    View.OnClickListener onClickListener;


    public BlockUserAdapter(Context context, List<BlockUserModel.ResponseBean> response, View.OnClickListener onClickListener) {
        this.blovkUserlList = response;
        this.context = context;
        this.onClickListener = onClickListener;
    }


    @Override
    public BlockUserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.blockuser_list, parent, false);
        return new BlockUserAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BlockUserAdapter.MyViewHolder holder, int position) {

        String name = blovkUserlList.get(position).getName();
        try {
            holder.tv_user_name_re.setText(name.substring(0, name.indexOf(" ") + 2));
        } catch (Exception e) {
            holder.tv_user_name_re.setText(name);
        }

        holder.tv_unblock.setTag(position);
        holder.tv_unblock.setOnClickListener(onClickListener);
        holder.rating.setVisibility(View.GONE);
        Picasso.with(context)
                .load(blovkUserlList.get(position).getImage())
                .placeholder(R.drawable.dami_image2)
                .into(holder.img_user);
    }

    @Override
    public int getItemCount() {
        return blovkUserlList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private TextView tv_user_name_re, tv_user_date, tv_unblock;
        public ImageView img_user;
        private LinearLayout laya_following;
        private RatingBar rating;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            tv_user_name_re = (TextView) itemView.findViewById(R.id.tv_user_name_re);
            tv_unblock = (TextView) itemView.findViewById(R.id.tv_unblock);
            rating = (RatingBar) itemView.findViewById(R.id.rating_bar_rev_blocked_user);


            img_user = (ImageView) itemView.findViewById(R.id.img_block_user);

        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(context, OfferUserActivity.class);
            String single_user_id = String.valueOf(blovkUserlList.get(getPosition()).getUser_id());
            intent.putExtra("other_user_id", single_user_id);
            context.startActivity(intent);
        }
    }
}

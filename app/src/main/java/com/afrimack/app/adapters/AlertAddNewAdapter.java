package com.afrimack.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.models.AlertSearchResponseModel;

import java.util.List;


public class AlertAddNewAdapter extends RecyclerView.Adapter<AlertAddNewAdapter.MyViewHolder> {
    Context context;
    LayoutInflater layoutInflater;
    static public List<AlertSearchResponseModel.ResponseBean.AlertBean> alertList;
    View.OnClickListener onClickListener;


    public AlertAddNewAdapter(Context context, List<AlertSearchResponseModel.ResponseBean.AlertBean> alert,
                              View.OnClickListener onClickListener) {
        this.alertList = alert;
        this.context = context;
        this.onClickListener = onClickListener;
    }


    @Override
    public AlertAddNewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.seach_add_list, parent, false);
        return new AlertAddNewAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AlertAddNewAdapter.MyViewHolder holder, int position) {
        holder.lay_seach.setVisibility(View.GONE);
        holder.seach_alert.setVisibility(View.VISIBLE);
        holder.tv_alret_titel.setText(alertList.get(position).getTerm());
        holder.tv_alret_titel.setTag(alertList.get(position).getTerm());
        holder.tv_alret_titel.setOnClickListener(onClickListener);
        holder.img_alrt_colose.setTag(position);
        holder.img_alrt_colose.setOnClickListener(onClickListener);

    }

    @Override
    public int getItemCount() {
        return alertList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_list_tetel, tv_alret_titel;
        public LinearLayout lay_seach, seach_alert;
        public ImageView img_list_cose, img_alrt_colose;


        public MyViewHolder(View itemView) {
            super(itemView);

            lay_seach = (LinearLayout) itemView.findViewById(R.id.lay_seach);
            tv_list_tetel = (TextView) itemView.findViewById(R.id.tv_list_tetel);
            img_list_cose = (ImageView) itemView.findViewById(R.id.img_list_cose);


            tv_alret_titel = (TextView) itemView.findViewById(R.id.tv_alret_titel);
            seach_alert = (LinearLayout) itemView.findViewById(R.id.seach_alert);
            img_alrt_colose = (ImageView) itemView.findViewById(R.id.img_alrt_colose);
        }
    }
}

package com.afrimack.app;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.support.multidex.MultiDex;

import com.afrimack.app.services.LocationServiceListner;

import java.util.List;


public class AfrimackApp extends Application {
    private static AfrimackApp mInstance;
    public static Location mLastLocation;

    public static Activity curentactivity;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    public static synchronized AfrimackApp getInstance() {
        return mInstance;
    }


}

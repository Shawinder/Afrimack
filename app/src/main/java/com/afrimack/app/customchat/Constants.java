package com.afrimack.app.customchat;


/**
 * Created by ubuntu on 30/5/16.
 */
public class Constants {

    public static final int INVALID_ID_INT = 0;
    public static final boolean INVALID_ID_BOOLEAN = false;
    public static final float TXT_FONT_SIZE = 15.0f;
    public static final int EMOJI_FONT_SIZE = 29;

    public static final String INVALID_ID_STRING = "-1";
    public static final String INVALID_STRING = "";

    public static final String COMINGDATEFORMATFROMSERVER = "MM/dd/yyyy HH:mm:ss";

    public static final String KEY_FIRST_TIME_CONTACT_SYNC = "contactsyncfirst";

    public static final String KEY_NOTIFICATION_TYPE = "NotificationType";
}

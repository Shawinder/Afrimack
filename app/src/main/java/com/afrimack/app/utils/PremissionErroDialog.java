package com.afrimack.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.afrimack.app.R;


/**
 * Created by ubuntu on 12/5/16.
 */
public class PremissionErroDialog extends Dialog implements
        View.OnClickListener {
    Context context;
    PremissionErroDialog dia;
    //String msg = "";
    TextView et_msg, tv_cancelbtn, tv_okbtn;
    PremissionErroDialogListner premissionErroDialogListner;

    public PremissionErroDialog(Context context, String msg, PremissionErroDialogListner premissionErroDialogListner) {
        super(context);
        this.context = context;
        dia = this;
        this.premissionErroDialogListner = premissionErroDialogListner;
        // this.msg = msg;

    }

    public void setMessage(String msg) {
        // this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(R.color.color_semiblack);
        LayoutInflater inflate = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View layout = inflate.inflate(R.layout.dialog_premissionerror, null);
        setContentView(layout);

        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER;
        wlmp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        setTitle(null);
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        setOnCancelListener(null);


        tv_okbtn.setOnClickListener(this);
        tv_cancelbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_okbtn) {
            if (premissionErroDialogListner != null) {
                premissionErroDialogListner.clickOnok(this);
            }
        } else if (v.getId() == R.id.tv_cancelbtn) {
            if (premissionErroDialogListner != null) {
                premissionErroDialogListner.clickOncancel(this);
            }
        }
    }


    public interface PremissionErroDialogListner {
        public void clickOnok(Dialog dialog);

        public void clickOncancel(Dialog dialog);
    }
}

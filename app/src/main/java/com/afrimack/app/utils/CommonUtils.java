package com.afrimack.app.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;
import android.widget.Toast;

import com.afrimack.app.R;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CommonUtils {
    ProgressDialog mLoadingDialog;
    android.app.AlertDialog mErrorDialog;

    private CommonUtils() {
        // empty constructor.
    }
    public static String Id="";
    public static String CategoryName="";

    //===========array_subcategory_id===============

    public static ArrayList<ArrayList<String>> getSub_categories_Id() {
        return Sub_categories_Id;
    }

    public static void setSub_categories_Id(ArrayList<ArrayList<String>> sub_categories_Id) {
        Sub_categories_Id = sub_categories_Id;
    }

    public static  ArrayList<ArrayList<String>> Sub_categories_Id = new ArrayList<>();
    //===================arrry_category_id==============
    public static ArrayList<String> getCategory_id() {
        return category_id;
    }

    public static void setCategory_id(ArrayList<String> category_id) {
        CommonUtils.category_id = category_id;
    }

    public static  ArrayList<String> category_id;

    //====================arry_categories======================

    public static ArrayList<String> getCategories() {
        return categories;
    }

    public static void setCategories(ArrayList<String> categories) {
        CommonUtils.categories = categories;
    }

    public static  ArrayList<String> categories;

    //=======================array_subcategories=======================

    public static ArrayList<ArrayList<String>> getSub_categories() {
        return Sub_categories;
    }

    public static void setSub_categories(ArrayList<ArrayList<String>> sub_categories) {
        Sub_categories = sub_categories;
    }

    public static  ArrayList<ArrayList<String>> Sub_categories = new ArrayList<>();

    //=====================================================================

    private static class UtilsHelper {
        private static final CommonUtils INSTANCE = new CommonUtils();
    }

    public static CommonUtils getInstance() {
        return UtilsHelper.INSTANCE;
    }


    public void displayLoadingDialog(boolean isCancellable, Context context) {
        try {


          mLoadingDialog = new ProgressDialog(context);
            mLoadingDialog = ProgressDialog.show(context, null, null, false, true);
//            mLoadingDialog.setTitle(com.brsoftech.core_utils.R.string.loading);
//            mLoadingDialog.setMessage(context.getString(com.brsoftech.core_utils.R.string.please_wait));
            mLoadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mLoadingDialog.setContentView(R.layout.progress_barr);
//            mLoadingDialog.setIndeterminate(true);
//            mLoadingDialog.setCancelable(isCancellable);
//            mLoadingDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissLoadingDialog() {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isNetConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isAvailable() && info.isConnected();
    }

    public boolean isEmailValid(String email) {
        return email != null && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void showToastMessage(Context context, String message) {
        if (message == null && context == null) {
            return;
        }
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }

    public void showAlertMessage(Context context, String message) {
        if (message == null && context == null) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setMessage(message);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        try {
            builder.show();
        } catch (Exception ignored) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }

    }

    public String getCurrentTime() {
        try {
            Date currentTime = Calendar.getInstance().getTime();
//            time format2017-11-01 15:30:00
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            return sdf.format(currentTime);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    public String getCountryName(Context context, double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(context);
        String country = "";
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return country;
    }

    public String getCompleteAddress(Context context, double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(context);
        String address = "", country = "", city = "", state = "";
        String completeAddress = "";
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            state = addresses.get(0).getAdminArea();
            country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            if (addresses.get(0).getMaxAddressLineIndex() > 0)
                completeAddress = address + "," + city + "," + state + "," + country;
            else
                completeAddress = city + "," + state + "," + country;
        } catch (IOException e) {
            e.printStackTrace();
        }


        return completeAddress;
    }

    public String getAppVersion(Context context) {
        String version = "1.0.1";
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return version;
    }

    public String getCurrentVersionCode(Context context) {
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return info != null ? String.valueOf(info.versionCode) : "";
    }


//    retrofit = new Retrofit.Builder()
//            .baseUrl(AppConstants.BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//            .build();
}

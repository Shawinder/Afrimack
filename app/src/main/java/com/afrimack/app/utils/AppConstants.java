package com.afrimack.app.utils;

import android.view.animation.AlphaAnimation;

/**
 * Created by root on 6/11/17.
 */

public class AppConstants {

    public static AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.5F);
    public static final long SPLASH_TIME_OUT = 3000;
    public static final String BASE_URL = "https://afrimack.com/api/";
    public static final String LOC_URL = "https://maps.googleapis.com/maps/api/";
}

package com.afrimack.app.sociallogin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.afrimack.app.models.SocialData;
import com.afrimack.app.utils.PremissionErroDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.Set;


public class FacebookLoginHandler {

    private static final String TAG = "FacebookLoginHandler";
    static FacebookLoginHandler facebookLoginHandler;
    CallbackManager callbackManager;
    SocialLoginListner socialLoginListner;
    private Context mContext;

    public FacebookLoginHandler(Context context) {
        mContext = context;
        setSocialLoginListner((SocialLoginListner) context);

        facebookInit();
    }

    public static FacebookLoginHandler getInstance(Context context) {
        if (facebookLoginHandler == null) {
            facebookLoginHandler = new FacebookLoginHandler(context);
        }
        return facebookLoginHandler;
    }

    public void setSocialLoginListner(SocialLoginListner socialLoginListner) {
        this.socialLoginListner = socialLoginListner;
    }

    private void facebookInit() {
        FacebookSdk.sdkInitialize(mContext.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookLogin(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        Log.e(TAG, "onCancel: " + "login cancel by user");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e(TAG, "onError: " + exception.getMessage());
                    }
                }
        );
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    private void handleFacebookLogin(LoginResult loginResult) {
        AccessToken accessToken = loginResult.getAccessToken();
        Set<String> d_premission = accessToken.getDeclinedPermissions();
        String[] d_prems = d_premission.toArray(new String[d_premission.size()]);
        if (TextUtils.join(",", d_prems).contains("email")) {
            new PremissionErroDialog(mContext, "", new PremissionErroDialog.PremissionErroDialogListner() {
                @Override
                public void clickOnok(Dialog dialog) {
                    dialog.dismiss();
                    callLoginwithRead(Arrays.asList("email"));
                }

                @Override
                public void clickOncancel(Dialog dialog) {
                    dialog.dismiss();
                }
            }).show();
        } else {
            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {

                    if (response.getError() != null) {
                        // handle error
                        FacebookRequestError facebookRequestError = response.getError();
                        String msg = facebookRequestError.getErrorMessage();
                        Log.e(TAG, "onCompleted: " + msg);
                    } else {

                        Log.e("Facebook_Values",object.optString(""));

                        SocialData socilaData = new SocialData();
                        socilaData.setId(object.optString("id"));
                        socilaData.setEmail(object.optString("email"));
                        socilaData.setFirstname(object.optString("name"));
                        socilaData.setLoginFrom("F");
                        String fb_id = object.optString("id");
                        String user_image = "http://graph.facebook.com/" + fb_id + "/picture?type=large";
                        socilaData.setUser_image(user_image);
                        if (socialLoginListner != null) {
                            socialLoginListner.SocialLoginSuccess(socilaData);
                        }
                    }
                }
            });
            Bundle parameters = new Bundle();
            parameters
                    .putString("fields", "id,name,email");
            request.setParameters(parameters);
            request.executeAsync();

        }
    }

    public void callLoginwithRead(List<String> premission) {
        if (premission != null) {
            LoginManager.getInstance().logInWithReadPermissions((Activity) mContext, premission);
        }
    }

    public void callLogout() {
        LoginManager.getInstance().logOut();
    }

    public interface SocialLoginListner {
        public void SocialLoginSuccess(SocialData socilaData);
    }
}

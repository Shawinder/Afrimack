package com.afrimack.app.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class EditTextNormal extends android.support.v7.widget.AppCompatEditText {

    public EditTextNormal(Context context) {
        super(context);
        init();
    }

    public EditTextNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextNormal(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    private void init() {
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto_regular.ttf");
        this.setTypeface(face);
    }
}

package com.afrimack.app.customviews;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by ubuntu on 8/12/16.
 */

public interface HeaderDecorationAdapter<VH extends RecyclerView.ViewHolder> {


    long getHeaderId(int position);


    VH onCreateHeaderViewHolder(ViewGroup parent);


    void onBindHeaderViewHolder(VH holder, int position);


    int getItemCount();
}

package com.afrimack.app.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewNormalBold extends TextView {


    public TextViewNormalBold(Context context) {
        super(context);
        init();
    }

    public TextViewNormalBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewNormalBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Typeface face = Typeface.createFromAsset(getContext().getAssets(), "Roboto_bold.ttf");
        this.setTypeface(face);
    }
}

package com.afrimack.app.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class ButtonNormal extends android.support.v7.widget.AppCompatButton {


    public ButtonNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }
    public ButtonNormal(Context context) {
        super(context);
        init();

    }
    public ButtonNormal(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init(){
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto_regular.ttf");
        setTypeface(face);
    }

}

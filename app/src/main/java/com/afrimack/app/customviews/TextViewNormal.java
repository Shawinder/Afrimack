package com.afrimack.app.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class TextViewNormal extends android.support.v7.widget.AppCompatTextView {


    public TextViewNormal(Context context) {
        super(context);
        init();
    }

    public TextViewNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewNormal(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Typeface face= Typeface.createFromAsset(getContext().getAssets(), "Roboto_regular.ttf");
        this.setTypeface(face);
    }
}


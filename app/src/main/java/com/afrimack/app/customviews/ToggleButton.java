package com.afrimack.app.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class ToggleButton extends android.widget.ToggleButton {


    public ToggleButton(Context context) {
        super(context);
    }

    public ToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToggleButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        Typeface face = Typeface.createFromAsset(getContext().getAssets(), "Roboto_regular.ttf");
        this.setTypeface(face);
    }


}

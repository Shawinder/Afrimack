package com.afrimack.app.rest;

public interface ApiHitListener {

    void onSuccessResponse(int apiId, Object response);

    void onFailResponse(int apiId, String error);

}

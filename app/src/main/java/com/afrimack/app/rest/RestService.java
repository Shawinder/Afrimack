package com.afrimack.app.rest;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
/*import okhttp3.logging.HttpLoggingInterceptor;*/
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestService {



    private static long CONNECTION_TIMEOUT = 100;

    private static String API_URL = "https://afrimack.com/api/";
    private static String API_URL_Map = "http://maps.googleapis.com/maps/api/";


    public static Rest getService() {
        OkHttpClient client = getOkHttpClient();
        Rest restService = new Retrofit.Builder()
                .baseUrl(API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Rest.class);
        return restService;
    }
    public static Rest getServiceMAP() {
        OkHttpClient client = getOkHttpClient();
        Rest restService = new Retrofit.Builder()
                .baseUrl(API_URL_Map)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Rest.class);
        return restService;
    }
  private static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.e("response", message);

            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okClientBuilder.addInterceptor(httpLoggingInterceptor);
        okClientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okClientBuilder.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okClientBuilder.writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        return okClientBuilder.build();
    }
}

package com.afrimack.app.rest;

public class ApiIds {


    public static final int COUNTRY = 1;
    public static final int LOGIN = 2;
    public static final int SIGNUP = 3;
    public static final int OTP = 4;
    public static final int VERIFYNUMBER = 5;
    public static final int CMS = 6;
    public static final int CURENCY = 7;
    public static final int CATEGORIES = 8;
    public static final int ADD = 9;
    public static final int SINGLEREGISTER = 10;
    public static final int FILTERCATEGORY = 11;
    public static final int ALLHOME = 12;
    public static final int SAVESEARCH = 13;
    public static final int ALERTSERCHLIST = 14;
    public static final int DELETSEARCH = 15;
    public static final int SELLING = 16;
    public static final int BUYING = 17;
    public static final int DELETSELL = 18;
    public static final int DELETBUYING = 19;
    public static final int PROFILE = 20;
    public static final int REVIEWS = 21;

    public static final int FOLLOWING = 23;
    public static final int SETTING = 24;
    public static final int UPDATESETTING = 25;
    public static final int PROFILESTATUS = 26;
    public static final int PROFILEUPDATE = 27;
    public static final int POSTDETAIL = 28;
    public static final int USERFOLLOW = 29;
    public static final int USERUNFOLLOW = 30;
    public static final int FOLLOWUNFILLOW = 31;
    public static final int HEARTFOLLOW = 32;
    public static final int QUESTION = 33;
    public static final int USERPROFILE = 34;
    public static final int OFFERDETAIL = 35;
    public static final int PLACEOFFER = 36;
    public static final int ACCEPTOFFER = 37;
    public static final int CEANCELOFFER = 39;
    public static final int CONFORMOFFER = 40;
    public static final int POSTCHAT = 41;
    public static final int BLOCKUSER = 43;
    public static final int RAEINGREVIEW = 42;
    public static final int SINGLEFILTER = 44;
    public static final int SINGLPROFILE = 45;
    public static final int DISCOVER = 46;
    public static final int NOTIFICATIONALL = 47;
    public static final int READNOTIFICATION = 48;
    public static final int SOCAILLOGIN = 49;
    public static final int FORGOTPASSWORD = 50;
    public static final int LOGOUT = 51;
    public static final int SINGLEUSERPROFILE = 52;
    public static final int SWAPDEATEL = 53;
    public static final int SWAPOFFER = 54;
    public static final int SWAPCONFROSM = 55;
    public static final int SWAPCONFROSMDONE = 56;
    public static final int SWAPCANCEL = 57;
    public static final int SWAPREMIND = 58;
    public static final int OFFERPREMIND = 59;
    public static final int POSTIMAGE = 60;
    public static final int DELETESINGLEPROFILE = 61;
    public static final int DELETEQUTION = 62;
    public static final int FEEDBACK = 63;
    public static final int MAPNAME = 64;
    public static final int RESETPASSWORD = 65;
    public static final int POSTREPORT = 66;
    public static final int PREMIUMFEATURE = 67;
    public static final int GROUPID = 68;
    public static final int RESENFMAIL = 69;
    public static final int GETCHAT = 70;
    public static final int POSTEDIT = 71;
    public static final int BLOG = 72;
    public static final int BLOGINFO = 73;
    public static final int BLOCKUSERLIST = 74;
    public static final int UNBLOCKUSER = 75;
    public static final int SEARCHPAYMENT = 76;
    public static final int POSTHPAYMENT = 77;








}

package com.afrimack.app.rest;


import android.content.Context;

import com.afrimack.app.models.AddResponseModel;
import com.afrimack.app.models.AlertSearchResponseModel;
import com.afrimack.app.models.BlockUserModel;
import com.afrimack.app.models.BlogInfoModel;
import com.afrimack.app.models.BlogModel;
import com.afrimack.app.models.BuyingResponseModel;
import com.afrimack.app.models.CategoriesResponseModel;
import com.afrimack.app.models.ChatHistoryModel;
import com.afrimack.app.models.ChatResponseModel;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.CountryListMode;
import com.afrimack.app.models.CurencyNameResponseModel;
import com.afrimack.app.models.DeletPostResponseModel;
import com.afrimack.app.models.DiscoverResponseModel;
import com.afrimack.app.models.FilterResponseModel;
import com.afrimack.app.models.FollowingResponseModel;
import com.afrimack.app.models.GetSettingResponseModel;
import com.afrimack.app.models.GroupIdResponseModel;
import com.afrimack.app.models.HomeResponseModel;
import com.afrimack.app.models.LoginResponseModel;
import com.afrimack.app.models.MApNameModel;
import com.afrimack.app.models.NotificationAllResponseModel;
import com.afrimack.app.models.OtherUserResponseModel;
import com.afrimack.app.models.OtpResponseModel;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.models.PostEditModel;
import com.afrimack.app.models.PostImageResponseModel;
import com.afrimack.app.models.PremiumResponseModel;
import com.afrimack.app.models.ProfileResponseModel;
import com.afrimack.app.models.ProfileStatusResponseModel;
import com.afrimack.app.models.ReviewsResponseModel;
import com.afrimack.app.models.SellingResponseModel;
import com.afrimack.app.models.SearchSaveResponseModel;
import com.afrimack.app.models.SignupResponseModel;
import com.afrimack.app.models.SingleProfileModel;
import com.afrimack.app.models.SingleProfileResponseModel;
import com.afrimack.app.models.SingleResponseModel;
import com.afrimack.app.models.SingleUserResponseModel;
import com.afrimack.app.models.SocilResponseModel;
import com.afrimack.app.models.SwapResponseModel;
import com.afrimack.app.models.TCPResponseModel;
import com.afrimack.app.models.VerifyNumberResponseModel;

import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class RestClient extends BaseRestClient{


   ApiHitListener apiHitListener;
    private Rest api;
    private Object object;

    public RestClient(Context _context) {
        super(_context);
    }

    public RestClient callback(ApiHitListener apiHitListener) {
        this.apiHitListener = apiHitListener;
        return this;
    }

    private Rest getApi() {
        if (api == null) {
            api = RestService.getService();
        }

        return api;
    }
    private Rest getApiMap() {
        if (api == null) {
            api = RestService.getServiceMAP();
        }

        return api;
    }

    public void PostProcessCountry() {
        Call<CountryListMode> call = getApi().PostProcessCountry();

        call.enqueue(new Callback<CountryListMode>() {
            @Override
            public void onResponse(Call<CountryListMode> call, Response<CountryListMode> response) {
                apiHitListener.onSuccessResponse(ApiIds.COUNTRY, response.body());
            }

            @Override
            public void onFailure(Call<CountryListMode> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.COUNTRY, t.getMessage());

            }
        });
    }





    public void PostOtp(String user_id, String country_id, String phone_number) {

        Call<OtpResponseModel> call = getApi().Otp(user_id, country_id, phone_number);
        call.enqueue(new Callback<OtpResponseModel>() {
            @Override
            public void onResponse(Call<OtpResponseModel> call, Response<OtpResponseModel> response) {
                apiHitListener.onSuccessResponse(ApiIds.OTP, response.body());
            }

            @Override
            public void onFailure(Call<OtpResponseModel> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.OTP, t.getMessage());

            }
        });
    }

    public void PostVerifyNumber(String userId, String otp) {
        Call<VerifyNumberResponseModel> call = getApi().VerifyNumber(userId, otp);
        call.enqueue(new Callback<VerifyNumberResponseModel>() {
            @Override
            public void onResponse(Call<VerifyNumberResponseModel> call, Response<VerifyNumberResponseModel> response) {
                apiHitListener.onSuccessResponse(ApiIds.VERIFYNUMBER, response.body());
            }

            @Override
            public void onFailure(Call<VerifyNumberResponseModel> call, Throwable t) {
                apiHitListener.onFailResponse(ApiIds.VERIFYNUMBER, t.getMessage());

            }
        });
    }
}

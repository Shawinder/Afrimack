package com.afrimack.app.rest;


import com.afrimack.app.models.AddResponseModel;
import com.afrimack.app.models.AlertSearchResponseModel;
import com.afrimack.app.models.BlockUserModel;
import com.afrimack.app.models.BlogInfoModel;
import com.afrimack.app.models.BlogModel;
import com.afrimack.app.models.BuyingResponseModel;
import com.afrimack.app.models.CategoriesResponseModel;
import com.afrimack.app.models.ChatHistoryModel;
import com.afrimack.app.models.ChatResponseModel;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.CountryListMode;
import com.afrimack.app.models.CurencyNameResponseModel;
import com.afrimack.app.models.DeletPostResponseModel;
import com.afrimack.app.models.DiscoverResponseModel;
import com.afrimack.app.models.FilterResponseModel;
import com.afrimack.app.models.FollowingResponseModel;
import com.afrimack.app.models.GetSettingResponseModel;
import com.afrimack.app.models.GroupIdResponseModel;
import com.afrimack.app.models.HomeResponseModel;
import com.afrimack.app.models.LoginResponseModel;
import com.afrimack.app.models.MApNameModel;
import com.afrimack.app.models.NotificationAllResponseModel;
import com.afrimack.app.models.OtherUserResponseModel;
import com.afrimack.app.models.OtpResponseModel;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.models.PostEditModel;
import com.afrimack.app.models.PostImageResponseModel;
import com.afrimack.app.models.PremiumResponseModel;
import com.afrimack.app.models.ProfileResponseModel;
import com.afrimack.app.models.ProfileStatusResponseModel;
import com.afrimack.app.models.ReviewsResponseModel;
import com.afrimack.app.models.SellingResponseModel;
import com.afrimack.app.models.SearchSaveResponseModel;
import com.afrimack.app.models.SignupResponseModel;
import com.afrimack.app.models.SingleProfileModel;
import com.afrimack.app.models.SingleProfileResponseModel;
import com.afrimack.app.models.SingleResponseModel;
import com.afrimack.app.models.SingleUserResponseModel;
import com.afrimack.app.models.SocilResponseModel;
import com.afrimack.app.models.SwapResponseModel;
import com.afrimack.app.models.TCPResponseModel;
import com.afrimack.app.models.VerifyNumberResponseModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface Rest {


    @POST("users/country.json")
    Call<CountryListMode> PostProcessCountry();


    @FormUrlEncoded
    @POST("users/login.json")
    Call<LoginResponseModel> Login(@Field("device_token") String device_token,
                                   @Field("username") String email,
                                   @Field("password") String password);


    @FormUrlEncoded
    @POST("users/otp.json")
    Call<OtpResponseModel> Otp(@Field("user_id") String user_id,
                               @Field("country_id") String country_id,
                               @Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("users/verify_number.json")
    Call<VerifyNumberResponseModel> VerifyNumber(@Field("user_id") String userId,
                                                 @Field("otp") String otp);


    @FormUrlEncoded
    @POST("users/feedback.json")
    Call<CommanResponseModel> feedback(@Field("name") String f_name,
                                       @Field("email") String f_email,
                                       @Field("subject") String f_subject,
                                       @Field("message") String f_message);

}

package com.afrimack.app.rest;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;


/**
 * Created by ububtu on 13/7/16.
 */
public class BaseRestClient {
    private final Context _context;
    private ProgressDialog pDialog;
    private String pDialogMessage = "Loading...";

    private AlertDialog mErrorDialog;

    protected BaseRestClient(Context _context) {
        this._context = _context;

        init();

    }

    private void init() {
        pDialog = new ProgressDialog(_context);
        pDialog.setMessage(pDialogMessage);
        pDialog.setCancelable(false);
    }


    public void showProgDialog() {
        pDialog.show();
    }

    public void showProgDialog(String message) {

        pDialog.setMessage(message);
        pDialog.show();
    }

    public void hideProgDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    public void showToast(String msg) {
        Toast.makeText(_context, msg, Toast.LENGTH_SHORT).show();
    }





}

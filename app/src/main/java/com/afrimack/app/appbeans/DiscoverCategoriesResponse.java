package com.afrimack.app.appbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ayush on 4/6/2018.
 */

public class DiscoverCategoriesResponse {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("response")
    @Expose
    private List<Response> response = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Response> getResponse() {
        return response;
    }

    public void setResponse(List<Response> response) {
        this.response = response;
    }


    public class Response {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("icon_logo")
        @Expose
        private String iconLogo;
        @SerializedName("cat_logo_app3")
        @Expose
        private String catLogoApp3;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("single_registered")
        @Expose
        private String singleRegistered;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getIconLogo() {
            return iconLogo;
        }

        public void setIconLogo(String iconLogo) {
            this.iconLogo = iconLogo;
        }

        public String getCatLogoApp3() {
            return catLogoApp3;
        }

        public void setCatLogoApp3(String catLogoApp3) {
            this.catLogoApp3 = catLogoApp3;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getSingleRegistered() {
            return singleRegistered;
        }

        public void setSingleRegistered(String singleRegistered) {
            this.singleRegistered = singleRegistered;
        }

    }


}

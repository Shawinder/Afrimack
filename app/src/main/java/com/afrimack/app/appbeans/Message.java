package com.afrimack.app.appbeans;

import java.io.Serializable;

/**
 * Created by root on 4/12/17.
 */

public class Message implements Serializable {

    public String idSender;
    public String idReceiver;
    public String text;
    public long timestamp;
    public String isRead;
    public String isImage;
    public String photoUrl;
}

package com.afrimack.app.appbeans;

/**
 * Created by Ayush on 4/6/2018.
 */

public class SubCategoryBean {


    private Integer id;
    private String category_name;
    private String cat_logo_app3;

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCat_logo_app3() {
        return cat_logo_app3;
    }

    public void setCat_logo_app3(String cat_logo_app3) {
        this.cat_logo_app3 = cat_logo_app3;
    }

    public String getIcon_logo() {
        return icon_logo;
    }

    public void setIcon_logo(String icon_logo) {
        this.icon_logo = icon_logo;
    }

    private String icon_logo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



}

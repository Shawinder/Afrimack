package com.afrimack.app.appbeans;

import java.io.Serializable;

/**
 * Created by root on 6/11/17.
 */

public class SpecialBean implements Serializable {

    private int id;
    private String category_name;
    private String cat_logo_app3;
    private String icon_logo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCat_logo_app3() {
        return cat_logo_app3;
    }

    public void setCat_logo_app3(String cat_logo_app3) {
        this.cat_logo_app3 = cat_logo_app3;
    }

    public String getIcon_logo() {
        return icon_logo;
    }

    public void setIcon_logo(String icon_logo) {
        this.icon_logo = icon_logo;
    }
}

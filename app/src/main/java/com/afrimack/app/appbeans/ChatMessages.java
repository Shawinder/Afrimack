package com.afrimack.app.appbeans;

import java.io.Serializable;

/**
 * Created by root on 4/12/17.
 */

public class ChatMessages implements Serializable {

    private String messageTo;
    private String messageFrom;
    private String senderId;
    private String receiverId;
    private Boolean isOnLine;
    private String receiverStatus;
    private Boolean isTyping;
    private long timeStamp;
    private Boolean ismessageRead;

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Boolean getIsmessageRead() {
        return ismessageRead;
    }

    public void setIsmessageRead(Boolean ismessageRead) {
        this.ismessageRead = ismessageRead;
    }

    public String getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(String messageTo) {
        this.messageTo = messageTo;
    }

    public String getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public Boolean getOnLine() {
        return isOnLine;
    }

    public void setOnLine(Boolean onLine) {
        isOnLine = onLine;
    }

    public String getReceiverStatus() {
        return receiverStatus;
    }

    public void setReceiverStatus(String receiverStatus) {
        this.receiverStatus = receiverStatus;
    }

    public Boolean getTyping() {
        return isTyping;
    }

    public void setTyping(Boolean typing) {
        isTyping = typing;
    }
}

package com.afrimack.app.appbeans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by root on 6/11/17.
 */

public class ResponseBean implements Serializable {
    private List<SpecialBean> special;

    public List<CategoryBean> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryBean> category) {
        this.category = category;
    }

    private List<CategoryBean> category ;
    private List<PartnerBean> partner;
    private List<Interest> interests = null;

    public List<SpecialBean> getSpecial() {
        return special;
    }

    public void setSpecial(List<SpecialBean> special) {
        this.special = special;
    }



    public List<PartnerBean> getPartner() {
        return partner;
    }

    public void setPartner(List<PartnerBean> partner) {
        this.partner = partner;
    }


    public List<Interest> getInterests() {
        return interests;
    }

    public void setInterests(List<Interest> interests) {
        this.interests = interests;
    }
}

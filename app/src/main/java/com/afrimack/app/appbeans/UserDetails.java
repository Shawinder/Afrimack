
package com.afrimack.app.appbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserDetails implements Serializable {

    @SerializedName("deviceToken")
    @Expose
    private String deviceToken;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("online")
    @Expose
    private String online;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("picture")
    @Expose
    private String picture;

    @SerializedName("php_user_id")
    @Expose
    private int user_id;

    @SerializedName("php_block_status")
    @Expose
    private int blockedStatus;

    @SerializedName("php_block_by")
    @Expose
    private int php_block_by;

    @SerializedName("php_block_id")
    @Expose
    private int php_block_id;

    public int getPhp_block_id() {
        return php_block_id;
    }

    public void setPhp_block_id(int php_block_id) {
        this.php_block_id = php_block_id;
    }

    public int getPhp_block_by() {
        return php_block_by;
    }

    public void setPhp_block_by(int php_block_by) {
        this.php_block_by = php_block_by;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getBlockedStatus() {
        return blockedStatus;
    }

    public void setBlockedStatus(int blockedStatus) {
        this.blockedStatus = blockedStatus;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}

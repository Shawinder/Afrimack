package com.afrimack.app.appbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by root on 12/3/18.
 */

public class Interest implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("cat_logo_app3")
    @Expose
    private Object catLogoApp3;
    @SerializedName("icon_logo")
    @Expose
    private String iconLogo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Object getCatLogoApp3() {
        return catLogoApp3;
    }

    public void setCatLogoApp3(Object catLogoApp3) {
        this.catLogoApp3 = catLogoApp3;
    }

    public String getIconLogo() {
        return iconLogo;
    }

    public void setIconLogo(String iconLogo) {
        this.iconLogo = iconLogo;
    }
}

package com.afrimack.app.appbeans;

import java.io.Serializable;

/**
 * Created by root on 20/12/17.
 */

public class BlockedUser implements Serializable {
    public String blockedBy;
    public String blockedStatus;
    public String blockedUser;

    public String getBlockedBy() {
        return blockedBy;
    }

    public void setBlockedBy(String blockedBy) {
        this.blockedBy = blockedBy;
    }

    public String getBlockedStatus() {
        return blockedStatus;
    }

    public void setBlockedStatus(String blockedStatus) {
        this.blockedStatus = blockedStatus;
    }

    public String getBlockedUser() {
        return blockedUser;
    }

    public void setBlockedUser(String blockedUser) {
        this.blockedUser = blockedUser;
    }
}

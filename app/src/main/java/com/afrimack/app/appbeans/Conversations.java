package com.afrimack.app.appbeans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by root on 4/12/17.
 */

public class Conversations implements Serializable {

    private ArrayList<Message> listMessageData;

    public Conversations() {
        listMessageData = new ArrayList<>();
    }

    public ArrayList<Message> getListMessageData() {
        return listMessageData;
    }

}

package com.afrimack.app.appbeans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by root on 12/1/18.
 */

public class SettingBean implements Serializable {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("response")
    @Expose
    private Response response;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public class Response {

        @SerializedName("push")
        @Expose
        private Push push;
        @SerializedName("email")
        @Expose
        private Email email;

        public Push getPush() {
            return push;
        }

        public void setPush(Push push) {
            this.push = push;
        }

        public Email getEmail() {
            return email;
        }

        public void setEmail(Email email) {
            this.email = email;
        }

    }


    public class Push {

        @SerializedName("ads_online")
        @Expose
        private String adsOnline;
        @SerializedName("new_question")
        @Expose
        private String newQuestion;
        @SerializedName("new_offer")
        @Expose
        private String newOffer;
        @SerializedName("new_ads_from_following")
        @Expose
        private String newAdsFromFollowing;
        @SerializedName("new_ads_from_facebook_friend")
        @Expose
        private String newAdsFromFacebookFriend;
        @SerializedName("single_profile_online")
        @Expose
        private String singleProfileOnline;
        @SerializedName("new_message_chat")
        @Expose
        private String newMessageChat;
        @SerializedName("new_message_afrimack")
        @Expose
        private String newMessageAfrimack;
        @SerializedName("accept_offer")
        @Expose
        private String acceptOffer;
        @SerializedName("confirm")
        @Expose
        private String confirm;
        @SerializedName("remind_confirm")
        @Expose
        private String remindConfirm;

        public String getAdsOnline() {
            return adsOnline;
        }

        public void setAdsOnline(String adsOnline) {
            this.adsOnline = adsOnline;
        }

        public String getNewQuestion() {
            return newQuestion;
        }

        public void setNewQuestion(String newQuestion) {
            this.newQuestion = newQuestion;
        }

        public String getNewOffer() {
            return newOffer;
        }

        public void setNewOffer(String newOffer) {
            this.newOffer = newOffer;
        }

        public String getNewAdsFromFollowing() {
            return newAdsFromFollowing;
        }

        public void setNewAdsFromFollowing(String newAdsFromFollowing) {
            this.newAdsFromFollowing = newAdsFromFollowing;
        }

        public String getNewAdsFromFacebookFriend() {
            return newAdsFromFacebookFriend;
        }

        public void setNewAdsFromFacebookFriend(String newAdsFromFacebookFriend) {
            this.newAdsFromFacebookFriend = newAdsFromFacebookFriend;
        }

        public String getSingleProfileOnline() {
            return singleProfileOnline;
        }

        public void setSingleProfileOnline(String singleProfileOnline) {
            this.singleProfileOnline = singleProfileOnline;
        }

        public String getNewMessageChat() {
            return newMessageChat;
        }

        public void setNewMessageChat(String newMessageChat) {
            this.newMessageChat = newMessageChat;
        }

        public String getNewMessageAfrimack() {
            return newMessageAfrimack;
        }

        public void setNewMessageAfrimack(String newMessageAfrimack) {
            this.newMessageAfrimack = newMessageAfrimack;
        }

        public String getAcceptOffer() {
            return acceptOffer;
        }

        public void setAcceptOffer(String acceptOffer) {
            this.acceptOffer = acceptOffer;
        }

        public String getConfirm() {
            return confirm;
        }

        public void setConfirm(String confirm) {
            this.confirm = confirm;
        }

        public String getRemindConfirm() {
            return remindConfirm;
        }

        public void setRemindConfirm(String remindConfirm) {
            this.remindConfirm = remindConfirm;
        }


    }

    public class Email {

        @SerializedName("ads_online")
        @Expose
        private String adsOnline;
        @SerializedName("new_question")
        @Expose
        private String newQuestion;
        @SerializedName("new_offer")
        @Expose
        private String newOffer;
        @SerializedName("new_ads_from_following")
        @Expose
        private String newAdsFromFollowing;
        @SerializedName("new_ads_from_facebook_friend")
        @Expose
        private String newAdsFromFacebookFriend;
        @SerializedName("single_profile_online")
        @Expose
        private String singleProfileOnline;
        @SerializedName("new_message_chat")
        @Expose
        private String newMessageChat;
        @SerializedName("new_message_afrimack")
        @Expose
        private String newMessageAfrimack;
        @SerializedName("accept_offer")
        @Expose
        private String acceptOffer;
        @SerializedName("confirm")
        @Expose
        private String confirm;
        @SerializedName("remind_confirm")
        @Expose
        private String remindConfirm;

        public String getAdsOnline() {
            return adsOnline;
        }

        public void setAdsOnline(String adsOnline) {
            this.adsOnline = adsOnline;
        }

        public String getNewQuestion() {
            return newQuestion;
        }

        public void setNewQuestion(String newQuestion) {
            this.newQuestion = newQuestion;
        }

        public String getNewOffer() {
            return newOffer;
        }

        public void setNewOffer(String newOffer) {
            this.newOffer = newOffer;
        }

        public String getNewAdsFromFollowing() {
            return newAdsFromFollowing;
        }

        public void setNewAdsFromFollowing(String newAdsFromFollowing) {
            this.newAdsFromFollowing = newAdsFromFollowing;
        }

        public String getNewAdsFromFacebookFriend() {
            return newAdsFromFacebookFriend;
        }

        public void setNewAdsFromFacebookFriend(String newAdsFromFacebookFriend) {
            this.newAdsFromFacebookFriend = newAdsFromFacebookFriend;
        }

        public String getSingleProfileOnline() {
            return singleProfileOnline;
        }

        public void setSingleProfileOnline(String singleProfileOnline) {
            this.singleProfileOnline = singleProfileOnline;
        }

        public String getNewMessageChat() {
            return newMessageChat;
        }

        public void setNewMessageChat(String newMessageChat) {
            this.newMessageChat = newMessageChat;
        }

        public String getNewMessageAfrimack() {
            return newMessageAfrimack;
        }

        public void setNewMessageAfrimack(String newMessageAfrimack) {
            this.newMessageAfrimack = newMessageAfrimack;
        }

        public String getAcceptOffer() {
            return acceptOffer;
        }

        public void setAcceptOffer(String acceptOffer) {
            this.acceptOffer = acceptOffer;
        }

        public String getConfirm() {
            return confirm;
        }

        public void setConfirm(String confirm) {
            this.confirm = confirm;
        }

        public String getRemindConfirm() {
            return remindConfirm;
        }

        public void setRemindConfirm(String remindConfirm) {
            this.remindConfirm = remindConfirm;
        }

    }

}

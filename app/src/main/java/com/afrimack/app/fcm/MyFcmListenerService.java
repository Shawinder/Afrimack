package com.afrimack.app.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActMyAfrimack;
import com.afrimack.app.activities.ActivityChat;
import com.afrimack.app.activities.ActivityChattingOverView;
import com.afrimack.app.activities.ActivityDiscoverDetail;
import com.afrimack.app.activities.SingleChatActivity;
import com.afrimack.app.activities.SingleProfileViewActivity;
import com.afrimack.app.activities.SwopDeal;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.Notification_model;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.Map;

import static com.afrimack.app.AfrimackApp.curentactivity;
import static com.brsoftech.core_utils.utils.StandardUtil.runOnUiThread;



public class MyFcmListenerService extends FirebaseMessagingService {

    public String kPushType = "action";
    ActivityChat activityChat;
    ActivityDiscoverDetail discoverDetail;
    SingleProfileViewActivity singleProfileViewActivity;
    SingleChatActivity singleChatActivity;
    SwopDeal swopDeal;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Log.e("remote Message ", remoteMessage.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (remoteMessage.getData().size() > 0) {
            if (remoteMessage.getData().get("title").equalsIgnoreCase("Afrimack Message")) {
                if (Config.getChatMessageStatus(this) == 1) {
                    if (curentactivity instanceof SingleChatActivity) {
                    } else {
                        handleChatNotification(this, remoteMessage.getData());
                    }
                }


            } else {

                Gson gson = new Gson();
                String str = gson.toJson(remoteMessage.getData());

                Notification_model notification_model = gson.fromJson(str, Notification_model.class);

                generateNotification1(this, notification_model);
            }
        }
    }

    private void handleChatNotification(Context context, Map<String, String> data) {
        int icon = R.mipmap.afrimack_icon_newspage;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            int importance = NotificationManager.IMPORTANCE_HIGH;
            String CHANNEL_ID = "my_channel_01";
            CharSequence name = "MY Channel";// The user-visible name of the channel.

            NotificationManager notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);


            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            if (notificationManager != null) notificationManager.createNotificationChannel(mChannel);
            Intent notificationIntent = null;
            notificationIntent = new Intent(context, ActivityChattingOverView.class);
            notificationIntent.putExtra("from", "from_chat");

            PendingIntent intent = PendingIntent.getActivity(context, 0,
                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    context, CHANNEL_ID)
                    .setSmallIcon(icon)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.afrimack_icon_newspage))
                    .setContentTitle("afrimack")
                    .setContentText("You have a new chat message.")
                    .setAutoCancel(true)
                    .setDefaults(
                            Notification.DEFAULT_SOUND
                                    | Notification.DEFAULT_VIBRATE)
                    .setContentText("You have a new chat message.")
                    .setChannelId(CHANNEL_ID);

            mBuilder.setContentIntent(intent);
            notificationManager.notify(0, mBuilder.build());
        }
        else{  NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent = null;
            notificationIntent = new Intent(context, ActivityChattingOverView.class);
            notificationIntent.putExtra("from", "from_chat");

            PendingIntent intent = PendingIntent.getActivity(context, 0,
                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    context)
                    .setSmallIcon(icon)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.afrimack_icon_newspage))
                    .setContentTitle("afrimack")
                    .setContentText("You have a new chat message.")
                    .setAutoCancel(true)
                    .setDefaults(
                            Notification.DEFAULT_SOUND
                                    | Notification.DEFAULT_VIBRATE)
                    .setContentText("You have a new chat message.");

            mBuilder.setContentIntent(intent);
            notificationManager.notify(0, mBuilder.build());


        }


    }


    public void generateNotification1(final Context context, final Notification_model pojoResponse) {
        if (curentactivity != null && curentactivity instanceof ActivityChat) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activityChat = (ActivityChat) curentactivity;
                    activityChat.chatnotification();
                }
            });

        } else if (curentactivity != null && curentactivity instanceof ActivityDiscoverDetail) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    discoverDetail = (ActivityDiscoverDetail) curentactivity;
                    discoverDetail.chatnotification();
                }
            });

        } else if (curentactivity != null && curentactivity instanceof SwopDeal) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    swopDeal = (SwopDeal) curentactivity;
                    swopDeal.chatnotification();
                }
            });
        } else if (pojoResponse.getMessage().equalsIgnoreCase("You have been blocked.")) {

            if (curentactivity instanceof SingleChatActivity) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        singleChatActivity = (SingleChatActivity) curentactivity;
                        singleChatActivity.chatnotification(1);
                    }
                });

            }
        } else if (pojoResponse.getMessage().equalsIgnoreCase("You have been un-blocked.")) {
            if (curentactivity instanceof SingleChatActivity) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        singleChatActivity = (SingleChatActivity) curentactivity;
                        singleChatActivity.chatnotification(0);
                    }
                });

            }

        } else if (curentactivity != null && curentactivity instanceof SingleProfileViewActivity) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // if (pojoResponse.getMessage().equalsIgnoreCase("Dating profile is online now. Find your love in category dating!")) {
                    if (pojoResponse.getMessage().equalsIgnoreCase("Friend profile is online now. In category Find new Friends you find people with same interests")) {
                        singleProfileViewActivity = (SingleProfileViewActivity) curentactivity;
                        Config.setSingleRegister("Y", context);
                        singleProfileViewActivity.notifyData();
                    } else {
                        singleProfileViewActivity = (SingleProfileViewActivity) curentactivity;
                        singleProfileViewActivity.notifyData();
                    }
                }
            });
        } else {
            int icon = R.mipmap.afrimack_icon_newspage;
            // if (pojoResponse.getMessage().equalsIgnoreCase("Dating profile is online now. Find your love in category dating!")) {
            if (pojoResponse.getMessage().equalsIgnoreCase("Friend profile is online now. In category Find new Friends you find people with same interests")) {
                Config.setSingleRegister("Y", context);
            }


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

                int importance = NotificationManager.IMPORTANCE_HIGH;
                String CHANNEL_ID = "my_channel_01";
                CharSequence name = "MY Channel";// The user-visible name of the channel.


                NotificationManager notificationManager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);


                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                if (notificationManager != null) notificationManager.createNotificationChannel(mChannel);

                String title = pojoResponse.getTitle();
                String message = pojoResponse.getMessage();
                Intent notificationIntent = null;
                notificationIntent = new Intent(context, ActMyAfrimack.class);
                notificationIntent.putExtra("massagebody", pojoResponse.getMessage());
                notificationIntent.putExtra("from", "from_notifications");

                PendingIntent intent = PendingIntent.getActivity(context, 0,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        context, CHANNEL_ID)
                        .setSmallIcon(icon)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.afrimack_icon_newspage))
                        .setContentTitle("afrimack")
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setDefaults(
                                Notification.DEFAULT_SOUND
                                        | Notification.DEFAULT_VIBRATE)
                        .setContentText(pojoResponse.getMessage())
                        .setChannelId(CHANNEL_ID);

                mBuilder.setContentIntent(intent);
                notificationManager.notify(0, mBuilder.build());
            }

            else {

                NotificationManager notificationManager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);

                String title = pojoResponse.getTitle();
                String message = pojoResponse.getMessage();
                Intent notificationIntent = null;
                notificationIntent = new Intent(context, ActMyAfrimack.class);
                notificationIntent.putExtra("massagebody", pojoResponse.getMessage());
                notificationIntent.putExtra("from", "from_notifications");

                PendingIntent intent = PendingIntent.getActivity(context, 0,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                        context)
                        .setSmallIcon(icon)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.afrimack_icon_newspage))
                        .setContentTitle("afrimack")
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setDefaults(
                                Notification.DEFAULT_SOUND
                                        | Notification.DEFAULT_VIBRATE)
                        .setContentText(pojoResponse.getMessage());

                mBuilder.setContentIntent(intent);
                notificationManager.notify(0, mBuilder.build());
            }
        }
    }
}



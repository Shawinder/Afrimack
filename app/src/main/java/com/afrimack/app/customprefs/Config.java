package com.afrimack.app.customprefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customchat.Constants;


/**
 * Created by ubuntu on 12/10/16.
 */

public class Config {

    public static final String CONFIG_FILE = "Afrimack";
    private static SharedPreferences preference = null;

    private static SharedPreferences.Editor editor;
    private static Context mContext;

    public Config(Context mContext) {
        preference = mContext.getSharedPreferences(CONFIG_FILE, Context.MODE_PRIVATE);
        editor = preference.edit();
    }

    public static void init(Context mContext) {
        Config.mContext = mContext;
        Config.preference = mContext.getSharedPreferences(CONFIG_FILE, Context.MODE_PRIVATE);
        Config.editor = preference.edit();
    }


    public static void removeOrClearPerferance(String key) {
        if (key != null && key.trim().length() != 0)
            editor.remove(key);
        else {
            editor.clear();
        }
        editor.commit();
    }

    public static void savePreferencese() {
        if (editor != null) {
            editor.commit();
        }

    }


    public static void setDeviceID(String deviceid, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.DEVICEID, deviceid);
        editor.commit();
    }


    public static String getDeviceID(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        return preferences.getString(ConstantMain.DEVICEID, "");
    }

    /*Device Token*/
    public static void setDeviceToken(String devicetoken, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.DEVICETOKEN, devicetoken);
        editor.commit();
    }

    public static String getDeviceToken(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.DEVICETOKEN, "");
    }


    /*ser Id*/
    public static void setUserId(String userid, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.USERID, userid);
        editor.commit();
    }

    public static String getUserId(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.USERID, "");
    }

    public static void setLoginProfileImg(String user_iamge, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.USERIMAGE, user_iamge);
        editor.commit();
    }

    public static String getLoginProfileImg(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.USERIMAGE, "");
    }

    public static void setLoginProfileType(String user_type, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.USERTYPE, user_type);
        editor.commit();
    }

    public static String getLoginProfileType(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.USERTYPE, "");
    }

    public static void setProfileComplet(String complet_profile, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.COMPLETPROFILE, complet_profile);
        editor.commit();
    }

    public static String getProfileComplet(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.COMPLETPROFILE, "");
    }

    public static void setFollowTotel(String followtotel, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.FOLLOWTOTEL, followtotel);
        editor.commit();
    }

    public static String getFollowTotel(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.FOLLOWTOTEL, "");
    }

    public static void setBlockTotel(String block_totel, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.BLOCKTOTEL, block_totel);
        editor.commit();
    }

    public static String getBlockTotel(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.BLOCKTOTEL, "");
    }
    /*Otp*/

    public static void setOtp(String otpNo, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.OPTNO, otpNo);
        editor.commit();
    }

    public static String getOtp(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.OPTNO, "");
    }

    /*Phone number*/
    public static void setPhoneNo(String phoneno, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.PHONENO, phoneno);
        editor.commit();
    }

    public static String getPhoneNo(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.PHONENO, "");
    }

    /*add sell profile */
    public static void setSellImage(String sellimage, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SELLIMAGE, sellimage);
        editor.commit();
    }

    public static String getSellImage(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SELLIMAGE, "");
    }


    /*let log */

    public static void setlatitude(String latitude, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.LATITUDE, latitude);
        editor.commit();
    }

    public static String getlatitude(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.LATITUDE, "");
    }

    public static void setlongitude(String longitude, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.LOGUTUDE, longitude);
        editor.commit();
    }

    public static String getlongitude(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.LOGUTUDE, "");
    }


    /*ser Id*/
    public static void setSingleRegister(String single_registered, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SingleRegister, single_registered);
        editor.commit();
    }

    public static String getSingleRegister(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SingleRegister, "");
    }


    public static void setSortBy(String sorting, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SORTING, sorting);
        editor.commit();
    }

    public static String getSortBy(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SORTING, "");
    }

    public static void setSortDay(String last_days, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.LASTDAYS, last_days);
        editor.commit();
    }

    public static String getSortDay(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.LASTDAYS, "");
    }

    public static void setSortRadius(String radius, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.RADIUS, radius);
        editor.commit();
    }

    public static String getSortRadius(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.RADIUS, "");
    }

    public static void setSortOwnCountry(String own_country, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.OWNCOUNTRY, own_country);
        editor.commit();
    }

    public static String getSortOwnCountry(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.OWNCOUNTRY, "");
    }

    public static void setSortCountry(String country, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.COUNTRYSORT, country);
        editor.commit();
    }

    public static String getSortCountry(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.COUNTRYSORT, "");
    }

    public static void setSortSpecialCategory(String special_category, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SPECIALCATEGORY, special_category);
        editor.commit();
    }

    public static String getSortSpecialCategory(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SPECIALCATEGORY, "");
    }

    public static void setSortCategory(String category, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SORTCATEGORY, category);
        editor.commit();

    }

    public static String getSortCategory(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SORTCATEGORY, "");
    }
 public static void setSortSubCategory(String category, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SORTSUBCATEGORY, category);
        editor.commit();

    }

    public static String getSortSubCategory(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SORTSUBCATEGORY, "");
    }

    public static void setSortMinPrice(String min_price, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.MINPRICE, min_price);
        editor.commit();
    }

    public static String getSortMinPrice(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.MINPRICE, "");
    }

    public static void setSortMaxPrice(String max_price, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.MAXPRICE, max_price);
        editor.commit();
    }

    public static String getSortMaxPrice(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.MAXPRICE, "");
    }

    public static void setBlogId(String blog_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.BLOGID, blog_id);
        editor.commit();
    }

    public static String getBlogId(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.BLOGID, "");
    }

    public static void setSearch(String search, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SEARCH, search);
        editor.commit();
    }

    public static String getSearch(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SEARCH, "");
    }

    public static void setSKip(String skip, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SKIP, skip);
        editor.commit();
    }

    public static String getSkip(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SKIP, "");
    }

    public static void setLoginBack(String loginback, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.LOGINBACk, loginback);
        editor.commit();
    }

    public static String getLoginBack(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.LOGINBACk, "");
    }

    public static void setFilter(String filter_type, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.FILTERTEPE, filter_type);
        editor.commit();
    }

    public static String getFilter(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.FILTERTEPE, "");
    }

    public static void setPostId(String post_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.POSTID, post_id);
        editor.commit();
    }

    public static String getPostId(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.POSTID, "");
    }

    public static void setOfferUserID(String offer_user_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.OFFERUSERID, offer_user_id);
        editor.commit();
    }

    public static String getOfferUserID(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.OFFERUSERID, "");
    }

    public static void setSingleChatId(String single_user_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.SINGLECAHTID, single_user_id);
        editor.commit();
    }

    public static String getSingleChatId(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.SINGLECAHTID, "");
    }

    public static void setSingleGroupId(String group_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.GROUPID, group_id);
        editor.commit();
    }

    public static String getSingleGroupId(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.GROUPID, "");
    }

    public static void setUserName(String user_f_name, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.USERFIRSTNAME, user_f_name);
        editor.commit();
    }

    public static String getUserName(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.USERFIRSTNAME, "");
    }

    public static void setj_id(String j_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.JID, j_id);
        editor.commit();
    }

    public static String getj_id(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.JID, "");
    }

    public static void setContectMode(String j_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.JID, j_id);
        editor.commit();
    }

    public static String getContectMode(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.JID, "");
    }

    public static void setOtherUserImage(String other_user_image, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.OTHERUSERIMAGE, other_user_image);
        editor.commit();
    }

    public static String getOtherUserImage(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.OTHERUSERIMAGE, "");
    }


    public static void setChatNotification(String chat_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.KEY_CHATACTIVTI, chat_id);
        editor.commit();
    }

    public static String getChatNotification(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.KEY_CHATACTIVTI, "");
    }

    public static void setPostType(String post_type, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.POSTTYPE, post_type);
        editor.commit();
    }

    public static String getPostType(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.POSTTYPE, "");
    }

    /************************chat********************/

    public static String getLoginUserName() {
        return preference.getString(ConstantMain.LOGING_USER_NAME, Constants.INVALID_ID_STRING);
    }

    public static void setLoginUserName(String userName) {
        editor.putString(ConstantMain.LOGING_USER_NAME, userName).apply();
    }

    public static String getLoginEmailId() {
        return preference.getString(ConstantMain.LOGING_EMAIL_ID, Constants.INVALID_ID_STRING);
    }

    public static void setLoginEmailId(String emailId) {
        editor.putString(ConstantMain.LOGING_EMAIL_ID, emailId).apply();
    }

    public static boolean isNotificationSoundStatus() {
        return preference.getBoolean(ConstantMain.KEY_NOTIFICATION_ON_OFF, Constants.INVALID_ID_BOOLEAN);
    }

    public static void setNotificationSoundStatus(boolean notifciationStatus) {
        editor.putBoolean(ConstantMain.KEY_NOTIFICATION_ON_OFF, notifciationStatus).apply();
    }

    public static boolean isNotificationMessageStatus() {
        return preference.getBoolean(ConstantMain.KEY_NOTIFICATIONMESSAGE_ON_OFF, Constants.INVALID_ID_BOOLEAN);
    }

    public static void setNotificationMessageStatus(boolean notifciationStatus) {
        editor.putBoolean(ConstantMain.KEY_NOTIFICATIONMESSAGE_ON_OFF, notifciationStatus).apply();
    }

    public static void setBooleanKeyvaluePrefs(Context ctx, String key,
                                               boolean value) {
        editor.putBoolean(key, value);
        editor.commit();

    }

    public static boolean getBooleanKeyvaluePrefs(String key) {
        return preference.getBoolean(key, false);
    }

    public static String getChatWallpaper() {
        return preference.getString(ConstantMain.CHAT_WALLPAPER, Constants.INVALID_ID_STRING);
    }

    public static void setChatWallpaper(String path) {
        editor.putString(ConstantMain.CHAT_WALLPAPER, path).apply();
    }

    public static void clearPer() {
        preference.edit().clear().apply();
    }

/*chat modelinfo*/

    public static void setmodel_user_j_id(String other_j_id, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.OTHER_JID, other_j_id);
        editor.commit();
    }

    public static String getmodelr_user_j_id(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.OTHER_JID, "");
    }

    public static void setmodel_user_Short_name(String other_name, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.OTHER_NAME, other_name);
        editor.commit();
    }

    public static String getmodel_user_Short_name(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.OTHER_NAME, "");
    }

    public static void setmodel_user_image(String other_image, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.OTHER_IMAGE, other_image);
        editor.commit();
    }

    public static String getmodel_user_image(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.OTHER_IMAGE, "");
    }

    public static void setLoginType(String other_image, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.LOGIN_TYPE, other_image);
        editor.commit();
    }

    public static String getLoginType(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.LOGIN_TYPE, "");
    }

    public static void setFollow(String userfollow, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.USER_FOLLOW, userfollow);
        editor.commit();
    }

    public static String getFollow(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.USER_FOLLOW, "");
    }

    public static void setOnline(boolean isONline) {
        editor.putBoolean(ConstantMain.IS_ONLINE, isONline).apply();
    }

    public static boolean isOnline() {
        return preference.getBoolean(ConstantMain.IS_ONLINE, Constants.INVALID_ID_BOOLEAN);
    }

    public static void setOtherUserUID(String uid, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.OTHERUSERUID, uid);
        editor.commit();
    }


    public static String getOtherUSERUID(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        return preferences.getString(ConstantMain.OTHERUSERUID, "");
    }

    public static void setLoginUserUID(String uid, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.LOGIN_UID, uid);
        editor.commit();
    }


    public static String getLoginUSERUID(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        return preferences.getString(ConstantMain.LOGIN_UID, "");
    }

//    public static void setLoggedOut(String uid, Context ctx) {
//        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putString(ConstantMain.LOGIN_UID, uid);
//        editor.commit();
//    }
//
//
//    public static String getLoggedOut(Context ctx) {
//        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
//        return preferences.getString(ConstantMain.LOGIN_UID, "");
//    }


    public static void setLoggedInUserStatus(String uid, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.CHAT_LOGIN_STATUS, uid);
        editor.commit();
    }


    public static String getLoggedInUserStatus(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        return preferences.getString(ConstantMain.CHAT_LOGIN_STATUS, "");
    }


    public static void setFullImageString(String uid, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.FULL_IMAGE, uid);
        editor.commit();
    }


    public static String getFullImageString(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(CONFIG_FILE, 0);
        return preferences.getString(ConstantMain.FULL_IMAGE, "");
    }


    public static void setChatMessageStatus(Integer userid, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(ConstantMain.CHAT_MESSAGE_NOTIFICATION, userid);
        editor.commit();
    }

    public static int getChatMessageStatus(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getInt(ConstantMain.CHAT_MESSAGE_NOTIFICATION, 0);
    }


    public static void setMainlatitude(String latitude, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.LATITUDEMAIN, latitude);
        editor.commit();
    }

    public static String getMainlatitude(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.LATITUDEMAIN, "");
    }

    public static void setMainlongitude(String longitude, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.LOGUTUDEMAIN, longitude);
        editor.commit();
    }

    public static String getMAinlongitude(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.LOGUTUDEMAIN, "");
    }


    public static void setCommonLatLng(String latitude, Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConstantMain.commonLatLng, latitude);
        editor.commit();
    }

    public static String getCommonLatLng(Context ctx) {
        SharedPreferences preference = ctx.getSharedPreferences(
                CONFIG_FILE, 0);
        return preference.getString(ConstantMain.commonLatLng, "");
    }

}

package com.afrimack.app.constants;

/**
 * Created by ubuntu on 29/4/17.
 */
public class ConstantMain {


    public static  String LOGIN = "a";
    public static int rating_count = 0;
    public static int rating = 0;
    public static final long SPLASH_TIME_OUT = 5000;


    //==============Instagram credentials && Keys===============
  /*  login: harsukhbir.impinge
            India1947*/

    public static final String CLIENT_ID = "b3a75aa23773412398366ad058ff8a27";
    public static final String CLIENT_SECRET = "3508273f1ca84962bf3f2958b65eaeab";
    public static final String CALLBACK_URL = "https://afrimack.com";
    //=====================================================

    public static final String LOGIN_TYPE = "LOGIN_TYPE";
    public static final String USER_FOLLOW = "USER_FOLLOW";
    public static final String IS_ONLINE = "IS_ONLINE";
    public static final String OTHERUSERUID = "OTHERUSERUID";
    public static final String LOGIN_UID = "LOGIN_UID";
    public static final String CHAT_LOGIN_STATUS = "CHAT_LOGIN";
    public static final String FULL_IMAGE = "FULL_IMAGE";
    public static String APPLICATION_NAME = "Afrimack";
    public static int SPLASH_SCREEN_DURATION = 100;

    public static String DEVICEID = "deviceid";
    public static String DEVICETOKEN = "devicetoken";
    public static String CURRENCYALERT = "Choose Currency";
    public static String HOURSALERT = "Choose Duration";
    public static String DURATIONALERT = "Choose Duration";

    public static final String NO_INTERNET_CONN = "No Internet Available";
    public static final String NO_INTERNET_CONN_DESC = "Unable to detect an internet connection. Please turn it on in your network settings or move in an area with internet connection.";

    public static String USERID = "userid";
    public static String CHAT_MESSAGE_NOTIFICATION = "chat_message_notification";
    public static String USERFIRSTNAME = "user_f_name";
    public static String USERIMAGE = "user_iamge";
    public static String USERTYPE = "user_type";
    public static String OPTNO = "otpNo";
    public static String PHONENO = "phoneno";
    public static String SELLIMAGE = "sellimage";
    public static String LATITUDE = "latitude";
    public static String LOGUTUDE = "longitude";
    public static String LATITUDEMAIN = "latitude_main";
    public static String LOGUTUDEMAIN = "longitude_main";
    public static String SingleRegister = "single_registered";
    public static String COMPLETPROFILE = "complet_profile";
    public static String FOLLOWTOTEL = "followtotel";
    public static String BLOCKTOTEL = "block_totel";

    /*----sorting ---*/
    public static String SORTING = "sorting";
    public static String LASTDAYS = "last_days";
    public static String RADIUS = "radius";
    public static String OWNCOUNTRY = "own_country";
    public static String COUNTRYSORT = "country";
    public static String SPECIALCATEGORY = "special_category";
    public static String SORTCATEGORY = "category";
    public static String SORTSUBCATEGORY = "sub_category";
    public static String MINPRICE = "min_price";
    public static String MAXPRICE = "max_price";
    public static String SORTLIST = "clist";
    public static String SEARCH = "search";
    public static String CHATMODEL = "chat_model";
    public static String FILTERTEPE = "filter_type";
    public static String BLOGID = "blog_id";



    /*----------------------------*/

    public static String SKIP = "skip";
    public static String LOGINBACk = "loginback";
    public static String POSTID = "post_id";
    public static String OFFERUSERID = "offer_user_id";
    public static String SINGLECAHTID = "single_user_id";
    public static String GROUPID = "group_id";
    public static String POSTTYPE = "post_type";


    /************chat**************/
    public static final String KEY_NOTIFICATION_ON_OFF = "key_notification_on_off";
    public static final String LOGING_USER_NAME = "login_user_name";
    public static final String LOGING_EMAIL_ID = "login_email_id";
    public static final String JID = "j_id";
    public static final String OTHERUSERIMAGE = "other_user_image";
    public static final String CHAT_WALLPAPER = "chat_wallpaper";
    public static final String KEY_NOTIFICATIONMESSAGE_ON_OFF = "key_notificationmessage_on_off";
    public static final String KEY_CHATACTIVTI = "chat_id";

    /************model chat**********************/
    public static final String OTHER_JID = "other_j_id";
    public static final String OTHER_NAME = "other_name";
    public static final String OTHER_IMAGE = "other_image";
    public static String profile_name = "";
    public static String commonLatLng="common_lat_lng";
}

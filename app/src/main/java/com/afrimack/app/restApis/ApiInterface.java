package com.afrimack.app.restApis;


import com.afrimack.app.appbeans.AddressFinderBean;
import com.afrimack.app.appbeans.BlockUserBean;
import com.afrimack.app.appbeans.ChatRequestBean;
import com.afrimack.app.appbeans.DiscoverResponseModel;
import com.afrimack.app.appbeans.FriendListBean;
import com.afrimack.app.appbeans.SettingBean;
import com.afrimack.app.models.AddResponseModel;
import com.afrimack.app.models.AlertSearchResponseModel;
import com.afrimack.app.models.BlockUserModel;
import com.afrimack.app.models.BlogInfoModel;
import com.afrimack.app.models.BlogModel;
import com.afrimack.app.models.BuyingResponseModel;
import com.afrimack.app.models.CategoriesResponseModel;
import com.afrimack.app.models.CategoryList;
import com.afrimack.app.models.ChatResponseModel;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.CountryListMode;
import com.afrimack.app.models.CurencyNameResponseModel;
import com.afrimack.app.models.DeletPostResponseModel;
import com.afrimack.app.models.DeleteChatImagesModel;
import com.afrimack.app.models.FilterResponseModel;
import com.afrimack.app.models.FollowingResponseModel;
import com.afrimack.app.models.GetSettingResponseModel;
import com.afrimack.app.models.HomeResponseModel;
import com.afrimack.app.models.InstagramLoginModel;
import com.afrimack.app.models.LoginResponseModel;
import com.afrimack.app.models.NotificationAllResponseModel;
import com.afrimack.app.models.OtherUserResponseModel;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.models.PostEditModel;
import com.afrimack.app.models.PostImageResponseModel;
import com.afrimack.app.models.PremiumResponseModel;
import com.afrimack.app.models.ProfileResponseModel;
import com.afrimack.app.models.ProfileStatusResponseModel;
import com.afrimack.app.models.ReviewsResponseModel;
import com.afrimack.app.models.SearchSaveResponseModel;
import com.afrimack.app.models.SellingResponseModel;
import com.afrimack.app.models.SignupResponseModel;
import com.afrimack.app.models.SingleProfileModel;
import com.afrimack.app.models.SingleProfileResponseModel;
import com.afrimack.app.models.SingleResponseModel;
import com.afrimack.app.models.SingleUserResponseModel;
import com.afrimack.app.models.SocilResponseModel;
import com.afrimack.app.models.SwapResponseModel;
import com.afrimack.app.models.TCPResponseModel;
import com.afrimack.app.models.UploadChatImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface ApiInterface {


    @FormUrlEncoded
    @POST("posts/all.json")
    Call<HomeResponseModel> PostHome(@Field("latitude") String lat,
                                     @Field("longitude") String longi,
                                     @Field("page_no") int page_no,
                                     @Field("last_days") String last_days,
                                     @Field("sorting") String sorting,
                                     @Field("radius") String radius,
                                     @Field("own_country") String own_country,
                                     @Field("special_category") String special_category,
                                     @Field("category") String category,
                                     @Field("sub_category") String sub_category,
                                     @Field("min_price") String min_price,
                                     @Field("max_price") String max_price,
                                     @Field("keyword") String saechAlret,
                                     @Field("user_id") String user_id,
                                     @Field("login_user_fid") String login_user_fid);

    @FormUrlEncoded
    @POST("categories/discoverCategory.json")
    Call<DiscoverResponseModel>Discover(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("alertSearches/listing.json")
    Call<AlertSearchResponseModel> AlertSearch(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("alertSearches/save.json")
    Call<SearchSaveResponseModel> SaveSearch(@Field("user_id") String user_id,
                                             @Field("term") String term,
                                             @Field("type") String type);

    @FormUrlEncoded
    @POST("alertSearches/delete.json")
    Call<SearchSaveResponseModel> DeleteSearch(@Field("user_id") String user_id,
                                               @Field("term") String term,
                                               @Field("type") String type);

    @FormUrlEncoded
    @POST("users/search_alert.json")
    Call<CommanResponseModel> AlertSearchPayment(@Field("user_id") String user_id,
                                                 @Field("package_id") String package_id);

    @FormUrlEncoded
    @POST("posts/detail.json")
    Call<PostDetailResponseModel> PostDetails(@Field("user_id") String user_id,
                                              @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("posts/addQuestion.json")
    Call<CommanResponseModel> Question(@Field("user_id") String user_id,
                                       @Field("post_id") String post_id,
                                       @Field("question") String question,
                                       @Field("question_id") String question_id);

    @FormUrlEncoded
    @POST("users/follow_user.json")
    Call<CommanResponseModel> UserFollow(@Field("user_id") String user_id,
                                         @Field("follower_id") String follower_id);

    @FormUrlEncoded
    @POST("users/unfollow.json")
    Call<CommanResponseModel> UserUnFollow(@Field("user_id") String user_id,
                                           @Field("follower_id") String follower_id);

    @FormUrlEncoded
    @POST("followings/favUnfav.json")
    Call<CommanResponseModel> HeartFollow(@Field("user_id") String user_id,
                                          @Field("post_id") String post_id,
                                          @Field("action") String action);

    @FormUrlEncoded
    @POST("followings/followUnfollow.json")
    Call<CommanResponseModel> PostFollowUnfollow(@Field("user_id") String user_id,
                                                 @Field("post_id") String post_id,
                                                 @Field("action") String action);

    @FormUrlEncoded
    @POST("users/block_user.json")
    Call<BlockUserBean> BlockUser(@Field("user_id") String user_id,
                                  @Field("block_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("posts/deleteQuestion.json")
    Call<CommanResponseModel> DeleteQuestion(@Field("user_id") String messuesr,
                                             @Field("question_id") String qution_id);

    @FormUrlEncoded
    @POST("posts/report.json")
    Call<CommanResponseModel> PostReport(@Field("user_id") String user_id,
                                         @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("users/report.json")
    Call<CommanResponseModel> ReportUser(@Field("report_user_id") String report_user_id,
                                         @Field("user_id") String user_id);

    @POST("categories/filter_category.json")
    Call<FilterResponseModel> FilterCategory();

    @FormUrlEncoded
    @POST("users/blogListing.json")
    Call<BlogModel> blog(@Field("page_no") int page_no);

    @FormUrlEncoded
    @POST("pages/cms.json")
    Call<TCPResponseModel> imprint(@Field("slug") String slug);

    @POST("posts/currency.json")
    Call<CurencyNameResponseModel> CurencyName();

    @POST("categories/ad_category.json")
    Call<CategoryList> Categories();


    @Multipart
    @POST("users/signup.json")
    Call<SignupResponseModel> Signup(@PartMap() Map<String, RequestBody> partMap,
                                     @Part MultipartBody.Part image);

    @Multipart
    @POST("users/signup.json")
    Call<SignupResponseModel> Signup(@PartMap() Map<String, RequestBody> partMap);

    @FormUrlEncoded
    @POST("users/resend_email.json")
    Call<CommanResponseModel> Resendmail(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("users/login.json")
    Call<LoginResponseModel> Login(@Field("device_token") String device_token,
                                   @Field("username") String email,
                                   @Field("password") String password);


    @FormUrlEncoded
    @POST("users/news.json")
    Call<NotificationAllResponseModel> NotificationAll(@Field("user_id") String user_id,
                                                       @Field("page_no") int page_no, @Field("login_user_fid") String login_user_fid);

    @FormUrlEncoded
    @POST("users/readMessage.json")
    Call<CommanResponseModel> ReadMessage(@Field("user_id") String user_id,
                                          @Field("notification_id") String notifiction_id);

    @FormUrlEncoded
    @POST("posts/selling.json")
    Call<SellingResponseModel> Selling(@Field("user_id") String user_id,
                                       @Field("type") String type,
                                       @Field("page_no") String page_no);

    @FormUrlEncoded
    @POST("posts/delete_selling.json")
    Call<DeletPostResponseModel> PostDeleteSell(@Field("user_id") String user_id,
                                                @Field("type") String post_id,
                                                @Field("post_id") String postId);

    @FormUrlEncoded
    @POST("followings/buying.json")
    Call<BuyingResponseModel> Buying(@Field("user_id") String user_id,
                                     @Field("type") String type,
                                     @Field("page_no") String page_no);

    @FormUrlEncoded
    @POST("followings/delete_buying.json")
    Call<DeletPostResponseModel> DeleteBuying(@Field("user_id") String user_id,
                                              @Field("post_id") String post_id,
                                              @Field("type") String type);

    @FormUrlEncoded
    @POST("users/profile.json")
    Call<ProfileResponseModel> Profile(@Field("user_id") String user_id, @Field("login_user_fid") String login_user_fid);

    @FormUrlEncoded
    @POST("users/following_users.json")
    Call<FollowingResponseModel> Following(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("users/reviews.json")
    Call<ReviewsResponseModel> Reviews(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("users/feedback.json")
    Call<CommanResponseModel> feedback(@Field("name") String f_name,
                                       @Field("email") String f_email,
                                       @Field("subject") String f_subject,
                                       @Field("message") String f_message);

    @FormUrlEncoded
    @POST("users/socialLogin.json")
    Call<SocilResponseModel> SocialLogin(@Field("device_token") String device_token,
                                         @Field("first_name") String first_name,
                                         @Field("media_type") String media_type,
                                         @Field("media_id") String media_id,
                                         @Field("email") String email,
                                         @Field("last_name") String last_name,
                                         @Field("image") String image,
                                         @Field("firebase_UID") String uid,
                                         @Field("fb_access_token") String fb_access_token,
                                         @Field("country") String country,
                                         @Field("address") String address);

    @FormUrlEncoded
    @POST("users/logout.json")
    Call<CommanResponseModel> Logout(@Field("user_id") String user_id,
                                     @Field("device_token") String deviceToken);

    @FormUrlEncoded
    @POST("users/forgotPassword.json")
    Call<CommanResponseModel> Forgotpassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("offers/detail.json")
    Call<ChatResponseModel> Chat(@Field("user_id") String user_id,
                                 @Field("post_id") String post_id,
                                 @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("offers/placeOffer.json")
    Call<CommanResponseModel> MakeOffer(@Field("user_id") String user_id,
                                        @Field("post_id") String post_id,
                                        @Field("offer_value") String price,
                                        @Field("offer_user_id") String offer_user_id,
                                        @Field("offer_type") String offer_type);

    @FormUrlEncoded
    @POST("offers/cancelOffer.json")
    Call<CommanResponseModel> CancelPost(@Field("user_id") String user_id,
                                         @Field("post_id") String post_id,
                                         @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("offers/remindPartner.json")
    Call<CommanResponseModel> RemindPost(@Field("user_id") String user_id,
                                         @Field("post_id") String post_id,
                                         @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    //@POST("offers/acceptOffer.json")
    @POST("offers/acceptOffer.json")
    Call<CommanResponseModel> AcceptOffer(@Field("user_id") String user_id,
                                          @Field("post_id") String post_id,
                                          @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("posts/chatMessage.json")
    Call<CommanResponseModel> ChatOffer(@Field("user_id") String user_id,
                                        @Field("post_id") String post_id,
                                        @Field("offer_user_id") String offer_user_id,
                                        @Field("message") String message);

    @FormUrlEncoded
    @POST("offers/confirmOffer.json")
    Call<CommanResponseModel> Confirm(@Field("user_id") String user_id,
                                      @Field("post_id") String post_id,
                                      @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("posts/rating.json")
    Call<CommanResponseModel> RatingReview(@Field("user_id") String user_id,
                                           @Field("offer_user_id") String offer_user_id,
                                           @Field("rating") String rating,
                                           @Field("review") String review,
                                           @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("users/userProfile.json")
    Call<OtherUserResponseModel> OtherUserProfile(@Field("user_id") String user_id,
                                                  @Field("login_user_id") String login_user_id);


    @FormUrlEncoded
    @POST("users/single_filter.json")
    Call<SingleUserResponseModel> SingleUser(@Field("user_id") String user_id,
                                             @Field("page_no") int page_no,
                                             @Field("latitude") String latitude,
                                             @Field("longitude") String longitude,
                                             @Field("own_country") String own_country,
                                             @Field("Male") String male,
                                             @Field("Female") String female,
                                             @Field("min-age") String min_age,
                                             @Field("max-age") String max_age,
                                             @Field("last_days") String last_days,
                                             @Field("sorting") String sorting,
                                             @Field("radius") String radius,
                                             @Field("interests") String interests);

    @POST("users/country.json")
    Call<CountryListMode> PostProcessCountry();

    @FormUrlEncoded
    @POST("users/singleRegisterProfile.json")
    Call<SingleProfileModel> SingleProfileUser(@Field("user_id") String user_id);

    @Multipart
    @POST("users/single_register.json")
    Call<SingleResponseModel> SingleRegister(@PartMap() HashMap<String, RequestBody> multipartRequest,
                                             @Part MultipartBody.Part image);

    @Multipart
    @POST("users/single_register.json")
    Call<SingleResponseModel> SingleRegister(@PartMap() HashMap<String, RequestBody> multipartRequest);

    @FormUrlEncoded
    @POST("users/deleteSingleProfile.json")
    Call<CommanResponseModel> DeleteSingleProfile(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("premiums/listing.json")
    Call<PremiumResponseModel> Premium(@Field("user_id") String userId, @Field("post_id") String postId);

    @FormUrlEncoded
    @POST("premiums/save.json")
    Call<CommanResponseModel> PostPayment(@Field("user_id") String user_id,
                                          @Field("package_id") String package_id,
                                          @Field("product_id") String post_id);

    @FormUrlEncoded
    @POST("users/changePassword.json")
    Call<CommanResponseModel> ResetPassword(@Field("user_id") String user_id,
                                            @Field("old_password") String old_pass,
                                            @Field("new_password") String new_pass,
                                            @Field("confirm_password") String cinfrm_pass);

    @FormUrlEncoded
    @POST("pages/cms.json")
    Call<TCPResponseModel> TermAndPolicy(@Field("slug") String slug);

    @FormUrlEncoded
    @POST("users/blogDetail.json")
    Call<BlogInfoModel> blogInfo(@Field("blog_id") String setBlogId);

    @FormUrlEncoded
    @POST("users/profile_status.json")
    Call<ProfileStatusResponseModel> ProfileStatus(@Field("user_id") String user_id);

    @Multipart
    @POST("users/profile_update.json")
    Call<ProfileStatusResponseModel> PostProfileUpdate(@PartMap() HashMap<String, RequestBody> multipartRequest,
                                                       @Part MultipartBody.Part image);

    @Multipart
    @POST("users/profile_update.json")
    Call<ProfileStatusResponseModel> PostProfileUpdate(@PartMap() HashMap<String, RequestBody> multipartRequest);

    @FormUrlEncoded
    @POST("users/blockList.json")
    Call<BlockUserModel> blockUser(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("users/unblockUser.json")
    Call<CommanResponseModel> unBlockUser(@Field("user_id") String user_id,
                                          @Field("block_id") String blockuser_id);

    @Multipart
    @POST("posts/add.json")
    Call<AddResponseModel> PostAddSell(@PartMap() HashMap<String, RequestBody> multipartRequest,
                                       @Part ArrayList<MultipartBody.Part> params);

    @Multipart
    @POST("posts/add.json")
    Call<AddResponseModel> PostAddSell(@PartMap() HashMap<String, RequestBody> multipartRequest);

    @FormUrlEncoded
    @POST("posts/postdetail.json")
    Call<PostEditModel> PostEdit(@Field("user_id") String user_id,
                                 @Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("swapDeals/detail.json")
    Call<SwapResponseModel> SwapDeal(@Field("user_id") String user_id,
                                     @Field("post_id") String post_id,
                                     @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("swapDeals/placeOffer.json")
    Call<CommanResponseModel> SwapMakeOffer(@Field("user_id") String user_id,
                                            @Field("post_id") String post_id,
                                            @Field("offer_post_id") String swap_id,
                                            @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("swapDeals/cancelOffer.json")
    Call<CommanResponseModel> SwapCancelPost(@Field("user_id") String user_id,
                                             @Field("post_id") String post_id,
                                             @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("swapDeals/remindPartner.json")
    Call<CommanResponseModel> SwapRemindPost(@Field("user_id") String user_id,
                                             @Field("post_id") String post_id,
                                             @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("swapDeals/acceptOffer.json")
    Call<CommanResponseModel> SwapAcceptOffer(@Field("user_id") String user_id,
                                              @Field("post_id") String post_id,
                                              @Field("offer_user_id") String offer_user_id);

    @FormUrlEncoded
    @POST("posts/chatMessage.json")
    Call<CommanResponseModel> Chatoffer(@Field("user_id") String user_id,
                                        @Field("post_id") String post_id,
                                        @Field("offer_user_id") String offer_user_id,
                                        @Field("message") String message);

    @FormUrlEncoded
    @POST("swapDeals/confirmOffer.json")
    Call<CommanResponseModel> SwapConfirm(@Field("user_id") String user_id,
                                          @Field("post_id") String post_id,
                                          @Field("offer_user_id") String offer_user_id);


    @FormUrlEncoded
    @POST("users/singleProfile.json")
    Call<SingleProfileResponseModel> SingleProfile(@Field("single_user_id") String single_user_id,
                                                   @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("posts/images.json")
    Call<PostImageResponseModel> PostImage(@Field("post_id") String post_id);

    @FormUrlEncoded
    @POST("users/chatListDetails.json")
    Call<FriendListBean> getFriendsList(@Field("login_user_fid") String lgginUserId, @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("users/getUserConfigurationSetting.json")
    Call<SettingBean> GetSettings(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("users/updateUserConfigurationSetting.json")
    Call<SettingBean> updateSettings(@Field("user_id") String user_id,
                                     @Field("ads_online") String ads_online,
                                     @Field("new_question") String new_question,
                                     @Field("new_offer") String new_offer,
                                     @Field("new_ads_from_following") String new_ads_from_following,
                                     @Field("new_ads_from_facebook_friend") String new_ads_from_facebook_friend,
                                     @Field("single_profile_online") String single_profile_online,
                                     @Field("new_message_chat") String new_message_chat,
                                     @Field("new_message_afrimack") String new_message_afrimack,
                                     @Field("accept_offer") String accept_offer,
                                     @Field("confirm") String confirm,
                                     @Field("remind_confirm") String remind_confirm,
                                     @Field("type") String type);


    @FormUrlEncoded
    @POST("users/PlaceChatRequest.json")
    Call<ChatRequestBean> placeChatRequest(@Field("from_user_id") String fromUserID,
                                           @Field("to_user_id") String toUserID);


    @FormUrlEncoded
    @POST("users/ConfirmIgnoreChatRequest.json")
    Call<ChatRequestBean> confirmIgnoreChatRequest(@Field("from_user_id") String fromUserID,
                                                   @Field("to_user_id") String toUserID,
                                                   @Field("status") String status);

//    https://maps.googleapis.com/maps/api/geocode/json?latlng=44.4647452,7.3553838&sensor=true


    @GET("geocode/json")
    Call<AddressFinderBean> getCompleteAddress(@Query("latlng") String latlong,
                                               @Query("sensor") Boolean sensor,
                                               @Query("key") String key
    );

    @FormUrlEncoded
    @POST("users/instaLogin.json")
    Call<InstagramLoginModel> insta_login(@Field("insta_id") String insta_id,
                                                       @Field("device_token") String device_token,
                                                       @Field("first_name") String first_name ,  @Field("email") String email,@Field("last_name") String last_name,
                                          @Field("image") String image,
                                          @Field("address") String address ,  @Field("country") String country);


    //========upload_image=========

    @Multipart
    @POST("followings/saveChatImage.json")
    Call<UploadChatImage> UploadImageChat(@PartMap() Map<String, RequestBody> partMap,
                                          @Part MultipartBody.Part image);

    @Multipart
    @POST("followings/saveChatImage.json")
    Call<UploadChatImage> UploadImageChat(@PartMap() Map<String, RequestBody> partMap);

    //===========delete_images_from_server=============

    @FormUrlEncoded
    @POST("followings/unlinkChatImage.json")
    Call<DeleteChatImagesModel> DeleteChatImage(@Field("user_id") String user_id);

}


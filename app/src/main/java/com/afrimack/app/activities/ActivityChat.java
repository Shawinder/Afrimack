package com.afrimack.app.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.afrimack.app.AfrimackApp;
import com.afrimack.app.R;
import com.afrimack.app.adapters.AllCatogeryAdapter;
import com.afrimack.app.adapters.ChatAdapter;
import com.afrimack.app.adapters.OfferAdapter;
import com.afrimack.app.appbeans.BlockUserBean;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.ChatResponseModel;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChat extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout chat_main, lay_chat;
    private ImageView img_back;
    private TextView tv_titel_chat;

    private ImageView post_image_chat, user_pci;
    private TextView last_update, post_titel_chat, last_date;
    private EditText chat_message;
    private Button make_offer;
    private Button btn_contet_offer;
    private Button btn_cancel_deal, btn_remind_partemer, btn_confirm;
    private Button btn_confrim_app;

    private String user_id, post_id, offer_user_id, post_user_id;
    private LinearLayout lay_conf_acc, lay_mack_offer;
    private RecyclerView recy_offer_chat, rec_chat;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.LayoutManager chatLayoutManager;
    private List<PostDetailResponseModel.ResponseBean.OffersBean> offerListl;
    private OfferAdapter offerAdapter;
    private ChatAdapter chatAdapter;
    private LinearLayout lay_user_profile, lay_offerc_chat, lay_conf_rem_can;
    private TextView tv_other_user, tv_user_name, tv_finall_deal;
    private ImageView img_other_user, img_user_off, stap_fill, tv_chat_dene;
    private ImageView chat_off, chat, img_dot_chat;
    String price, rating_type, post_user_profile, offer_user_profile;


    /*chat */
    private ImageView chat_user_profile, img_send_message;
    private EditText et_message;
    private TextView rate_user;
    public static LinearLayout lay_rate;
    private RatingBar rating_bar_app;
    String posttitel, post_prise, offer_user_name;

    ArrayAdapter<String> swapAdapter;
    private Spinner sp_swop_del;
    private Dialog dialogswap;

    private LinearLayout lay_swap_deal;
    private RecyclerView recy_caterer_list;
    private AllCatogeryAdapter allCatogery;
    private int pos, posimage;
    private ArrayList<String> categories;
    private String swap_id, dealTepe;
    private ChatResponseModel chatDetail;
    private TextView tv_swao_deal;
    private static final String NUMBER_REGEX = "^[0-9]+$+(\\.0-9]+)";
    private TextView tv_currency_symbol;
    private String chanoti;
    private Context context = this;
    private String TAG = ActivityChat.class.getSimpleName();

    View.OnClickListener onClickListener;
    String useroffer_id;
    private String from = "";
    private PopupWindow popupWindow;
    private LinearLayout id_linear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        from = getIntent().getStringExtra("from");
        user_id = Config.getUserId(this);
        post_id = Config.getPostId(this);
        offer_user_id = Config.getOfferUserID(this);

        init();
        onClickListener = this;

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
            } else {

                chanoti = extras.getString("notification");
            }
        } else {
        }

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recy_offer_chat.setLayoutManager(mLayoutManager);

        chatLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rec_chat.setLayoutManager(chatLayoutManager);

        chat_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                tv_swao_deal.setText("");
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        hitApi();


        InputFilter filter = new InputFilter() {
            final int maxDigitsBeforeDecimalPoint = 20;
            final int maxDigitsAfterDecimalPoint = 2;

            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                StringBuilder builder = new StringBuilder(dest);
                builder.replace(dstart, dend, source
                        .subSequence(start, end).toString());
                if (!builder.toString().matches(
                        "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?"

                )) {
                    if (source.length() == 0)
                        return dest.subSequence(dstart, dend);
                    return "";
                }

                return null;

            }
        };

        chat_message.setFilters(new InputFilter[]{filter});
    }


    private void hitApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ChatResponseModel> call = apiInterface.Chat(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<ChatResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<ChatResponseModel> call, @NonNull Response<ChatResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        chatDetail = response.body();
                        if (chatDetail.isError() == false) {
                            chat_main.setVisibility(View.VISIBLE);
                            post_user_id = chatDetail.getResponse().getPost_user_id();
                            if (post_user_id.equalsIgnoreCase(user_id)) {
                                img_dot_chat.setVisibility(View.GONE);
                            }

                            Picasso
                                    .with(context)
                                    .load(chatDetail.getResponse().getPost_image())
                                    .placeholder(R.drawable.dami_background)
                                    .into(post_image_chat);

                            post_prise = chatDetail.getResponse().getLast_offer();
                            last_update.setText("LISTED BY " + chatDetail.getResponse().getPost_user_name()
                                    + " FOR " + chatDetail.getResponse().getPost_price());
                            last_date.setText("UPDATED ON " + chatDetail.getResponse().getLast_update());
                            offer_user_name = chatDetail.getResponse().getOffer_user_name();

                            if (from.equalsIgnoreCase("news")) {
                                Picasso
                                        .with(context)
                                        .load(chatDetail.getResponse().getPost_user_image())
                                        .placeholder(R.drawable.dami_image2)
                                        .into(user_pci);
                            } else {
                                Picasso
                                        .with(context)
                                        .load(chatDetail.getResponse().getOffer_user_image())
                                        .placeholder(R.drawable.dami_image2)
                                        .into(user_pci);
                            }

                            tv_other_user.setText(chatDetail.getResponse().getPost_user_name());
                            tv_user_name.setText(chatDetail.getResponse().getOffer_user_name());
                            if (chatDetail.getResponse().getMy_product() == 1) {
                                lay_swap_deal.setVisibility(View.GONE);

                                Picasso
                                        .with(context)
                                        .load(chatDetail.getResponse().getPost_user_image())
                                        .placeholder(R.drawable.dami_image2)
                                        .into(chat_user_profile);

                                rate_user.setText(getResources().getString(R.string.all_dine) + " " + chatDetail.getResponse().getOffer_user_name());
                            } else {

                                Picasso
                                        .with(context)
                                        .load(chatDetail.getResponse().getOffer_user_image())
                                        .placeholder(R.drawable.dami_image2)
                                        .into(chat_user_profile);

                                rate_user.setText(getResources().getString(R.string.all_dine) + " " + chatDetail.getResponse().getPost_user_name());
                            }

                            Picasso
                                    .with(context)
                                    .load(chatDetail.getResponse().getPost_user_image())
                                    .placeholder(R.drawable.dami_image2)
                                    .into(img_other_user);

                            post_user_profile = chatDetail.getResponse().getPost_user_image();
                            offer_user_profile = chatDetail.getResponse().getOffer_user_image();
                            tv_finall_deal.setText(chatDetail.getResponse().getFinal_offer_value());


                            Picasso
                                    .with(context)
                                    .load(chatDetail.getResponse().getOffer_user_image())
                                    .placeholder(R.drawable.dami_image2)
                                    .into(img_user_off);


                            posttitel = chatDetail.getResponse().getTitle();


                            post_titel_chat.setText(chatDetail.getResponse().getTitle());
                            tv_currency_symbol.setHint(chatDetail.getResponse().getCurrency_symbol());
                            if (chatDetail.getResponse().getLast_offer_user_id().equalsIgnoreCase(user_id)) {
                                lay_conf_acc.setVisibility(View.GONE);
                                lay_mack_offer.setVisibility(View.VISIBLE);
                                make_offer.setVisibility(View.VISIBLE);
                            } else if (String.valueOf(chatDetail.getResponse().getLast_offer_user_id()).equalsIgnoreCase("")) {
                                lay_conf_acc.setVisibility(View.GONE);
                                lay_mack_offer.setVisibility(View.VISIBLE);
                                make_offer.setVisibility(View.VISIBLE);
                            } else {
                                lay_conf_acc.setVisibility(View.VISIBLE);
                                lay_mack_offer.setVisibility(View.GONE);
                                make_offer.setVisibility(View.GONE);
                            }

                            if (chatDetail.getResponse().getLast_status().equalsIgnoreCase("A")) {
                                make_offer.setVisibility(View.GONE);
                                lay_conf_acc.setVisibility(View.GONE);
                                lay_user_profile.setVisibility(View.VISIBLE);
                                stap_fill.setBackground(getResources().getDrawable(R.drawable.step1));

                            } else if (chatDetail.getResponse().getLast_status().equalsIgnoreCase("CO")) {
                                if (chatDetail.getResponse().getLast_offer_user_id().equalsIgnoreCase(user_id)) {

                                } else {
                                    lay_conf_acc.setVisibility(View.VISIBLE);
                                    lay_mack_offer.setVisibility(View.GONE);
                                    make_offer.setVisibility(View.GONE);
                                }


                            } else if (chatDetail.getResponse().getLast_status().equalsIgnoreCase("C")) {


                                make_offer.setVisibility(View.GONE);
                                lay_conf_acc.setVisibility(View.GONE);
                                lay_user_profile.setVisibility(View.VISIBLE);
                                lay_chat.setVisibility(View.VISIBLE);
                                lay_conf_rem_can.setVisibility(View.GONE);
                                stap_fill.setBackground(getResources().getDrawable(R.drawable.step2));

                            } else if (chatDetail.getResponse().getLast_status().equalsIgnoreCase("X")) {
                                make_offer.setVisibility(View.GONE);
                                lay_conf_acc.setVisibility(View.GONE);
                                lay_user_profile.setVisibility(View.VISIBLE);
                                lay_chat.setVisibility(View.GONE);
                                lay_conf_rem_can.setVisibility(View.GONE);

                            }
                            if (chatDetail.getResponse().getStatus_by().equalsIgnoreCase("Y")) {
                                btn_remind_partemer.setVisibility(View.VISIBLE);
                                btn_confirm.setVisibility(View.GONE);
                                lay_offerc_chat.setVisibility(View.GONE);

                            } else {
                                btn_remind_partemer.setVisibility(View.GONE);
                                btn_confirm.setVisibility(View.VISIBLE);
                            }

                            rating_type = chatDetail.getResponse().getRating();
                            if (rating_type.equalsIgnoreCase("N")) {
                                lay_rate.setVisibility(View.VISIBLE);
                            }
                            if (chatDetail.getResponse().getOffers().size() > 0) {
                                recy_offer_chat.setVisibility(View.VISIBLE);
                                offerAdapter = new OfferAdapter(context, chatDetail.getResponse().getOffers(), onClickListener);
                                recy_offer_chat.setAdapter(offerAdapter);
                                offerAdapter.notifyDataSetChanged();
                            }

                            if (chatDetail.getResponse().getChat().size() > 0) {

                                stap_fill.setBackground(getResources().getDrawable(R.drawable.step3));
                                tv_chat_dene.setBackgroundColor(getResources().getColor(R.color.color_green));
                                chat_off.setVisibility(View.GONE);
                                chat.setVisibility(View.VISIBLE);

                                chatAdapter = new ChatAdapter(context, chatDetail.getResponse().getChat(), onClickListener);
                                rec_chat.setAdapter(chatAdapter);
                                chatAdapter.notifyDataSetChanged();
                            }
                            List<String> hour = new ArrayList<String>();
                            for (int k = 1; k <= 5; k++) {
                                hour.add(String.valueOf(k));
                            }
                            swapAdapter = new ArrayAdapter<String>(context, R.layout.spinner_layout_view, hour);
                            swapAdapter.setDropDownViewResource(R.layout.spinner_text_view);

                            if (chatDetail.getResponse().getPost_swap().size() > 0) {
                                lay_swap_deal.setVisibility(View.VISIBLE);
                                categories = new ArrayList<String>();
                                for (int i = 0; i < chatDetail.getResponse().getPost_swap().size(); i++) {
                                    categories.add(chatDetail.getResponse().getPost_swap().get(i).getTitle());

                                }
                                if (chatDetail.getResponse().getSwap_deal().equalsIgnoreCase("N")) {
                                    lay_swap_deal.setVisibility(View.GONE);
                                }
                            } else {
                                lay_swap_deal.setVisibility(View.GONE);
                            }

                        } else {
                            CommonUtils.getInstance().dismissLoadingDialog();
                            Common.displayLongToast(context, "This content is no more available.");
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ChatResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void init() {

        id_linear = (LinearLayout) findViewById(R.id.id_linear);
        chat_main = (RelativeLayout) findViewById(R.id.chat_main);

        img_back = (ImageView) findViewById(R.id.img_back_chat);
        tv_titel_chat = (TextView) findViewById(R.id.tv_titel_chat);
        img_dot_chat = (ImageView) findViewById(R.id.img_dot_chat);

        img_back.setOnClickListener(this);
        tv_titel_chat.setText(getResources().getString(R.string.app_titel));

        post_image_chat = (ImageView) findViewById(R.id.post_image_chat);
        last_update = (TextView) findViewById(R.id.last_update);
        last_date = (TextView) findViewById(R.id.last_date);
        user_pci = (ImageView) findViewById(R.id.user_pci);
        chat_message = (EditText) findViewById(R.id.chat_message);
        post_titel_chat = (TextView) findViewById(R.id.post_titel_chat);
        make_offer = (Button) findViewById(R.id.make_offer);

        lay_mack_offer = (LinearLayout) findViewById(R.id.lay_mack_offer);
        lay_conf_acc = (LinearLayout) findViewById(R.id.lay_conf_acc);
        btn_contet_offer = (Button) findViewById(R.id.btn_contet_offer);
        btn_confirm = (Button) findViewById(R.id.btn_confirm);


        btn_cancel_deal = (Button) findViewById(R.id.btn_cancel_deal);
        btn_remind_partemer = (Button) findViewById(R.id.btn_remind_partemer);
        btn_confrim_app = (Button) findViewById(R.id.btn_confrim_app);

        lay_chat = (RelativeLayout) findViewById(R.id.lay_chat);
        recy_offer_chat = (RecyclerView) findViewById(R.id.recy_offer_chat);
        rec_chat = (RecyclerView) findViewById(R.id.rec_chat);

        chat_user_profile = (ImageView) findViewById(R.id.chat_user_profile);
        et_message = (EditText) findViewById(R.id.et_message);
        img_send_message = (ImageView) findViewById(R.id.img_send_message);

        lay_user_profile = (LinearLayout) findViewById(R.id.lay_user_profile);
        lay_offerc_chat = (LinearLayout) findViewById(R.id.lay_offerc_chat);
        lay_conf_rem_can = (LinearLayout) findViewById(R.id.lay_conf_rem_can);

        tv_other_user = (TextView) findViewById(R.id.tv_other_user);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        img_other_user = (ImageView) findViewById(R.id.img_other_user);
        tv_finall_deal = (TextView) findViewById(R.id.tv_finall_deal);
        img_user_off = (ImageView) findViewById(R.id.img_user_off);
        stap_fill = (ImageView) findViewById(R.id.stap_fill);
        tv_chat_dene = (ImageView) findViewById(R.id.tv_chat_dene);

        chat_off = (ImageView) findViewById(R.id.chat_off);
        chat = (ImageView) findViewById(R.id.chat);

        lay_rate = (LinearLayout) findViewById(R.id.lay_rate);
        rating_bar_app = (RatingBar) findViewById(R.id.rating_bar_app);
        rate_user = (TextView) findViewById(R.id.rate_user);
        make_offer.setOnClickListener(this);
        btn_contet_offer.setOnClickListener(this);
        btn_cancel_deal.setOnClickListener(this);
        btn_remind_partemer.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);

        btn_confrim_app.setOnClickListener(this);
        img_send_message.setOnClickListener(this);
        img_dot_chat.setOnClickListener(this);
        lay_rate.setOnClickListener(this);


        lay_swap_deal = (LinearLayout) findViewById(R.id.lay_swap_deal);
        lay_swap_deal.setOnClickListener(this);
        tv_swao_deal = (TextView) findViewById(R.id.tv_swao_deal);
        tv_swao_deal.setOnClickListener(this);
        tv_currency_symbol = (TextView) findViewById(R.id.tv_currency_symbol);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AfrimackApp.curentactivity = this;

    }

    @Override
    public void onBackPressed() {
        if (chanoti != null) {
            Intent intent = new Intent(this, ActMyAfrimack.class);
            intent.putExtra("from", "a");
            chanoti = null;
            startActivity(intent);
            finish();

        } else {
            super.onBackPressed();
        }
        AfrimackApp.curentactivity = null;
    }

    @SuppressLint("NewApi")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_chat:
                onBackPressed();
                break;

            case R.id.make_offer:
                v.startAnimation(AppConstants.buttonClick);
                kayhden();
                String swaidel_text = tv_swao_deal.getText().toString();
                price = chat_message.getText().toString().trim();
                // if(swaidel_text.equalsIgnoreCase("Select Swop Deal")){
                if (swaidel_text.length() > 0) {

                    if (!price.matches(NUMBER_REGEX)) {
                        chat_message.setText("");
                        String offer_type = "POST";
                        if (swaidel_text.length() != 0) {
                            hitMakeOffer(swap_id, offer_type);
                        }

                    } else {
                    }
                } else {
                    if (price.matches("")) {
                        chat_message.setText("");
                        Common.displayLongToast(this, "Please select valid deal");

                    } else {
                        String offer_type = "PRICE";
                        if (price.length() != 0) {
                            hitMakeOffer(price, offer_type);
                        }
                    }

                }

                price = chat_message.getText().toString().trim();

                break;
            case R.id.btn_contet_offer:
                v.startAnimation(AppConstants.buttonClick);
                kayhden();
                lay_conf_acc.setVisibility(View.GONE);
                lay_mack_offer.setVisibility(View.VISIBLE);
                make_offer.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_cancel_deal:
                v.startAnimation(AppConstants.buttonClick);
                kayhden();

                //=============changes by sukh================
                AlertDialog.Builder alert_dialog_cancel = new AlertDialog.Builder(ActivityChat.this);

                // Setting Dialog Title
                alert_dialog_cancel.setTitle("Cancel Deal");

                // Setting Dialog Message
                alert_dialog_cancel.setMessage("Are you sure you want to cancel this deal?");


                // Setting Positive "Yes" Button
                alert_dialog_cancel.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        hitCancelDealApi();

                    }
                });

                // Setting Negative "NO" Button
                alert_dialog_cancel.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alert_dialog_cancel.show();

//=============================



                break;
            case R.id.btn_remind_partemer:
                v.startAnimation(AppConstants.buttonClick);
                kayhden();

//=======================changes by sukh==============
                AlertDialog.Builder alert_dialog_remind= new AlertDialog.Builder(ActivityChat.this);

                // Setting Dialog Title
                alert_dialog_remind.setTitle("Remind Partner");

                // Setting Dialog Message
                alert_dialog_remind.setMessage("Are you sure you want to send reminder? ");


                // Setting Positive "Yes" Button
                alert_dialog_remind.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        hitRemindPartenerApi();

                    }
                });

                // Setting Negative "NO" Button
                alert_dialog_remind.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alert_dialog_remind.show();
//=====================================================



                break;
            case R.id.btn_confrim_app:
                v.startAnimation(AppConstants.buttonClick);
                kayhden();
                AlertDialog.Builder alertDialogConfirm = new AlertDialog.Builder(ActivityChat.this);
                alertDialogConfirm.setTitle("Accept Deal");
                try {

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        if (chatDetail.getResponse().getMy_product() == 1) {

                            alertDialogConfirm.setMessage(Html.fromHtml("Are you sure you want to sell " + "<b>" + posttitel + "</b>" + " for " + "<b>" + post_prise + "</b>" + "? \nYour acceptance is binding when " + offer_user_name + " confirms."));

                        } else {
                            alertDialogConfirm.setMessage(Html.fromHtml("Are you sure you want to buy " + "<b>" + posttitel + "</b>" + " for " + "<b>" + post_prise + "</b>" + "? \nYour acceptance is binding when " + chatDetail.getResponse().getPost_user_name() + " confirms."));
                        }
                    } else {
                        if (chatDetail.getResponse().getMy_product() == 1) {

                            alertDialogConfirm.setMessage(Html.fromHtml("Are you sure you want to sell " + "<b>" + posttitel + "</b>" + " for " + "<b>" + post_prise + "</b>" + "? \nYour acceptance is binding when " + offer_user_name + " confirms."));

                        } else {
                            alertDialogConfirm.setMessage(Html.fromHtml("Are you sure you want to buy " + "<b>" + posttitel + "</b>" + " for " + "<b>" + post_prise + "</b>" + "? \nYour acceptance is binding when " + chatDetail.getResponse().getPost_user_name() + " confirms."));
                        }
                    }

                }
                catch (Exception e) {
                    e.printStackTrace();
                }


                alertDialogConfirm.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        hitApprovedApi();

                    }
                });
                alertDialogConfirm.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });
                alertDialogConfirm.show();
                break;
            case R.id.img_send_message:
                kayhden();
                if (et_message.getText().length() > 0) {
                    String question = et_message.getText().toString().trim();
                    hitChatApi(question);
                } else {
                    Common.displayLongToast(ActivityChat.this, getResources().getString(R.string.enter_search));
                }
                break;
            case R.id.btn_confirm:
                v.startAnimation(AppConstants.buttonClick);
                kayhden();
                AlertDialog.Builder alertDialogConfirmfinal = new AlertDialog.Builder(ActivityChat.this);

                // Setting Dialog Title
                alertDialogConfirmfinal.setTitle("Confirm Deal");

                // Setting Dialog Message
                try {
                    if (chatDetail.getResponse().getLast_status().equalsIgnoreCase("A")) {
                        if (user_id.equalsIgnoreCase(post_user_id)) {
                            alertDialogConfirmfinal.setMessage("Your offer is accepted by " + chatDetail.getResponse().getOffer_user_name() + ". Are you sure you want to confirm " + chatDetail.getResponse().getOffer_user_name() + " offer?");
                        } else {
                            alertDialogConfirmfinal.setMessage("Your offer is accepted by " + chatDetail.getResponse().getPost_user_name() + ". Are you sure you want to confirm " + chatDetail.getResponse().getPost_user_name() + " offer?");
                        }

                    } else {

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {

                            alertDialogConfirmfinal.setMessage(Html.fromHtml("Are you sure you want to sell " + "<b>" + posttitel + "</b>" + " for " + "<b>" + post_prise + "</b>" + "? \nYour acceptance is binding when " + offer_user_name + " confirms."));

                        } else {

                            alertDialogConfirmfinal.setMessage(Html.fromHtml("Are you sure you want to sell " + "<b>" + posttitel + "</b>" + " for " + "<b>" + post_prise + "</b>" + "? \nYour acceptance is binding when " + offer_user_name + " confirms."));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                // Setting Positive "Yes" Button
                alertDialogConfirmfinal.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        hitConfirmApi();

                    }
                });

                // Setting Negative "NO" Button
                alertDialogConfirmfinal.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialogConfirmfinal.show();

                break;

            case R.id.img_dot_chat:
                v.startAnimation(AppConstants.buttonClick);
                kayhden();
                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.dialog_chat, null);
                TextView tv_block_user = (TextView) popupView.findViewById(R.id.tv_block_user);

                tv_block_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        popupWindow.dismiss();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setMessage("Are you sure you want to block this user?");
                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        hitBlockUserApi();
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    }
                });


                popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setOutsideTouchable(true);
//                popupWindow.showAtLocation(id_linear, Gravity.RIGHT | Gravity.TOP, 0, 0);
                popupWindow.showAsDropDown(id_linear, 0, 0, Gravity.RIGHT);


                break;


            case R.id.lay_rate:

                kayhden();
                if (rating_type.equalsIgnoreCase("N")) {
                    Intent intent = new Intent(ActivityChat.this, RateReviewActivity.class);
                    intent.putExtra("post_id", post_id);
                    if (user_id.equalsIgnoreCase(post_user_id)) {
                        intent.putExtra("post_user_id", offer_user_id);
                        intent.putExtra("post_user_profile", offer_user_profile);
                    } else {
                        intent.putExtra("post_user_id", offer_user_id);
                        intent.putExtra("post_user_profile", post_user_profile);
                    }


                    startActivity(intent);
                } else {
                    Common.displayLongToast(this, "You have already made your rating of this user. Thank you!");
                }

                break;
            case R.id.tv_swao_deal:

                kayhden();
                swaplist();
                break;
            case R.id.lay_swap_deal:
                kayhden();
                swaplist();

                break;
            case R.id.catogety:
                dialogswap.dismiss();
                pos = (int) (v.getTag());
                swap_id = String.valueOf(chatDetail.getResponse().getPost_swap().get(pos).getId());
                chat_message.setText("");
                tv_swao_deal.setText(String.valueOf(chatDetail.getResponse().getPost_swap().get(pos).getTitle()));

                break;
            case R.id.me_profile_chat:
                posimage = (int) (v.getTag());
                useroffer_id = String.valueOf(chatDetail.getResponse().getChat().get(posimage).getUser_id());
                sendOfferUserActivity(useroffer_id);
                break;
            case R.id.other_profile_chat:
                posimage = (int) (v.getTag());
                useroffer_id = String.valueOf(chatDetail.getResponse().getChat().get(posimage).getUser_id());
                sendOfferUserActivity(useroffer_id);
                break;
            case R.id.me_profile:
                posimage = (int) (v.getTag());
                useroffer_id = String.valueOf(chatDetail.getResponse().getOffers().get(posimage).getUser_id());
                sendOfferUserActivity(useroffer_id);
                break;
            case R.id.other_profile:
                posimage = (int) (v.getTag());
                useroffer_id = String.valueOf(chatDetail.getResponse().getOffers().get(posimage).getUser_id());
                sendOfferUserActivity(useroffer_id);
                break;
            case R.id.lay_other:
                int swaop_pos = (int) (v.getTag());
                String swop_type = String.valueOf(chatDetail.getResponse().getOffers().get(swaop_pos).getOffer_type());
                String swaop_id = chatDetail.getResponse().getOffers().get(swaop_pos).getOffer_value();
                if (swop_type.equalsIgnoreCase("POST")) {

                    Intent intent = new Intent(ActivityChat.this, ActivityDiscoverDetail.class);
                    //  Config.setPostId(swaop_id,this);
                    intent.putExtra("post_id", swaop_id);
                    startActivity(intent);
                }
                break;
        }

    }

    private void hitBlockUserApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<BlockUserBean> call = apiInterface.BlockUser(user_id, post_user_id);
            call.enqueue(new Callback<BlockUserBean>() {
                @Override
                public void onResponse(@NonNull Call<BlockUserBean> call, @NonNull Response<BlockUserBean> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        BlockUserBean commanResponseModel = response.body();

                        if (commanResponseModel.getError() == false) {
                            Common.displayLongToast(context, commanResponseModel.getMessage());
                            Intent intent = new Intent(context, ActivityHome.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BlockUserBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitConfirmApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.Confirm(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
//                        Common.displayLongToast(context, commanResponseModel.getMessage());

                        if (commanResponseModel.isError() == false) {
                            make_offer.setVisibility(View.GONE);
                            lay_conf_acc.setVisibility(View.GONE);
                            lay_user_profile.setVisibility(View.VISIBLE);
                            lay_chat.setVisibility(View.VISIBLE);
                            lay_conf_rem_can.setVisibility(View.GONE);
                            stap_fill.setBackground(getResources().getDrawable(R.drawable.step2));
//                            hitApiAgain();
                        } else {
                            CommonUtils.getInstance().showAlertMessage(context, commanResponseModel.getMessage());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void hitChatApi(String question) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.ChatOffer(user_id, post_id, offer_user_id, question);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
//                        CommonUtils.getInstance().showAlertMessage(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            et_message.setText("");
                            hitApi();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    @Override
    protected void onDestroy() {
        AfrimackApp.curentactivity = null;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        AfrimackApp.curentactivity = null;
        super.onPause();
    }

    private void hitApprovedApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.AcceptOffer(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
//                        Common.displayLongToast(context, commanResponseModel.getMessage());
//                        CommonUtils.getInstance().showAlertMessage(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            hitApi();
                        } else {
                            CommonUtils.getInstance().showAlertMessage(context, commanResponseModel.getMessage());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitRemindPartenerApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.RemindPost(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
//                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        CommonUtils.getInstance().showAlertMessage(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            //  callapidata();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void hitCancelDealApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.CancelPost(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();

//                        CommonUtils.getInstance().showAlertMessage(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            Common.displayLongToast(context, commanResponseModel.getMessage());
                        } else {
                            CommonUtils.getInstance().showAlertMessage(context, commanResponseModel.getMessage());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void hitMakeOffer(String swap_id, String offer_type) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.MakeOffer(user_id, post_id, swap_id, offer_user_id, offer_type);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
//                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            chat_message.getText().clear();
                            hitApi();
                        } else {
                            CommonUtils.getInstance().showAlertMessage(context, commanResponseModel.getMessage());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void sendOfferUserActivity(String user_offer_id1) {
        Intent intent = new Intent(ActivityChat.this, OfferUserActivity.class);
        intent.putExtra("other_user_id", user_offer_id1);
        startActivity(intent);
    }

    private void kayhden() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    private void swaplist() {

        dialogswap = new Dialog(this);
        dialogswap.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogswap.setContentView(R.layout.all_catogery_list);
        dialogswap.setCancelable(true);
        dialogswap.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        recy_caterer_list = (RecyclerView) dialogswap.findViewById(R.id.recy_caterer_list);
        mLayoutManager = new LinearLayoutManager(ActivityChat.this, LinearLayoutManager.VERTICAL, false);
        recy_caterer_list.setLayoutManager(mLayoutManager);
        if (categories != null && categories.size() > 0) {
            allCatogery = new AllCatogeryAdapter(this, categories, this);
            recy_caterer_list.setAdapter(allCatogery);
        }


        dialogswap.show();
    }

    public void chatnotification() {
        hitApi();

    }


}

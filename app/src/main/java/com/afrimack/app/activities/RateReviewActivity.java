package com.afrimack.app.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RateReviewActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;

    private RatingBar RatingBar;
    private Button send_review;
    private EditText et_review;
    private ImageView profrile_rate_uaer;

    private String post_id, user_id, offer_user_id, rating, review;
    private String post_user_profile;
    private Context context = this;
    private String TAG = RateReviewActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_review);
        user_id = Config.getUserId(this);
        init();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                post_id = null;
            } else {
                post_id = extras.getString("post_id");
                offer_user_id = extras.getString("post_user_id");
                post_user_profile = extras.getString("post_user_profile");

                // post_user_id =extras.getString("post_user_id");

            }
        } else {
        }
        Picasso.with(this)
                .load(post_user_profile)
                .placeholder(R.drawable.user_round)
                .error(R.drawable.user_round)
                .into(profrile_rate_uaer);
    }

    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);
        img_tick.setVisibility(View.GONE);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.user_rate).toString());
        img_back.setOnClickListener(this);

        //************end titel*****************//
        profrile_rate_uaer = (ImageView) findViewById(R.id.profrile_rate_uaer);

        RatingBar = (android.widget.RatingBar) findViewById(R.id.rating_post);

        send_review = (Button) findViewById(R.id.send_review);
        et_review = (EditText) findViewById(R.id.et_review);
        send_review.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.send_review:
                v.startAnimation(AppConstants.buttonClick);
                rating = String.valueOf(RatingBar.getRating());
                review = et_review.getText().toString().trim();
                hitRatingAndReviewApi();
                break;
        }
    }

    private void hitRatingAndReviewApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.RatingReview(user_id, offer_user_id, rating, review, post_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            ActivityChat.lay_rate.setVisibility(View.GONE);
                            finish();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

}

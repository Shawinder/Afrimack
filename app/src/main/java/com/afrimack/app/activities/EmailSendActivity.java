package com.afrimack.app.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailSendActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel;
    private TextView tv_click_here;
    String user_id, onscreen;
    private Dialog dialogmessage;
    private Button btn_continue;
    private Context context = this;
    public String TAG = EmailSendActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_send);

        init();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //post_id= null;
            } else {

                user_id = extras.getString("userId");
                onscreen = extras.getString("onscreen");

            }
        } else {
        }
//        Log.d("user_id", user_id);
        try {
            if (onscreen.equalsIgnoreCase("signup")) {
                btn_continue.setVisibility(View.VISIBLE);
                showDialogAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showDialogAlert() {
        dialogmessage = new Dialog(this);
        dialogmessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogmessage.setContentView(R.layout.custom_dialog);
        dialogmessage.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogmessage.setCancelable(false);
        TextView mess_colose = (TextView) dialogmessage.findViewById(R.id.mess_colose);
        TextView dailog_message = (TextView) dialogmessage.findViewById(R.id.dailog_message);
        TextView dalog_ok = (TextView) dialogmessage.findViewById(R.id.dalog_ok);
        TextView dalog_titel = (TextView) dialogmessage.findViewById(R.id.dalog_titel);
        dailog_message.setText("Please check your mail. It can takes several minutes until you receive it.");

        dalog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogmessage.dismiss();
//                finish();
            }
        });
        mess_colose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogmessage.dismiss();
//                finish();

            }


        });

        dialogmessage.show();
    }

    private void init() {
        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);

        img_tick.setVisibility(View.GONE);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.app_titel).toString());
        img_back.setOnClickListener(this);

        //************end titel*****************//

        tv_click_here = (TextView) findViewById(R.id.tv_click_here);
        tv_click_here.setOnClickListener(this);

        btn_continue = (Button) findViewById(R.id.btn_continue);
        btn_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_click_here:
                v.startAnimation(AppConstants.buttonClick);
                hitApi();

                break;
            case R.id.btn_continue:
                v.startAnimation(AppConstants.buttonClick);
                //Intent intenthome = new Intent(this, SplashLogin.class);
                Intent intenthome = new Intent(this, Login.class);
                intenthome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intenthome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intenthome);
                finish();
                break;

        }
    }

    private void hitApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            Call<CommanResponseModel> call = apiInterface.Resendmail(user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel model = response.body();
                        if (model.isError() == false) {
                            dailogMesaage(model.getMessage());
                            //finish();
                        } else {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }


    private void dailogMesaage(String message) {

        dialogmessage = new Dialog(this);
        dialogmessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogmessage.setContentView(R.layout.custom_dialog);
        dialogmessage.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogmessage.setCancelable(false);
        TextView mess_colose = (TextView) dialogmessage.findViewById(R.id.mess_colose);
        TextView dailog_message = (TextView) dialogmessage.findViewById(R.id.dailog_message);
        TextView dalog_ok = (TextView) dialogmessage.findViewById(R.id.dalog_ok);
        TextView dalog_titel = (TextView) dialogmessage.findViewById(R.id.dalog_titel);
        dailog_message.setText(message);

        dalog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogmessage.dismiss();
                finish();
            }
        });
        mess_colose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogmessage.dismiss();
                finish();

            }


        });

        dialogmessage.show();
    }
}

package com.afrimack.app.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.FilterAdapter;
import com.afrimack.app.adapters.FollowingAdapters;
import com.afrimack.app.adapters.SpicelAdapter;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.FilterResponseModel;
import com.afrimack.app.models.FollowingResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowingActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rec_following;
    private ImageView img_tick, img_back;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tv_titel;

    private String user_id;
    private List<FollowingResponseModel> followingListl;
    private FollowingAdapters followingAdapters;
    View.OnClickListener onClickListener;
    FollowingResponseModel model;
    private Context context = this;
    public String TAG = FollowingActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);

        user_id = Config.getUserId(this);
        onClickListener = this;
        rec_following = (RecyclerView) findViewById(R.id.rec_following);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
        img_tick.setVisibility(View.GONE);

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rec_following.setLayoutManager(mLayoutManager);

        img_back.setOnClickListener(this);
        tv_titel.setText(getResources().getString(R.string.following));

        hitFollowingApi();

    }

    private void hitFollowingApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<FollowingResponseModel> call = apiInterface.Following(user_id);
            call.enqueue(new Callback<FollowingResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<FollowingResponseModel> call, @NonNull Response<FollowingResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        model = response.body();
                        if (model.isError() == false) {
                            if (model.getResponse() != null) {

                                if (model.getResponse().size() > 0) {
                                    Config.setFollowTotel(String.valueOf(model.getResponse().size()), context);
                                    followingListl = new ArrayList<FollowingResponseModel>();
                                    followingAdapters = new FollowingAdapters(context, model.getResponse(), onClickListener);
                                    rec_following.setAdapter(followingAdapters);

                                } else {
                                    Config.setFollowTotel("0", context);
                                    Common.displayLongToast(context, getResources().getString(R.string.data_nota));
                                    finish();

                                }

                            }

                        } else {
                            Config.setFollowTotel("", context);
                            finish();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<FollowingResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_unfollow:

                int po = (int) v.getTag();
                String followkuser_id = String.valueOf(model.getResponse().get(po).getFollower_id());
                if (ConnectionDetector.isNetAvail(this)) {
                    hitUserUnFollowApi(followkuser_id);
                } else {
                    Common.displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                }
                break;
            case R.id.img_back:
                finish();
                break;
        }
    }

    private void hitUserUnFollowApi(String followUserId) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.UserUnFollow(user_id, followUserId);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            hitFollowingApi();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }


}

package com.afrimack.app.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.location.Address;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.afrimack.app.R;
import com.afrimack.app.adapters.AllCatogeryAdapter;
import com.afrimack.app.constants.GPSTracker;
import com.afrimack.app.models.CategoriesResponseModel;
import com.afrimack.app.models.CurencyNameResponseModel;
import com.afrimack.app.rest.RestClient;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

public class EditPostActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    ArrayAdapter<String> hoursAdapter;
    double latitude, longitude;
    List<Address> addresses;
    private EditText etSelling;
    private EditText etDescribe;
    private EditText etPrice;
    private EditText tvSellOn;
    // public static TextView tv_catoger;
    private ToggleButton tbSwapDeal;
    private ToggleButton tbShareOnFacebook;
    private Spinner spPrice;
    private Spinner spCategory;
    private Spinner spHour, sptype;
    private Button btSellRent;
    private String btnName;
    private LinearLayout lay_week;
    private int dayweekm = 1;
    private RestClient restClient;
    private ArrayList<String> currency;
    private CurencyNameResponseModel curencyNameModel;
    private ArrayAdapter<String> currencyAdapter;
    private ArrayList<String> categories;
    private CategoriesResponseModel categoriesModel;
    private ArrayAdapter<String> categoriesAdapter;
    private String country_id, title, description, price, post_type, user_id, addressfull ="Jaipur", lat, longi, swap_deal_title, hours, category_id,duration_type ="",duration_value="";
    private String swap_deal = "N", share_fb = "N";
    private ArrayList<String> images;
    //private PopupWindow popupWindow;
    private TextView tv_cteagr;
    private RecyclerView recy_caterer_list;
    private LinearLayoutManager mLayoutManager;
    private AllCatogeryAdapter allCatogery;
    private int pos;
    private Dialog dialog,dialogmessage;


    //  private Location mLastLocation;
    private ArrayList<String> imagesPathList;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private GPSTracker gpsTracker;

    private LocationManager manager;
    private boolean isDataRecieved = false;
    private AlertDialog gpsAlertDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        init();
    }

    private void init() {
        tv_cteagr = (TextView) findViewById(R.id.tv_cteagr);
        tv_cteagr.setOnClickListener(this);
        //set views
        etSelling = (EditText) findViewById(R.id.etSelling);
        etDescribe = (EditText) findViewById(R.id.etDescibe);
        etPrice = (EditText) findViewById(R.id.etPrice);
        tvSellOn = (EditText) findViewById(R.id.tvSellOn);
        tbSwapDeal = (ToggleButton) findViewById(R.id.tbSwapDeal);
        tbShareOnFacebook = (ToggleButton) findViewById(R.id.tbShareOnFacebook);
        spPrice = (Spinner) findViewById(R.id.spPrice);
        spCategory = (Spinner) findViewById(R.id.spCategory);
        spHour = (Spinner) findViewById(R.id.spHour);
        sptype = (Spinner) findViewById(R.id.sptype);
        btSellRent = (Button) findViewById(R.id.btSellRent);
        lay_week = (LinearLayout) findViewById(R.id.lay_week);
        //click task
        etSelling.setOnClickListener(this);
        etDescribe.setOnClickListener(this);
        etPrice.setOnClickListener(this);
        tbSwapDeal.setOnClickListener(this);
        tbShareOnFacebook.setOnClickListener(this);
        btSellRent.setOnClickListener(this);
        // Spinner click listener
        spCategory.setOnItemSelectedListener(this);
        spPrice.setOnItemSelectedListener(this);
        spHour.setOnItemSelectedListener(this);
        sptype.setOnItemSelectedListener(this);

        etSelling.setHint(getResources().getString(R.string.what_you_are_rating));
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

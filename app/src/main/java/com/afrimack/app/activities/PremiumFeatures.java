package com.afrimack.app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;

public class PremiumFeatures extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back,img_tick;
    private TextView tv_titel;
    private RecyclerView recy_premium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium_features);

        init();
    }

    private void init() {
        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);

        img_tick.setVisibility(View.GONE);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(R.string.premium_features);
        img_back.setOnClickListener(this);
        //************end titel*****************//

        recy_premium = (RecyclerView) findViewById(R.id.recy_premium);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }
}

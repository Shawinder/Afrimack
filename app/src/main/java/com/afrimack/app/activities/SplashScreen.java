package com.afrimack.app.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.afrimack.app.AfrimackApp;
import com.afrimack.app.R;
import com.afrimack.app.appbeans.AddressFinderBean;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.services.Locationservice;
import com.afrimack.app.services.OnClearFromRecentService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static com.afrimack.app.services.Locationservice.mGoogleApiClient;
import static com.afrimack.app.utils.Common.checkPermissions;

public class SplashScreen extends Activity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private String TAG = getClass().getName();
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private Context context = this;
    private ProgressBar mProgressBar;
    private LocationManager manager;
    private boolean isDataRecieved = false;
    LocationManager locationManager;
    private AlertDialog gpsAlertDialog;
    private boolean isGpsDialogShowing = false;
    public GoogleApiClient mGoogleApiClient;
    private FusedLocationProviderClient mFusedLocationClient;
    private boolean canGetLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screenn);


        mProgressBar = (ProgressBar) findViewById(R.id.splashscreen_pb_progressbar);
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (Config.getCommonLatLng(this).equalsIgnoreCase("")) {
            Config.setCommonLatLng("yes", this);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                ShowGpsDialog();

                if (!isMyServiceRunning()) {
                    startService(new Intent(SplashScreen.this, Locationservice.class));
                }
                //setViewAction();


            }

        } else {
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                ShowGpsDialog();
                if (!isMyServiceRunning()) {
                    startService(new Intent(SplashScreen.this, Locationservice.class));
                }
                // setViewAction();
            }

        }

        Config.setSortDay("", this);
        Config.setSortRadius("", this);
        Config.setSortMinPrice("", this);
        Config.setSortMaxPrice("", this);
        Config.setSortCategory("", this);
        Config.setSortSpecialCategory("", this);
        Config.setSortBy("", this);
        Config.setSortOwnCountry("no", this);
        Config.setSearch("", this);
        try {
            startService(new Intent(getBaseContext(), OnClearFromRecentService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        hashKey();


    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getPackageName() + ".services.Locationservice").equals(service1.service.getClassName())) {

                return true;
            }
        }
        return false;
    }

    private void setViewAction() {
        try {
            mProgressBar.setVisibility(View.VISIBLE);
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.e("Refreshed token: ", refreshedToken);
            Config.setDeviceToken(refreshedToken, getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mProgressBar.setVisibility(View.GONE);


                startActivity(new Intent(SplashScreen.this, ActivityHome.class));
                SplashScreen.this.finish();


            }
        }, ConstantMain.SPLASH_TIME_OUT);
    }

    private void hashKey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.afrimack.app",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private boolean checkAndRequestPermissions() {
        try {
            int storage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int internet = ContextCompat.checkSelfPermission(this, android.Manifest.permission.INTERNET);
            int camera = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
            int accessCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            List<String> listPermissionsNeeded = new ArrayList<>();


            if (storage != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (internet != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.INTERNET);
            }
            if (accessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (camera != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }


            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                        (new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (checkAndRequestPermissions()) {

                    try {
                        startService(new Intent(SplashScreen.this, Locationservice.class));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    setViewAction();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {

        super.onResume();

        try {
            startService(new Intent(SplashScreen.this, Locationservice.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return;
        }

        Location location = null;


        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            locationManager.getProviders(true);
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) /*&& !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)*/) {
                Log.e("MainLocation", "Enable");
                //  locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,this);
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        0,
                        0, this);
                Log.e("Network", "Network Enabled");
                location = locationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    double lat = location.getLatitude();
                    double longi = location.getLongitude();
                    Config.setlatitude(lat + "", this);
                    Config.setlongitude(longi + "", this);
                    Config.setMainlongitude(lat + "", this);
                    Config.setMainlongitude(longi + "", this);
                    Log.e("Splash latLng: ", lat + "," + longi);


                }
//                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//
//
//                    // startService(new Intent(SplashScreen.this, Locationservice.class));
//
//                    if (checkAndRequestPermissions()) {
//
//                        setViewAction();
//                    }
//
//
//                }
            }
            Log.e("RETURN", "VALUE");
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        isDataRecieved = false;

    }


    @Override
    protected void onDestroy() {
        Log.e("ON_DESTROY_SPLASH", "FDGSRDGDRf");
        stopService(new Intent(this, Locationservice.class));
        removeGpsDialog();
        super.onDestroy();

    }

    private void ShowGpsDialog() {
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                context);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("No GPS")
                .setMessage(getString(R.string.gps_info))
                .setPositiveButton(getString(R.string.gps_seating),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                try {
                                    // if (gpsAlertDialog != null)
                                    gpsAlertDialog.dismiss();
                                    // continue with delete
                                    removeGpsDialog();
                                    Intent intent = new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                    removeGpsDialog();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                // finish();
                            }
                        });

        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                gpsAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(false);
            }
        });
        gpsAlertDialog.show();
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStart() {
//        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(SplashScreen.this)
//                .addOnConnectionFailedListener(SplashScreen.this).addApi(LocationServices.API).build();
//        if (ActivityCompat.checkSelfPermission(context,
//                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Log.e("inside iffff",">>>>>>>>>> ");
//        }
//        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        Log.e("latitude",">>>>>>>>>> " + location.getLatitude());
//        if (mGoogleApiClient != null) {
//            mGoogleApiClient.connect();
//        }
        super.onStart();
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            try {
                startService(new Intent(SplashScreen.this, Locationservice.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
            // startService(new Intent(SplashScreen.this, Locationservice.class));

            if (checkAndRequestPermissions()) {

                setViewAction();
            }


        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.e("ONLOCTIONCHANGED", "UPDATE");
        Config.setlatitude(String.valueOf(location.getLatitude()), this);
        Config.setlongitude(String.valueOf(location.getLongitude()), this);
        if(Config.getCommonLatLng(this).equalsIgnoreCase("yes"))
        {
            Config.setMainlatitude(String.valueOf(location.getLatitude()), this);
            Config.setMainlongitude(String.valueOf(location.getLongitude()), this);
            Config.setCommonLatLng("no",this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

//        Location location = null;
//
//
//        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
////            LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
////            locationManager.getProviders(true);
////            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) /*&& !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)*/) {
////                Log.e("MainLocation", "Enable");
////                //  locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,this);
//                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
////                locationManager.requestLocationUpdates(
////                        LocationManager.NETWORK_PROVIDER,
////                        0,
////                        0, this);
//                Log.e("Network", "Network Enabled");
//                location = locationManager
//                        .getLastKnownLocation(provider);
//                if (location != null) {
//                    double lat = location.getLatitude();
//                    double longi = location.getLongitude();
//                    Config.setlatitude(lat + "", this);
//                    Config.setlongitude(longi + "", this);
//                    Config.setMainlongitude(lat + "", this);
//                    Config.setMainlongitude(longi + "", this);
//                    Log.e("Splash latLng: ", lat + "," + longi);
//
//
//                }

//            }
        Log.e("RETURN", "VALUE");
//        }
    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

package com.afrimack.app.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.appbeans.ChatRequestBean;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.SingleProfileResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.CommonUtils;
import com.brsoftech.emozi.EmojiconEditText;
import com.squareup.picasso.Picasso;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChatRequest extends AppCompatActivity implements View.OnClickListener {
    private EmojiconEditText et_message;
    private ImageView img_back_chat, img_profile_chat;
    private TextView tv_chat_user_name;
    private TextView tv_message_text, tv_confirm, tv_ignore, tv_time_chat_request, tv_on_off;
    private ImageView img_chat_menu;
    private LinearLayout id_linear_chat;
    private Context context = this;
    private String TAG = ActivityChatRequest.class.getSimpleName();

    private String from = "";
    private int userIdBlock = 0;
    private int blockedStatus = 0;
    private int blocked_by = 0;
    private int php_block_id = 0;
    private String chatUserImage = "", userName = "";
    private String otherUserUID = "";
    private long timeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_request);

        from = getIntent().getStringExtra("from");
        chatUserImage = getIntent().getStringExtra("user_image");
        otherUserUID = getIntent().getStringExtra("U_ID");
        userIdBlock = getIntent().getIntExtra("other_user_id", 0);
        blockedStatus = getIntent().getIntExtra("blocked_status", 0);
        blocked_by = getIntent().getIntExtra("blocked_by", 0);
        php_block_id = getIntent().getIntExtra("php_block_id", 0);
        userName = getIntent().getStringExtra("user_name");
        timeStamp = getIntent().getLongExtra("time_stamp", 0);
        init();
        setData();
    }

    private void setData() {

        try {
            Picasso.with(context)
                    .load(chatUserImage)
                    .placeholder(R.drawable.dami_image2)
                    .into(img_profile_chat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_chat_user_name.setText(userName);
        tv_message_text.setText(userName + " sent you a chat request.");
        tv_time_chat_request.setText(getDateCurrentTimeZone(timeStamp));

    }

    public String getDateCurrentTimeZone(long timestamp) {
        try {
            return DateFormat.format("HH:mm a", new Date(timestamp)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    private void init() {
        id_linear_chat = (LinearLayout) findViewById(R.id.id_linear_chat);
        img_back_chat = (ImageView) findViewById(R.id.img_back_chat);
        img_profile_chat = (ImageView) findViewById(R.id.img_profile_chat);
        tv_chat_user_name = (TextView) findViewById(R.id.tv_chat_user_name);
        tv_on_off = (TextView) findViewById(R.id.tv_on_off);
        tv_on_off.setVisibility(View.GONE);
        tv_message_text = (TextView) findViewById(R.id.tv_message_text);
        tv_ignore = (TextView) findViewById(R.id.tv_ignore);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
        tv_time_chat_request = (TextView) findViewById(R.id.tv_time_chat_request);

        img_chat_menu = (ImageView) findViewById(R.id.img_chat_menu);
        img_chat_menu.setVisibility(View.GONE);

        img_profile_chat.setOnClickListener(this);
        img_back_chat.setOnClickListener(this);
        tv_confirm.setOnClickListener(this);
        tv_ignore.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_chat:
                this.finish();
                break;

            case R.id.img_profile_chat:
                break;

            case R.id.tv_confirm:
                view.startAnimation(AppConstants.buttonClick);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                alertDialogBuilder.setMessage("Are you sure you want to confirm this chat request?");


                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
//                                1 for confirm status
                                hitApi("1");
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


                break;

            case R.id.tv_ignore:
                view.startAnimation(AppConstants.buttonClick);

                AlertDialog.Builder alertDialogBuilderr = new AlertDialog.Builder(context);

                alertDialogBuilderr.setMessage("Are you sure you want to ignore this chat request?");


                alertDialogBuilderr.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // 2 for ignore status
                                hitApi("2");
                            }
                        });

                alertDialogBuilderr.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialogg = alertDialogBuilderr.create();
                alertDialogg.show();

                break;
        }
    }

    private void hitApi(final String status) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ChatRequestBean> call = apiInterface.confirmIgnoreChatRequest(String.valueOf(userIdBlock), Config.getUserId(context), status);
            call.enqueue(new Callback<ChatRequestBean>() {
                @Override
                public void onResponse(@NonNull Call<ChatRequestBean> call, @NonNull Response<ChatRequestBean> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        ChatRequestBean mObj = response.body();
                        Toast.makeText(context, mObj.getMessage(), Toast.LENGTH_LONG).show();
                        if (status.equalsIgnoreCase("1")) {
                            Intent mIntent = new Intent(context, SingleChatActivity.class);
                            mIntent.putExtra("U_ID", otherUserUID);
                            mIntent.putExtra("user_image", chatUserImage);
                            mIntent.putExtra("other_user_id", userIdBlock);
                            mIntent.putExtra("blocked_status", blockedStatus);
                            mIntent.putExtra("blocked_by", blocked_by);
                            mIntent.putExtra("php_block_id", php_block_id);
                            mIntent.putExtra("from", from);
                            startActivity(mIntent);
                            ActivityChatRequest.this.finish();
                        } else {
                            ActivityChatRequest.this.finish();
                        }


                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ChatRequestBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }
}

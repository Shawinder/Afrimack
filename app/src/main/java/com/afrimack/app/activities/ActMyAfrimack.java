package com.afrimack.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.SellViewPagerAdapter;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.customviews.TextViewNormal;
import com.afrimack.app.customviews.TextViewNormalBold;
import com.afrimack.app.fragments.FragBuying;
import com.afrimack.app.fragments.FragNews;
import com.afrimack.app.fragments.FragProfile;
import com.afrimack.app.fragments.FragSellingBuying;
import com.afrimack.app.rest.RestClient;
import com.brsoftech.core_utils.base.BaseAppCompatActivity;

/**
 * Created by ubuntu on 3/5/17.
 */
public class ActMyAfrimack extends BaseAppCompatActivity implements View.OnClickListener {


    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;
    private Toolbar toolbarMyAfrimack;
    public TabLayout tablayoutMyAfrimack;
    private ViewPager viewpagerMyAfrimack;
    private LinearLayout llProfileHeader;
    private String user_id;
    private String backHome;
    private String from = "";

    public TabLayout getTablayoutMyAfrimack() {
        return tablayoutMyAfrimack;
    }

    public LinearLayout getLlProfileHeader() {
        return llProfileHeader;
    }

    public ImageView getImg_tick() {
        return img_tick;
    }


    public TextView getTv_titel() {
        return tv_titel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_afrimack);

        from = getIntent().getStringExtra("from");
        user_id = Config.getUserId(this);
        init();
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //post_id= null;
            } else {

                backHome = extras.getString("notification");
            }
        } else {
        }
    }

    /*@Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        from = intent.getStringExtra("from");
        user_id = Config.getUserId(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) backHome = extras.getString("notification");
    }*/

    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.my_afrimack).toString());
        img_back.setOnClickListener(this);
        toolbarMyAfrimack = (Toolbar) findViewById(R.id.toolbarMyAfrimack);
        tablayoutMyAfrimack = (TabLayout) findViewById(R.id.tablayoutMyAfrimack);
        viewpagerMyAfrimack = (ViewPager) findViewById(R.id.viewpagerMyAfrimack);
        viewpagerMyAfrimack.setOffscreenPageLimit(1);


        SellViewPagerAdapter MyAfrimackViewPagerAdapter = new SellViewPagerAdapter(getSupportFragmentManager());
        MyAfrimackViewPagerAdapter.addFrag(new FragNews(), 1, getString(R.string.NEWS));
        MyAfrimackViewPagerAdapter.addFrag(new FragSellingBuying(), 2, getString(R.string.SELLING));
        MyAfrimackViewPagerAdapter.addFrag(new FragBuying(), 3, getString(R.string.BUYING));
        MyAfrimackViewPagerAdapter.addFrag(new FragProfile(), 4, getString(R.string.PROFILE));
        viewpagerMyAfrimack.setAdapter(MyAfrimackViewPagerAdapter);
        tablayoutMyAfrimack.setupWithViewPager(viewpagerMyAfrimack);
        TextViewNormal customTab_news = (TextViewNormal) LayoutInflater.from(this).inflate(R.layout.custom_tab_news, null);
        TextViewNormal customTab_selling = (TextViewNormal) LayoutInflater.from(this).inflate(R.layout.custom_tab_selling, null);
        TextViewNormal customTab_buying = (TextViewNormal) LayoutInflater.from(this).inflate(R.layout.custom_tab_buying, null);
        TextViewNormal customTab_profile = (TextViewNormal) LayoutInflater.from(this).inflate(R.layout.custom_tab_profile, null);
        tablayoutMyAfrimack.getTabAt(0).setCustomView(customTab_news);
        tablayoutMyAfrimack.getTabAt(1).setCustomView(customTab_selling);
        tablayoutMyAfrimack.getTabAt(2).setCustomView(customTab_buying);
        tablayoutMyAfrimack.getTabAt(3).setCustomView(customTab_profile);

    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            Config.setFollowTotel("", this);
            Config.setBlockTotel("", this);
            onBackPressed();

        }

    }

    @Override
    public void onBackPressed() {

        if (from.equalsIgnoreCase("from_notifications")) {
            Intent mIntent = new Intent(ActMyAfrimack.this, ActivityHome.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mIntent);
            ActMyAfrimack.this.finish();
        } else {
            super.onBackPressed();
        }


    }

}

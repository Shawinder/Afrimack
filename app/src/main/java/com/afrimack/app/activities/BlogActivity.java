package com.afrimack.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.BlogAdapter;
import com.afrimack.app.models.BlogModel;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.CommonUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlogActivity extends AppCompatActivity implements OnLoadMoreListener, View.OnClickListener {

    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;

    private RecyclerView rcy_blog;
    private LinearLayoutManager mLayoutManager;
    private BlogAdapter blogAdapter;
    int page_no = 1;
    private OnLoadMoreListener onLoadMoreListener;
    BlogModel blogModel;
    private ArrayList<BlogModel.ResponseBean> blList = new ArrayList<BlogModel.ResponseBean>();
    private Context context = this;
    private String TAG = BlogActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        onLoadMoreListener = this;
        init();
        hitBlogApi();
    }

    private void hitBlogApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<BlogModel> call = apiInterface.blog(page_no);
            call.enqueue(new Callback<BlogModel>() {
                @Override
                public void onResponse(@NonNull Call<BlogModel> call, @NonNull Response<BlogModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        blogModel = response.body();

                        if (blogModel.isError() == false) {

                            if (blogModel.getResponse().size() > 0) {
                                blList.addAll(blogModel.getResponse());
                                blogAdapter.bloglist = blList;
                                blogAdapter.setLoaded();
                                blogAdapter.notifyDataSetChanged();

                            }

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BlogModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void init() {


        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText("Blogs");
        img_back.setOnClickListener(this);
        //************end titel*****************//

        rcy_blog = (RecyclerView) findViewById(R.id.rcy_blog);

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcy_blog.setLayoutManager(mLayoutManager);

        blogAdapter = new BlogAdapter(this, blList, rcy_blog, onLoadMoreListener);
        rcy_blog.setAdapter(blogAdapter);


    }

    @Override
    public void onLoadMore() {
        if (blList.size() > 8) {
            page_no = page_no + 1;
            hitBlogApi();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }
}

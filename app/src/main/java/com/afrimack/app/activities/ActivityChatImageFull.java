package com.afrimack.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class ActivityChatImageFull extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_full, img_back, img_dot_chat;
    private Context context = this;
    private TextView tv_titel_chat;
    ProgressBar full_image_pb_progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_image_full);

        img_back = (ImageView) findViewById(R.id.img_back_chat);
        tv_titel_chat = (TextView) findViewById(R.id.tv_titel_chat);
        img_dot_chat = (ImageView) findViewById(R.id.img_dot_chat);
        full_image_pb_progressbar = (ProgressBar) findViewById(R.id.full_image_pb_progressbar);
        img_dot_chat.setVisibility(View.GONE);
        img_back.setOnClickListener(this);

        tv_titel_chat.setText(R.string.app_name);
        iv_full = (ImageView) findViewById(R.id.iv_full);
        full_image_pb_progressbar.setVisibility(View.VISIBLE);

        loadImage(context,Config.getFullImageString(context),iv_full,full_image_pb_progressbar);

    /*    try {
            Glide.with(context)
                    .load(Config.getFullImageString(context))
                   .asBitmap()
                   // .asGif()
                   // .placeholder(R.raw.loading_icon_new)
                 //   .placeholder(R.drawable.loadin_product)
                    .into(iv_full);



            Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    full_image_pb_progressbar.setVisibility(View.GONE);
                }
            },2000);
            //full_image_pb_progressbar.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    public void loadImage(Context context, String url, ImageView img, final ProgressBar eProgressBar) {

        eProgressBar.setVisibility(View.VISIBLE);
        Glide.with(context)
                .load(url)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        eProgressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        eProgressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .crossFade()
              //  .error(R.drawable.placeholder_image)
                .into(img);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_chat:
                ActivityChatImageFull.this.finish();
                break;
        }
    }
}

package com.afrimack.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afrimack.app.utils.Common.displayErrorDialog;

public class ActivityForgot extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel;

    public EditText et_email_forgot;
    private Button btn_reset_forgot;
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private String TAG = ActivityForgot.class.getSimpleName();
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        init();

    }

    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);

        tv_titel = (TextView) findViewById(R.id.tv_titel);

        img_back.setOnClickListener(this);
        tv_titel.setText(getString(R.string.app_titel).toString());

        //************end titel*****************//

        et_email_forgot = (EditText) findViewById(R.id.et_email_forgot);
        btn_reset_forgot = (Button) findViewById(R.id.btn_reset_forgot);
        btn_reset_forgot.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == img_back) {
            onBackPressed();
        }
        if (v == btn_reset_forgot) {
            v.startAnimation(AppConstants.buttonClick);

            String email = et_email_forgot.getText().toString().trim();
            if (email.length() == 0) {

                displayErrorDialog(getResources().getString(R.string.fill_email), this);
            } else if (!email.matches(EMAIL_REGEX)) {

                displayErrorDialog(getResources().getString(R.string.invalid_email), this);
            } else {
                if (ConnectionDetector.isNetAvail(this)) {
                    hitApi(email);
                } else {
                    displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                }
            }
        }
    }

    private void hitApi(String email) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            Call<CommanResponseModel> call = apiInterface.Forgotpassword(email);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel model = response.body();
                        Common.displayLongToast(context, model.getMessage());
                        if (model.isError() == false) {
                            finish();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


}

package com.afrimack.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.ChooseCategoriesListAdapter;
import com.afrimack.app.utils.CommonUtils;
import com.facebook.share.widget.JoinAppGroupDialog;

public class ActivityCategoryListDialog extends Activity {

    RecyclerView recycler_categoryList;
    TextView tv_cteagr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_category_list_dialog);

        init();
    }

    public void init(){
        recycler_categoryList=(RecyclerView)findViewById(R.id.recycler_categoryList);

        ChooseCategoriesListAdapter chooseCategoriesListAdapter = new ChooseCategoriesListAdapter(this,CommonUtils.getCategories());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycler_categoryList.setLayoutManager(mLayoutManager);
        recycler_categoryList.setAdapter(chooseCategoriesListAdapter);

    /*    tv_cteagr=(TextView)((Activity)).findViewById(R.id.tv_cteagr);
        tv_cteagr.setText("Afrimack");*/

    }
}

package com.afrimack.app.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.afrimack.app.R;
import com.afrimack.app.adapters.FilterAdapter;
import com.afrimack.app.adapters.SpicelAdapter;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CategoriesMode;
import com.afrimack.app.models.FilterResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.services.Locationservice;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.CommonUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.maps.model.BitmapDescriptorFactory.defaultMarker;

public class ActivityFilter extends AppCompatActivity implements View.OnClickListener,
        OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static List<FilterResponseModel.ResponseBean.CategoryBean> stList;
    public static List<FilterResponseModel.ResponseBean.SpecialBean> sptList;
    String listcheck = "Yes";
    private ImageView img_back, img_tick;
    private GridLayoutManager lLayout, cate_layoutmanager;
    private RadioButton radio_distance, radio_date, radio_price_order, radio_price_desorder;
    private LinearLayout lay_distance, lay_date, lay_price_order, lay_price_desorder;
    private TextView tv_seek_list_show, tv_sort_value, tv_titel;
    private TextView tv_min_price, tv_max_price;
    private SeekBar seek_bar_list, seek_sort;
    private ToggleButton tb_country;
    private String last_days = "24 hours", sorting = "", radius, own_country = "no", country, special_category, category, min_price, max_price;
    private FilterResponseModel model;
    private List<FilterResponseModel> filterListItem;
    private FilterAdapter filterAdapter;
    private SpicelAdapter spicelAdapter;
    private Button btn_apply, btn_clear_filter;
    private RecyclerView recy_spicle_categotery, recyclerView;
    private static GoogleMap mMap;
    private static double lat;
    private static double lng;
    private static Marker marker;
    private LinearLayout ll_map;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    private Context context = this;
    private static String whereFrom = "";
    public static String TAG = ActivityFilter.class.getSimpleName();
    private AlertDialog gpsAlertDialog;
    private boolean isGpsDialogShowing = false;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        whereFrom = "";
        hitCategoryListApi();

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            ShowGpsDialog();
            if (!isMyServiceRunning()) {
                startService(new Intent(ActivityFilter.this, Locationservice.class));
            }
        } else {
            removeGpsDialog();
            try {
                lat = Double.parseDouble(Config.getlatitude(this));
                lng = Double.parseDouble(Config.getlongitude(this));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        init();
        setdata();

        lLayout = new GridLayoutManager(this, 2);

        recyclerView = (RecyclerView) findViewById(R.id.recy_reviews_filter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lLayout);

        cate_layoutmanager = new GridLayoutManager(this, 2);
        recy_spicle_categotery.setHasFixedSize(true);
        recy_spicle_categotery.setLayoutManager(cate_layoutmanager);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_filter);

        mapFragment.getMapAsync(this);


    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getPackageName() + ".services.Locationservice").equals(service1.service.getClassName())) {

                return true;
            }
        }
        return false;
    }

    private void hitCategoryListApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<FilterResponseModel> call = apiInterface.FilterCategory();
            call.enqueue(new Callback<FilterResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<FilterResponseModel> call, @NonNull Response<FilterResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        FilterResponseModel model = response.body();

                        if (model.isError() == false) {
                            filterListItem = new ArrayList<FilterResponseModel>();
                            filterAdapter = new FilterAdapter(context, model.getResponse().getCategory(), false, listcheck);
                            recyclerView.setAdapter(filterAdapter);
                            filterAdapter.notifyDataSetChanged();
                            if (model.getResponse().getSpecial().size() > 0) {
                                spicelAdapter = new SpicelAdapter(context, model.getResponse().getSpecial(), false, listcheck);
                                recy_spicle_categotery.setAdapter(spicelAdapter);
                                spicelAdapter.notifyDataSetChanged();
                            }
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<FilterResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void init() {
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getResources().getString(R.string.filter));

        radio_distance = (RadioButton) findViewById(R.id.radio_distance);
        lay_distance = (LinearLayout) findViewById(R.id.lay_distance);
        radio_date = (RadioButton) findViewById(R.id.radio_date);
        lay_date = (LinearLayout) findViewById(R.id.lay_date);
        radio_price_order = (RadioButton) findViewById(R.id.radio_price_order);
        lay_price_order = (LinearLayout) findViewById(R.id.lay_price_order);
        radio_price_desorder = (RadioButton) findViewById(R.id.radio_price_desorder);
        lay_price_desorder = (LinearLayout) findViewById(R.id.lay_price_desorder);
        tv_seek_list_show = (TextView) findViewById(R.id.tv_seek_list_show);
        seek_bar_list = (SeekBar) findViewById(R.id.seek_bar_list);
        tv_sort_value = (TextView) findViewById(R.id.tv_sort_value);
        seek_sort = (SeekBar) findViewById(R.id.seek_sort);
        tb_country = (ToggleButton) findViewById(R.id.tb_country);
        tv_min_price = (TextView) findViewById(R.id.tv_min_price);
        tv_max_price = (TextView) findViewById(R.id.tv_max_price);
        recy_spicle_categotery = (RecyclerView) findViewById(R.id.recy_spicle_categotery);
        btn_apply = (Button) findViewById(R.id.btn_apply);
        btn_clear_filter = (Button) findViewById(R.id.btn_clear_filter);

        btn_apply.setOnClickListener(this);
        btn_clear_filter.setOnClickListener(this);

        ll_map = (LinearLayout) findViewById(R.id.ll_map);
        ll_map.setOnClickListener(this);


    }


    private void setdata() {

        if (Config.getSortBy(this) != null && !Config.getSortBy(this).equalsIgnoreCase("")) {
            sorting = Config.getSortBy(this);
        }
        if (Config.getSortDay(this) != null && !Config.getSortDay(this).equalsIgnoreCase("")) {
            last_days = Config.getSortDay(this);

            if (last_days.equalsIgnoreCase("24 hours")) {
                seek_bar_list.setProgress(0);
                seekday(0);
            }
            if (last_days.equalsIgnoreCase("3 days")) {
                seekday(1);
                seek_bar_list.setProgress(1);
            }
            if (last_days.equalsIgnoreCase("7 days")) {
                seekday(2);
                seek_bar_list.setProgress(2);
                //tv_seek_list_show.setText(last_days);*/
            }
            if (last_days.equalsIgnoreCase("14 days")) {
                seekday(3);
                seek_bar_list.setProgress(3);
                //tv_seek_list_show.setText(last_days);*/
            }
            if (last_days.equalsIgnoreCase("30 days")) {
                seekday(4);
                seek_bar_list.setProgress(4);
                // tv_seek_list_show.setText(last_days);*/
            }
            if (last_days.equalsIgnoreCase("forever")) {
                seekday(5);
                seek_bar_list.setProgress(5);
                //tv_seek_list_show.setText(last_days);*/
            }
        } else {
            seek_bar_list.setProgress(100);
            last_days = "Everywhere";
            tv_seek_list_show.setText(last_days);
        }

        if (sorting.equalsIgnoreCase("distance")) {
            radio_distance();
            seek_sort.setProgress(100);

        } else if (sorting.equalsIgnoreCase("date")) {
            radio_date();
           // seek_sort.setProgress(9);
            seek_sort.setProgress(100);
           // tv_sort_value.setText("30 km");
        } else if (sorting.equalsIgnoreCase("price-low")) {
            radio_price_order();
            seek_sort.setProgress(100);
           /* seek_sort.setProgress(9);
            tv_sort_value.setText("30 km");*/

        } else if (sorting.equalsIgnoreCase("price-high")) {
            radio_price_desorder();
            seek_sort.setProgress(100);
            /*seek_sort.setProgress(9);
            tv_sort_value.setText("30 km");*/
        } else {
           // radio_distance();
            seek_sort.setProgress(100);
        }

        if (Config.getSortRadius(this) != null && !Config.getSortRadius(this).equalsIgnoreCase("")) {
            radius = Config.getSortRadius(this);
            if (radius.equalsIgnoreCase("1 km")) {
                seek_sortradius(0);
                seek_sort.setProgress(0);
            } else if (radius.equalsIgnoreCase("2 km")) {
                seek_sortradius(1);
                seek_sort.setProgress(1);
            } else if (radius.equalsIgnoreCase("3 km")) {
                seek_sortradius(2);
                seek_sort.setProgress(2);
            } else if (radius.equalsIgnoreCase("4 km")) {
                seek_sortradius(3);
                seek_sort.setProgress(3);
            } else if (radius.equalsIgnoreCase("5 km")) {
                seek_sortradius(4);
                seek_sort.setProgress(4);
            } else if (radius.equalsIgnoreCase("7 km")) {
                seek_sortradius(5);
                seek_sort.setProgress(5);
            } else if (radius.equalsIgnoreCase("10 km")) {
                seek_sortradius(6);
                seek_sort.setProgress(6);
            } else if (radius.equalsIgnoreCase("15 km")) {
                seek_sortradius(7);
                seek_sort.setProgress(7);
            } else if (radius.equalsIgnoreCase("30 km")) {
                seek_sortradius(8);
                seek_sort.setProgress(8);
            } else if (radius.equalsIgnoreCase("60 km")) {
                seek_sortradius(9);
                seek_sort.setProgress(9);
            } else if (radius.equalsIgnoreCase("100 km")) {
                seek_sortradius(10);
                seek_sort.setProgress(10);
            } else if (radius.equalsIgnoreCase("200 km")) {
                seek_sortradius(11);
                seek_sort.setProgress(11);
            } else if (radius.equalsIgnoreCase("300 km")) {
                seek_sortradius(12);
                seek_sort.setProgress(12);
            } else if (radius.equalsIgnoreCase("400 km")) {
                seek_sortradius(13);
                seek_sort.setProgress(13);
            } else if (radius.equalsIgnoreCase("500 km")) {
                seek_sortradius(14);
                seek_sort.setProgress(14);
            } else if (radius.equalsIgnoreCase("1000 km")) {
                seek_sortradius(15);
                seek_sort.setProgress(15);
            } else {
                seek_sortradius(16);
                seek_sort.setProgress(16);
            }
        } else {
            seek_sortradius(16);
            seek_sort.setProgress(16);
        }

        if (Config.getSortMinPrice(this) != null && !Config.getSortMinPrice(this).equalsIgnoreCase("")) {
            tv_min_price.setText(Config.getSortMinPrice(this));
        }
        if (Config.getSortMaxPrice(this) != null && !Config.getSortMaxPrice(this).equalsIgnoreCase("")) {
            tv_max_price.setText(Config.getSortMaxPrice(this));
        }
        if (Config.getSortOwnCountry(this).equalsIgnoreCase("yes")) {
            tb_country.setChecked(true);
            own_country = "yes";
        }


        lay_distance.setOnClickListener(this);
        lay_date.setOnClickListener(this);
        lay_price_order.setOnClickListener(this);
        lay_price_desorder.setOnClickListener(this);

        radio_distance.setOnClickListener(this);
        radio_date.setOnClickListener(this);
        radio_price_order.setOnClickListener(this);
        radio_price_desorder.setOnClickListener(this);
        tb_country.setOnClickListener(this);
        seek_bar_list.setMax(5);
        seek_bar_list.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekday(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seek_sort.setMax(16);
        seek_sort.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                seek_sortradius(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }


    @Override
    public void onClick(View v) {
        if (v == img_back) {
            v.startAnimation(AppConstants.buttonClick);
            onBackPressed();
        }
        if ((v == img_tick)) {
            v.startAnimation(AppConstants.buttonClick);
            applbutton();

        }
        if (v == lay_distance) {
            v.startAnimation(AppConstants.buttonClick);
            radio_distance();

        }
        if (v == lay_date) {
            v.startAnimation(AppConstants.buttonClick);
            radio_date();
        }

        if (v == lay_price_order) {
            v.startAnimation(AppConstants.buttonClick);
            radio_price_order();
        }
        if (v == lay_price_desorder) {
            v.startAnimation(AppConstants.buttonClick);
            radio_price_desorder();
        }
        if (v == radio_distance) {
            v.startAnimation(AppConstants.buttonClick);
            radio_distance();
        }
        if (v == radio_date) {
            v.startAnimation(AppConstants.buttonClick);
            radio_date();
        }
        if (v == radio_price_order) {
            v.startAnimation(AppConstants.buttonClick);
            radio_price_order();
        }
        if (v == radio_price_desorder) {
            v.startAnimation(AppConstants.buttonClick);
            radio_price_desorder();
        }

        if (v == tb_country) {

            if (tb_country.isChecked()) {
                own_country = "yes";
            } else {
                own_country = "no";
            }
        }
        if (v == btn_apply) {
            v.startAnimation(AppConstants.buttonClick);
            applbutton();
        }
        if (v == btn_clear_filter) {
            v.startAnimation(AppConstants.buttonClick);

            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            listcheck = "NO";
            seek_bar_list.setProgress(0);
            last_days = "24 hours";
            tv_seek_list_show.setText(last_days);

            seek_sort.setProgress(100);
            radius = "Everywhere";
            radio_distance();
            tb_country.setChecked(false);


            tv_min_price.setText("");
            tv_max_price.setText("");

            if (tb_country.isChecked()) {
                own_country = "yes";
            } else {
                own_country = "no";
            }

            hitCategoryListApi();

        }
        if (v == ll_map) {
            Intent intent = new Intent(ActivityFilter.this, FullMapActivity.class);
            startActivity(intent);
        }
    }

    private void applbutton() {


        try {
            Config.setlatitude(String.valueOf(lat), this);
            Config.setlongitude(String.valueOf(lng), this);
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
            listcheck = "Yes";
            String data = "";
            String spdata = "";
            stList = ((FilterAdapter) filterAdapter)
                    .getStudentist();
            for (int i = 0; i < stList.size(); i++) {
                FilterResponseModel.ResponseBean.CategoryBean singleStudent = stList.get(i);
                if (singleStudent.isSelected() == true) {

                    //  data = data + "\n" + singleStudent.getId();
                    if (data.length() == 0) {
                        data = String.valueOf(singleStudent.getId());
                    } else {
                        data = data + "," + singleStudent.getId();
                    }

                }

            }

            sptList = ((SpicelAdapter) spicelAdapter)
                    .getSPtist();

            for (int i = 0; i < sptList.size(); i++) {
                FilterResponseModel.ResponseBean.SpecialBean singleSPL = sptList.get(i);
                if (singleSPL.isSelected() == true) {

                    //  data = data + "\n" + singleStudent.getId();
                    if (spdata.length() == 0) {
                        spdata = String.valueOf(singleSPL.getId());
                    } else {
                        spdata = spdata + "," + singleSPL.getId();
                    }
                }
            }

            category = data;
            special_category = spdata;

            last_days = tv_seek_list_show.getText().toString().trim();
            if (last_days.equalsIgnoreCase("24 hours")) {
                Config.setSortDay("", this);
            } else {
                Config.setSortDay(last_days, this);
            }
            radius = tv_sort_value.getText().toString().trim();
            if (radius.equalsIgnoreCase("Everywhere")) {
                Config.setSortRadius("", this);
            } else {
                Config.setSortRadius(radius, this);
            }
            // special_category,category;
            min_price = tv_min_price.getText().toString().trim();
            if (min_price.equalsIgnoreCase("")) {
                Config.setSortMinPrice("", this);
            } else {
                Config.setSortMinPrice(min_price, this);
            }
            max_price = tv_max_price.getText().toString().trim();
            if (max_price.equalsIgnoreCase("")) {
                Config.setSortMaxPrice("", this);
            } else {
                Config.setSortMaxPrice(max_price, this);
            }
            if (category.equalsIgnoreCase("")) {
                Config.setSortCategory(category, this);
            } else {
                Config.setSortCategory(category, this);
            }
            if (special_category.equalsIgnoreCase("")) {
                Config.setSortSpecialCategory("", this);
            } else {
                Config.setSortSpecialCategory(special_category, this);
            }

            if (sorting.equalsIgnoreCase("distance")) {
                Config.setSortBy("", this);
            } else {
                Config.setSortBy(sorting, this);
            }

            Config.setSortOwnCountry(own_country, this);
            String filter_type = "Yes";
            Config.setFilter(filter_type, this);

            Intent intent = new Intent(this, ActivityHome.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {

        }


    }


    private void radio_distance() {
        lay_distance.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));
        lay_date.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_price_order.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_price_desorder.setBackgroundColor(this.getResources().getColor(R.color.border_gray));

        radio_distance.setTextColor(this.getResources().getColor(R.color.color_black));
        radio_date.setTextColor(this.getResources().getColor(R.color.dark_gray));
        radio_price_order.setTextColor(this.getResources().getColor(R.color.dark_gray));
        radio_price_desorder.setTextColor(this.getResources().getColor(R.color.dark_gray));

        radio_distance.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));
        radio_date.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_price_order.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_price_desorder.setBackgroundColor(this.getResources().getColor(R.color.border_gray));

        radio_distance.setChecked(true);
        radio_date.setChecked(false);
        radio_price_order.setChecked(false);
        radio_price_desorder.setChecked(false);

        sorting = "distance";
        seek_sort.setProgress(16);
        radius = "Everywhere";
        tv_sort_value.setText(radius);


    }

    private void radio_date() {
        lay_distance.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_date.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));
        lay_price_order.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_price_desorder.setBackgroundColor(this.getResources().getColor(R.color.border_gray));

        radio_distance.setTextColor(this.getResources().getColor(R.color.dark_gray));
        radio_date.setTextColor(this.getResources().getColor(R.color.color_black));
        radio_price_order.setTextColor(this.getResources().getColor(R.color.dark_gray));
        radio_price_desorder.setTextColor(this.getResources().getColor(R.color.dark_gray));

        radio_distance.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_date.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));
        radio_price_order.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_price_desorder.setBackgroundColor(this.getResources().getColor(R.color.border_gray));

        radio_distance.setChecked(false);
        radio_date.setChecked(true);
        radio_price_order.setChecked(false);
        radio_price_desorder.setChecked(false);

        sorting = "date";
        seek_sort.setProgress(100);
        tv_sort_value.setText("Everywhere");
      //  seek_sort.setProgress(8);
      //  tv_sort_value.setText("30 km");

    }

    private void radio_price_order() {
        lay_distance.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_date.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_price_order.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));
        lay_price_desorder.setBackgroundColor(this.getResources().getColor(R.color.border_gray));

        radio_distance.setTextColor(this.getResources().getColor(R.color.color_white));
        radio_date.setTextColor(this.getResources().getColor(R.color.color_white));
        radio_price_order.setTextColor(this.getResources().getColor(R.color.color_black));
        radio_price_desorder.setTextColor(this.getResources().getColor(R.color.color_white));

        radio_distance.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_date.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_price_order.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));
        radio_price_desorder.setBackgroundColor(this.getResources().getColor(R.color.border_gray));

        radio_distance.setChecked(false);
        radio_date.setChecked(false);
        radio_price_order.setChecked(true);
        radio_price_desorder.setChecked(false);

        sorting = "price-low";
      /*  seek_sort.setProgress(8);
        tv_sort_value.setText("30 km");*/
        seek_sort.setProgress(100);
        tv_sort_value.setText("Everywhere");
    }

    private void radio_price_desorder() {
        lay_distance.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_date.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_price_order.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        lay_price_desorder.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));

        radio_distance.setTextColor(this.getResources().getColor(R.color.dark_gray));
        radio_date.setTextColor(this.getResources().getColor(R.color.dark_gray));
        radio_price_order.setTextColor(this.getResources().getColor(R.color.dark_gray));
        radio_price_desorder.setTextColor(this.getResources().getColor(R.color.color_black));

        radio_distance.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_date.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_price_order.setBackgroundColor(this.getResources().getColor(R.color.border_gray));
        radio_price_desorder.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));

        radio_distance.setChecked(false);
        radio_date.setChecked(false);
        radio_price_order.setChecked(false);
        radio_price_desorder.setChecked(true);

        sorting = "price-high";
       /* seek_sort.setProgress(8);
        tv_sort_value.setText("30 km");*/
        seek_sort.setProgress(100);
        tv_sort_value.setText("Everywhere");
    }


    private void seekday(int progress) {
        if (progress == 0) {
            last_days = "24 hours";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 1) {
            last_days = "3 days";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 2) {
            last_days = "7 days";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 3) {
            last_days = "14 days";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 4) {
            last_days = "30 days";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 5) {
            last_days = "forever";
            tv_seek_list_show.setText(last_days);
        } else {
            last_days = "24 hours";
            tv_seek_list_show.setText(last_days);
        }

    }

    private void seek_sortradius(int progress) {
        if (progress == 0) {
            radius = "1" + " km";
            tv_sort_value.setText(radius);
        } else if (progress <= 4) {
            radius = String.valueOf(progress) + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 5) {
            radius = "7" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 6) {
            radius = "10" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 7) {
            radius = "15" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 8) {
            radius = "30" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 9) {
            radius = "60" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 10) {
            radius = "100" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 11) {
            radius = "200" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 12) {
            radius = "300" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 13) {
            radius = "400" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 14) {
            radius = "500" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 15) {
            radius = "1000" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 16) {
            radius = "Everywhere";
            tv_sort_value.setText(radius);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }


        mMap.setOnMapClickListener(this);


    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        if (!whereFrom.equalsIgnoreCase("FullMap")) {

            mLastLocation = location;
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }

            //Place current location marker
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            lat = location.getLatitude();
            lng = location.getLongitude();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            marker = mMap.addMarker(markerOptions);

            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
        }

    }

    private void ShowGpsDialog() {
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                ActivityFilter.this);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("No GPS")
                .setMessage(getString(R.string.gps_info))
                .setPositiveButton(getString(R.string.gps_seating),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                gpsAlertDialog.dismiss();
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeGpsDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                // finish();
                            }
                        });

        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;

        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(ActivityFilter.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onMapClick(LatLng latLng) {
        Intent intent = new Intent(ActivityFilter.this, FullMapActivity.class);
        intent.putExtra("latitude", lat);
        intent.putExtra("longitude", lng);
        startActivity(intent);

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        //Toast.makeText(getApplicationContext(), "onMarkerDrag..!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng dragPosition = marker.getPosition();
        double dragLat = dragPosition.latitude;
        double dragLong = dragPosition.longitude;

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
    }


    public static void setnewlocation(String from, LatLng latLng) {
        whereFrom = from;
        lat = latLng.latitude;
        lng = latLng.longitude;
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        if (marker != null) {
            marker.remove();
        }
        marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Post Location").icon(defaultMarker(BitmapDescriptorFactory.HUE_RED)));


    }
}

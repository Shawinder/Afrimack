package com.afrimack.app.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.BuildConfig;
import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.ProfileStatusResponseModel;
import com.afrimack.app.rest.RetrofitUtils;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.afrimack.app.utils.Common.checkPermissions;
import static com.afrimack.app.utils.Common.displayErrorDialog;

public class ActivityProfileStatus extends BaseActivity implements View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel, profile_status_info;

    private Spinner spi_gender, spi_profile_type;
    private View lay_dob;
    private TextView tv_date_of_birth;
    private TextView tv_user_email;
    private ImageView login_fb, login_gp, login_normal;

    Calendar c;
    Calendar myCalendar = Calendar.getInstance();
    private String formattedDate;
    public static Bitmap thumbnail;

    private String user_id, type, value;
    private ImageView img_user_profile,img_status_circle;
    private static final String IMAGE_DIRECTORY_NAME = "Afrimack";
    private Uri photoUri;
    private PopupWindow popupWindow;


    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static BitmapFactory.Options bitmap_factory;
    private static Uri uri;
    private static Bitmap bm;
    private String selectedImagePath = null;
    private String imgString = null, imgString1;
    private ByteArrayOutputStream stream;
    byte[] imageInByte;

    private Dialog dialog;
    private EditText et_new_email;
    private Button cancel_email, btn_ok;
    private TextView profile_status_text;
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private String newEmail;
    private List<String> gender;
    private ArrayAdapter<String> genderAdapter;
    private ArrayAdapter<String> profileTypeAdapter;
    private String UserEmail = "";

    private Button btn_update;
    private String image, profile_type, gender_u, dob;
    String profiltT;

   // PieChart pieChart;
    ArrayList<Integer> PieEntryLabels;
    ArrayList<PieEntry> yEntrys;
    private Context context = this;
    private String TAG = ActivityProfileStatus.class.getSimpleName();

    public static final int MY_PERMISSIONS_REQUEST_READ_CAMERA = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_status);
        user_id = Config.getUserId(this);
        init();
        setdata();
        hitDataApi();
        c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c.getTime());


        spi_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    value = "Choose";
                    spi_gender.setSelection(position);

                    type = "gender";
                    gender_u = value;
                } else if (position == 1) {
                    value = "M";
                    spi_gender.setSelection(position);

                    type = "gender";
                    gender_u = value;
                } else if (position == 2) {
                    value = "F";
                    gender_u = value;
                    spi_gender.setSelection(position);
                    type = "gender";
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spi_profile_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                profiltT = parent.getItemAtPosition(position).toString();
                if (position == 0) {
                    value = "0";
                    spi_profile_type.setSelection(position);
                    profile_type = value;
                    type = "profile_type";

                } else if (position == 1) {
                    value = "I";
                    spi_profile_type.setSelection(position);
                    profile_type = value;
                    type = "profile_type";

                } else if (position == 2) {
                    value = "E";
                    profile_type = value;
                    spi_profile_type.setSelection(position);

                    type = "profile_type";
                } else if (position == 3) {
                    value = "Y";
                    profile_type = value;
                    spi_profile_type.setSelection(position);

                    type = "profile_type";
                } else if (position == 4) {
                    value = "A";
                    profile_type = value;
                    spi_profile_type.setSelection(position);
                    type = "profile_type";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void hitDataApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ProfileStatusResponseModel> call = apiInterface.ProfileStatus(user_id);
            call.enqueue(new Callback<ProfileStatusResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<ProfileStatusResponseModel> call, @NonNull Response<ProfileStatusResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        ProfileStatusResponseModel mode = response.body();
                        if (mode.getCode() == 0) {
                            if (mode.isError() == false) {
                                if (mode.getResponse().getEmail() != null) {
                                    tv_user_email.setText(mode.getResponse().getEmail());
                                    UserEmail = mode.getResponse().getEmail();
                                }


                                if (mode.getResponse().getGender() != null && mode.getResponse().getGender().equalsIgnoreCase("M")) {
                                    spi_gender.setPrompt("MALE");
                                    spi_gender.setSelection(1);

                                } else if (mode.getResponse().getGender() != null && mode.getResponse().getGender().equalsIgnoreCase("F")) {
                                    spi_gender.setPrompt("FEMALE");
                                    spi_gender.setSelection(2);

                                } else {
                                    spi_gender.setPrompt("Choose");
                                    spi_gender.setSelection(0);
                                }

                                if (mode.getResponse().getProfile_type().equalsIgnoreCase("I")) {
                                    spi_profile_type.setSelection(1);

                                } else if (mode.getResponse().getProfile_type().equalsIgnoreCase("E")) {
                                    spi_profile_type.setSelection(2);

                                } else if (mode.getResponse().getProfile_type().equalsIgnoreCase("Y")) {
                                    spi_profile_type.setSelection(3);

                                } else if (mode.getResponse().getProfile_type().equalsIgnoreCase("A")) {
                                    spi_profile_type.setSelection(4);

                                } else if (mode.getResponse().getProfile_type().equalsIgnoreCase("N")) {
                                    spi_profile_type.setSelection(5);

                                } else {
                                    spi_profile_type.setSelection(0);
                                }


                                if (mode.getResponse().getDob() != null) {
                                    tv_date_of_birth.setText(mode.getResponse().getDob());

                                }

                                if (mode.getResponse().getFb_id().equalsIgnoreCase("Y")) {
                                    login_fb.setVisibility(View.VISIBLE);

                                }
                                if (mode.getResponse().getGoogle_id().equalsIgnoreCase("Y")) {
                                    login_gp.setVisibility(View.VISIBLE);

                                }
                                if (mode.getResponse().getNo_verified().equalsIgnoreCase("Y")) {
                                    login_normal.setVisibility(View.VISIBLE);
                                }


                                if (mode.getResponse().getImage() != null) {

                                    Picasso.with(context)
                                            .load(mode.getResponse().getImage())
                                            .placeholder(R.drawable.user_round)
                                            .into(img_user_profile);
                                }


                                if ((mode.getResponse().getPercentage().getCompletion() != 0)) {
                                    if (mode.getResponse().getPercentage().getCompletion()<=25)
                                    {
                                        img_status_circle.setBackgroundResource(R.drawable.status_circle_25);
                                    }
                                    else  if (mode.getResponse().getPercentage().getCompletion()<=50 && mode.getResponse().getPercentage().getCompletion()>25)
                                    {
                                        img_status_circle.setBackgroundResource(R.drawable.status_circle_50);
                                    }

                                    else  if (mode.getResponse().getPercentage().getCompletion()<=75 && mode.getResponse().getPercentage().getCompletion()>50)
                                    {
                                        img_status_circle.setBackgroundResource(R.drawable.status_circle_75);
                                    }
                                    else  if (mode.getResponse().getPercentage().getCompletion()<100 && mode.getResponse().getPercentage().getCompletion()>75)
                                    {
                                        img_status_circle.setBackgroundResource(R.drawable.status_circle_75);
                                    }
                                    else  if (mode.getResponse().getPercentage().getCompletion() == 100) {
                                        profile_status_text.setText("Your profile is complete");
                                        img_status_circle.setBackgroundResource(R.drawable.status_circle_100);
                                    }
                                   // PieEntryLabels.add(mode.getResponse().getPercentage().getCompletion());
                                }
                               /* if (mode.getResponse().getPercentage().getDob() != 0) {
                                  //  PieEntryLabels.add(15);
                                   img_status_circle.setBackgroundResource(R.drawable.status_circle_50);
                                }
                                if ((mode.getResponse().getPercentage().getImage() != 0)) {
                                   // PieEntryLabels.add(15);
                                    img_status_circle.setBackgroundResource(R.drawable.status_circle_50);
                                }
                                if ((mode.getResponse().getPercentage().getSocial() != 0)) {
                                   // PieEntryLabels.add(25);
                                    img_status_circle.setBackgroundResource(R.drawable.status_circle_75);
                                }
                                if ((mode.getResponse().getPercentage().getEmail() != 0)) {
                                  //  PieEntryLabels.add(15);
                                    img_status_circle.setBackgroundResource(R.drawable.status_circle_50);
                                }
                                if ((mode.getResponse().getPercentage().getGender() != 0)) {
                                   // PieEntryLabels.add(15);
                                   img_status_circle.setBackgroundResource(R.drawable.status_circle_50);
                                }
                                if ((mode.getResponse().getPercentage().getProfile_type() != 0)) {
                                   // PieEntryLabels.add(15);
                                    img_status_circle.setBackgroundResource(R.drawable.status_circle_50);
                                }*/

                                //addDataSet();
                            }

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ProfileStatusResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.profile_status).toString());
        img_back.setOnClickListener(this);
        //************end titel*****************//
        profile_status_text = (TextView) findViewById(R.id.profile_status_text);
        profile_status_info = (TextView) findViewById(R.id.profile_status_info);
        profile_status_info.setVisibility(View.GONE);
        spi_gender = (Spinner) findViewById(R.id.spi_gender);
        spi_profile_type = (Spinner) findViewById(R.id.spi_profile_type);
        lay_dob = findViewById(R.id.lay_dob);
        tv_date_of_birth = (TextView) findViewById(R.id.tv_date_of_birth);
        tv_user_email = (TextView) findViewById(R.id.tv_user_email);
        login_fb = (ImageView) findViewById(R.id.login_fb);
        login_gp = (ImageView) findViewById(R.id.login_gp);
        img_status_circle = (ImageView) findViewById(R.id.img_status_circle);
        login_normal = (ImageView) findViewById(R.id.login_normal);
        img_user_profile = (ImageView) findViewById(R.id.img_user_profile);

        lay_dob.setOnClickListener(this);
        // tv_user_email.setOnClickListener(this);

        img_user_profile.setOnClickListener(this);

        btn_update = (Button) findViewById(R.id.btn_update);
        btn_update.setText("APPLY");
        btn_update.setOnClickListener(this);

      /*  pieChart = (PieChart) findViewById(R.id.idPieChart);

        pieChart.setDescription("");
        pieChart.setRotationEnabled(false);
        pieChart.setHoleRadius(0f);
        pieChart.setTransparentCircleAlpha(0);
        PieEntryLabels = new ArrayList<Integer>();
        yEntrys = new ArrayList<>();*/
    }

    private void addDataSet() {

        ArrayList<String> xEntrys = new ArrayList<>();

        for (int i = 0; i < PieEntryLabels.size(); i++) {
            yEntrys.add(new PieEntry(PieEntryLabels.get(i), i));
        }


        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setSliceSpace(0);
        pieDataSet.setValueTextSize(0);

        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(getResources().getColor(R.color.color_green));
        //colors.add(Color.BLUE);
        colors.add(Color.GRAY);
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        colors.add(Color.CYAN);
        colors.add(Color.YELLOW);
        colors.add(Color.MAGENTA);

        pieDataSet.setColors(colors);


        //create pie data object
        PieData pieData = new PieData(pieDataSet);
       // pieChart.setData(pieData);
        //pieChart.invalidate();

    }

    private void setdata() {
        gender = new ArrayList<String>();
        gender.add("Choose");
        gender.add("Male");
        gender.add("Female");
        // gender.add("Other");

        genderAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout_view_normal, gender);
        genderAdapter.setDropDownViewResource(R.layout.spinner_text_view);
        spi_gender.setAdapter(genderAdapter);

        List<String> profileType = new ArrayList<String>();
        profileType.add("Choose");
        profileType.add("Individual");
        profileType.add("Entrepreneur");
        profileType.add("Young Designer");
        profileType.add("Artist");
        // profileType.add("None");


        profileTypeAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout_view_normal, profileType);
        profileTypeAdapter.setDropDownViewResource(R.layout.spinner_text_view);
        spi_profile_type.setAdapter(profileTypeAdapter);
    }

    @Override
    public void onClick(View v) {

        if (v == img_back) {
            onBackPressed();
        }
        if (v == lay_dob) {
            new DatePickerDialog(this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
        if (v == tv_user_email) {

            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dalog_email);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            et_new_email = (EditText) dialog.findViewById(R.id.et_new_email);
            cancel_email = (Button) dialog.findViewById(R.id.cancel_email);
            btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
            et_new_email.setText(UserEmail);
            cancel_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newEmail = et_new_email.getText().toString().trim();

                    if (newEmail.length() == 0) {
                        Common.displayLongToast(ActivityProfileStatus.this, getResources().getString(R.string.fill_email));

                    } else if (!newEmail.matches(EMAIL_REGEX)) {

                        Common.displayLongToast(ActivityProfileStatus.this, getResources().getString(R.string.invalid_email));
                    } else {
                        dialog.dismiss();
                        value = et_new_email.getText().toString().trim();
                        type = "email";
                        //cellUplodeSetting(type,value);
                        tv_user_email.setText(value);
                        et_new_email.setText(value);
                    }
                }
            });
            dialog.show();

        }
        if (v == img_user_profile) {

            if (Build.VERSION.SDK_INT>=23){


            if (checkPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, 99)) {

                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.camera_dalog, null);
                Button image_camera = (Button) popupView.findViewById(R.id.image_camera);
                Button image_gallery = (Button) popupView.findViewById(R.id.image_gallery);

                image_camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        captureImageFromCamera();
                        popupWindow.dismiss();
                    }
                });
                image_gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                        captureImageFromGallery();
                    }
                });

                popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setOutsideTouchable(true);
                popupWindow.showAsDropDown(img_user_profile);

            }
            else {

                ActivityCompat.requestPermissions(ActivityProfileStatus.this,
                        new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_CAMERA);


            }
            }
            else {
                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.camera_dalog, null);
                Button image_camera = (Button) popupView.findViewById(R.id.image_camera);
                Button image_gallery = (Button) popupView.findViewById(R.id.image_gallery);

                image_camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        captureImageFromCamera();
                        popupWindow.dismiss();
                    }
                });
                image_gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                        captureImageFromGallery();
                    }
                });

                popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setOutsideTouchable(true);
                popupWindow.showAsDropDown(img_user_profile);

            }

        }
        if (v == btn_update) {
            v.startAnimation(AppConstants.buttonClick);
            dob = tv_date_of_birth.getText().toString().trim();
            if (gender_u.equalsIgnoreCase("Choose")) {
                displayErrorDialog("Please select gender", this);
            } else if (dob.equalsIgnoreCase("Choose")) {
                displayErrorDialog("Please select date of birth", this);
                dob = "";
                // Common.displayLongToast(this,getResources().getString(R.string.select_dob));
            } else if (profile_type.equalsIgnoreCase("0")) {
                displayErrorDialog("Please select profile type", this);
            } else {
                try {

//                    updateImageToFirebaseDatabase(type, imageInByte);
                    hitUpdateSettingApi(user_id, image, profile_type, gender_u, dob);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (v == img_tick) {
            dob = tv_date_of_birth.getText().toString().trim();
            if (dob.equalsIgnoreCase("Choose")) {
                dob = "";
            }
            hitUpdateSettingApi(user_id, image, profile_type, gender_u, dob);
        }

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 123 :
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    // Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                    try {
                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View popupView = layoutInflater.inflate(R.layout.camera_dalog, null);
                        Button image_camera = (Button) popupView.findViewById(R.id.image_camera);
                        Button image_gallery = (Button) popupView.findViewById(R.id.image_gallery);

                        image_camera.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                captureImageFromCamera();
                                popupWindow.dismiss();
                            }
                        });
                        image_gallery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                popupWindow.dismiss();
                                captureImageFromGallery();
                            }
                        });

                        popupWindow = new PopupWindow(
                                popupView,
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        popupWindow.setBackgroundDrawable(new BitmapDrawable());
                        popupWindow.setOutsideTouchable(true);
                        popupWindow.showAsDropDown(img_user_profile);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                        new AlertDialog.Builder(this).
                                setTitle("Access Camera permission").
                                setMessage("You need to grant access camera permission to capture images" +
                                        "feature. Retry and grant it !").show();

                    } else {
                        new AlertDialog.Builder(this).
                                setTitle("Access Camera permission denied").
                                setMessage("You denied access camera permission." +
                                        " So, the feature will be disabled. To enable it" +
                                        ", go on settings and " +
                                        "grant access camera for the application").show();
                    }
                }
                break;
        }
    }


    private void hitUpdateSettingApi(String user_id, String image, String profile_type, String gender_u, String dob) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<ProfileStatusResponseModel> call;
            HashMap<String, String> requestValuePairsMap = new HashMap<>();
            requestValuePairsMap.put("user_id", user_id);
            requestValuePairsMap.put("profile_type", profile_type);
            requestValuePairsMap.put("gender", gender_u);
            requestValuePairsMap.put("dob", dob);
            if (image != null && !image.isEmpty()) {
                call = apiInterface.PostProfileUpdate(RetrofitUtils.createMultipartRequest(requestValuePairsMap),
                        RetrofitUtils.createFilePart("image", image, RetrofitUtils.MEDIA_TYPE_IMAGE_PNG));

            } else {
                call = apiInterface.PostProfileUpdate(RetrofitUtils.createMultipartRequest(requestValuePairsMap));
            }
            call.enqueue(new Callback<ProfileStatusResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<ProfileStatusResponseModel> call, @NonNull Response<ProfileStatusResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        ProfileStatusResponseModel responseModel = response.body();
                        if (responseModel.isError() == false) {
                            if (responseModel.isError() == false) {

                                Config.setLoginProfileImg(responseModel.getResponse().getImage(), context);
                                ActivityProfileStatus.this.finish();

                            }
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ProfileStatusResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        if (myCalendar.getTime().before(c.getTime())) {
            tv_date_of_birth.setText(sdf.format(myCalendar.getTime()));

            value = tv_date_of_birth.getText().toString().trim();
            type = "dob";
            dob = value;
        } else {
            Common.displayLongToast(this, getResources().getString(R.string.dob_check));
        }
    }


    /****************************
     * Capturing Image from camera
     ****************************/


     String mCurrentPhotoPath="";

    private File createImageFile()  {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        imageFileName="temp";

        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File dir = new File(Environment.getExternalStorageDirectory(), "myfolder");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            File image = File.createTempFile(imageFileName, ".jpg", dir);
            mCurrentPhotoPath = image.getAbsolutePath();
            return image;

        }catch (Exception e){

        }
        // Save a file: path for use with ACTION_VIEW intents
        return null;
    }

    private void captureImageFromCamera() {


//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        File photo = new File(getOutputMediaFileUri(MEDIA_TYPE_IMAGE).getPath());
//
//        //photoUri = Uri.fromFile(photo);
//        photoUri = FileProvider.getUriForFile(ActivityProfileStatus.this, getString(R.string.file_provider_authority), photo);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//
//        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

        Intent takePictureIntent =new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (Exception e) {
                // Error occurred while creating the File
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                if (Build.VERSION.SDK_INT>=23){
                photoUri = FileProvider.getUriForFile(this, "com.afrimack.app.fileprovider",
                        photoFile);
                }
                   else  {
                    photoUri = Uri.fromFile(photoFile);
                    }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        }
    }

  /*  private void captureImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
*/
    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null,
                    null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    private String saveToInternalSorage1(Bitmap bitmapImage) {

        File folder = new File(Environment.getExternalStorageDirectory()
                + "/Afrimack");
        if (!folder.exists()) {
            folder.mkdir();
        }
        int pathnew = (int) System.currentTimeMillis();

        File mypath = new File(folder, pathnew + ".jpg");

        FileOutputStream fos = null;
        try {
            mypath.createNewFile();
            fos = new FileOutputStream(mypath);


            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return folder.getAbsolutePath() + "/" + pathnew + ".jpg";
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
	 */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create"
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    /****************************
     * Capturing Image from gallery
     ****************************/
    private void captureImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, System.currentTimeMillis() + "", null);
        return Uri.parse(path);
    }

    /*****
     * Receiving activity result method will be called after closing the camera
     ******/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            previewImage(0, data);

        } else if (requestCode == GALLERY_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            previewImage(1, data);

        } else {
            Toast.makeText(this, " cancel capture image ", Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();
        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void previewImage(int previewfrom, Intent data) {
        switch (previewfrom) {
            case 0:

                try {
                    ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
                    int or = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    float toRotate = 0;
                    if (or == ExifInterface.ORIENTATION_ROTATE_90)
                        toRotate = 90;
                    else if (or == ExifInterface.ORIENTATION_ROTATE_180)
                        toRotate = 180;
                    else if (or == ExifInterface.ORIENTATION_ROTATE_270)
                        toRotate = 270;
                    thumbnail = getResizedBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri), 800);
                    if (toRotate != 0) {
                        thumbnail = rotateBitmap(thumbnail, toRotate);
                    }
                    img_user_profile.setImageBitmap(thumbnail);
                    selectedImagePath = saveToInternalSorage1(thumbnail);
                } catch (Exception e) {
                    e.printStackTrace();
                }


             //   selectedImagePath = destination.getAbsolutePath();
                type = "image";
                value = selectedImagePath;
                image = value;

                break;
            case 1:
                uri = data.getData();
                Log.d("uri1", uri + "");
                try {

                    selectedImagePath = Common.getPath(this, uri);

                    img_user_profile.setImageBitmap(Common.getImageFromPath(selectedImagePath, 500, 500));

                    bm = (Common.getImageFromPath(selectedImagePath, 500, 500));

                    Log.d("Bitmap", "" + bm);

                    Matrix matrix1 = new Matrix();
                    matrix1.postRotate(90);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix1, true);

                    stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    imageInByte = stream.toByteArray();
                    imgString = Base64.encodeToString(imageInByte,
                            Base64.DEFAULT);
                    Log.e("imageview", imgString);
                    //Config.setSellImage(imgString,this);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                type = "image";
                value = selectedImagePath;
                image = value;
                break;
        }
    }

}

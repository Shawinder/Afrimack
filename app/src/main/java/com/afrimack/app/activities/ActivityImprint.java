package com.afrimack.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.models.TCPResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityImprint extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel;

    private String slug = "Contact";
    private WebView web_polcy;
    private Context context = this;
    private String TAG = ActivityImprint.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imprint);
        init();
        hitApi();
    }

    private void hitApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<TCPResponseModel> call = apiInterface.imprint(slug);
            call.enqueue(new Callback<TCPResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<TCPResponseModel> call, @NonNull Response<TCPResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        TCPResponseModel model = response.body();
                        if (model.isError() == false) {
                            tv_titel.setText(model.getResponse().getTitle());
                            String ss = model.getResponse().getDescription();
                            web_polcy.loadData(ss, "text/html; charset=utf-8", "utf-8");
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<TCPResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);

        tv_titel = (TextView) findViewById(R.id.tv_titel);

        img_back.setOnClickListener(this);


        web_polcy = (WebView) findViewById(R.id.web_import);

    }

    @Override
    public void onClick(View v) {

        if (v == img_back) {
            onBackPressed();
        }
    }

}

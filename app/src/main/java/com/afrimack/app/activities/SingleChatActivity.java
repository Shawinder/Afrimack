package com.afrimack.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.AfrimackApp;
import com.afrimack.app.BuildConfig;
import com.afrimack.app.R;
import com.afrimack.app.adapters.ChatRecyclerAdapter;
import com.afrimack.app.appbeans.AddressFinderBean;
import com.afrimack.app.appbeans.BlockUserBean;
import com.afrimack.app.appbeans.ChatRequestBean;
import com.afrimack.app.appbeans.Message;
import com.afrimack.app.appbeans.User;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.customviews.HeaderDecoration;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.DeleteChatImagesModel;
import com.afrimack.app.models.SignupResponseModel;
import com.afrimack.app.models.SingleResponseModel;
import com.afrimack.app.models.UploadChatImage;
import com.afrimack.app.rest.RetrofitUtils;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.services.ServiceUtils;
import com.afrimack.app.utilities.FilePathGetter;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.NotificationBuilder;
import com.brsoftech.core_utils.utils.ImagePickerUtil;
import com.brsoftech.emozi.EmojiconEditText;
import com.brsoftech.emozi.EmojiconGridFragment;
import com.brsoftech.emozi.EmojiconsFragment;
import com.brsoftech.emozi.imozemodel.Emojicon;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.webkit.MimeTypeMap.getFileExtensionFromUrl;

public class SingleChatActivity extends AppCompatActivity implements ImagePickerUtil.OnImagePickerListener, EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener, View.OnClickListener {

    private RecyclerView recyclerChat;
    private ImageView img_emoji;
    private LinearLayout ll_emojilay;
    private EmojiconEditText et_message;
    private ImageView iv_send_mess;
    private ImageView iv_camerabtn, iv_attach_file;
    private ImageView img_back_chat, img_profile_chat;
    private TextView tv_chat_user_name;
    private TextView tv_on_off;
    private ImageView img_chat_menu;
    private PopupWindow popupWindow;

    private String other_user_id, group_id;
    //=========camera==========
    public static Bitmap thumbnail;
    private static Uri uri;
    private ByteArrayOutputStream stream;
    private static Bitmap bm;
    private String user_id, type, value;
    private String image;
    private String imgString = null;
    private String selectedImagePath = null;
    private static final String IMAGE_DIRECTORY_NAME = "Afrimack";
    private Uri photoUri;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    byte[] imageInByte;
    //=========================
    private int image_type;
    private ImagePickerUtil imagePickerUtil;
    // SingleProfileResponseModel model;
    public static final String FROM_USER_ID = "FROM_USER_ID";
    public static final String FROM_USER_INFO = "FROM_USER_INFO";
    public static final String PRIVES_FGM = "PRIVES_FGM";
    public static final int VIEW_TYPE_USER_MESSAGE = 0;
    public static final int VIEW_TYPE_FRIEND_MESSAGE = 1;
    public static HashMap<String, Bitmap> bitmapAvataFriend;

    public static final int MY_PERMISSIONS_REQUEST_READ_CAMERA = 123;


    /*Chat */
    HeaderDecoration headerDecoration;
    ChatRecyclerAdapter mAdapter;

    long previosLastSeen = -1L;


    private int RESULT_LOAD_IMAGE = 1;
    private String imagePath = null;
    private static final int PICKFILE_REQUEST_CODE = 2;

    String UsserJ_id;
    private Dialog dialog;
    String chat_useimage;
    List<String> messageList = new ArrayList<>();
    String chat_user_name;
    private String otherUserUID = "";
    private Context context = this;
    private String TAG = SingleChatActivity.class.getSimpleName();
    private LinearLayoutManager linearLayoutManager;
    private String roomId = "";
    private String userID = "";
    public static HashMap<String, Message> messages = new HashMap<>();
    public static ArrayList<String> messageIds = new ArrayList<>();
    private DatabaseReference mDatabase;
    private String otherUSERDeviceToken = "";
    private String from = "";
    private int userIdBlock = 0;
    private int blockedStatus = 0;
    private int blocked_by = 0;
    private int php_block_id = 0;
    private LinearLayout id_linear_chat;
    TextView tv_block_user;
    private int fromBlock = 0;
    private String otherUserStatus = "";
    StorageReference reference = null;
    StorageReference imagesRef = null;
    UploadTask uploadTask = null;
    private String fromCam = "";

    private String chatUserImage = "";
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_single_chat);

        Config.init(this);

        fromBlock = 0;
        from = getIntent().getStringExtra("from");
        chatUserImage = getIntent().getStringExtra("user_image");
        otherUserUID = getIntent().getStringExtra("U_ID");
        userIdBlock = getIntent().getIntExtra("other_user_id", 0);
        blockedStatus = getIntent().getIntExtra("blocked_status", 0);
        blocked_by = getIntent().getIntExtra("blocked_by", 0);
        php_block_id = getIntent().getIntExtra("php_block_id", 0);


        userID = Config.getLoginUSERUID(context);

//        userID = Config.getUserId(context);

        Config.setOtherUserUID(otherUserUID, context);
        roomId = otherUserUID.compareTo(userID) > 0 ? (userID + otherUserUID).hashCode() + "" : "" + (otherUserUID + userID).hashCode();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("message/" + roomId);

        //firebase storage
        FirebaseStorage storage = FirebaseStorage.getInstance();
        reference = storage.getReference();


        imagePickerUtil = new ImagePickerUtil(this);
        // model = (SingleProfileResponseModel) getIntent().getSerializableExtra(ConstantMain.CHATMODEL);
        chat_useimage = Config.getmodel_user_image(this);
        chat_user_name = Config.getmodel_user_Short_name(this);

        user_id = Config.getUserId(this);
        other_user_id = Config.getSingleChatId(this);
        group_id = Config.getSingleGroupId(this);
        UsserJ_id = Config.getj_id(this);
        Config.setChatNotification(other_user_id, this);


        init();
        recyclerChat = (RecyclerView) findViewById(R.id.rv_chats);
        img_emoji = (ImageView) findViewById(R.id.img_emoji);
        ll_emojilay = (LinearLayout) findViewById(R.id.ll_emojilay);
        et_message = (EmojiconEditText) findViewById(R.id.et_message);
        iv_send_mess = (ImageView) findViewById(R.id.iv_send_mess);
        iv_camerabtn = (ImageView) findViewById(R.id.iv_camerabtn);
        img_chat_menu = (ImageView) findViewById(R.id.img_chat_menu);
        getChatMessagesFromFirebase();
        getUserDEtailsFromFirebase();


        img_emoji.setOnClickListener(this);
        et_message.setOnClickListener(this);
        iv_send_mess.setOnClickListener(this);
        iv_camerabtn.setOnClickListener(this);
        img_chat_menu.setOnClickListener(this);

        handler=new Handler();


    }


    private void getUserDEtailsFromFirebase() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");
            // Read from the database
            mDatabase.child(otherUserUID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
//                    showProgress(false);
                    try {
                        User user = dataSnapshot.getValue(User.class);
                        if (user != null) {
                            otherUSERDeviceToken = user.getDeviceToken();
                            setData(user);

                        } else {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }
                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    CommonUtils.getInstance().dismissLoadingDialog();
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });
        } else {
            CommonUtils.getInstance().showAlertMessage(context, "Please ensure internet connectivity.");
        }

    }

    private void setData(User user) {
        try {
            Glide.with(context)
                    .load(chatUserImage)
                    .asBitmap()
                    .placeholder(R.drawable.user_round)
                    .into(img_profile_chat);
        } catch (Exception e) {
            CommonUtils.getInstance().dismissLoadingDialog();
            e.printStackTrace();
        }
        tv_chat_user_name.setText(user.getFirstName());
        if (user.getOnline().equalsIgnoreCase("0")) {
            tv_on_off.setVisibility(View.GONE);
            otherUserStatus = "0";
        } else if (user.getOnline().equalsIgnoreCase("1")) {
            tv_on_off.setVisibility(View.GONE);
            otherUserStatus = "1";
        } else if (user.getOnline().equalsIgnoreCase("3")) {
            tv_on_off.setVisibility(View.GONE);
            otherUserStatus = "3";
        } else {
            tv_on_off.setVisibility(View.VISIBLE);
            tv_on_off.setText("Online");
            otherUserStatus = "2";
        }
        CommonUtils.getInstance().dismissLoadingDialog();

    }


    public ChildEventListener childEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            try {
//                CommonUtils.getInstance().dismissLoadingDialog();
                if (dataSnapshot.getValue() != null) {
                    HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                    Message newMessage = new Message();
                    newMessage.idSender = (String) mapMessage.get("idSender");
                    newMessage.idReceiver = (String) mapMessage.get("idReceiver");
                    newMessage.text = (String) mapMessage.get("text");
                    newMessage.timestamp = (long) mapMessage.get("timestamp");
                    newMessage.isRead = (String) mapMessage.get("isRead");
                    newMessage.isImage = (String) mapMessage.get("isImage");
                    newMessage.photoUrl = (String) mapMessage.get("photoUrl");

                    try {
                        if (fromCam.equalsIgnoreCase("a")) {
                            if (messages.get("abc") != null) {
                                messages.remove("abc");
                                messageIds.remove(messageIds.size() - 1);
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (newMessage.idSender.equals(userID)) {

                    } else {
                        // Update read status
                        newMessage.isRead = "1";
                        FirebaseDatabase.getInstance().getReference().child("message/" + roomId).child(dataSnapshot.getKey()).setValue(newMessage);
                    }
                    if (messages.containsKey(dataSnapshot.getKey())) {

                    } else {
                        messageIds.add(dataSnapshot.getKey());
                    }


                    messages.put(dataSnapshot.getKey(), newMessage);
                    mAdapter.notifyDataSetChanged();
                    linearLayoutManager.scrollToPosition(messages.size() - 1);

                }
            } catch (Exception e) {
                CommonUtils.getInstance().dismissLoadingDialog();
                e.printStackTrace();
            }

        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            try {
                if (dataSnapshot.getValue() != null) {
                    HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                    Message newMessage = new Message();
                    newMessage.idSender = (String) mapMessage.get("idSender");
                    newMessage.idReceiver = (String) mapMessage.get("idReceiver");
                    newMessage.text = (String) mapMessage.get("text");
                    newMessage.timestamp = (long) mapMessage.get("timestamp");
                    newMessage.isRead = (String) mapMessage.get("isRead");
                    newMessage.isImage = (String) mapMessage.get("isImage");
                    newMessage.photoUrl = (String) mapMessage.get("photoUrl");

                    if (messages.containsKey(dataSnapshot.getKey())) {

                    } else {
                        messageIds.add(dataSnapshot.getKey());
                    }
                    messages.put(dataSnapshot.getKey(), newMessage);
                    mAdapter.notifyDataSetChanged();
                    linearLayoutManager.scrollToPosition(messages.size() - 1);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

            if (mDatabase != null) {
                if (messages != null && messageIds != null) {
                    messages.clear();
                    messageIds.clear();
                    mAdapter.notifyDataSetChanged();
                }
            }

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e("database error ", databaseError.getMessage());
            CommonUtils.getInstance().dismissLoadingDialog();
        }
    };

    private void getChatMessagesFromFirebase() {

        messages.clear();
        messageIds.clear();

    }


    @Override
    public void onResume() {
        super.onResume();

        try {
            ServiceUtils.updateLoginUserStatus(context, "2");
        } catch (Exception e) {
            e.printStackTrace();
        }

        AfrimackApp.curentactivity = this;
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerChat.setLayoutManager(linearLayoutManager);
        mAdapter = new ChatRecyclerAdapter(this, userID);
        recyclerChat.setAdapter(mAdapter);
        childEventListener = mDatabase.addChildEventListener(childEventListener);
//        mDatabase.addChildEventListener(childEventListener);

        mDatabase.addChildEventListener(childEventListener);
    }

    @Override
    protected void onPause() {
        try {
            ServiceUtils.updateLoginUserStatus(context, "3");
        } catch (Exception e) {
            e.printStackTrace();
        }

        AfrimackApp.curentactivity = null;
        if (childEventListener != null) {
            mDatabase.removeEventListener(childEventListener);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        try {
            ServiceUtils.updateLoginUserStatus(context, "3");
        } catch (Exception e) {
            e.printStackTrace();
        }

        AfrimackApp.curentactivity = null;
        if (childEventListener != null) {
            mDatabase.removeEventListener(childEventListener);
        }
        super.onDestroy();
    }

    private void init() {
        id_linear_chat = (LinearLayout) findViewById(R.id.id_linear_chat);
        img_back_chat = (ImageView) findViewById(R.id.img_back_chat);
        img_profile_chat = (ImageView) findViewById(R.id.img_profile_chat);
        tv_chat_user_name = (TextView) findViewById(R.id.tv_chat_user_name);
        tv_on_off = (TextView) findViewById(R.id.tv_on_off);
        iv_attach_file = (ImageView) findViewById(R.id.iv_attach_file);

        img_profile_chat.setOnClickListener(this);
        iv_attach_file.setOnClickListener(this);
        img_back_chat.setOnClickListener(this);
    }


    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
//        et_message.setEmojiconSize(60);
        EmojiconsFragment.input(et_message, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(et_message);
    }

    @Override
    public void onBackPressed() {
        AfrimackApp.curentactivity = null;
        Config.setChatNotification("", this);
        if (ll_emojilay.getVisibility() == View.VISIBLE) {
            ll_emojilay.setVisibility(View.GONE);
        } else if (from.equalsIgnoreCase("from")) {
            super.onBackPressed();
        } else {
            Intent mIntent = new Intent(context, ActivityHome.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mIntent);
            SingleChatActivity.this.finish();
        }
    }


    @SuppressLint("NewApi")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_back_chat:
                v.startAnimation(AppConstants.buttonClick);
                if (from.equalsIgnoreCase("from")) {
                    super.onBackPressed();
                } else {
                    Intent mIntent = new Intent(context, ActivityHome.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                    SingleChatActivity.this.finish();
                }
                break;

            case R.id.img_profile_chat:
                v.startAnimation(AppConstants.buttonClick);

                Intent intent = new Intent(this, SingleProfileViewActivity.class);
                intent.putExtra("from","chat");

                Config.setSingleChatId(String.valueOf(userIdBlock), context);
//                intent.putExtra("id", String.valueOf(userIdBlock));
                startActivity(intent);
                break;

            case R.id.iv_attach_file:
                v.startAnimation(AppConstants.buttonClick);
                if (blocked_by == Integer.valueOf(user_id)) {
                    CommonUtils.getInstance().showAlertMessage(context, "Un-block user first.");
                } else if (blocked_by == userIdBlock) {
                    CommonUtils.getInstance().showAlertMessage(context, "You are blocked by this user.");
                } else {
                    if (blockedStatus == 1) {

                    } else {
                        if (fromCam.equalsIgnoreCase("fromCam")) {
                            Toast.makeText(context, "Wait while other image is uploading to server...", Toast.LENGTH_LONG).show();
                        } else {
                            fromCam = "fromCam";
                            imagePickerUtil.pickImageFromGallery(this);
                        }

                    }

                }


                break;

            case R.id.img_emoji:
                v.startAnimation(AppConstants.buttonClick);
                if (ll_emojilay.getVisibility() == View.VISIBLE) {
                    ll_emojilay.setVisibility(View.GONE);
                    showKeyBoardView();
                } else {
                    hideKeyboard();
                    ll_emojilay.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ll_emojilay.setVisibility(View.VISIBLE);
                        }
                    }, 100);

                }
                break;


            case R.id.img_chat_menu:
                v.startAnimation(AppConstants.buttonClick);
                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.dialog_chat, null);
                tv_block_user = (TextView) popupView.findViewById(R.id.tv_block_user);
                TextView tv_chat_overview = (TextView) popupView.findViewById(R.id.tv_chat_overview);
                TextView tv_delete_this_chat = (TextView) popupView.findViewById(R.id.tv_delete_this_chat);
                View chat_view = (View) popupView.findViewById(R.id.chat_view);
                View delete_view = (View) popupView.findViewById(R.id.delete_view);
                chat_view.setVisibility(View.VISIBLE);
                delete_view.setVisibility(View.VISIBLE);
                tv_chat_overview.setVisibility(View.VISIBLE);
                tv_delete_this_chat.setVisibility(View.VISIBLE);

                if (fromBlock == 1) {
                    tv_block_user.setVisibility(View.GONE);

                } else {
                    tv_block_user.setVisibility(View.VISIBLE);
                }
                if (blocked_by == Integer.valueOf(user_id)) {
                    if (blockedStatus == 1) {
                        tv_block_user.setText("Un-Block this User");
                    } else {
                        tv_block_user.setText("Block this User");
                    }
                } else if (blocked_by == userIdBlock) {
                    tv_block_user.setText("Block this User");
                } else {
                    if (blockedStatus == 1) {
                        tv_block_user.setText("Un-Block this User");
                    } else {
                        tv_block_user.setText("Block this User");
                    }
                }


                tv_delete_this_chat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        view.startAnimation(AppConstants.buttonClick);
                        //delete chat
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//                        if (String.valueOf(userIdBlock).equalsIgnoreCase(user_id)) {
                        alertDialogBuilder.setMessage("Are you sure you want to delete all chat messages?\nInfo: The other user won't be seen it anymore either.");

                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        if (CommonUtils.getInstance().isNetConnected(context)) {
                                            if (mDatabase != null) {
//
                                                try {
//                                                    deleteStorageImages();
                                                    hitDeleteImagesServerApi();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                mDatabase.setValue(null);
                                                messages.clear();
                                                messageIds.clear();
                                                mAdapter.notifyDataSetChanged();
                                            }
                                        } else {
                                            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
                                        }
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        popupWindow.dismiss();
                    }
                });

                tv_chat_overview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        view.startAnimation(AppConstants.buttonClick);
                        popupWindow.dismiss();
                        Intent mIntent = new Intent(SingleChatActivity.this, ActivityChattingOverView.class);
                        startActivity(mIntent);
                    }
                });

                tv_block_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        view.startAnimation(AppConstants.buttonClick);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                        if (blocked_by == Integer.valueOf(user_id)) {
                            if (blockedStatus == 1) {
                                alertDialogBuilder.setMessage("Are you sure you want to un-block this user?");
                            } else {
                                alertDialogBuilder.setMessage("Are you sure you want to block this user?");
                            }
                        } else if (blocked_by == userIdBlock) {
                            alertDialogBuilder.setMessage("Are you sure you want to block this user?");
                        } else {
                            if (blockedStatus == 1) {
                                alertDialogBuilder.setMessage("Are you sure you want to un-block this user?");
                            } else {
                                alertDialogBuilder.setMessage("Are you sure you want to block this user?");
                            }
                        }

                        alertDialogBuilder.setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        try {

                                            if (blocked_by == Integer.valueOf(user_id)) {
                                                if (blockedStatus == 1) {
                                                    hitUnblockApi(user_id, String.valueOf(php_block_id));
                                                } else {
                                                    hitBlockUserApi();
                                                }
                                            } else if (blocked_by == userIdBlock) {
                                                CommonUtils.getInstance().showAlertMessage(context, "You are already blocked by this user.");
                                            } else {
                                                if (blockedStatus == 1) {
                                                    hitUnblockApi(user_id, String.valueOf(php_block_id));
                                                } else {
                                                    hitBlockUserApi();
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        popupWindow.dismiss();
                    }
                });

                popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setOutsideTouchable(true);
                popupWindow.showAsDropDown(id_linear_chat, 0, 0, Gravity.RIGHT);

                break;
            case R.id.et_message:
                if (ll_emojilay.getVisibility() == View.VISIBLE) {
                    ll_emojilay.setVisibility(View.GONE);
                    showKeyBoardView();
                }
                break;

            case R.id.iv_send_mess:
                String content = et_message.getText().toString().trim();
                if (content.length() > 0) {
                    et_message.setText("");
                    Message newMessage = new Message();
                    newMessage.text = content;
                    newMessage.idSender = userID;
                    newMessage.idReceiver = roomId;
                    newMessage.timestamp = System.currentTimeMillis();
                    newMessage.isRead = "0";
                    newMessage.isImage = "0";
                    newMessage.photoUrl = "";
                    if (blocked_by == Integer.valueOf(user_id)) {
                        CommonUtils.getInstance().showAlertMessage(context, "Un-block user first.");
                    } else if (blocked_by == userIdBlock) {
                        CommonUtils.getInstance().showAlertMessage(context, "You are blocked by this user.");
                    } else {
                        if (blockedStatus == 1) {

                        } else {
                            sendMessage(newMessage);
                        }
                    }
                }
                break;

            case R.id.iv_camerabtn:
                v.startAnimation(AppConstants.buttonClick);
                if (blocked_by == Integer.valueOf(user_id)) {
                    CommonUtils.getInstance().showAlertMessage(context, "Un-block user first.");
                } else if (blocked_by == userIdBlock) {
                    CommonUtils.getInstance().showAlertMessage(context, "You are blocked by this user.");
                } else {
                    if (blockedStatus == 1) {

                    } else {
                        if (fromCam.equalsIgnoreCase("fromCam")) {
                            Toast.makeText(context, "Wait while other image is uploading to server...", Toast.LENGTH_LONG).show();
                        } else {

                            if (ContextCompat.checkSelfPermission(SingleChatActivity.this,
                                    Manifest.permission.CAMERA)
                                    != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(SingleChatActivity.this,
                                        new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_CAMERA);
                                //Toast.makeText(SellOffer.this, "Permission not  granted", Toast.LENGTH_SHORT).show();
                                //  popupWindow.dismiss();
                            }
                            else {
                                //Toast.makeText(SellOffer.this, "Permission already granted", Toast.LENGTH_SHORT).show();
                                fromCam = "fromCam";
                                captureImageFromCamera();
//                                popupWindow.dismiss();
                                // imagePickerUtil.pickImageFromCamera(this);
                            }


                        }
                    }
                }
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 123 :
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    // Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                    try {
                        captureImageFromCamera();
                       // popupWindow.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                        new AlertDialog.Builder(this).
                                setTitle("Access Camera permission").
                                setMessage("You need to grant access camera permission to capture images" +
                                        "feature. Retry and grant it !").show();

                    } else {
                        new AlertDialog.Builder(this).
                                setTitle("Access Camera permission denied").
                                setMessage("You denied access camera permission." +
                                        " So, the feature will be disabled. To enable it" +
                                        ", go on settings and " +
                                        "grant access camera for the application").show();
                    }
                }
                break;
        }
    }




    private void deleteStorageImages() {
        final FirebaseStorage firebaseStorage = FirebaseStorage.getInstance().getReference().getStorage();
        CommonUtils.getInstance().displayLoadingDialog(false, context);
        Query query = mDatabase.orderByChild("isImage").equalTo("1");
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    CommonUtils.getInstance().dismissLoadingDialog();
                    // dataSnapshot is the "issue" node with all children with id 0
                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        // do something with the individual "issues"
                        HashMap mapMessage = (HashMap) issue.getValue();
                        String val = (String) mapMessage.get("photoUrl");
                        StorageReference photoRef = firebaseStorage.getReferenceFromUrl(val);
                        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                // File deleted successfully
                                Log.d(TAG, "onSuccess: deleted file");
                                CommonUtils.getInstance().dismissLoadingDialog();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Uh-oh, an error occurred!
                                Log.d(TAG, "onFailure: did not delete file");
                                CommonUtils.getInstance().dismissLoadingDialog();
                            }
                        });
                    }
                } else {
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                CommonUtils.getInstance().dismissLoadingDialog();
            }
        });
    }

    private void sendMessage(Message newMessage) {
        try {
            hitApi("3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        FirebaseDatabase.getInstance().getReference().child("message/" + roomId).push().setValue(newMessage);

        if (otherUserStatus.equalsIgnoreCase("1") || otherUserStatus.equalsIgnoreCase("2") || otherUserStatus.equalsIgnoreCase("3")) {

            if (newMessage.isImage.equalsIgnoreCase("1")) {
                sendNotification("Image");
            } else {
                sendNotification(newMessage.text);
            }
        }
    }

    private void hitDeleteImagesServerApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<DeleteChatImagesModel> call = apiInterface.DeleteChatImage( Config.getUserId(context));
            call.enqueue(new Callback<DeleteChatImagesModel>() {
                @Override
                public void onResponse(@NonNull Call<DeleteChatImagesModel> call, @NonNull Response<DeleteChatImagesModel> response) {

                    try {
                       // Toast.makeText(SingleChatActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("response ", response.body().getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<DeleteChatImagesModel> call, Throwable t) {
                }
            });

        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void hitApi(final String status) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ChatRequestBean> call = apiInterface.confirmIgnoreChatRequest(String.valueOf(userIdBlock), Config.getUserId(context), status);
            call.enqueue(new Callback<ChatRequestBean>() {
                @Override
                public void onResponse(@NonNull Call<ChatRequestBean> call, @NonNull Response<ChatRequestBean> response) {

                    try {
                        Log.e("response ", response.body().getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ChatRequestBean> call, Throwable t) {
                }
            });


        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitBlockUserApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<BlockUserBean> call = apiInterface.BlockUser(user_id, String.valueOf(userIdBlock));
            call.enqueue(new Callback<BlockUserBean>() {
                @Override
                public void onResponse(@NonNull Call<BlockUserBean> call, @NonNull Response<BlockUserBean> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        BlockUserBean commanResponseModel = response.body();

                        if (commanResponseModel.getError() == false) {
                            Common.displayLongToast(context, commanResponseModel.getMessage());
                            php_block_id = commanResponseModel.getResponse().getBlockId();
                            blockedStatus = 1;
                            blocked_by = Integer.parseInt(Config.getUserId(context));
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BlockUserBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitUnblockApi(String user_id, String blockuser_id) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.unBlockUser(user_id, blockuser_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel response1 = response.body();
                        Common.displayLongToast(context, response1.getMessage());
                        if (response1.isError() == false) {
//                            php_block_id = php_block_id + 1;
                            blockedStatus = 0;
                            blocked_by = 0;


                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    public void sendNotification(String message) {

        NotificationBuilder.initialize()
                .title("Afrimack Message")
                .message(message)
                .username(Config.getLoginUserName())
                .uid(userID)
                .firebaseToken(Config.getDeviceToken(context))
                .receiverFirebaseToken(otherUSERDeviceToken)
                .send();
    }

    private void hideKeyboard() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = this.getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    private void showKeyBoardView() {
        et_message.requestFocus();
        et_message.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imgr = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imgr.showSoftInput(et_message, InputMethodManager.SHOW_IMPLICIT);
            }
        }, 50);
    }

    /****************************camera pick image*******************/


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {

            handler.post(new Runnable() {
                @Override
                public void run() {
                    previewImage(0, data);
                }
            });


        } else if (resultCode == RESULT_OK && requestCode == ImagePickerUtil.RC_PICK_IMAGE_STORAGE) {
            imagePickerUtil.onActivityResult(requestCode, resultCode, data);

      /*  if (resultCode == RESULT_OK && (requestCode == ImagePickerUtil.RC_PICK_IMAGE_CAMERA || requestCode == ImagePickerUtil.RC_PICK_IMAGE_STORAGE)) {
            try {
                imagePickerUtil.onActivityResult(requestCode, resultCode, data);
            } catch (Exception e) {
                e.printStackTrace();
            }
*/

        } else if (requestCode == PICKFILE_REQUEST_CODE && resultCode == this.RESULT_OK && data != null && data.getData() != null) {
            Uri fileuri = data.getData();
            HashMap<String, String> hMapdoc = new HashMap<>();
            hMapdoc.put("pdf", "pdf");
            hMapdoc.put("ppt", "ppt");
            hMapdoc.put("txt", "txt");
            hMapdoc.put("xls", "xls");


            String realPath = FilePathGetter.getPath(this, fileuri);
            //            tv_aj_resume.setText(docFilePath);
            if (!TextUtils.isEmpty(realPath)) {
                String fileExtension = getFileExtensionFromUrl(fileuri
                        .toString()).toLowerCase();

                if (hMapdoc.containsKey(fileExtension)) {
                    // sendDocumentMedia(realPath);
                } else {
                    //Utils.showCenteredToast(this.getResources().getString(R.string.Please_select_doc_only));
                }
                Log.e("file path", realPath);
            }

        }

    }



    //private void uploadImage(byte[] file) {
    private void uploadImage(String file) {
        if (file != null) {
            Log.e("IMAGE_URL_UPLOADED", "HIT_API");
            hitUploadImageApi(file);
            Log.e("IMAGE_URL_UPLOADED", "HIT SUCCESS");
/*

//            CommonUtils.getInstance().displayLoadingDialog(false, context);
            imagesRef = reference.child("chatmessage/" + roomId + "/" + System.currentTimeMillis());
            uploadTask = imagesRef.putBytes(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    e.printStackTrace();
//                    CommonUtils.getInstance().dismissLoadingDialog();
                    Toast.makeText(context, "Error : " + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if (taskSnapshot != null) {*/
//                        CommonUtils.getInstance().dismissLoadingDialog();

        }
    }

    @Override
    public void onImagePickerSuccess(String imgPath, boolean fromCamera) {

    }


    @Override
    public void onImagePickerSuccess(final Uri imgPath, boolean fromCamera) {
        if (imgPath != null) {
//            fromCam="fromCam";

            try {
                /*      bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgPath);
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bm.compress(Bitmap.CompressFormat.PNG, 100, baos); *///bm is the bitmap object
                       //    selectedImagePath = saveToInternalSorage1(thumbnail);
                            // byte[] imageInBytes = baos.toByteArray();


                Message newMessage = new Message();
                newMessage.text = "";
                newMessage.idSender = userID;
                newMessage.idReceiver = roomId;
                newMessage.timestamp = System.currentTimeMillis();
                newMessage.isRead = "0";
                newMessage.isImage = "1";
                newMessage.photoUrl = "";
                messageIds.add("abc");
                messages.put("abc", newMessage);
                Log.e("ID_SNEDER", newMessage.idSender);
                mAdapter.notifyItemInserted(messages.size() - 1);

                //=========

                //uploadImage(imageInBytes);
                if (fromCamera) {
                    Log.e("UPLOAD_IMAGe", selectedImagePath);
                    uploadImage(selectedImagePath);
                } else {
                    File finalFile = new File(getRealPathFromURI(imgPath));
                    Log.e("UPLOAD_IMAGe", finalFile.getAbsolutePath());
                    uploadImage(finalFile.getAbsolutePath().toString());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    public void onImagePickerFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    View.OnClickListener mediaImgOpenClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String imgPath = (String) v.getTag();
            if (imgPath != null) {
                Log.e("ImgPath", imgPath);
                dialog = new Dialog(SingleChatActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_image);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setCancelable(false);
                ImageView chat_image = (ImageView) dialog.findViewById(R.id.imvChatImgOpen);
                TextView dailog_close = (TextView) dialog.findViewById(R.id.dailog_close);
                chat_image.setImageURI(Uri.parse(imgPath));
                dailog_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }
    };

    public View.OnClickListener openMediaImageClick() {
        return mediaImgOpenClick;
    }


    public void chatnotification(int blocked) {
        blockedStatus = blocked;
        fromBlock = 1;
        if (blockedStatus == 1) {
            fromBlock = 1;
        } else {
            fromBlock = 0;
            ;
        }
    }

    //================camera_ functionality===================
    String mCurrentPhotoPath="";

    private File createImageFile()  {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        imageFileName="temp";

        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File dir = new File(Environment.getExternalStorageDirectory(), "myfolder");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            File image = File.createTempFile(imageFileName, ".jpg", dir);
            mCurrentPhotoPath = image.getAbsolutePath();
            return image;

        }catch (Exception e){

        }
        // Save a file: path for use with ACTION_VIEW intents
        return null;
    }



    private void captureImageFromCamera() {

       /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(getOutputMediaFileUri(MEDIA_TYPE_IMAGE).getPath());

       // photoUri = Uri.fromFile(photo);
        photoUri = FileProvider.getUriForFile(SingleChatActivity.this, BuildConfig.APPLICATION_ID, photo);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);

        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);*/

        Intent takePictureIntent =new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (Exception e) {
                // Error occurred while creating the File
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                if (Build.VERSION.SDK_INT>=23){
                    photoUri = FileProvider.getUriForFile(this, "com.afrimack.app.fileprovider",
                            photoFile);
                }
                else  {
                    photoUri = Uri.fromFile(photoFile);
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        }

    }


    private String saveToInternalSorage1(Bitmap bitmapImage) {

        File folder = new File(Environment.getExternalStorageDirectory()
                + "/Afrimack");
        if (!folder.exists()) {
            folder.mkdir();
        }
        int pathnew = (int) System.currentTimeMillis();

        File mypath = new File(folder, pathnew + ".jpg");

        FileOutputStream fos = null;
        try {
            mypath.createNewFile();
            fos = new FileOutputStream(mypath);


            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return folder.getAbsolutePath() + "/" + pathnew + ".jpg";
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /* * returning image / video*/


    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();
        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    private void previewImage(int previewfrom, Intent data) {
        switch (previewfrom) {
            case 0:

                try {
                    ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
                    int or = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    float toRotate = 0;
                    if (or == ExifInterface.ORIENTATION_ROTATE_90)
                        toRotate = 90;
                    else if (or == ExifInterface.ORIENTATION_ROTATE_180)
                        toRotate = 180;
                    else if (or == ExifInterface.ORIENTATION_ROTATE_270)
                        toRotate = 270;
                    thumbnail = getResizedBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri), 800);
                    if (toRotate != 0) {
                        thumbnail = rotateBitmap(thumbnail, toRotate);
                    }

                    //  img_user_profile.setImageBitmap(thumbnail);
             /*       handler.post(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
*/
                    selectedImagePath = saveToInternalSorage1(thumbnail);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                //   selectedImagePath = destination.getAbsolutePath();
                type = "image";
               value = selectedImagePath;
               image = value;
                onImagePickerSuccess(photoUri, true);
                break;
            case 1:
                uri = data.getData();
                Log.d("uri1", uri + "");
                try {

                    selectedImagePath = Common.getPath(this, uri);
                    //img_user_profile.setImageBitmap(Common.getImageFromPath(selectedImagePath, 500, 500));

                    bm = (Common.getImageFromPath(selectedImagePath, 500, 500));
                    Log.d("Bitmap", "" + bm);

                    Matrix matrix1 = new Matrix();
                    matrix1.postRotate(90);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix1, true);

                    stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    imageInByte = stream.toByteArray();
                    imgString = Base64.encodeToString(imageInByte,
                            Base64.DEFAULT);
                    Log.e("imageview", imgString);

                    //Config.setSellImage(imgString,this);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                type = "image";
                value = selectedImagePath;
                image = value;
                break;
        }
    }


    //=======================upload_image_on_server===================

    private void hitUploadImageApi(final String selectedImagePath) {

        if (CommonUtils.getInstance().isNetConnected(context)) {

            Call<UploadChatImage> call;
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);

            HashMap<String, String> requestValuePairsMap = new HashMap<>();
            requestValuePairsMap.put("user_id", Config.getUserId(context));

            if (selectedImagePath != null && !selectedImagePath.isEmpty()) {
                call = apiInterface.UploadImageChat(RetrofitUtils.createMultipartRequest(requestValuePairsMap),
                        RetrofitUtils.createFilePart("file", selectedImagePath, RetrofitUtils.MEDIA_TYPE_IMAGE_PNG));
            } else {
                call = apiInterface.UploadImageChat(RetrofitUtils.createMultipartRequest(requestValuePairsMap));
            }

            call.enqueue(new Callback<UploadChatImage>() {
                @Override
                public void onResponse(@NonNull Call<UploadChatImage> call, @NonNull Response<UploadChatImage> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        UploadChatImage mode = response.body();
                        if (mode.getCode() == 0) {
                            if (mode.getError() == false) {

                                Log.e("IMAGE_URL_UPLOADED", mode.getResponse().getImageUrl());
                                try {
                                    fromCam = "a";
                                    Message newMessage = new Message();
                                    newMessage.text = "";
                                    newMessage.idSender = userID;
                                    newMessage.idReceiver = roomId;
                                    newMessage.timestamp = System.currentTimeMillis();
                                    newMessage.isRead = "0";
                                    newMessage.isImage = "1";
                                    newMessage.photoUrl = mode.getResponse().getImageUrl();
                                    //newMessage.photoUrl = String.valueOf(taskSnapshot.getDownloadUrl());
                                    sendMessage(newMessage);

                                } catch (Exception e) {
//                            CommonUtils.getInstance().dismissLoadingDialog();
                                    fromCam = "a";
                                    e.printStackTrace();
                                }
/*
                                                    String userimage = String.valueOf(mode.getResponse().getImage());
                                                    String user_id = String.valueOf(mode.getResponse().getId());
                                                    Intent intent = new Intent(context, EmailSendActivity.class);
                                                    intent.putExtra("userId", user_id);
                                                    intent.putExtra("onscreen", "signup");
                                                    startActivity(intent);
                                                    finish();*/
                            } else {
                                CommonUtils.getInstance().showAlertMessage(context, mode.getMessage());
                            }
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<UploadChatImage> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });
        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


}

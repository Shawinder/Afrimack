package com.afrimack.app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.appbeans.AddressFinderBean;
import com.afrimack.app.appbeans.User;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.LoginResponseModel;
import com.afrimack.app.models.SignupResponseModel;
import com.afrimack.app.rest.RetrofitUtils;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.CommonUtils;
import com.google.android.gms.internal.ca;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.afrimack.app.utils.Common.displayErrorDialog;


public class BaseActivity extends AppCompatActivity {
    protected FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    protected EditText edtEmail, edtPass, edtFirstName, edtLastName, edtMobileNo, edtCountryCode;
    private SharedPreferences preferences;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Context context = this;
    public String TAG = BaseActivity.class.getSimpleName();
    StorageReference reference = null;
    StorageReference imagesRef = null;
    UploadTask uploadTask = null;
    DatabaseReference mDatabase = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        //firebase storage
        FirebaseStorage storage = FirebaseStorage.getInstance();
        reference = storage.getReference();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("TAG", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d("TAG", "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        preferences = getSharedPreferences("MyPrefs", MODE_PRIVATE);

    }

    private void setUserId(String userId) {
        Config.setLoginUserUID(userId, context);
    }

    public String getUserId() {
        return Config.getLoginUSERUID(context);
    }


    public void createUserWithEmailAndPassword(final String firstName, final String lastName, final String email,
                                               final byte[] selectedImagePath, final String password, final String device_token, final String lat, final String longi) {


        CommonUtils.getInstance().displayLoadingDialog(false, context);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("TAG", "createUserWithEmail:onComplete:" + task.isSuccessful());
                        //selectedImagePath=;
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.e(TAG, getString(R.string.auth_failed));
                            CommonUtils.getInstance().showAlertMessage(context, task.getException().getLocalizedMessage());
                            CommonUtils.getInstance().dismissLoadingDialog();
                        } else {
                            Log.e(TAG, getString(R.string.auth_success));
                            writeImageToFirebaseStorage(firstName, lastName, email, selectedImagePath, password, device_token, lat, longi);
                        }
                    }
                });
    }

    private void writeImageToFirebaseStorage(final String firstName, final String lastName, final String email,
                                             byte[] file, final String password, final String device_token, final String lat, final String longi) {
        try {
            User user = new User(firstName, lastName, email, "", password, device_token);
            writeMsgToDatabase(user, lat, longi);

        } catch (Exception e) {
            CommonUtils.getInstance().dismissLoadingDialog();
            e.printStackTrace();
        }

    }

    public void signInWithEmailAndPassword(final User user) {
        CommonUtils.getInstance().displayLoadingDialog(false, context);
        mAuth.signInWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        Log.d("TAG", "signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
//                            showProgress(false);
                            Log.e("TAG", "signInWithEmail:failed", task.getException());
                            CommonUtils.getInstance().dismissLoadingDialog();
                            displayErrorDialog("Login Failed. Please try again!", context);
                            return;
                        }
                        setDeviceTokenToFirebase(user);
                        readFromDatabase(user);
                    }
                });
    }

    private void setDeviceTokenToFirebase(User user) {
        String userId;

//        Create user id with FirebaseAuth
        mDatabase = FirebaseDatabase.getInstance().getReference("users");
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            userId = firebaseUser.getUid();
        } else
            userId = mDatabase.push().getKey();  //if firebaseUser is null then create the unique userId with this line

        mDatabase.child(userId).child("deviceToken").setValue(Config.getDeviceToken(context));
    }


    protected void writeMsgToDatabase(User user, final String lat, final String longi) {
        try {
            String userId;

//        Create user id with FirebaseAuth
            mDatabase = FirebaseDatabase.getInstance().getReference("users");
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            if (firebaseUser != null) {
                userId = firebaseUser.getUid();
            } else
                userId = mDatabase.push().getKey();  //if firebaseUser is null then create the unique userId with this line

            setUserId(userId);
            mDatabase.child(userId).setValue(user);

//        // pushing user to 'users' node using the userId
            hitSignUpApi(user, userId, lat, longi);


        } catch (Exception e) {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, "Failed!!! please try again..");
            e.printStackTrace();
        }


    }

    protected void writeMsgToDatabaseSocialLogin(User user) {
        try {
            String userId;

//        Create user id with FirebaseAuth
            mDatabase = FirebaseDatabase.getInstance().getReference("users");
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            if (firebaseUser != null) {
                userId = firebaseUser.getUid();
            } else
                userId = mDatabase.push().getKey();  //if firebaseUser is null then create the unique userId with this line

            setUserId(userId);
            // creating user object
            mDatabase.child(userId).setValue(user);
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void hitSignUpApi(final User user, final String firebaseUserId, final String lat, final String longi) {

        if (CommonUtils.getInstance().isNetConnected(context)) {

            Retrofit retrofit1 = new Retrofit.Builder()
                    .baseUrl(AppConstants.LOC_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ApiInterface apiInterface1 = retrofit1.create(ApiInterface.class);
            Call<AddressFinderBean> call1;
            String latlng = lat + "," + longi;
            call1 = apiInterface1.getCompleteAddress(latlng, true, getString(R.string.geolocation_api_key));
            call1.enqueue(new Callback<AddressFinderBean>() {
                @Override
                public void onResponse(Call<AddressFinderBean> calll, Response<AddressFinderBean> response) {

                    try {

                        AddressFinderBean mObj = response.body();

                        if (mObj != null && mObj.getStatus().equalsIgnoreCase("OK")) {
                            String country = "";
                            String address = "";
                            String fullAddress = mObj.getResults().get(0).getFormattedAddress();
                            try {
                                country = fullAddress.substring(fullAddress.lastIndexOf(",") + 1, fullAddress.length());
                                address = fullAddress;

                                Call<SignupResponseModel> call;
                                OkHttpClient client = new OkHttpClient.Builder()
                                        .connectTimeout(100, TimeUnit.SECONDS)
                                        .readTimeout(100, TimeUnit.SECONDS).build();
                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl(AppConstants.BASE_URL).client(client)
                                        .addConverterFactory(GsonConverterFactory.create(new Gson())).build();
                                ApiInterface apiInterface = retrofit.create(ApiInterface.class);

                                HashMap<String, String> requestValuePairsMap = new HashMap<>();
                                requestValuePairsMap.put("device_token", user.getDeviceToken());
                                requestValuePairsMap.put("email", user.getEmail());
                                requestValuePairsMap.put("password", user.getPassword());
                                requestValuePairsMap.put("first_name", user.getFirstName());
                                requestValuePairsMap.put("last_name", user.getLastName());
                                requestValuePairsMap.put("firebase_UID", firebaseUserId);
                                requestValuePairsMap.put("country", country);
                                requestValuePairsMap.put("address", address);
                                if (SignUp.selectedImagePath != null && !SignUp.selectedImagePath.isEmpty()) {
                                    call = apiInterface.Signup(RetrofitUtils.createMultipartRequest(requestValuePairsMap),
                                            RetrofitUtils.createFilePart("image", SignUp.selectedImagePath, RetrofitUtils.MEDIA_TYPE_IMAGE_PNG));
                                } else {
                                    call = apiInterface.Signup(RetrofitUtils.createMultipartRequest(requestValuePairsMap));
                                }

                                call.enqueue(new Callback<SignupResponseModel>() {
                                    @Override
                                    public void onResponse(@NonNull Call<SignupResponseModel> call, @NonNull Response<SignupResponseModel> response) {

                                        try {
                                            CommonUtils.getInstance().dismissLoadingDialog();
                                            SignupResponseModel mode = response.body();
                                            if (mode.getCode() == 0) {
                                                if (mode.isError() == false) {

                                                    String userimage = String.valueOf(mode.getResponse().getImage());
                                                    String user_id = String.valueOf(mode.getResponse().getId());
                                                    Intent intent = new Intent(context, EmailSendActivity.class);
                                                    intent.putExtra("userId", user_id);
                                                    intent.putExtra("onscreen", "signup");
                                                    startActivity(intent);
                                                    finish();
                                                } else {
                                                    CommonUtils.getInstance().showAlertMessage(context, mode.getMessage());
                                                    deleteUser(user);
                                                }
                                            } else {
                                                deleteUser(user);
                                            }

                                        } catch (Exception e) {
                                            CommonUtils.getInstance().dismissLoadingDialog();
                                            deleteUser(user);
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<SignupResponseModel> call, Throwable t) {
                                        Log.e(TAG, "error: " + t.toString());
                                        CommonUtils.getInstance().dismissLoadingDialog();
                                        deleteUser(user);
                                    }
                                });


                            } catch (Exception e) {
                                CommonUtils.getInstance().dismissLoadingDialog();
                                deleteUser(user);
                                e.printStackTrace();
                            }

                        } else {
                            CommonUtils.getInstance().dismissLoadingDialog();
                            deleteUser(user);
                        }
                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        deleteUser(user);
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<AddressFinderBean> call, Throwable t) {
                    CommonUtils.getInstance().dismissLoadingDialog();
                    deleteUser(user);
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void deleteUser(final User userData) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // Get auth credentials from the user for re-authentication. The example below shows
        // email and password credentials but there are multiple possible providers,
        // such as GoogleAuthProvider or FacebookAuthProvider.
        AuthCredential credential = EmailAuthProvider
                .getCredential(userData.email, userData.password);

        // Prompt the user to re-provide their sign-in credentials
        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        user.delete()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Log.e(TAG, "User account deleted.");
                                        }
                                    }
                                });

                    }
                });
    }

    private void readFromDatabase(final User userData) {
        mDatabase = FirebaseDatabase.getInstance().getReference("users");
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            final String userId = firebaseUser.getUid();

            setUserId(userId);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    // Read from the database
                    mDatabase.child(userId).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User user = dataSnapshot.getValue(User.class);
                            if (user != null) {
                                if (ConstantMain.LOGIN.equalsIgnoreCase("fromLogin")) {
                                    ConstantMain.LOGIN = "";
                                    hitLoginApi(user);

                                }

                            } else {
                                CommonUtils.getInstance().dismissLoadingDialog();
                                CommonUtils.getInstance().showAlertMessage(context, "Authentication failed!!!");
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError error) {
                            // Failed to read value
                            CommonUtils.getInstance().dismissLoadingDialog();
                            Log.w("TAG", "Failed to read value.", error.toException());
                        }
                    });
                }
            }, 1000);

        } else showToast("user is not authenticated");
    }


    public void updateImageToFirebaseDatabase(String type, byte[] uri) {
        if (type.equalsIgnoreCase("image")) {
            updatImageFirebasStorage(uri);
        }

    }

    private void updatImageFirebasStorage(byte[] file) {
        if (file != null) {
            imagesRef = reference.child("userImages/" + Config.getLoginEmailId());
            uploadTask = imagesRef.putBytes(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    e.printStackTrace();
                    CommonUtils.getInstance().dismissLoadingDialog();
                    Toast.makeText(context, "Error : " + e.toString(), Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if (taskSnapshot != null) {
                        try {
                            String image = String.valueOf(taskSnapshot.getDownloadUrl());
                            mDatabase = FirebaseDatabase.getInstance().getReference("users");
                            mDatabase.child(Config.getLoginUSERUID(context)).child("picture").setValue(image);

                        } catch (Exception e) {
                            CommonUtils.getInstance().dismissLoadingDialog();
                            e.printStackTrace();
                        }

                    }
                }
            });
        }
    }

    private void hitLoginApi(final User user) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<LoginResponseModel> call = apiInterface.Login(user.getDeviceToken(), user.getEmail(), user.getPassword());

            call.enqueue(new Callback<LoginResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<LoginResponseModel> call, @NonNull Response<LoginResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        LoginResponseModel model = response.body();
                        if (model.isError() == false) {

                            if (model.getCode() == 0) {
                                Config.setLoginUserUID(model.getResponse().getFirebase_UID(), context);

                                User users = null;

                                users = new User(model.getResponse().getFirst_name(), model.getResponse().getLast_name(), model.getResponse().getEmail(), model.getResponse().getImage(), user.getPassword(), user.getDeviceToken(), "1");

                                writeMsgToDatabaseSocialLogin(users);

//                                ServiceUtils.updateLoginUserStatus(context, "1");
                                Config.setUserName(model.getResponse().getFirst_name(), context);
                                Config.setUserId(String.valueOf(model.getResponse().getId()), context);

                                Log.e("USER_ID", String.valueOf(model.getResponse().getId()));
                                Config.setSingleRegister(model.getResponse().getSingle_registered(), context);
                                Config.setLoginUserName(model.getResponse().getFirst_name() + " " + model.getResponse().getLast_name());
                                Config.setLoginEmailId(model.getResponse().getEmail());
                                Config.setLoginProfileImg(model.getResponse().getImage(), context);
                                Config.setLoginType("email", context);


                                Intent intenthome = new Intent(context, ActivityHome.class);
                                intenthome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intenthome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intenthome);
                                finish();
                            }

                        } else {
                            if (model.getEmailverfied().equalsIgnoreCase("N") && !model.getUser_id().equalsIgnoreCase("")) {
                                Intent intent = new Intent(context, EmailSendActivity.class);
                                intent.putExtra("userId", model.getUser_id());
                                intent.putExtra("onscreen", "login");
                                startActivity(intent);
                            } else {
                                displayErrorDialog(model.getMessage(), context);
                            }


                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }


    public void showToast(String msg) {
        Toast.makeText(this, "" + msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

}

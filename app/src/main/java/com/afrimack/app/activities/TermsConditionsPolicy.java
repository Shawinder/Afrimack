package com.afrimack.app.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.TCPResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afrimack.app.utils.Common.dismissLoadingDialog;
import static com.afrimack.app.utils.Common.displayErrorDialog;
import static com.afrimack.app.utils.Common.displayLoadingDialog;

public class TermsConditionsPolicy extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel;
    private String slug = "terms-conditions";
    //private String slug = "policy";
    private WebView web_polcy;
    private String TAG = TermsConditionsPolicy.class.getSimpleName();
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions_policy);


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //post_id= null;
            } else {
                slug = extras.getString("slug");
            }
        } else {
        }

        hitTermaAndPolicyApi();
        init();
    }

    private void hitTermaAndPolicyApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<TCPResponseModel> call = apiInterface.TermAndPolicy(slug);
            call.enqueue(new Callback<TCPResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<TCPResponseModel> call, @NonNull Response<TCPResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        TCPResponseModel model = response.body();
                        if (model.isError() == false) {
                            tv_titel.setText(model.getResponse().getTitle());
                            String ss = model.getResponse().getDescription();
                            web_polcy.loadData(ss, "text/html; charset=utf-8", "utf-8");
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<TCPResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
//        img_tick = (ImageView) findViewById(R.id.img_tick);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
//        img_tick.setVisibility(View.GONE);
        img_back.setOnClickListener(this);

        //************end titel*****************//

        web_polcy = (WebView) findViewById(R.id.web_polcy);

    }


    @Override
    public void onClick(View v) {

        if (v == img_back) {
            onBackPressed();
        }
    }
}

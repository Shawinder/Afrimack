package com.afrimack.app.activities;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.DetailOfferAdapter;
import com.afrimack.app.adapters.DetailQuestionAdapter;
import com.afrimack.app.adapters.SlidingImageEventDetailAdapter;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.models.PostImageResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afrimack.app.utils.Common.displayErrorDialog;
import static com.afrimack.app.utils.Common.displayLoadingDialog;
import static com.google.android.gms.maps.model.BitmapDescriptorFactory.defaultMarker;

public class FullImageActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel;
    private ImageView full_image;
    private String post_id;

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private List<String> ImagesArray = new ArrayList<>();
    private String TAG = FullImageActivity.class.getSimpleName();
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        post_id = Config.getPostId(this);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                post_id = null;
            } else {
                post_id = extras.getString("post_id");
            }
        } else {
        }
        init();
        mPager = (ViewPager) findViewById(R.id.event_pager);
        hitApiImages();





    }

    private void hitApiImages() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PostImageResponseModel> call = apiInterface.PostImage(post_id);
            call.enqueue(new Callback<PostImageResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<PostImageResponseModel> call, @NonNull Response<PostImageResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        PostImageResponseModel imageResponseModel = response.body();
                        if (imageResponseModel.isError() == false) {
                            if (imageResponseModel.getResponse().size() > 0)

                                for (int i = 0; i < imageResponseModel.getResponse().size(); i++) {
                                    // ImagesArray.add(resBean.getEventImageUrl().get(i));
                                    ImagesArray.add(imageResponseModel.getResponse().get(i).getImage());
                                }
                            mPager.setAdapter(new SlidingImageEventDetailAdapter(context, ImagesArray));
                            CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.event_indicator);

                            indicator.setViewPager(mPager);

                            final float density = getResources().getDisplayMetrics().density;

                            indicator.setRadius(5 * density);


                            NUM_PAGES = ImagesArray.size();


                            // Auto start of viewpager
                            final Handler handler = new Handler();
                            final Runnable Update = new Runnable() {
                                public void run() {
                                    if (currentPage == NUM_PAGES) {
                                        currentPage = 0;
                                    }
                                    mPager.setCurrentItem(currentPage++, true);
                                }
                            };
                            // Pager listener over indicator
                            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                @Override
                                public void onPageSelected(int position) {
                                    currentPage = position;

                                }

                                @Override
                                public void onPageScrolled(int pos, float arg1, int arg2) {

                                }

                                @Override
                                public void onPageScrollStateChanged(int pos) {

                                }
                            });


                        } else {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PostImageResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void init() {
        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.app_titel).toString());
        img_back.setOnClickListener(this);

        //************end titel*****************//

        full_image = (ImageView) findViewById(R.id.full_image);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;

        }
    }

}

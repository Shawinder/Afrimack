package com.afrimack.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.appbeans.User;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.LoginResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;

import static com.afrimack.app.rest.ApiIds.LOGIN;
import static com.afrimack.app.utils.Common.dismissLoadingDialog;
import static com.afrimack.app.utils.Common.displayErrorDialog;
import static com.afrimack.app.utils.Common.displayLoadingDialog;

public class Login extends BaseActivity implements View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel;
    private EditText et_email, et_password;
    private TextView tv_forgot_password;
    private Button login_btnsignup, login_btn_login;

    private static final String TAG = "Login";
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private String email, password;
    private TextView tv_show_hide;
    private boolean showPwdFlag = false;
    private String device_token;
    private Context context = this;
    private String lat = "", longi = "", country = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //********chat********************//
        Config.init(this);
        device_token = Config.getDeviceToken(this);
        try {
            lat = Config.getlatitude(this);
            longi = Config.getlongitude(this);
            try {
                country = CommonUtils.getInstance().getCountryName(context, Double.valueOf(lat), Double.valueOf(longi));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
    }

    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        //   img_tick = (ImageView) findViewById(R.id.img_tick);

        //  img_tick.setVisibility(View.GONE);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.app_titel).toString());
        img_back.setOnClickListener(this);

        //************end titel*****************//

        et_email = (EditText) findViewById(R.id.et_email_login);
        et_password = (EditText) findViewById(R.id.et_password_login);
        tv_forgot_password = (TextView) findViewById(R.id.tv_forgot_password_login);
        login_btnsignup = (Button) findViewById(R.id.btnsignup_login);
        login_btn_login = (Button) findViewById(R.id.btn_login);
        tv_show_hide = (TextView) findViewById(R.id.tv_show_login);

        tv_forgot_password.setOnClickListener(this);
        login_btnsignup.setOnClickListener(this);
        login_btn_login.setOnClickListener(this);
        tv_show_hide.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, SplashLogin.class);
        startActivity(intent);
        finish();
        // super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        if (v == img_back) {
            Intent intent = new Intent(this, SplashLogin.class);
            startActivity(intent);
            finish();
        }
        if (v == tv_forgot_password) {
            v.startAnimation(AppConstants.buttonClick);
            Intent forgotInt = new Intent(this, ActivityForgot.class);
            startActivity(forgotInt);
        }
        if (v == login_btnsignup) {
            try {
                v.startAnimation(AppConstants.buttonClick);
                Intent intent = new Intent(this, SignUp.class);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (v == login_btn_login) {
            v.startAnimation(AppConstants.buttonClick);
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
            email = et_email.getText().toString().trim();
            password = et_password.getText().toString().trim();

            if (email.length() == 0 && password.length() == 0) {

                displayErrorDialog(getResources().getString(R.string.required_field), this);

            } else if (email.length() == 0) {

                displayErrorDialog(getResources().getString(R.string.fill_email), this);
            } else if (password.length() == 0) {

                displayErrorDialog(getResources().getString(R.string.fill_password), this);

            } else if (!email.matches(EMAIL_REGEX)) {

                displayErrorDialog(getResources().getString(R.string.invalid_email), this);
            } else {
                if (ConnectionDetector.isNetAvail(this)) {

                    User user = new User(email, password, device_token);
                    ConstantMain.LOGIN = "fromLogin";
                    signInWithEmailAndPassword(user);

                } else {
                    displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                }
            }

        }
        if (v == tv_show_hide) {
            if (et_password.getText().length() != 0) {
                showPwd();
            }
        }
    }

    private void showPwd() {
        if (showPwdFlag) {
            tv_show_hide.setText(getResources().getString(R.string.show));
            et_password.setInputType(129);
            showPwdFlag = false;
        } else {
            tv_show_hide.setText(getResources().getString(R.string.hide));
            et_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            showPwdFlag = true;
        }
    }


}

package com.afrimack.app.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.AfrimackApp;
import com.afrimack.app.R;
import com.afrimack.app.appbeans.ChatRequestBean;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.SingleProfileResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleProfileViewActivity extends AppCompatActivity implements View.OnClickListener
       /* EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener*/ {

    private ImageView img_back, img_star;

    private TextView tv_titel_filter, tv_about_me, tv_interests, tv_profile_last_update, post_user_name;
    private TextView age, country, place, tv_report_inappropriate;
//    private ImageView gengder;

    private ImageView post_user_image;
    private TextView tv_i_am, i_am_tell;

    private String single_user_id, user_id;
    private Button bt_chat;
    private SingleProfileResponseModel singlemodel;

    private LinearLayout lay_singale_view, lay_single_fram;
    private TextView tv_follow;
    String UserFoliow;
    private Dialog dialog;
    private TextView tv_image_date;
    private Context context = this;
    public String TAG = SingleProfileViewActivity.class.getSimpleName();
    private String offerUserFirebaseUID = "";
    private List<String> lisFriendsIds = null;
    String roomId = "";
    private int blockedStatus = 0;
    private int blocked_by = 0;
    private int php_block_id = 0;
    private String chatStatus = "0";
    private String From="";
    public static SingleProfileViewActivity singleProfileViewActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_profile_view);

        single_user_id = Config.getSingleChatId(this);
        user_id = Config.getUserId(this);
        Log.e("single_user_id ", single_user_id);
        Log.e("user_id ", user_id);
        From = getIntent().getStringExtra("from");
        singleProfileViewActivity = this;

        init();
        hitProfileApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AfrimackApp.curentactivity = this;
    }

    private void hitProfileApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SingleProfileResponseModel> call = apiInterface.SingleProfile(single_user_id, user_id);
            call.enqueue(new Callback<SingleProfileResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<SingleProfileResponseModel> call, @NonNull Response<SingleProfileResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        singlemodel = response.body();
                        if (!single_user_id.equalsIgnoreCase(user_id)) {
                            //chatStatus=0(request pending),1(chat Request confirmed),2(chat request ignored),3(chat conversation started)
                            chatStatus = String.valueOf(singlemodel.getResponse().getChat_request_status());
                            if (singlemodel.isError() == false) {
                                if (singlemodel.getResponse().getChat_request_status() == 1 || singlemodel.getResponse().getChat_request_status() == 3) {
                                    bt_chat.setText("CHAT");
                                } else {
                                    bt_chat.setText("SEND CHAT REQUEST");
                                }
                            }
                            blocked_by = singlemodel.getResponse().getPhp_block_by();
                            blockedStatus = singlemodel.getResponse().getBlockedStatus();
                            php_block_id = singlemodel.getResponse().getPhp_block_id();
                            offerUserFirebaseUID = singlemodel.getResponse().getFirebase_UID();
                            tv_profile_last_update.setText("Last profile update on " + singlemodel.getResponse().getProfile_date());
                            Picasso
                                    .with(context)
                                    .load(singlemodel.getResponse().getImage())
                                    .placeholder(R.drawable.dami_image2)
                                    .into(post_user_image);


                            tv_about_me.setText(singlemodel.getResponse().getAbout_me());
                            post_user_name.setText(singlemodel.getResponse().getFirst_name());
                            age.setText(singlemodel.getResponse().getAge() + " years");
                            country.setText(singlemodel.getResponse().getCountry_name());
                            tv_i_am.setText(singlemodel.getResponse().getLooking_for());

                            if (singlemodel.getResponse().getFollow().equalsIgnoreCase("Y")) {
                                tv_follow.setText("- UnFollow");
                                UserFoliow = "0";
                            } else {
                                tv_follow.setText("+ Follow");
                                UserFoliow = "1";
                            }
                            if (singlemodel.getResponse().getInterests() != null && !singlemodel.getResponse().getInterests().equalsIgnoreCase("")) {
                                String text = singlemodel.getResponse().getInterests().toString();
                                try {
                                    if (text.contains(",")) {
                                        String replacedText = text.replaceAll(",", "|");
                                        tv_interests.setText(String.valueOf(replacedText));
                                    } else {
                                        tv_interests.setText(String.valueOf(text));
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                tv_interests.setText("N/A");
                            }


                        } else {

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SingleProfileResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void init() {

        // rela_main = (LinearLayout) findViewById(R.id.rela_main);

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back_dis);
        img_star = (ImageView) findViewById(R.id.img_star);
        img_star.setVisibility(View.GONE);
        img_back.setOnClickListener(this);
        img_star.setOnClickListener(this);
        post_user_name = (TextView) findViewById(R.id.post_user_name);
        age = (TextView) findViewById(R.id.tv_age);
        age.setVisibility(View.VISIBLE);
        country = (TextView) findViewById(R.id.tv_country);
//        place = (TextView) findViewById(R.id.tv_place);
        country.setVisibility(View.VISIBLE);

        post_user_image = (ImageView) findViewById(R.id.post_user_image);
        tv_about_me = (TextView) findViewById(R.id.tv_about_me);
        tv_i_am = (TextView) findViewById(R.id.tv_i_am);
        tv_interests = (TextView) findViewById(R.id.tv_interests);
        tv_profile_last_update = (TextView) findViewById(R.id.tv_profile_last_update);
        tv_report_inappropriate = (TextView) findViewById(R.id.tv_report_inappropriate);
        tv_report_inappropriate.setOnClickListener(this);
        bt_chat = (Button) findViewById(R.id.bt_chat);

        bt_chat.setOnClickListener(this);
        post_user_image.setOnClickListener(this);
        tv_follow = (TextView) findViewById(R.id.tv_follow);
        tv_follow.setOnClickListener(this);
        if (single_user_id.equalsIgnoreCase(user_id)) {
            bt_chat.setText("OK");
            tv_report_inappropriate.setVisibility(View.INVISIBLE);

//            bt_chat.setVisibility(View.INVISIBLE);
            tv_follow.setVisibility(View.INVISIBLE);
        } else {
            if (chatStatus.equalsIgnoreCase("1") || chatStatus.equalsIgnoreCase("3")) {
                bt_chat.setText("CHAT");
            } else {
                bt_chat.setText("SEND CHAT REQUEST");
            }
            tv_report_inappropriate.setVisibility(View.VISIBLE);
            bt_chat.setVisibility(View.VISIBLE);
            tv_follow.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (From != null) {
         /*   if (From.equalsIgnoreCase("SingleDation")) {
                finish();
            }
            else if(From.equalsIgnoreCase("chat"))
            {
                finish();
            }*/
         finish();

        } else {
            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                Intent intent = new Intent(this, OfferUserActivity.class);
                intent.putExtra("other_user_id", single_user_id);
                //Config.setFollow(USERFOLLOW, this);
//                        Config.setOfferUserID(follower_id, this);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_dis:
                // onBackPressed();
                if (From != null) {
                    finish();
               /*     if (From.equalsIgnoreCase("SingleDation")) {
                        finish();
                    }
                    else if(From.equalsIgnoreCase("chat"))
                    {
                        finish();
                    }*/
                }
                else {
                    if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                        Intent intent = new Intent(this, OfferUserActivity.class);
                        intent.putExtra("other_user_id", single_user_id);
                        //Config.setFollow(USERFOLLOW, this);
//                        Config.setOfferUserID(follower_id, this);
                        startActivity(intent);
                        finish();
                    }
                }
                break;

            case R.id.tv_report_inappropriate:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SingleProfileViewActivity.this);

                alertDialog.setMessage("Are you sure you want to report this as inappropriate?");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        hitReportPostApi();
                    }
                });

                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.show();
                break;

            case R.id.bt_chat:
                v.startAnimation(AppConstants.buttonClick);
                if (bt_chat.getText().toString().equalsIgnoreCase("OK")) {
                    SingleProfileViewActivity.this.finish();
                } else {
                    if (bt_chat.getText().toString().equalsIgnoreCase("SEND CHAT REQUEST")) {

                        AlertDialog.Builder alert_dialog_remind= new AlertDialog.Builder(SingleProfileViewActivity.this);

                        // Setting Dialog Title
                        alert_dialog_remind.setTitle("Send Request");

                        // Setting Dialog Message
                        alert_dialog_remind.setMessage("Are you sure you want to send chat request? ");


                        // Setting Positive "Yes" Button
                        alert_dialog_remind.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                hitPlaceRequestApi();

                            }
                        });

                        // Setting Negative "NO" Button
                        alert_dialog_remind.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.cancel();
                            }
                        });

                        // Showing Alert Message
                        alert_dialog_remind.show();





                    } else {
                        roomId = offerUserFirebaseUID.compareTo(Config.getLoginUSERUID(context)) > 0 ? (Config.getLoginUSERUID(context) + offerUserFirebaseUID).hashCode() + "" : "" + (offerUserFirebaseUID + Config.getLoginUSERUID(context)).hashCode();
                        getDataFromFirebase();
                    }
                }


//                CommonUtils.getInstance().showAlertMessage(context, "Under Developement");

                break;
            case R.id.tv_follow:
                v.startAnimation(AppConstants.buttonClick);

//                CommonUtils.getInstance().showAlertMessage(context, "Under Developement");

                if (UserFoliow.equalsIgnoreCase("1")) {
                    if (ConnectionDetector.isNetAvail(this)) {
                        hitFollowApi();
                    } else {
                        Common.displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                    }
                } else {
                    if (ConnectionDetector.isNetAvail(this)) {
                        hitUnFollowApi();
                    } else {
                        Common.displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                    }

                }
                break;

            case R.id.post_user_image:
                // Log.e("ImgPath", imgPath);

                try {
                    if (From != null) {
                      //  if (From.equalsIgnoreCase("SingleDation")) {
                            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                                Intent intent = new Intent(this, OfferUserActivity.class);
                                intent.putExtra("other_user_id", single_user_id);
                                intent.putExtra("from", "Single profile");
                                //Config.setFollow(USERFOLLOW, this);
//                        Config.setOfferUserID(follower_id, this);
                                startActivity(intent);
//                                finish();
                            }

                      //  }

                    }

                    else {

                        if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                            Intent intent = new Intent(this, OfferUserActivity.class);
                            intent.putExtra("other_user_id", single_user_id);
                            //Config.setFollow(USERFOLLOW, this);
//                        Config.setOfferUserID(follower_id, this);
                            startActivity(intent);
//                            finish();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }


    private void hitPlaceRequestApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ChatRequestBean> call = apiInterface.placeChatRequest(user_id, single_user_id);
            call.enqueue(new Callback<ChatRequestBean>() {
                @Override
                public void onResponse(@NonNull Call<ChatRequestBean> call, @NonNull Response<ChatRequestBean> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();

                        roomId = offerUserFirebaseUID.compareTo(Config.getLoginUSERUID(context)) > 0 ? (Config.getLoginUSERUID(context) + offerUserFirebaseUID).hashCode() + "" : "" + (offerUserFirebaseUID + Config.getLoginUSERUID(context)).hashCode();
                        getDataFromFirebase();
                        ChatRequestBean beanObj = response.body();
                        dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.view_notification_dialog);
                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog.setCancelable(false);
                        TextView mess_titel = (TextView) dialog.findViewById(R.id.mess_titel);
                        TextView mess_info = (TextView) dialog.findViewById(R.id.mess_info);
                        TextView header_title = (TextView) dialog.findViewById(R.id.header_title);
                        mess_titel.setVisibility(View.GONE);
                        header_title.setText("Info");
//                        mess_titel.setText(String.valueOf(ContactAdapter.notificationList.get(pos).getTitle()));
//                        if (ContactAdapter.notificationList.get(pos).getTitle() != null) {
                        mess_info.setText(beanObj.getMessage());
//                        }

                        TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);


                        tv_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });

                        dialog.show();


                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ChatRequestBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void getDataFromFirebase() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("friend");
            // Read from the database
            mDatabase.child(Config.getLoginUSERUID(context)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        if (dataSnapshot.getValue() != null) {
                            lisFriendsIds = new ArrayList<String>();
                            HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                            String value, substrVal = null;
                            String key = "";
                            Iterator myVeryOwnIterator = mapMessage.keySet().iterator();
                            while (myVeryOwnIterator.hasNext()) {
                                key = (String) myVeryOwnIterator.next();
                                value = (String) mapMessage.get(key);
                                substrVal = value.substring(value.indexOf(",") + 1, value.length());
                                lisFriendsIds.add(substrVal);
                            }

                            Log.e("NEW_VALUE_PROFILE",Config.getSingleRegister(context));
                            if (Config.getSingleRegister(context).equalsIgnoreCase("Y")) {

                                if (offerUserFirebaseUID != null & !offerUserFirebaseUID.equalsIgnoreCase("")) {
                                    if (lisFriendsIds != null && lisFriendsIds.contains(roomId)) {
                                        if (bt_chat.getText().toString().equalsIgnoreCase("CHAT")) {
                                            Intent mIntent = new Intent(context, SingleChatActivity.class);
                                            mIntent.putExtra("U_ID", offerUserFirebaseUID);
                                            mIntent.putExtra("other_user_id", Integer.valueOf(single_user_id));
                                            mIntent.putExtra("blocked_status", blockedStatus);
                                            mIntent.putExtra("blocked_by", blocked_by);
                                            mIntent.putExtra("php_block_id", php_block_id);
                                            mIntent.putExtra("user_image", singlemodel.getResponse().getImage());
                                            mIntent.putExtra("from", "from");
                                            startActivity(mIntent);
                                        }
                                    } else {
                                        addFriend(offerUserFirebaseUID, true);
                                    }


                                } else {
                                    CommonUtils.getInstance().showAlertMessage(context, "Not Authorised!!!");
                                }
                            } else {
                                CommonUtils.getInstance().showAlertMessage(context, "Please create your single dating profile first");
                            }
                        } else {
                            addFriend(offerUserFirebaseUID, true);
                        }


                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    CommonUtils.getInstance().dismissLoadingDialog();
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });
        } else {
            CommonUtils.getInstance().showAlertMessage(context, "Please ensure internet connectivity.");
        }
    }


    private void hitReportPostApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.ReportUser(single_user_id, user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        if (commanResponseModel.isError() == false) {
                            Common.displayLongToast(context, commanResponseModel.getMessage());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void addFriend(final String idFriend, boolean isIdFriend) {
        if (idFriend != null) {
            if (isIdFriend) {
                FirebaseDatabase.getInstance().getReference().child("friend/" + Config.getLoginUSERUID(context)).push().setValue(offerUserFirebaseUID + "," + roomId)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    addFriend(idFriend, false);
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        });
            } else {
                FirebaseDatabase.getInstance().getReference().child("friend/" + idFriend).push().setValue(Config.getLoginUSERUID(context) + "," + roomId).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            addFriend(null, false);
                        }
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        });
            }
        } else {
//            FriendDB.getInstance(context).addFriend(roomId);
            if (bt_chat.getText().toString().equalsIgnoreCase("CHAT")) {
                Intent mIntent = new Intent(context, SingleChatActivity.class);
                mIntent.putExtra("U_ID", offerUserFirebaseUID);
                mIntent.putExtra("other_user_id", Integer.valueOf(single_user_id));
                mIntent.putExtra("blocked_status", blockedStatus);
                mIntent.putExtra("blocked_by", blocked_by);
                mIntent.putExtra("user_image", singlemodel.getResponse().getImage());
                mIntent.putExtra("php_block_id", php_block_id);
                mIntent.putExtra("from", "from");
                startActivity(mIntent);
            }
        }
    }

    private void hitUnFollowApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.UserUnFollow(user_id, single_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            tv_follow.setText("+ Follow");
                            UserFoliow = "1";
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitFollowApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.UserFollow(user_id, single_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            tv_follow.setText("- UnFollow");
                            UserFoliow = "0";

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    public void notifyData() {
        hitProfileApi();
    }
}

package com.afrimack.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.BlockUserAdapter;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.BlockUserModel;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlockUserActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_tick, img_back;
    private TextView tv_titel;

    private RecyclerView rec_block;
    private LinearLayoutManager layoutManager;
    private String user_id;
    private BlockUserAdapter blockUserAdapter;
    View.OnClickListener onClickListener;
    BlockUserModel model;
    private TextView block_user;
    private Context context = this;
    public String TAG = BlockUserActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_user);
        user_id = Config.getUserId(this);
        onClickListener = this;
        init();
        hitBlockApi();
    }

    private void hitBlockApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<BlockUserModel> call = apiInterface.blockUser(user_id);
            call.enqueue(new Callback<BlockUserModel>() {
                @Override
                public void onResponse(@NonNull Call<BlockUserModel> call, @NonNull Response<BlockUserModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        model = response.body();
                        if (model.isError() == false) {
                            if (model.getResponse() != null) {

                                if (model.getResponse().size() > 0) {
                                    // followingListl = new ArrayList<FollowingResponseModel>();
                                    Config.setBlockTotel(String.valueOf(model.getResponse().size()), context);
                                    blockUserAdapter = new BlockUserAdapter(context, model.getResponse(), onClickListener);
                                    rec_block.setAdapter(blockUserAdapter);

                                } else {
                                    Config.setBlockTotel("", context);
                                    rec_block.setVisibility(View.GONE);
                                    block_user.setVisibility(View.VISIBLE);
                                    block_user.setText(getResources().getString(R.string.data_nota));
                                    // Common.displayLongToast(this,getResources().getString(R.string.data_nota));

                                }

                            }

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BlockUserModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void init() {

        img_back = (ImageView) findViewById(R.id.img_back);
//        img_tick = (ImageView) findViewById(R.id.img_tick);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
//        img_tick.setVisibility(View.GONE);
        img_back.setOnClickListener(this);
        tv_titel.setText(R.string.blocked_users);

        rec_block = (RecyclerView) findViewById(R.id.rec_block);

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rec_block.setLayoutManager(layoutManager);

        block_user = (TextView) findViewById(R.id.block_user);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_unblock:

                int po = (int) v.getTag();

                String blockuser_id = String.valueOf(model.getResponse().get(po).getBlock_id());
                hitUnblockApi(user_id, blockuser_id);
                break;
            case R.id.img_back:
                finish();
                break;
        }

    }

    private void hitUnblockApi(String user_id, String blockuser_id) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.unBlockUser(user_id, blockuser_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel response1 = response.body();
                        Common.displayLongToast(context, response1.getMessage());
                        if (response1.isError() == false) {
                            hitBlockApi();


                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


}

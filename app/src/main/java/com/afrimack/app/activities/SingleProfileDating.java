package com.afrimack.app.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.adapters.InterestsAdapter;
import com.afrimack.app.appbeans.AddressFinderBean;
import com.afrimack.app.appbeans.DiscoverResponseModel;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.CountryListMode;
import com.afrimack.app.models.SignupResponseModel;
import com.afrimack.app.models.SingleProfileModel;
import com.afrimack.app.models.SingleResponseModel;
import com.afrimack.app.rest.RetrofitUtils;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.services.Locationservice;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.afrimack.app.utils.Common.checkPermissions;
import static com.afrimack.app.utils.Common.displayErrorDialog;

public class SingleProfileDating extends BaseActivity implements View.OnClickListener, InterestsAdapter.OnItemCheckListener {

    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;
    private RadioGroup rgGender;
    private RadioButton rbMale;
    private RadioButton rbFemale;
    private TextView etAge, tv_interests;
    private EditText etLookingFor;
    private EditText etTellingAbout;
    private Button btSingIn;
    private String user_id, gender, looking_for, about_me, dob = "";
    private String lat, longi;
    private ImageView single_profile;


    Calendar myCalendar = Calendar.getInstance();
    Calendar c;
    private String formattedDate;

    private PopupWindow popupWindow;
    private Dialog dialog;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static BitmapFactory.Options bitmap_factory;
    private static Uri uri;
    private static Bitmap bm;
    private String selectedImagePath = null;
    private String imgString = null, imgString1;
    private ByteArrayOutputStream stream;
    byte[] imageInByte;
    private TextView tv_delete_single_profile;

    LocationManager locationManager;
    private boolean isDataRecieved = false;

    private Spinner spi_country;
    private CountryListMode cointrymode;
    private String country_id = null;
    private ArrayList<String> country;
    private ArrayAdapter<String> countryAdapter;
    int ci;
    private Context context = this;
    private String TAG = SingleProfileDating.class.getSimpleName();
    private String type = "";
    private AlertDialog gpsAlertDialog;
    private boolean isGpsDialogShowing = false;
    private Spinner sp_interests;
    private RecyclerView recy_interest_list;
    private GridLayoutManager mLayoutManager;
    private InterestsAdapter interestsAdapter;
    private ArrayList<String> interestsList;
    private ArrayAdapter<String> interestArrayAdapter;
    private int pos;
    private DiscoverResponseModel obj = null;
    private Button btn_ok;
    private List<String> currentSelectedItems = new ArrayList<>();
    private List<String> currentCheckedItems = new ArrayList<>();
    private String intereststext = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_profile);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        user_id = getIntent().getStringExtra("id");
        init();

        c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c.getTime());
        hitApi();
        hitCountryDataApi();
        hitGetDataApi();


    }

    private void hitApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<DiscoverResponseModel> call = apiInterface.Discover(user_id);
            call.enqueue(new Callback<DiscoverResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<DiscoverResponseModel> call, @NonNull Response<DiscoverResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        if (response.body() != null) {
                            obj = response.body();
                            if (!obj.isError()) {
                                interestsList = new ArrayList<String>();
                                for (int i = 0; i < obj.getResponse().getInterests().size(); i++) {
                                    interestsList.add(obj.getResponse().getInterests().get(i).getCategoryName());
                                }
                                interestArrayAdapter = new ArrayAdapter<String>(context, R.layout.spinner_layout_view, interestsList);
                                interestArrayAdapter.setDropDownViewResource(R.layout.spinner_text_view);
                                sp_interests.setAdapter(interestArrayAdapter);
//                                InterestsAdapter myAdapter = new InterestsAdapter(SingleProfileDating.this, 0,
//                                        obj.getResponse().getInterests());
//                                sp_interests.setAdapter(myAdapter);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<DiscoverResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                }
            });


        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitGetDataApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SingleProfileModel> call = apiInterface.SingleProfileUser(user_id);
            call.enqueue(new Callback<SingleProfileModel>() {
                @Override
                public void onResponse(@NonNull Call<SingleProfileModel> call, @NonNull Response<SingleProfileModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        try {
                            SingleProfileModel singleProfileModel = response.body();

                            if (singleProfileModel.isError() == false) {

                                if (singleProfileModel.getSingle_registered().equalsIgnoreCase("P")) {
                                    dialogMessage("Your entry will be online within the next 12 hours.");
                                } else if (singleProfileModel.getSingle_registered().equalsIgnoreCase("Y")) {
                                    if (!singleProfileModel.getResponse().getImage().equalsIgnoreCase("")) {
                                        Picasso.with(context)
                                                .load(singleProfileModel.getResponse().getImage())
                                                .placeholder(R.drawable.user_round)
                                                .error(R.drawable.user_round)
                                                .into(single_profile);
                                    }
                                    if (singleProfileModel.getResponse().getDob() != null && !singleProfileModel.getResponse().getDob().equalsIgnoreCase("")) {
                                        if (singleProfileModel.getResponse().getGender().equalsIgnoreCase("M")) {
                                            rbMale.setChecked(true);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                rbMale.setButtonTintList(getResources().getColorStateList(R.color.color_black));
                                            }
                                            rbMale.setTextColor(getResources().getColor(R.color.color_black));
                                            rbFemale.setChecked(false);
                                        } else if (singleProfileModel.getResponse().getGender().equalsIgnoreCase("F")) {
                                            rbFemale.setChecked(true);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                rbFemale.setButtonTintList(getResources().getColorStateList(R.color.color_black));
                                            }
                                            rbFemale.setTextColor(getResources().getColor(R.color.color_black));
                                            rbMale.setChecked(false);
                                        }
                                    }
                                    if (user_id.equalsIgnoreCase(Config.getUserId(context))) {
                                        if (singleProfileModel.getResponse().getSingle_registered().equalsIgnoreCase("Y")) {
                                            tv_delete_single_profile.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        tv_delete_single_profile.setVisibility(View.GONE);
                                    }

                                    if (singleProfileModel.getResponse().getDob() != null && !singleProfileModel.getResponse().getDob().equalsIgnoreCase("")) {
                                        etAge.setText(singleProfileModel.getResponse().getDob());

                                    }
                                    if (singleProfileModel.getResponse().getAbout_me() != null && !singleProfileModel.getResponse().getAbout_me().equalsIgnoreCase("")) {
                                        etTellingAbout.setText(singleProfileModel.getResponse().getAbout_me());
                                    }
                                    if (singleProfileModel.getResponse().getLooking_for() != null && !singleProfileModel.getResponse().getLooking_for().equalsIgnoreCase("")) {
                                        etLookingFor.setText(singleProfileModel.getResponse().getLooking_for());
                                    }

                                    if (singleProfileModel.getResponse().getInterests() != null && !singleProfileModel.getResponse().getInterests().equalsIgnoreCase("")) {
                                        intereststext = singleProfileModel.getResponse().getInterests().toString();
                                        try {
                                            if (intereststext.contains(",")) {
                                                currentCheckedItems = Arrays.asList(intereststext.split("\\s*,\\s*"));
                                                currentSelectedItems.addAll(currentCheckedItems);
                                                String replacedText = intereststext.replaceAll(",", "|");
                                                tv_interests.setText(String.valueOf(replacedText));

                                            } else {
                                                currentSelectedItems.add(intereststext);
                                                tv_interests.setText(String.valueOf(intereststext));
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                } else {
                                    if (!singleProfileModel.getResponse().getImage().equalsIgnoreCase("")) {
                                        Picasso.with(context)
                                                .load(singleProfileModel.getResponse().getImage())

                                                .placeholder(R.drawable.user_round)
                                                .error(R.drawable.user_round)
                                                .into(single_profile);
                                    }
                                    if (singleProfileModel.getResponse().getDob() != null && !singleProfileModel.getResponse().getDob().equalsIgnoreCase("")) {
                                        if (singleProfileModel.getResponse().getGender().equalsIgnoreCase("M")) {
                                            rbMale.setChecked(true);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                rbMale.setButtonTintList(getResources().getColorStateList(R.color.color_black));
                                            }
                                            rbMale.setTextColor(getResources().getColor(R.color.color_black));
                                            rbFemale.setChecked(false);
                                        } else if (singleProfileModel.getResponse().getGender().equalsIgnoreCase("F")) {
                                            rbFemale.setChecked(true);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                rbFemale.setButtonTintList(getResources().getColorStateList(R.color.color_black));
                                            }
                                            rbFemale.setTextColor(getResources().getColor(R.color.color_black));
                                            rbMale.setChecked(false);

                                        }
                                    }
                                    if (user_id.equalsIgnoreCase(Config.getUserId(context))) {
                                        if (singleProfileModel.getResponse().getSingle_registered().equalsIgnoreCase("Y")) {
                                            tv_delete_single_profile.setVisibility(View.VISIBLE);
                                        } else {
                                            tv_delete_single_profile.setVisibility(View.GONE);
                                        }
                                    } else {
                                        tv_delete_single_profile.setVisibility(View.GONE);
                                    }

                                    if (singleProfileModel.getResponse().getDob() != null && !singleProfileModel.getResponse().getDob().equalsIgnoreCase("")) {
                                        etAge.setText(singleProfileModel.getResponse().getDob());

                                    }

                                }
                            } else {

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SingleProfileModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitCountryDataApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CountryListMode> call = apiInterface.PostProcessCountry();
            call.enqueue(new Callback<CountryListMode>() {
                @Override
                public void onResponse(@NonNull Call<CountryListMode> call, @NonNull Response<CountryListMode> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        cointrymode = response.body();
                        if (cointrymode.getCode() == 0) {
                            country = new ArrayList<String>();
                            Log.e("mode.getsize()", "" + cointrymode.getResponse().size());
                            for (int i = 0; i < cointrymode.getResponse().size(); i++) {
                                country.add(cointrymode.getResponse().get(i).getCountry_name().toString());
                            }
                            countryAdapter = new ArrayAdapter<String>(context, R.layout.spinner_layout_view, country);
                            countryAdapter.setDropDownViewResource(R.layout.spinner_text_view);
                            spi_country.setAdapter(countryAdapter);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CountryListMode> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                }
            });


        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (!isDataRecieved) {
                isDataRecieved = true;
                if (!isMyServiceRunning()) {
                    startService(new Intent(SingleProfileDating.this, Locationservice.class));
                }
            }
        }

    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getPackageName() + ".services.Locationservice").equals(service1.service.getClassName())) {

                return true;
            }
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this, Locationservice.class));
    }

    private void init() {

        //**************titel bar**************//
        tv_interests = (TextView) findViewById(R.id.tv_interests);
        tv_interests.setOnClickListener(this);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);
        img_tick.setVisibility(View.GONE);
        sp_interests = (Spinner) findViewById(R.id.sp_interests);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.Single_profile).toString());
        img_back.setOnClickListener(this);
        //************end titel*****************//

        rgGender = (RadioGroup) findViewById(R.id.rgGender);
        rbMale = (RadioButton) findViewById(R.id.rbMale);
        rbFemale = (RadioButton) findViewById(R.id.rbFemale);
        etAge = (TextView) findViewById(R.id.etAge);
        etLookingFor = (EditText) findViewById(R.id.etLookingFor);
        etTellingAbout = (EditText) findViewById(R.id.etTellingAbout);
        btSingIn = (Button) findViewById(R.id.btSignIn);
        if (user_id.equalsIgnoreCase(Config.getUserId(context))) {
            btSingIn.setVisibility(View.VISIBLE);
        } else {
            btSingIn.setVisibility(View.GONE);
        }
        btSingIn.setText("APPLY");

        single_profile = (ImageView) findViewById(R.id.single_profile);

        btSingIn.setOnClickListener(this);



        etAge.setOnClickListener(this);
        single_profile.setOnClickListener(this);

        tv_delete_single_profile = (TextView) findViewById(R.id.tv_delete_single_profile);
        tv_delete_single_profile.setOnClickListener(this);

        spi_country = (Spinner) findViewById(R.id.spi_country_sms);

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i==R.id.rbMale){
                    rbMale.setChecked(true);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        rbMale.setButtonTintList(getResources().getColorStateList(R.color.color_black));
                        rbFemale.setButtonTintList(getResources().getColorStateList(R.color.color_white));
                    }
                    rbMale.setTextColor(getResources().getColor(R.color.color_black));
                    rbFemale.setTextColor(getResources().getColor(R.color.color_white));
                    rbFemale.setChecked(false);
                }

                else if (i==R.id.rbFemale){
                rbFemale.setChecked(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    rbFemale.setButtonTintList(getResources().getColorStateList(R.color.color_black));
                    rbMale.setButtonTintList(getResources().getColorStateList(R.color.color_white));
                }
                rbFemale.setTextColor(getResources().getColor(R.color.color_black));
                rbMale.setTextColor(getResources().getColor(R.color.color_white));
                rbMale.setChecked(false);
                }
            }
        });

        spi_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country_id = String.valueOf(cointrymode.getResponse().get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etAge:
                new DatePickerDialog(this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.catogory_interest:
                break;

            case R.id.tv_interests:
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.all_interest_list);
                dialog.setCancelable(true);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                recy_interest_list = (RecyclerView) dialog.findViewById(R.id.recy_interest_list);
                btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
                mLayoutManager = new GridLayoutManager(context, 2);
                recy_interest_list.setLayoutManager(mLayoutManager);
                if (interestsList != null && interestsList.size() > 0) {
                    interestsAdapter = new InterestsAdapter(SingleProfileDating.this, interestsList, currentSelectedItems, this, this);
                    recy_interest_list.setAdapter(interestsAdapter);
                }
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        try {
                            String items = android.text.TextUtils.join(" | ", currentSelectedItems);
                            tv_interests.setText(String.valueOf(items));
                            Log.e("valSel ", String.valueOf(items));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                dialog.show();
                break;

            case R.id.btSignIn:

                v.startAnimation(AppConstants.buttonClick);
                looking_for = etLookingFor.getText().toString().trim();
                about_me = etTellingAbout.getText().toString().trim();
                dob = etAge.getText().toString().trim();

                if (rbMale.isChecked()) {
                    gender = "M";
                }
                if (rbFemale.isChecked()) {
                    gender = "F";
                }
                if (looking_for.length() == 0 && about_me.length() == 0 && about_me.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), this);
                } else if (looking_for.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), this);
                } else if (about_me.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), this);
                } else if (gender == null) {
                    displayErrorDialog(getResources().getString(R.string.select_gender), this);
                } else if (tv_interests.getText().toString().equalsIgnoreCase("")) {
                    displayErrorDialog(getResources().getString(R.string.select_interests), this);
                } else {

                    country_id = "0";
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        ShowGpsDialog();
                        if (!isMyServiceRunning()) {
                            startService(new Intent(SingleProfileDating.this, Locationservice.class));
                        }
                    } else {
                        removeGpsDialog();
                        if (!isMyServiceRunning()) {
                            context.startService(new Intent(context, Locationservice.class));
                        }

//                        updateImageToFirebaseDatabase(type, imageInByte);

                        hitSingleRegisterApi();
                    }
                }

                break;

            case R.id.single_profile:
                v.startAnimation(AppConstants.buttonClick);
                if (checkPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 99)) {

                    LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View popupView = layoutInflater.inflate(R.layout.camera_dalog, null);
                    Button image_camera = (Button) popupView.findViewById(R.id.image_camera);
                    Button image_gallery = (Button) popupView.findViewById(R.id.image_gallery);

                    image_camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            captureImageFromCamera();
                            popupWindow.dismiss();
                        }
                    });
                    image_gallery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            popupWindow.dismiss();
                            captureImageFromGallery();
                        }
                    });

                    popupWindow = new PopupWindow(
                            popupView,
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    popupWindow.setBackgroundDrawable(new BitmapDrawable());
                    popupWindow.setOutsideTouchable(true);
                    popupWindow.showAsDropDown(single_profile);
                }

                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.tv_delete_single_profile:
                v.startAnimation(AppConstants.buttonClick);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Are you sure you want to delete your Friend Profile?");


                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                hitDeleteApi();

                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


                break;
        }


    }

    private void ShowGpsDialog() {
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                SingleProfileDating.this);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("No GPS")
                .setMessage(getString(R.string.gps_info))
                .setPositiveButton(getString(R.string.gps_seating),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                gpsAlertDialog.dismiss();
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeGpsDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                // finish();
                            }
                        });

        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;

        }
    }

    private void deleteChatFromFirebase() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("friend");
            // Read from the database
            mDatabase.child(Config.getLoginUSERUID(context)).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        if (dataSnapshot.getValue() != null) {
                            HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                            String value, substrVal = null;
                            String key = "";
                            Iterator myVeryOwnIterator = mapMessage.keySet().iterator();
                            while (myVeryOwnIterator.hasNext()) {
                                key = (String) myVeryOwnIterator.next();
                                value = (String) mapMessage.get(key);
                                substrVal = value.substring(value.indexOf(",") + 1, value.length());
                                deleteStorageImages(substrVal);
                            }
                        }
                        SingleProfileDating.this.finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    CommonUtils.getInstance().dismissLoadingDialog();
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });
        } else {
            CommonUtils.getInstance().showAlertMessage(context, "Please ensure internet connectivity.");
        }
    }


    private void deleteStorageImages(String substrVal) {

        DatabaseReference chatDatabase = FirebaseDatabase.getInstance().getReference().child("message/" + substrVal);

//        final FirebaseStorage firebaseStorage = FirebaseStorage.getInstance().getReference().getStorage();
//        Query query = chatDatabase.orderByChild("isImage").equalTo("1");
//        query.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
//                        HashMap mapMessage = (HashMap) issue.getValue();
//                        String val = (String) mapMessage.get("photoUrl");
//                        StorageReference photoRef = firebaseStorage.getReferenceFromUrl(val);
//                        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
//                            @Override
//                            public void onSuccess(Void aVoid) {
//                            }
//                        }).addOnFailureListener(new OnFailureListener() {
//                            @Override
//                            public void onFailure(@NonNull Exception exception) {
//                            }
//                        });
//                    }
//                } else {
//                    Log.d(TAG, "onFailure: no action");
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
        chatDatabase.setValue(null);
    }

    private void hitDeleteApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.DeleteSingleProfile(user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel deletResponseModel = response.body();
                        Common.displayLongToast(context, deletResponseModel.getMessage());
                        if (deletResponseModel.isError() == false) {
                            Config.setSingleRegister("N", context);
                            try {
                                deleteChatFromFirebase();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void hitSingleRegisterApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {

            CommonUtils.getInstance().displayLoadingDialog(false, context);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        lat = Config.getlatitude(context);
                        longi = Config.getlongitude(context);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Retrofit retrofit1 = new Retrofit.Builder()
                            .baseUrl(AppConstants.LOC_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    ApiInterface apiInterface1 = retrofit1.create(ApiInterface.class);
                    Call<AddressFinderBean> call1;
                    String latlng = lat + "," + longi;
                    call1 = apiInterface1.getCompleteAddress(latlng, true, getString(R.string.geolocation_api_key));
                    call1.enqueue(new Callback<AddressFinderBean>() {
                        @Override
                        public void onResponse(Call<AddressFinderBean> calll, Response<AddressFinderBean> response) {
                            try {
                                AddressFinderBean mObj = response.body();
                                if (mObj != null && mObj.getStatus().equalsIgnoreCase("OK")) {
                                    String country = "";
                                    String address = "";
                                    String fullAddress = mObj.getResults().get(0).getFormattedAddress();
                                    try {
                                        country = fullAddress.substring(fullAddress.lastIndexOf(",") + 1, fullAddress.length());
                                        address = fullAddress;

                                        String interests = "";

                                        if (currentSelectedItems != null && currentSelectedItems.size() < 1) {
                                            try {
                                                interests = intereststext;
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            interests = android.text.TextUtils.join(" , ", currentSelectedItems);
                                        }


                                        final HashMap<String, String> requestValuePairsMap = new HashMap<>();
                                        requestValuePairsMap.put("user_id", user_id);
                                        requestValuePairsMap.put("gender", gender);
                                        requestValuePairsMap.put("looking_for", looking_for);
                                        requestValuePairsMap.put("about_me", about_me);
                                        requestValuePairsMap.put("dob", dob);
                                        requestValuePairsMap.put("latitude", lat);
                                        requestValuePairsMap.put("longitude", longi);
                                        requestValuePairsMap.put("interests", interests);

                                        try {
                                            requestValuePairsMap.put("country", country);
                                            requestValuePairsMap.put("address", address);
                                        } catch (Exception e) {
                                            e.printStackTrace();
//            CommonUtils.getInstance().showAlertMessage(context, "Please check your location settings");
                                        }
                                        OkHttpClient client = new OkHttpClient.Builder()
                                                .connectTimeout(100, TimeUnit.SECONDS)
                                                .readTimeout(100, TimeUnit.SECONDS).build();
                                        Retrofit retrofit = new Retrofit.Builder()
                                                .baseUrl(AppConstants.BASE_URL).client(client)
                                                .addConverterFactory(GsonConverterFactory.create(new Gson())).build();
                                        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

                                        Call<SingleResponseModel> call;
                                        if (selectedImagePath == null) {
                                            call = apiInterface.SingleRegister(RetrofitUtils.createMultipartRequest(requestValuePairsMap));

                                        } else {
                                            call = apiInterface.SingleRegister(RetrofitUtils.createMultipartRequest(requestValuePairsMap), RetrofitUtils.createFilePart("image", selectedImagePath, RetrofitUtils.MEDIA_TYPE_IMAGE_PNG));
                                        }

                                        call.enqueue(new Callback<SingleResponseModel>() {
                                            @Override
                                            public void onResponse(@NonNull Call<SingleResponseModel> call, @NonNull Response<SingleResponseModel> response) {

                                                try {
                                                    Log.d(TAG, "onResponse: " + response.body());
                                                    CommonUtils.getInstance().dismissLoadingDialog();
                                                    try {
                                                        SingleResponseModel singleResponseModel = response.body();
                                                        if (singleResponseModel.isError() == false) {
                                                            dialogMessage(singleResponseModel.getMessage());

                                                        } else {
                                                            Common.displayLongToast(context, singleResponseModel.getMessage());
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }

                                                } catch (Exception e) {
                                                    CommonUtils.getInstance().dismissLoadingDialog();
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<SingleResponseModel> call, Throwable t) {
                                                Log.e(TAG, "error: " + t.toString());
                                                CommonUtils.getInstance().dismissLoadingDialog();
                                            }
                                        });


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(Call<AddressFinderBean> call, Throwable t) {

                        }
                    });


                }
            }, 1000);


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    /****************************
     * Capturing Image from camera
     ****************************/
    private void captureImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /****************************
     * Capturing Image from gallery
     ****************************/
    private void captureImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
    }

    /*****
     * Receiving activity result method will be called after closing the camera
     ******/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            previewImage(0, data);

        } else if (requestCode == GALLERY_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            previewImage(1, data);

        } else {
            Toast.makeText(this, " cancel capture image ", Toast.LENGTH_SHORT).show();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, System.currentTimeMillis() + "", null);
        return Uri.parse(path);
    }

    private void previewImage(int previewfrom, Intent data) {
        switch (previewfrom) {
            case 0:

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                uri = getImageUri(context, thumbnail);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                imageInByte = bytes.toByteArray();

                imgString = Base64.encodeToString(imageInByte,
                        Base64.DEFAULT);
                type = "image";
                single_profile.setImageBitmap(thumbnail);

                final String dir = Environment.getExternalStorageDirectory() + "/AfrimackImages/";
                File newdir = new File(dir);
                if (!newdir.exists())
                    newdir.mkdirs();


                File destination = new File(newdir,
                        System.currentTimeMillis() + ".png");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                selectedImagePath = destination.getAbsolutePath();


                break;
            case 1:
                uri = data.getData();
                try {

                    selectedImagePath = Common.getPath(this, uri);

                    single_profile.setImageBitmap(Common.getImageFromPath(selectedImagePath, 500, 500));

                    bm = (Common.getImageFromPath(selectedImagePath, 500, 500));

                    Matrix matrix1 = new Matrix();
                    matrix1.postRotate(90);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix1, true);

                    stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    imageInByte = stream.toByteArray();
                    imgString = Base64.encodeToString(imageInByte,
                            Base64.DEFAULT);
                    type = "image";

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
        }
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {

        // String myFormat = "MM/dd/yy"; //In which you need put here
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        dob = sdf.format(myCalendar.getTime());
        if (myCalendar.getTime().before(c.getTime())) {
            int ca_year = myCalendar.YEAR;
            int cc = c.YEAR;
            etAge.setText(sdf.format(myCalendar.getTime()));
            dob = sdf.format(myCalendar.getTime());
        } else {
            Common.displayLongToast(this, getResources().getString(R.string.dob_check));
        }


    }


    private void dialogMessage(String message) {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        TextView mess_colose = (TextView) dialog.findViewById(R.id.mess_colose);
        TextView dailog_message = (TextView) dialog.findViewById(R.id.dailog_message);
        TextView dalog_ok = (TextView) dialog.findViewById(R.id.dalog_ok);
        dailog_message.setText(message);
        TextView dalog_titel = (TextView) dialog.findViewById(R.id.dalog_titel);
        dalog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
            }
        });
        mess_colose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();

            }


        });

        dialog.show();
    }

    @Override
    public void onItemCheck(String item) {

        if (!currentSelectedItems.contains(item)) {
            currentSelectedItems.add(item);
        }
tv_interests.setText(item);

    }

    @Override
    public void onItemUncheck(String item) {
        currentSelectedItems.remove(item);
        tv_interests.setText(item);
    }
}

package com.afrimack.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.ChatOverviewAdapter;
import com.afrimack.app.appbeans.FriendListBean;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.services.ServiceUtils;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.CommonUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ActivityChattingOverView extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvChat;
    private ImageView imgBack, img_my_afrmiack;
    private int page_no = 1;
    private Context context = this;
    private String TAG = ActivityChattingOverView.class.getSimpleName();
    private LinearLayoutManager mLayoutManager;
    private ChatOverviewAdapter chatOverviewAdapter;
    private List<com.afrimack.app.appbeans.Response> mLIst;
    private String from = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting_over_view);
        if (getIntent().getStringExtra("from") != null) {
            from = getIntent().getStringExtra("from");
        }
        init();
//        hitApi();

    }


    private void init() {
        img_my_afrmiack = (ImageView) findViewById(R.id.img_my_afrmiack);
        img_my_afrmiack.setVisibility(View.GONE);
        rvChat = (RecyclerView) findViewById(R.id.rv_chat);
        imgBack = (ImageView) findViewById(R.id.img_back_chat);
        imgBack.setOnClickListener(this);
        img_my_afrmiack.setOnClickListener(this);

        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        rvChat.setLayoutManager(mLayoutManager);
        rvChat.setHasFixedSize(true);

//        chatOverviewAdapter = new ChatOverviewAdapter(rvChat, context, mLIst, this, this);
//        rvChat.setAdapter(chatOverviewAdapter);
    }

    @Override
    public void onBackPressed() {
        if (from.equalsIgnoreCase("from_chat")) {
            Intent mIntent = new Intent(context, ActivityHome.class);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mIntent);
            ActivityChattingOverView.this.finish();

        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        hitApi();
    }


    @Override
    protected void onPause() {
        try {
            ServiceUtils.updateLoginUserStatus(context, "3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        try {
            ServiceUtils.updateLoginUserStatus(context, "3");
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void hitApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();


            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<FriendListBean> call = apiInterface.getFriendsList(Config.getLoginUSERUID(context), Config.getUserId(context));
            call.enqueue(new Callback<FriendListBean>() {
                @Override
                public void onResponse(@NonNull Call<FriendListBean> call, @NonNull Response<FriendListBean> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        try {
                            ServiceUtils.updateLoginUserStatus(context, "2");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        FriendListBean listBean = response.body();
                        if (listBean.getError() == false) {

                            mLIst = listBean.getResponse();
                            if (mLIst != null && mLIst.size() > 0) {
                                chatOverviewAdapter = new ChatOverviewAdapter(context, mLIst);
                                rvChat.setAdapter(chatOverviewAdapter);
                            } else {
                                try {
//                                    if (mLIst != null) {
                                    mLIst = new ArrayList<com.afrimack.app.appbeans.Response>();
                                    chatOverviewAdapter = new ChatOverviewAdapter(context, mLIst);
                                    rvChat.setAdapter(chatOverviewAdapter);
//                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                CommonUtils.getInstance().showAlertMessage(context, "No chat data available");
                            }

                        } else {
                            try {
//                                if (mLIst != null) {
                                mLIst = new ArrayList<com.afrimack.app.appbeans.Response>();
                                chatOverviewAdapter = new ChatOverviewAdapter(context, mLIst);
                                rvChat.setAdapter(chatOverviewAdapter);
//                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            CommonUtils.getInstance().showAlertMessage(context, "No chat data available");
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<FriendListBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().showAlertMessage(context, "Something went wrong. Please try again later.");
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });

        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_chat:
                if (from.equalsIgnoreCase("from_chat")) {
                    Intent mIntent = new Intent(context, ActivityHome.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mIntent);
                    ActivityChattingOverView.this.finish();

                } else {
                    super.onBackPressed();
                }
                break;

            case R.id.img_my_afrmiack:
                Intent intent = new Intent(this, ActMyAfrimack.class);
                intent.putExtra("from", "a");
                startActivity(intent);
                break;
        }
    }

}

package com.afrimack.app.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.afrimack.app.AfrimackApp;
import com.afrimack.app.R;
import com.afrimack.app.adapters.AllCatogeryAdapter;
import com.afrimack.app.adapters.SwapChatAdapter;
import com.afrimack.app.adapters.SwapOfferAdapter;
import com.afrimack.app.appbeans.BlockUserBean;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.models.PostEditModel;
import com.afrimack.app.models.SwapResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SwopDeal extends AppCompatActivity implements View.OnClickListener {


    private LinearLayout chat_main;
    private ImageView img_back;
    private TextView tv_titel_chat;

    private ImageView post_image_chat, user_pci;
    private TextView last_update, post_titel_chat, last_date;
    private TextView chat_message;
    private Button make_offer;
    private Button btn_contet_offer;
    private Button btn_cancel_deal, btn_remind_partemer, btn_confirm;
    private Button btn_confrim_app;

    private String user_id, post_id, offer_user_id, post_user_id;
    private LinearLayout lay_conf_acc, lay_mack_offer, lay_chat;
    private RecyclerView recy_offer_chat, rec_chat;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.LayoutManager chatLayoutManager;
    private List<PostDetailResponseModel.ResponseBean.OffersBean> offerListl;
    private SwapOfferAdapter swapOfferAdapter;
    private SwapChatAdapter swapChatAdapter;
    private LinearLayout lay_user_profile, lay_offerc_chat, lay_conf_rem_can;
    private TextView tv_other_user, tv_user_name, tv_finall_deal;
    private ImageView img_other_user, img_user_off, stap_fill, tv_chat_dene;
    private ImageView chat_off, chat, img_dot_chat;
    String price, rating_type, post_user_profile;


    /*chat */
    private ImageView chat_user_profile, img_send_message;
    private EditText et_message;
    private TextView rate_user;
    private LinearLayout lay_rate;
    private RatingBar rating_bar_app;
    String posttitel, post_prise, offer_user_name;

    ArrayAdapter<String> swapAdapter;
    private Spinner sp_swop_del;


    private LinearLayout lay_swap_deal;
    private Dialog dialogswap;
    private RecyclerView recy_caterer_list;
    private AllCatogeryAdapter allCatogery;
    private int pos;
    private ArrayList<String> categories;
    private SwapResponseModel swapModel;
    private String swap_id;
    private Context context = this;
    private String TAG = SwopDeal.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swop_deal);

        user_id = Config.getUserId(this);
        init();


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                post_id = null;
            } else {
                post_id = extras.getString("post_id");
                offer_user_id = extras.getString("offer_user_id");
                post_user_id = extras.getString("post_user_id");

            }
        } else {
        }
        Log.e("post_id", post_id + " offer_user_id " + offer_user_id + " pi" + post_user_id);

        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recy_offer_chat.setLayoutManager(mLayoutManager);


        chatLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rec_chat.setLayoutManager(chatLayoutManager);

        hitDataApi();
    }

    private void hitDataApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SwapResponseModel> call = apiInterface.SwapDeal(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<SwapResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<SwapResponseModel> call, @NonNull Response<SwapResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        swapModel = response.body();
                        if (swapModel.isError() == false) {
                            chat_main.setVisibility(View.VISIBLE);
                            if (post_user_id.equalsIgnoreCase(user_id)) {
                                img_dot_chat.setVisibility(View.GONE);
                            }
                            //  offer_user_id = String.valueOf(chatDetail.getResponse().getUser_id());
                            Picasso.with(context)
                                    .load(swapModel.getResponse().getPost_image())
                                    .fit()
                                    .placeholder(R.drawable.img2)
                                    .error(R.drawable.img2)
                                    .into(post_image_chat);
                            post_prise = swapModel.getResponse().getPost_price();
                            last_update.setText("LISTED BY " + swapModel.getResponse().getPost_user_name()
                                    + " FOR " + swapModel.getResponse().getPost_price());
                            last_date.setText("UPDATED ON " + swapModel.getResponse().getLast_update());
                            offer_user_name = swapModel.getResponse().getOffer_user_name();
                            Picasso.with(context)
                                    .load(swapModel.getResponse().getOffer_user_image())
                                    .fit()
                                    .placeholder(R.drawable.user_round)
                                    .error(R.drawable.user_round)
                                    .into(user_pci);

                            Picasso.with(context)
                                    .load(swapModel.getResponse().getOffer_user_image())
                                    .fit()
                                    .placeholder(R.drawable.user_round)
                                    .error(R.drawable.user_round)
                                    .into(chat_user_profile);


                            tv_other_user.setText(swapModel.getResponse().getPost_user_name());
                            tv_user_name.setText(swapModel.getResponse().getOffer_user_name());
                            if (swapModel.getResponse().getMy_product() == 1) {
                                lay_conf_acc.setVisibility(View.VISIBLE);
                                lay_mack_offer.setVisibility(View.GONE);
                                make_offer.setVisibility(View.GONE);
                                rate_user.setText(getResources().getString(R.string.all_dine) + " " + swapModel.getResponse().getOffer_user_name());
                            } else {
                                btn_remind_partemer.setVisibility(View.GONE);
                                btn_confirm.setVisibility(View.VISIBLE);
                                rate_user.setText(getResources().getString(R.string.all_dine) + " " + swapModel.getResponse().getPost_user_name());
                            }
                            Picasso.with(context)
                                    .load(swapModel.getResponse().getPost_user_image())
                                    .fit()
                                    .placeholder(R.drawable.user_round)
                                    .error(R.drawable.user_round)
                                    .into(img_other_user);
                            post_user_profile = swapModel.getResponse().getPost_user_image();
                            //tv_finall_deal.setText(swapModel.getResponse().getFinal_price());

                            Picasso.with(context)
                                    .load(swapModel.getResponse().getOffer_user_image())
                                    .fit()
                                    .placeholder(R.drawable.user_round)
                                    .error(R.drawable.user_round)
                                    .into(img_user_off);


                            posttitel = swapModel.getResponse().getTitle();
                            post_titel_chat.setText(swapModel.getResponse().getTitle());
                            // chat_message.setHint(swapModel.getResponse().getPost_price());
               /* if(String.valueOf(swapModel.getResponse().getLast_offer_user_id()).equalsIgnoreCase(user_id)){
                    lay_conf_acc.setVisibility(View.GONE);
                    lay_mack_offer.setVisibility(View.VISIBLE);
                    make_offer.setVisibility(View.VISIBLE);
                }else if(String.valueOf(swapModel.getResponse().getLast_offer_user_id()).equalsIgnoreCase("")) {
                    lay_conf_acc.setVisibility(View.GONE);
                    lay_mack_offer.setVisibility(View.VISIBLE);
                    make_offer.setVisibility(View.VISIBLE);
                }else {
                    lay_conf_acc.setVisibility(View.VISIBLE);
                    lay_mack_offer.setVisibility(View.GONE);
                    make_offer.setVisibility(View.GONE);
                }
*/
                            if (swapModel.getResponse().getLast_status().equalsIgnoreCase("A")) {
                                make_offer.setVisibility(View.GONE);
                                lay_conf_acc.setVisibility(View.GONE);
                                lay_user_profile.setVisibility(View.VISIBLE);
                                stap_fill.setBackground(getResources().getDrawable(R.drawable.step1));
                                lay_offerc_chat.setVisibility(View.GONE);

                            } else if (swapModel.getResponse().getLast_status().equalsIgnoreCase("CO")) {
                                lay_conf_acc.setVisibility(View.VISIBLE);
                                lay_mack_offer.setVisibility(View.GONE);
                                make_offer.setVisibility(View.GONE);
                                lay_offerc_chat.setVisibility(View.GONE);

                            } else if (swapModel.getResponse().getLast_status().equalsIgnoreCase("C")) {


                                make_offer.setVisibility(View.GONE);
                                lay_conf_acc.setVisibility(View.GONE);
                                lay_user_profile.setVisibility(View.VISIBLE);
                                lay_chat.setVisibility(View.VISIBLE);
                                lay_conf_rem_can.setVisibility(View.GONE);
                                stap_fill.setBackground(getResources().getDrawable(R.drawable.step2));
                                lay_offerc_chat.setVisibility(View.GONE);

                            } else if (swapModel.getResponse().getLast_status().equalsIgnoreCase("X")) {
                                make_offer.setVisibility(View.GONE);
                                lay_conf_acc.setVisibility(View.GONE);
                                lay_user_profile.setVisibility(View.VISIBLE);
                                lay_chat.setVisibility(View.GONE);
                                lay_conf_rem_can.setVisibility(View.GONE);
                                lay_offerc_chat.setVisibility(View.GONE);

                            }
               /* if(swapModel.getResponse().getStatus_by().equalsIgnoreCase("Y")){
                    btn_remind_partemer.setVisibility(View.VISIBLE);
                    btn_confirm.setVisibility(View.GONE);
                    lay_offerc_chat.setVisibility(View.GONE);

                }else {
                    btn_remind_partemer.setVisibility(View.GONE);
                    btn_confirm.setVisibility(View.VISIBLE);
                }*/

                            rating_type = swapModel.getResponse().getRating();
                            if (swapModel.getResponse().getOffers().size() > 0) {

                                recy_offer_chat.setVisibility(View.VISIBLE);


                                swapOfferAdapter = new SwapOfferAdapter(context, swapModel.getResponse().getOffers());
                                recy_offer_chat.setAdapter(swapOfferAdapter);
                                swapOfferAdapter.notifyDataSetChanged();
                            }

                            if (swapModel.getResponse().getChat().size() > 0) {

                                stap_fill.setBackground(getResources().getDrawable(R.drawable.step3));
                                tv_chat_dene.setBackgroundColor(getResources().getColor(R.color.color_green));
                                chat_off.setVisibility(View.GONE);
                                chat.setVisibility(View.VISIBLE);

                                swapChatAdapter = new SwapChatAdapter(context, swapModel.getResponse().getChat());
                                rec_chat.setAdapter(swapChatAdapter);
                                swapChatAdapter.notifyDataSetChanged();
                            }

                            List<String> hour = new ArrayList<String>();
                            for (int k = 1; k <= 5; k++) {
                                hour.add(String.valueOf(k));
                            }
                            swapAdapter = new ArrayAdapter<String>(context, R.layout.spinner_layout_view, hour);
                            swapAdapter.setDropDownViewResource(R.layout.spinner_text_view);
                            //   sp_swop_del.setAdapter(swapAdapter);


                            if (swapModel.getResponse().getPost_for_swap().size() > 0) {

                                Log.d("list swap", swapModel.getResponse().getPost_for_swap().size() + "");
                                categories = new ArrayList<String>();
                                for (int i = 0; i < swapModel.getResponse().getPost_for_swap().size(); i++) {
                        /*if (i == 0) {
                            categories.add("Swop Deal");
                        } else {
                            Log.d("list swap tt",i+"");
                            categories.add(swapModel.getResponse().getPost_for_swap().get(i-1).getTitle());
                        }*/
                                    categories.add(swapModel.getResponse().getPost_for_swap().get(i).getTitle());

                                }
                            }


                        } else {
//                            Common.dismissLoadingDialog();
                            CommonUtils.getInstance().dismissLoadingDialog();
                            Common.displayLongToast(context, "Repeat");
                        }


                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SwapResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void init() {

        chat_main = (LinearLayout) findViewById(R.id.chat_main);

        img_back = (ImageView) findViewById(R.id.img_back_chat);
        tv_titel_chat = (TextView) findViewById(R.id.tv_titel_chat);
        img_dot_chat = (ImageView) findViewById(R.id.img_dot_chat);
        try {
            img_dot_chat.setImageResource(R.drawable.ic_delete);
        } catch (Exception e) {
            e.printStackTrace();
        }


        img_back.setOnClickListener(this);
        tv_titel_chat.setText(getResources().getString(R.string.app_titel));

        post_image_chat = (ImageView) findViewById(R.id.post_image_chat);
        last_update = (TextView) findViewById(R.id.last_update);
        last_date = (TextView) findViewById(R.id.last_date);
        user_pci = (ImageView) findViewById(R.id.user_pci);
        chat_message = (TextView) findViewById(R.id.chat_message);
        post_titel_chat = (TextView) findViewById(R.id.post_titel_chat);
        make_offer = (Button) findViewById(R.id.make_offer);

        lay_mack_offer = (LinearLayout) findViewById(R.id.lay_mack_offer);
        lay_conf_acc = (LinearLayout) findViewById(R.id.lay_conf_acc);
        btn_contet_offer = (Button) findViewById(R.id.btn_contet_offer);
        btn_confirm = (Button) findViewById(R.id.btn_confirm);


        btn_cancel_deal = (Button) findViewById(R.id.btn_cancel_deal);
        btn_remind_partemer = (Button) findViewById(R.id.btn_remind_partemer);
        btn_confrim_app = (Button) findViewById(R.id.btn_confrim_app);

        lay_chat = (LinearLayout) findViewById(R.id.lay_chat);
        recy_offer_chat = (RecyclerView) findViewById(R.id.recy_offer_chat);
        rec_chat = (RecyclerView) findViewById(R.id.rec_chat);

        chat_user_profile = (ImageView) findViewById(R.id.chat_user_profile);
        et_message = (EditText) findViewById(R.id.et_message);
        img_send_message = (ImageView) findViewById(R.id.img_send_message);

        lay_user_profile = (LinearLayout) findViewById(R.id.lay_user_profile);
        lay_offerc_chat = (LinearLayout) findViewById(R.id.lay_offerc_chat);
        lay_conf_rem_can = (LinearLayout) findViewById(R.id.lay_conf_rem_can);

        tv_other_user = (TextView) findViewById(R.id.tv_other_user);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        img_other_user = (ImageView) findViewById(R.id.img_other_user);
        tv_finall_deal = (TextView) findViewById(R.id.tv_finall_deal);
        img_user_off = (ImageView) findViewById(R.id.img_user_off);
        stap_fill = (ImageView) findViewById(R.id.stap_fill);
        tv_chat_dene = (ImageView) findViewById(R.id.tv_chat_dene);

        chat_off = (ImageView) findViewById(R.id.chat_off);
        chat = (ImageView) findViewById(R.id.chat);

        lay_rate = (LinearLayout) findViewById(R.id.lay_rate);
        rating_bar_app = (RatingBar) findViewById(R.id.rating_bar_app);
        rate_user = (TextView) findViewById(R.id.rate_user);
        make_offer.setOnClickListener(this);
        btn_contet_offer.setOnClickListener(this);
        btn_cancel_deal.setOnClickListener(this);
        btn_remind_partemer.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);

        btn_confrim_app.setOnClickListener(this);
        img_send_message.setOnClickListener(this);
        img_dot_chat.setOnClickListener(this);
        lay_rate.setOnClickListener(this);

        //sp_swop_del = (Spinner) findViewById(R.id.sp_swop_del);

        lay_swap_deal = (LinearLayout) findViewById(R.id.lay_swap_deal);
        lay_swap_deal.setOnClickListener(this);
        chat_message.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AfrimackApp.curentactivity = this;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AfrimackApp.curentactivity = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_chat:
                AfrimackApp.curentactivity = null;
                onBackPressed();

                break;
            case R.id.make_offer:

                if (swap_id.length() != 0) {
                    hitMakeOfferApi(swap_id);
                }

                break;
            case R.id.btn_contet_offer:
                lay_conf_acc.setVisibility(View.GONE);
                lay_mack_offer.setVisibility(View.VISIBLE);
                make_offer.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_cancel_deal:


                AlertDialog.Builder alert_dialog_cancel = new AlertDialog.Builder(SwopDeal.this);

                // Setting Dialog Title
                alert_dialog_cancel.setTitle("Cancel Deal");

                // Setting Dialog Message
                alert_dialog_cancel.setMessage("Are you sure you want to cancel this deal?");


                // Setting Positive "Yes" Button
                alert_dialog_cancel.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        hitCancelDeal();

                    }
                });

                // Setting Negative "NO" Button
                alert_dialog_cancel.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alert_dialog_cancel.show();




                break;
            case R.id.btn_remind_partemer:


                AlertDialog.Builder alert_dialog_remind= new AlertDialog.Builder(SwopDeal.this);

                // Setting Dialog Title
                alert_dialog_remind.setTitle("Remind Partner");

                // Setting Dialog Message
                alert_dialog_remind.setMessage("Are you sure you want to send reminder? ");


                // Setting Positive "Yes" Button
                alert_dialog_remind.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        hitRemindPartenerApi();

                    }
                });

                // Setting Negative "NO" Button
                alert_dialog_remind.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alert_dialog_remind.show();




                break;
            case R.id.btn_confrim_app:
                //callapiappoived();
                AlertDialog.Builder alertDialogConfirm = new AlertDialog.Builder(SwopDeal.this);

                // Setting Dialog Title
                alertDialogConfirm.setTitle("Accept Deal");

                // Setting Dialog Message
                alertDialogConfirm.setMessage("Are you sure you want Swop " + "'" + posttitel + "'" + " for " + post_prise + "? \n You acceptance is legally binding when " + offer_user_name + " confirms");


                // Setting Positive "Yes" Button
                alertDialogConfirm.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        hitApprovedApi();

                    }
                });

                // Setting Negative "NO" Button
                alertDialogConfirm.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialogConfirm.show();
                break;
            case R.id.img_send_message:
                if (et_message.getText().length() > 0) {
                    String question = et_message.getText().toString().trim();
                    hitChatApi(question);
                } else {
                    Common.displayLongToast(SwopDeal.this, getResources().getString(R.string.enter_search));
                }
                break;
            case R.id.btn_confirm:

                AlertDialog.Builder alertDialogConfirmfinal = new AlertDialog.Builder(SwopDeal.this);

                // Setting Dialog Title
                alertDialogConfirmfinal.setTitle("Confirm Swop Deal");

                // Setting Dialog Message
                alertDialogConfirmfinal.setMessage("Are you sure you want swop " + "'" + posttitel + "'" + " for " + post_prise + "? You acceptance is legally binding when " + offer_user_name + " confirms");


                // Setting Positive "Yes" Button
                alertDialogConfirmfinal.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        hitConfirmApi();

                    }
                });

                // Setting Negative "NO" Button
                alertDialogConfirmfinal.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialogConfirmfinal.show();

                // cellcomformapi();
                break;

            case R.id.img_dot_chat:

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("Are you sure,You want to Block User");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                hitUserBlockApi();
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;


            case R.id.lay_rate:
                if (rating_type.equalsIgnoreCase("N")) {
                    Intent intent = new Intent(SwopDeal.this, RateReviewActivity.class);
                    intent.putExtra("post_id", post_id);
                    intent.putExtra("post_user_id", offer_user_id);
                    intent.putExtra("post_user_profile", post_user_profile);
                   /*post_id= extras.getString("post_id");
                   offer_user_id =extras.getString("offer_user_id");
                   post_user_id =extras.getString("post_user_id");*/
                    startActivity(intent);
                }

                break;
            case R.id.lay_swap_deal:
                swaplist();

                break;
            case R.id.chat_message:
                swaplist();
                break;
            case R.id.catogety:
                dialogswap.dismiss();
                pos = (int) (v.getTag());
                /*if(pos!=0){
                    String    category_id = String.valueOf(swapModel.getResponse().getPost_for_swap().get(pos -1).getId());
                    chat_message.setText(String.valueOf(swapModel.getResponse().getPost_for_swap().get(pos -1).getTitle()));
                    Log.d("category_id",category_id);
                }else {
                    chat_message.setText("Swop Deal");
                }*/
                swap_id = String.valueOf(swapModel.getResponse().getPost_for_swap().get(pos).getId());
                chat_message.setText(String.valueOf(swapModel.getResponse().getPost_for_swap().get(pos).getTitle()));
                Log.d("swap_id", swap_id);

                break;
        }

    }

    private void hitUserBlockApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<BlockUserBean> call = apiInterface.BlockUser(user_id, offer_user_id);
            call.enqueue(new Callback<BlockUserBean>() {
                @Override
                public void onResponse(@NonNull Call<BlockUserBean> call, @NonNull Response<BlockUserBean> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        BlockUserBean commanResponseModel = response.body();

                        if (commanResponseModel.getError() == false) {
                            Common.displayLongToast(context, commanResponseModel.getMessage());
                            Intent intent = new Intent(context, ActivityHome.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BlockUserBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitConfirmApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.SwapConfirm(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            hitDataApi();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitChatApi(String question) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.Chatoffer(user_id, post_id, offer_user_id, question);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            et_message.setText("");
                            hitDataApi();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitRemindPartenerApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.SwapRemindPost(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            //  callapidata();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitApprovedApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.SwapAcceptOffer(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            hitDataApi();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitCancelDeal() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.SwapCancelPost(user_id, post_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            //  callapidata();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitMakeOfferApi(String swap_id) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.SwapMakeOffer(user_id, post_id, swap_id, offer_user_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            hitDataApi();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void swaplist() {
        dialogswap = new Dialog(this);
        dialogswap.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogswap.setContentView(R.layout.all_catogery_list);
        dialogswap.setCancelable(true);
        dialogswap.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        recy_caterer_list = (RecyclerView) dialogswap.findViewById(R.id.recy_caterer_list);
        mLayoutManager = new LinearLayoutManager(SwopDeal.this, LinearLayoutManager.VERTICAL, false);
        recy_caterer_list.setLayoutManager(mLayoutManager);
        if (categories != null && categories.size() > 0) {
            allCatogery = new AllCatogeryAdapter(this, categories, this);
            recy_caterer_list.setAdapter(allCatogery);
        }
        dialogswap.show();
    }


    public void chatnotification() {
        hitDataApi();

    }
}

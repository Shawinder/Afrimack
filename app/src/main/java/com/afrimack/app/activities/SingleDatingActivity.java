package com.afrimack.app.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.afrimack.app.R;
import com.afrimack.app.adapters.InterestFilterAdapter;
import com.afrimack.app.adapters.InterestsAdapter;
import com.afrimack.app.adapters.OtherUserAdapter;
import com.afrimack.app.adapters.SingleDationAdapter;
import com.afrimack.app.appbeans.DiscoverResponseModel;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.fragments.DiscoverCategories;
import com.afrimack.app.fragments.DiscoverHome;
import com.afrimack.app.models.OtherUserResponseModel;
import com.afrimack.app.models.SingleUserResponseModel;
import com.afrimack.app.recycler.DividerDecoration;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingleDatingActivity extends AppCompatActivity implements View.OnClickListener, OnLoadMoreListener, InterestsAdapter.OnItemCheckListener {
    private ImageView img_search, img_user_with_badge, img_arrow, img_back_dat;
    private TextView tv_discover;
    private ImageView img_filter, img_back;
    private TextView tv_titel_filter, no_data, get_free_access;
    private LinearLayout linear_create_your_profile;

    private RecyclerView recy_single;
    private StaggeredGridLayoutManager GridLayoutManager;
    private SingleDationAdapter dationAdapter;

    private String user_id;
    private String latitude, longitude;
    private int page_no = 1;
    private PopupWindow popupWindow;
    private ToggleButton country;
    private CheckBox ch_male, ch_female;
    private LinearLayout title_discover_parent;

    private String own_country = "no", male = "", female = "", min_age = "", max_age = "";
    SingleUserResponseModel model;
    private ArrayList<SingleUserResponseModel.ResponseBean.UsersBean> userList = new ArrayList<SingleUserResponseModel.ResponseBean.UsersBean>();
    private OnLoadMoreListener onLoadMoreListener;

    /**************chat*****************/
    public static final String ARG_HISTORY = "HISTORY";
    public static final String ARG_ONLINE = "ONLINE";
    private static final String ARG_LISTING_TYPE = "LISTING_TYPE";

    public static TextView home_notification, tv_discover_sort;
    private SeekBar seek_bar_list, seek_sort;
    private TextView tv_seek_list_show, tv_sort_value;
    private String last_days = "forever", sorting = "", radius = "Everywhere";
    private LinearLayout lay_distance, lay_date, discover_title_bar, title_bar;
    private RadioButton radio_distance, radio_date;


    private Fragment frg;
    int show = 0;
    private Context context = this;
    private String TAG = SingleDatingActivity.class.getSimpleName();
    private RecyclerView recy_interest_list_filter;
    private GridLayoutManager interestsLayout;
    private InterestFilterAdapter interestsAdapter;
    private ArrayList<String> interestsList;
    private DiscoverResponseModel obj = null;
    private List<String> currentSelectedItems = new ArrayList<>();
    private String items = "";
    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //com.afrimack.app.AfrimackApp.getInstance().getXmppListners().addUiListner(this);

        setContentView(R.layout.activity_single_dation);
        user_id = Config.getUserId(this);
        init();
        onLoadMoreListener = this;
        latitude = Config.getlatitude(this);
        longitude = Config.getlongitude(this);
        DividerDecoration itemDecoration = new DividerDecoration(this);
        GridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recy_single.setLayoutManager(GridLayoutManager);
        recy_single.setHasFixedSize(true);
        //recy_single.setAdapter("");


        dationAdapter = new SingleDationAdapter(this, userList, recy_single, this);
        recy_single.setAdapter(dationAdapter);
        recy_single.addItemDecoration(itemDecoration);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //male= null;
            } else {
                male = extras.getString("male");
                female = extras.getString("female");

            }
        } else {
        }
        hitApiInterests();
        hitApi();
    }

    private void hitApiInterests() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<DiscoverResponseModel> call = apiInterface.Discover(user_id);
            call.enqueue(new Callback<DiscoverResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<DiscoverResponseModel> call, @NonNull Response<DiscoverResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        if (response.body() != null) {
                            obj = response.body();
                            if (!obj.isError()) {
                                interestsList = new ArrayList<String>();
                                for (int i = 0; i < obj.getResponse().getInterests().size(); i++) {
                                    interestsList.add(obj.getResponse().getInterests().get(i).getCategoryName());

                                }

//                                InterestsAdapter myAdapter = new InterestsAdapter(SingleProfileDating.this, 0,
//                                        obj.getResponse().getInterests());
//                                sp_interests.setAdapter(myAdapter);
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<DiscoverResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                }
            });


        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitApi() {


        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SingleUserResponseModel> call = apiInterface.SingleUser(user_id, page_no, latitude, longitude, own_country, male, female, min_age, max_age, last_days, sorting, radius, items);
            call.enqueue(new Callback<SingleUserResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<SingleUserResponseModel> call, @NonNull Response<SingleUserResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        model = response.body();

                        if (model.isError() == false) {
                            if (!radius.equalsIgnoreCase("0KM")) {
                                if (radius.equalsIgnoreCase("Everywhere")) {
                                    SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.singles_within) + " 5511KM");

                                } else {
                                    SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.singles_within) + " " + radius);
                                }
                            } else {
                                SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.singles_within) + " 1KM");
                            }
                            min_age = model.getResponse().getSelected().getMinage();
                            max_age = model.getResponse().getSelected().getMaxage();
                            if (model.getResponse().getUsers().size() > 0) {
                                no_data.setVisibility(View.GONE);
                                get_free_access.setVisibility(View.GONE);
                                linear_create_your_profile.setVisibility(View.GONE);
                                recy_single.setVisibility(View.VISIBLE);
                                img_filter.setVisibility(View.VISIBLE);

//                                dationAdapter = new SingleDationAdapter(context, model.getResponse().getUsers(), recy_single, onLoadMoreListener);
//                                recy_single.setAdapter(dationAdapter);

                                userList.addAll(model.getResponse().getUsers());

                                dationAdapter.singlelist = userList;
                                dationAdapter.setLoaded();
                                dationAdapter.notifyDataSetChanged();

                            } else {
                                dationAdapter.singlelist = new ArrayList<SingleUserResponseModel.ResponseBean.UsersBean>();
                                dationAdapter.notifyDataSetChanged();
                            }

                        } else {

                            tv_discover_sort.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                            get_free_access.setVisibility(View.VISIBLE);


                            img_filter.setVisibility(View.GONE);
                            linear_create_your_profile.setVisibility(View.VISIBLE);
                            recy_single.setVisibility(View.GONE);
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SingleUserResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }


    }

    private void hitApiAgain() {


        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SingleUserResponseModel> call = apiInterface.SingleUser(user_id, page_no, latitude, longitude, own_country, male, female, min_age, max_age, last_days, sorting, radius, items);
            call.enqueue(new Callback<SingleUserResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<SingleUserResponseModel> call, @NonNull Response<SingleUserResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        model = response.body();

                        if (model.isError() == false) {
                            if (!radius.equalsIgnoreCase("0KM")) {
                                if (radius.equalsIgnoreCase("Everywhere")) {
                                    SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.singles_within) + " 5511KM");

                                } else {
                                    SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.singles_within) + " " + radius);

                                }
                            } else {
                                SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.singles_within) + " 1KM");
                            }
                            min_age = model.getResponse().getSelected().getMinage();
                            max_age = model.getResponse().getSelected().getMaxage();
                            if (model.getResponse().getUsers().size() > 0) {
                                no_data.setVisibility(View.GONE);
                                get_free_access.setVisibility(View.GONE);
                                linear_create_your_profile.setVisibility(View.GONE);
                                recy_single.setVisibility(View.VISIBLE);
                                img_filter.setVisibility(View.VISIBLE);

//                                dationAdapter = new SingleDationAdapter(context, model.getResponse().getUsers(), recy_single, onLoadMoreListener);
//                                recy_single.setAdapter(dationAdapter);


                                userList.addAll(model.getResponse().getUsers());

                                dationAdapter.singlelist = userList;
                                dationAdapter.setLoaded();
                                dationAdapter.notifyDataSetChanged();

                            }
//                            else {
//                                dationAdapter.singlelist = new ArrayList<SingleUserResponseModel.ResponseBean.UsersBean>();
//                                dationAdapter.notifyDataSetChanged();
//                            }

                        } else {

                            tv_discover_sort.setVisibility(View.GONE);
                            no_data.setVisibility(View.VISIBLE);
                            get_free_access.setVisibility(View.VISIBLE);
                            img_filter.setVisibility(View.GONE);
                            linear_create_your_profile.setVisibility(View.VISIBLE);
                            recy_single.setVisibility(View.GONE);
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SingleUserResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }


    }

    private void init() {
        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        img_arrow = (ImageView) findViewById(R.id.img_arrow);
        img_arrow.setVisibility(View.GONE);
        img_back_dat = (ImageView) findViewById(R.id.img_back_dat);
        img_back_dat.setOnClickListener(this);
        img_back_dat.setVisibility(View.VISIBLE);
        discover_title_bar = (LinearLayout) findViewById(R.id.discover_title_bar);
        title_bar = (LinearLayout) findViewById(R.id.title_bar);
        img_search = (ImageView) findViewById(R.id.img_search);
        img_user_with_badge = (ImageView) findViewById(R.id.img_user_with_badge);
        img_user_with_badge.setVisibility(View.GONE);
        tv_discover = (TextView) findViewById(R.id.tv_discover);
        tv_discover_sort = (TextView) findViewById(R.id.tv_discover_sort_dating);
        img_filter = (ImageView) findViewById(R.id.img_filter);
        tv_discover.setText("Find new Friends");
        img_filter.setVisibility(View.VISIBLE);
        img_search.setVisibility(View.GONE);
        img_search.setOnClickListener(this);
        img_filter.setOnClickListener(this);
        tv_discover.setOnClickListener(this);

        tv_titel_filter = (TextView) findViewById(R.id.tv_titel);
        /// tv_titel_filter.setText(getString(R.string.dating).toString());
        img_user_with_badge.setOnClickListener(this);
        img_back.setOnClickListener(this);

        //************end titel*****************//

        recy_single = (RecyclerView) findViewById(R.id.recy_single);
        img_filter.setOnClickListener(this);
        no_data = (TextView) findViewById(R.id.no_data);
        get_free_access = (TextView) findViewById(R.id.tv_get_free_access);
        linear_create_your_profile = (LinearLayout) findViewById(R.id.linear_create_your_profile);
        linear_create_your_profile.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                //finish();
                break;

            case R.id.img_back_dat:
//                finish();
//                Intent intenthome = new Intent(context, ActivityHome.class);
//                intenthome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                intenthome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intenthome);
//                finish();
                onBackPressed();
                break;

            case R.id.img_user_with_badge:
                Intent myAfrimackIntent = new Intent(this, ActMyAfrimack.class);
                myAfrimackIntent.putExtra("from", "a");
                startActivity(myAfrimackIntent);
                break;

            case R.id.img_search:
                Intent intent_search = new Intent(this, ActivitySearch.class);
                startActivity(intent_search);
//                }
                break;

            case R.id.linear_create_your_profile:
                v.startAnimation(AppConstants.buttonClick);
                Intent intent = new Intent(this, SingleProfileDating.class);
                intent.putExtra("id", Config.getUserId(context));
                startActivity(intent);
                break;

            case R.id.img_filter:
                v.startAnimation(AppConstants.buttonClick);
                // discover_title_bar.setVisibility(View.GONE);
//                if (currentSelectedItems != null && currentSelectedItems.size() > 0) {
//                    currentSelectedItems.clear();
//                }
                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                final View popupView = layoutInflater.inflate(R.layout.single_filter, null);
                TextView filter = (TextView) popupView.findViewById(R.id.tv_filter);
                TextView claer = (TextView) popupView.findViewById(R.id.tv_clear);
                interestsLayout = new GridLayoutManager(context, 2);
                recy_interest_list_filter = (RecyclerView) popupView.findViewById(R.id.recy_interest_list_filter);
                recy_interest_list_filter.setLayoutManager(interestsLayout);
                if (interestsList != null && interestsList.size() > 0) {
                    interestsAdapter = new InterestFilterAdapter(SingleDatingActivity.this, interestsList, currentSelectedItems, this, this);
                    recy_interest_list_filter.setAdapter(interestsAdapter);
                }

                final EditText et_min_age, et_max_age;
                ImageView img_back = (ImageView) popupView.findViewById(R.id.img_back);
                final LinearLayout title_bar = (LinearLayout) popupView.findViewById(R.id.title_bar);
                img_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        discover_title_bar.setVisibility(View.VISIBLE);
                        popupWindow.dismiss();
                    }
                });


                country = (ToggleButton) popupView.findViewById(R.id.tb_country_filter);
                ch_male = (CheckBox) popupView.findViewById(R.id.ch_male);
                ch_female = (CheckBox) popupView.findViewById(R.id.ch_female);
                et_min_age = (EditText) popupView.findViewById(R.id.et_min_age);
                et_max_age = (EditText) popupView.findViewById(R.id.et_max_age);

                et_min_age.setText(min_age);
                et_max_age.setText(max_age);

                seek_bar_list = (SeekBar) popupView.findViewById(R.id.seek_bar_list);
                tv_seek_list_show = (TextView) popupView.findViewById(R.id.tv_seek_list_show);
                seek_sort = (SeekBar) popupView.findViewById(R.id.seek_sort);
                lay_distance = (LinearLayout) popupView.findViewById(R.id.lay_distance);
                lay_date = (LinearLayout) popupView.findViewById(R.id.lay_date);
                radio_distance = (RadioButton) popupView.findViewById(R.id.radio_distance);
                radio_date = (RadioButton) popupView.findViewById(R.id.radio_date);
                tv_sort_value = (TextView) popupView.findViewById(R.id.tv_sort_value);

                if (own_country.equalsIgnoreCase(
                        "yes")) {
                    country.setChecked(true);
                } else {
                    country.setChecked(false);
                }
                if (male.equalsIgnoreCase("M")) {
                    ch_male.setChecked(true);
                } else {
                    ch_male.setChecked(false);
                }
                if (female.equalsIgnoreCase("F")) {
                    ch_female.setChecked(true);
                } else {
                    ch_female.setChecked(false);
                }
                filter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        min_age = et_min_age.getText().toString().trim();
                        max_age = et_max_age.getText().toString().trim();

                        popupWindow.dismiss();
                        page_no = 1;
                        if (userList.size() > 0) {
                            userList.clear();
                        }
                        last_days = tv_seek_list_show.getText().toString().trim();
                        radius = tv_sort_value.getText().toString().trim();
                        try {
                            items = android.text.TextUtils.join(" , ", currentSelectedItems);
                            Log.e("valuesSelected ", items);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        hitApi();
                    }
                });
                claer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        own_country = "no";
                        male = "";
                        female = "";
                        min_age = "";
                        max_age = "";
                        country.setChecked(false);
                        ch_male.setChecked(false);
                        ch_female.setChecked(false);
                        et_min_age.setText("");
                        et_max_age.setText("");

                        page_no = 1;
                        if (userList.size() > 0) {
                            userList.clear();
                        }

                        /*addnew */

                        // listcheck = "NO";
                        seek_bar_list.setProgress(5);
                        last_days = "forever";
                        tv_seek_list_show.setText(last_days);

                        seek_sort.setProgress(100);
                        radius = "Everywhere";
                        items = "";
                        if (currentSelectedItems != null && currentSelectedItems.size() > 0) {
                            currentSelectedItems.clear();
                        }
                        popupWindow.dismiss();
                      //  radio_distance();
                        //tb_country.setChecked(false);
                        hitApi();
                    }
                });

                country.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (country.isChecked()) {
                            own_country = "yes";
                        } else {
                            own_country = "no";
                        }
                    }
                });

                ch_male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (ch_male.isChecked()) {
                            male = "M";
                        } else {
                            male = "";
                        }
                    }
                });
                ch_female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (ch_female.isChecked()) {
                            female = "F";
                        } else {
                            female = "";
                        }
                    }
                });
                lay_distance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        radio_distance();
                    }
                });

                lay_date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        radio_date();
                    }
                });
                radio_distance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        radio_distance();
                    }
                });
                radio_date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        radio_date();
                    }
                });
                //radio_distance();
                seek_bar_list.setMax(5);
                seek_bar_list.setProgress(5);
                seekday(5);
                seek_bar_list.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                        seekday(progress);


                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                seek_sort.setMax(16);
                seek_sortradius(16);
                seek_sort.setProgress(16);
                seek_sort.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                        seek_sortradius(progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                popupWindow.setFocusable(true);
                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setOutsideTouchable(true);
                popupWindow.showAtLocation(popupView, 0, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                break;

            case R.id.tv_discover:
                break;

            case R.id.img_arrow:
                if (show == 1) {
                    show = 0;
//                    recy_single.setVisibility(View.VISIBLE);
                    img_arrow.setImageResource(R.drawable.ic_down_arrow_discover);
                    frg = new DiscoverHome();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, frg);
                    fragmentTransaction.commit();
                    while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getSupportFragmentManager().popBackStackImmediate();
                    }
                } else {
                    show = 1;
                    img_arrow.setImageResource(R.drawable.ic_up_arrow_discover);
                    recy_single.setVisibility(View.GONE);
                    frg = new DiscoverCategories();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, frg);
                    fragmentTransaction.commit();
                    while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getSupportFragmentManager().popBackStackImmediate();
                    }
                }
                break;
        }
    }

    @Override
    public void onLoadMore() {
        if (userList.size() > 5) {
            page_no = page_no + 1;
            hitApiAgain();
        }

    }


    private void seekday(int progress) {
        if (progress == 0) {
            last_days = "24 hours";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 1) {
            last_days = "3 days";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 2) {
            last_days = "7 days";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 3) {
            last_days = "14 days";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 4) {
            last_days = "30 days";
            tv_seek_list_show.setText(last_days);
        } else if (progress == 5) {
            last_days = "forever";
            tv_seek_list_show.setText(last_days);
        } else {
            last_days = "forever";
            tv_seek_list_show.setText(last_days);
        }

    }

    private void seek_sortradius(int progress) {
        if (progress == 0) {
            radius = "1" + " km";
            tv_sort_value.setText(radius);
        } else if (progress <= 4) {
            radius = String.valueOf(progress) + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 5) {
            radius = "7" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 6) {
            radius = "10" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 7) {
            radius = "15" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 8) {
            radius = "30" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 9) {
            radius = "60" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 10) {
            radius = "100" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 11) {
            radius = "200" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 12) {
            radius = "300" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 13) {
            radius = "400" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 14) {
            radius = "500" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 15) {
            radius = "1000" + " km";
            tv_sort_value.setText(radius);
        } else if (progress == 16) {
            radius = "Everywhere";
            tv_sort_value.setText(radius);
        }

    }

    private void radio_distance() {
        lay_distance.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));
        lay_date.setBackgroundColor(this.getResources().getColor(R.color.color_gray));

        radio_distance.setTextColor(this.getResources().getColor(R.color.color_black));
        radio_date.setTextColor(this.getResources().getColor(R.color.color_white));


        radio_distance.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));
        radio_date.setBackgroundColor(this.getResources().getColor(R.color.color_gray));


        radio_distance.setChecked(true);
        radio_date.setChecked(false);


        sorting = "distance";
        seek_sort.setProgress(16);
        radius = "Everywhere";
        tv_sort_value.setText(radius);


    }

    private void radio_date() {
        lay_distance.setBackgroundColor(this.getResources().getColor(R.color.color_gray));
        lay_date.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));


        radio_distance.setTextColor(this.getResources().getColor(R.color.color_white));
        radio_date.setTextColor(this.getResources().getColor(R.color.color_black));

        radio_distance.setBackgroundColor(this.getResources().getColor(R.color.color_gray));
        radio_date.setBackgroundColor(this.getResources().getColor(R.color.color_yallow));

        radio_distance.setChecked(false);
        radio_date.setChecked(true);


        sorting = "date";
        //seek_sort.setProgress(8);
        seek_sort.setProgress(100);
        radius = "Everywhere";
        tv_sort_value.setText(radius);
        //tv_sort_value.setText("30 km");
    }

    @Override
    public void onItemCheck(String item) {
        currentSelectedItems.add(item);
    }

    @Override
    public void onItemUncheck(String item) {
        currentSelectedItems.remove(item);
    }
}

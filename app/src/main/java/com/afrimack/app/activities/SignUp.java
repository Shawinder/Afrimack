package com.afrimack.app.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.appbeans.User;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.SignupResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.services.Locationservice;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.afrimack.app.utils.Common.checkPermissions;
import static com.afrimack.app.utils.Common.dismissLoadingDialog;
import static com.afrimack.app.utils.Common.displayErrorDialog;
import static com.afrimack.app.utils.Common.displayLoadingDialog;
import static com.afrimack.app.utils.Common.displayLongToast;


public class SignUp extends BaseActivity implements View.OnClickListener {
    private ImageView img_back, img_tick;
    private TextView tv_titel;
    private EditText et_emailId, et_password, et_firstName, et_lastName, et_re_enter_password_signup;
    private TextView tv_showLastName, tv_teramPolicy, tv_policy;
    private Button btn_signUp;
    private ImageView img_profile;

    private String device_token;
    private String email, password, firstName, lastName, repeatPassword;
    private String profile;
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String TAG = "SignUp";
    RestClient restClient;


    private PopupWindow popupWindow;
    private LocationManager manager;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static BitmapFactory.Options bitmap_factory;
    private static Uri uri;
    private static Bitmap bm;
    public static String selectedImagePath = null;
    public static String imgString = null, imgString1;
    private ByteArrayOutputStream stream;
    byte[] imageInByte;
    private Dialog dialog;
    private TextView tv_show_hide;
    private boolean showPwdFlag = false;
    private Context context = this;
    private String lat = "", longi = "", country = "", address = "";
    private AlertDialog gpsAlertDialog;
    private boolean isGpsDialogShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (!isMyServiceRunning()) {
                startService(new Intent(SignUp.this, Locationservice.class));
            }
        }
        restClient = new RestClient(this);
        device_token = Config.getDeviceToken(this);
        try {
            lat = Config.getlatitude(this);
            longi = Config.getlongitude(this);
//            try {
//                country = CommonUtils.getInstance().getCountryName(context, Double.valueOf(lat), Double.valueOf(longi));
//                address = CommonUtils.getInstance().getCompleteAddress(context, Double.valueOf(lat), Double.valueOf(longi));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        init();

        //setdata();
    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getPackageName() + ".services.Locationservice").equals(service1.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        // img_tick = (ImageView) findViewById(R.id.img_tick);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.app_titel).toString());
        img_back.setOnClickListener(this);
        //  img_tick.setOnClickListener(this);
        //************end titel*****************//
        et_re_enter_password_signup = (EditText) findViewById(R.id.et_re_enter_password_signup);
        et_emailId = (EditText) findViewById(R.id.et_email_signup);
        et_password = (EditText) findViewById(R.id.et_password_signup);
        et_firstName = (EditText) findViewById(R.id.et_first_name);
        et_lastName = (EditText) findViewById(R.id.et_list_name);
        btn_signUp = (Button) findViewById(R.id.btn_signup);
        tv_showLastName = (TextView) findViewById(R.id.tv_show_last_name);
        // tv_show_hide = (TextView) findViewById(R.id.tv_show_hide);
       /* tv_teramPolicy = (TextView) findViewById(R.id.tv_trems_policy);
        tv_policy = (TextView) findViewById(R.id.tv_policy);*/
        img_profile = (ImageView) findViewById(R.id.img_profle_signup);

        btn_signUp.setOnClickListener(this);
        img_profile.setOnClickListener(this);
       /* tv_teramPolicy.setOnClickListener(this);
        tv_policy.setOnClickListener(this);*/
        //  tv_show_hide.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            onBackPressed();
        }
        if (v == btn_signUp) {
            v.startAnimation(AppConstants.buttonClick);
            callApi();

        }
        if (v == img_profile) {

            if (checkPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, 99)) {

                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.camera_dalog, null);
                Button image_camera = (Button) popupView.findViewById(R.id.image_camera);
                Button image_gallery = (Button) popupView.findViewById(R.id.image_gallery);
                // Button camera_cancel = (Button) popupView.findViewById(R.id.camera_cancel);

                image_camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        captureImageFromCamera();
                        popupWindow.dismiss();
                    }
                });
                image_gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                        captureImageFromGallery();
                    }
                });

                popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setOutsideTouchable(true);
                popupWindow.showAsDropDown(img_profile);
            }

        }
        if (v == img_tick) {
            callApi();
        }
    }

    private void showPwd() {
        if (showPwdFlag) {
            tv_show_hide.setText(getResources().getString(R.string.show));
            et_password.setInputType(129);
            showPwdFlag = false;
        } else {
            tv_show_hide.setText(getResources().getString(R.string.hide));
            et_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            showPwdFlag = true;
        }
    }

    private void callApi() {
        email = et_emailId.getText().toString().trim();
        password = et_password.getText().toString().trim();
        firstName = et_firstName.getText().toString().trim();
        lastName = et_lastName.getText().toString().trim();
        repeatPassword = et_re_enter_password_signup.getText().toString().trim();

        if (email.length() == 0 && password.length() == 0 && firstName.length() == 0 && lastName.length() == 0) {

            displayErrorDialog(getResources().getString(R.string.required_field), this);
        } else if (email.length() == 0) {

            displayErrorDialog(getResources().getString(R.string.fill_email), this);
        } else if (password.length() == 0) {

            displayErrorDialog(getResources().getString(R.string.fill_password), this);

        } else if (password.length() < 6) {
            displayErrorDialog(getString(R.string.password_alert), this);
        } else if (!password.equalsIgnoreCase(repeatPassword)) {
            displayErrorDialog(getString(R.string.password_mismatch), this);
        } else if (firstName.length() == 0) {

            displayErrorDialog(getResources().getString(R.string.fill_first_name), this);
        } else if (firstName.length() < 3) {
            displayErrorDialog(getString(R.string.first_name_alert), this);
        } else if (lastName.length() == 0) {
            displayErrorDialog(getResources().getString(R.string.fill_last_name), this);
        } else if (lastName.length() < 3) {
            displayErrorDialog(getString(R.string.last_name_alert), this);
        } else if (selectedImagePath == null) {
            displayErrorDialog(getResources().getString(R.string.select_image), this);
        } else if (!email.matches(EMAIL_REGEX)) {

            displayErrorDialog(getResources().getString(R.string.invalid_email), this);
        } else {

//                User user = new User(firstName, lastName, email, selectedImagePath, password, device_token);

            if (Config.getlatitude(this).equalsIgnoreCase("")) {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ShowGpsDialog();
                } else {
                    removeGpsDialog();
                    if (!isMyServiceRunning()) {
                        startService(new Intent(SignUp.this, Locationservice.class));
                    }
                    if (ConnectionDetector.isNetAvail(getApplicationContext())) {
//                        try {
                        lat = Config.getlatitude(this);
                        longi = Config.getlongitude(this);
//                            try {
//                                country = CommonUtils.getInstance().getCountryName(context, Double.valueOf(lat), Double.valueOf(longi));
//                                address = CommonUtils.getInstance().getCompleteAddress(context, Double.valueOf(lat), Double.valueOf(longi));
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }

                        createUserWithEmailAndPassword(firstName, lastName, email, imageInByte, password, device_token, lat, longi);
                    } else {
                        displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                    }
                }
            } else {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ShowGpsDialog();
                } else {

                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        if (!isMyServiceRunning()) {
                            startService(new Intent(SignUp.this, Locationservice.class));
                        }
                    }
                    if (ConnectionDetector.isNetAvail(getApplicationContext())) {
                        try {
                            lat = Config.getlatitude(this);
                            longi = Config.getlongitude(this);
//                            try {
//                                country = CommonUtils.getInstance().getCountryName(context, Double.valueOf(lat), Double.valueOf(longi));
//                                address = CommonUtils.getInstance().getCompleteAddress(context, Double.valueOf(lat), Double.valueOf(longi));
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        createUserWithEmailAndPassword(firstName, lastName, email, imageInByte, password, device_token, lat, longi);
                    } else {
                        displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                    }
                }
            }


//                createUserWithEmailAndPassword(firstName, lastName, email, uri, password, device_token, country, address);


        }

    }

    private void ShowGpsDialog() {
        // AndyUtils.removeCustomProgressDialog();
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                SignUp.this);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("No GPS")
                //.setTitle(Html.fromHtml("<font color='#ffdd00'>No GPS</font>"))
                .setMessage(getString(R.string.gps_info))
                .setPositiveButton(getString(R.string.gps_seating),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                gpsAlertDialog.dismiss();
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeGpsDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                //finish();
                            }
                        });

        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;

        }
    }

    /****************************
     * Capturing Image from camera
     ****************************/
    private void captureImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /****************************
     * Capturing Image from gallery
     ****************************/
    private void captureImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, System.currentTimeMillis() + "", null);
        return Uri.parse(path);
    }

    /*****
     * Receiving activity result method will be called after closing the camera
     ******/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            previewImage(0, data);

        } else if (requestCode == GALLERY_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            previewImage(1, data);

        } else {
//            Toast.makeText(this, " cancel capture image ", Toast.LENGTH_SHORT).show();
        }
    }

    private void previewImage(int previewfrom, Intent data) {
        switch (previewfrom) {
            case 0:
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                uri = getImageUri(context, thumbnail);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                imageInByte = bytes.toByteArray();
                imgString = Base64.encodeToString(imageInByte,
                        Base64.DEFAULT);

                img_profile.setImageBitmap(thumbnail);
                //final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/TAXWAYIMAGES/";

                final String dir = Environment.getExternalStorageDirectory() + "/AfrimackImages/";
                File newdir = new File(dir);
                if (!newdir.exists())
                    newdir.mkdirs();


                File destination = new File(newdir,
                        System.currentTimeMillis() + ".png");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //  Utils.ShowToast(getActivity(), destination.getAbsolutePath());
                // Log.d("picturePath", destination.getAbsolutePath() + "camera");

                selectedImagePath = destination.getAbsolutePath();


                break;
            case 1:
                uri = data.getData();
                Log.d("uri1", uri + "");
                try {


                    selectedImagePath = Common.getPath(this, uri);

                    img_profile.setImageBitmap(Common.getImageFromPath(selectedImagePath, 500, 500));
                    imageInByte = stream.toByteArray();
                    imgString = Base64.encodeToString(imageInByte,
                            Base64.DEFAULT);
/*
                    bm = (Common.getImageFromPath(selectedImagePath, 500, 500));

                    Log.d("Bitmap", "" + bm);

                    Matrix matrix1 = new Matrix();
                    matrix1.postRotate(90);
//                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix1, true);
                    bm = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    imageInByte = stream.toByteArray();
                    imgString = Base64.encodeToString(imageInByte,
                            Base64.DEFAULT);
                    Log.e("imageview", imgString);*/
                    //Config.setSellImage(imgString,this);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
        }
    }

}

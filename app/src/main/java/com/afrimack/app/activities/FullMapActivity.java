package com.afrimack.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import static com.google.android.gms.maps.model.BitmapDescriptorFactory.defaultMarker;

public class FullMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel;
    private GoogleMap mMapfull;
    private double lat, lng;
    private Marker marker;
    private int REQUEST_CODE_AUTOCOMPLETE = 1;
    private LatLng latLngnew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_map);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            lat = Double.parseDouble(Config.getlatitude(this));
            lng = Double.parseDouble(Config.getlongitude(this));
        } else {
            // lat = Double.parseDouble(extras.getString("latitude"));
            // lng = Double.parseDouble(extras.getString("longitude"));
            lat = extras.getDouble("latitude");
            lng = extras.getDouble("longitude");

        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_filter);

        mapFragment.getMapAsync(this);

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.choose_location).toString());
        img_back.setOnClickListener(this);
//        img_tick.setOnClickListener(this);

        //************end titel*****************//


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMapfull = googleMap;
        LatLng sydney = new LatLng(lat, lng);
        marker = mMapfull.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lng))
                .title("Post Location").icon(defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        CameraUpdate center =
                CameraUpdateFactory.newLatLng(sydney);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);

        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);

        mMapfull.setOnMapClickListener(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {

        latLngnew = latLng;
        lat = latLng.latitude;
        lng = latLng.longitude;
        mMapfull.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        if (marker != null) {
            marker.remove();
        }
        marker = mMapfull.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Post Location").icon(defaultMarker(BitmapDescriptorFactory.HUE_RED)));//icon(BitmapDescriptorFactory.fromResource(R.drawable.location)));

        if (latLngnew != null) {
            ActivityFilter.setnewlocation("FullMap", latLngnew);
        }
        finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_tick:
                if (latLngnew != null) {
                    ActivityFilter.setnewlocation("FullMap", latLngnew);
                }
                finish();
                break;
            case R.id.img_back:
                finish();
                break;
        }

    }


}

package com.afrimack.app.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.BuildConfig;
import com.afrimack.app.R;
import com.afrimack.app.adapters.HorizontalRecyclerAdapter;
import com.afrimack.app.adapters.PickImageAdapter;
import com.afrimack.app.adapters.SellViewPagerAdapter;
import com.afrimack.app.constants.CustomPhotoGalleryActivity;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.customviews.TextViewNormal;
import com.afrimack.app.customviews.TextViewNormalBold;
import com.afrimack.app.fragments.FragSellRent;
import com.afrimack.app.utils.Common;
import com.brsoftech.core_utils.base.BaseAppCompatActivity;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.afrimack.app.utils.Common.checkPermissions;

/**
 * Created by ubuntu on 1/5/17.
 */
public class SellOffer extends BaseAppCompatActivity implements View.OnClickListener, HorizontalRecyclerAdapter.ClickListener {

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    static public ArrayList<String> photos = new ArrayList<>();
    private static BitmapFactory.Options bitmap_factory;
    private static Uri uri;
    private static Bitmap bm;
    private final int SELECT_PHOTO = 2;
    private final int PICK_IMAGE_MULTIPLE = 1;
    byte[] imageInByte;
    //public static String selectedImagePath;
    private ImageView img_back, img_tick;
    private TextView tv_titel;
    private Toolbar sellRentToolbar;
    private ViewPager sellRentViewPager;
    private TabLayout sellRentTabLayout;

    private PopupWindow popupWindow;
    private String selectedImagePath = null;
    private String imgString = null;
    private ByteArrayOutputStream stream;
    private Bitmap finalBitmap;
    private String photo = null;
    private ArrayList<String> imagesPathList;
    public static String GetImagrTepe;

    private RecyclerView recy_select_iamge;
    private PickImageAdapter imageAdapter;

    public static Bitmap thumbnail;
    public static List<Bitmap> images;
    private static List<String> imagesUri;
    public static HorizontalRecyclerAdapter adapter;
    public static RecyclerView myList;
    private LocationManager manager;
    private boolean isDataRecieved = false, isRecieverRegistered = false,
            isNetDialogShowing = false, isGpsDialogShowing = false;
    int m = 0;

    public static final int MY_PERMISSIONS_REQUEST_READ_CAMERA = 123;

    private Uri photoUri;
//    private CallbackManager callbackManager;

    private Uri fileUri; // file url to store image/video
    static public ArrayList<String> imagels = null;
    private static final String IMAGE_DIRECTORY_NAME = "Afrimack";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_offer);
        //Common.chackGps(this);

        init();
        imagels = new ArrayList<String>();
        Config.setSellImage(imgString, this);
//        callbackManager = CallbackManager.Factory.create();
//        if (Config.getLoginType(SellOffer.this).equalsIgnoreCase("fb")) {
//
//
//            AlertDialog.Builder alertDialogConfirm = new AlertDialog.Builder(SellOffer.this);
//            alertDialogConfirm.setTitle("Facebook authentication");
//            try {
//
//                alertDialogConfirm.setMessage("You need to allow permissions to share on facebook.");
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//
//            alertDialogConfirm.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                    try {
//                        LoginManager.getInstance().logOut();
//                        LoginManager.getInstance().logInWithPublishPermissions(
//                                SellOffer.this,
//                                Arrays.asList("publish_actions","manage_pages","publish_pages","user_posts"));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//
//                }
//            });
//            alertDialogConfirm.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.cancel();
//                }
//            });
//            alertDialogConfirm.show();
//
//
//        }


    }

    private void init() {
//        photos = new ArrayList<>();
        /*//**************titel bar**************/
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);
        img_tick.setVisibility(View.GONE);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
        //  tv_titel.setText(getString(R.string.my_afrimack).toString());

        img_back.setOnClickListener(this);

        // recy_select_iamge = (RecyclerView) findViewById(R.id.recy_select_iamge);

        //************end titel*****************//*/
        sellRentToolbar = (Toolbar) findViewById(R.id.sell_toolbar);
        sellRentViewPager = (ViewPager) findViewById(R.id.sell_viewpager);
        sellRentTabLayout = (TabLayout) findViewById(R.id.sell_tabs);
        sellRentViewPager.setOffscreenPageLimit(0);


        SellViewPagerAdapter sellRentViewPagerAdapter = new SellViewPagerAdapter(getSupportFragmentManager());
        try {
            if (Config.getPostType(this) != null && Config.getPostType(this).equalsIgnoreCase("S")) {
                sellRentViewPagerAdapter.addFrag(FragSellRent.newInstance(getString(R.string.SELL_OFFER)), 1, getString(R.string.SELL_OFFER));

            } else if (Config.getPostType(this) != null && Config.getPostType(this).equalsIgnoreCase("R")) {
                sellRentViewPagerAdapter.addFrag(FragSellRent.newInstance(getString(R.string.RENT)), 2, getString(R.string.RENT));

            } else {
                sellRentViewPagerAdapter.addFrag(FragSellRent.newInstance(getString(R.string.SELL_OFFER)), 1, getString(R.string.SELL_OFFER));
                sellRentViewPagerAdapter.addFrag(FragSellRent.newInstance(getString(R.string.RENT)), 2, getString(R.string.RENT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        sellRentViewPager.setAdapter(sellRentViewPagerAdapter);
        sellRentTabLayout.setupWithViewPager(sellRentViewPager);
        sellRentTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        sellRentTabLayout.setTabMode(TabLayout.MODE_FIXED);
        TextViewNormal tabSell = (TextViewNormal) LayoutInflater.from(this).inflate(R.layout.custom_tab_sell, null);
        TextViewNormal tabRent = (TextViewNormal) LayoutInflater.from(this).inflate(R.layout.custom_tab_rent, null);
        try {
            if (Config.getPostType(this) != null && Config.getPostType(this).equalsIgnoreCase("S")) {
                sellRentTabLayout.getTabAt(0).setCustomView(tabSell);
            } else if (Config.getPostType(this) != null && Config.getPostType(this).equalsIgnoreCase("R")) {
                sellRentTabLayout.getTabAt(0).setCustomView(tabRent);
            } else {
                sellRentTabLayout.getTabAt(0).setCustomView(tabSell);
                sellRentTabLayout.getTabAt(1).setCustomView(tabRent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        images = new ArrayList<Bitmap>();
        imagesUri = new ArrayList<String>();
        images.add(null);
        imagesUri.add(null);
        Collections.reverse(imagesUri);
        final Integer cancelButton = R.drawable.ic_cancel_home;

        myList = (RecyclerView) findViewById(R.id.horizontalRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        layoutManager.setReverseLayout(true);

        adapter = new HorizontalRecyclerAdapter(this, images, imagesUri, cancelButton, this, Config.getPostType(this));
        myList.setLayoutManager(layoutManager);
        myList.setItemAnimator(new DefaultItemAnimator());
        myList.setFocusable(true);
        myList.computeHorizontalScrollOffset();
        myList.setAdapter(adapter);
        adapter.setOnClickListener(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragSellRent.image_delete = "";
    }

    @Override
    public void onClick(View v) {

        if (v == img_back) {
            onBackPressed();
        } else if (v.getId() == R.id.image) {
            if (checkPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, 99)) {

            }
            if (images.size() < 6) {
                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.camera_dalog, null);
                Button image_camera = (Button) popupView.findViewById(R.id.image_camera);
                Button image_gallery = (Button) popupView.findViewById(R.id.image_gallery);
                // Button camera_cancel = (Button) popupView.findViewById(R.id.camera_cancel);

                image_camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (ContextCompat.checkSelfPermission(SellOffer.this,
                                Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(SellOffer.this,
                                    new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_CAMERA);
                            //Toast.makeText(SellOffer.this, "Permission not  granted", Toast.LENGTH_SHORT).show();
                            //  popupWindow.dismiss();
                        }
                        else {
                            //Toast.makeText(SellOffer.this, "Permission already granted", Toast.LENGTH_SHORT).show();

                                captureImageFromCamera();
                                popupWindow.dismiss();
                        }
                    }
                });
                image_gallery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                        captureImageFromGallery();
                    }
                });

                popupWindow = new PopupWindow(
                        popupView,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                popupWindow.setBackgroundDrawable(new BitmapDrawable());
                popupWindow.setOutsideTouchable(true);
                popupWindow.showAsDropDown(v);

            } else {
                Common.displayLongToast(this, "You already reached the maximum number of pictures!");
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 123 :
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                   // Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
                    try {
                        captureImageFromCamera();
                        popupWindow.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                        new AlertDialog.Builder(this).
                                setTitle("Access Camera permission").
                                setMessage("You need to grant access camera permission to capture images" +
                                        "feature. Retry and grant it !").show();

                    } else {
                        new AlertDialog.Builder(this).
                                setTitle("Access Camera permission denied").
                                setMessage("You denied access camera permission." +
                                        " So, the feature will be disabled. To enable it" +
                                        ", go on settings and " +
                                        "grant access camera for the application").show();
                    }
                }
                break;
        }
    }



    /****************************
     * Capturing Image from camera
     ****************************/

    String mCurrentPhotoPath="";

    private File createImageFile()  {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        imageFileName="temp";

        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File dir = new File(Environment.getExternalStorageDirectory(), "myfolder");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        try {
            File image = File.createTempFile(imageFileName, ".jpg", dir);
            mCurrentPhotoPath = image.getAbsolutePath();
            return image;

        }catch (Exception e){

        }
        // Save a file: path for use with ACTION_VIEW intents
        return null;
    }


    private void captureImageFromCamera() {
    /*    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(getOutputMediaFileUri(MEDIA_TYPE_IMAGE).getPath());

       // photoUri = Uri.fromFile(photo);
       photoUri = FileProvider.getUriForFile(SellOffer.this, BuildConfig.APPLICATION_ID, photo);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);*/

        Intent takePictureIntent =new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (Exception e) {
                // Error occurred while creating the File
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                if (Build.VERSION.SDK_INT>=23){
                    photoUri = FileProvider.getUriForFile(this, "com.afrimack.app.fileprovider",
                            photoFile);
                }
                else  {
                    photoUri = Uri.fromFile(photoFile);
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePictureIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        }
    }

    /****************************
     * Capturing Image from gallery
     ****************************/
    private void captureImageFromGallery() {
        Intent intent = new Intent(SellOffer.this, CustomPhotoGalleryActivity.class);
        startActivityForResult(intent, PICK_IMAGE_MULTIPLE);
    }

    /*****
     * Receiving activity result method will be called after closing the camera
     ******/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // TODO Auto-generated method stub
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            previewImage(0, data);

        } else if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK) {
            if (CustomPhotoGalleryActivity.photosgallery != null) {

                for (int j = 0; j < CustomPhotoGalleryActivity.photosgallery.size(); j++) {

                    thumbnail = Common.getImageFromPath(CustomPhotoGalleryActivity.photosgallery.get(j), 1000, 500);
                    addImageInList(CustomPhotoGalleryActivity.photosgallery.get(j));


                }
            }
        } else {
//            Common.displayLongToast(this, " cancel capture image");
        }
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();
        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void previewImage(int previewfrom, Intent data) {
        switch (previewfrom) {

            case 0:
                try {
                    ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
                    int or = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    float toRotate = 0;
                    if (or == ExifInterface.ORIENTATION_ROTATE_90)
                        toRotate = 90;
                    else if (or == ExifInterface.ORIENTATION_ROTATE_180)
                        toRotate = 180;
                    else if (or == ExifInterface.ORIENTATION_ROTATE_270)
                        toRotate = 270;
                    thumbnail = getResizedBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri), 800);
                    if (toRotate != 0) {
                        thumbnail = rotateBitmap(thumbnail, toRotate);
                    }
                    selectedImagePath = saveToInternalSorage1(thumbnail);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!selectedImagePath.isEmpty())
                    addImageInList(selectedImagePath);

                for (int j = 0; j < photos.size(); j++) {

                    Log.e("photosphotos", photos.get(j) + "");
                }
                Config.setSellImage(selectedImagePath, this);
                break;
            case 1:

                //If uploaded with the new Android Photos gallery

                ClipData clipData = data.getClipData();
                if (clipData != null && clipData.getItemCount() > 0) {
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        clipData.getItemAt(i);

                        ClipData.Item item = clipData.getItemAt(i);
                        Uri uri = item.getUri();

                        String pathimages = getRealPathFromURI(this, uri);
                        photos.add(pathimages);

                    }
                } else {
                    Uri selectedImage = data.getData();
                    InputStream imageStream;
                    try {
                        imageStream = getContentResolver().openInputStream(selectedImage);
                        Bitmap yourSelectedImage = BitmapFactory.decodeStream(imageStream);
                        // Log.e("photos",selectedImage+" vv"+saveToInternalSorage1(yourSelectedImage));
                        photos.add(saveToInternalSorage1(yourSelectedImage));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    public static void addImageInList(String selectedImagePath) {
        if (images.size() < 6) {
            images.add(0, thumbnail);
            imagesUri.add(0, selectedImagePath);
            photos.add(selectedImagePath);
            adapter.notifyDataSetChanged();
            imagels.add(0, "");
            myList.smoothScrollToPosition(images.size());
        }
    }

    public static void addImageInList(String selectedImagePath, String id) {

        if (images.size() < 6) {
            images.add(0, thumbnail);
            imagesUri.add(0, selectedImagePath);
            imagels.add(0, id);
            adapter.notifyDataSetChanged();
            myList.smoothScrollToPosition(images.size());
        }
    }

    public void RemoveImageFromList(int position) {
        try {
            if (imagels.size() > 0) {
                if (FragSellRent.image_delete.length() == 0) {
                    FragSellRent.image_delete = imagels.get(position).toString();
                } else {
                    if (imagels.get(position).toString().length() != 0) {
                        FragSellRent.image_delete = FragSellRent.image_delete + "," + imagels.get(position).toString();
                    }
                }
                Log.d("image_delete", FragSellRent.image_delete);
                imagels.remove(position);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        images.remove(position);
        imagesUri.remove(position);
        photos.clear();
        if (imagesUri.size() == 1) {
            photos.clear();
        } else {
            for (int i = 0; i < imagesUri.size(); i++) {
                photos.add(imagesUri.get(i));
            }
        }

        Log.e("photos ", photos.size() + "");
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(HorizontalRecyclerAdapter.RecyclerViewHolder view, int position) {
        this.RemoveImageFromList(position);
    }


    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null,
                    null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    private String saveToInternalSorage1(Bitmap bitmapImage) {

        File folder = new File(Environment.getExternalStorageDirectory()
                + "/Afrimack");
        if (!folder.exists()) {
            folder.mkdir();
        }
        int pathnew = (int) System.currentTimeMillis();

        File mypath = new File(folder, pathnew + ".jpg");

        FileOutputStream fos = null;
        try {
            mypath.createNewFile();
            fos = new FileOutputStream(mypath);


            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return folder.getAbsolutePath() + "/" + pathnew + ".jpg";
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

    }

    /*
     * returning image / video
	 */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


}
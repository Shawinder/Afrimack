package com.afrimack.app.activities;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.OtherUserAdapter;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.OtherUserResponseModel;
import com.afrimack.app.recycler.DividerDecoration;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferUserActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back, img_tick;
    private TextView tv_titel;

    private TextView tvProfileName, tvProfileRating, tvProfileSales, tvProfilePurchased, tvProfileSince;
    private RatingBar rbProfileRating;
    private ImageView cIvProfile, img_heart_offer_act, img_facebook;
    private LinearLayout rating_layout;

    private RecyclerView recycler_offer_sell;
    private StaggeredGridLayoutManager GridLayoutManager;
    private OtherUserAdapter userAdapter;
    private TextView no_sell;
    private String facebook_Id;
    static String user_id;
    String login_user_id;
    private Dialog dialog;
    String imgPath = null;
    private TextView tv_profilr_type, txt_follow_offer_act;
    private RelativeLayout relative_follow_offer_act;
    private Context context = this;
    private String TAG = OfferUserActivity.class.getSimpleName();
    private String USERFOLLOW = "";
    private String offerUserFirebaseUID = "";
    private List<String> lisFriendsIds = new ArrayList<>();
    String roomId = "";
    private String single_registered = "";
    private int ratingCount = 0;
    private int blockedStatus = 0;
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_user);

        USERFOLLOW = Config.getFollow(context);
        login_user_id = Config.getUserId(this);
        user_id = Config.getOfferUserID(this);
//        Log.e("other_user_id sdsd",user_id);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {

            } else {
                user_id = extras.getString("other_user_id");
                from = extras.getString("from");
//                Log.e("other_user_id",user_id);
            }
        } else {
        }

        init();


        //  GridLayoutManager = new GridLayoutManager(2,1);
        DividerDecoration itemDecoration = new DividerDecoration(this);
        GridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recycler_offer_sell.setLayoutManager(GridLayoutManager);
        recycler_offer_sell.setHasFixedSize(true);
        recycler_offer_sell.addItemDecoration(itemDecoration);

        hitApi();
    }

    private void hitApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<OtherUserResponseModel> call = apiInterface.OtherUserProfile(user_id, login_user_id);
            call.enqueue(new Callback<OtherUserResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<OtherUserResponseModel> call, @NonNull Response<OtherUserResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        OtherUserResponseModel model = response.body();
                        if (model.isError() == false) {

                            blockedStatus = model.getResponse().getBlockedStatus();
                            tvProfileName.setText(model.getResponse().getName());
                            ratingCount = model.getResponse().getRating_count();
                            rbProfileRating.setRating(Float.parseFloat(String.valueOf(model.getResponse().getRating())));
                            tvProfileRating.setText(String.valueOf(model.getResponse().getRating_count()));
                            tvProfileSales.setText(String.valueOf(model.getResponse().getSelling() + " " + getResources().getString(R.string.sales)));
                            tvProfilePurchased.setText(String.valueOf(model.getResponse().getPurchase() + " " + getResources().getString(R.string.purchases)));
                            tvProfileSince.setText(getResources().getString(R.string.since) + " " + model.getResponse().getSince());

                            if (model.getResponse().getFirebase_UID() != null) {
                                offerUserFirebaseUID = model.getResponse().getFirebase_UID();
                            }

                            if (user_id.equalsIgnoreCase(Config.getUserId(context))) {
                                img_heart_offer_act.setVisibility(View.GONE);

                            } else {
                                if (model.getResponse().getSingle_registered().equalsIgnoreCase("Y")) {
                                    single_registered = "Y";
                                    img_heart_offer_act.setVisibility(View.VISIBLE);
                                } else {
                                    single_registered = "N";
                                    img_heart_offer_act.setVisibility(View.GONE);
                                }
                            }

                            if (user_id.equalsIgnoreCase(Config.getUserId(context))) {
                                img_facebook.setVisibility(View.GONE);
                            } else {

                                if (model.getResponse().getFbId() != null) {
                                    facebook_Id = model.getResponse().getFbId();
                                    //img_facebook.setVisibility(View.VISIBLE);
                                    img_facebook.setVisibility(View.GONE);
                                } else {
                                    img_facebook.setVisibility(View.GONE);
                                }
                            }

                            if (user_id.equalsIgnoreCase(Config.getUserId(context))) {
                                txt_follow_offer_act.setVisibility(View.GONE);
                            } else {
                                if (model.getResponse().getFollow_user().equalsIgnoreCase("Y")) {
                                    txt_follow_offer_act.setText("- UnFollow");
                                } else {
                                    txt_follow_offer_act.setText("+ Follow");
                                }
                            }
                            imgPath = model.getResponse().getImage();
                            Picasso
                                    .with(context)
                                    .load(model.getResponse().getImage())
                                    .placeholder(R.drawable.user_round)
                                    .into(cIvProfile);
                            tv_profilr_type.setText(model.getResponse().getProfile_type());
                            if (model.getResponse().getSelling_arr().size() > 0) {
                                recycler_offer_sell.setVisibility(View.VISIBLE);
                                no_sell.setVisibility(View.GONE);
                                userAdapter = new OtherUserAdapter(context, model.getResponse().getSelling_arr());
                                recycler_offer_sell.setAdapter(userAdapter);
                            } else {

                            }
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<OtherUserResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }


    }


    private void init() {
        rating_layout = (LinearLayout) findViewById(R.id.rating_layout);
        rating_layout.setOnClickListener(this);
        relative_follow_offer_act = (RelativeLayout) findViewById(R.id.relative_follow_offer_act);
        relative_follow_offer_act.setVisibility(View.VISIBLE);
        txt_follow_offer_act = (TextView) findViewById(R.id.txt_follow_offer_act);
        txt_follow_offer_act.setOnClickListener(this);
        img_heart_offer_act = (ImageView) findViewById(R.id.img_heart_offer_act);
       img_facebook = (ImageView) findViewById(R.id.img_facebook);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        img_heart_offer_act.setOnClickListener(this);
        img_facebook.setOnClickListener(this);


        tv_titel = (TextView) findViewById(R.id.tv_titel);
        // tv_titel.setText(getResources().getString(R.string.Selling));
        tv_titel.setText("User");
        no_sell = (TextView) findViewById(R.id.no_sell);

        tvProfileName = (TextView) findViewById(R.id.tvProfileName);
        tvProfileName.setOnClickListener(this);
        rbProfileRating = (RatingBar) findViewById(R.id.rbProfileRating);
        rbProfileRating.setOnClickListener(this);
        tvProfileRating = (TextView) findViewById(R.id.tvProfileRating);
        tvProfileSales = (TextView) findViewById(R.id.tvProfileSales);
        tvProfilePurchased = (TextView) findViewById(R.id.tvProfilePurchased);
        tvProfileSince = (TextView) findViewById(R.id.tvProfileSince);
        cIvProfile = (ImageView) findViewById(R.id.cIvProfile);

        recycler_offer_sell = (RecyclerView) findViewById(R.id.recycler_offer_sell);

        cIvProfile.setOnClickListener(this);
        tv_profilr_type = (TextView) findViewById(R.id.tv_profilr_type);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                if (from != null) {
                    v.startAnimation(AppConstants.buttonClick);
                  //  onBackPressed();
                    finish();
                } else {
                    Intent intent_back = new Intent(context, ActivityDiscoverDetail.class);
                    intent_back.putExtra("post_id", Config.getPostId(this));
                    startActivity(intent_back);
                    finish();
                }
                break;
            case R.id.tvProfileName:
                v.startAnimation(AppConstants.buttonClick);
                if (ratingCount > 0) {
                    Intent intent = new Intent(context, ActivityYourReviews.class);
                    intent.putExtra("id", user_id);
                    startActivity(intent);
                }
                break;

            case R.id.rating_layout:

                if (ratingCount > 0) {
                    Intent intent = new Intent(context, ActivityYourReviews.class);
                    intent.putExtra("id", user_id);
                    startActivity(intent);
                }
                break;

            case R.id.rbProfileRating:

                if (ratingCount > 0) {
                    Intent intent = new Intent(context, ActivityYourReviews.class);
                    intent.putExtra("id", user_id);
                    startActivity(intent);
                }
                break;

            case R.id.img_heart_offer_act:
                v.startAnimation(AppConstants.buttonClick);
                try {
                    if (SingleProfileViewActivity.singleProfileViewActivity != null) {
                        SingleProfileViewActivity.singleProfileViewActivity.finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (from != null) {
                    Intent intent = new Intent(context, SingleProfileViewActivity.class);
                    //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                    Config.setSingleChatId(user_id, context);
                    intent.putExtra("from", "SingleDation");
                    startActivity(intent);
                    finish();
                } else {
                    if (Config.getSingleRegister(OfferUserActivity.this).equalsIgnoreCase("Y")) {
                        Intent intent = new Intent(context, SingleProfileViewActivity.class);
                        //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                        Config.setSingleChatId(user_id, context);
                        startActivity(intent);
                        finish();
                    }

                    //==================changes by sukh===============
                    else {
                        Intent intent = new Intent(context, SingleDatingActivity.class);
                        //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                      //  Config.setSingleChatId(user_id, context);
                        startActivity(intent);
                        finish();
                    }
                    //=================================================
                }

                break;
            case R.id.img_facebook:
                v.startAnimation(AppConstants.buttonClick);
                Intent facebookIntent = getOpenFacebookIntent(this);
                startActivity(facebookIntent);
//                try {
//                    //try to open page in facebook native app.
//                    String uri = "fb://page/" + facebook_Id;    //Cutsom URL
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                    startActivity(intent);
//                } catch (ActivityNotFoundException ex) {
//                    //facebook native app isn't available, use browser.
//                    String uri = "http://touch.facebook.com/pages/x/" + facebook_Id;  //Normal URL
//                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                    startActivity(i);
//                }

                break;

            case R.id.txt_follow_offer_act:
                v.startAnimation(AppConstants.buttonClick);
                if (txt_follow_offer_act.getText().toString().equalsIgnoreCase("- UnFollow")) {
                    hitUnFollowUserApi(login_user_id, user_id);
                } else {
                    hitFollowUserApi(login_user_id, user_id);
                }
                break;

            case R.id.cIvProfile:
                if (imgPath != null) {
                    dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_image);
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.setCancelable(false);
                    ImageView chat_image = (ImageView) dialog.findViewById(R.id.imvChatImgOpen);
                    TextView dailog_close = (TextView) dialog.findViewById(R.id.dailog_close);
                    chat_image.setImageURI(Uri.parse(imgPath));
                    Picasso
                            .with(this)
                            .load(imgPath)
                            .into(chat_image);
                    dailog_close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (from != null) {
            finish();

        } else {
            Intent intent_back = new Intent(context, ActivityDiscoverDetail.class);
            intent_back.putExtra("post_id", Config.getPostId(this));
            startActivity(intent_back);
            finish();
        }
    }

    public Intent getOpenFacebookIntent(Context context) {



        try {
            if (isAppInstalled(getApplicationContext(), "com.facebook.katana")) {
                context.getPackageManager()
                        .getPackageInfo("com.facebook.katana", 0);
                //Checks if FB is even installed.
                return new Intent(Intent.ACTION_VIEW,
                        Uri.parse("fb://facewebmodal/f?href=https://www.facebook.com/" + facebook_Id)); //Trys to make intent with FB's URI
                       // Uri.parse("www.fb.com/" + facebook_Id)); //Trys to make intent with FB's URI
            } else {

                return new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.facebook.com/" + facebook_Id));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/arkverse")); //catches and opens a url to the desired page
        }




    }


    private void hitFollowUserApi(String user_id, String follower_id) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.UserFollow(user_id, follower_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            txt_follow_offer_act.setText("- UnFollow");
                            USERFOLLOW = "0";
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitUnFollowUserApi(String user_id, String follower_id) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.UserUnFollow(user_id, follower_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            txt_follow_offer_act.setText("+ Follow");
                            USERFOLLOW = "1";
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    public static boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}

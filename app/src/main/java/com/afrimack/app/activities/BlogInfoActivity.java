package com.afrimack.app.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.BlogInfoModel;
import com.afrimack.app.models.BlogModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afrimack.app.utils.Common.displayErrorDialog;
import static com.afrimack.app.utils.Common.displayLoadingDialog;

public class BlogInfoActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;

    private ImageView img_info_blog;
    private TextView tv_info;
    String setBlogId;
    private Context context = this;
    private String TAG = BlogInfoActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_info);
        setBlogId = Config.getBlogId(this);
        hitBlogInfoApi();
        init();

    }

    private void hitBlogInfoApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<BlogInfoModel> call = apiInterface.blogInfo(setBlogId);
            call.enqueue(new Callback<BlogInfoModel>() {
                @Override
                public void onResponse(@NonNull Call<BlogInfoModel> call, @NonNull Response<BlogInfoModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        BlogInfoModel blogInfoModel = response.body();

                        if (blogInfoModel.isError() == false) {

                            tv_titel.setText(blogInfoModel.getResponse().getTitle());
                            Picasso.with(context)
                                    .load(blogInfoModel.getResponse().getImage())
                                    .placeholder(R.drawable.no_image)
                                    .error(R.drawable.no_image)
                                    .into(img_info_blog);

                            tv_info.setText(Html.fromHtml(blogInfoModel.getResponse().getDescription()));

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BlogInfoModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);
        img_tick.setVisibility(View.GONE);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        //tv_titel.setText("Blog");
        img_back.setOnClickListener(this);

        //************end titel*****************//

        img_info_blog = (ImageView) findViewById(R.id.img_info_blog);
        tv_info = (TextView) findViewById(R.id.tv_info);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }


}

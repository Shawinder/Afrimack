package com.afrimack.app.activities;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.appbeans.AddressFinderBean;
import com.afrimack.app.appbeans.User;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;

import com.afrimack.app.insta.InstagramApp;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.InstagramLoginModel;
import com.afrimack.app.models.SignupResponseModel;
import com.afrimack.app.models.SocialData;
import com.afrimack.app.models.SocilResponseModel;
import com.afrimack.app.rest.RetrofitUtils;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.services.Locationservice;
import com.afrimack.app.sociallogin.FacebookLoginHandler;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.afrimack.app.utils.Common.displayErrorDialog;

public class SplashLogin extends BaseActivity implements View.OnClickListener, FacebookLoginHandler.SocialLoginListner {

    private CardView facebook_login, google_login, email_login;
    private Dialog dialogmessage;
    private FacebookLoginHandler facebookLoginHandler;
    private GoogleApiClient mGoogleApiClient;
    private Context context = this;
    private static final int GOOGLE_SIGN_IN = 123;
    public String LoginType;
    private TextView terms, feedback, imprint;
    private String mFullName, mEmail;
    private String first_name, media_type, media_id, device_token, email, last_name, user_image;
    private TextView tv_skip;
    private Dialog dialog;
    private EditText et_name, et_email, et_subject, et_messsage;
    private CallbackManager mCallbackManager;
    private String f_name, f_email, f_subject, f_message;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private LocationManager manager;
    private AlertDialog gpsAlertDialog;
    private boolean isGpsDialogShowing = false;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private TextView tv_teramPolicy, tv_policy;
    private String fb_access_token = "";
    private String lat = "", longi = "", country = "", address = "";
    //===============insta==========

    private InstagramApp mApp;
    private CardView rel_lay_insta_login;
    private Button btnConnect, btnViewInfo, btnGetAllImages;
    private LinearLayout llAfterLoginView;
    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();


    private boolean publishPermissionsRequested = false;
    private LoginResult readResultSuccess;
    List<String> publishPermissions = Arrays.asList("publish_actions");

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramApp.WHAT_FINALIZE) {
                userInfoHashmap = mApp.getUserInfo();
            } else if (msg.what == InstagramApp.WHAT_FINALIZE) {
                Toast.makeText(SplashLogin.this, "Check your network.",
                        Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    });

    /*
Geolocation key====   AIzaSyC9i31HXJ4zzHhwAAZkQniexxMLQ_95kZs*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_login);
        device_token = Config.getDeviceToken(this);

        setupGPlus();

        mApp = new InstagramApp(this, ConstantMain.CLIENT_ID,
                ConstantMain.CLIENT_SECRET, ConstantMain.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {
//                btnConnect.setText("Disconnect");
     //           llAfterLoginView.setVisibility(View.VISIBLE);
                // userInfoHashmap = mApp.
                mApp.fetchUserName(handler);
            }

            @Override
            public void onFail(String error) {

                Toast.makeText(SplashLogin.this, error, Toast.LENGTH_SHORT)
                        .show();
            }
        });

        setupFacebook();
        mCallbackManager = CallbackManager.Factory.create();

        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (!isMyServiceRunning()) {
                startService(new Intent(SplashLogin.this, Locationservice.class));
            }
        }
        init();
    }

    private void init() {
        facebook_login = (CardView) findViewById(R.id.rel_lay_facebook_login);
        google_login = (CardView) findViewById(R.id.rel_lay_google_login);
        email_login = (CardView) findViewById(R.id.rel_lay_email_login);
        rel_lay_insta_login=(CardView)findViewById(R.id.rel_lay_insta_login);
        terms = (TextView) findViewById(R.id.terms);
        feedback = (TextView) findViewById(R.id.feedback);
        imprint = (TextView) findViewById(R.id.imprint);

        facebook_login.setOnClickListener(this);
        google_login.setOnClickListener(this);
        email_login.setOnClickListener(this);
        rel_lay_insta_login.setOnClickListener(this);
        terms.setOnClickListener(this);
        feedback.setOnClickListener(this);
        imprint.setOnClickListener(this);

        tv_skip = (TextView) findViewById(R.id.tv_skip);
        tv_skip.setOnClickListener(this);

        tv_teramPolicy = (TextView) findViewById(R.id.tv_trems_policy);
        tv_policy = (TextView) findViewById(R.id.tv_policy);

        tv_teramPolicy.setOnClickListener(this);
        tv_policy.setOnClickListener(this);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
    }


    @Override
    public void onClick(View v) {

        if (v == facebook_login) {

            // Common.displayshortToast(SplashLogin.this, "Under Development");
            if (Config.getlatitude(this).equalsIgnoreCase("")) {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ShowGpsDialog();
                } else {
                    removeGpsDialog();
                    if (!isMyServiceRunning()) {
                        startService(new Intent(SplashLogin.this, Locationservice.class));
                    }
                    if (ConnectionDetector.isNetAvail(getApplicationContext())) {

                        facebookLoginFirebase();
                    } else {
                        displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                    }
                }
            } else {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ShowGpsDialog();
                } else {
                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        if (!isMyServiceRunning()) {
                            startService(new Intent(SplashLogin.this, Locationservice.class));
                        }
                    }
                    if (ConnectionDetector.isNetAvail(getApplicationContext())) {
                        facebookLoginFirebase();
                    } else {
                        displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                    }
                }
            }

        }
        if (v == google_login) {
            if (Config.getlatitude(this).equalsIgnoreCase("")) {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ShowGpsDialog();
                } else {
                    removeGpsDialog();
                    if (!isMyServiceRunning()) {
                        startService(new Intent(SplashLogin.this, Locationservice.class));
                    }

                    signIn();
                }
            } else {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ShowGpsDialog();
                } else {
                    if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        if (!isMyServiceRunning()) {
                            startService(new Intent(SplashLogin.this, Locationservice.class));
                        }
                    }
                    signIn();
                }
            }

        }
        if (v == terms) {
        }
        if (v == feedback) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.feedback);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);
            TextView tv_feedback = (TextView) dialog.findViewById(R.id.tv_feedback);
            TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);

            et_name = (EditText) dialog.findViewById(R.id.et_name);
            et_email = (EditText) dialog.findViewById(R.id.et_email);
            et_subject = (EditText) dialog.findViewById(R.id.et_subject);
            et_messsage = (EditText) dialog.findViewById(R.id.et_message);


            tv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            tv_feedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    f_name = et_name.getText().toString().trim();
                    f_email = et_email.getText().toString().trim();
                    f_subject = et_subject.getText().toString().trim();
                    f_message = et_messsage.getText().toString().trim();

                    if (f_name.length() == 0 && f_email.length() == 0 && f_subject.length() == 0 && f_message.length() == 0) {

                        showmessage(getResources().getString(R.string.required_field));

                    } else if (f_name.length() == 0) {
                        showmessage(getResources().getString(R.string.user_name_fill));
                    } else if (f_email.length() == 0) {
                        showmessage(getResources().getString(R.string.fill_email));
                    } else if (f_subject.length() == 0) {
                        showmessage(getResources().getString(R.string.fill_subject));
                    } else if (f_message.length() == 0) {
                        showmessage(getResources().getString(R.string.fill_message));
                    } else if (!f_email.matches(EMAIL_REGEX)) {
                        showmessage(getResources().getString(R.string.invalid_email));
                    } else {
                        feedbackapi();
                    }


                }


            });

            dialog.show();
        }
        if (v == imprint) {
            Intent intent = new Intent(this, ActivityImprint.class);
            startActivity(intent);
        }
        if (v == tv_policy) {
            Intent intent = new Intent(this, TermsConditionsPolicy.class);
            intent.putExtra("slug", "policy");
            startActivity(intent);
        }
        if (v == tv_teramPolicy) {
            Intent intent = new Intent(this, TermsConditionsPolicy.class);
            intent.putExtra("slug", "terms-conditions");
            startActivity(intent);
        }
        if (v == email_login) {
            v.startAnimation(AppConstants.buttonClick);
            if (Config.getlatitude(this).equalsIgnoreCase("")) {
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    ShowGpsDialog();
                    if (!isMyServiceRunning()) {
                        startService(new Intent(SplashLogin.this, Locationservice.class));
                    }
                } else {
                    removeGpsDialog();
                    if (ConnectionDetector.isNetAvail(this)) {
                        Intent intent = new Intent(this, Login.class);
                        startActivity(intent);
                        finish();
                    } else {
                        displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                    }
                }
            } else {
                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    if (!isMyServiceRunning()) {
                        startService(new Intent(SplashLogin.this, Locationservice.class));
                    }
                }
                if (ConnectionDetector.isNetAvail(this)) {
                    Intent intent = new Intent(this, Login.class);
                    startActivity(intent);
                    finish();
                } else {
                    displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);


                }
            }

        }
        if (v == tv_skip) {

            if (!(Config.getLoginBack(this)).equalsIgnoreCase("null") && !(Config.getLoginBack(this)).equalsIgnoreCase("")) {
                Config.setLoginBack("", this);
                Intent intent = new Intent(this, ActivityHome.class);
                Config.setSKip("Yes", this);
                Config.setLoginBack("", this);
                startActivity(intent);
            } else {
                finish();
            }

        }

        if(v== rel_lay_insta_login){


            if (mApp.hasAccessToken()) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(
                        SplashLogin.this);
                builder.setMessage("Disconnect from Instagram?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        mApp.resetAccessToken();


                                        // btnConnect.setVisibility(View.VISIBLE);
//                                        llAfterLoginView.setVisibility(View.GONE);
//                                        btnConnect.setText("Connect");
                                        // tvSummary.setText("Not connected");
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });
                final AlertDialog alert = builder.create();
                alert.show();
            } else {
                mApp.authorize();
            }

        }
    }

    private void feedbackapi() {

        if (CommonUtils.getInstance().isNetConnected(this)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.feedback(f_name, f_email, f_subject, f_message);


            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel model = response.body();
                        if (model.isError() == false) {
                            dailogMesaage(model.getMessage());
                        } else {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(this, getString(R.string.please_ensure_internet_connectivity));
        }


    }

    private void dailogMesaage(String message) {

        dialogmessage = new Dialog(this);
        dialogmessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogmessage.setContentView(R.layout.custom_dialog);
        dialogmessage.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogmessage.setCancelable(false);
        TextView mess_colose = (TextView) dialogmessage.findViewById(R.id.mess_colose);
        TextView dailog_message = (TextView) dialogmessage.findViewById(R.id.dailog_message);
        TextView entry_message = (TextView) dialogmessage.findViewById(R.id.entry_message);
        entry_message.setVisibility(View.GONE);
        TextView dalog_ok = (TextView) dialogmessage.findViewById(R.id.dalog_ok);
        TextView dalog_titel = (TextView) dialogmessage.findViewById(R.id.dalog_titel);
        dailog_message.setText(message + ".");

        dalog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogmessage.dismiss();
                finish();
            }
        });
        mess_colose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogmessage.dismiss();
                finish();

            }


        });

        dialogmessage.show();
    }

    private void showmessage(String mstring) {
        displayErrorDialog(mstring, this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                CommonUtils.getInstance().displayLoadingDialog(false, context);
                // Get account information
                mFullName = acct.getDisplayName();
                LoginType = "G";
                first_name = acct.getGivenName();
                media_type = "google";
                media_id = acct.getId();

                email = acct.getEmail();
                last_name = acct.getFamilyName();
                user_image = String.valueOf(acct.getPhotoUrl());
                Log.d("google image", user_image);
                firebaseAuthWithGoogle(acct);
            }
        } else {
            if (facebookLoginHandler != null) {
                LoginType = "F";
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
            }
            Log.e("DUMMY","Test");
        }
    }


    private void setupFacebook() {
        facebookLoginHandler = new FacebookLoginHandler(this);
    }

    public void setupGPlus() {
        try {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.OAuth_2_client_IDs_Web_Client))
                    .requestEmail()
                    .build();

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }

                    })
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void SocialLoginSuccess(SocialData socilaData) {

        first_name = socilaData.getFirstname();
        media_type = "fb";
        media_id = socilaData.getId();
        email = socilaData.getEmail();
        //email="";
        last_name = socilaData.getLastname();
        user_image = socilaData.getUser_image();

    }


    private void ShowGpsDialog() {
        // AndyUtils.removeCustomProgressDialog();
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                SplashLogin.this);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("No GPS")
                //.setTitle(Html.fromHtml("<font color='#ffdd00'>No GPS</font>"))
                .setMessage(getString(R.string.gps_info))
                .setPositiveButton(getString(R.string.gps_seating),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                gpsAlertDialog.dismiss();
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeGpsDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                // finish();
                            }
                        });

        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;

        }
    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getPackageName() + ".services.Locationservice").equals(service1.service.getClassName())) {

                return true;
            }
        }
        return false;
    }

    public void facebookLoginFirebase() {
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }

        LoginManager.getInstance().logInWithReadPermissions(this,
                Arrays.asList("public_profile", "user_friends", "email"));

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                CommonUtils.getInstance().displayLoadingDialog(false, context);

                handleFacebookAccessToken(loginResult.getAccessToken());

            }

            @Override
            public void onCancel() {
                Log.d("facebook result", "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("facebook result", "facebook:onError", error);


            }
        });
    }


    private void handleFacebookAccessToken(AccessToken token) {

        media_id = token.getUserId();
        fb_access_token = token.getToken();
        Log.e("Facebook token ", ">>>>>>>>>" + fb_access_token);

        AuthCredential authCredential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    final FirebaseUser user = mAuth.getCurrentUser();
                    media_type = "fb";
                   email = user.getEmail();
                   // email = "";
                    String name = user.getDisplayName();
                    try {
                        if (name != null) {
                            if (name.contains(" ")) {
                                first_name = name.substring(0, name.indexOf(" "));
                                last_name = name.substring(name.indexOf(" ") + 1, name.length());
                            } else {
                                first_name = name;
                                last_name = "";
                            }
                        } else {
                            first_name = "";
                            last_name = "";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    user_image = String.valueOf(user.getPhotoUrl());


                    //=========changes==========
                    if (email==null || email.equalsIgnoreCase("") || email.equalsIgnoreCase("null")) {
                        dialog = new Dialog(SplashLogin.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_fb_email);
                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                        final EditText et_email_fb = (EditText) dialog.findViewById(R.id.et_email_fb);
                      Button  cancel_email = (Button) dialog.findViewById(R.id.cancel_email);
                      Button  btn_send_email = (Button) dialog.findViewById(R.id.btn_send_email);

                        cancel_email.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });


                        btn_send_email.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                email=et_email_fb.getText().toString().trim();

                                if (!email.matches(emailPattern)) {
                                    showmessage(getResources().getString(R.string.invalid_email));
                                }
                                else {
                                    hitSocialLoginApi(user.getUid());
                                }
                            }
                        });

                        dialog.show();
                    }

                    else {
                        hitSocialLoginApi(user.getUid());

                    //==========================
                }
        }
                else

                {
                    // If sign in fails, display a message to the user.
                    CommonUtils.getInstance().dismissLoadingDialog();
                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                    Toast.makeText(SplashLogin.this, "Authentication failed.",
                            Toast.LENGTH_SHORT).show();

                }
            }
        });

    }




    private void hitSocialLoginApi(final String firebase_UID) {

        try {
            lat = Config.getlatitude(this);
            longi = Config.getlongitude(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (CommonUtils.getInstance().isNetConnected(context)) {

            Retrofit retrofit1 = new Retrofit.Builder()
                    .baseUrl(AppConstants.LOC_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ApiInterface apiInterface1 = retrofit1.create(ApiInterface.class);
            Call<AddressFinderBean> call1;
            String latlng = lat + "," + longi;
            call1 = apiInterface1.getCompleteAddress(latlng, true, getString(R.string.geolocation_api_key));
            call1.enqueue(new Callback<AddressFinderBean>() {
                @Override
                public void onResponse(Call<AddressFinderBean> calll, Response<AddressFinderBean> response) {
                    try {
                        AddressFinderBean mObj = response.body();
                        if (mObj != null && mObj.getStatus().equalsIgnoreCase("OK")) {
                            String country = "";
                            String address = "";
                            String fullAddress = mObj.getResults().get(0).getFormattedAddress();
                            try {
                                country = fullAddress.substring(fullAddress.lastIndexOf(",") + 1, fullAddress.length());
                                address = fullAddress;

//                                Log.e("country ", country);
//                                Log.e("address ", address);
//                                Log.e("device_token ", device_token);
//                                Log.e("first_name ", first_name);
//                                Log.e("media_type ", media_type);
//                                Log.e("media_id ", media_id);
//                                Log.e("email ", email);
//                                Log.e("last_name ", last_name);
//                                Log.e("user_image ", user_image);
//                                Log.e("firebase_UID ", firebase_UID);
//                                Log.e("fb_access_token ", fb_access_token);

                                //displayErrorDialog("Email:"+email +",Name:"+first_name+",media:"+media_id+",fb_token:"+fb_access_token,context);
                                ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                                Call<SocilResponseModel> call = apiInterface.SocialLogin(device_token, first_name, media_type, media_id, email, last_name, user_image, firebase_UID, fb_access_token, country, address);

                                call.enqueue(new Callback<SocilResponseModel>() {
                                    @Override
                                    public void onResponse(@NonNull Call<SocilResponseModel> call, @NonNull Response<SocilResponseModel> response) {

                                        try {
                                            CommonUtils.getInstance().dismissLoadingDialog();
                                            SocilResponseModel model = response.body();
                                            User users = null;

                                            if (model.isError() == false) {
                                                if (media_type.equalsIgnoreCase("fb")) {
                                                    users = new User(first_name, last_name, email, model.getResponse().getImage(), "facebook", device_token, "1");
                                                } else {
                                                    users = new User(first_name, last_name, email, model.getResponse().getImage(), "google", device_token, "1");
                                                }

                                                writeMsgToDatabaseSocialLogin(users);

                                                Config.setLoginUserUID(model.getResponse().getFirebase_UID(), context);


                                                Config.setUserName(model.getResponse().getFirst_name(), context);
                                                Config.setUserId(String.valueOf(model.getResponse().getId()), context);
                                                Config.setSingleRegister(model.getResponse().getSingle_registered(), context);
                                                Config.setLoginUserName(model.getResponse().getFirst_name() + " " + model.getResponse().getLast_name());
                                                Config.setLoginEmailId(model.getResponse().getEmail());
                                                Config.setLoginProfileImg(model.getResponse().getImage(), context);
                                                Config.setLoginType(media_type, context);


                                                /**********chat*************/
                                                String userId = String.valueOf(model.getResponse().getId());
                                                String userName = model.getResponse().getFirst_name() + " " + model.getResponse().getLast_name();
                                                String userImg = model.getResponse().getImage();


                                                Intent intenthome = new Intent(context, ActivityHome.class);
                                                intenthome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                intenthome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intenthome);
                                                finish();

                                            } else {
                                              displayErrorDialog(model.getMessage(), context);
                                                FirebaseAuth.getInstance().signOut();
                                            }


                                        } catch (Exception e) {
                                            CommonUtils.getInstance().dismissLoadingDialog();
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<SocilResponseModel> call, Throwable t) {
                                        Log.e(TAG, "error: " + t.toString());
                                        CommonUtils.getInstance().dismissLoadingDialog();
                                    }
                                });


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<AddressFinderBean> call, Throwable t) {

                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void firebaseAuthWithInstagram(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            user.getEmail();
                            media_id = user.getUid();
                            media_type = "google";
                            email = user.getEmail();
                            String name = user.getDisplayName();
                            first_name = name.substring(0, name.indexOf(" "));
                            last_name = name.substring(name.indexOf(" ") + 1, name.length());
                            user_image = String.valueOf(user.getPhotoUrl());
                            fb_access_token = "";
                            hitSocialLoginApi(media_id);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            CommonUtils.getInstance().dismissLoadingDialog();
                            Toast.makeText(SplashLogin.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            user.getEmail();
                            media_id = user.getUid();
                            media_type = "google";
                            email = user.getEmail();
                            String name = user.getDisplayName();
                            first_name = name.substring(0, name.indexOf(" "));
                            last_name = name.substring(name.indexOf(" ") + 1, name.length());
                            user_image = String.valueOf(user.getPhotoUrl());
                            fb_access_token = "";
                            hitSocialLoginApi(media_id);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            CommonUtils.getInstance().dismissLoadingDialog();
                            Toast.makeText(SplashLogin.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

}

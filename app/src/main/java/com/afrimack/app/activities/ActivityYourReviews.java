package com.afrimack.app.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.FollowingAdapters;
import com.afrimack.app.adapters.YourReviewAdapter;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.FollowingResponseModel;
import com.afrimack.app.models.ReviewsResponseModel;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afrimack.app.utils.Common.dismissLoadingDialog;
import static com.afrimack.app.utils.Common.displayErrorDialog;
import static com.afrimack.app.utils.Common.displayLoadingDialog;

public class ActivityYourReviews extends AppCompatActivity implements View.OnClickListener {
    private ImageView img_back, img_tick;
    private TextView tv_titel, no_of_reviews;

    private RecyclerView recyclerList;
    private YourReviewAdapter mAdapter;
    private List<ReviewsResponseModel> reviewList;
    private RecyclerView.LayoutManager mLayoutManager;

    private String user_id;
    private Context context = this;
    public String TAG = ActivityYourReviews.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_reviews);

        user_id = getIntent().getStringExtra("id");
        init();
        hitReviewApi();
    }

    private void hitReviewApi() {


        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ReviewsResponseModel> call = apiInterface.Reviews(user_id);
            call.enqueue(new Callback<ReviewsResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<ReviewsResponseModel> call, @NonNull Response<ReviewsResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        ReviewsResponseModel model = response.body();
                        if (model.isError() == false) {
                            if (model.getResponse() != null) {
                                if (model.getResponse().size() > 0) {

                                    reviewList = new ArrayList<ReviewsResponseModel>();
                                    mAdapter = new YourReviewAdapter(context, model.getResponse());
                                    recyclerList.setAdapter(mAdapter);
                                    mAdapter.notifyDataSetChanged();

                                } else {
                                    recyclerList.setVisibility(View.GONE);
                                    Common.displayLongToast(context, getResources().getString(R.string.data_nota));
                                }

                            }

                        }


                    } catch (Exception e)

                    {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ReviewsResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else

        {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }


    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);

        img_tick.setVisibility(View.GONE);
        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(R.string.user_reviews);
        img_back.setOnClickListener(this);
        //************end titel*****************//
        no_of_reviews = (TextView) findViewById(R.id.no_of_reviews);

        recyclerList = (RecyclerView) findViewById(R.id.recy_reviews);
        // mAdapter = new YourReviewAdapter(movieList);
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerList.setLayoutManager(mLayoutManager);


    }



    @Override
    public void onClick(View v) {
        onBackPressed();
    }


}

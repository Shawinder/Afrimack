package com.afrimack.app.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.AlertAddNewAdapter;
import com.afrimack.app.adapters.AlertSearchAdapter;
import com.afrimack.app.appinpurchase.IabHelper;
import com.afrimack.app.appinpurchase.IabResult;
import com.afrimack.app.appinpurchase.Inventory;
import com.afrimack.app.appinpurchase.Purchase;
import com.afrimack.app.appinpurchase.payment.Constants;
import com.afrimack.app.appinpurchase.payment.IabActivity;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.AlertSearchResponseModel;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.SearchSaveResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.afrimack.app.appinpurchase.payment.Constants.ADDITIONAL_ONE;
import static com.afrimack.app.appinpurchase.payment.Constants.ADDITIONAL_THREE;
import static com.afrimack.app.appinpurchase.payment.Constants.SKU_GAS;
import static com.afrimack.app.appinpurchase.payment.Constants.SKU_INFINITE_GAS;
import static com.afrimack.app.appinpurchase.payment.Constants.SKU_PREMIUM;
import static com.afrimack.app.utils.Common.displayErrorDialog;
import static com.afrimack.app.utils.Common.displayLoadingDialog;

public class ActivitySearch extends IabActivity implements View.OnClickListener {

    private Context context = this;
    private ImageView img_back, img_colse, img_search, img_add_search;
    private EditText et_search;

    private TextView tv_search_text_show, tv_dalog_text;
    private Dialog dialog;
    private EditText tv_search_dalog;
    private Button cancel_search, create_search;
    private RecyclerView rec_search_list, rec_add_comment;
    private LinearLayout lay_search_show;

    private AlertAddNewAdapter alertAddNewAdapter;

    private AlertSearchAdapter alertSeachAdapter;
    private String user_id, term, type;//  type[S: for search term ,  A : for search alert]
    private String seach, alret;
    RecyclerView.LayoutManager mLayoutManager, mLayoutManageralert;
    View.OnClickListener onClickListener;
    int pos;
    private Button btn_go;
    private Button btn_upgrade;
    private Dialog dialog_pament, dialog_search_info;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private LinearLayout ll_search_best, search_three;
    /*payment*/
    static final String TAG = Constants.LOG_IAB;

    // Helper object for in-app billing.
    IabHelper mHelper = null;
    private LinearLayout ll_search_alert;
    private String package_id = "";
    private int SEARCH_TYPE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        user_id = Config.getUserId(this);
        init();

        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mLayoutManageralert = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rec_search_list.setLayoutManager(mLayoutManager);
        rec_add_comment.setLayoutManager(mLayoutManageralert);

        hitApi();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        // Start setup of in-app billing. Note that this work is now done in the IabActivity, which
        // is the superclass of this method.
        setupIabHelper(true, true);

        // Set a variable for convenient access to the iab helper object.
        mHelper = getIabHelper();

        loadData();

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);
    }

    private void hitApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<AlertSearchResponseModel> call = apiInterface.AlertSearch(user_id);
            call.enqueue(new Callback<AlertSearchResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<AlertSearchResponseModel> call, @NonNull Response<AlertSearchResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        AlertSearchResponseModel mode = response.body();
                        if (mode.getCode() == 0) {
                            if (mode.isError() == false) {

                                if (mode.getResponse().getAlert().size() > 0) {
                                    alertAddNewAdapter = new AlertAddNewAdapter(context, mode.getResponse().getAlert(), onClickListener);
                                    rec_add_comment.setAdapter(alertAddNewAdapter);
                                    alertAddNewAdapter.notifyDataSetChanged();
                                }

                                if (mode.getResponse().getSearch().size() > 0) {
                                    alertSeachAdapter = new AlertSearchAdapter(ActivitySearch.this, mode.getResponse().getSearch(), "YES", onClickListener);
                                    rec_search_list.setAdapter(alertSeachAdapter);
                                    alertSeachAdapter.notifyDataSetChanged();
                                }


                            }

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<AlertSearchResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void hitSaveSearchApi(String searchType, final String from) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SearchSaveResponseModel> call = apiInterface.SaveSearch(user_id, term, searchType);
            call.enqueue(new Callback<SearchSaveResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<SearchSaveResponseModel> call, @NonNull Response<SearchSaveResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        SearchSaveResponseModel models = response.body();
                        //  Common.displayLongToast(this, models.getMessage());
                        if (models.isError() == false) {
                            if (from.equalsIgnoreCase("add")) {
                                hitApi();
                            } else {
                                Config.setSearch(term, context);
                                Intent intent = new Intent(context, ActivityHome.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }

//                            alertSeachAdapter = new AlertSearchAdapter(ActivitySearch.this, models.getResponse().getSearch(), "YES", onClickListener);
//                            rec_search_list.setAdapter(alertSeachAdapter);
//                            alertSeachAdapter.notifyDataSetChanged();

                        } else {
                            if (from.equalsIgnoreCase("add")) {
                                Common.displayLongToast(context, models.getMessage());
                            } else {
                                Config.setSearch(term, context);
                                Intent intent = new Intent(context, ActivityHome.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }

//

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SearchSaveResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void hitDeleteSearchApi(String term, String searchType, final int pos) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SearchSaveResponseModel> call = apiInterface.DeleteSearch(user_id, term, searchType);
            call.enqueue(new Callback<SearchSaveResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<SearchSaveResponseModel> call, @NonNull Response<SearchSaveResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        SearchSaveResponseModel modeld = response.body();
                        //Common.displayLongToast(this, modeld.getMessage());
                        if (modeld.getMessage().equalsIgnoreCase("Removed from the list")) {
                            AlertAddNewAdapter.alertList.remove(pos);
                            alertAddNewAdapter.notifyDataSetChanged();

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SearchSaveResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void hitPaymentSearchApi(String package_id) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.AlertSearchPayment(user_id, package_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel response1 = response.body();
                        if (response1.isError() == false) {
                            Common.displayLongToast(context, response1.getMessage());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }


    private void init() {

        img_back = (ImageView) findViewById(R.id.img_back_search);
        img_colse = (ImageView) findViewById(R.id.img_close_search);
        img_search = (ImageView) findViewById(R.id.img_seach_seach);
        img_add_search = (ImageView) findViewById(R.id.img_add_search);

        et_search = (EditText) findViewById(R.id.et_search);
        tv_search_text_show = (TextView) findViewById(R.id.tv_search_text_show);
        tv_dalog_text = (TextView) findViewById(R.id.tv_dalog_text);
        lay_search_show = (LinearLayout) findViewById(R.id.lay_search_show);

        rec_search_list = (RecyclerView) findViewById(R.id.rec_search_list);
        rec_add_comment = (RecyclerView) findViewById(R.id.rec_add_comment);
        onClickListener = this;

        img_add_search.setOnClickListener(this);
        img_back.setOnClickListener(this);
        btn_upgrade = (Button) findViewById(R.id.btn_upgrade);
        btn_upgrade.setOnClickListener(this);
        tv_dalog_text.setOnClickListener(this);
        tv_search_text_show.setOnClickListener(this);
        ll_search_alert = (LinearLayout) findViewById(R.id.ll_search_alert);
        ll_search_alert.setOnClickListener(this);
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.e("on", s + " ");
                if (s.length() > 0) {
                    lay_search_show.setVisibility(View.VISIBLE);
                    img_search.setVisibility(View.GONE);
                    // img_colse.setVisibility(View.VISIBLE);
                    seach = String.valueOf(s);
                    tv_search_text_show.setText(getResources().getString(R.string.invate_comm) + s + getResources().getString(R.string.invate_comm));
                } else {
                    lay_search_show.setVisibility(View.GONE);
                    img_search.setVisibility(View.VISIBLE);
                    //img_colse.setVisibility(View.GONE);
                    tv_search_text_show.setText("");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                            term = seach;
                            type = "S";
                            hitSaveSearchApi(type, "a");

                            return true;

                    }
                }
                return false;
            }
        });
        btn_go = (Button) findViewById(R.id.btn_go);
        btn_go.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == img_back) {
            onBackPressed();
        }
        if (v == img_add_search) {
            v.startAnimation(AppConstants.buttonClick);
            if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
                dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dalog_new_seach);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                tv_search_dalog = (EditText) dialog.findViewById(R.id.tv_search_dalog);
                cancel_search = (Button) dialog.findViewById(R.id.cancel_search);
                create_search = (Button) dialog.findViewById(R.id.create_search);
                cancel_search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                create_search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (tv_search_dalog.getText().length() > 0) {
                            dialog.dismiss();
                            term = tv_search_dalog.getText().toString().trim();
                            type = "A";
                            hitSaveSearchApi(type, "add");
                        } else {
                            Common.displayLongToast(ActivitySearch.this, getResources().getString(R.string.enter_search));
                        }
                    }
                });
                dialog.show();
            } else {
                Intent intent = new Intent(context, SplashLogin.class);
                startActivity(intent);
            }

        }
        if (v == ll_search_alert) {
            v.startAnimation(AppConstants.buttonClick);
            dialog_search_info = new Dialog(ActivitySearch.this);
            dialog_search_info.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_search_info.setContentView(R.layout.dialog_search_info);
            dialog_search_info.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog_search_info.setCancelable(false);

            TextView dailog_ok = (TextView) dialog_search_info.findViewById(R.id.dailog_ok);


            dailog_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_search_info.dismiss();
                }
            });
            dialog_search_info.show();
        }
        if (v == btn_upgrade) {
            v.startAnimation(AppConstants.buttonClick);
            dialog_pament = new Dialog(this);
            dialog_pament.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog_pament.setContentView(R.layout.search_dialog);
            dialog_pament.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            ll_search_best = (LinearLayout) dialog_pament.findViewById(R.id.ll_search_best);
            search_three = (LinearLayout) dialog_pament.findViewById(R.id.search_three);
            ll_search_best.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_pament.dismiss();

//                    CommonUtils.getInstance().showAlertMessage(context, "Under Developement");
                    if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
                        Log.d(TAG, "Buy Basic Hot");
                        if (mSubscribedToInfiniteGas) {
                            complain("No need! You're subscribed to infinite gas. Isn't that awesome?");
                            return;
                        }
                        Log.d(TAG, "Launching Basic Hot.");
                        launchInAppPurchaseFlow(ActivitySearch.this, ADDITIONAL_ONE);
                        String payload = "";
                    } else {
                        Intent intent = new Intent(context, SplashLogin.class);
                        startActivity(intent);
                    }


                }
            });
            search_three.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(AppConstants.buttonClick);
                    dialog_pament.dismiss();
//                    CommonUtils.getInstance().showAlertMessage(context, "Under Developement");
                    if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
                        Log.d(TAG, "Buy Basic Hot");

                        if (mSubscribedToInfiniteGas) {
                            complain("No need! You're subscribed to infinite gas. Isn't that awesome?");
                            return;
                        }

                        Log.d(TAG, "Launching 3 search.");

                        launchInAppPurchaseFlow(ActivitySearch.this, ADDITIONAL_THREE);
                        String payload = "";
                    } else {
                        Intent intent = new Intent(context, SplashLogin.class);
                        startActivity(intent);
                    }


                }
            });
            dialog_pament.show();
        }
        if (v == img_colse) {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
            et_search.setText("");
            //et_search.setHint("");
            img_search.setVisibility(View.VISIBLE);
        }
        if (v == tv_dalog_text) {


            if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
                dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dalog_new_seach);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                tv_search_dalog = (EditText) dialog.findViewById(R.id.tv_search_dalog);
                cancel_search = (Button) dialog.findViewById(R.id.cancel_search);
                create_search = (Button) dialog.findViewById(R.id.create_search);
                cancel_search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.startAnimation(AppConstants.buttonClick);
                        dialog.dismiss();
                    }
                });

                create_search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.startAnimation(AppConstants.buttonClick);
                        if (tv_search_dalog.getText().length() > 0) {
                            dialog.dismiss();
                            term = tv_search_dalog.getText().toString().trim();
                            type = "A";
                            hitSaveSearchApi(type, "a");
                        } else {
                            Common.displayLongToast(ActivitySearch.this, getResources().getString(R.string.enter_search));
                        }
                    }
                });
                dialog.show();
            } else {
                Intent intent = new Intent(context, SplashLogin.class);
                startActivity(intent);
            }


        }
        if (v == tv_search_text_show) {
            v.startAnimation(AppConstants.buttonClick);
            type = "S";
            if (ConnectionDetector.isNetAvail(this)) {
                term = seach;
                hitSaveSearchApi(type, "a");


            } else {
                displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
            }
        }
        if (v.getId() == R.id.tv_alret_titel) {
            v.startAnimation(AppConstants.buttonClick);
            String getsearch = (String) v.getTag();
            Config.setSearch(getsearch, this);
            Intent intent = new Intent(this, ActivityHome.class);
            startActivity(intent);
            finish();
        }
        if (v.getId() == R.id.img_alrt_colose) {
            v.startAnimation(AppConstants.buttonClick);
            pos = (int) v.getTag();
            type = "A";
            hitDeleteSearchApi(AlertAddNewAdapter.alertList.get(pos).getTerm(), type, pos);


        }
        if (v.getId() == R.id.tv_list_tetel) {
            v.startAnimation(AppConstants.buttonClick);
            String getsearch = (String) v.getTag();
            Config.setSearch(getsearch, this);
            Intent intent = new Intent(this, ActivityHome.class);
            startActivity(intent);
            finish();
        }
        if (v == btn_go) {
            v.startAnimation(AppConstants.buttonClick);
            if (et_search.getText().length() > 0) {
                String getsearch = et_search.getText().toString().trim();
                type = "S";
                if (ConnectionDetector.isNetAvail(this)) {
                    term = seach;
                    hitSaveSearchApi(type, "a");
                } else {
                    displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, this);
                }

            }

        }

    }


    @Override
    protected void onIabPurchaseFailed(IabHelper h, int errorNum) {
        // We did set up in such a way so that error messages have already been display (with complain method).
        // So all we have to do is remove the "waiting" indicator.
        if (errorNum != 0) ;


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    // (wgl, May 2015)
    // The next two variables are the original listener objects.
    // They are no longer used. See superclass IabActivity for the current ones.

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListenerOLD = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
               /* updateUi();
                setWaitScreen(false);*/
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                /*updateUi();
                setWaitScreen(false);*/
                return;
            }

            Log.d(TAG, "Purchase successful.");

            if (purchase.getSku().equals(SKU_GAS)) {
                // bought 1/4 tank of gas. So consume it.
                Log.d(TAG, "Purchase is gas. Starting gas consumption.");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else if (purchase.getSku().equals(SKU_PREMIUM)) {
                // bought the premium upgrade!
                Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
                alert("Thank you for upgrading to premium!");
                mIsPremium = true;
                /*updateUi();
                setWaitScreen(false);*/
            } else if (purchase.getSku().equals(SKU_INFINITE_GAS)) {
                // bought the infinite gas subscription
                Log.d(TAG, "Infinite gas subscription purchased.");
                alert("Thank you for subscribing to infinite gas!");
                mSubscribedToInfiniteGas = true;
                //mTank = TANK_MAX;
                /*updateUi();
                setWaitScreen(false);*/
            } else if (purchase.getSku().equals(ADDITIONAL_ONE)) {
                // bought the premium upgrade!
                Log.d(TAG, "Purchase is premium upgrade. ADDITIONAL_ONE.");
                alert("Thank you for upgrading to premium!");
                // mIsPremium = true;
                package_id = "1";
//                cellseachpurchas(package_id);
                hitPaymentSearchApi(package_id);

            } else if (purchase.getSku().equals(ADDITIONAL_THREE)) {
                // bought the premium upgrade!
                Log.d(TAG, "Purchase is premium upgrade. ADDITIONAL_THREE.");
                alert("Thank you for upgrading to premium!");
                // mIsPremium = true;
                package_id = "3";
                hitPaymentSearchApi(package_id);
//                cellseachpurchas(package_id);
            }
        }
    };


    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListenerOLD = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");
                //mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                saveData();
                // alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
            } else {
                complain("Error while consuming: " + result);
            }
          /*  updateUi();
            setWaitScreen(false);*/
            Log.d(TAG, "End consumption flow.");
        }
    };

// Methods

    // Drive button clicked. Burn gas!
    public void onDriveButtonClicked(View arg0) {
        Log.d(TAG, "Drive button clicked.");
       /* if (!mSubscribedToInfiniteGas && mTank <= 0) alert("Oh, no! You are out of gas! Try buying some!");
        else {
            if (!mSubscribedToInfiniteGas) --mTank;
            saveData();
            alert("Vroooom, you drove a few miles.");
           // updateUi();
            Log.d(TAG, "Vrooom. Tank is now " + mTank);
        }*/
        // updateUi ();
    }

    // We're being destroyed. It's important to dispose of the helper here!
    @Override
    public void onDestroy() {
        super.onDestroy();

        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }


    void saveData() {

        /*
         * WARNING: on a real application, we recommend you save data in a secure way to
         * prevent tampering. For simplicity in this sample, we simply store the data using a
         * SharedPreferences.
         */

        SharedPreferences.Editor spe = getPreferences(MODE_PRIVATE).edit();
        //spe.putInt("tank", mTank);
        spe.commit();
        // Log.d(TAG, "Saved data: tank = " + String.valueOf(mTank));
    }

    void loadData() {
        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        // mTank = sp.getInt("tank", 2);
        ///Log.d(TAG, "Loaded data: tank = " + String.valueOf(mTank));
    }

/**
 */
// Methods for IabHelperListener.
// Subclasses should call the superclass method if they override any of these methods.

    /**
     * Called when consumption of a purchase item fails.
     * <p>
     * <p> If this class was set up to issue messages upon failure, there is probably
     * nothing else to be done.
     */

    public void onIabConsumeItemFailed(IabHelper h) {
        super.onIabConsumeItemFailed(h);

        // Do whatever you need to in the ui to indicate that consuming a purchase failed.
        /*updateUi();
        setWaitScreen(false);*/

    }

    /**
     * Called when consumption of a purchase item succeeds.
     * <p>
     * SKU_GAS is the only consumable ite,. When it is purchased, this method gets called.
     * So this is the place where the tank is filled.
     *
     * @param h        IabHelper - helper object
     * @param purchase Purchase
     * @param result   IabResult
     */

    public void onIabConsumeItemSucceeded(IabHelper h, Purchase purchase, IabResult result) {
        super.onIabConsumeItemSucceeded(h, purchase, result);

        // Update the state of the app and the ui to show the item we purchased and consumed.
        String purchaseSku = purchase.getSku();
        if (purchaseSku.equals(SKU_GAS)) {
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");
                // mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                saveData();
                // alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
            } else {
                complain("Error while consuming regular gas: " + result);
            }

        }

        // Original code did some processing here. I moved this to IabActivity. (wgl, May 2015)
    /*
    else if (purchase.getSku().equals(SKU_PREMIUM)) {
      // bought the premium upgrade!
      Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
      alert("Thank you for upgrading to premium!");
      mIsPremium = true;
      updateUi();
      setWaitScreen(false);
    } else if (purchase.getSku().equals(SKU_INFINITE_GAS)) {

      // bought the infinite gas subscription
      Log.d(TAG, "Infinite gas subscription purchased.");
      alert("Thank you for subscribing to infinite gas!");
      mSubscribedToInfiniteGas = true;
      mTank = TANK_MAX;
      updateUi();
      setWaitScreen(false);
    }
    */

      /*  updateUi();
        setWaitScreen(false);*/

    }

    /**
     * Called when setup fails and the inventory of items is not available.
     * <p>
     * <p> If this class was set up to issue messages upon failure, there is probably
     * nothing else to be done.
     */

    public void onIabSetupFailed(IabHelper h) {
        super.onIabSetupFailed(h);

        // This would be where to change the ui in the event of a set up error.
    }

    /**
     * Called when setup succeeds and the inventory of items is available.
     *
     * @param h         IabHelper - helper object
     * @param result    IabResult
     * @param inventory Inventory
     */

    public void onIabSetupSucceeded(IabHelper h, IabResult result, Inventory inventory) {
        super.onIabSetupSucceeded(h, result, inventory);

        // The superclass setup method checks to see what has been purchased and what has been subscribed to.
        // Premium and infinite gas are handled here. If there was a regular gas purchase, steps to consume
        // it (via an async call to consume) were started in the superclass.

        //if (mSubscribedToInfiniteGas) mTank = TANK_MAX;

        if (mIsPremium) {
            // FIX THIS
        }


    }

}

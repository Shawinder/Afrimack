package com.afrimack.app.activities;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.AfrimackApp;
import com.afrimack.app.R;
import com.afrimack.app.adapters.DetailOfferAdapter;
import com.afrimack.app.adapters.DetailQuestionAdapter;
import com.afrimack.app.adapters.SubQuestionAdapter;
import com.afrimack.app.appbeans.BlockUserBean;
import com.afrimack.app.appinpurchase.PaymentActivity;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.fragments.DiscoverHome;
import com.afrimack.app.fragments.FragProfile;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.PostDetailResponseModel;
import com.afrimack.app.models.PremiumResponseModel;
import com.afrimack.app.models.SubQuation;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.maps.model.BitmapDescriptorFactory.defaultMarker;

public class ActivityDiscoverDetail extends AppCompatActivity implements OnMapReadyCallback,
        View.OnClickListener, DetailQuestionAdapter.SubQutiionMessage,
        SubQuestionAdapter.SubQutiionDelete, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public String postuserName;
    double dLatitude, dLongitude;
    View.OnClickListener onClickListener;
    String USERFOLLOW, heart_fill, start_fill;
    private LinearLayout lay_main;
    private ImageView img_back;
    private TextView tv_titel;
    private FrameLayout frame_container;
    private MapView map;
    private GoogleMap mMap;
    // private GPSTracker gpsTracker;
    private String lata, longa;
    private double lat, lng;
    private LinearLayout lay_chat, lay_ask_qution;
    private Dialog dialog, dialog_block;
    private TextView tv_post_user_name;
    private EditText et_qution;
    private Button cancel_qution, create_qution;
    private ImageView post_image, post_user_image;
    private TextView post_user_name, post_titel_datel, post_repus, post_info, post_update;
    private TextView tv_deal_done,tv_duration;
    private RatingBar user_rate;
    private TextView total_offer, post_questopn;
    private RecyclerView recy_offer, recy_qestion;

    private String user_id, post_id, follower_id;
    private LinearLayout liy_dis_blow;
    private TextView tv_follow, total_rate;
    private List<PostDetailResponseModel.ResponseBean.OffersBean> offerListl;
    private List<PostDetailResponseModel.ResponseBean.QuestionsBean> questionListl;
    private DetailOfferAdapter detailOfferAdapter;
    private DetailQuestionAdapter detailQuestionAdapter;
    //question list
    private ImageView qution_user_image;
    private EditText et_qution_list;
    private ImageView img_send_qution;
    private LinearLayout lay_question, lay_swop;
    private TextView tv_like_dis, swop_titel;
    private ImageView img_heart, img_star;
    private Marker marker;
    private TextView block_titel;
    private Button block_type;
    private Button block_cancel, delet_message;
    private ImageView ivFacebook, ivTwitter, ivWhatsapp, ivSMS, ivMail, ivShare;
    private String post_image_url;
    private int pos;
    private TextView tv_report_as;
    private String chanoti, dealTepe;
    private String messuesr, qution_id;
    private LinearLayout lay_premium, lay_profile;
    private TextView tv_hot, tv_top, tv_hot_top;
    private String logininfo = "no";
    private ImageView img_share;
    String thumbImage = null;
    PremiumResponseModel premiumResponseModel;
    Bitmap bitmapurl;
    private TextView tv_left_taxt;
    private SubQuation submessage;
    private LinearLayoutManager mLayoutManager;
    private LinearLayoutManager questionLayoutManager;
    private TextView tv_edit;
    private String post_type, share_url;
    private Context context = this;
    public static String TAG = ActivityDiscoverDetail.class.getSimpleName();
    private Fragment frg;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_detail);
        user_id = Config.getUserId(this);
        post_id = Config.getPostId(this);
        init();
        onClickListener = this;


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                //  post_id = null;
            } else {
                String post_id_swaop = extras.getString("post_id");
                if (post_id_swaop != null) {
                    post_id = extras.getString("post_id");
                }
                chanoti = extras.getString("notification");

            }
        } else {
        }
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recy_offer.setLayoutManager(mLayoutManager);
        questionLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recy_qestion.setLayoutManager(questionLayoutManager);


        //*******************************************map******************************************
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);


    }


    private void hitApiData() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<PostDetailResponseModel> call = apiInterface.PostDetails(user_id, post_id);
            call.enqueue(new Callback<PostDetailResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<PostDetailResponseModel> call, @NonNull Response<PostDetailResponseModel> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        PostDetailResponseModel postDetail = response.body();
                        if (postDetail.isError() == false) {

                            lay_main.setVisibility(View.VISIBLE);
                            if (postDetail.getResponse().getPost_status().equalsIgnoreCase("C")) {
                                tv_deal_done.setVisibility(View.VISIBLE);

                            } else {
                                tv_deal_done.setVisibility(View.GONE);

                            }
                            post_type = postDetail.getResponse().getPost_type();
                            share_url = postDetail.getResponse().getShare_url();
                            follower_id = String.valueOf(postDetail.getResponse().getUser_id());
                            if (follower_id.equalsIgnoreCase(user_id)) {
                                img_star.setVisibility(View.GONE);
                            }

                            if(post_type.equalsIgnoreCase("R")){
                                tv_duration.setVisibility(View.VISIBLE);
                              //  tv_deal_done.setVisibility(View.VISIBLE);
                            }
                            else {
                                tv_duration.setVisibility(View.GONE);
                              //  tv_deal_done.setVisibility(View.GONE);
                            }

                            if (postDetail.getResponse().getMy_product() == 1) {
                                liy_dis_blow.setVisibility(View.GONE);
                                if (postDetail.getResponse().getPost_status().equalsIgnoreCase("P")) {
                                    lay_premium.setVisibility(View.VISIBLE);
                                }

                                lay_profile.setVisibility(View.GONE);
                                tv_edit.setVisibility(View.VISIBLE);
                            } else {
                                liy_dis_blow.setVisibility(View.VISIBLE);
                                lay_premium.setVisibility(View.GONE);
                                lay_profile.setVisibility(View.VISIBLE);
                                tv_edit.setVisibility(View.GONE);
                            }
                            if (postDetail.getResponse().getPost_status().equalsIgnoreCase("C")) {
                                liy_dis_blow.setVisibility(View.GONE);
                                tv_edit.setVisibility(View.GONE);
                            }

                            tv_like_dis.setText(String.valueOf(postDetail.getResponse().getFav_count()));
                            if (postDetail.getResponse().getFollow_user().equalsIgnoreCase("Y")) {
                                tv_follow.setText("- UnFollow");
                                USERFOLLOW = "0";
                            } else {
                                tv_follow.setText("+ Follow");
                                USERFOLLOW = "1";
                            }
                            if (postDetail.getResponse().getFavorite_product().equalsIgnoreCase("Y")) {
                                img_heart.setBackground(getResources().getDrawable(R.drawable.like_fill));
                                heart_fill = "0";
                            } else {
                                img_heart.setBackground(getResources().getDrawable(R.drawable.like));
                                heart_fill = "1";
                            }
                            try {
                                if (postDetail.getResponse().getFollow_product().equalsIgnoreCase("Y")) {
                                    img_star.setImageResource(R.drawable.star_profile_fill);
                                    start_fill = "0";
                                } else {
                                    img_star.setImageResource(R.drawable.star_profile);
                                    start_fill = "1";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            dealTepe = postDetail.getResponse().getSwap_deal();
                            if (postDetail.getResponse().getSwap_deal().equalsIgnoreCase("Y")) {
                                lay_swop.setVisibility(View.VISIBLE);
                                swop_titel.setText(postDetail.getResponse().getSwap_deal_title());
                            } else {
                                lay_swop.setVisibility(View.GONE);
                            }

                            post_image_url = postDetail.getResponse().getPost_image();
                            Picasso.with(context)
                                    .load(postDetail.getResponse().getPost_image())
                        //            .placeholder(R.drawable.dami_background)
                                    .error(R.drawable.dami_background)
                                    .into(post_image);

                            Picasso
                                    .with(context)
                                    .load(postDetail.getResponse().getUser_image())
                                    .into(post_user_image);


                            post_user_name.setText(postDetail.getResponse().getUser_name());
                            postuserName = postDetail.getResponse().getUser_name();
                            user_rate.setRating(Float.parseFloat(String.valueOf(postDetail.getResponse().getRating())));
                            total_rate.setText(String.valueOf(postDetail.getResponse().getRating_count()));
                            post_titel_datel.setText(postDetail.getResponse().getTitle());

                            try {
                                String sub = postDetail.getResponse().getPrice();
                                String s = sub.substring(0, sub.indexOf("."));
                                if (sub.contains(".00")) {
                                    post_repus.setText(s);
                                } else {
                                    post_repus.setText(sub);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                String duration = postDetail.getResponse().getDuration_value();
                                String duration_type = postDetail.getResponse().getDuration_type();
                                String total = duration+" "+duration_type;
                                tv_duration.setText(total);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            post_info.setText(postDetail.getResponse().getDescription());
                            post_update.setText(getResources().getString(R.string.post_update) + " " + postDetail.getResponse().getLast_update());
                            dLatitude = Double.parseDouble(postDetail.getResponse().getLatitude());
                            dLongitude = Double.parseDouble(postDetail.getResponse().getLongitude());

                            if (marker != null) {
                                marker.remove();
                            }
                            LatLng sydney = new LatLng(dLatitude, dLongitude);
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(dLatitude, dLongitude))
                                    .title("Post Location")
                                    .icon(defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                            CameraUpdate center =
                                    CameraUpdateFactory.newLatLngZoom(sydney, 11);
//                            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

                            mMap.moveCamera(center);
//                            mMap.animateCamera(center);

                            if (!(Config.getUserId(context)).equalsIgnoreCase("null") && !(Config.getUserId(context)).equalsIgnoreCase("")) {
                                if (postDetail.getResponse().getOffers() != null && postDetail.getResponse().getOffers().size() > 0) {
                                    total_offer.setVisibility(View.VISIBLE);
                                    if (postDetail.getResponse().getOffers().size() == 1) {
                                        total_offer.setText(String.valueOf(postDetail.getResponse().getOffers().size()) + " " + "OFFER");
                                    } else {
                                        total_offer.setText(String.valueOf(postDetail.getResponse().getOffers().size()) + " " + getResources().getString(R.string.offer));
                                    }
                                    recy_offer.setVisibility(View.VISIBLE);


                                    offerListl = new ArrayList<PostDetailResponseModel.ResponseBean.OffersBean>();
                                    detailOfferAdapter = new DetailOfferAdapter(context, postDetail.getResponse().getOffers(), post_id, follower_id, dealTepe);
                                    recy_offer.setAdapter(detailOfferAdapter);
                                    detailOfferAdapter.notifyDataSetChanged();
                                }
                                if (postDetail.getResponse().getQuestions() != null && postDetail.getResponse().getQuestions().size() > 0) {

                                    post_questopn.setVisibility(View.VISIBLE);

                                    if (postDetail.getResponse().getMy_product() == 1) {
                                        lay_question.setVisibility(View.GONE);
                                    } else {
                                        lay_question.setVisibility(View.VISIBLE);
                                    }


                                    if (!postDetail.getResponse().getLogin_user_image().equalsIgnoreCase("")) {
                                        Picasso
                                                .with(context)
                                                .load(postDetail.getResponse().getLogin_user_image())
                                                .placeholder(R.drawable.user_round)
                                                .into(qution_user_image);
                                    }


                                    questionListl = new ArrayList<PostDetailResponseModel.ResponseBean.QuestionsBean>();

                                    for (int i = 0; i < postDetail.getResponse().getQuestions().size(); i++) {
                                        questionListl.add(postDetail.getResponse().getQuestions().get(i));
                                    }
                                    detailQuestionAdapter = new DetailQuestionAdapter(context, questionListl, onClickListener, ActivityDiscoverDetail.this, ActivityDiscoverDetail.this);
                                    recy_qestion.setAdapter(detailQuestionAdapter);


                                }
                            }

                        } else {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PostDetailResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }


    private void init() {

        lay_main = (LinearLayout) findViewById(R.id.discover_detail_main);
        img_back = (ImageView) findViewById(R.id.img_back_dis);
        lay_chat = (LinearLayout) findViewById(R.id.lay_chat);
        lay_ask_qution = (LinearLayout) findViewById(R.id.lay_ask_qution);

        img_back.setOnClickListener(this);
        lay_chat.setOnClickListener(this);
        lay_ask_qution.setOnClickListener(this);

        post_image = (ImageView) findViewById(R.id.post_image);
        post_user_image = (ImageView) findViewById(R.id.post_user_image);
        post_user_name = (TextView) findViewById(R.id.post_user_name);
        user_rate = (RatingBar) findViewById(R.id.user_rate);

        post_titel_datel = (TextView) findViewById(R.id.post_titel_datel);
        post_repus = (TextView) findViewById(R.id.post_repus);
        tv_deal_done = (TextView) findViewById(R.id.tv_deal_done);
        tv_duration = (TextView) findViewById(R.id.tv_duration);
        post_info = (TextView) findViewById(R.id.post_info);
        post_update = (TextView) findViewById(R.id.post_update);
        total_rate = (TextView) findViewById(R.id.total_rate);

        liy_dis_blow = (LinearLayout) findViewById(R.id.liy_dis_blow);
        total_offer = (TextView) findViewById(R.id.total_offer);
        tv_follow = (TextView) findViewById(R.id.tv_follow);

        recy_offer = (RecyclerView) findViewById(R.id.recy_offer);
        recy_qestion = (RecyclerView) findViewById(R.id.recy_qestion);
        post_questopn = (TextView) findViewById(R.id.post_questopn);

        qution_user_image = (ImageView) findViewById(R.id.qution_user_image);
        et_qution_list = (EditText) findViewById(R.id.et_qution_list);
        img_send_qution = (ImageView) findViewById(R.id.img_send_qution);
        lay_question = (LinearLayout) findViewById(R.id.lay_question);

        tv_like_dis = (TextView) findViewById(R.id.tv_like_dis);
        img_heart = (ImageView) findViewById(R.id.img_heart);
        img_star = (ImageView) findViewById(R.id.img_star);

        lay_swop = (LinearLayout) findViewById(R.id.lay_swop);
        swop_titel = (TextView) findViewById(R.id.swop_titel);
        tv_report_as = (TextView) findViewById(R.id.tv_report_as);

        tv_follow.setOnClickListener(this);
        img_send_qution.setOnClickListener(this);
        img_heart.setOnClickListener(this);
        img_star.setOnClickListener(this);
        post_image.setOnClickListener(this);
        tv_report_as.setOnClickListener(this);
        post_user_image.setOnClickListener(this);
        //share

        ivFacebook = (ImageView) findViewById(R.id.ivFacebook);
        ivTwitter = (ImageView) findViewById(R.id.ivTwitter);
        ivWhatsapp = (ImageView) findViewById(R.id.ivWhatsapp);
        ivSMS = (ImageView) findViewById(R.id.ivSMS);
        ivMail = (ImageView) findViewById(R.id.ivMail);
        ivShare = (ImageView) findViewById(R.id.ivShare);


        ivFacebook.setOnClickListener(this);
        ivTwitter.setOnClickListener(this);
        ivWhatsapp.setOnClickListener(this);
        ivSMS.setOnClickListener(this);
        ivMail.setOnClickListener(this);
        ivShare.setOnClickListener(this);

        lay_premium = (LinearLayout) findViewById(R.id.lay_premium);
        lay_profile = (LinearLayout) findViewById(R.id.lay_profile);
        tv_hot = (TextView) findViewById(R.id.tv_hot);
        tv_top = (TextView) findViewById(R.id.tv_top);
        tv_hot_top = (TextView) findViewById(R.id.tv_hot_top);
        tv_hot.setOnClickListener(this);
        tv_top.setOnClickListener(this);
        tv_hot_top.setOnClickListener(this);

        img_share = (ImageView) findViewById(R.id.img_share);
        img_share.setOnClickListener(this);

        tv_edit = (TextView) findViewById(R.id.tv_edit);
        tv_edit.setOnClickListener(this);
        frame_container = (FrameLayout) findViewById(R.id.container);

    }

    public void onRestart() {
        super.onRestart();


        if (logininfo.equalsIgnoreCase("yes")) {
            user_id = Config.getUserId(this);
            logininfo = "no";
            hitApiData();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        AfrimackApp.curentactivity = this;
        hitApiData();
    }

    @Override
    public void onBackPressed() {

        if (chanoti != null) {
            Intent intent = new Intent(this, ActMyAfrimack.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("from", "a");
            chanoti = null;
            startActivity(intent);
            finish();

        } else {
            super.onBackPressed();
        }
        AfrimackApp.curentactivity = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.mMap = googleMap;


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.location_permission_needed)
                        .setMessage(R.string.location_permission_accept_alert)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(ActivityDiscoverDetail.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onDestroy() {
        AfrimackApp.curentactivity = null;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        AfrimackApp.curentactivity = null;
        super.onPause();
    }

    @Override
    public void onClick(View v) {

        if (v == img_back) {
            ActivityDiscoverDetail.this.finish();
        }
        if (v == lay_chat) {
            v.startAnimation(AppConstants.buttonClick);

            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {

                Intent intent = new Intent(this, ActivityChat.class);
                Config.setFollow(USERFOLLOW, this);
                Config.setOfferUserID(user_id, this);
                intent.putExtra("from", "details");
                intent.putExtra("dealTepe", dealTepe);
                startActivity(intent);
                //  }
            } else {
                logininfo = "yes";
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }

        }
        if (v == post_user_image) {
            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                Intent intent = new Intent(this, OfferUserActivity.class);
                intent.putExtra("other_user_id", follower_id);
                Config.setFollow(USERFOLLOW, this);
                Config.setOfferUserID(follower_id, this);
                Config.setPostId(post_id, this);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }


        }
        if (v == tv_edit) {

            Config.setPostId(post_id, this);
            //Config.setPostType(SellBuyingAdapter.selllList.get(pos).getPost_type(),this);
            Config.setPostType(post_type, this);
            Intent sell = new Intent(this, SellOffer.class);
            startActivity(sell);

        }
        if (v == lay_ask_qution) {
            v.startAnimation(AppConstants.buttonClick);
            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                dialog = new Dialog(this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_ask_qution);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                tv_post_user_name = (TextView) dialog.findViewById(R.id.tv_post_user_name);
                tv_post_user_name.setText("Ask " + postuserName);
                tv_left_taxt = (TextView) dialog.findViewById(R.id.tv_left_taxt);
                et_qution = (EditText) dialog.findViewById(R.id.et_qution);
                cancel_qution = (Button) dialog.findViewById(R.id.cancel_qution);
                create_qution = (Button) dialog.findViewById(R.id.create_qution);
                cancel_qution.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                et_qution.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (s.length() >= 500) {
                            tv_left_taxt.setText("0 characters left");
//                            Common.displayLongToast(ActivityDiscoverDetail.this, getResources().getString(R.string.max_char_qution));
                        } else {
                            String textsi = String.valueOf(500 - s.length());
                            tv_left_taxt.setText(textsi + " characters left");
                        }
                    }
                });
                create_qution.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (et_qution.getText().length() > 0) {
                            dialog.dismiss();
                            String question = et_qution.getText().toString().trim();
                            hitQuestionApi(question, "");
                        } else {
                            Common.displayLongToast(ActivityDiscoverDetail.this, getResources().getString(R.string.enter_search));
                        }
                    }


                });
                dialog.show();
            } else {
                logininfo = "yes";
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }

        }
        if (v == tv_follow) {

            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                if (USERFOLLOW.equalsIgnoreCase("1")) {
                    hitFollowUserApi(user_id, follower_id);
                } else {
                    hitUnFollowUserApi(user_id, follower_id);
                }
            } else {
                logininfo = "yes";
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }

        }
        if (v == img_heart) {

            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                String action;
                if (heart_fill.equalsIgnoreCase("1")) {
                    img_heart.setBackground(getResources().getDrawable(R.drawable.like_fill));
                    heart_fill = "0";
                    action = "1";
                } else {
                    img_heart.setBackground(getResources().getDrawable(R.drawable.like));
                    heart_fill = "1";
                    action = "0";
                }

                hitHeartUnFollow(user_id, post_id, action);

            } else {
                logininfo = "yes";
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }

        }
        if (v == img_star) {
            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                String action = "";
                try {
                    if (start_fill.equalsIgnoreCase("1")) {
                        img_star.setImageResource(R.drawable.star_profile_fill);
                        start_fill = "0";
                        action = "1";
                    } else {
                        img_star.setImageResource(R.drawable.star_profile);
                        start_fill = "1";
                        action = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                hitPostFollowUnFollowApi(user_id, post_id, action);

            } else {
                logininfo = "yes";
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }

        }
        if (v == img_send_qution) {
            if (et_qution_list.getText().length() > 0) {
                String question = et_qution_list.getText().toString().trim();
                hitQuestionApi(question, "");
            } else {
                Common.displayLongToast(ActivityDiscoverDetail.this, getResources().getString(R.string.enter_question));
            }
        }

        if (v.getId() == R.id.question_profile) {
            pos = (int) v.getTag();
            Intent intent = new Intent(this, OfferUserActivity.class);
            intent.putExtra("other_user_id", DetailQuestionAdapter.quetionList.get(pos).getUser_id());
            Config.setFollow(USERFOLLOW, this);
            Config.setOfferUserID(DetailQuestionAdapter.quetionList.get(pos).getUser_id(), this);
            startActivity(intent);
        }

        if (v.getId() == R.id.question_profilee) {
            pos = (int) v.getTag();
            Intent intent = new Intent(this, OfferUserActivity.class);
            intent.putExtra("other_user_id", String.valueOf(DetailQuestionAdapter.quetionList.get(pos).getSubquestion().get(pos).getUser_id()));
            Config.setFollow(USERFOLLOW, this);
            Config.setOfferUserID(String.valueOf(DetailQuestionAdapter.quetionList.get(pos).getSubquestion().get(pos).getUser_id()), this);
            startActivity(intent);
        }


        if (v.getId() == R.id.block) {

            pos = (int) v.getTag();
            messuesr = DetailQuestionAdapter.quetionList.get(pos).getUser_id();
            qution_id = DetailQuestionAdapter.quetionList.get(pos).getQuestion_id();
            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                if (Config.getUserId(this).equalsIgnoreCase(messuesr)) {
                    alertDialogBuilder.setMessage("Are you sure you want to delete this question?");
                } else {
                    alertDialogBuilder.setMessage("Are you sure you want to block this user?");
                }


                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if (Config.getUserId(context).equalsIgnoreCase(messuesr)) {
                                    hitDeleteQuestionApi(messuesr, qution_id);
                                } else {
                                    hitBlockUserApi();
                                }
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            } else {
                logininfo = "yes";
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }
        }
        if (v.getId() == R.id.sub_block) {

            pos = (int) v.getTag();
            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                if (Config.getUserId(this).equalsIgnoreCase(messuesr)) {
                    alertDialogBuilder.setMessage("Are you sure you want to delete this question?");
                } else {
                    alertDialogBuilder.setMessage("Are you sure you want to block this user?");
                }


                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                if (Config.getUserId(context).equalsIgnoreCase(messuesr)) {
                                    hitDeleteQuestionApi(messuesr, qution_id);
                                } else {
                                    hitBlockUserApi();
                                }
                            }
                        });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            } else {
                logininfo = "yes";
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }


        }

        if (v.getId() == R.id.img_sub_send_qution) {
            // getSubMessage();
            SubQuation subQuation = (SubQuation) v.getTag();

            qution_id = DetailQuestionAdapter.quetionList.get(pos).getQuestion_id();
        }
        if (v == post_image) {
            Intent intent = new Intent(this, FullImageActivity.class);
            intent.putExtra("post_id", post_id);
            startActivity(intent);

        }
        if (v == tv_report_as) {
            if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActivityDiscoverDetail.this);

                // Setting Dialog Title
                //  alertDialog.setTitle("Confirm Delete...");

                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want to report this as inappropriate?");
                alertDialog.setCancelable(false);
                // Setting Icon to Dialog
                // alertDialog.setIcon(R.drawable.delete);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        hitReportPostApi();
                        // Write your code here to invoke YES event
                        // Toast.makeText(getApplicationContext(), "You clicked on YES", Toast.LENGTH_SHORT).show();
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            } else {
                logininfo = "yes";
                Intent intent = new Intent(this, SplashLogin.class);
                startActivity(intent);
            }
        }

        if (v == tv_hot) {
            premiunApi();
        }
        if (v == tv_top) {
            premiunApi();
        }
        if (v == tv_hot_top) {
            premiunApi();
        }
        if (v == ivFacebook) {

            new ShareonFacebook().execute(post_image_url);

        }
        if (v == ivTwitter) {

            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            PackageManager pm = this.getPackageManager();
            List<ResolveInfo> resInfos = pm.queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);

                    if (packageName.contains("com.twitter.android")) {
                        Intent intent = new Intent();

                        intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                        intent.putExtra("AppName", resInfo.loadLabel(pm).toString());
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, share_url);
                        intent.putExtra(Intent.EXTRA_STREAM, post_image_url);
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Afrimack");
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Select app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                    startActivity(chooserIntent);
                } else {
                    Toast.makeText(this, "No app to share.", Toast.LENGTH_LONG).show();
                }
            }
        }
        if (v == ivWhatsapp) {

            new ShareonWhatsApp().execute(post_image_url);

        }
        if (v == ivSMS) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setType("image/");
            intent.putExtra(Intent.EXTRA_TEXT, share_url);
            startActivity(Intent.createChooser(intent, "Share"));

        }
        if (v == ivMail) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.setType("image/");
            intent.putExtra(Intent.EXTRA_TEXT, share_url);
//            intent.putExtra(Intent.EXTRA_STREAM, post_image_url);
            startActivity(Intent.createChooser(intent, "Share"));

        }
        if (v == ivShare) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);

            // # change the type of data you need to share,
            // # for image use "image/*"

            intent.setType("text/plain");
            intent.setType("image/");
            intent.putExtra(Intent.EXTRA_TEXT, share_url);
//            intent.putExtra(Intent.EXTRA_STREAM, post_image_url);
            startActivity(Intent.createChooser(intent, "Share"));
        }
        if (v == img_share) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);

            intent.setType("text/plain");
            intent.setType("image/");
            intent.putExtra(Intent.EXTRA_TEXT, share_url);
//            intent.putExtra(Intent.EXTRA_STREAM, post_image_url);
            startActivity(Intent.createChooser(intent, "Share"));
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void hitReportPostApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.PostReport(user_id, post_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        if (commanResponseModel.isError() == false) {
                            Common.displayLongToast(context, commanResponseModel.getMessage());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitBlockUserApi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<BlockUserBean> call = apiInterface.BlockUser(user_id, messuesr);
            call.enqueue(new Callback<BlockUserBean>() {
                @Override
                public void onResponse(@NonNull Call<BlockUserBean> call, @NonNull Response<BlockUserBean> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();

                        BlockUserBean commanResponseModel = response.body();
                        if (commanResponseModel.getError() == false) {
                            Common.displayLongToast(context, commanResponseModel.getMessage());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BlockUserBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void hitPostFollowUnFollowApi(String user_id, String post_id, String action) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.PostFollowUnfollow(user_id, post_id, action);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            if (commanResponseModel.getMessage().equalsIgnoreCase("Successully follow the post")) {
                                img_star.setBackground(getResources().getDrawable(R.drawable.star_profile_fill));
                            } else {
                                img_star.setBackground(getResources().getDrawable(R.drawable.star_profile));
                            }

                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitHeartUnFollow(String user_id, String post_id, String action) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.HeartFollow(user_id, post_id, action);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            tv_like_dis.setText(String.valueOf(commanResponseModel.getResponse()));
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitUnFollowUserApi(String user_id, String follower_id) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.UserUnFollow(user_id, follower_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            tv_follow.setText("+ Follow");
                            USERFOLLOW = "1";
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitFollowUserApi(String user_id, String follower_id) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.UserFollow(user_id, follower_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            tv_follow.setText("- UnFollow");
                            USERFOLLOW = "0";
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitQuestionApi(String question, String questionId) {

        hideKeyboard();
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

            String question_val=StringEscapeUtils.escapeJava(question);

            Call<CommanResponseModel> call = apiInterface.Question(user_id, post_id, question_val, questionId);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        //Common.displayLongToast(this, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            et_qution_list.setText("");
                            hitApiData();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void premiunApi() {

        Intent intent = new Intent(this, PaymentActivity.class);
        startActivity(intent);
    }


    private void hideKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    public void chatnotification() {
        hitApiData();
    }

    @Override
    public void getSubMessage(SubQuation sub_message) {
        submessage = sub_message;
        String question = submessage.getSubqution();
        qution_id = submessage.getQution_id();
        if (question.length() > 0) {
            hitQuestionApi(question, qution_id);

        } else {
            Common.displayLongToast(ActivityDiscoverDetail.this, getResources().getString(R.string.enter_question));
        }
    }


    @Override
    public void getSubdelete(SubQuation sub_message) {
        submessage = sub_message;
        String question = submessage.getSubqution();
        qution_id = submessage.getQution_id();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setMessage("Are you sure you want to delete this answer? Not question at answers.?");


        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        hitDeleteQuestionApi(messuesr, qution_id);
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    private void hitDeleteQuestionApi(String messuesr, String qution_id) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.DeleteQuestion(messuesr, qution_id);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {
                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel commanResponseModel = response.body();
                        Common.displayLongToast(context, commanResponseModel.getMessage());
                        if (commanResponseModel.isError() == false) {
                            hitApiData();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    class ShareonFacebook extends AsyncTask<String, Void, String> {

//        private Exception exception;

        protected String doInBackground(String... urls) {
            Bitmap image = null;
            try {
                URL url = new URL(urls[0]);
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                Uri uri = getImageUri(context, image);
                return String.valueOf(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        protected void onPostExecute(String result) {
            // TODO: check this.exception
            // TODO: do something with the feed
            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
//            shareIntent.setType("image/");
            PackageManager pm = context.getPackageManager();
            List<ResolveInfo> resInfos = pm.queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);

                    if (packageName.contains("com.facebook.katana")
                            || packageName.contains("com.facebook.orca")) {
                        Intent intent = new Intent();

                        intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                        intent.putExtra("AppName", resInfo.loadLabel(pm).toString());
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
//                        intent.setType("image/");
                        intent.putExtra(Intent.EXTRA_TEXT, share_url);
//                        intent.putExtra(Intent.EXTRA_TEXT, post_image_url);
                        intent.putExtra(Intent.EXTRA_STREAM, result);
//                        shareIntent.setType("image/jpeg");
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Afrimack");
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Select app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                    startActivity(chooserIntent);
                } else {
                    Toast.makeText(ActivityDiscoverDetail.this, "No app to share.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    class ShareonWhatsApp extends AsyncTask<String, Void, String> {

//        private Exception exception;

        protected String doInBackground(String... urls) {
            Bitmap image = null;
            try {
                URL url = new URL(urls[0]);
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                Uri uri = getImageUri(context, image);
                return String.valueOf(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }

        protected void onPostExecute(String result) {
            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            PackageManager pm = context.getPackageManager();
            List<ResolveInfo> resInfos = pm.queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);

                    if (packageName.contains("com.whatsapp")) {
                        Intent intent = new Intent();

                        intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                        intent.putExtra("AppName", resInfo.loadLabel(pm).toString());
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");

                        intent.putExtra(Intent.EXTRA_TEXT, share_url);
                        intent.putExtra(Intent.EXTRA_STREAM, result);
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Afrimack");
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    Collections.sort(targetShareIntents, new Comparator<Intent>() {
                        @Override
                        public int compare(Intent o1, Intent o2) {
                            return o1.getStringExtra("AppName").compareTo(o2.getStringExtra("AppName"));
                        }
                    });
                    Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Select app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                    startActivity(chooserIntent);

                } else {
                    Toast.makeText(context, "No app to share.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

}

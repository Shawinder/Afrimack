package com.afrimack.app.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.appbeans.SettingBean;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.CommonUtils;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ActivitySettings extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;


    private CheckBox iv_ads_online, iv_new_question, iv_new_offers, iv_ads_following, iv_ads_fb_friend, iv_single_profile,
            iv_new_chat_message, iv_new_message;

    private CheckBox iv_ads_online_email, iv_new_question_email, iv_new_offers_email, iv_ads_following_email, iv_ads_fb_friend_email, iv_single_profile_email,
            iv_new_chat_message_email, iv_new_message_email;

    private String user_id = "";
    private Button btn_save;
    private String ads_online = "", new_question = "", new_offers = "", ads_following = "", ads_fb_friend = "", single_profile = "", new_chat_message = "", new_message = "";
    private String ads_online_email = "", new_question_email = "", new_offers_email = "", ads_following_email = "", ads_fb_friend_email = "", single_profile_email = "", new_chat_message_email = "", new_message_email = "";
    private Context context = this;
    private String TAG = ActivitySettings.class.getSimpleName();
    private String acceptOffer = "", confirm = "", remindConfirm = "";
    private String acceptOffer_email = "", confirm_email = "", remindConfirm_email = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        user_id = Config.getUserId(this);

       hitSettingApi();
      // hitSetting_emailApi();
        init();
    }

    private void hitSettingApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();

            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<SettingBean> call = apiInterface.GetSettings(user_id);
            call.enqueue(new Callback<SettingBean>() {
                @Override
                public void onResponse(@NonNull Call<SettingBean> call, @NonNull Response<SettingBean> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        SettingBean mBean = response.body();
                        if (mBean.getError() == false) {
                            if (mBean.getResponse().getPush().getAdsOnline().equalsIgnoreCase("1")) {
                                iv_ads_online.setChecked(true);
                                ads_online = "1";
                            } else {
                                iv_ads_online.setChecked(false);
                                ads_online = "0";
                            }
                            if (mBean.getResponse().getPush().getNewQuestion().equalsIgnoreCase("1")) {
                                iv_new_question.setChecked(true);
                                new_question = "1";
                            } else {
                                iv_new_question.setChecked(false);
                                new_question = "0";
                            }
                            if (mBean.getResponse().getPush().getNewOffer().equalsIgnoreCase("1")) {
                                iv_new_offers.setChecked(true);
                                new_offers = "1";
                            } else {
                                iv_new_offers.setChecked(false);
                                new_offers = "0";
                            }
                            if (mBean.getResponse().getPush().getNewAdsFromFollowing().equalsIgnoreCase("1")) {
                                iv_ads_following.setChecked(true);
                                ads_following = "1";
                            } else {
                                iv_ads_following.setChecked(false);
                                ads_following = "0";
                            }
                            if (mBean.getResponse().getPush().getNewAdsFromFacebookFriend().equalsIgnoreCase("1")) {
                                iv_ads_fb_friend.setChecked(true);
                                ads_fb_friend = "1";
                            } else {
                                iv_ads_fb_friend.setChecked(false);
                                ads_fb_friend = "0";
                            }
                            if (mBean.getResponse().getPush().getSingleProfileOnline().equalsIgnoreCase("1")) {
                                iv_single_profile.setChecked(true);
                                single_profile = "1";
                            } else {
                                iv_single_profile.setChecked(false);
                                single_profile = "0";
                            }

                            Config.setChatMessageStatus(Integer.valueOf(mBean.getResponse().getPush().getNewMessageChat()), context);
                            if (mBean.getResponse().getPush().getNewMessageChat().equalsIgnoreCase("1")) {
                                iv_new_chat_message.setChecked(true);
                                new_chat_message = "1";
                            } else {
                                iv_new_chat_message.setChecked(false);
                                new_chat_message = "0";
                            }
                            if (mBean.getResponse().getPush().getNewMessageAfrimack().equalsIgnoreCase("1")) {
                                iv_new_message.setChecked(true);
                                new_message = "1";
                            } else {
                                iv_new_message.setChecked(false);
                                new_message = "0";
                            }
                            acceptOffer = String.valueOf(mBean.getResponse().getPush().getAcceptOffer());
                            confirm = String.valueOf(mBean.getResponse().getPush().getConfirm());
                            remindConfirm = String.valueOf(mBean.getResponse().getPush().getRemindConfirm());


                            if (mBean.getResponse().getEmail().getAdsOnline().equalsIgnoreCase("1")) {
                                iv_ads_online_email.setChecked(true);
                                ads_online_email = "1";
                            } else {
                                iv_ads_online_email.setChecked(false);
                                ads_online_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewQuestion().equalsIgnoreCase("1")) {
                                iv_new_question_email.setChecked(true);
                                new_question_email = "1";
                            } else {
                                iv_new_question_email.setChecked(false);
                                new_question_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewOffer().equalsIgnoreCase("1")) {
                                iv_new_offers_email.setChecked(true);
                                new_offers_email = "1";
                            } else {
                                iv_new_offers_email.setChecked(false);
                                new_offers_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewAdsFromFollowing().equalsIgnoreCase("1")) {
                                iv_ads_following_email.setChecked(true);
                                ads_following_email= "1";
                            } else {
                                iv_ads_following_email.setChecked(false);
                                ads_following_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewAdsFromFacebookFriend().equalsIgnoreCase("1")) {
                                iv_ads_fb_friend_email.setChecked(true);
                                ads_fb_friend_email = "1";
                            } else {
                                iv_ads_fb_friend_email.setChecked(false);
                                ads_fb_friend_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getSingleProfileOnline().equalsIgnoreCase("1")) {
                                iv_single_profile_email.setChecked(true);
                                single_profile_email = "1";
                            } else {
                                iv_single_profile_email.setChecked(false);
                                single_profile_email = "0";
                            }

                            if (mBean.getResponse().getEmail().getNewMessageChat().equalsIgnoreCase("1")) {
                                iv_new_chat_message_email.setChecked(true);
                                new_chat_message_email = "1";
                            } else {
                                iv_new_chat_message_email.setChecked(false);
                                new_chat_message_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewMessageAfrimack().equalsIgnoreCase("1")) {
                                iv_new_message_email.setChecked(true);
                                new_message_email = "1";
                            } else {
                                iv_new_message_email.setChecked(false);
                                new_message_email = "0";
                            }
                            acceptOffer_email = String.valueOf(mBean.getResponse().getEmail().getAcceptOffer());
                            confirm_email = String.valueOf(mBean.getResponse().getEmail().getConfirm());
                            remindConfirm_email = String.valueOf(mBean.getResponse().getEmail().getRemindConfirm());
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SettingBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().showAlertMessage(context, "Something went wrong. Please try again later.");
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });

        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }
//

    private void hitSetting_emailApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();

            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<SettingBean> call = apiInterface.GetSettings(user_id);
            call.enqueue(new Callback<SettingBean>() {
                @Override
                public void onResponse(@NonNull Call<SettingBean> call, @NonNull Response<SettingBean> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        SettingBean mBean = response.body();
                        if (mBean.getError() == false) {
                            if (mBean.getResponse().getEmail().getAdsOnline().equalsIgnoreCase("1")) {
                                iv_ads_online_email.setChecked(true);
                                ads_online_email = "1";
                            } else {
                                iv_ads_online_email.setChecked(false);
                                ads_online_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewQuestion().equalsIgnoreCase("1")) {
                                iv_new_question_email.setChecked(true);
                                new_question_email = "1";
                            } else {
                                iv_new_question_email.setChecked(false);
                                new_question_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewOffer().equalsIgnoreCase("1")) {
                                iv_new_offers_email.setChecked(true);
                                new_offers_email = "1";
                            } else {
                                iv_new_offers_email.setChecked(false);
                                new_offers_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewAdsFromFollowing().equalsIgnoreCase("1")) {
                                iv_ads_following_email.setChecked(true);
                                ads_following_email= "1";
                            } else {
                                iv_ads_following_email.setChecked(false);
                                ads_following_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewAdsFromFacebookFriend().equalsIgnoreCase("1")) {
                                iv_ads_fb_friend_email.setChecked(true);
                                ads_fb_friend_email = "1";
                            } else {
                                iv_ads_fb_friend_email.setChecked(false);
                                ads_fb_friend_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getSingleProfileOnline().equalsIgnoreCase("1")) {
                                iv_single_profile_email.setChecked(true);
                                single_profile_email = "1";
                            } else {
                                iv_single_profile_email.setChecked(false);
                                single_profile_email = "0";
                            }

                            Config.setChatMessageStatus(Integer.valueOf(mBean.getResponse().getPush().getNewMessageChat()), context);
                            if (mBean.getResponse().getEmail().getNewMessageChat().equalsIgnoreCase("1")) {
                                iv_new_chat_message_email.setChecked(true);
                                new_chat_message_email = "1";
                            } else {
                                iv_new_chat_message_email.setChecked(false);
                                new_chat_message_email = "0";
                            }
                            if (mBean.getResponse().getEmail().getNewMessageAfrimack().equalsIgnoreCase("1")) {
                                iv_new_message_email.setChecked(true);
                                new_message_email = "1";
                            } else {
                                iv_new_message_email.setChecked(false);
                                new_message_email = "0";
                            }
                            acceptOffer_email = String.valueOf(mBean.getResponse().getEmail().getAcceptOffer());
                            confirm_email = String.valueOf(mBean.getResponse().getEmail().getConfirm());
                            remindConfirm_email = String.valueOf(mBean.getResponse().getEmail().getRemindConfirm());

                            Log.e("NEW_VAL_EMAIL","Success");
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SettingBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                  //  CommonUtils.getInstance().showAlertMessage(context, "Something went wrong. Please try again later.");
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });

        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);
        img_tick.setVisibility(View.GONE);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.Settings).toString());
        img_back.setOnClickListener(this);
        //************end titel*****************//

        iv_ads_online = (CheckBox) findViewById(R.id.iv_ads_online);
        iv_ads_online_email = (CheckBox) findViewById(R.id.iv_ads_online_email);
        iv_ads_online_email.setOnClickListener(this);
        iv_ads_online.setOnClickListener(this);
        iv_new_question_email = (CheckBox) findViewById(R.id.iv_new_question_email);
        iv_new_question = (CheckBox) findViewById(R.id.iv_new_question);
        iv_new_question_email.setOnClickListener(this);
        iv_new_question.setOnClickListener(this);
        iv_new_offers = (CheckBox) findViewById(R.id.iv_new_offers);
        iv_new_offers_email = (CheckBox) findViewById(R.id.iv_new_offers_email);
        iv_new_offers.setOnClickListener(this);
        iv_new_offers_email.setOnClickListener(this);
        iv_ads_following = (CheckBox) findViewById(R.id.iv_ads_following);
        iv_ads_following_email = (CheckBox) findViewById(R.id.iv_ads_following_email);
        iv_ads_following.setOnClickListener(this);
        iv_ads_following_email.setOnClickListener(this);
        iv_ads_fb_friend = (CheckBox) findViewById(R.id.iv_ads_fb_friend);
        iv_ads_fb_friend_email = (CheckBox) findViewById(R.id.iv_ads_fb_friend_email);
        iv_ads_fb_friend.setOnClickListener(this);
        iv_ads_fb_friend_email.setOnClickListener(this);
        iv_single_profile = (CheckBox) findViewById(R.id.iv_single_profile);
        iv_single_profile_email = (CheckBox) findViewById(R.id.iv_single_profile_email);
        iv_single_profile.setOnClickListener(this);
        iv_single_profile_email.setOnClickListener(this);
        iv_new_chat_message = (CheckBox) findViewById(R.id.iv_new_chat_message);
        iv_new_chat_message_email = (CheckBox) findViewById(R.id.iv_new_chat_message_email);
        iv_new_chat_message.setOnClickListener(this);
        iv_new_chat_message_email.setOnClickListener(this);
        iv_new_message = (CheckBox) findViewById(R.id.iv_new_message);
        iv_new_message_email = (CheckBox) findViewById(R.id.iv_new_message_email);
        iv_new_message.setOnClickListener(this);
        iv_new_message_email.setOnClickListener(this);


        img_back.setOnClickListener(this);

        btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

        //=============push notifications===========

            case R.id.iv_ads_online:
                if (!iv_ads_online.isChecked()) {
                    iv_ads_online.setChecked(false);
                    ads_online = "0";

                } else {
                    iv_ads_online.setChecked(true);
                    ads_online = "1";
                }
                break;

            case R.id.iv_new_question:
                if (!iv_new_question.isChecked()) {
                    iv_new_question.setChecked(false);
                    new_question = "0";

                } else {
                    iv_new_question.setChecked(true);
                    new_question = "1";
                }
                break;
            case R.id.iv_new_offers:
                if (!iv_new_offers.isChecked()) {
                    iv_new_offers.setChecked(false);
                    new_offers = "0";

                } else {
                    iv_new_offers.setChecked(true);
                    new_offers = "1";
                }
                break;

            case R.id.iv_ads_following:
                if (!iv_ads_following.isChecked()) {
                    iv_ads_following.setChecked(false);
                    ads_following = "0";

                } else {
                    iv_ads_following.setChecked(true);
                    ads_following = "1";
                }
                break;
            case R.id.iv_ads_fb_friend:
                if (!iv_ads_fb_friend.isChecked()) {
                    iv_ads_fb_friend.setChecked(false);
                    ads_fb_friend = "0";

                } else {
                    iv_ads_fb_friend.setChecked(true);
                    ads_fb_friend = "1";
                }
                break;
            case R.id.iv_single_profile:
                if (!iv_single_profile.isChecked()) {
                    iv_single_profile.setChecked(false);
                    single_profile = "0";

                } else {
                    iv_single_profile.setChecked(true);
                    single_profile = "1";
                }
                break;
            case R.id.iv_new_chat_message:
                if (!iv_new_chat_message.isChecked()) {
                    iv_new_chat_message.setChecked(false);
                    new_chat_message = "0";

                } else {
                    iv_new_chat_message.setChecked(true);
                    new_chat_message = "1";
                }
                break;
            case R.id.iv_new_message:
                if (!iv_new_message.isChecked()) {
                    iv_new_message.setChecked(false);
                    new_message = "0";

                } else {
                    iv_new_message.setChecked(true);
                    new_message = "1";
                }
                break;

            case R.id.img_back:
                finish();
                break;
            case R.id.btn_save:
                v.startAnimation(AppConstants.buttonClick);
               callUpdateSettings();
               callUpdateEmailSettings();
                break;

        //===========Email notifications=============

            case R.id.iv_ads_online_email:
                if (!iv_ads_online_email.isChecked()) {
                    iv_ads_online_email.setChecked(false);
                    ads_online_email = "0";

                } else {
                    iv_ads_online_email.setChecked(true);
                    ads_online_email = "1";
                }
                break;

            case R.id.iv_new_question_email:
                if (!iv_new_question_email.isChecked()) {
                    iv_new_question_email.setChecked(false);
                    new_question_email = "0";

                } else {
                    iv_new_question_email.setChecked(true);
                    new_question_email = "1";
                }
                break;
            case R.id.iv_new_offers_email:
                if (!iv_new_offers_email.isChecked()) {
                    iv_new_offers_email.setChecked(false);
                    new_offers_email = "0";

                } else {
                    iv_new_offers_email.setChecked(true);
                    new_offers_email = "1";
                }
                break;

            case R.id.iv_ads_following_email:
                if (!iv_ads_following_email.isChecked()) {
                    iv_ads_following_email.setChecked(false);
                    ads_following_email = "0";

                } else {
                    iv_ads_following_email.setChecked(true);
                    ads_following_email = "1";
                }
                break;
            case R.id.iv_ads_fb_friend_email:
                if (!iv_ads_fb_friend_email.isChecked()) {
                    iv_ads_fb_friend_email.setChecked(false);
                    ads_fb_friend_email = "0";

                } else {
                    iv_ads_fb_friend_email.setChecked(true);
                    ads_fb_friend_email = "1";
                }
                break;
            case R.id.iv_single_profile_email:
                if (!iv_single_profile_email.isChecked()) {
                    iv_single_profile_email.setChecked(false);
                    single_profile_email = "0";

                } else {
                    iv_single_profile_email.setChecked(true);
                    single_profile_email = "1";
                }
                break;
            case R.id.iv_new_chat_message_email:

                new_chat_message_email = "1";
              /*  if (!iv_new_chat_message_email.isChecked()) {
                    iv_new_chat_message_email.setChecked(false);
                    new_chat_message_email = "0";

                } else {
                    iv_new_chat_message_email.setChecked(true);
                    new_chat_message_email = "1";
                }*/
                break;
            case R.id.iv_new_message_email:
                if (!iv_new_message_email.isChecked()) {
                    iv_new_message_email.setChecked(false);
                    new_message_email = "0";

                } else {
                    iv_new_message_email.setChecked(true);
                    new_message_email = "1";
                }
                break;
            //==========================================
        }
    }

    private void callUpdateSettings() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();

            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<SettingBean> call = apiInterface.updateSettings(user_id, ads_online, new_question, new_offers, ads_following,
                    ads_fb_friend, single_profile, new_chat_message, new_message, acceptOffer, confirm, remindConfirm,"push");
            call.enqueue(new Callback<SettingBean>() {
                @Override
                public void onResponse(@NonNull Call<SettingBean> call, @NonNull Response<SettingBean> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        SettingBean mBean = response.body();
                        if (mBean.getError() == false) {
                            Toast.makeText(context, "Settings saved successfully.", Toast.LENGTH_LONG).show();
                            finish();
                            Config.setChatMessageStatus(Integer.valueOf(mBean.getResponse().getPush().getNewMessageChat()), context);
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SettingBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                  //  CommonUtils.getInstance().showAlertMessage(context, "Something went wrong. Please try again later.");
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });

        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }



    //=============update_email_api================

    private void callUpdateEmailSettings() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();

            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Log.e("VALUE_OF_EMAIL",ads_online_email+ new_question_email+ new_offers_email+ ads_following_email+
                    ads_fb_friend_email+ single_profile_email+ "1"+ new_message_email+ acceptOffer_email+ confirm_email+remindConfirm_email+"email");


            Call<SettingBean> call = apiInterface.updateSettings(user_id, ads_online_email, new_question_email, new_offers_email, ads_following_email,
                    ads_fb_friend_email, single_profile_email, "1", new_message_email, acceptOffer_email, confirm_email, remindConfirm_email,"email");
            call.enqueue(new Callback<SettingBean>() {
                @Override
                public void onResponse(@NonNull Call<SettingBean> call, @NonNull Response<SettingBean> response) {

                    try {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        SettingBean mBean = response.body();
                        if (mBean.getError() == false) {
                           // Toast.makeText(context, "Settings saved successfully.", Toast.LENGTH_LONG).show();
                            Log.e("SETTINGS_SAVED","Successfully");
                            finish();
                            Config.setChatMessageStatus(Integer.valueOf(mBean.getResponse().getPush().getNewMessageChat()), context);
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SettingBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().showAlertMessage(context, "Something went wrong. Please try again later.");
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });

        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }



}

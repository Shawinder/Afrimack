package com.afrimack.app.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.CategoriesAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActivityCategoriesList extends AppCompatActivity implements View.OnClickListener {

    private ImageView img_back,img_tick;
    private TextView tv_titel;
    private CategoriesAdapter categoriesAdapter;
    private List<String> categriesList = new ArrayList<>();

    RecyclerView recy_categore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_list);
        init();
    }

    private void init() {

        //**************titel bar**************//
        img_back = (ImageView) findViewById(R.id.img_back);
        img_tick = (ImageView) findViewById(R.id.img_tick);

        tv_titel = (TextView) findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.categories_titel).toString());
        img_back.setOnClickListener(this);
        //************end titel*****************//
        recy_categore = (RecyclerView) findViewById(R.id.recy_categore);

        categriesList.add("Automobile");
        categriesList.add("Business Services");
        categriesList.add("Computers");
        categriesList.add("Education");
        categriesList.add("Personal");
        categriesList.add("Travel");


        categoriesAdapter = new CategoriesAdapter(categriesList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recy_categore.setLayoutManager(mLayoutManager);

        recy_categore.setAdapter(categoriesAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v == img_back){
            onBackPressed();
        }
    }
}

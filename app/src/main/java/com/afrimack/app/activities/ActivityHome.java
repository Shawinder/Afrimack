package com.afrimack.app.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.AfrimackApp;
import com.afrimack.app.R;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.fragments.DiscoverCategories;
import com.afrimack.app.fragments.DiscoverHome;
import com.afrimack.app.services.Locationservice;
import com.afrimack.app.utils.AppConstants;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import static com.afrimack.app.utils.Common.checkPermissions;

public class ActivityHome extends AppCompatActivity implements View.OnClickListener {


    private ImageView img_search, img_filter, img_user_with_badge;
    private TextView tv_discover;
    private LinearLayout all_select;
    public static LinearLayout li_lay_my_afrimack, lay_sell, lay_map, linear_place_your_ad;
    private FrameLayout frame_my_afrimack;
    LocationManager locationManager;
    boolean GpsStatus;
    AlertDialog alert;
    AlertDialog.Builder alertDialogBuilder;
    public static TextView home_notification, tv_discover_sort;
    public static ImageView imgArrow;
    private Fragment frg;
    private String lat, longi;
    int show = 0;
    private AlertDialog gpsAlertDialog;
    public static String currentFragment;
    private boolean isDataRecieved = false, isRecieverRegistered = false,
            isNetDialogShowing = false, isGpsDialogShowing = false;

    public static ActivityHome getInstance = null;
    private String TAG = "Afrimack";
    private boolean LogoutFlag = false;
    private Context context = this;

    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Config.init(this);
        getInstance = this;

        init();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (checkPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE}, 99)) {

        }


        String filter = Config.getFilter(this);
        if (filter != null && !filter.equals("")) {

        } else {
            try {
                lat = "" + AfrimackApp.getInstance().mLastLocation.getLatitude();
                longi = "" + AfrimackApp.getInstance().mLastLocation.getLongitude();
                if (lat != null) {
                    Config.setlatitude(lat, this);
                    Config.setlongitude(longi, this);
                } else {

                    finish();
                    startActivity(getIntent());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        frg = new DiscoverHome();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_container, frg);
        fragmentTransaction.commit();
        while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

       if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
       // else{
            if (!isDataRecieved) {
                isDataRecieved = true;


                if (!isMyServiceRunning()) {
                    startService(new Intent(ActivityHome.this, Locationservice.class));
                }
                else{
                    finish();
                    startActivity(getIntent());
                }
            }
        }


    }

    private void ShowGpsDialog() {
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                context);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("No GPS")
                .setMessage(getString(R.string.gps_info))
                .setPositiveButton(getString(R.string.gps_seating),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                try {
                                    // if (gpsAlertDialog != null)
                                    gpsAlertDialog.dismiss();
                                    // continue with delete
                                    removeGpsDialog();
                                    Intent intent = new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                    removeGpsDialog();
                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                // finish();
                            }
                        });

        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(false);
            }
        });
        gpsAlertDialog.show();
    }
    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;

        }
    }



    @Override
    protected void onPause() {
        super.onPause();
            isDataRecieved = false;
            stopService(new Intent(this, Locationservice.class));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getPackageName() + ".services.Locationservice").equals(service1.service.getClassName())) {
                return true;
            }
        }
        return false;
    }





    private void init() {
        imgArrow = (ImageView) findViewById(R.id.img_arrow);
        imgArrow.setOnClickListener(this);
        frame_my_afrimack = (FrameLayout) findViewById(R.id.frame_my_afrimack);
        linear_place_your_ad = (LinearLayout) findViewById(R.id.linear_place_your_ad);
        home_notification = (TextView) findViewById(R.id.home_notification);
        img_search = (ImageView) findViewById(R.id.img_search);
        img_filter = (ImageView) findViewById(R.id.img_filter);
        img_user_with_badge = (ImageView) findViewById(R.id.img_user_with_badge);
        tv_discover = (TextView) findViewById(R.id.tv_discover);

        all_select = (LinearLayout) findViewById(R.id.all_select);
        img_search.setOnClickListener(this);
        img_filter.setOnClickListener(this);
        tv_discover.setOnClickListener(this);
        frame_my_afrimack.setOnClickListener(this);
        linear_place_your_ad.setOnClickListener(this);

        tv_discover_sort = (TextView) findViewById(R.id.tv_discover_sort);
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.img_search:
                v.startAnimation(AppConstants.buttonClick);
                Intent intent = new Intent(this, ActivitySearch.class);
                startActivity(intent);

                break;

            case R.id.img_filter:
                v.startAnimation(AppConstants.buttonClick);
                Intent filter = new Intent(this, ActivityFilter.class);
                startActivity(filter);

//                Intent facebookIntent = getOpenFacebookIntent(this);
//                startActivity(facebookIntent);

                break;
            case R.id.tv_discover:
                v.startAnimation(AppConstants.buttonClick);
                if (show == 1) {
                    show = 0;
                    Config.setSortSubCategory("",context);
                    frg = new DiscoverHome();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, frg);
                    fragmentTransaction.commit();
                    while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getSupportFragmentManager().popBackStackImmediate();
                    }
                } else {
                    show = 1;
                    frg = new DiscoverCategories();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, frg);
                    fragmentTransaction.commit();
                    while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getSupportFragmentManager().popBackStackImmediate();
                    }
                }
                break;
            case R.id.img_arrow:

                if (show == 1) {
                    show = 0;
                    frg = new DiscoverHome();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, frg);
                    fragmentTransaction.commit();
                    while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getSupportFragmentManager().popBackStackImmediate();
                    }
                } else {
                    show = 1;
                    frg = new DiscoverCategories();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, frg);
                    fragmentTransaction.commit();
                    while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getSupportFragmentManager().popBackStackImmediate();
                    }
                }
                break;

            case R.id.frame_my_afrimack:
                v.startAnimation(AppConstants.buttonClick);
                if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                    Intent my_arimack = new Intent(this, ActMyAfrimack.class);
                    my_arimack.putExtra("from", "a");
                    startActivity(my_arimack);
                } else {
                    Intent mIntent = new Intent(this, SplashLogin.class);
                    startActivity(mIntent);
                }
                break;
            case R.id.linear_place_your_ad:
                v.startAnimation(AppConstants.buttonClick);
                if (!(Config.getUserId(this)).equalsIgnoreCase("null") && !(Config.getUserId(this)).equalsIgnoreCase("")) {
                    Intent sell = new Intent(this, SellOffer.class);
                    Config.setPostId("", this);
                    Config.setPostType("", this);
                    startActivity(sell);
                } else {
                    Intent mIntent = new Intent(this, SplashLogin.class);
                    startActivity(mIntent);
                }

                break;
        }

    }

    public static Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://facewebmodal/f?href=https://www.facebook.com/504799703224146")); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/arkverse")); //catches and opens a url to the desired page
        }
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(ActivityHome.this, Locationservice.class));
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if (show == 1) {
            show = 0;
            frg = new DiscoverHome();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_container, frg);
            fragmentTransaction.commit();
            while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }
        else if( DiscoverHome.all_select.getVisibility()==View.VISIBLE)             //DiscoverHome.TAG.equalsIgnoreCase("DiscoverHome") &&
        {
            Log.e("back press at home:- ","called");

            DiscoverHome.all_select.setVisibility(View.GONE);
            Config.setSortSpecialCategory("", context);
            Config.setSortCategory("", context);
            show = 0;
            frg = new DiscoverHome();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_container, frg);
            fragmentTransaction.commit();
            while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            }
        }
        else {
            super.onBackPressed();
            finish();
        }
    }


}

package com.afrimack.app.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.adapters.SellViewPagerAdapter;
import com.afrimack.app.rest.ApiHitListener;
import com.afrimack.app.rest.ApiIds;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.utils.Common;

public class PremiumFeaturesInfo extends AppCompatActivity implements View.OnClickListener, ApiHitListener {

    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;
    private Toolbar toolbarMyAfrimack;
    public TabLayout tablayoutMyAfrimack;
    private ViewPager viewpagerper;
    private RestClient restClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium_features_info);
        init();
    }


    private void init() {
        //**************titel bar**************//
        img_back = (ImageView)findViewById(R.id.img_back);
        img_tick = (ImageView)findViewById(R.id.img_tick);
        img_tick.setVisibility(View.GONE);

        tv_titel = (TextView)findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.premiun_fetures).toString());
        img_back.setOnClickListener(this);


        viewpagerper = (ViewPager) findViewById(R.id.viewpagerper);
        toolbarMyAfrimack = (Toolbar)findViewById(R.id.toolbarMyAfrimack);
        tablayoutMyAfrimack = (TabLayout)findViewById(R.id.tablayoutMyAfrimack);
        SellViewPagerAdapter MyAfrimackViewPagerAdapter = new SellViewPagerAdapter(getSupportFragmentManager());

        viewpagerper.setAdapter(MyAfrimackViewPagerAdapter);
        tablayoutMyAfrimack.setupWithViewPager(viewpagerper);
    }

    @Override
    public void onClick(View v) {
        if (v == img_back){
            onBackPressed();
        }

    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }

    @Override
    public void onSuccessResponse(int apiId, Object response) {
        Common.dismissLoadingDialog();
        if(apiId == ApiIds.PREMIUMFEATURE){

        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {
        Common.dismissLoadingDialog();
    }
}

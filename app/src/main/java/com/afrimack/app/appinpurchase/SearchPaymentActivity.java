package com.afrimack.app.appinpurchase;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.appinpurchase.payment.Constants;
import com.afrimack.app.appinpurchase.payment.IabActivity;

import static com.afrimack.app.appinpurchase.payment.Constants.SKU_GAS;
import static com.afrimack.app.appinpurchase.payment.Constants.SKU_INFINITE_GAS;
import static com.afrimack.app.appinpurchase.payment.Constants.SKU_PREMIUM;

public class SearchPaymentActivity extends IabActivity implements View.OnClickListener {

    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;
    private LinearLayout ll_search_best;
    /*payment*/
    static final String TAG = Constants.LOG_IAB;

    // Helper object for in-app billing.
    IabHelper mHelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_payment);

        init();

        // Start setup of in-app billing. Note that this work is now done in the IabActivity, which
        // is the superclass of this method.
        setupIabHelper (true, true);

        // Set a variable for convenient access to the iab helper object.
        mHelper = getIabHelper ();

        // load game data
        loadData();
        // updateUi ();

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);
    }

    private void init() {
        //**************titel bar**************//
        img_back = (ImageView)findViewById(R.id.img_back);
        img_tick = (ImageView)findViewById(R.id.img_tick);
        img_tick.setVisibility(View.GONE);

        tv_titel = (TextView)findViewById(R.id.tv_titel);
        tv_titel.setText(getString(R.string.premiun_fetures).toString());
        img_back.setOnClickListener(this);

        ll_search_best = (LinearLayout) findViewById(R.id.ll_search_best);
        ll_search_best.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back:
              finish();
                break;
            case R.id.ll_search_best:

                    Log.d(TAG, "Buy Basic Hot");

                    if (mSubscribedToInfiniteGas) {
                        complain("No need! You're subscribed to infinite gas. Isn't that awesome?");
                        return;
                    }

                    /*if (mTank >= TANK_MAX) {
                        complain("Your tank is full. Drive around a bit!");
                        return;
                    }*/

                    // launch the gas purchase UI flow.
                    // We will be notified of completion via mPurchaseFinishedListener
                    // setWaitScreen(true);
                    Log.d(TAG, "Launching Basic Hot.");

                    // The steps needed to complete a purchase are done in code in the IabActivity superclass.
                    // IabActivity provides standard handling and calls back to this class.
                    launchInAppPurchaseFlow (this, SKU_GAS);

                    /* TODO: for security, generate your payload here for verification. See the comments on
                     *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
                     *        an empty string, but on a production app you should carefully generate this. */

                    String payload = "";

                  /*  mHelper.launchPurchaseFlow(this, SKU_GAS, RC_PURCHASE_REQUEST,
                            mPurchaseFinishedListener, payload);*/




                break;
        }

    }

  /*  private void premiunApi() {
        if(ConnectionDetector.isNetAvail(this)){

            restClient.callback(this).PostPremium(user_id);
            displayLoadingDialog(false,this);

        }else {
            displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC,this);
        }
    }
*/
    /*@Override
    public void onSuccessResponse(int apiId, Object response) {
        Common.dismissLoadingDialog();
        premiumResponseModel = (PremiumResponseModel) response;
        if(apiId == ApiIds.PREMIUMFEATURE){
            premiumResponseModel = (PremiumResponseModel) response;
            if(premiumResponseModel.isError() == false){
                if(premiumResponseModel.getResponse().getBASIC().size()>0){
                    basiclist.addAll(premiumResponseModel.getResponse().getBASIC()) ;
                    CategoriesAdapter = new BasicPremiumAdapter(this, basiclist,onClickListener);
                    recy_basic.setAdapter(CategoriesAdapter);
                }
                if(premiumResponseModel.getResponse().getMEDIUM().size()>0){
                    mediumlist.addAll(premiumResponseModel.getResponse().getMEDIUM()) ;
                    mediumPremiumAdapter = new MediumPremiumAdapter(this, mediumlist,onClickListener);
                    recy_medium.setAdapter(mediumPremiumAdapter);
                }
                if(premiumResponseModel.getResponse().getLARGE().size()>0){
                    largelist.addAll(premiumResponseModel.getResponse().getLARGE()) ;
                    largePremiumAdapter = new LargePremiumAdapter(this,largelist,onClickListener);
                    recy_large.setAdapter(largePremiumAdapter);
                }

            }

        }
    }

    @Override
    public void onFailResponse(int apiId, String error) {

    }
*/    /**
     * Called when something fails in the purchase flow before the part where the item is consumed.
     *
     * <p> This is the place to reset the UI if something was done to indicate that a purchase has started.
     *
     * @param h IabHelper
     * @param errorNum int - error number from Constants
     * @return void
     */

    @Override
    protected void onIabPurchaseFailed(IabHelper h, int errorNum) {
        // We did set up in such a way so that error messages have already been display (with complain method).
        // So all we have to do is remove the "waiting" indicator.
        if (errorNum != 0) ;


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

// (wgl, May 2015)
// The next two variables are the original listener objects.
// They are no longer used. See superclass IabActivity for the current ones.

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListenerOLD = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
               /* updateUi();
                setWaitScreen(false);*/
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                /*updateUi();
                setWaitScreen(false);*/
                return;
            }

            Log.d(TAG, "Purchase successful.");

            if (purchase.getSku().equals(SKU_GAS)) {
                // bought 1/4 tank of gas. So consume it.
                Log.d(TAG, "Purchase is gas. Starting gas consumption.");
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }
            else if (purchase.getSku().equals(SKU_PREMIUM)) {
                // bought the premium upgrade!
                Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
                alert("Thank you for upgrading to premium!");
                mIsPremium = true;
                /*updateUi();
                setWaitScreen(false);*/
            }
            else if (purchase.getSku().equals(SKU_INFINITE_GAS)) {
                // bought the infinite gas subscription
                Log.d(TAG, "Infinite gas subscription purchased.");
                alert("Thank you for subscribing to infinite gas!");
                mSubscribedToInfiniteGas = true;
                //mTank = TANK_MAX;
                /*updateUi();
                setWaitScreen(false);*/
            }
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListenerOLD = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");
                //mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                saveData();
                // alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
            }
            else {
                complain("Error while consuming: " + result);
            }
          /*  updateUi();
            setWaitScreen(false);*/
            Log.d(TAG, "End consumption flow.");
        }
    };

// Methods

    // Drive button clicked. Burn gas!
    public void onDriveButtonClicked(View arg0) {
        Log.d(TAG, "Drive button clicked.");
       /* if (!mSubscribedToInfiniteGas && mTank <= 0) alert("Oh, no! You are out of gas! Try buying some!");
        else {
            if (!mSubscribedToInfiniteGas) --mTank;
            saveData();
            alert("Vroooom, you drove a few miles.");
           // updateUi();
            Log.d(TAG, "Vrooom. Tank is now " + mTank);
        }*/
        // updateUi ();
    }

    // We're being destroyed. It's important to dispose of the helper here!
    @Override
    public void onDestroy() {
        super.onDestroy();

        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }


    void saveData() {

        /*
         * WARNING: on a real application, we recommend you save data in a secure way to
         * prevent tampering. For simplicity in this sample, we simply store the data using a
         * SharedPreferences.
         */

        SharedPreferences.Editor spe = getPreferences(MODE_PRIVATE).edit();
        //spe.putInt("tank", mTank);
        spe.commit();
        // Log.d(TAG, "Saved data: tank = " + String.valueOf(mTank));
    }

    void loadData() {
        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        // mTank = sp.getInt("tank", 2);
        ///Log.d(TAG, "Loaded data: tank = " + String.valueOf(mTank));
    }

/**
 */
// Methods for IabHelperListener.
// Subclasses should call the superclass method if they override any of these methods.

    /**
     * Called when consumption of a purchase item fails.
     *
     * <p> If this class was set up to issue messages upon failure, there is probably
     * nothing else to be done.
     */

    public void onIabConsumeItemFailed(IabHelper h) {
        super.onIabConsumeItemFailed (h);

        // Do whatever you need to in the ui to indicate that consuming a purchase failed.
        /*updateUi();
        setWaitScreen(false);*/

    }

    /**
     * Called when consumption of a purchase item succeeds.
     *
     * SKU_GAS is the only consumable ite,. When it is purchased, this method gets called.
     * So this is the place where the tank is filled.
     *
     * @param h IabHelper - helper object
     * @param purchase Purchase
     * @param result IabResult
     */

    public void onIabConsumeItemSucceeded(IabHelper h, Purchase purchase, IabResult result) {
        super.onIabConsumeItemSucceeded (h, purchase, result);

        // Update the state of the app and the ui to show the item we purchased and consumed.
        String purchaseSku = purchase.getSku ();
        if (purchaseSku.equals(SKU_GAS)) {
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                Log.d(TAG, "Consumption successful. Provisioning.");
                // mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                saveData();
                // alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
            } else {
                complain("Error while consuming regular gas: " + result);
            }

        }

        // Original code did some processing here. I moved this to IabActivity. (wgl, May 2015)
    /*
    else if (purchase.getSku().equals(SKU_PREMIUM)) {
      // bought the premium upgrade!
      Log.d(TAG, "Purchase is premium upgrade. Congratulating user.");
      alert("Thank you for upgrading to premium!");
      mIsPremium = true;
      updateUi();
      setWaitScreen(false);
    } else if (purchase.getSku().equals(SKU_INFINITE_GAS)) {

      // bought the infinite gas subscription
      Log.d(TAG, "Infinite gas subscription purchased.");
      alert("Thank you for subscribing to infinite gas!");
      mSubscribedToInfiniteGas = true;
      mTank = TANK_MAX;
      updateUi();
      setWaitScreen(false);
    }
    */

      /*  updateUi();
        setWaitScreen(false);*/

    }

    /**
     * Called when setup fails and the inventory of items is not available.
     *
     * <p> If this class was set up to issue messages upon failure, there is probably
     * nothing else to be done.
     */

    public void onIabSetupFailed(IabHelper h) {
        super.onIabSetupFailed (h);

        // This would be where to change the ui in the event of a set up error.
    }

    /**
     * Called when setup succeeds and the inventory of items is available.
     *
     * @param h IabHelper - helper object
     * @param result IabResult
     * @param inventory Inventory
     *
     */

    public void onIabSetupSucceeded(IabHelper h, IabResult result, Inventory inventory) {
        super.onIabSetupSucceeded (h, result, inventory);

        // The superclass setup method checks to see what has been purchased and what has been subscribed to.
        // Premium and infinite gas are handled here. If there was a regular gas purchase, steps to consume
        // it (via an async call to consume) were started in the superclass.

        //if (mSubscribedToInfiniteGas) mTank = TANK_MAX;

        if (mIsPremium) {
            // FIX THIS
        }

       /* updateUi();
        setWaitScreen(false);*/

    }
}

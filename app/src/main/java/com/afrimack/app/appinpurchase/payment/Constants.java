package com.afrimack.app.appinpurchase.payment;

import com.afrimack.app.R;

/**
 * A class that defines constants used throughout the app.
 *
 */
public class Constants {

/**
 */
// In-app billing constants

// SKUs for our products: the premium upgrade (non-consumable)
// and gas (consumable)
public static final String SKU_PREMIUM = "premium2";
public static final String SKU_GAS = "basicdhot";

// SKU for our subscription (infinite gas)
public static final String SKU_INFINITE_GAS = "infinite_gas2";

    //used in app post
    public static final String BASIC_HOT = "basic3dhot";
    public static final String BASIC_TOP = "basictop";
    public static final String BASIC_HOT_TOP = "basichottop";

    public static final String MEDIUM_HOT = "mediumhot";
    public static final String MEDIUM_TOP = "mediumtop";
    public static final String MEDIUM_HOT_TOP = "mediumhottop";

    public static final String LARGE_HOT = "largehot";
    public static final String LARGE_TOP = "largetop";
    public static final String LARGE_HOT_TOP = "largehottop";

    // used for additional search
    public static final String ADDITIONAL_ONE = "additionalone";
    public static final String ADDITIONAL_THREE = "additionalthree";



/**
 */
// Activity request code

public static final int RC_PURCHASE_REQUEST = 10001;

/**
 */
// IAB error 

public static final int IAB_PURCHASE_FAILED = 101;
public static final int IAB_PURCHASE_FAILED_PAYLOAD_PROBLEM = 102;

/**
 */
public static final String LOG_IAB = "AFRIMACK IAB";


/**
 */
// Graphics for the gas gauge
public static final int[] TANK_RES_IDS = { R.drawable.ic_launcher, R.drawable.ic_launcher, R.drawable.ic_launcher,
                                   R.drawable.ic_launcher, R.drawable.ic_launcher };

// How many units (1/4 tank is our unit) fill in the tank.
public static final int TANK_MAX = 4;

}


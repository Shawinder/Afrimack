package com.afrimack.app.models;

import java.util.List;


public class PostEditModel {



    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int id;
        private int user_id;
        private Object buyer_id;
        private int category_id;
        private String post_type;
        private String title;
        private String description;
        private String address;
        private String latitude;
        private String longitude;
        private String country;
        private String currency_symbol;
        private int price;
        private String duration_type;
        private String duration_value;
        private String swap_deal;
        private String swap_deal_title;
        private String share_fb;
        private String post_status;
        private Object counter_price;
        private Object deal_price;
        private int search_counter;
        private int premium;
        private String enabled;
        private String is_deleted;
        private String created;
        private String modified;
        private Object updated;
        private int country_id;
        private String currency_name;
        private String sub_category_name;
        private String sub_category_id;
        private String category_name;
        private List<ImagesBean> images;

        public int getId() {
            return id;
        }


        //=======changes=========
        public String getSub_category_id() {
            return sub_category_id;
        }

        public void setSub_category_id(String sub_category_id) {
            this.sub_category_id = sub_category_id;
        }


        public String getSub_category_name() {
            return sub_category_name;
        }

        public void setSub_category_name(String sub_category_name) {
            this.sub_category_name = sub_category_name;
        }

        //=========================
        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public Object getBuyer_id() {
            return buyer_id;
        }

        public void setBuyer_id(Object buyer_id) {
            this.buyer_id = buyer_id;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCurrency_symbol() {
            return currency_symbol;
        }

        public void setCurrency_symbol(String currency_symbol) {
            this.currency_symbol = currency_symbol;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getDuration_type() {
            return duration_type;
        }

        public void setDuration_type(String duration_type) {
            this.duration_type = duration_type;
        }

        public String getDuration_value() {
            return duration_value;
        }

        public void setDuration_value(String duration_value) {
            this.duration_value = duration_value;
        }

        public String getSwap_deal() {
            return swap_deal;
        }

        public void setSwap_deal(String swap_deal) {
            this.swap_deal = swap_deal;
        }

        public String getSwap_deal_title() {
            return swap_deal_title;
        }

        public void setSwap_deal_title(String swap_deal_title) {
            this.swap_deal_title = swap_deal_title;
        }

        public String getShare_fb() {
            return share_fb;
        }

        public void setShare_fb(String share_fb) {
            this.share_fb = share_fb;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public Object getCounter_price() {
            return counter_price;
        }

        public void setCounter_price(Object counter_price) {
            this.counter_price = counter_price;
        }

        public Object getDeal_price() {
            return deal_price;
        }

        public void setDeal_price(Object deal_price) {
            this.deal_price = deal_price;
        }

        public int getSearch_counter() {
            return search_counter;
        }

        public void setSearch_counter(int search_counter) {
            this.search_counter = search_counter;
        }

        public int getPremium() {
            return premium;
        }

        public void setPremium(int premium) {
            this.premium = premium;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getModified() {
            return modified;
        }

        public void setModified(String modified) {
            this.modified = modified;
        }

        public Object getUpdated() {
            return updated;
        }

        public void setUpdated(Object updated) {
            this.updated = updated;
        }

        public int getCountry_id() {
            return country_id;
        }

        public void setCountry_id(int country_id) {
            this.country_id = country_id;
        }

        public String getCurrency_name() {
            return currency_name;
        }

        public void setCurrency_name(String currency_name) {
            this.currency_name = currency_name;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public List<ImagesBean> getImages() {
            return images;
        }

        public void setImages(List<ImagesBean> images) {
            this.images = images;
        }

        public static class ImagesBean {

            private int id;
            private String value;
            private String thumb;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getThumb() {
                return thumb;
            }

            public void setThumb(String thumb) {
                this.thumb = thumb;
            }
        }
    }
}

package com.afrimack.app.models;

import java.util.List;

public class FollowingResponseModel {



    private int code;
    private boolean error;
    private String message;
    private List<ResponseBean> response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResponseBean> getResponse() {
        return response;
    }

    public void setResponse(List<ResponseBean> response) {
        this.response = response;
    }

    public static class ResponseBean {

        private String full_name;
        private String date;
        private String image;
        private String short_name;
        private int follow_id;
        private int follower_id;
        private int rating;
        private String single_registered;
        private String firebase_UID;

        public String getFirebase_UID() {
            return firebase_UID;
        }

        public void setFirebase_UID(String firebase_UID) {
            this.firebase_UID = firebase_UID;
        }

        public String getSingle_registered() {
            return single_registered;
        }

        public void setSingle_registered(String single_registered) {
            this.single_registered = single_registered;
        }

        public String getShort_name() {
            return short_name;
        }

        public void setShort_name(String short_name) {
            this.short_name = short_name;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getFollow_id() {
            return follow_id;
        }

        public void setFollow_id(int follow_id) {
            this.follow_id = follow_id;
        }

        public int getFollower_id() {
            return follower_id;
        }

        public void setFollower_id(int follower_id) {
            this.follower_id = follower_id;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }
    }
}

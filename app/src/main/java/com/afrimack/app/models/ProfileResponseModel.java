package com.afrimack.app.models;

public class ProfileResponseModel {



    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int id;
        private String full_name;
        private String image;
        private String since;
        private int selling;
        private int purchase;
        private int favorite;
        private int rating_count;
        private int rating;
        private int following;
        private int completion;
        private int block;
        private String profile_type;
        private int total_unread_msg_count;
        private String single_registered;

        public String getSingle_registered() {
            return single_registered;
        }

        public void setSingle_registered(String single_registered) {
            this.single_registered = single_registered;
        }

        public int getTotal_unread_msg_count() {
            return total_unread_msg_count;
        }

        public void setTotal_unread_msg_count(int total_unread_msg_count) {
            this.total_unread_msg_count = total_unread_msg_count;
        }

        public int getBlock() {
            return block;
        }

        public void setBlock(int block) {
            this.block = block;
        }

        public String getProfile_type() {
            return profile_type;
        }

        public void setProfile_type(String profile_type) {
            this.profile_type = profile_type;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getSince() {
            return since;
        }

        public void setSince(String since) {
            this.since = since;
        }

        public int getSelling() {
            return selling;
        }

        public void setSelling(int selling) {
            this.selling = selling;
        }

        public int getPurchase() {
            return purchase;
        }

        public void setPurchase(int purchase) {
            this.purchase = purchase;
        }

        public int getFavorite() {
            return favorite;
        }

        public void setFavorite(int favorite) {
            this.favorite = favorite;
        }

        public int getRating_count() {
            return rating_count;
        }

        public void setRating_count(int rating_count) {
            this.rating_count = rating_count;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public int getFollowing() {
            return following;
        }

        public void setFollowing(int following) {
            this.following = following;
        }

        public int getCompletion() {
            return completion;
        }

        public void setCompletion(int completion) {
            this.completion = completion;
        }
    }
}

package com.afrimack.app.models;

import java.util.List;

public class AlertSearchResponseModel {



    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        private List<AlertBean> alert;
        private List<SearchBean> search;

        public List<AlertBean> getAlert() {
            return alert;
        }

        public void setAlert(List<AlertBean> alert) {
            this.alert = alert;
        }

        public List<SearchBean> getSearch() {
            return search;
        }

        public void setSearch(List<SearchBean> search) {
            this.search = search;
        }

        public static class AlertBean {

            private String term;

            public String getTerm() {
                return term;
            }

            public void setTerm(String term) {
                this.term = term;
            }
        }

        public static class SearchBean {

            private String term;

            public String getTerm() {
                return term;
            }

            public void setTerm(String term) {
                this.term = term;
            }
        }
    }
}

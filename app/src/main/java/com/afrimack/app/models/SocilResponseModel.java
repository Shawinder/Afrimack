package com.afrimack.app.models;

public class SocilResponseModel {


    private boolean error;
    private int code;
    private String message;
    private ResponseBean response;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int id;
        private int access_level_id;
        private String username;
        private String password;
        private Object gender;
        private String first_name;
        private Object last_name;
        private String email;
        private Object country_id;
        private Object phone_number;
        private String image;
        private Object dob;
        private String profile_type;
        private String single_registered;
        private Object looking_for;
        private Object about_me;
        private Object latitude;
        private Object longitude;
        private String fb_id;
        private Object google_id;
        private String no_verified;
        private String email_notification;
        private String notification_sound;
        private String email_chat;
        private String device_type;
        private String device_token;
        private Object otp;
        private Object last_login;
        private int is_logged;
        private String enabled;
        private String is_deleted;
        private String created;
        private String modified;
        private Object age;
        private String full_name;
        private String firebase_UID;

        public String getFirebase_UID() {
            return firebase_UID;
        }

        public void setFirebase_UID(String firebase_UID) {
            this.firebase_UID = firebase_UID;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAccess_level_id() {
            return access_level_id;
        }

        public void setAccess_level_id(int access_level_id) {
            this.access_level_id = access_level_id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Object getGender() {
            return gender;
        }

        public void setGender(Object gender) {
            this.gender = gender;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public Object getLast_name() {
            return last_name;
        }

        public void setLast_name(Object last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getCountry_id() {
            return country_id;
        }

        public void setCountry_id(Object country_id) {
            this.country_id = country_id;
        }

        public Object getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(Object phone_number) {
            this.phone_number = phone_number;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getDob() {
            return dob;
        }

        public void setDob(Object dob) {
            this.dob = dob;
        }

        public String getProfile_type() {
            return profile_type;
        }

        public void setProfile_type(String profile_type) {
            this.profile_type = profile_type;
        }

        public String getSingle_registered() {
            return single_registered;
        }

        public void setSingle_registered(String single_registered) {
            this.single_registered = single_registered;
        }

        public Object getLooking_for() {
            return looking_for;
        }

        public void setLooking_for(Object looking_for) {
            this.looking_for = looking_for;
        }

        public Object getAbout_me() {
            return about_me;
        }

        public void setAbout_me(Object about_me) {
            this.about_me = about_me;
        }

        public Object getLatitude() {
            return latitude;
        }

        public void setLatitude(Object latitude) {
            this.latitude = latitude;
        }

        public Object getLongitude() {
            return longitude;
        }

        public void setLongitude(Object longitude) {
            this.longitude = longitude;
        }

        public String getFb_id() {
            return fb_id;
        }

        public void setFb_id(String fb_id) {
            this.fb_id = fb_id;
        }

        public Object getGoogle_id() {
            return google_id;
        }

        public void setGoogle_id(Object google_id) {
            this.google_id = google_id;
        }

        public String getNo_verified() {
            return no_verified;
        }

        public void setNo_verified(String no_verified) {
            this.no_verified = no_verified;
        }

        public String getEmail_notification() {
            return email_notification;
        }

        public void setEmail_notification(String email_notification) {
            this.email_notification = email_notification;
        }

        public String getNotification_sound() {
            return notification_sound;
        }

        public void setNotification_sound(String notification_sound) {
            this.notification_sound = notification_sound;
        }

        public String getEmail_chat() {
            return email_chat;
        }

        public void setEmail_chat(String email_chat) {
            this.email_chat = email_chat;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public Object getOtp() {
            return otp;
        }

        public void setOtp(Object otp) {
            this.otp = otp;
        }

        public Object getLast_login() {
            return last_login;
        }

        public void setLast_login(Object last_login) {
            this.last_login = last_login;
        }

        public int getIs_logged() {
            return is_logged;
        }

        public void setIs_logged(int is_logged) {
            this.is_logged = is_logged;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getModified() {
            return modified;
        }

        public void setModified(String modified) {
            this.modified = modified;
        }

        public Object getAge() {
            return age;
        }

        public void setAge(Object age) {
            this.age = age;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }
    }
}

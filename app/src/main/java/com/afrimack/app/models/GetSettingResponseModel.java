package com.afrimack.app.models;

public class GetSettingResponseModel {


    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private String email_notification;
        private String notification_sound;
        private String email_chat;
        private String newsletter;

        public String getEmail_notification() {
            return email_notification;
        }

        public void setEmail_notification(String email_notification) {
            this.email_notification = email_notification;
        }

        public String getNotification_sound() {
            return notification_sound;
        }

        public void setNotification_sound(String notification_sound) {
            this.notification_sound = notification_sound;
        }

        public String getEmail_chat() {
            return email_chat;
        }

        public void setEmail_chat(String email_chat) {
            this.email_chat = email_chat;
        }

        public String getNewsletter() {
            return newsletter;
        }

        public void setNewsletter(String newsletter) {
            this.newsletter = newsletter;
        }
    }
}

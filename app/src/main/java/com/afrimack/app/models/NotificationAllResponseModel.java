package com.afrimack.app.models;

import java.util.List;

public class NotificationAllResponseModel {


    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int unread_count;
        private List<NotificationsBean> notifications;

        public int getUnread_count() {
            return unread_count;
        }

        public void setUnread_count(int unread_count) {
            this.unread_count = unread_count;
        }

        public List<NotificationsBean> getNotifications() {
            return notifications;
        }

        public void setNotifications(List<NotificationsBean> notifications) {
            this.notifications = notifications;
        }

        public static class NotificationsBean {

            private int notification_id;
            private String message;
            private int post_id;
            private String is_read;
            private String duration;
            private String post_image;
            private String user_image;
            private String title;
            private String type;
            private String offer_user_id;
            private String post_user_id;


            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getOffer_user_id() {
                return offer_user_id;
            }

            public void setOffer_user_id(String offer_user_id) {
                this.offer_user_id = offer_user_id;
            }

            public String getPost_user_id() {
                return post_user_id;
            }

            public void setPost_user_id(String post_user_id) {
                this.post_user_id = post_user_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getNotification_id() {
                return notification_id;
            }

            public void setNotification_id(int notification_id) {
                this.notification_id = notification_id;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public int getPost_id() {
                return post_id;
            }

            public void setPost_id(int post_id) {
                this.post_id = post_id;
            }

            public String getIs_read() {
                return is_read;
            }

            public void setIs_read(String is_read) {
                this.is_read = is_read;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }

            public String getPost_image() {
                return post_image;
            }

            public void setPost_image(String post_image) {
                this.post_image = post_image;
            }

            public String getUser_image() {
                return user_image;
            }

            public void setUser_image(String user_image) {
                this.user_image = user_image;
            }
        }
    }
}

package com.afrimack.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SingleProfileResponseModel implements Serializable {


    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean implements Serializable {

        private String looking_for;
        private String about_me;
        private String image;
        private String gender;
        private String first_name;
        private String last_name;
        private String country_name;
        private String age;
        private String short_name;
        private String follow;
        private String profile_date;
        private String image_thumb;
        private String firebase_UID;
        private String interests;
        private int chat_request_status;

        public String getInterests() {
            return interests;
        }

        public void setInterests(String interests) {
            this.interests = interests;
        }

        @SerializedName("php_user_id")
        @Expose
        private int user_id;

        @SerializedName("php_block_status")
        @Expose
        private int blockedStatus;
        @SerializedName("php_block_by")
        @Expose
        private int php_block_by;
        @SerializedName("php_block_id")
        @Expose
        private int php_block_id;

        public int getChat_request_status() {
            return chat_request_status;
        }

        public void setChat_request_status(int chat_request_status) {
            this.chat_request_status = chat_request_status;
        }

        public int getPhp_block_id() {
            return php_block_id;
        }

        public void setPhp_block_id(int php_block_id) {
            this.php_block_id = php_block_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getPhp_block_by() {
            return php_block_by;
        }

        public void setPhp_block_by(int php_block_by) {
            this.php_block_by = php_block_by;
        }

        public int getBlockedStatus() {
            return blockedStatus;
        }

        public void setBlockedStatus(int blockedStatus) {
            this.blockedStatus = blockedStatus;
        }

        public String getFirebase_UID() {
            return firebase_UID;
        }

        public void setFirebase_UID(String firebase_UID) {
            this.firebase_UID = firebase_UID;
        }

        public String getProfile_date() {
            return profile_date;
        }

        public void setProfile_date(String profile_date) {
            this.profile_date = profile_date;
        }

        public String getFollow() {
            return follow;
        }

        public void setFollow(String follow) {
            this.follow = follow;
        }

        public String getImage_thumb() {
            return image_thumb;
        }

        public void setImage_thumb(String image_thumb) {
            this.image_thumb = image_thumb;
        }


        public String getShort_name() {
            return short_name;
        }

        public void setShort_name(String short_name) {
            this.short_name = short_name;
        }

        public String getLooking_for() {
            return looking_for;
        }

        public void setLooking_for(String looking_for) {
            this.looking_for = looking_for;
        }

        public String getAbout_me() {
            return about_me;
        }

        public void setAbout_me(String about_me) {
            this.about_me = about_me;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getCountry_name() {
            return country_name;
        }

        public void setCountry_name(String country_name) {
            this.country_name = country_name;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }
    }
}

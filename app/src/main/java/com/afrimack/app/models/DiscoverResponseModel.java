package com.afrimack.app.models;

import java.util.List;

public class DiscoverResponseModel {


    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        private List<SpecialBean> special;
        private List<CategoryBean> category;
        private List<PartnerBean> partner;

        public List<SpecialBean> getSpecial() {
            return special;
        }

        public void setSpecial(List<SpecialBean> special) {
            this.special = special;
        }

        public List<CategoryBean> getCategory() {
            return category;
        }

        public void setCategory(List<CategoryBean> category) {
            this.category = category;
        }

        public List<PartnerBean> getPartner() {
            return partner;
        }

        public void setPartner(List<PartnerBean> partner) {
            this.partner = partner;
        }

        public static class SpecialBean {

            private int id;
            private String category_name;
            private String cat_logo_app3;
            private String icon_logo;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public String getCat_logo_app3() {
                return cat_logo_app3;
            }

            public void setCat_logo_app3(String cat_logo_app3) {
                this.cat_logo_app3 = cat_logo_app3;
            }

            public String getIcon_logo() {
                return icon_logo;
            }

            public void setIcon_logo(String icon_logo) {
                this.icon_logo = icon_logo;
            }
        }

        public static class CategoryBean {

            private int id;
            private String category_name;
            private String cat_logo_app3;
            private String icon_logo;
            private boolean isSelected = false;

            public boolean isSelected() {
                return isSelected;
            }

            public void setSelected(boolean isSelected) {
                this.isSelected = isSelected;
            }
            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public String getCat_logo_app3() {
                return cat_logo_app3;
            }

            public void setCat_logo_app3(String cat_logo_app3) {
                this.cat_logo_app3 = cat_logo_app3;
            }

            public String getIcon_logo() {
                return icon_logo;
            }

            public void setIcon_logo(String icon_logo) {
                this.icon_logo = icon_logo;
            }
        }

        public static class PartnerBean {

            private int id;
            private String category_name;
            private String cat_logo_app3;
            private String icon_logo;

            public String getSingle_registered() {
                return single_registered;
            }

            public void setSingle_registered(String single_registered) {
                this.single_registered = single_registered;
            }

            private String single_registered;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCategory_name() {
                return category_name;
            }

            public void setCategory_name(String category_name) {
                this.category_name = category_name;
            }

            public String getCat_logo_app3() {
                return cat_logo_app3;
            }

            public void setCat_logo_app3(String cat_logo_app3) {
                this.cat_logo_app3 = cat_logo_app3;
            }

            public String getIcon_logo() {
                return icon_logo;
            }

            public void setIcon_logo(String icon_logo) {
                this.icon_logo = icon_logo;
            }
        }
    }
}

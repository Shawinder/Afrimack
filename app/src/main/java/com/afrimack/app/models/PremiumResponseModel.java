package com.afrimack.app.models;

import java.util.List;

public class PremiumResponseModel {


    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {
        private List<BASICBean> BASIC;
        private List<MEDIUMBean> MEDIUM;
        private List<LARGEBean> LARGE;


        public List<BASICBean> getBASIC() {
            return BASIC;
        }

        public void setBASIC(List<BASICBean> BASIC) {
            this.BASIC = BASIC;
        }

        public List<MEDIUMBean> getMEDIUM() {
            return MEDIUM;
        }

        public void setMEDIUM(List<MEDIUMBean> MEDIUM) {
            this.MEDIUM = MEDIUM;
        }

        public List<LARGEBean> getLARGE() {
            return LARGE;
        }

        public void setLARGE(List<LARGEBean> LARGE) {
            this.LARGE = LARGE;
        }

        public static class BASICBean {

            private String days;
            private String package_type;
            private double price;
            private int package_id;
            private int plan_status;

            public int getPlan_status() {
                return plan_status;
            }

            public void setPlan_status(int plan_status) {
                this.plan_status = plan_status;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public String getPackage_type() {
                return package_type;
            }

            public void setPackage_type(String package_type) {
                this.package_type = package_type;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public int getPackage_id() {
                return package_id;
            }

            public void setPackage_id(int package_id) {
                this.package_id = package_id;
            }
        }

        public static class MEDIUMBean {

            private String days;
            private String package_type;
            private double price;
            private int package_id;
            private int plan_status;

            public int getPlan_status() {
                return plan_status;
            }

            public void setPlan_status(int plan_status) {
                this.plan_status = plan_status;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public String getPackage_type() {
                return package_type;
            }

            public void setPackage_type(String package_type) {
                this.package_type = package_type;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public int getPackage_id() {
                return package_id;
            }

            public void setPackage_id(int package_id) {
                this.package_id = package_id;
            }
        }

        public static class LARGEBean {

            private String days;
            private String package_type;
            private double price;
            private int package_id;
            private int plan_status;

            public int getPlan_status() {
                return plan_status;
            }

            public void setPlan_status(int plan_status) {
                this.plan_status = plan_status;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public String getPackage_type() {
                return package_type;
            }

            public void setPackage_type(String package_type) {
                this.package_type = package_type;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public int getPackage_id() {
                return package_id;
            }

            public void setPackage_id(int package_id) {
                this.package_id = package_id;
            }
        }
    }
}

package com.afrimack.app.models;

import java.util.List;

public class PostDetailResponseModel {



    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int rating;
        private int rating_count;
        private int total_sell;
        private int total_purchase;
        private int user_id;
        private String title;
        private String description;
        private String swap_deal;
        private String swap_deal_title;
        private String latitude;
        private String longitude;
        private String address;
        private String post_status;
        private String duration_type;
        private String duration_value;
        private String post_image;
        private String user_image;
        private String user_name;
        private String price;
        private String last_update;
        private String follow_user;
        private String favorite_product;
        private String follow_product;
        private int fav_count;
        private int my_product;
        private int offer_count;
        private String login_user_image;
        private List<OffersBean> offers;
        private List<QuestionsBean> questions;
        private String share_url;
        private String post_type;

        public String getShare_url() {
            return share_url;
        }

        public void setShare_url(String share_url) {
            this.share_url = share_url;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getLogin_user_image() {
            return login_user_image;
        }

        public void setLogin_user_image(String login_user_image) {
            this.login_user_image = login_user_image;
        }

        public String getDuration_type() {
            return duration_type;
        }

        public void setDuration_type(String duration_type) {
            this.duration_type = duration_type;
        }

        public String getDuration_value() {
            return duration_value;
        }

        public void setDuration_value(String duration_value) {
            this.duration_value = duration_value;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public int getRating_count() {
            return rating_count;
        }

        public void setRating_count(int rating_count) {
            this.rating_count = rating_count;
        }

        public int getTotal_sell() {
            return total_sell;
        }

        public void setTotal_sell(int total_sell) {
            this.total_sell = total_sell;
        }

        public int getTotal_purchase() {
            return total_purchase;
        }

        public void setTotal_purchase(int total_purchase) {
            this.total_purchase = total_purchase;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSwap_deal() {
            return swap_deal;
        }

        public void setSwap_deal(String swap_deal) {
            this.swap_deal = swap_deal;
        }

        public String getSwap_deal_title() {
            return swap_deal_title;
        }

        public void setSwap_deal_title(String swap_deal_title) {
            this.swap_deal_title = swap_deal_title;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public String getPost_image() {
            return post_image;
        }

        public void setPost_image(String post_image) {
            this.post_image = post_image;
        }

        public String getUser_image() {
            return user_image;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getLast_update() {
            return last_update;
        }

        public void setLast_update(String last_update) {
            this.last_update = last_update;
        }

        public String getFollow_user() {
            return follow_user;
        }

        public void setFollow_user(String follow_user) {
            this.follow_user = follow_user;
        }

        public String getFavorite_product() {
            return favorite_product;
        }

        public void setFavorite_product(String favorite_product) {
            this.favorite_product = favorite_product;
        }

        public String getFollow_product() {
            return follow_product;
        }

        public void setFollow_product(String follow_product) {
            this.follow_product = follow_product;
        }

        public int getFav_count() {
            return fav_count;
        }

        public void setFav_count(int fav_count) {
            this.fav_count = fav_count;
        }

        public int getMy_product() {
            return my_product;
        }

        public void setMy_product(int my_product) {
            this.my_product = my_product;
        }

        public int getOffer_count() {
            return offer_count;
        }

        public void setOffer_count(int offer_count) {
            this.offer_count = offer_count;
        }

        public List<OffersBean> getOffers() {
            return offers;
        }

        public void setOffers(List<OffersBean> offers) {
            this.offers = offers;
        }

        public List<QuestionsBean> getQuestions() {
            return questions;
        }

        public void setQuestions(List<QuestionsBean> questions) {
            this.questions = questions;
        }

        public static class OffersBean {

            private int user_id;
            private String user;
            private String image;
            private String offer;


            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public String getUser() {
                return user;
            }

            public void setUser(String user) {
                this.user = user;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getOffer() {
                return offer;
            }

            public void setOffer(String offer) {
                this.offer = offer;
            }
        }

        public static class QuestionsBean {

            private String user;
            private String image;
            private String question;
            private String dateTime;
            private String user_id;
            private String question_id;
            private int block_status;
            private String login_user_image;
            private int can_reply;
            private int my_product;
            private List<SubquestionBean> subquestion;
            public int getBlock_status() {
                return block_status;
            }

            public String getLogin_user_image() {
                return login_user_image;
            }

            public void setLogin_user_image(String login_user_image) {
                this.login_user_image = login_user_image;
            }

            public int getCan_reply() {
                return can_reply;
            }

            public void setCan_reply(int can_reply) {
                this.can_reply = can_reply;
            }

            public int getMy_product() {
                return my_product;
            }

            public void setMy_product(int my_product) {
                this.my_product = my_product;
            }

            public void setBlock_status(int block_status) {
                this.block_status = block_status;
            }
            public String getQuestion_id() {
                return question_id;
            }

            public void setQuestion_id(String question_id) {
                this.question_id = question_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getUser() {
                return user;
            }

            public void setUser(String user) {
                this.user = user;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public String getDateTime() {
                return dateTime;
            }

            public void setDateTime(String dateTime) {
                this.dateTime = dateTime;
            }

            public List<SubquestionBean> getSubquestion() {
                return subquestion;
            }

            public void setSubquestion(List<SubquestionBean> subquestion) {
                this.subquestion = subquestion;
            }

            public static class SubquestionBean {

                private int question_id;
                private int user_id;
                private String user;
                private String image;
                private String question;
                private String dateTime;

                public int getQuestion_id() {
                    return question_id;
                }

                public void setQuestion_id(int question_id) {
                    this.question_id = question_id;
                }

                public int getUser_id() {
                    return user_id;
                }

                public void setUser_id(int user_id) {
                    this.user_id = user_id;
                }

                public String getUser() {
                    return user;
                }

                public void setUser(String user) {
                    this.user = user;
                }

                public String getImage() {
                    return image;
                }

                public void setImage(String image) {
                    this.image = image;
                }

                public String getQuestion() {
                    return question;
                }

                public void setQuestion(String question) {
                    this.question = question;
                }

                public String getDateTime() {
                    return dateTime;
                }

                public void setDateTime(String dateTime) {
                    this.dateTime = dateTime;
                }
            }
        }
    }
}

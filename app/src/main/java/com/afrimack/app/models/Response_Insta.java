package com.afrimack.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Response_Insta {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("firebase_UID")
@Expose
private Object firebaseUID;
@SerializedName("access_level_id")
@Expose
private Integer accessLevelId;
@SerializedName("username")
@Expose
private String username;
@SerializedName("password")
@Expose
private String password;
@SerializedName("raw_password")
@Expose
private Object rawPassword;
@SerializedName("gender")
@Expose
private String gender;
@SerializedName("first_name")
@Expose
private String firstName;
@SerializedName("last_name")
@Expose
private String lastName;
@SerializedName("email")
@Expose
private String email;
@SerializedName("country")
@Expose
private String country;
@SerializedName("phone_number")
@Expose
private Object phoneNumber;
@SerializedName("image")
@Expose
private String image;
@SerializedName("dob")
@Expose
private String dob;
@SerializedName("profile_type")
@Expose
private String profileType;
@SerializedName("single_registered")
@Expose
private String singleRegistered;
@SerializedName("interests")
@Expose
private String interests;
@SerializedName("looking_for")
@Expose
private String lookingFor;
@SerializedName("about_me")
@Expose
private String aboutMe;
@SerializedName("latitude")
@Expose
private String latitude;
@SerializedName("longitude")
@Expose
private String longitude;
@SerializedName("address")
@Expose
private String address;
@SerializedName("fb_id")
@Expose
private Object fbId;
@SerializedName("insta_id")
@Expose
private String instaId;
@SerializedName("fb_access_token")
@Expose
private Object fbAccessToken;
@SerializedName("google_id")
@Expose
private Object googleId;
@SerializedName("no_verified")
@Expose
private String noVerified;
@SerializedName("email_verified")
@Expose
private String emailVerified;
@SerializedName("email_notification")
@Expose
private String emailNotification;
@SerializedName("notification_sound")
@Expose
private String notificationSound;
@SerializedName("email_chat")
@Expose
private String emailChat;
@SerializedName("newsletter")
@Expose
private String newsletter;
@SerializedName("device_type")
@Expose
private String deviceType;
@SerializedName("device_token")
@Expose
private String deviceToken;
@SerializedName("otp")
@Expose
private Object otp;
@SerializedName("last_login")
@Expose
private String lastLogin;
@SerializedName("is_logged")
@Expose
private Integer isLogged;
@SerializedName("enabled")
@Expose
private String enabled;
@SerializedName("is_deleted")
@Expose
private String isDeleted;
@SerializedName("random_code")
@Expose
private Object randomCode;
@SerializedName("alert_limit")
@Expose
private Integer alertLimit;
@SerializedName("profile_date")
@Expose
private String profileDate;
@SerializedName("profile_update")
@Expose
private String profileUpdate;
@SerializedName("created")
@Expose
private String created;
@SerializedName("modified")
@Expose
private String modified;
@SerializedName("unread_count")
@Expose
private Integer unreadCount;
@SerializedName("age")
@Expose
private String age;
@SerializedName("full_name")
@Expose
private String fullName;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Object getFirebaseUID() {
return firebaseUID;
}

public void setFirebaseUID(Object firebaseUID) {
this.firebaseUID = firebaseUID;
}

public Integer getAccessLevelId() {
return accessLevelId;
}

public void setAccessLevelId(Integer accessLevelId) {
this.accessLevelId = accessLevelId;
}

public String getUsername() {
return username;
}

public void setUsername(String username) {
this.username = username;
}

public String getPassword() {
return password;
}

public void setPassword(String password) {
this.password = password;
}

public Object getRawPassword() {
return rawPassword;
}

public void setRawPassword(Object rawPassword) {
this.rawPassword = rawPassword;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getCountry() {
return country;
}

public void setCountry(String country) {
this.country = country;
}

public Object getPhoneNumber() {
return phoneNumber;
}

public void setPhoneNumber(Object phoneNumber) {
this.phoneNumber = phoneNumber;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

public String getDob() {
return dob;
}

public void setDob(String dob) {
this.dob = dob;
}

public String getProfileType() {
return profileType;
}

public void setProfileType(String profileType) {
this.profileType = profileType;
}

public String getSingleRegistered() {
return singleRegistered;
}

public void setSingleRegistered(String singleRegistered) {
this.singleRegistered = singleRegistered;
}

public String getInterests() {
return interests;
}

public void setInterests(String interests) {
this.interests = interests;
}

public String getLookingFor() {
return lookingFor;
}

public void setLookingFor(String lookingFor) {
this.lookingFor = lookingFor;
}

public String getAboutMe() {
return aboutMe;
}

public void setAboutMe(String aboutMe) {
this.aboutMe = aboutMe;
}

public String getLatitude() {
return latitude;
}

public void setLatitude(String latitude) {
this.latitude = latitude;
}

public String getLongitude() {
return longitude;
}

public void setLongitude(String longitude) {
this.longitude = longitude;
}

public String getAddress() {
return address;
}

public void setAddress(String address) {
this.address = address;
}

public Object getFbId() {
return fbId;
}

public void setFbId(Object fbId) {
this.fbId = fbId;
}

public String getInstaId() {
return instaId;
}

public void setInstaId(String instaId) {
this.instaId = instaId;
}

public Object getFbAccessToken() {
return fbAccessToken;
}

public void setFbAccessToken(Object fbAccessToken) {
this.fbAccessToken = fbAccessToken;
}

public Object getGoogleId() {
return googleId;
}

public void setGoogleId(Object googleId) {
this.googleId = googleId;
}

public String getNoVerified() {
return noVerified;
}

public void setNoVerified(String noVerified) {
this.noVerified = noVerified;
}

public String getEmailVerified() {
return emailVerified;
}

public void setEmailVerified(String emailVerified) {
this.emailVerified = emailVerified;
}

public String getEmailNotification() {
return emailNotification;
}

public void setEmailNotification(String emailNotification) {
this.emailNotification = emailNotification;
}

public String getNotificationSound() {
return notificationSound;
}

public void setNotificationSound(String notificationSound) {
this.notificationSound = notificationSound;
}

public String getEmailChat() {
return emailChat;
}

public void setEmailChat(String emailChat) {
this.emailChat = emailChat;
}

public String getNewsletter() {
return newsletter;
}

public void setNewsletter(String newsletter) {
this.newsletter = newsletter;
}

public String getDeviceType() {
return deviceType;
}

public void setDeviceType(String deviceType) {
this.deviceType = deviceType;
}

public String getDeviceToken() {
return deviceToken;
}

public void setDeviceToken(String deviceToken) {
this.deviceToken = deviceToken;
}

public Object getOtp() {
return otp;
}

public void setOtp(Object otp) {
this.otp = otp;
}

public String getLastLogin() {
return lastLogin;
}

public void setLastLogin(String lastLogin) {
this.lastLogin = lastLogin;
}

public Integer getIsLogged() {
return isLogged;
}

public void setIsLogged(Integer isLogged) {
this.isLogged = isLogged;
}

public String getEnabled() {
return enabled;
}

public void setEnabled(String enabled) {
this.enabled = enabled;
}

public String getIsDeleted() {
return isDeleted;
}

public void setIsDeleted(String isDeleted) {
this.isDeleted = isDeleted;
}

public Object getRandomCode() {
return randomCode;
}

public void setRandomCode(Object randomCode) {
this.randomCode = randomCode;
}

public Integer getAlertLimit() {
return alertLimit;
}

public void setAlertLimit(Integer alertLimit) {
this.alertLimit = alertLimit;
}

public String getProfileDate() {
return profileDate;
}

public void setProfileDate(String profileDate) {
this.profileDate = profileDate;
}

public String getProfileUpdate() {
return profileUpdate;
}

public void setProfileUpdate(String profileUpdate) {
this.profileUpdate = profileUpdate;
}

public String getCreated() {
return created;
}

public void setCreated(String created) {
this.created = created;
}

public String getModified() {
return modified;
}

public void setModified(String modified) {
this.modified = modified;
}

public Integer getUnreadCount() {
return unreadCount;
}

public void setUnreadCount(Integer unreadCount) {
this.unreadCount = unreadCount;
}

public String getAge() {
return age;
}

public void setAge(String age) {
this.age = age;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

}
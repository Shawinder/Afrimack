package com.afrimack.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OtherUserResponseModel {


    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int rating;
        private int rating_count;
        private int selling;
        private int purchase;
        private String user_id;
        private String name;
        private String image;
        private String since;
        private String follow_user;
        private String profile_type;

        public String getFbId() {
            return fbId;
        }

        public void setFbId(String fbId) {
            this.fbId = fbId;
        }

        public Object getGoogleId() {
            return googleId;
        }

        public void setGoogleId(Object googleId) {
            this.googleId = googleId;
        }

        private String fbId;
        private Object googleId;
        private String single_registered;
        private String firebase_UID;
        @SerializedName("php_block_status")
        @Expose
        private int blockedStatus;

        public int getBlockedStatus() {
            return blockedStatus;
        }

        public void setBlockedStatus(int blockedStatus) {
            this.blockedStatus = blockedStatus;
        }

        public String getFirebase_UID() {
            return firebase_UID;
        }

        public void setFirebase_UID(String firebase_UID) {
            this.firebase_UID = firebase_UID;
        }

        public String getSingle_registered() {
            return single_registered;
        }

        public void setSingle_registered(String single_registered) {
            this.single_registered = single_registered;
        }

        private List<SellingArrBean> selling_arr;

        public String getProfile_type() {
            return profile_type;
        }

        public void setProfile_type(String profile_type) {
            this.profile_type = profile_type;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public int getRating_count() {
            return rating_count;
        }

        public void setRating_count(int rating_count) {
            this.rating_count = rating_count;
        }

        public int getSelling() {
            return selling;
        }

        public void setSelling(int selling) {
            this.selling = selling;
        }

        public int getPurchase() {
            return purchase;
        }

        public void setPurchase(int purchase) {
            this.purchase = purchase;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getSince() {
            return since;
        }

        public void setSince(String since) {
            this.since = since;
        }

        public String getFollow_user() {
            return follow_user;
        }

        public void setFollow_user(String follow_user) {
            this.follow_user = follow_user;
        }

        public List<SellingArrBean> getSelling_arr() {
            return selling_arr;
        }

        public void setSelling_arr(List<SellingArrBean> selling_arr) {
            this.selling_arr = selling_arr;
        }

        public static class SellingArrBean {

            private int post_id;
            private String title;
            private String post_status;
            private String price;
            private String image;
            private String dateTime;
            private String premium;

            public String getPremium() {
                return premium;
            }

            public void setPremium(String premium) {
                this.premium = premium;
            }

            public int getPost_id() {
                return post_id;
            }

            public void setPost_id(int post_id) {
                this.post_id = post_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getPost_status() {
                return post_status;
            }

            public void setPost_status(String post_status) {
                this.post_status = post_status;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getDateTime() {
                return dateTime;
            }

            public void setDateTime(String dateTime) {
                this.dateTime = dateTime;
            }
        }
    }
}

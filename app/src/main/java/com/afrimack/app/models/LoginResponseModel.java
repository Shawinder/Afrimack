package com.afrimack.app.models;


public class LoginResponseModel {



    private boolean error;
    private int code;
    private String message;
    private String emailverfied;
    private String user_id;
    private ResponseBean response;


    public String getEmailverfied() {
        return emailverfied;
    }

    public void setEmailverfied(String emailverfied) {
        this.emailverfied = emailverfied;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int id;
        private int access_level_id;
        private Object username;
        private Object gender;
        private String first_name;
        private String last_name;
        private String email;
        private Object country_id;
        private Object phone_number;
        private String image;
        private Object dob;
        private String profile_type;
        private String single_registered;
        private Object looking_for;
        private Object about_me;
        private Object fb_id;
        private Object google_id;
        private String no_verified;
        private String email_notification;
        private String push_notifcation;
        private String device_type;
        private String device_token;
        private Object otp;
        private Object last_login;
        private int is_logged;
        private String enabled;
        private String is_deleted;
        private String created;
        private String modified;
        private int unread_count;
        private String firebase_UID;

        public String getFirebase_UID() {
            return firebase_UID;
        }

        public void setFirebase_UID(String firebase_UID) {
            this.firebase_UID = firebase_UID;
        }

        public int getUnread_count() {
            return unread_count;
        }

        public void setUnread_count(int unread_count) {
            this.unread_count = unread_count;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAccess_level_id() {
            return access_level_id;
        }

        public void setAccess_level_id(int access_level_id) {
            this.access_level_id = access_level_id;
        }

        public Object getUsername() {
            return username;
        }

        public void setUsername(Object username) {
            this.username = username;
        }

        public Object getGender() {
            return gender;
        }

        public void setGender(Object gender) {
            this.gender = gender;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getCountry_id() {
            return country_id;
        }

        public void setCountry_id(Object country_id) {
            this.country_id = country_id;
        }

        public Object getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(Object phone_number) {
            this.phone_number = phone_number;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getDob() {
            return dob;
        }

        public void setDob(Object dob) {
            this.dob = dob;
        }

        public String getProfile_type() {
            return profile_type;
        }

        public void setProfile_type(String profile_type) {
            this.profile_type = profile_type;
        }

        public String getSingle_registered() {
            return single_registered;
        }

        public void setSingle_registered(String single_registered) {
            this.single_registered = single_registered;
        }

        public Object getLooking_for() {
            return looking_for;
        }

        public void setLooking_for(Object looking_for) {
            this.looking_for = looking_for;
        }

        public Object getAbout_me() {
            return about_me;
        }

        public void setAbout_me(Object about_me) {
            this.about_me = about_me;
        }

        public Object getFb_id() {
            return fb_id;
        }

        public void setFb_id(Object fb_id) {
            this.fb_id = fb_id;
        }

        public Object getGoogle_id() {
            return google_id;
        }

        public void setGoogle_id(Object google_id) {
            this.google_id = google_id;
        }

        public String getNo_verified() {
            return no_verified;
        }

        public void setNo_verified(String no_verified) {
            this.no_verified = no_verified;
        }

        public String getEmail_notification() {
            return email_notification;
        }

        public void setEmail_notification(String email_notification) {
            this.email_notification = email_notification;
        }

        public String getPush_notifcation() {
            return push_notifcation;
        }

        public void setPush_notifcation(String push_notifcation) {
            this.push_notifcation = push_notifcation;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public Object getOtp() {
            return otp;
        }

        public void setOtp(Object otp) {
            this.otp = otp;
        }

        public Object getLast_login() {
            return last_login;
        }

        public void setLast_login(Object last_login) {
            this.last_login = last_login;
        }

        public int getIs_logged() {
            return is_logged;
        }

        public void setIs_logged(int is_logged) {
            this.is_logged = is_logged;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getModified() {
            return modified;
        }

        public void setModified(String modified) {
            this.modified = modified;
        }
    }
}

package com.afrimack.app.models;


public class SubQuation {

    private String postion;
    private String subqution;
    private String qution_id;

    public String getPostion() {
        return postion;
    }

    public void setPostion(String postion) {
        this.postion = postion;
    }

    public String getSubqution() {
        return subqution;
    }

    public void setSubqution(String subqution) {
        this.subqution = subqution;
    }

    public String getQution_id() {
        return qution_id;
    }

    public void setQution_id(String qution_id) {
        this.qution_id = qution_id;
    }
}

package com.afrimack.app.models;

import java.util.List;

public class SwapResponseModel {



    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int my_product;
        private String currency_symbol;
        private String post_price;
        private String title;
        private String post_image;
        private String last_update;
        private String post_status;
        private String post_user_name;
        private String post_user_image;
        private String offer_user_image;
        private String offer_user_name;
        private String rating;
        private String last_status;
        private int offer_count;
        private List<ChatBean> chat;
        private List<OffersBean> offers;
        private List<PostForSwapBean> post_for_swap;

        public int getMy_product() {
            return my_product;
        }

        public void setMy_product(int my_product) {
            this.my_product = my_product;
        }

        public String getCurrency_symbol() {
            return currency_symbol;
        }

        public void setCurrency_symbol(String currency_symbol) {
            this.currency_symbol = currency_symbol;
        }

        public String getPost_price() {
            return post_price;
        }

        public void setPost_price(String post_price) {
            this.post_price = post_price;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPost_image() {
            return post_image;
        }

        public void setPost_image(String post_image) {
            this.post_image = post_image;
        }

        public String getLast_update() {
            return last_update;
        }

        public void setLast_update(String last_update) {
            this.last_update = last_update;
        }

        public String getPost_status() {
            return post_status;
        }

        public void setPost_status(String post_status) {
            this.post_status = post_status;
        }

        public String getPost_user_name() {
            return post_user_name;
        }

        public void setPost_user_name(String post_user_name) {
            this.post_user_name = post_user_name;
        }

        public String getPost_user_image() {
            return post_user_image;
        }

        public void setPost_user_image(String post_user_image) {
            this.post_user_image = post_user_image;
        }

        public String getOffer_user_image() {
            return offer_user_image;
        }

        public void setOffer_user_image(String offer_user_image) {
            this.offer_user_image = offer_user_image;
        }

        public String getOffer_user_name() {
            return offer_user_name;
        }

        public void setOffer_user_name(String offer_user_name) {
            this.offer_user_name = offer_user_name;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getLast_status() {
            return last_status;
        }

        public void setLast_status(String last_status) {
            this.last_status = last_status;
        }

        public int getOffer_count() {
            return offer_count;
        }

        public void setOffer_count(int offer_count) {
            this.offer_count = offer_count;
        }

        public List<ChatBean> getChat() {
            return chat;
        }

        public void setChat(List<ChatBean> chat) {
            this.chat = chat;
        }

        public List<OffersBean> getOffers() {
            return offers;
        }

        public void setOffers(List<OffersBean> offers) {
            this.offers = offers;
        }

        public List<PostForSwapBean> getPost_for_swap() {
            return post_for_swap;
        }

        public void setPost_for_swap(List<PostForSwapBean> post_for_swap) {
            this.post_for_swap = post_for_swap;
        }

        public static class ChatBean {

            private String image;
            private String user;
            private String message;
            private String date;
            private int user_id;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getUser() {
                return user;
            }

            public void setUser(String user) {
                this.user = user;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }
        }

        public static class OffersBean {

            private String image;
            private String date;
            private int user_id;
            private String title;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }

        public static class PostForSwapBean {

            private int id;
            private String title;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }
        }
    }
}

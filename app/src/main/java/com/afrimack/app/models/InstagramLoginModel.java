package com.afrimack.app.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstagramLoginModel {

@SerializedName("error")
@Expose
private Boolean error;
@SerializedName("code")
@Expose
private Integer code;
@SerializedName("message")
@Expose
private String message;
@SerializedName("response")
@Expose
private Response_Insta response;
@SerializedName("user_id")
@Expose
private Integer userId;
@SerializedName("emailverfied")
@Expose
private String emailverfied;

public Boolean getError() {
return error;
}

public void setError(Boolean error) {
this.error = error;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public Response_Insta getResponse() {
return response;
}

public void setResponse(Response_Insta response) {
this.response = response;
}

public Integer getUserId() {
return userId;
}

public void setUserId(Integer userId) {
this.userId = userId;
}

public String getEmailverfied() {
return emailverfied;
}

public void setEmailverfied(String emailverfied) {
this.emailverfied = emailverfied;
}

}


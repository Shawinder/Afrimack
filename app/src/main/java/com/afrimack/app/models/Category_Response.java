package com.afrimack.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Category_Response {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("category_name")
@Expose
private String categoryName;
@SerializedName("image")
@Expose
private String image;
@SerializedName("subcategory")
@Expose
private List<Subcategory> subcategory = null;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getCategoryName() {
return categoryName;
}

public void setCategoryName(String categoryName) {
this.categoryName = categoryName;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

public List<Subcategory> getSubcategory() {
return subcategory;
}

public void setSubcategory(List<Subcategory> subcategory) {
this.subcategory = subcategory;
}

}

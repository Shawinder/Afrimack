package com.afrimack.app.models;

public class SingleProfileModel {


    private int code;
    private boolean error;
    private String message;
    private String single_registered;
    private ResponseBean response;

    public String getSingle_registered() {
        return single_registered;
    }

    public void setSingle_registered(String single_registered) {
        this.single_registered = single_registered;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int id;
        private String looking_for;
        private String about_me;
        private String dob;
        private String gender;
        private String image;
        private String single_registered;
        private String interests;

        public String getInterests() {
            return interests;
        }

        public void setInterests(String interests) {
            this.interests = interests;
        }

        public String getSingle_registered() {
            return single_registered;
        }

        public void setSingle_registered(String single_registered) {
            this.single_registered = single_registered;
        }

        public String getCountry_id() {
            return country_id;
        }

        public void setCountry_id(String country_id) {
            this.country_id = country_id;
        }

        private String country_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getLooking_for() {
            return looking_for;
        }

        public void setLooking_for(String looking_for) {
            this.looking_for = looking_for;
        }

        public String getAbout_me() {
            return about_me;
        }

        public void setAbout_me(String about_me) {
            this.about_me = about_me;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}

package com.afrimack.app.models;

public class ProfileStatusResponseModel {



    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private int id;
        private String image;
        private String profile_type;
        private String dob;
        private String gender;
        private String email;
        private String phone_number;
        private String fb_id;
        private String google_id;
        private String no_verified;
        private PercentageBean percentage;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getProfile_type() {
            return profile_type;
        }

        public void setProfile_type(String profile_type) {
            this.profile_type = profile_type;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getFb_id() {
            return fb_id;
        }

        public void setFb_id(String fb_id) {
            this.fb_id = fb_id;
        }

        public String getGoogle_id() {
            return google_id;
        }

        public void setGoogle_id(String google_id) {
            this.google_id = google_id;
        }

        public String getNo_verified() {
            return no_verified;
        }

        public void setNo_verified(String no_verified) {
            this.no_verified = no_verified;
        }
        public PercentageBean getPercentage() {
            return percentage;
        }

        public void setPercentage(PercentageBean percentage) {
            this.percentage = percentage;
        }

        public static class PercentageBean {

            private int image;
            private int dob;
            private int social;
            private int email;
            private int gender;
            private int profile_type;
            private int completion;

            public int getImage() {
                return image;
            }

            public void setImage(int image) {
                this.image = image;
            }

            public int getDob() {
                return dob;
            }

            public void setDob(int dob) {
                this.dob = dob;
            }

            public int getSocial() {
                return social;
            }

            public void setSocial(int social) {
                this.social = social;
            }

            public int getEmail() {
                return email;
            }

            public void setEmail(int email) {
                this.email = email;
            }

            public int getGender() {
                return gender;
            }

            public void setGender(int gender) {
                this.gender = gender;
            }

            public int getProfile_type() {
                return profile_type;
            }

            public void setProfile_type(int profile_type) {
                this.profile_type = profile_type;
            }

            public int getCompletion() {
                return completion;
            }

            public void setCompletion(int completion) {
                this.completion = completion;
            }
        }
    }
}

package com.afrimack.app.models;

import java.util.List;


public class ChatHistoryModel {


    private int code;
    private boolean error;
    private List<MessageBean> message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<MessageBean> getMessage() {
        return message;
    }

    public void setMessage(List<MessageBean> message) {
        this.message = message;
    }

    public static class MessageBean {

        private String id;
        private String messid;
        private String is_received;
        private String dateTime;
        private String fromuser;
        private String touser;
        private String mess1;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMessid() {
            return messid;
        }

        public void setMessid(String messid) {
            this.messid = messid;
        }

        public String getIs_received() {
            return is_received;
        }

        public void setIs_received(String is_received) {
            this.is_received = is_received;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        public String getFromuser() {
            return fromuser;
        }

        public void setFromuser(String fromuser) {
            this.fromuser = fromuser;
        }

        public String getTouser() {
            return touser;
        }

        public void setTouser(String touser) {
            this.touser = touser;
        }

        public String getMess1() {
            return mess1;
        }

        public void setMess1(String mess1) {
            this.mess1 = mess1;
        }
    }
}

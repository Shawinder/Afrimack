package com.afrimack.app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SingleUserResponseModel {



    private int code;
    private boolean error;
    private String message;
    private ResponseBean response;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseBean getResponse() {
        return response;
    }

    public void setResponse(ResponseBean response) {
        this.response = response;
    }

    public static class ResponseBean {

        private SelectedBean selected;
        private List<UsersBean> users;

        public SelectedBean getSelected() {
            return selected;
        }

        public void setSelected(SelectedBean selected) {
            this.selected = selected;
        }

        public List<UsersBean> getUsers() {
            return users;
        }

        public void setUsers(List<UsersBean> users) {
            this.users = users;
        }

        public static class SelectedBean {

            private String own_country;
            private String Male;
            private String Female;
            @SerializedName("min-age")
            private String minage;
            @SerializedName("max-age")
            private String maxage;

            public String getOwn_country() {
                return own_country;
            }

            public void setOwn_country(String own_country) {
                this.own_country = own_country;
            }

            public String getMale() {
                return Male;
            }

            public void setMale(String Male) {
                this.Male = Male;
            }

            public String getFemale() {
                return Female;
            }

            public void setFemale(String Female) {
                this.Female = Female;
            }

            public String getMinage() {
                return minage;
            }

            public void setMinage(String minage) {
                this.minage = minage;
            }

            public String getMaxage() {
                return maxage;
            }

            public void setMaxage(String maxage) {
                this.maxage = maxage;
            }
        }

        public static class UsersBean {

            private int id;
            private String image;
            private String gender;
            private String first_name;
            private String last_name;
            private String country_name;
            private String age;
            private String short_name;

            public String getShort_name() {
                return short_name;
            }

            public void setShort_name(String short_name) {
                this.short_name = short_name;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getCountry_name() {
                return country_name;
            }

            public void setCountry_name(String country_name) {
                this.country_name = country_name;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }
        }
    }
}

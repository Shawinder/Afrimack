package com.afrimack.app.services;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;

/**
 * Created by ubuntu on 21/5/16.
 */
public interface LocationServiceListner {
    void userLocationChange(Location location);

    void googleApiclientConnecte(GoogleApiClient googleApiClient, LocationRequest locationRequest);
}

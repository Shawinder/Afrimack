package com.afrimack.app.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import com.afrimack.app.appbeans.User;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.utils.CommonUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class ServiceUtils {


    public static void updateLoginUserStatus(Context context, String online) {
        if (isNetworkConnected(context)) {
            String uid = Config.getLoginUSERUID(context);
            if (!uid.equals("")) {
                FirebaseDatabase.getInstance().getReference().child("users/" + uid + "/online").setValue(online);
            }
        }
    }


    public static boolean isNetworkConnected(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null;
        } catch (Exception e) {
            return true;
        }
    }
}

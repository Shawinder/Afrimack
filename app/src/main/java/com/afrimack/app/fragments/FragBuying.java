package com.afrimack.app.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActMyAfrimack;
import com.afrimack.app.adapters.BuyingAdapter;
import com.afrimack.app.adapters.SellBuyingAdapter;
import com.afrimack.app.adapters.SellingBuyingAdapter;
import com.afrimack.app.adapters.SoldAdapter;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.BuyingResponseModel;
import com.afrimack.app.models.DeletPostResponseModel;
import com.afrimack.app.rest.RestClient;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragBuying extends Fragment implements View.OnClickListener {
    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel, tv_buyinf_total;
    private TextView tv_all_by, tv_buying, tv_bought, tv_watching;
    private LinearLayout llNewsContainor_buy;
    private Button btDiscover_buy;
    private RecyclerView rvBuying;
    private BuyingAdapter mAdapter;
    private List<BuyingResponseModel.ResponseBean.PostsBean> buyListl = new ArrayList<>();
    private LinearLayout llProfileHeader;
    private ActMyAfrimack activity;
    private String user_id, type = "All";
    private int page_no = 1;
    private LinearLayoutManager mLayoutManager;
    View.OnClickListener onClickListener;
    int pos;
    View view;
    private Context context;
    private String TAG = "Buying Fragment";

    public FragBuying() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_frag_buying, container, false);
            context = getActivity();
            user_id = Config.getUserId(getActivity());
            activity = (ActMyAfrimack) getActivity();
            tv_titel = activity.getTv_titel();
            tv_all_by = (TextView) view.findViewById(R.id.tv_all_by);
            tv_buying = (TextView) view.findViewById(R.id.tv_buying);
            tv_bought = (TextView) view.findViewById(R.id.tv_bought);
            tv_watching = (TextView) view.findViewById(R.id.tv_watching);
            tv_buyinf_total = (TextView) view.findViewById(R.id.tv_buyinf_total);
            btDiscover_buy = (Button) view.findViewById(R.id.btDiscover_buy);
            llNewsContainor_buy = (LinearLayout) view.findViewById(R.id.llNewsContainor_buy);
            rvBuying = (RecyclerView) view.findViewById(R.id.rvBuying);
            btDiscover_buy.setOnClickListener(this);
            tv_all_by.setOnClickListener(this);
            tv_buying.setOnClickListener(this);
            tv_bought.setOnClickListener(this);
            tv_watching.setOnClickListener(this);
            mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            rvBuying.setLayoutManager(mLayoutManager);
            rvBuying.setVisibility(View.GONE);
            type = "all";
            hitApi("withoutClick");
            mAdapter = new BuyingAdapter(getActivity(), buyListl, this);
            rvBuying.setAdapter(mAdapter);
            rvBuying.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {

                    page_no = page_no + 1;
                    hitApi("withoutClick");
                }
            });
        }
        return view;
    }

    private void hitApi(final String fromWhere) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            if (fromWhere.equalsIgnoreCase("onClick")) {
                CommonUtils.getInstance().displayLoadingDialog(false, context);
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<BuyingResponseModel> call = apiInterface.Buying(user_id, type, String.valueOf(page_no));
            call.enqueue(new Callback<BuyingResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<BuyingResponseModel> call, @NonNull Response<BuyingResponseModel> response) {
                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        if (fromWhere.equalsIgnoreCase("onClick")) {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }
                        BuyingResponseModel modelbuy = response.body();
                        if (modelbuy.isError() == false) {
                            if (modelbuy.getResponse() != null) {
                                tv_buyinf_total.setText(modelbuy.getResponse().getCount() + " " + getResources().getString(R.string.item));
                                if (modelbuy.getResponse().getPosts().size() > 0) {
                                    llNewsContainor_buy.setVisibility(View.GONE);
                                    rvBuying.setVisibility(View.VISIBLE);
                                    buyListl.addAll(modelbuy.getResponse().getPosts());
                                    BuyingAdapter.buyingList = buyListl;
                                    mAdapter.notifyDataSetChanged();
                                } else {
                                    if (buyListl.size() == 0) {
                                        llNewsContainor_buy.setVisibility(View.VISIBLE);
                                        rvBuying.setVisibility(View.GONE);
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        if (fromWhere.equalsIgnoreCase("onClick")) {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<BuyingResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    if (fromWhere.equalsIgnoreCase("onClick")) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                    }
                }
            });
        } else {
            if (fromWhere.equalsIgnoreCase("onClick")) {
                CommonUtils.getInstance().dismissLoadingDialog();
            }
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btDiscover_buy) {
            getActivity().finish();
        }
        if (v == tv_all_by) {
            tv_all_by.setBackgroundColor(getActivity().getResources().getColor(R.color.color_yallow));
            tv_buying.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_bought.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_watching.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));

     /*       tv_all_by.setTextColor(getActivity().getResources().getColor(R.color.color_black));
            tv_buying.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_bought.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_watching.setTextColor(getActivity().getResources().getColor(R.color.color_white));*/


            llNewsContainor_buy.setVisibility(View.GONE);
            rvBuying.setVisibility(View.VISIBLE);
            type = "all";
            if (buyListl != null) {
                buyListl.clear();
            }
            page_no = 1;
            hitApi("onClick");
        }
        if (v == tv_buying) {
            tv_all_by.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_buying.setBackgroundColor(getActivity().getResources().getColor(R.color.color_yallow));
            tv_bought.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_watching.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));

            /*tv_all_by.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_buying.setTextColor(getActivity().getResources().getColor(R.color.color_black));
            tv_bought.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_watching.setTextColor(getActivity().getResources().getColor(R.color.color_white));*/


            llNewsContainor_buy.setVisibility(View.GONE);
            rvBuying.setVisibility(View.VISIBLE);
            type = "buying";
            if (buyListl != null) {
                buyListl.clear();
            }
            page_no = 1;
            hitApi("onClick");

        }
        if (v == tv_bought) {

            tv_all_by.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_buying.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_bought.setBackgroundColor(getActivity().getResources().getColor(R.color.color_yallow));
            tv_watching.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));

        /*    tv_all_by.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_buying.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_bought.setTextColor(getActivity().getResources().getColor(R.color.color_black));
            tv_watching.setTextColor(getActivity().getResources().getColor(R.color.color_white));*/


            llNewsContainor_buy.setVisibility(View.GONE);
            rvBuying.setVisibility(View.VISIBLE);
            type = "bought";
            if (buyListl != null) {
                buyListl.clear();
            }
            page_no = 1;
            hitApi("onClick");
        }
        if (v == tv_watching) {
            tv_all_by.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_buying.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_bought.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_watching.setBackgroundColor(getActivity().getResources().getColor(R.color.color_yallow));

         /*   tv_all_by.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_buying.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_bought.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_watching.setTextColor(getActivity().getResources().getColor(R.color.color_black));*/
            llNewsContainor_buy.setVisibility(View.GONE);
            rvBuying.setVisibility(View.VISIBLE);
            type = "watching";
            page_no = 1;
            if (buyListl != null) {
                buyListl.clear();
            }

            hitApi("onClick");
        }

        if (v.getId() == R.id.ivSellingItemClose) {
            pos = (int) v.getTag();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setMessage("Are you sure you want to delete this offer?");


            alertDialogBuilder.setPositiveButton("yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            hitPostDeleteApi(BuyingAdapter.buyingList.get(pos).getPost_id());

                        }
                    });

            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();


        }
    }

    private void hitPostDeleteApi(int postId) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<DeletPostResponseModel> call = apiInterface.DeleteBuying(user_id, String.valueOf(postId), type);
            call.enqueue(new Callback<DeletPostResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<DeletPostResponseModel> call, @NonNull Response<DeletPostResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        DeletPostResponseModel deletPostResponseModel = response.body();
                        Common.displayLongToast(getActivity(), deletPostResponseModel.getMessage());
                        if (deletPostResponseModel.isError() == false) {
                            BuyingAdapter.buyingList.clear();
                            page_no = 1;
                            hitApi("withoutClick");
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<DeletPostResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

}
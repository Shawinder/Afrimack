package com.afrimack.app.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityCategoryListDialog;
import com.afrimack.app.activities.ActivityChat;
import com.afrimack.app.activities.SellOffer;
import com.afrimack.app.activities.SplashLogin;
import com.afrimack.app.adapters.AllCatogeryAdapter;
import com.afrimack.app.adapters.CategoriesAdapter;
import com.afrimack.app.adapters.ChooseCategoriesListAdapter;
import com.afrimack.app.adapters.CustomSpinnerAdapter;
import com.afrimack.app.adapters.CustomSpinnerAdapterDuration;
import com.afrimack.app.adapters.CustomSpinnerAdapterHours;
import com.afrimack.app.adapters.NothingSelectedSpinnerAdapter;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.constants.GPSTracker;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.AddResponseModel;
import com.afrimack.app.models.CategoriesResponseModel;
import com.afrimack.app.models.CategoryList;
import com.afrimack.app.models.CurencyNameResponseModel;
import com.afrimack.app.models.PostEditModel;
import com.afrimack.app.models.Subcategory;
import com.afrimack.app.rest.RetrofitUtils;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.services.Locationservice;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.LocationSource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.ACTIVITY_SERVICE;
import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.afrimack.app.utils.AppConstants.BASE_URL;
import static com.afrimack.app.utils.Common.checkPermissions;
import static com.afrimack.app.utils.Common.displayErrorDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragSellRent extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener,
        LocationSource.OnLocationChangedListener {


    protected static final String TAG = "FragSellRent";
    ArrayAdapter<String> hoursAdapter;
    double latitude, longitude;
    List<Address> addresses;
    private EditText etSelling;
    private EditText etDescribe;
    private EditText etPrice;
    private EditText tvSellOn;
    // public static TextView tv_catoger;
    private ToggleButton tbSwapDeal;
    private ToggleButton tbShareOnFacebook;
    private Spinner spPrice;
    private Spinner spCategory;
    private Spinner spHour, sptype;
    private Button btSellRent, btUpdate;
    private String btnName = "";
    private LinearLayout lay_week;

    String sub_category_name="";
    String sub_category_id="";


    private int dayweekm = 1;
    //  private RestClient restClient;
    private ArrayList<CurencyNameResponseModel.ResponseBean> currency;
    private CurencyNameResponseModel curencyNameModel;
    private ArrayAdapter<String> currencyAdapter;
    private ArrayList<String> categories;
    private ArrayList<String> categories_id;
    private ArrayList<String> Sub_categories_list;
    private ArrayList<String> Sub_categories_list_Id;
    private CategoriesResponseModel categoriesModel;
    private ArrayAdapter<String> categoriesAdapter;
    private String country_id, title, description, price, post_type, user_id, addressfull = "Jaipur", lat, longi, swap_deal_title, hours = "", category_id, duration_type = "", duration_value = "";
    private String swap_deal = "N", share_fb = "N";
    private ArrayList<String> images;
    //private PopupWindow popupWindow;
    TextView tv_cteagr;
    private RecyclerView recy_caterer_list;
    private LinearLayoutManager mLayoutManager;
    private AllCatogeryAdapter allCatogery;
    private int pos;
    private Dialog dialog, dialogmessage;
    CategoryList categoryList;

    private ArrayList<String> imagesPathList;
    private GoogleApiClient mGoogleApiClient;
    private GPSTracker gpsTracker;
    private TextView tvSwapDeal;
    private TextView tvShareOnFacebookRent;

    private LocationManager manager;
    private boolean isDataRecieved = false;
    private AlertDialog gpsAlertDialog;
    private boolean isGpsDialogShowing = false;
    private String post_id;
    List<String> selestType;
    List<String> hour;
    private Context context;
    private LinearLayout linear_share;

    public static String image_delete = "";
    CustomSpinnerAdapterDuration customSpinnerAdapterDuration;
    String valueToSet="";

    public FragSellRent() {
    }

    public static FragSellRent newInstance(String btnName) {
        Bundle args = new Bundle();
        FragSellRent fragment = new FragSellRent();
        fragment.setArguments(args);
        fragment.btnName = btnName;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.frag_sell_rent, container, false);

        user_id = Config.getUserId(getActivity());
        post_id = Config.getPostId(getActivity());

        Log.e("POST_ID",post_id);

        context = getActivity();
        hour = new ArrayList<String>();
        try {
            lat = Config.getlatitude(getActivity());
            longi = Config.getlongitude(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        IntentFilter intentFilter_notify = new IntentFilter("android.intent.action.CATEGORY");
        getActivity().registerReceiver(myBroadcastReceiver_notify, intentFilter_notify);


        if (checkPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION}, 99)) {
        }

        init(rootview);
        hitCategoryDataApi();
        hitCurrencyDataApi();


        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (!post_id.equalsIgnoreCase("")) {
                        btSellRent.setVisibility(View.GONE);
                        btUpdate.setVisibility(View.VISIBLE);
                        calleditapi();
                    }
                }
            }, 500);


        } catch (Exception e) {
            e.printStackTrace();
        }

        Geocoder geocoder;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());


        etPrice.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                            if (getActivity().getCurrentFocus() != null) {
                                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                            }

                            return true;
                    }
                }
                return false;
            }
        });
        InputFilter filter = new InputFilter() {
            final int maxDigitsBeforeDecimalPoint = 20;
            final int maxDigitsAfterDecimalPoint = 2;

            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                StringBuilder builder = new StringBuilder(dest);
                builder.replace(dstart, dend, source
                        .subSequence(start, end).toString());
                if (!builder.toString().matches(
                        "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?"

                )) {
                    if (source.length() == 0)
                        return dest.subSequence(dstart, dend);
                    return "";
                }

                return null;

            }
        };

        etPrice.setFilters(new InputFilter[]{filter});
        return rootview;
    }

    private void hitCategoryDataApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {

            final ArrayList<ArrayList<String>> globalSubCategories = new ArrayList<>();
            final ArrayList<ArrayList<String>> globalSubCategories_Id = new ArrayList<>();

            CommonUtils.getSub_categories().clear();
            CommonUtils.getSub_categories_Id().clear();
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CategoryList> call = apiInterface.Categories();
            call.enqueue(new Callback<CategoryList>() {
                @Override
                public void onResponse(@NonNull Call<CategoryList> call, @NonNull Response<CategoryList> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        // categoriesModel = response.body();
                        categoryList = response.body();
                        globalSubCategories.clear();
                        globalSubCategories_Id.clear();

                        if (categoryList.getError() == false) {
                            putValuesInSpinner();
                            categories = new ArrayList<String>();
                            categories_id = new ArrayList<String>();


                            for (int i = 0; i < categoryList.getResponse().size(); i++) {
//                                Sub_categories_list.clear();
                                //    categories.add(categoriesModel.getResponse().get(i).getCategory_name());
                                categories.add(categoryList.getResponse().get(i).getCategoryName());
                                categories_id.add(categoryList.getResponse().get(i).getId().toString());
                                Log.e("CAT LIST:", categories + "");

                                if (categoryList.getResponse().get(i).getSubcategory().size() > 0) {
                                    Sub_categories_list = new ArrayList<String>();
                                    Sub_categories_list_Id = new ArrayList<String>();
                                    for (int j = 0; j < categoryList.getResponse().get(i).getSubcategory().size(); j++) {
                                        //   CommonUtils.getSub_categories().clear();
                                        Sub_categories_list.add(categoryList.getResponse().get(i).getSubcategory().get(j).getCategoryName());
                                        Sub_categories_list_Id.add(categoryList.getResponse().get(i).getSubcategory().get(j).getId().toString());
                                    }
                                    globalSubCategories.add(i, Sub_categories_list);
                                    globalSubCategories_Id.add(i, Sub_categories_list_Id);
                                } else {
                                    Sub_categories_list = new ArrayList<String>();
                                    Sub_categories_list_Id = new ArrayList<String>();
                                    globalSubCategories.add(i, Sub_categories_list);
                                    globalSubCategories_Id.add(i, Sub_categories_list_Id);
                                }
                                Log.e("SUBCAT LIST:", Sub_categories_list + "");

//                              for (int j=0;j<categoryList.getResponse().size();j++){
//                                    Sub_categories_list.add(categoryList.getResponse().get(i).getSubcategory().get(j).getCategoryName());
//                                    Log.e("SUBCAT LIST:",Sub_categories_list+"");
//                                    CommonUtils.setSub_categories(Sub_categories_list);
//                              }
                                CommonUtils.setCategories(categories);
                                CommonUtils.setCategory_id(categories_id);
                            }

                            CommonUtils.setSub_categories(globalSubCategories);
                            CommonUtils.setSub_categories_Id(globalSubCategories_Id);

                            Log.e("NEW_DATA: ", globalSubCategories_Id + "");
                            //   CommonUtils.setSub_categories(Sub_categories_list);
                            // startActivity(new Intent(getContext(),ActivityCategoryListDialog.class));
                           /* categoriesAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_layout_view, categories);
                            categoriesAdapter.setDropDownViewResource(R.layout.spinner_text_view);
                            spCategory.setAdapter(categoriesAdapter);*/

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CategoryList> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                }
            });


        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitCurrencyDataApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CurencyNameResponseModel> call = apiInterface.CurencyName();
            call.enqueue(new Callback<CurencyNameResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CurencyNameResponseModel> call, @NonNull Response<CurencyNameResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        curencyNameModel = response.body();
                        if (curencyNameModel.isError() == false) {
                            currency = new ArrayList<CurencyNameResponseModel.ResponseBean>();
                            for (int i = 0; i < curencyNameModel.getResponse().size(); i++) {
                                currency.add(curencyNameModel.getResponse().get(i));
//                                currency.add(curencyNameModel.getResponse().get(i).getCurrency_name().toString());
                            }
                            if (!currency.get(0).getCurrency_name().equalsIgnoreCase("Choose")) {
                                CurencyNameResponseModel.ResponseBean res = new CurencyNameResponseModel.ResponseBean();
                                res.setId(-1);
                                res.setCurrency_name("Choose");
                                currency.add(0, res);
                            }
                            CustomSpinnerAdapter customSpinnerAdapter = new CustomSpinnerAdapter(context, android.R.layout.simple_spinner_item, currency);
//                            currencyAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_layout_view, currency);
//                            currencyAdapter.setDropDownViewResource(R.layout.spinner_text_view);
                            //   NothingSelectedSpinnerAdapter adapter=new NothingSelectedSpinnerAdapter(currencyAdapter,R.layout.no_item_selected, getActivity());
                            spPrice.setAdapter(customSpinnerAdapter);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CurencyNameResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                }
            });


        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        if (CommonUtils.CategoryName.equalsIgnoreCase("")) {
            // tv_cteagr.setText(CommonUtils.CategoryName);
        } else {

        }

        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (!isDataRecieved) {
                isDataRecieved = true;
                if (!isMyServiceRunning()) {
                    getActivity().startService(new Intent(getActivity(), Locationservice.class));
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isDataRecieved = false;
        getActivity().stopService(new Intent(getActivity(), Locationservice.class));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (myBroadcastReceiver_notify != null) {
            getActivity().unregisterReceiver(myBroadcastReceiver_notify);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

        }
    }

    private void putValuesInSpinner() {

        //hours values
        selestType = new ArrayList<String>();
        selestType.add("Choose");
        selestType.add("Hour");
        selestType.add("Day");
        selestType.add("Week");
        selestType.add("Month");

        CustomSpinnerAdapterHours customSpinnerAdapterHours = new CustomSpinnerAdapterHours(context, android.R.layout.simple_spinner_item, selestType);
        sptype.setAdapter(customSpinnerAdapterHours);

       /* ArrayAdapter<String> selectTypeAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_layout_view, selestType);
        selectTypeAdapter.setDropDownViewResource(R.layout.spinner_text_view);
        sptype.setAdapter(selectTypeAdapter);*/
    }

    private void init(View rootview) {

        tv_cteagr = (TextView) rootview.findViewById(R.id.tv_cteagr);
        tv_cteagr.setOnClickListener(this);
        linear_share = (LinearLayout) rootview.findViewById(R.id.linear_share);
//        if (Config.getLoginType(getActivity()).equalsIgnoreCase("fb")) {
//            linear_share.setVisibility(View.VISIBLE);
//        } else {
        linear_share.setVisibility(View.GONE);
//        }
        //set views
        etSelling = (EditText) rootview.findViewById(R.id.etSelling);
        etDescribe = (EditText) rootview.findViewById(R.id.etDescibe);
        etPrice = (EditText) rootview.findViewById(R.id.etPrice);
        tvSellOn = (EditText) rootview.findViewById(R.id.tvSellOn);
        tvSwapDeal = (TextView) rootview.findViewById(R.id.tvSwapDeal);
        tvShareOnFacebookRent = (TextView) rootview.findViewById(R.id.tvShareOnFacebookRent);
        tbSwapDeal = (ToggleButton) rootview.findViewById(R.id.tbSwapDeal);
        tbShareOnFacebook = (ToggleButton) rootview.findViewById(R.id.tbShareOnFacebook);
        spPrice = (Spinner) rootview.findViewById(R.id.spPrice);
        spCategory = (Spinner) rootview.findViewById(R.id.spCategory);
        spHour = (Spinner) rootview.findViewById(R.id.spHour);
        sptype = (Spinner) rootview.findViewById(R.id.sptype);
        btSellRent = (Button) rootview.findViewById(R.id.btSellRent);
        btUpdate = (Button) rootview.findViewById(R.id.btUpdate);
        // tv_catoger = (TextView) rootview.findViewById(R.id.tv_catoger);

        lay_week = (LinearLayout) rootview.findViewById(R.id.lay_week);
        share_fb = "Y";
        //click task
        etSelling.setOnClickListener(this);
        etDescribe.setOnClickListener(this);
        etPrice.setOnClickListener(this);
        tbSwapDeal.setOnClickListener(this);
        tbShareOnFacebook.setOnClickListener(this);
        tbShareOnFacebook.setChecked(true);
        //   tv_catoger.setOnClickListener(this);

        btSellRent.setOnClickListener(this);
        btUpdate.setOnClickListener(this);
        // Spinner click listener
        spCategory.setOnItemSelectedListener(this);
        spPrice.setOnItemSelectedListener(this);
        spHour.setOnItemSelectedListener(this);
        sptype.setOnItemSelectedListener(this);

        etSelling.setHint(getResources().getString(R.string.what_you_are_rating));
        setDataToView();
    }

    private void setDataToView() {
        btSellRent.setText(btnName);
        if (btnName != null && btnName.equalsIgnoreCase(getActivity().getResources().getString(R.string.SELL_OFFER))) {
            lay_week.setVisibility(View.GONE);
            sptype.setVisibility(View.GONE);
            // sptype.setVisibility(View.GONE);
            etSelling.setHint(getResources().getString(R.string.what_you_are_selling));
            post_type = "S";
        } else {

            post_type = "R";

        }
    }

    @Override
    public void onClick(View v) {

        if (v == tv_cteagr) {


            startActivity(new Intent(getActivity(), ActivityCategoryListDialog.class));

   /*         dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.all_catogery_list);
            dialog.setCancelable(true);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            recy_caterer_list = (RecyclerView) dialog.findViewById(R.id.recy_caterer_list);
            mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            recy_caterer_list.setLayoutManager(mLayoutManager);
            if (categories != null && categories.size() > 0) {
                allCatogery = new AllCatogeryAdapter(getActivity(), categories, this);
                recy_caterer_list.setAdapter(allCatogery);
            }
            dialog.show();*/
        }
        //   if (v.getId() == R.id.catogety) {
        if (v.getId() == R.id.txtVwCategoryTitle) {
            //dialog.dismiss();
            pos = (int) (v.getTag());
            //category_id = String.valueOf(categoriesModel.getResponse().get(pos).getId());
            category_id = String.valueOf(categoryList.getResponse().get(pos).getId());
            //tv_cteagr.setText(String.valueOf(categoriesModel.getResponse().get(pos).getCategory_name()));
            tv_cteagr.setText(String.valueOf(categoryList.getResponse().get(pos).getCategoryName()));
        }

        if (v == tbSwapDeal) {
            if (getActivity().getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
            if (tbSwapDeal.isChecked()) {
                tvSellOn.setVisibility(View.VISIBLE);
                swap_deal = "Y";

                tvSwapDeal.setTextColor(getResources().getColor(R.color.color_black));
            } else {
                tvSellOn.setVisibility(View.GONE);
                swap_deal = "N";
                tvSwapDeal.setTextColor(getResources().getColor(R.color.color_white));
            }
        }
        if (v == tbShareOnFacebook) {
            if (tbShareOnFacebook.isChecked()) {
                share_fb = "Y";
                tvShareOnFacebookRent.setTextColor(getResources().getColor(R.color.color_black));
            } else {
                share_fb = "N";
                tvShareOnFacebookRent.setTextColor(getResources().getColor(R.color.color_white));
            }
        }
        if (v == btSellRent) {
            v.startAnimation(AppConstants.buttonClick);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                ShowGpsDialog();
                if (!isMyServiceRunning()) {
                    getActivity().startService(new Intent(getActivity(), Locationservice.class));
                }
            } else {
                removeGpsDialog();

                try {

                    lat = Config.getlatitude(getActivity());
                    longi = Config.getlongitude(getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                title = etSelling.getText().toString().trim();
                description = etDescribe.getText().toString().trim();
                price = etPrice.getText().toString().trim();
                swap_deal_title = tvSellOn.getText().toString().trim();

                String imagesize = String.valueOf(SellOffer.photos.size());

                String Value = CommonUtils.Id;

                if (SellOffer.photos != null && SellOffer.photos.size() >= 0 && title.length() == 0 && description.length() == 0 && price.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), getActivity());
                } else if (title.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), getActivity());
                } else if (description.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), getActivity());
                } else if (price.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), getActivity());
                } else if (SellOffer.photos.size() == 0) {
                    displayErrorDialog(getResources().getString(R.string.select_image_post), getActivity());
                } /*else if (category_id == null) {
                    displayErrorDialog(getResources().getString(R.string.invalid_category), getActivity());
                } */ else {
                    if (!(Config.getUserId(getActivity())).equalsIgnoreCase("null") && !(Config.getUserId(getActivity())).equalsIgnoreCase("")) {
                        if (ConnectionDetector.isNetAvail(getActivity())) {

                            if (hours.equalsIgnoreCase("Choose") || duration_value.equalsIgnoreCase("Choose")){
                                Common.displayErrorDialog(ConstantMain.HOURSALERT, getActivity());
                            }
                            else if (country_id.equalsIgnoreCase("-1")){
                                Common.displayErrorDialog("Choose Currency", getActivity());
                            }
                            else {
                                hitSellRentApi(user_id, post_type, title, description, lat, longi, addressfull,
                                        country_id, price, swap_deal, swap_deal_title, share_fb, hours, duration_type, duration_value, CommonUtils.Id, SellOffer.photos, post_id
                                , image_delete, "sellRent");
                            }
                        } else {
                            Common.displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, getActivity());
                        }
                    } else {
                        Intent intent = new Intent(getActivity(), SplashLogin.class);
                        startActivity(intent);
                    }
                }
            }
        }
        if (v == btUpdate) {
            v.startAnimation(AppConstants.buttonClick);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                ShowGpsDialog();
                if (!isMyServiceRunning()) {
                    getActivity().startService(new Intent(getActivity(), Locationservice.class));
                }
            } else {
                removeGpsDialog();
                try {
                    lat = Config.getlatitude(getActivity());
                    longi = Config.getlongitude(getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                title = etSelling.getText().toString().trim();
                description = etDescribe.getText().toString().trim();
                price = etPrice.getText().toString().trim();
                swap_deal_title = tvSellOn.getText().toString().trim();
                if (SellOffer.photos != null && SellOffer.photos.size() >= 0 && title.length() == 0 && description.length() == 0 && price.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), getActivity());
                } else if (title.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), getActivity());
                } else if (description.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), getActivity());
                } else if (price.length() == 0) {
                    displayErrorDialog(getResources().getString(R.string.required_field), getActivity());
                } else {
                    if (!(Config.getUserId(getActivity())).equalsIgnoreCase("null") && !(Config.getUserId(getActivity())).equalsIgnoreCase("")) {
                        if (ConnectionDetector.isNetAvail(getActivity())) {

                            if (hours.equalsIgnoreCase("Choose") || duration_value.equalsIgnoreCase("Choose")){
                                Common.displayErrorDialog(ConstantMain.HOURSALERT, getActivity());
                            }
                            else if (country_id.equalsIgnoreCase("-1")){
                                Common.displayErrorDialog("Choose Currency", getActivity());
                            }
                            else {

                                hitSellRentApi(user_id, post_type, title, description, lat, longi, addressfull,
                                        country_id, price, swap_deal, swap_deal_title, share_fb, hours, duration_type, duration_value, CommonUtils.Id, SellOffer.photos, post_id
                                        , image_delete, "update");
                            }
                          /*  delete_image*/
                        } else {
                            Common.displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, getActivity());
                        }
                    } else {
                        Intent intent = new Intent(getActivity(), SplashLogin.class);
                        startActivity(intent);
                    }
                }
            }
        }
    }

    private int count = -1;


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {

        Log.e("NEW_VALUE_CURRENCY", i + "");
//        TextView tv=(TextView)view;
        int position = adapterView.getSelectedItemPosition();

//        if (count == -1){
//            tv.setTextColor(getResources().getColor(R.color.color_white));
//        }
//
//        if (position == 0) {
//            count = position;
//        }
//        if (++count >1) {

        if (adapterView == spCategory) {
            String item = adapterView.getItemAtPosition(i).toString();
            // Showing selected spinner item
            // category_id = String.valueOf(categoriesModel.getResponse().get(i).getId());
            category_id = String.valueOf(categoryList.getResponse().get(i).getId());

            Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
        } else if (adapterView == spPrice) {


//               tv.setTextColor(getResources().getColor(R.color.color_black));
            //((TextView) adapterView.getChildAt(0)).setTextColor(Color.BLUE);

                country_id = String.valueOf(currency.get(i).getId());

//                country_id = String.valueOf(curencyNameModel.getResponse().get(i).getId());
        } else if (adapterView == sptype) {

            int totalCount = adapterView.getCount();
            Log.e("TOTAL_COUNT", totalCount + "");

            Log.e("INDEX", i + "" + adapterView.getId());
            // hours = adapterView.getItemAtPosition(i).toString();


                hours = selestType.get(i).toString();

            Log.e("HOURS_VALUE", hours);

            // String selestion = adapterView.getItemAtPosition(i).toString();
            String selestion = selestType.get(i).toString();
            if (selestion.equalsIgnoreCase("Hour")) {

                if (hour.size() > 0) {
                    hour.clear();
                }
                duration_type = "H";
                dayweekm = 24;
            } else if (selestion.equalsIgnoreCase("Day")) {
                if (hour.size() > 0) {
                    hour.clear();
                }
                duration_type = "D";
                dayweekm = 30;
            } else if (selestion.equalsIgnoreCase("Week")) {
                if (hour.size() > 0) {
                    hour.clear();
                }
                duration_type = "W";
                dayweekm = 4;
            } else if (selestion.equalsIgnoreCase("Month")) {
                if (hour.size() > 0) {
                    hour.clear();
                }
                duration_type = "M";
                dayweekm = 12;
            }
            hour.add("Choose");
            for (int k = 1; k <= dayweekm; k++) {
                hour.add(String.valueOf(k));
            }

             /*   if(!hour.get(0).equalsIgnoreCase("Choose")) {
                    hour = new ArrayList<String>();
                    // selestType.indexOf(-1);
                    ///selestType.indexOf(-1);
                    hour.add(0, "Choose");
                }*/
//             if(customSpinnerAdapterDuration!=null)
//             {
//                 customSpinnerAdapterDuration.notifyDataSetChanged();
//             }
//             else
//             {
            customSpinnerAdapterDuration = new CustomSpinnerAdapterDuration(context, android.R.layout.simple_spinner_item, hour);
            spHour.setAdapter(customSpinnerAdapterDuration);
            if(valueToSet!=null && !valueToSet.equalsIgnoreCase(""))
            {
                for (int j = 0; j < hour.size(); j++) {
                    if (valueToSet.equalsIgnoreCase(hour.get(j))) {
                        spHour.setSelection(j);
                    }
                }
                valueToSet="";
            }
//             }

/*
                hoursAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_layout_view, hour);
                hoursAdapter.setDropDownViewResource(R.layout.spinner_text_view);
              //  NothingSelectedSpinnerAdapter adapter = new NothingSelectedSpinnerAdapter(hoursAdapter, R.layout.no_item_selected_hour, getActivity());
                spHour.setAdapter(hoursAdapter);*/
        } else if (adapterView == spHour) {
            try {

                Log.e(">>>>>>> ", "count" + adapterView.getCount());
                Log.e(">>>>>>> ", "iiiiiii" + i);
                //  duration_value = adapterView.getItemAtPosition(i).toString();
                    duration_value = hour.get(i).toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
//        }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Log.e("NOTHING_SELECTED", "YES");
      /*  if (adapterView == spPrice) {
            TextView tv = (TextView) adapterView.findViewById(R.id.spinner_text);
            tv.setTextColor(getResources().getColor(R.color.color_white));
        }
        else if (adapterView == sptype){
            TextView tv = (TextView) adapterView.findViewById(R.id.spinner_text);
            tv.setTextColor(getResources().getColor(R.color.color_white));
        }

        else if (adapterView == spHour) {
            TextView tv = (TextView) adapterView.findViewById(R.id.spinner_text);
            tv.setTextColor(getResources().getColor(R.color.color_white));
        }*/
    }

    private void calleditapi() {
        if (ConnectionDetector.isNetAvail(getActivity())) {
            EditPost();
        } else {
            Common.displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, getActivity());
        }
    }

    private void dalomesaage(String message) {
        dialogmessage = new Dialog(getActivity());
        dialogmessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogmessage.setContentView(R.layout.custom_dialog);
        dialogmessage.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogmessage.setCancelable(false);
        TextView mess_colose = (TextView) dialogmessage.findViewById(R.id.mess_colose);
        TextView dailog_message = (TextView) dialogmessage.findViewById(R.id.dailog_message);
        TextView dalog_ok = (TextView) dialogmessage.findViewById(R.id.dalog_ok);

        dailog_message.setText(message);

        dalog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogmessage.dismiss();
                getActivity().finish();
            }
        });
        mess_colose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogmessage.dismiss();
                getActivity().finish();

            }


        });

        dialogmessage.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = String.valueOf(location.getLatitude());
        longi = String.valueOf(location.getLongitude());
        Config.setlatitude(lat, getActivity());
        Config.setlongitude(longi, getActivity());

    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((getActivity().getPackageName() + ".services.Locationservice").equals(service1.service.getClassName())) {

                return true;
            }
        }
        return false;
    }

    private void ShowGpsDialog() {
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                getActivity());
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("No GPS")
                .setMessage(getString(R.string.gps_info))
                .setPositiveButton(getString(R.string.gps_seating),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                gpsAlertDialog.dismiss();
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeGpsDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                // finish();
                            }
                        });

        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;
        }
    }


    private void hitSellRentApi(String user_id, String post_type, String title, String description, String lat, String longi, String addressfull, String country_id, String price, String swap_deal, String swap_deal_title, String share_fb, String hours, String duration_type, String duration_value, String category_id, ArrayList<String> imgList, String post_id, String image_delete, final String type) {


        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            btSellRent.setEnabled(false);

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();


            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();


           /* Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();*/
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            HashMap<String, String> requestValueAddPairsMap = new HashMap<>();
            Call<AddResponseModel> call;
            ArrayList<MultipartBody.Part> params = new ArrayList<>();
            if (imgList.size() > 0) {

                for (int i = 0; i < imgList.size(); i++) {
                    if (imgList.get(i) == null || imgList.get(i).contains("http") || imgList.get(i).contains("https")) {

                    } else {
                        params.add(RetrofitUtils.createFilePart("images[]",
                                imgList.get(i),
                                RetrofitUtils.MEDIA_TYPE_IMAGE_PNG));
                    }
                }
            }
            if (post_type != null && post_type.equalsIgnoreCase("R")) {
                if (swap_deal_title != null && !swap_deal_title.isEmpty()) {
                    requestValueAddPairsMap.put("user_id", user_id);
                    requestValueAddPairsMap.put("post_type", post_type);
                    requestValueAddPairsMap.put("title", title);
                    requestValueAddPairsMap.put("description", description);
                    requestValueAddPairsMap.put("latitude", lat);
                    requestValueAddPairsMap.put("longitude", longi);
                    requestValueAddPairsMap.put("country_id", country_id);
                    requestValueAddPairsMap.put("price", price);
                    requestValueAddPairsMap.put("swap_deal", swap_deal);
                    requestValueAddPairsMap.put("swap_deal_title", swap_deal_title);
                    requestValueAddPairsMap.put("share_fb", share_fb);
                    requestValueAddPairsMap.put("hours", hours);
                    requestValueAddPairsMap.put("category_id", category_id);
                    requestValueAddPairsMap.put("duration_type", duration_type);
                    requestValueAddPairsMap.put("duration_value", duration_value);
                    requestValueAddPairsMap.put("post_id", post_id);
                    requestValueAddPairsMap.put("delete_image", image_delete);


                } else {

                    requestValueAddPairsMap.put("user_id", user_id);
                    requestValueAddPairsMap.put("post_type", post_type);
                    requestValueAddPairsMap.put("title", title);
                    requestValueAddPairsMap.put("description", description);
                    requestValueAddPairsMap.put("latitude", lat);
                    requestValueAddPairsMap.put("longitude", longi);
                    // requestValueAddPairsMap.put("address", addressfull);
                    requestValueAddPairsMap.put("country_id", country_id);
                    requestValueAddPairsMap.put("price", price);
                    requestValueAddPairsMap.put("swap_deal", swap_deal);
                    // requestValueAddPairsMap.put("swap_deal_title", swap_deal_title);
                    requestValueAddPairsMap.put("share_fb", share_fb);
                    //requestValueAddPairsMap.put("hours", hours);
                    requestValueAddPairsMap.put("category_id", category_id);
                    requestValueAddPairsMap.put("duration_type", duration_type);
                    requestValueAddPairsMap.put("duration_value", duration_value);
                    requestValueAddPairsMap.put("post_id", post_id);
                    requestValueAddPairsMap.put("delete_image", image_delete);
                }
            } else {
                if (swap_deal_title != null && !swap_deal_title.isEmpty()) {
                    requestValueAddPairsMap.put("user_id", user_id);
                    requestValueAddPairsMap.put("post_type", post_type);
                    requestValueAddPairsMap.put("title", title);
                    requestValueAddPairsMap.put("description", description);
                    requestValueAddPairsMap.put("latitude", lat);
                    requestValueAddPairsMap.put("longitude", longi);
                    //requestValueAddPairsMap.put("address", addressfull);
                    requestValueAddPairsMap.put("country_id", country_id);
                    requestValueAddPairsMap.put("price", price);
                    requestValueAddPairsMap.put("swap_deal", swap_deal);
                    requestValueAddPairsMap.put("swap_deal_title", swap_deal_title);
                    requestValueAddPairsMap.put("share_fb", share_fb);
                    requestValueAddPairsMap.put("category_id", category_id);
                    requestValueAddPairsMap.put("duration_type", duration_type);
                    requestValueAddPairsMap.put("duration_value", duration_value);
                    requestValueAddPairsMap.put("post_id", post_id);
                    requestValueAddPairsMap.put("delete_image", image_delete);
                } else {
                    requestValueAddPairsMap.put("user_id", user_id);
                    requestValueAddPairsMap.put("post_type", post_type);
                    requestValueAddPairsMap.put("title", title);
                    requestValueAddPairsMap.put("description", description);
                    requestValueAddPairsMap.put("latitude", lat);
                    requestValueAddPairsMap.put("longitude", longi);
                    //requestValueAddPairsMap.put("address", addressfull);
                    requestValueAddPairsMap.put("country_id", country_id);
                    requestValueAddPairsMap.put("price", price);
                    requestValueAddPairsMap.put("swap_deal", swap_deal);
                    //requestValueAddPairsMap.put("swap_deal_title", swap_deal_title);
                    requestValueAddPairsMap.put("share_fb", share_fb);
                    requestValueAddPairsMap.put("category_id", category_id);
                    requestValueAddPairsMap.put("duration_type", duration_type);
                    requestValueAddPairsMap.put("duration_value", duration_value);
                    requestValueAddPairsMap.put("post_id", post_id);
                    requestValueAddPairsMap.put("delete_image", image_delete);
                }
            }

            if (imgList.size() > 0) {
                call = apiInterface.PostAddSell(RetrofitUtils.createMultipartRequest(requestValueAddPairsMap), params);
            } else {
                call = apiInterface.PostAddSell(RetrofitUtils.createMultipartRequest(requestValueAddPairsMap));
            }

            call.enqueue(new Callback<AddResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<AddResponseModel> call, @NonNull Response<AddResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        btSellRent.setEnabled(true);

                        AddResponseModel addResponseModel = response.body();
                        if (addResponseModel.isError() == false) {
                            SellOffer.photos.clear();
                            if (type.equalsIgnoreCase("update")) {
                                dalomesaage(getString(R.string.update_message));
                            } else {
                                dalomesaage(getString(R.string.online_post_message));
                            }
                        } else {
                            Common.displayLongToast(getActivity(), addResponseModel.getMessage());
                        }


                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<AddResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().showAlertMessage(context, "Please try again!!!");
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });
        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void EditPost() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();
            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<PostEditModel> call = apiInterface.PostEdit(user_id, post_id);
            call.enqueue(new Callback<PostEditModel>() {
                @Override
                public void onResponse(@NonNull Call<PostEditModel> call, @NonNull Response<PostEditModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        PostEditModel model = response.body();
                        currency = new ArrayList<>();
                        hour = new ArrayList<>();
                        if (model.isError() == false)

                        {
                            swap_deal = model.getResponse().getSwap_deal();
                            share_fb = model.getResponse().getShare_fb();
                            sub_category_name=model.getResponse().getSub_category_name();
                            sub_category_id=model.getResponse().getSub_category_id();


                            etSelling.setText(model.getResponse().getTitle());
                            etDescribe.setText(model.getResponse().getDescription());
                            tv_cteagr.setText(model.getResponse().getCategory_name()+" | "+sub_category_name);
                            etPrice.setText(String.valueOf(model.getResponse().getPrice()));
                            try {
                                if (model.getResponse().getDuration_type() != null) {
                                    if (model.getResponse().getDuration_type().equalsIgnoreCase("H")) {
//                                        hour.clear();
                                        sptype.setSelection(1);
                                        dayweekm = 24;

                                    } else if (model.getResponse().getDuration_type().equalsIgnoreCase("D")) {
//                                        hour.clear();
                                        sptype.setSelection(2);
                                        dayweekm = 30;
                                    } else if (model.getResponse().getDuration_type().equalsIgnoreCase("W")) {
//                                        hour.clear();
                                        sptype.setSelection(3);
                                        dayweekm = 4;
                                    } else if (model.getResponse().getDuration_type().equalsIgnoreCase("M")) {
//                                        hour.clear();
                                        sptype.setSelection(4);
                                        dayweekm = 12;
                                    }
                                }
                                if (model.getResponse().getDuration_value() != null) {
                                    hour.add("Choose");
                                    for (int k = 1; k <= dayweekm; k++) {
                                        hour.add(String.valueOf(k));
                                    }
                                    valueToSet = model.getResponse().getDuration_value();
                                    customSpinnerAdapterDuration = new CustomSpinnerAdapterDuration(context, android.R.layout.simple_spinner_item, hour);
                                    spHour.setAdapter(customSpinnerAdapterDuration);

                                    for (int i = 0; i < hour.size(); i++) {
                                        if (valueToSet.equalsIgnoreCase(hour.get(i))) {
                                            spHour.setSelection(i);
                                        }
                                    }

                                }

                                //==========category_id===========
                                CommonUtils.Id=model.getResponse().getSub_category_id()+"";
                              //  CommonUtils.Id=model.getResponse().getCategory_id()+"";

                                //=============================
                                for (int i = 0; i < curencyNameModel.getResponse().size(); i++) {
                                    currency.add(curencyNameModel.getResponse().get(i));
//                                    curencyNameModel.getResponse().get(i).getCurrency_name().toString();
                                }
                                if (!currency.get(0).getCurrency_name().equalsIgnoreCase("Choose")) {
                                    CurencyNameResponseModel.ResponseBean res = new CurencyNameResponseModel.ResponseBean();
                                    res.setId(-1);
                                    res.setCurrency_name("Choose");
                                    currency.add(0, res);
                                }

                                for (int j = 0; j < currency.size(); j++) {
                                    if (model.getResponse().getCurrency_name().equalsIgnoreCase(currency.get(j).getCurrency_name())) {
                                        spPrice.setSelection(j);
                                    }
                                }
//                                if (model.getResponse().getCurrency_name().equalsIgnoreCase(curencyNameModel.getResponse().get(i).getCurrency_name().toString())) {
//                                    spPrice.setSelection(i);
//                                }

//
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (model.getResponse().getSwap_deal().equalsIgnoreCase("Y")) {
                                tbSwapDeal.setChecked(true);
                                tvSellOn.setVisibility(View.VISIBLE);
                                tvSellOn.setText(model.getResponse().getSwap_deal_title());
                            }
                            if (model.getResponse().getShare_fb().equalsIgnoreCase("Y")) {
                                tbShareOnFacebook.setChecked(true);
                            }

                            category_id = String.valueOf(model.getResponse().getCategory_id());
                            if (model.getResponse().getImages().size() > 0) {
                                Bitmap image = null;
                                for (int im = 0; im < model.getResponse().getImages().size(); im++) {
                                    SellOffer.addImageInList(model.getResponse().getImages().get(im).getThumb(), String.valueOf(model.getResponse().getImages().get(im).getId()));
                                }
                            }
                        } else {
                            CommonUtils.showToastMessage(context, model.getMessage());
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }
                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PostEditModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });

        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    BroadcastReceiver myBroadcastReceiver_notify = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            tv_cteagr.setText(intent.getStringExtra("category_name"));
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

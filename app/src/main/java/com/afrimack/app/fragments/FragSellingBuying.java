package com.afrimack.app.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActMyAfrimack;
import com.afrimack.app.adapters.SellBuyingAdapter;
import com.afrimack.app.adapters.SoldAdapter;
import com.afrimack.app.adapters.SellingBuyingAdapter;
import com.afrimack.app.appbeans.BlockedUser;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.DeletPostResponseModel;
import com.afrimack.app.models.SellingResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.EndlessRecyclerOnScrollListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragSellingBuying extends Fragment implements View.OnClickListener {


    private LinearLayout lay_main_sell;
    View.OnClickListener onClickListener;
    int pos;
    private String fragmentTitle;
    private ImageView img_back;
    //    private ImageView img_tick;
    private TextView tv_titel, tv_totalSell;
    private TextView tv_all, tv_selling, tv_sold, tv_swapped;
    private LinearLayout llNewsContainor;
    private Button btDiscover;
    private RecyclerView rvSellingBuying;
    private RecyclerView rvSelling, rvSold;
    private SellingBuyingAdapter mAdapter;
    private SellBuyingAdapter sellBuyingAdapter;
    private SoldAdapter soldAdapter;
    private List<SellingResponseModel.ResponseBean.PostsBean> sellListl = new ArrayList<>();
    private LinearLayout llProfileHeader;
    private ActMyAfrimack activity;
    private String user_id, type;
    private int page_no = 1;
    private View rootview;
    private LinearLayoutManager mLayoutManager, mLayoutManager1, mLayoutManager2;
    private Context context;
    public String TAG = "SellingBuyingFragment";

    public FragSellingBuying() {
        // Required empty public constructor
    }

    public static FragSellingBuying newInstance(String fragmentTitle) {
        Bundle args = new Bundle();
        FragSellingBuying fragment = new FragSellingBuying();
        fragment.setArguments(args);
        fragment.fragmentTitle = fragmentTitle;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        if (rootview == null) {
            rootview = inflater.inflate(R.layout.fragment_selling_buying, container, false);
            context = getActivity();
            user_id = Config.getUserId(getActivity());
            init(rootview);

            mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            rvSellingBuying.setLayoutManager(mLayoutManager);


            llNewsContainor.setVisibility(View.GONE);
            rvSellingBuying.setVisibility(View.VISIBLE);
            type = "all";

            mAdapter = new SellingBuyingAdapter(getActivity(), sellListl, this);
            rvSellingBuying.setAdapter(mAdapter);

            hitApi("withoutClick");


            rvSellingBuying.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {

                    page_no = page_no + 1;
                    hitApi("withoutClick");
                }
            });

            mLayoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            rvSelling.setLayoutManager(mLayoutManager1);
            rvSelling.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager1) {
                @Override
                public void onLoadMore(int current_page) {

                    page_no = page_no + 1;
                    hitApi("withoutClick");
                }
            });
            mLayoutManager2 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            rvSold.setLayoutManager(mLayoutManager2);
            rvSold.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager2) {
                @Override
                public void onLoadMore(int current_page) {

                    page_no = page_no + 1;
                    hitApi("withoutClick");
                }
            });

        }
        return rootview;
    }

    private void hitApi(final String fromWhere) {


        if (CommonUtils.getInstance().isNetConnected(context)) {
            if (fromWhere.equalsIgnoreCase("onClick")) {
                CommonUtils.getInstance().displayLoadingDialog(false, context);
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<SellingResponseModel> call = apiInterface.Selling(user_id, type, String.valueOf(page_no));
            call.enqueue(new Callback<SellingResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<SellingResponseModel> call, @NonNull Response<SellingResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        if (fromWhere.equalsIgnoreCase("onClick")) {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }
                        SellingResponseModel model = response.body();
                        if (model.isError() == false) {
                            lay_main_sell.setVisibility(View.VISIBLE);
                            if (model.getResponse() != null) {
                                tv_totalSell.setText(model.getResponse().getCount() + " " + getResources().getString(R.string.item));
                                if (model.getResponse().getPosts().size() > 0) {
                                    sellListl.addAll(model.getResponse().getPosts());
                                    if (type.equalsIgnoreCase("all")) {
                                        SellingBuyingAdapter.sellList = sellListl;
                                        mAdapter.notifyDataSetChanged();
                                    } else if (type.equalsIgnoreCase("selling")) {
                                        // SellBuyingAdapter sellBuyingAdapter;
                                        SellBuyingAdapter.selllList = sellListl;
                                        sellBuyingAdapter.notifyDataSetChanged();
                                    } else if (type.equalsIgnoreCase("sold")) {
                                        //SoldAdapter soldAdapter
                                        SoldAdapter.soldList = sellListl;
                                        soldAdapter.notifyDataSetChanged();
                                    }

                                } else {
                                    if (sellListl.size() == 0) {

                                        rvSellingBuying.setVisibility(View.GONE);
                                        llNewsContainor.setVisibility(View.VISIBLE);
                                        //  Common.displayLongToast(getActivity(),getResources().getString(R.string.data_nota));
                                    }
                                }
                            }
                        }


                    } catch (Exception e) {
                        if (fromWhere.equalsIgnoreCase("onClick")) {
                            CommonUtils.getInstance().dismissLoadingDialog();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SellingResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    if (fromWhere.equalsIgnoreCase("onClick")) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                    }
                }
            });


        } else {
            if (fromWhere.equalsIgnoreCase("onClick")) {
                CommonUtils.getInstance().dismissLoadingDialog();
            }
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void init(View rootview) {
        tv_swapped = (TextView) rootview.findViewById(R.id.tv_swapped);
        tv_swapped.setVisibility(View.GONE);
        lay_main_sell = (LinearLayout) rootview.findViewById(R.id.lay_main_sell);
        activity = (ActMyAfrimack) getActivity();
        tv_titel = activity.getTv_titel();

        tv_all = (TextView) rootview.findViewById(R.id.tv_all);
        tv_selling = (TextView) rootview.findViewById(R.id.tv_selling);
        tv_sold = (TextView) rootview.findViewById(R.id.tv_sold);
        tv_totalSell = (TextView) rootview.findViewById(R.id.tv_totalSell);
        btDiscover = (Button) rootview.findViewById(R.id.btDiscover);
        llNewsContainor = (LinearLayout) rootview.findViewById(R.id.llNewsContainor);
        rvSellingBuying = (RecyclerView) rootview.findViewById(R.id.rvSellingBuying);
        rvSelling = (RecyclerView) rootview.findViewById(R.id.rvSelling);
        rvSold = (RecyclerView) rootview.findViewById(R.id.rvSold);

        btDiscover.setOnClickListener(this);
        tv_all.setOnClickListener(this);
        tv_selling.setOnClickListener(this);
        tv_sold.setOnClickListener(this);
        tv_swapped.setOnClickListener(this);

        rvSellingBuying.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {

        if (v == tv_all) {
            rvSelling.setVisibility(View.GONE);
            rvSold.setVisibility(View.GONE);
            tv_all.setBackgroundColor(getActivity().getResources().getColor(R.color.color_yallow));
            tv_selling.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_sold.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));

            tv_all.setTextColor(getActivity().getResources().getColor(R.color.color_black));/*
            tv_selling.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_sold.setTextColor(getActivity().getResources().getColor(R.color.color_white));*/

            type = "all";
            llNewsContainor.setVisibility(View.GONE);
            rvSellingBuying.setVisibility(View.VISIBLE);
            page_no = 1;
            if (sellListl != null) {
                sellListl.clear();
            }
            hitApi("onClick");

        }
        if (v == tv_selling) {
            rvSellingBuying.setVisibility(View.GONE);
            rvSold.setVisibility(View.GONE);
            rvSelling.setVisibility(View.VISIBLE);
            tv_all.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_selling.setBackgroundColor(getActivity().getResources().getColor(R.color.color_yallow));
            tv_sold.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));

       /*     tv_all.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_selling.setTextColor(getActivity().getResources().getColor(R.color.color_black));
            tv_sold.setTextColor(getActivity().getResources().getColor(R.color.color_white));*/

            llNewsContainor.setVisibility(View.GONE);
            type = "selling";
            page_no = 1;
            if (sellListl != null) {
                sellListl.clear();
            }
            hitApi("onClick");

            sellBuyingAdapter = new SellBuyingAdapter(getActivity(), sellListl, this);
            rvSelling.setAdapter(sellBuyingAdapter);
        }
        if (v == tv_sold) {
            rvSellingBuying.setVisibility(View.GONE);
            rvSold.setVisibility(View.VISIBLE);
            rvSelling.setVisibility(View.GONE);
            tv_all.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_selling.setBackgroundColor(getActivity().getResources().getColor(R.color.color_white));
            tv_sold.setBackgroundColor(getActivity().getResources().getColor(R.color.color_yallow));
            /*tv_all.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_selling.setTextColor(getActivity().getResources().getColor(R.color.color_white));
            tv_sold.setTextColor(getActivity().getResources().getColor(R.color.color_black));*/
            llNewsContainor.setVisibility(View.GONE);
            type = "sold";
            page_no = 1;
            if (sellListl != null) {
                sellListl.clear();
            }
            hitApi("onClick");
            soldAdapter = new SoldAdapter(getActivity(), sellListl, this);
            rvSold.setAdapter(soldAdapter);
        }


        if (v == btDiscover) {
            getActivity().finish();
        }
        if (v.getId() == R.id.ivSellingItemClose) {
            pos = (int) v.getTag();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setMessage("Are you sure you want to delete this offer?");


            alertDialogBuilder.setPositiveButton("yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            if (type.equalsIgnoreCase("all")) {
                                hitPostDeleteApi(SellingBuyingAdapter.sellList.get(pos).getPost_id());
                            } else if (type.equalsIgnoreCase("selling")) {
                                hitPostDeleteApi(SellBuyingAdapter.selllList.get(pos).getPost_id());
                            } else if (type.equalsIgnoreCase("sold")) {
                                hitPostDeleteApi(SoldAdapter.soldList.get(pos).getPost_id());
                            }
                        }
                    });

            alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();


        }
    }

    private void hitPostDeleteApi(int postId) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<DeletPostResponseModel> call = apiInterface.PostDeleteSell(user_id, type, String.valueOf(postId));
            call.enqueue(new Callback<DeletPostResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<DeletPostResponseModel> call, @NonNull Response<DeletPostResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        DeletPostResponseModel deletPostResponseModel = response.body();
                        Common.displayLongToast(getActivity(), deletPostResponseModel.getMessage());
                        if (deletPostResponseModel.isError() == false) {
                            SellingBuyingAdapter.sellList.clear();
                            page_no = 1;
                            hitApi("withoutClick");
                        }
                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<DeletPostResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }


    }

}

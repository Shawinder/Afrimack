package com.afrimack.app.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActMyAfrimack;
import com.afrimack.app.activities.ActivityChattingOverView;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.activities.ActivityImprint;
import com.afrimack.app.activities.ActivityProfileStatus;
import com.afrimack.app.activities.ActivitySettings;
import com.afrimack.app.activities.ActivityYourReviews;
import com.afrimack.app.activities.BlockUserActivity;
import com.afrimack.app.activities.BlogActivity;
import com.afrimack.app.activities.FollowingActivity;
import com.afrimack.app.activities.PremiumFeatures;
import com.afrimack.app.activities.SingleProfileDating;
import com.afrimack.app.activities.TermsConditionsPolicy;
import com.afrimack.app.appbeans.User;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.ProfileResponseModel;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.services.ServiceUtils;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.brsoftech.core_utils.base.BaseSupportFragment;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragProfile extends BaseSupportFragment implements View.OnClickListener {

    private LinearLayout lay_main_profile, rating_layout;
    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;
    private TextView tvPromote;
    private TextView tvYourReviews;
    private TextView tvProfileCompletion, tv_logout;
    private TextView tvSettings;
    private ImageView ivPromote;
    private ImageView ivYourReviews;
    private ImageView ivProfileCompletion;
    private ImageView ivSettings;
    private LinearLayout llPromote, lay_following;
    private LinearLayout llYourReviews;
    private LinearLayout llProfileCompletion;
    private LinearLayout llSettings, lay_Single;
    private LinearLayout llProfileHeader;
    private ActMyAfrimack activity;

    private TextView tvUserFollow, tvbock_user;
    private TextView tvProfileName, tvProfileRating, tvProfileSales, tvProfilePurchased, tvProfileSince,tv_delete_account;
    private RatingBar rbProfileRating;
    public static ImageView IvProfile;
    private String user_id;
    ProfileResponseModel mode;
    private ImageView ivFacebook, ivTwitter, ivWhatsapp, ivSMS, ivMail, ivShare;

    private TextView terma_p, imprint, tv_feedback, tv_few_more_steps;
    private Dialog dialog, dialogreset;
    private EditText et_name, et_email, et_subject, et_messsage;
    private String f_name, f_email, f_subject, f_message;
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private TextView app_version;
    PackageInfo info;
    private TextView tv_reset_password, tv_blog;
    private EditText et_old_pass, et_new_pass, et_cinfrm_pass;
    private String old_pass, new_pass, cinfrm_pass;
    public static TextView tv_profilr_type;
    private TextView tvUnreadMessages, tv_unread_message_count;
    private LinearLayout lay_block_user, ll_chat;
    private Context context;
    private String TAG = "Profile Fragment";
    private Dialog dialogmessage;
    private View view_below_blog;
    private int ratingCount = 0;


    public FragProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        user_id = Config.getUserId(getActivity());
        context = getActivity();
        init(rootview);
        try {
            PackageManager manager = getActivity().getPackageManager();
            info = manager.getPackageInfo(getActivity().getPackageName(), 0);
        } catch (Exception e) {

        }
        app_version.setText("Version " + info.versionName);

        hitProfileApi();

        return rootview;
    }


    private void hitProfileApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

            Call<ProfileResponseModel> call = apiInterface.Profile(user_id, Config.getLoginUSERUID(context));
            call.enqueue(new Callback<ProfileResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<ProfileResponseModel> call, @NonNull Response<ProfileResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        mode = response.body();

                        if (mode.getCode() == 0) {
                            if (mode.isError() == false) {
                                ratingCount = mode.getResponse().getRating_count();
                                lay_main_profile.setVisibility(View.VISIBLE);
                                tvProfileName.setText(mode.getResponse().getFull_name());
                                ConstantMain.profile_name = mode.getResponse().getFull_name();
                                ConstantMain.rating = mode.getResponse().getRating();
                                ConstantMain.rating_count = mode.getResponse().getRating_count();
                                rbProfileRating.setRating(Float.parseFloat(String.valueOf(mode.getResponse().getRating())));
                                tvProfileRating.setText(String.valueOf(mode.getResponse().getRating_count()));

                                tvProfileSales.setText(String.valueOf(mode.getResponse().getSelling() + " " + getResources().getString(R.string.sales)));
                                tvProfilePurchased.setText(String.valueOf(mode.getResponse().getPurchase() + " " + getResources().getString(R.string.purchases)));
                                tvProfileSince.setText(getResources().getString(R.string.since) + " " + mode.getResponse().getSince());

                                if (mode.getResponse().getTotal_unread_msg_count() > 0) {
                                    tv_unread_message_count.setVisibility(View.VISIBLE);
                                    tv_unread_message_count.setText(String.valueOf(mode.getResponse().getTotal_unread_msg_count()));
                                } else {
                                    tv_unread_message_count.setVisibility(View.GONE);
                                }

                                Config.setSingleRegister(mode.getResponse().getSingle_registered(), context);
                                Config.setLoginProfileType(mode.getResponse().getProfile_type(), getActivity());
                                Config.setProfileComplet(String.valueOf(mode.getResponse().getCompletion()), getActivity());
                                Config.setFollowTotel(String.valueOf(mode.getResponse().getFollowing()), getActivity());
                                Config.setBlockTotel(String.valueOf(mode.getResponse().getBlock()), getActivity());

                                if (mode.getResponse().getFollowing() == 1) {
                                    tvUserFollow.setText("You are following " + String.valueOf(mode.getResponse().getFollowing()) + " user");
                                } else if (mode.getResponse().getFollowing() != 0) {
                                    tvUserFollow.setText("You are following " + String.valueOf(mode.getResponse().getFollowing()) + " users");
                                }
                                tvProfileCompletion.setText(getResources().getString(R.string.Profile_Completion) + " " + String.valueOf(mode.getResponse().getCompletion()) + " %");
                                if (mode.getResponse().getCompletion() == 100) {
                                    ivProfileCompletion.setBackgroundResource(R.drawable.profile_profile_full);
                                    tv_few_more_steps.setText(R.string.profile_complete);

                                } else {
                                    tv_few_more_steps.setText(R.string.Few_more_steps);
                                }


                                if (mode.getResponse().getRating_count() == 1) {
                                    tvYourReviews.setText("You have " + String.valueOf(mode.getResponse().getRating_count()) + " review");
                                } else if (mode.getResponse().getRating_count() != 0) {
                                    tvYourReviews.setText("You have " + String.valueOf(mode.getResponse().getRating_count()) + " reviews");
                                } else {
                                    tvYourReviews.setText(getResources().getString(R.string.Nobody_has_reviewed_you_yet));
                                }
                                if (mode.getResponse().getBlock() == 1) {
                                    tvbock_user.setText("You have " + String.valueOf(mode.getResponse().getBlock()) + " user block");
                                } else if (mode.getResponse().getBlock() != 0) {
                                    tvbock_user.setText("You have " + String.valueOf(mode.getResponse().getBlock()) + " users blocked");
                                } else {
                                    tvbock_user.setText("You haven't blocked any user");
                                }

                                tv_profilr_type.setText(mode.getResponse().getProfile_type());

                                Picasso.with(getActivity())
                                        .load(mode.getResponse().getImage())
                                        .placeholder(R.drawable.dami_image2)
                                        .into(IvProfile);
                                try {
                                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");
                                    mDatabase.child(Config.getLoginUSERUID(context)).child("picture").setValue(mode.getResponse().getImage());

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ProfileResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                }
            });


        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Config.getSingleRegister(context).equalsIgnoreCase("Y")) {
            ll_chat.setVisibility(View.VISIBLE);
        } else {
            ll_chat.setVisibility(View.GONE);
        }

        tvProfileName.setText(ConstantMain.profile_name);
        rbProfileRating.setRating(Float.parseFloat(String.valueOf(ConstantMain.rating)));
        tvProfileRating.setText(String.valueOf(ConstantMain.rating_count));

        if (Config.getLoginProfileImg(getActivity()) != null && !Config.getLoginProfileImg(getActivity()).equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(Config.getLoginProfileImg(getActivity()))
                    .placeholder(R.drawable.dami_image2)
                    .into(IvProfile);
        }
        if (Config.getLoginProfileType(getActivity()) != null) {
            tv_profilr_type.setText(Config.getLoginProfileType(getActivity()));
        }
        if (Config.getProfileComplet(getActivity()) != null) {
            tvProfileCompletion.setText(getResources().getString(R.string.Profile_Completion) + " " + Config.getProfileComplet(getActivity()) + " %");
        }

        if (!Config.getFollowTotel(getActivity()).equalsIgnoreCase("")) {
            if (Config.getFollowTotel(getActivity()).equalsIgnoreCase("0")) {
                tvUserFollow.setText(" You are not following any user yet");
            } else {
                tvUserFollow.setText("You are following " + String.valueOf(Config.getFollowTotel(getActivity()) + " users"));
            }

        } else {
            tvUserFollow.setText("List of User I follow");
        }
        if (!Config.getBlockTotel(getActivity()).equalsIgnoreCase("")) {
            if ((Config.getBlockTotel(getActivity()).equalsIgnoreCase("1"))) {
                tvbock_user.setText("You have " + String.valueOf(Config.getBlockTotel(getActivity())) + " user block");
            } else if (!(Config.getBlockTotel(getActivity())).equalsIgnoreCase("0")) {
                tvbock_user.setText("You have " + String.valueOf(Config.getBlockTotel(getActivity())) + " users blocked");
            }

        } else {
            tvbock_user.setText("You haven't blocked any user");
        }
        hitProfileApi();
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

        }
    }

    private void init(View rootview) {
        ll_chat = (LinearLayout) rootview.findViewById(R.id.ll_chat);
        if (Config.getSingleRegister(context).equalsIgnoreCase("Y")) {
            ll_chat.setVisibility(View.VISIBLE);
        } else {
            ll_chat.setVisibility(View.GONE);
        }
        view_below_blog = (View) rootview.findViewById(R.id.view_below_blog);
        view_below_blog.setVisibility(View.GONE);
        rating_layout = (LinearLayout) rootview.findViewById(R.id.rating_layout);
        rating_layout.setOnClickListener(this);
        ll_chat.setOnClickListener(this);
        tv_unread_message_count = (TextView) rootview.findViewById(R.id.tv_unread_message_count);
        lay_main_profile = (LinearLayout) rootview.findViewById(R.id.lay_main_profile);
        tvPromote = (TextView) rootview.findViewById(R.id.tvPromote);
        tvYourReviews = (TextView) rootview.findViewById(R.id.tvYourReviews);
        tvProfileCompletion = (TextView) rootview.findViewById(R.id.tvProfileCompletion);
        tv_few_more_steps = (TextView) rootview.findViewById(R.id.tv_few_more_steps);
        tvSettings = (TextView) rootview.findViewById(R.id.tvSettings);
        ivFacebook = (ImageView) rootview.findViewById(R.id.ivFacebook);
        ivWhatsapp = (ImageView) rootview.findViewById(R.id.ivWhatsapp);
        ivTwitter = (ImageView) rootview.findViewById(R.id.ivTwitter);
        ivSMS = (ImageView) rootview.findViewById(R.id.ivSMS);
        ivMail = (ImageView) rootview.findViewById(R.id.ivMail);
        ivShare = (ImageView) rootview.findViewById(R.id.ivShare);
        ivPromote = (ImageView) rootview.findViewById(R.id.ivPromote);
        ivYourReviews = (ImageView) rootview.findViewById(R.id.ivYourReviews);
        ivProfileCompletion = (ImageView) rootview.findViewById(R.id.ivProfileCompletion);
        ivSettings = (ImageView) rootview.findViewById(R.id.ivSettings);
        llPromote = (LinearLayout) rootview.findViewById(R.id.llPromote);
        llYourReviews = (LinearLayout) rootview.findViewById(R.id.llYourReviews);
        llProfileCompletion = (LinearLayout) rootview.findViewById(R.id.llProfileCompletion);
        llSettings = (LinearLayout) rootview.findViewById(R.id.llSettings);
        llSettings.setVisibility(View.VISIBLE);
        tv_logout = (TextView) rootview.findViewById(R.id.tv_logout);

        tvProfileName = (TextView) rootview.findViewById(R.id.tvProfileName);
        tvProfileName.setOnClickListener(this);
        rbProfileRating = (RatingBar) rootview.findViewById(R.id.rbProfileRating);
        tvProfileRating = (TextView) rootview.findViewById(R.id.tvProfileRating);
        tvProfileSales = (TextView) rootview.findViewById(R.id.tvProfileSales);
        tv_delete_account = (TextView) rootview.findViewById(R.id.tv_delete_account);
        tvProfilePurchased = (TextView) rootview.findViewById(R.id.tvProfilePurchased);
        tvProfileSince = (TextView) rootview.findViewById(R.id.tvProfileSince);
        IvProfile = (ImageView) rootview.findViewById(R.id.cIvProfile);
        IvProfile.setOnClickListener(this);
        lay_following = (LinearLayout) rootview.findViewById(R.id.lay_following);
        lay_Single = (LinearLayout) rootview.findViewById(R.id.lay_Single);

        tvUserFollow = (TextView) rootview.findViewById(R.id.tvUserFollow);
        tvbock_user = (TextView) rootview.findViewById(R.id.tvbock_user);
        activity = (ActMyAfrimack) getActivity();
        // activity = (ActivityDiscoverDetail) getActivity();

        llProfileHeader = activity.getLlProfileHeader();
        tv_titel = activity.getTv_titel();

        llPromote.setOnClickListener(this);
        llYourReviews.setOnClickListener(this);
        llProfileCompletion.setOnClickListener(this);
        llSettings.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        lay_following.setOnClickListener(this);
        lay_Single.setOnClickListener(this);
        tv_delete_account.setOnClickListener(this);

        ivFacebook.setOnClickListener(this);
        ivTwitter.setOnClickListener(this);
        ivWhatsapp.setOnClickListener(this);
        ivSMS.setOnClickListener(this);
        ivMail.setOnClickListener(this);
        ivShare.setOnClickListener(this);

        terma_p = (TextView) rootview.findViewById(R.id.terma_p);
        imprint = (TextView) rootview.findViewById(R.id.imprint);
        tv_feedback = (TextView) rootview.findViewById(R.id.tv_feedback);

        terma_p.setOnClickListener(this);
        imprint.setOnClickListener(this);
        tv_feedback.setOnClickListener(this);


        app_version = (TextView) rootview.findViewById(R.id.app_version);
        tv_reset_password = (TextView) rootview.findViewById(R.id.tv_reset_password);
        tv_blog = (TextView) rootview.findViewById(R.id.tv_blog);
        tv_reset_password.setOnClickListener(this);
        tv_blog.setVisibility(View.GONE);
        tv_blog.setOnClickListener(this);

        tv_profilr_type = (TextView) rootview.findViewById(R.id.tv_profilr_type);

        tvUnreadMessages = (TextView) rootview.findViewById(R.id.tvUnreadMessages);
        tvUnreadMessages.setText("SHARE afrimack & INVITE YOUR FRIENDS");
        tvUnreadMessages.setTextColor(getResources().getColor(R.color.dark_gray));


        lay_block_user = (LinearLayout) rootview.findViewById(R.id.lay_block_user);
        lay_block_user.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        int vId = v.getId();

        if (vId == R.id.llPromote) {
            Intent intent = new Intent(getActivity(), PremiumFeatures.class);
            startActivity(intent);

        } else if (vId == R.id.cIvProfile) {
            //profile status
            Intent intent = new Intent(getActivity(), ActivityProfileStatus.class);
            startActivity(intent);
        } else if (vId == R.id.tvProfileName) {
            //reviews
            if (ratingCount != 0) {
                Intent intent = new Intent(getActivity(), ActivityYourReviews.class);
                intent.putExtra("id", Config.getUserId(context));
                startActivity(intent);
            } else {
                CommonUtils.showToastMessage(context, "Nobody has review you yet");
            }

        } else if (vId == R.id.rating_layout) {
            if (ratingCount != 0) {
                Intent intent = new Intent(getActivity(), ActivityYourReviews.class);
                intent.putExtra("id", Config.getUserId(context));
                startActivity(intent);
            } else {
                CommonUtils.showToastMessage(context, "Nobody has review you yet");
            }

        } else if (vId == R.id.llYourReviews) {
            try {
                if (ratingCount != 0) {
                    Intent intent = new Intent(getActivity(), ActivityYourReviews.class);
                    intent.putExtra("id", Config.getUserId(context));
                    startActivity(intent);
                } else {
                    CommonUtils.showToastMessage(context, "Nobody has review you yet");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (vId == R.id.llProfileCompletion) {
            Intent intent = new Intent(getActivity(), ActivityProfileStatus.class);
            startActivity(intent);
        } else if (vId == R.id.llSettings)

        {

            Intent intentsetting = new Intent(getActivity(), ActivitySettings.class);
            startActivity(intentsetting);
        }
      if (v==tv_delete_account){
          AlertDialog.Builder alert_dialog_remind= new AlertDialog.Builder(getActivity());

          // Setting Dialog Title
          alert_dialog_remind.setTitle("Delete Account");

          // Setting Dialog Message
          alert_dialog_remind.setMessage("Are you sure you want to delete your account completely? ");


          // Setting Positive "Yes" Button
          alert_dialog_remind.setPositiveButton("YES", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int which) {
                  dialog.cancel();
                 // hitRemindPartenerApi();

              }
          });

          // Setting Negative "NO" Button
          alert_dialog_remind.setNegativeButton("NO", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int which) {

                  dialog.cancel();
              }
          });

          // Showing Alert Message
          alert_dialog_remind.show();

      }

        if (v == tv_reset_password) {
            dialogreset = new Dialog(getActivity());
            dialogreset.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogreset.setContentView(R.layout.reset_dialog);
            dialogreset.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialogreset.setCancelable(false);


            et_old_pass = (EditText) dialogreset.findViewById(R.id.et_old_pass);
            et_new_pass = (EditText) dialogreset.findViewById(R.id.et_new_pass);
            et_cinfrm_pass = (EditText) dialogreset.findViewById(R.id.et_cinfrm_pass);
            TextView tv_cancel = (TextView) dialogreset.findViewById(R.id.tv_cancel);
            TextView tv_reset = (TextView) dialogreset.findViewById(R.id.tv_reset);
            TextView pass_colose = (TextView) dialogreset.findViewById(R.id.pass_colose);

            tv_cancel.setOnClickListener(this);
            tv_reset.setOnClickListener(this);
            pass_colose.setOnClickListener(this);

            tv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogreset.dismiss();
                }
            });
            pass_colose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogreset.dismiss();
                }
            });
            tv_reset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    old_pass = et_old_pass.getText().toString().trim();
                    new_pass = et_new_pass.getText().toString().trim();
                    cinfrm_pass = et_cinfrm_pass.getText().toString().trim();


                    if (old_pass.length() == 0 && new_pass.length() == 0 && cinfrm_pass.length() == 0) {

                        showmessage(getResources().getString(R.string.required_field));

                    } else if (old_pass.length() == 0) {
                        showmessage(getResources().getString(R.string.enter_old_pass));
                    } else if (new_pass.length() == 0) {
                        showmessage(getResources().getString(R.string.enter_new_pass));
                    } else if (cinfrm_pass.length() == 0) {
                        showmessage(getResources().getString(R.string.enter_new_confrom));
                    } else if (!new_pass.matches(cinfrm_pass)) {
                        showmessage(getResources().getString(R.string.enter_new_confrom_not));
                    } else {
                        resetPasswordFirebase(old_pass, new_pass);
                    }


                    // dialog.dismiss();


                }


            });

            dialogreset.show();
        }
        if (v == tv_blog) {
            Intent intent = new Intent(getActivity(), BlogActivity.class);
            startActivity(intent);
        }
        if (v == lay_following) {

            try {
                if (!String.valueOf(Config.getFollowTotel(context)).equalsIgnoreCase("") && !String.valueOf(Config.getFollowTotel(context)).equalsIgnoreCase("0")) {
                    Intent intent = new Intent(getActivity(), FollowingActivity.class);
                    startActivity(intent);
                } else {
                    CommonUtils.showToastMessage(context, "You are not following any user yet");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (v == ll_chat) {
            Intent mIntent = new Intent(context, ActivityChattingOverView.class);
            startActivity(mIntent);

        }
        if (v == tv_logout) {
            hitLogoutApi(user_id, Config.getDeviceToken(getActivity()));

        }
        if (v == lay_Single) {
            Intent intSingleDating = new Intent(getActivity(), SingleProfileDating.class);
            intSingleDating.putExtra("id", Config.getUserId(context));
            startActivity(intSingleDating);
        }

        if (v == terma_p) {
            Intent intent = new Intent(getActivity(), TermsConditionsPolicy.class);
            intent.putExtra("slug", "terms-conditions");
            startActivity(intent);
        }
        if (v == imprint) {
            Intent intent = new Intent(getActivity(), ActivityImprint.class);
            startActivity(intent);
        }
        if (v == tv_feedback) {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.feedback);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setCancelable(false);
            TextView tv_feedback = (TextView) dialog.findViewById(R.id.tv_feedback);
            TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);

            et_name = (EditText) dialog.findViewById(R.id.et_name);
            et_email = (EditText) dialog.findViewById(R.id.et_email);
            et_subject = (EditText) dialog.findViewById(R.id.et_subject);
            et_messsage = (EditText) dialog.findViewById(R.id.et_message);


            tv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            tv_feedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    f_name = et_name.getText().toString().trim();
                    f_email = et_email.getText().toString().trim();
                    f_subject = et_subject.getText().toString().trim();
                    f_message = et_messsage.getText().toString().trim();

                    if (f_name.length() == 0 && f_email.length() == 0 && f_subject.length() == 0 && f_message.length() == 0) {

                        showmessage(getResources().getString(R.string.required_field));

                    } else if (f_name.length() == 0) {
                        showmessage(getResources().getString(R.string.user_name_fill));
                    } else if (f_email.length() == 0) {
                        showmessage(getResources().getString(R.string.fill_email));
                    } else if (f_subject.length() == 0) {
                        showmessage(getResources().getString(R.string.fill_subject));
                    } else if (f_message.length() == 0) {
                        showmessage(getResources().getString(R.string.fill_message));
                    } else if (!f_email.matches(EMAIL_REGEX)) {
                        showmessage(getResources().getString(R.string.invalid_email));

                    } else {
                        feedbackapi();
                    }
                }


            });

            dialog.show();
        }
        if (v == lay_block_user) {

            Intent intent = new Intent(getActivity(), BlockUserActivity.class);
            startActivity(intent);
        }
        if (v == ivFacebook) {

            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            PackageManager pm = getActivity().getPackageManager();
            List<ResolveInfo> resInfos = pm.queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);

                    if (packageName.contains("com.facebook.katana")
                            || packageName.contains("com.facebook.orca")) {
                        Intent intent = new Intent();

                        intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                        intent.putExtra("AppName", resInfo.loadLabel(pm).toString());
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.afrimack.app");
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Afrimack");
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Select app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                    startActivity(chooserIntent);
                } else {
                    Toast.makeText(getActivity(), "No app to share.", Toast.LENGTH_LONG).show();
                }
            }
        }
        if (v == ivTwitter) {

            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            PackageManager pm = getActivity().getPackageManager();
            List<ResolveInfo> resInfos = pm.queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);

                    if (packageName.contains("com.twitter.android")) {
                        Intent intent = new Intent();

                        intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                        intent.putExtra("AppName", resInfo.loadLabel(pm).toString());
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.afrimack.app");
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Afrimack");
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Select app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                    startActivity(chooserIntent);
                } else {
                    Toast.makeText(getActivity(), "No app to share.", Toast.LENGTH_LONG).show();
                }
            }
        }
        if (v == ivWhatsapp) {

            List<Intent> targetShareIntents = new ArrayList<Intent>();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            PackageManager pm = getActivity().getPackageManager();
            List<ResolveInfo> resInfos = pm.queryIntentActivities(shareIntent, 0);
            if (!resInfos.isEmpty()) {
                System.out.println("Have package");
                for (ResolveInfo resInfo : resInfos) {
                    String packageName = resInfo.activityInfo.packageName;
                    Log.i("Package Name", packageName);

                    if (packageName.contains("com.whatsapp")) {
                        Intent intent = new Intent();

                        intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
                        intent.putExtra("AppName", resInfo.loadLabel(pm).toString());
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.afrimack.app");
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Afrimack");
                        intent.setPackage(packageName);
                        targetShareIntents.add(intent);
                    }
                }
                if (!targetShareIntents.isEmpty()) {
                    Collections.sort(targetShareIntents, new Comparator<Intent>() {
                        @Override
                        public int compare(Intent o1, Intent o2) {
                            return o1.getStringExtra("AppName").compareTo(o2.getStringExtra("AppName"));
                        }
                    });
                    Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Select app to share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
                    startActivity(chooserIntent);

                } else {
                    Toast.makeText(getActivity(), "No app to share.", Toast.LENGTH_LONG).show();
                }
            }

        }
        if (v == ivSMS) {

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            String url = "https://play.google.com/store/apps/details?id=com.afrimack.app";
            intent.setType("text/plain");
            intent.setType("image/");
            intent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(intent, "Share"));

        }
        if (v == ivMail) {

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            String url = "https://play.google.com/store/apps/details?id=com.afrimack.app";
            intent.setType("text/plain");
            intent.setType("image/");
            intent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(intent, "Share"));

        }
        if (v == ivShare) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);

            // # change the type of data you need to share,
            // # for image use "image/*"
            String url = "https://play.google.com/store/apps/details?id=com.afrimack.app";
            intent.setType("text/plain");
            intent.setType("image/");
            intent.putExtra(Intent.EXTRA_TEXT, url);
            startActivity(Intent.createChooser(intent, "Share"));
        }

    }

    private void resetPasswordFirebase(String old_pass, final String new_pass) {
        CommonUtils.getInstance().displayLoadingDialog(false, context);
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        // Get auth credentials from the user for re-authentication. The example below shows// email and password credentials but there are multiple possible providers,// such as GoogleAuthProvider or FacebookAuthProvider.
        AuthCredential credential = EmailAuthProvider.getCredential(Config.getLoginEmailId(), old_pass);
        // Prompt the user to re-provide their sign-in credentials
        user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    user.updatePassword(new_pass).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Password updated");
                                String name = Config.getLoginUserName();
                                User user = new User(name.substring(0, name.indexOf(" ")), name.substring(name.indexOf(" "), name.length()), Config.getLoginEmailId(), Config.getLoginProfileImg(context), new_pass, Config.getDeviceToken(context));
                                setPasswordToFirebase(user);

                            } else {
                                Common.displayLongToast(getActivity(), "Error password not updated");
                                Log.d(TAG, "Error password not updated");
                            }
                        }
                    });
                } else {
                    Log.d(TAG, "Error auth failed");
                }
            }
        });
    }

    private void setPasswordToFirebase(User user) {

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");

//        Creating new user node, which returns the unique key value

        String userId;

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            userId = firebaseUser.getUid();
        } else
            userId = mDatabase.push().getKey();  //if firebaseUser is null then create the unique userId with this line

        mDatabase.child(userId).child("password").setValue(user.getPassword());
        hitResetPasswordApi();
    }

    private void dialogMessage(String message) {

        dialogmessage = new Dialog(context);
        dialogmessage.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogmessage.setContentView(R.layout.custom_dialog);
        dialogmessage.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogmessage.setCancelable(false);
        TextView mess_colose = (TextView) dialogmessage.findViewById(R.id.mess_colose);
        TextView dailog_message = (TextView) dialogmessage.findViewById(R.id.dailog_message);
        TextView entry_message = (TextView) dialogmessage.findViewById(R.id.entry_message);
        entry_message.setVisibility(View.GONE);
        TextView dalog_ok = (TextView) dialogmessage.findViewById(R.id.dalog_ok);
        TextView dalog_titel = (TextView) dialogmessage.findViewById(R.id.dalog_titel);
        //dalog_titel.setVisibility(View.VISIBLE);
        //dalog_titel.setText("Sell Offer Rent page new");
        dailog_message.setText(message + ".");

        dalog_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogmessage.dismiss();
//                finish();
            }
        });
        mess_colose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogmessage.dismiss();
//                finish();

            }


        });

        dialogmessage.show();
    }

    private void feedbackapi() {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.feedback(f_name, f_email, f_subject, f_message);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel comm_model = response.body();
                        if (comm_model.isError() == false) {
                            dialogMessage(comm_model.getMessage());
                            dialog.dismiss();
                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }


    }

    private void hitResetPasswordApi() {


        if (CommonUtils.getInstance().isNetConnected(context)) {
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.ResetPassword(user_id, old_pass, new_pass, cinfrm_pass);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel comm_model = response.body();
                        Common.displayLongToast(getActivity(), comm_model.getMessage());
                        if (comm_model.isError() == false) {
                            dialogreset.dismiss();


                        }

                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });

        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    private void hitLogoutApi(String user_id, String deviceToken) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.Logout(user_id, Config.getDeviceToken(context));
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel addResponseModel = response.body();
                        if (addResponseModel.isError() == false) {

                            try {
                                ServiceUtils.updateLoginUserStatus(context, "0");
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        FirebaseAuth.getInstance().signOut();
                                        try {
                                            LoginManager.getInstance().logOut();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (Config.getLoginType(getActivity()).equalsIgnoreCase("fb")) {
                                            Config.setLoginType("", getActivity());
                                        }
                                        Config.setLoginUserUID("", getActivity());
                                        Config.setUserId("null", getActivity());
                                        Config.setSortDay("", getActivity());
                                        Config.setSortRadius("", getActivity());
                                        Config.setSortMinPrice("", getActivity());
                                        Config.setSortMaxPrice("", getActivity());
                                        Config.setSortCategory("", getActivity());
                                        Config.setSortSpecialCategory("", getActivity());
                                        Config.setSortBy("", getActivity());
                                        Config.setSortOwnCountry("no", getActivity());
                                        Config.setLoginProfileImg("", getActivity());
                                        Config.setSearch("", getActivity());
                                        Config.savePreferencese();
                                        Intent intent = new Intent(getActivity(), ActivityHome.class);
                                        // Intent intent = new Intent(getActivity(), ActivityHome.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                }, 1000);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }


                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }


    private void showmessage(String mstring) {
        Common.displayErrorDialog(mstring, getActivity());
    }


}

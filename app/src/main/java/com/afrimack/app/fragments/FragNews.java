package com.afrimack.app.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActMyAfrimack;
import com.afrimack.app.activities.ActivityChat;
import com.afrimack.app.activities.ActivityChattingOverView;
import com.afrimack.app.activities.ActivityDiscoverDetail;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.activities.OfferUserActivity;
import com.afrimack.app.activities.SingleDatingActivity;
import com.afrimack.app.activities.SingleProfileViewActivity;
import com.afrimack.app.adapters.ContactAdapter;
import com.afrimack.app.adapters.NewsFeedAdapter;
import com.afrimack.app.constants.ConstantMain;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.CommanResponseModel;
import com.afrimack.app.models.NotificationAllResponseModel;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.Common;
import com.afrimack.app.utils.CommonUtils;
import com.afrimack.app.utils.ConnectionDetector;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FragNews extends Fragment implements View.OnClickListener, OnLoadMoreListener {

    private LinearLayout lay_main_nofiction;
    private ImageView img_back;
    private ImageView img_tick;
    private TextView tv_titel;
    private TextView tvUnreadMessages, read_all;
    private RecyclerView recyclerNews;
    private NewsFeedAdapter mAdapter;
    private List<NotificationAllResponseModel.ResponseBean.NotificationsBean> notiList = new ArrayList<>();
    private List<String> hoursList = new ArrayList<>();
    private List<String> questionPosedList = new ArrayList<>();
    private LinearLayout llProfileHeader;
    private ActMyAfrimack activity;
    private String user_id, post_id;
    private int page_no = 1;
    private int pos;
    private LinearLayout llNewsContainor;
    private Button btDiscover;
    private LinearLayoutManager mLayoutManager;
    private ContactAdapter contactAdapter;
    protected Handler handler;
    View rootview;
    private PopupWindow popupWindow;
    private Dialog dialog;
    TextView mess_titel, mess_info;
    LinearLayout ll_read_unread;
    String notifiction_id;
    private Context context;
    public String TAG = "NewsFragment";
    ProgressBar news_progressbar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootview == null) {
            context = getActivity();
            rootview = inflater.inflate(R.layout.fragment_news, container, false);
            user_id = Config.getUserId(getActivity());
            init(rootview);
            hitApiNews(false);
        }
        return rootview;
    }

    private void hitApiNews(final boolean loadmore) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            news_progressbar.setVisibility(View.VISIBLE);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<NotificationAllResponseModel> call = apiInterface.NotificationAll(user_id, page_no, Config.getLoginUSERUID(context));
            call.enqueue(new Callback<NotificationAllResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<NotificationAllResponseModel> call, @NonNull Response<NotificationAllResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());

                        NotificationAllResponseModel model = response.body();
                        if (model.isError() == false) {
                            lay_main_nofiction.setVisibility(View.VISIBLE);
                            if (tvUnreadMessages != null) {
                                tvUnreadMessages.setText(model.getResponse().getUnread_count() + " UNREAD MESSAGES");
                            }
                            if (model.getResponse().getUnread_count() == 0) {
                                ll_read_unread.setVisibility(View.GONE);
                            } else {
                                ll_read_unread.setVisibility(View.VISIBLE);
                            }
                            if (ActivityHome.home_notification != null) {
                                if (model.getResponse().getUnread_count() == 0) {
                                    ll_read_unread.setVisibility(View.GONE);
                                    ActivityHome.home_notification.setVisibility(View.GONE);
                                } else {
                                    if (model.getResponse().getUnread_count() < 99) {

                                        ll_read_unread.setVisibility(View.VISIBLE);
                                        ActivityHome.home_notification.setVisibility(View.VISIBLE);
                                        ActivityHome.home_notification.setText(String.valueOf(model.getResponse().getUnread_count()));
                                    } else {
                                        ll_read_unread.setVisibility(View.VISIBLE);
                                        ActivityHome.home_notification.setVisibility(View.VISIBLE);
                                        ActivityHome.home_notification.setText("99+");
                                    }
                                }
                            }
                            if (model.getResponse().getNotifications().size() > 0) {
                                notiList.addAll(model.getResponse().getNotifications());
                                llNewsContainor.setVisibility(View.GONE);
                                recyclerNews.setVisibility(View.VISIBLE);

                                contactAdapter.notificationList = notiList;
                                contactAdapter.setLoaded();
                                contactAdapter.notifyDataSetChanged();
                            }

                        }
                        news_progressbar.setVisibility(View.GONE);

                    } catch (Exception e) {
//                        CommonUtils.getInstance().dismissLoadingDialog();
                        news_progressbar.setVisibility(View.GONE);
                        e.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Call<NotificationAllResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    news_progressbar.setVisibility(View.GONE);
//                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            news_progressbar.setVisibility(View.GONE);
//            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    private void init(View rootview) {
        news_progressbar=(ProgressBar)rootview.findViewById(R.id.news_progressbar);
        lay_main_nofiction = (LinearLayout) rootview.findViewById(R.id.lay_main_nofiction);
        activity = (ActMyAfrimack) getActivity();


        img_tick = activity.getImg_tick();
        tv_titel = activity.getTv_titel();
        llNewsContainor = (LinearLayout) rootview.findViewById(R.id.llNewsContainor_buy);
        btDiscover = (Button) rootview.findViewById(R.id.btDiscover_now);
        tvUnreadMessages = (TextView) rootview.findViewById(R.id.tvUnreadMessages);
        read_all = (TextView) rootview.findViewById(R.id.read_all);

        btDiscover.setOnClickListener(this);
        read_all.setOnClickListener(this);


        recyclerNews = (RecyclerView) rootview.findViewById(R.id.rvNews);

        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        recyclerNews.setLayoutManager(mLayoutManager);
        recyclerNews.setHasFixedSize(true);

        contactAdapter = new ContactAdapter(recyclerNews, getActivity(), notiList, this, this);
        contactAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                page_no = page_no + 1;
                hitApiNews(true);
            }
        });
        recyclerNews.setAdapter(contactAdapter);

        ll_read_unread = (LinearLayout) rootview.findViewById(R.id.ll_read_unread);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.notifiction_main:
                pos = (int) (v.getTag());
                post_id = String.valueOf(ContactAdapter.notificationList.get(pos).getPost_id());
                if (ContactAdapter.notificationList.get(pos).getIs_read().equalsIgnoreCase("N")) {
                    if (ConnectionDetector.isNetAvail(getActivity())) {
                        notifiction_id = String.valueOf(ContactAdapter.notificationList.get(pos).getNotification_id());
                        hitReadMessageApi(notifiction_id, ContactAdapter.notificationList.get(pos).getMessage());

                    } else {
                        Common.displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, getActivity());
                    }
                } else {
                    if (!post_id.equalsIgnoreCase("0") && !post_id.equalsIgnoreCase("null") && !post_id.equalsIgnoreCase("")) {


                       if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("question")) {
                            Intent intent = new Intent(getActivity(), ActivityDiscoverDetail.class);
                            Config.setPostId(post_id, getActivity());
                            startActivity(intent);
                        } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("chat_message")) {

                            if (Config.getSingleRegister(getActivity()).equalsIgnoreCase("Y")) {
                                Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                startActivity(mIntent);
                            }
                            else {
                                Intent intent = new Intent(context, SingleDatingActivity.class);
                                //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                                //  Config.setSingleChatId(user_id, context);
                                startActivity(intent);
                            }



                        } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("chat_request")) {

                           if (Config.getSingleRegister(getActivity()).equalsIgnoreCase("Y")) {
                               Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                               startActivity(mIntent);
                           }
                           else {
                               Intent intent = new Intent(context, SingleDatingActivity.class);
                               //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                               //  Config.setSingleChatId(user_id, context);
                               startActivity(intent);
                           }

                         /*   Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                            startActivity(mIntent);*/
                        } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("post_online")) {
                            Intent intent = new Intent(getActivity(), ActivityDiscoverDetail.class);
                            Config.setPostId(post_id, getActivity());
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getActivity(), ActivityChat.class);
                            Config.setPostId(post_id, getActivity());
                            intent.putExtra("from", "news");
                            Config.setOfferUserID(ContactAdapter.notificationList.get(pos).getOffer_user_id(), getActivity());
                            startActivity(intent);

                        }


                    } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("profile_online")) {

                        Intent intent = new Intent(context, SingleDatingActivity.class);
                        intent.putExtra("male", "");
                        intent.putExtra("female", "");
                        context.startActivity(intent);
                    } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("chat_message")) {

                        if (Config.getSingleRegister(getActivity()).equalsIgnoreCase("Y")) {
                            Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                            startActivity(mIntent);
                        }
                        else {
                            Intent intent = new Intent(context, SingleDatingActivity.class);
                            //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                            //  Config.setSingleChatId(user_id, context);
                            startActivity(intent);
                        }
                     /*   Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                        startActivity(mIntent);*/
                    } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("chat_request")) {
                        if (Config.getSingleRegister(getActivity()).equalsIgnoreCase("Y")) {
                            Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                            startActivity(mIntent);
                        }
                        else {
                            Intent intent = new Intent(context, SingleDatingActivity.class);
                            //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                            //  Config.setSingleChatId(user_id, context);
                            startActivity(intent);
                        }

                     /*   Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                        startActivity(mIntent);*/
                    } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("block user")) {
                        dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.view_notification_dialog);
                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog.setCancelable(false);
                        mess_titel = (TextView) dialog.findViewById(R.id.mess_titel);
                        mess_info = (TextView) dialog.findViewById(R.id.mess_info);
                        mess_titel.setVisibility(View.GONE);
                        mess_info.setText("You have been blocked.");

                        TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);


                        tv_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });

                        dialog.show();
                    } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("unblock user")) {
                        dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.view_notification_dialog);
                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog.setCancelable(false);
                        mess_titel = (TextView) dialog.findViewById(R.id.mess_titel);
                        mess_info = (TextView) dialog.findViewById(R.id.mess_info);
                        mess_titel.setVisibility(View.GONE);
                        mess_info.setText("You have been un-blocked");

                        TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);


                        tv_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });

                        dialog.show();
                    } else {
                        dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.view_notification_dialog);
                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog.setCancelable(false);
                        mess_titel = (TextView) dialog.findViewById(R.id.mess_titel);
                        mess_info = (TextView) dialog.findViewById(R.id.mess_info);

                        mess_titel.setText(String.valueOf(ContactAdapter.notificationList.get(pos).getTitle()));
                        if (ContactAdapter.notificationList.get(pos).getTitle() != null) {
                            mess_info.setText(String.valueOf(ContactAdapter.notificationList.get(pos).getMessage()));
                        }

                        TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);


                        tv_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });

                        dialog.show();

                    }

                }

                break;
            case R.id.btDiscover_now:
                getActivity().finish();
                break;

            case R.id.read_all:
                if (ConnectionDetector.isNetAvail(getActivity())) {
                    post_id = "0";
                    notifiction_id = "all";
                    hitReadMessageApi(notifiction_id, "");
                } else {
                    Common.displayErrorDialog(ConstantMain.NO_INTERNET_CONN_DESC, getActivity());
                }
                break;


        }

    }

    private void hitReadMessageApi(String notificationId, final String message) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CommanResponseModel> call = apiInterface.ReadMessage(user_id, notificationId);
            call.enqueue(new Callback<CommanResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<CommanResponseModel> call, @NonNull Response<CommanResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        CommanResponseModel comm_model = response.body();


                        if (comm_model.isError() == false) {
                            Log.e("post_id", post_id);
                            if (notifiction_id.equalsIgnoreCase("all")) {
                                Intent intent = new Intent(getActivity(), ActMyAfrimack.class);
                                intent.putExtra("from", "a");
                                startActivity(intent);
                                getActivity().finish();

                            } else if (!post_id.equalsIgnoreCase("0") && !post_id.equalsIgnoreCase("null") && !post_id.equalsIgnoreCase("")) {

                                if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("question")) {
                                    Intent intent = new Intent(getActivity(), ActivityDiscoverDetail.class);
                                    intent.putExtra("post_id", post_id);
                                    Config.setPostId(post_id, getActivity());
                                    intent.putExtra("notification", "yes");
                                    startActivity(intent);
                                    getActivity().finish();
                                } else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("post_online")) {
                                    Intent intent = new Intent(getActivity(), ActivityDiscoverDetail.class);
                                    intent.putExtra("post_id", post_id);
                                    Config.setPostId(post_id, getActivity());
                                    intent.putExtra("notification", "yes");
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                                else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("chat_message")) {

                                    if (Config.getSingleRegister(getActivity()).equalsIgnoreCase("Y")) {
                                        Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                        startActivity(mIntent);
                                    }
                                    else {
                                        Intent intent = new Intent(context, SingleDatingActivity.class);
                                        //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                                        //  Config.setSingleChatId(user_id, context);
                                        startActivity(intent);
                                    }
                                 /*   Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                    startActivity(mIntent);*/
                                }
                                else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("chat_request")) {

                                    if (Config.getSingleRegister(getActivity()).equalsIgnoreCase("Y")) {
                                        Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                        startActivity(mIntent);
                                    }
                                    else {
                                        Intent intent = new Intent(context, SingleDatingActivity.class);
                                        //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                                        //  Config.setSingleChatId(user_id, context);
                                        startActivity(intent);
                                    }
                                  /*  Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                    startActivity(mIntent);*/
                                }
                                else {
                                    Intent intent = new Intent(getActivity(), ActivityChat.class);
                                    Config.setPostId(post_id, getActivity());
                                    Config.setOfferUserID(ContactAdapter.notificationList.get(pos).getOffer_user_id(), getActivity());
                                    intent.putExtra("from", "news");
                                    intent.putExtra("notification", "yes");
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            }
                            else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("chat_message")) {

                                if (Config.getSingleRegister(getActivity()).equalsIgnoreCase("Y")) {
                                    Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                    startActivity(mIntent);
                                }
                                else {
                                    Intent intent = new Intent(context, SingleDatingActivity.class);
                                    //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                                    //  Config.setSingleChatId(user_id, context);
                                    startActivity(intent);
                                }
                              /*  Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                startActivity(mIntent);*/
                            }
                            else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("chat_request")) {

                                if (Config.getSingleRegister(getActivity()).equalsIgnoreCase("Y")) {
                                    Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                    startActivity(mIntent);
                                }
                                else {
                                    Intent intent = new Intent(context, SingleDatingActivity.class);
                                    //Config.setOfferUserID(Config.getUserId(getActivity()),getActivity());
                                    //  Config.setSingleChatId(user_id, context);
                                    startActivity(intent);
                                }
                              /*  Intent mIntent = new Intent(context, ActivityChattingOverView.class);
                                startActivity(mIntent);*/
                            }
                            else if (ContactAdapter.notificationList.get(pos).getType().equalsIgnoreCase("profile_online")) {
                                Intent intent = new Intent(context, SingleDatingActivity.class);
                                intent.putExtra("male", "");
                                intent.putExtra("female", "");
                                context.startActivity(intent);
                            }
                            else if (comm_model.getMessage().equalsIgnoreCase("Message marked as read")) {
                                dialog = new Dialog(getActivity());
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.view_notification_dialog);
                                dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                dialog.setCancelable(false);
                                mess_titel = (TextView) dialog.findViewById(R.id.mess_titel);
                                mess_info = (TextView) dialog.findViewById(R.id.mess_info);
                                mess_titel.setVisibility(View.GONE);
                                mess_info.setText(message);
//                                }
                                TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
                                tv_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        Intent intent = new Intent(getActivity(), ActMyAfrimack.class);
                                        intent.putExtra("from", "a");
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                });
                                dialog.show();
                            }
                            else {
                                dialog = new Dialog(getActivity());
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setContentView(R.layout.view_notification_dialog);
                                dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                dialog.setCancelable(false);
                                mess_titel = (TextView) dialog.findViewById(R.id.mess_titel);
                                mess_info = (TextView) dialog.findViewById(R.id.mess_info);
                                mess_titel.setText(String.valueOf(ContactAdapter.notificationList.get(pos).getTitle()));
                                if (ContactAdapter.notificationList.get(pos).getTitle() != null) {
                                    mess_info.setText(String.valueOf(ContactAdapter.notificationList.get(pos).getMessage()));
                                }
                                TextView tv_cancel = (TextView) dialog.findViewById(R.id.tv_cancel);
                                tv_cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        Intent intent = new Intent(getActivity(), ActMyAfrimack.class);
                                        intent.putExtra("from", "a");
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                });
                                dialog.show();
                            }
                        }
                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CommanResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });

        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }

    }

    @Override
    public void onLoadMore() {
    }
}

package com.afrimack.app.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.activities.SingleDatingActivity;
import com.afrimack.app.adapters.CategoriesLIstAdapter;
import com.afrimack.app.adapters.DatingPartenerListAdapter;
import com.afrimack.app.adapters.SpecialListAdapter;
import com.afrimack.app.appbeans.DiscoverResponseModel;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.utils.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DiscoverCategories extends Fragment implements View.OnClickListener {

    private View view;
    int pos;
    private Fragment frg;
    String user_id;

    public String TAG = "DiscoverCategories";
    private RecyclerView recyclerView, recycler_view_categories, recycler_view_pataner;
    private GridLayoutManager specialLayout, categoriesLayout, datingLayout;
    private TextView txtSpecial, txtCategories, txtDatingPartener;
    private Context context;
    private SpecialListAdapter specialListAdapter;
    private CategoriesLIstAdapter categoriesLIstAdapter;
    private DatingPartenerListAdapter datingPartenerListAdapter;


    public DiscoverCategories() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_discover_categories, container, false);
        context = getActivity();
        findIds(view);
        user_id = Config.getUserId(getActivity());
        hitApi();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityHome.tv_discover_sort.setText("");
        try {
            ActivityHome.imgArrow.setImageResource(R.drawable.ic_up_arrow_discover);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void hitApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<DiscoverResponseModel> call = apiInterface.Discover(user_id);
            call.enqueue(new Callback<DiscoverResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<DiscoverResponseModel> call, @NonNull Response<DiscoverResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        if (response.body() != null) {
                            DiscoverResponseModel obj = response.body();
                            if (!obj.isError()) {
                                if (obj.getResponse().getSpecial()!=null &&obj.getResponse().getSpecial().size() > 0) {
                                    txtSpecial.setVisibility(View.VISIBLE);
                                    specialListAdapter = new SpecialListAdapter(context, obj.getResponse().getSpecial());
                                    recyclerView.setAdapter(specialListAdapter);
                                }
                                if(obj.getResponse().getCategory()!=null && obj.getResponse().getCategory().size() > 0) {
                                    txtCategories.setVisibility(View.VISIBLE);
                                    categoriesLIstAdapter = new CategoriesLIstAdapter(context, obj.getResponse().getCategory());
                                    recycler_view_categories.setAdapter(categoriesLIstAdapter);
                                }
                               if (obj.getResponse().getPartner().size() > 0) {
                                    txtDatingPartener.setVisibility(View.VISIBLE);
                                    datingPartenerListAdapter = new DatingPartenerListAdapter(context, obj.getResponse().getPartner());
                                    recycler_view_pataner.setAdapter(datingPartenerListAdapter);

                                }
                            }

                        }
                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<DiscoverResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });
            
        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void findIds(View view) {
        ActivityHome.linear_place_your_ad.setVisibility(View.GONE);
        specialLayout = new GridLayoutManager(context, 2);
        categoriesLayout = new GridLayoutManager(context, 2);
        datingLayout = new GridLayoutManager(context, 2);
        txtSpecial = (TextView) view.findViewById(R.id.txt_special);
        txtCategories = (TextView) view.findViewById(R.id.txt_categories);
        txtDatingPartener = (TextView) view.findViewById(R.id.txt_dating_partener);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_special);
        recycler_view_categories = (RecyclerView) view.findViewById(R.id.recycler_view_categories);
        recycler_view_pataner = (RecyclerView) view.findViewById(R.id.recycler_view_dating);
        recyclerView.setHasFixedSize(true);
        recycler_view_categories.setHasFixedSize(true);
        recycler_view_pataner.setHasFixedSize(true);


        recyclerView.setLayoutManager(specialLayout);
        recycler_view_categories.setLayoutManager(categoriesLayout);
        recycler_view_categories.setHasFixedSize(true);

        recycler_view_pataner.setLayoutManager(datingLayout);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fm = getFragmentManager();
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getFragmentManager().getBackStackEntryCount() == 0) ;//finish();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_all:
                frg = new DiscoverHome();
                FragmentManager fragManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragTransaction = fragManager.beginTransaction();
                fragTransaction.replace(R.id.main_container, frg);
                fragTransaction.commit();
                while (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                }

                break;
            case R.id.lay_discover:
                pos = (int) v.getTag();
                String categoryname = String.valueOf(SpecialListAdapter.mLIst.get(pos).getCategory_name());
                if (categoryname.equalsIgnoreCase("Friends")) {
                    Intent intent = new Intent(getActivity(), SingleDatingActivity.class);
                    startActivity(intent);
                } else {

                    frg = new DiscoverHome();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.main_container, frg);
                    fragmentTransaction.commit();
                    while (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getActivity().getSupportFragmentManager().popBackStackImmediate();
                    }
                }

                break;
        }
    }


}

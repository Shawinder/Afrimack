package com.afrimack.app.fragments;


import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.afrimack.app.R;
import com.afrimack.app.activities.ActivityFilter;
import com.afrimack.app.activities.ActivityHome;
import com.afrimack.app.activities.SingleDatingActivity;
import com.afrimack.app.adapters.AdapterStaggreGrid;
import com.afrimack.app.adapters.SelectTagAdapter;
import com.afrimack.app.appbeans.SettingBean;
import com.afrimack.app.constants.GPSTracker;
import com.afrimack.app.customprefs.Config;
import com.afrimack.app.models.HomeResponseModel;
import com.afrimack.app.recycler.DividerDecoration;
import com.afrimack.app.reference.OnLoadMoreListener;
import com.afrimack.app.restApis.ApiClient;
import com.afrimack.app.restApis.ApiInterface;
import com.afrimack.app.services.Locationservice;
import com.afrimack.app.utils.AppConstants;
import com.afrimack.app.utils.CommonUtils;
import com.google.android.gms.maps.LocationSource;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.ACTIVITY_SERVICE;
import static com.afrimack.app.activities.ActivityFilter.sptList;
import static com.afrimack.app.activities.ActivityFilter.stList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiscoverHome extends Fragment implements LocationListener, View.OnClickListener, LocationSource.OnLocationChangedListener, OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "DiscoverHome";

    private View view;
    private RecyclerView recyclerView, recy_select_tag;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private SelectTagAdapter selectTagAdapter;
    private String lat, longi;
    private int page_no = 1;
    private AdapterStaggreGrid homeAdapter;
    private GPSTracker gpsTracker;
    private LinearLayoutManager horizontalLayoutManagaer;
    private ArrayList<HomeResponseModel.ResponseBean.PostsBean> allpostList = new ArrayList<HomeResponseModel.ResponseBean.PostsBean>();
    private boolean isLastPage = false;
    private String last_days, sorting, radius, own_country, special_category, category, sub_category, min_price, max_price, saechAlret;
    private List<String> selectlist;
    public static LinearLayout all_select;
    private ImageView img_remove_all_selecation;
    private LinearLayout lay_s_calender, lay_distance, lay_min_max, lay_sort, lay_courent, lay_search;
    private TextView tv_set_calender_date, tv_set_distance, tv_set_max_min, tv_set_sort_type, tv_set_courent, tv_search;
    private ImageView img_calender_close, img_destance_close, img_set_mix_min_colse, img_set_sort_colse, img_courent_colse, img_search_colse;
    private String distall;
    private String user_id;
    private Context context;
    private String login_user_fid = "";


    private SwipeRefreshLayout swipeRefreshLayout;
    private int offSet = 0;
    String apiinfo = "N";
    LocationManager locationManager;
    boolean GpsStatus;
    private AlertDialog gpsAlertDialog;
    private boolean isGpsDialogShowing = false;

    public DiscoverHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_discover_home, container, false);
        context = getActivity();
        login_user_fid = Config.getLoginUSERUID(context);
        gpsTracker = new GPSTracker(getActivity());
        ActivityHome.tv_discover_sort.setVisibility(View.VISIBLE);
        String filter_type = "";
        Config.setFilter(filter_type, getActivity());
        try {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {

            e.printStackTrace();
        }


        user_id = Config.getUserId(getActivity());
        last_days = Config.getSortDay(getActivity());
        sorting = Config.getSortBy(getActivity());
        radius = Config.getSortRadius(getActivity());
        own_country = Config.getSortOwnCountry(getActivity());
        special_category = Config.getSortSpecialCategory(getActivity());
        sub_category = Config.getSortSubCategory(getActivity());
        category = Config.getSortCategory(getActivity());
        min_price = Config.getSortMinPrice(getActivity());
        max_price = Config.getSortMaxPrice(getActivity());
        saechAlret = Config.getSearch(getActivity());


        init();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        DividerDecoration itemDecoration = new DividerDecoration(getActivity());

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(itemDecoration);
        homeAdapter = new AdapterStaggreGrid(getActivity(), allpostList, recyclerView, this);
        recyclerView.setAdapter(homeAdapter);

        horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recy_select_tag.setLayoutManager(horizontalLayoutManagaer);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);


        img_remove_all_selecation.setOnClickListener(this);
        img_calender_close.setOnClickListener(this);
        img_destance_close.setOnClickListener(this);
        img_set_mix_min_colse.setOnClickListener(this);
        img_set_sort_colse.setOnClickListener(this);
        img_courent_colse.setOnClickListener(this);
        img_search_colse.setOnClickListener(this);


        return view;
    }


    private void init() {

        ActivityHome.linear_place_your_ad.setVisibility(View.VISIBLE);
        recy_select_tag = (RecyclerView) view.findViewById(R.id.recy_select_tag);

        all_select = (LinearLayout) view.findViewById(R.id.all_select);
        img_remove_all_selecation = (ImageView) view.findViewById(R.id.img_remove_all_selecation);
        lay_s_calender = (LinearLayout) view.findViewById(R.id.lay_s_calender);
        tv_set_calender_date = (TextView) view.findViewById(R.id.tv_set_calender_date);
        img_calender_close = (ImageView) view.findViewById(R.id.img_calender_close);
        lay_distance = (LinearLayout) view.findViewById(R.id.lay_distance);
        tv_set_distance = (TextView) view.findViewById(R.id.tv_set_distance);
        img_destance_close = (ImageView) view.findViewById(R.id.img_destance_close);
        lay_min_max = (LinearLayout) view.findViewById(R.id.lay_min_max);
        tv_set_max_min = (TextView) view.findViewById(R.id.tv_set_max_min);
        img_set_mix_min_colse = (ImageView) view.findViewById(R.id.img_set_mix_min_colse);

        lay_search = (LinearLayout) view.findViewById(R.id.lay_search);
        tv_search = (TextView) view.findViewById(R.id.tv_search);
        img_search_colse = (ImageView) view.findViewById(R.id.img_search_colse);

        lay_sort = (LinearLayout) view.findViewById(R.id.lay_sort);
        tv_set_sort_type = (TextView) view.findViewById(R.id.tv_set_sort_type);
        img_set_sort_colse = (ImageView) view.findViewById(R.id.img_set_sort_colse);
        lay_courent = (LinearLayout) view.findViewById(R.id.lay_courent);
        tv_set_courent = (TextView) view.findViewById(R.id.tv_set_courent);
        img_courent_colse = (ImageView) view.findViewById(R.id.img_courent_colse);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_remove_all_selecation:
                Config.setSortDay("", getActivity());
                Config.setSortBy("", getActivity());
                Config.setSortRadius("", getActivity());
                Config.setSortMinPrice("", getActivity());
                Config.setSortMaxPrice("", getActivity());
                Config.setSortCategory("", getActivity());
                Config.setSortSpecialCategory("", getActivity());
                Config.setSortOwnCountry("no", getActivity());
                Config.setSearch("", getActivity());
                if (stList != null) {
                    stList.clear();
                }
                if (sptList != null) {
                    sptList.clear();
                }
                last_days = "";
                radius = "";
                min_price = "";
                max_price = "";
                sorting = "";
                own_country = "no";
                special_category = "";
                category = "";
                sub_category = "";
                saechAlret = "";
                allpostList.clear();
                homeAdapter.additemList.clear();
                homeAdapter.notifyDataSetChanged();
                hitApi("");
                all_select.setVisibility(View.GONE);

                break;

            case R.id.img_calender_close:
                Config.setSortDay("", getActivity());
                lay_s_calender.setVisibility(View.GONE);
                last_days = "";
                allpostList.clear();
                homeAdapter.additemList.clear();
                homeAdapter.notifyDataSetChanged();
                hitApi("1");

                break;
            case R.id.img_destance_close:
                Config.setSortRadius("", getActivity());
                lay_distance.setVisibility(View.GONE);
                radius = "";
                allpostList.clear();
                homeAdapter.additemList.clear();
                homeAdapter.notifyDataSetChanged();
                hitApi("1");
                apiinfo = "Y";
                break;

            case R.id.img_set_mix_min_colse:
                Config.setSortMinPrice("", getActivity());
                Config.setSortMaxPrice("", getActivity());
                lay_min_max.setVisibility(View.GONE);

                min_price = "";
                max_price = "";
                allpostList.clear();
                homeAdapter.additemList.clear();
                homeAdapter.notifyDataSetChanged();
                hitApi("1");
                break;
            case R.id.img_search_colse:
                Config.setSearch("", getActivity());
                lay_search.setVisibility(View.GONE);
                saechAlret = "";
                allpostList.clear();
                homeAdapter.additemList.clear();
                homeAdapter.notifyDataSetChanged();
                hitApi("1");
                break;
            case R.id.img_set_sort_colse:
                Config.setSortBy("", getActivity());
                lay_sort.setVisibility(View.GONE);
                sorting = "";
                allpostList.clear();
                homeAdapter.additemList.clear();
                homeAdapter.notifyDataSetChanged();
                hitApi("1");

                break;
            case R.id.img_courent_colse:
                Config.setSortOwnCountry("no", getActivity());
                lay_courent.setVisibility(View.GONE);
                own_country = "no";
                allpostList.clear();

                homeAdapter.notifyDataSetChanged();
                hitApi("1");


                break;
            case R.id.img_tag_colse:

                int pos = (int) v.getTag();
                String removedata = String.valueOf(SelectTagAdapter.horizontalList.get(pos).getId());
                String categettype = String.valueOf(SelectTagAdapter.horizontalList.get(pos).getType());

                if (categettype.equalsIgnoreCase("c")) {
                    String s = category;
                    List<String> myList = new ArrayList<String>(Arrays.asList(s.split(",")));
                    String abb = "";
                    for (int i = 0; i < myList.size(); i++) {

                        if (removedata.equalsIgnoreCase(myList.get(i))) {
                            myList.remove(i);
                        }

                        if (ActivityFilter.stList != null && ActivityFilter.stList.size() > 0) {

                            for (int j = 0; j < ActivityFilter.stList.size(); j++) {
                                String acf = String.valueOf(stList.get(j).getId());
                                if (removedata.equalsIgnoreCase(acf)) {

                                    if (stList.get(j).isSelected() == true) {
                                        stList.get(j).setSelected(false);

                                    }
                                }

                            }
                        }
                        try {
                            if (myList.size() > 0) {


                                if (abb.length() == 0) {
                                    abb = String.valueOf(myList.get(i));
                                } else {
                                    abb = abb + "," + myList.get(i);
                                }
                            } else {
                                abb = "";
                            }
                        } catch (Exception e) {

                            e.printStackTrace();

                        }

                        System.out.println(abb);
                    }
                    category = abb;
                    Config.setSortCategory(category, getActivity());

                } else if (categettype.equalsIgnoreCase("s")) {
                    String s = special_category;
                    List<String> myList = new ArrayList<String>(Arrays.asList(s.split(",")));
                    String abb = "";
                    for (int i = 0; i < myList.size(); i++) {

                        if (removedata.equalsIgnoreCase(myList.get(i))) {
                            myList.remove(i);
                        }

                        if (ActivityFilter.sptList != null && ActivityFilter.sptList.size() > 0) {


                            for (int j = 0; j < ActivityFilter.sptList.size(); j++) {
                                String acf = String.valueOf(sptList.get(j).getId());
                                if (removedata.equalsIgnoreCase(acf)) {

                                    if (sptList.get(j).isSelected() == true) {
                                        sptList.get(j).setSelected(false);

                                    }
                                }

                            }
                        }
                        if (myList.size() > 0) {


                            if (abb.length() == 0) {
                                abb = String.valueOf(myList.get(i));
                            } else {
                                abb = abb + "," + myList.get(i);
                            }
                        } else {
                            abb = "";
                        }

                    }


                    special_category = abb;
                    Config.setSortSpecialCategory(special_category, getActivity());
                }
                homeAdapter.additemList.clear();
                homeAdapter.notifyDataSetChanged();
                hitApi("1");
                break;
        }
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        try {
            lat = String.valueOf(location.getLatitude());
            longi = String.valueOf(location.getLongitude());
            Config.setlatitude(lat, getActivity());
            Config.setlongitude(longi, getActivity());

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRefresh() {
        lat = Config.getlatitude(getActivity());
        longi = Config.getlongitude(getActivity());
        if (allpostList != null) {
            allpostList.clear();
            homeAdapter.additemList.clear();
            homeAdapter.notifyDataSetChanged();
        }

        page_no = 1;
        hitApiAgain("");
    }


    @Override
    public void onResume() {
        super.onResume();


        try {
            ActivityHome.imgArrow.setImageResource(R.drawable.ic_down_arrow_discover);

            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//                ShowGpsDialog();
                if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {

                } else {
                    ShowGpsDialog();
                }
                if (!isMyServiceRunning()) {
                    context.startService(new Intent(context, Locationservice.class));
                }
            } else {
                if (!isMyServiceRunning()) {
                    context.startService(new Intent(context, Locationservice.class));
                }
                removeGpsDialog();
                try {
                    if (homeAdapter.additemList.size() == 0) {
                        page_no = 1;

                        if (allpostList != null) {
                            allpostList.clear();
                            homeAdapter.additemList.clear();
                            homeAdapter.notifyDataSetChanged();
                        }
                        swipeRefreshLayout.setRefreshing(true);
                        hitApiAgain("");

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                hitSettingApi();
            }
        });

    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service1 : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ((context.getPackageName() + ".services.Locationservice").equals(service1.service.getClassName())) {

                return true;
            }
        }
        return false;
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;
        }
    }

    private void ShowGpsDialog() {
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                context);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("No GPS")
                .setMessage(getString(R.string.gps_info))
                .setPositiveButton(getString(R.string.gps_seating),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                try {
                                    // if (gpsAlertDialog != null)
                                    gpsAlertDialog.dismiss();
                                    // continue with delete
                                    removeGpsDialog();
                                    Intent intent = new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                    removeGpsDialog();
                                } catch (Exception e) {

                                    e.printStackTrace();

                                }
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                // finish();
                            }
                        });

        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE).setEnabled(false);
            }
        });
        gpsAlertDialog.show();
    }

    private void hitSettingApi() {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(new Gson())).build();


            ApiInterface apiInterface = retrofit.create(ApiInterface.class);
            Call<SettingBean> call = apiInterface.GetSettings(user_id);
            call.enqueue(new Callback<SettingBean>() {
                @Override
                public void onResponse(@NonNull Call<SettingBean> call, @NonNull Response<SettingBean> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        SettingBean mBean = response.body();
                        if (mBean.getError() == false) {
                            try {
                                Config.setChatMessageStatus(Integer.valueOf(mBean.getResponse().getPush().getNewMessageChat()), context);
                            } catch (Exception ex) {

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<SettingBean> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    CommonUtils.getInstance().showAlertMessage(context, "Something went wrong. Please try again later.");
                }
            });

        } else {
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitApi(final String from) {
        if (CommonUtils.getInstance().isNetConnected(context)) {
            CommonUtils.getInstance().displayLoadingDialog(false, context);

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<HomeResponseModel> call = apiInterface.PostHome(lat, longi, page_no, last_days, sorting, radius, own_country, special_category, category, sub_category, min_price, max_price, saechAlret, user_id, login_user_fid);
            call.enqueue(new Callback<HomeResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<HomeResponseModel> call, @NonNull Response<HomeResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        HomeResponseModel model = response.body();

                        all_select.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        if (model.isError() == false) {

                            if (model.getResponse().getNotification_count() == 0) {
                                ActivityHome.home_notification.setVisibility(View.GONE);
                            } else {
                                if (model.getResponse().getNotification_count() < 99) {
                                    ActivityHome.home_notification.setVisibility(View.VISIBLE);
                                    ActivityHome.home_notification.setText(String.valueOf(model.getResponse().getNotification_count()));
                                } else {
                                    ActivityHome.home_notification.setVisibility(View.VISIBLE);
                                    ActivityHome.home_notification.setText("99+");
                                }

                            }
                            model.getResponse().getPosts();
                            if (model.getResponse().getLast_day() != null && !model.getResponse().getLast_day().equalsIgnoreCase("")) {
                                Config.setSortDay(model.getResponse().getLast_day(), getActivity());
                                tv_set_calender_date.setText(model.getResponse().getLast_day());
                                all_select.setVisibility(View.VISIBLE);
                                lay_s_calender.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    if (from.equalsIgnoreCase("1")) {
                                        all_select.setVisibility(View.GONE);
                                    }
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }


                            }
                            if (model.getResponse().getRadius() != null && !model.getResponse().getRadius().equalsIgnoreCase("")) {
                                Config.setSortRadius(model.getResponse().getRadius(), getActivity());
                                tv_set_distance.setText(model.getResponse().getRadius());
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }

                                all_select.setVisibility(View.VISIBLE);
                                lay_distance.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getSorting() != null && !model.getResponse().getSorting().equalsIgnoreCase("")) {
                                Config.setSortBy(model.getResponse().getSorting(), getActivity());
                                tv_set_sort_type.setText(model.getResponse().getSorting());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_sort.setVisibility(View.VISIBLE);
                            }//nag search
                            if (model.getResponse().getKeyword() != null && !model.getResponse().getKeyword().equalsIgnoreCase("")) {
                                Config.setSearch(model.getResponse().getKeyword(), getActivity());
                                tv_search.setText(model.getResponse().getKeyword());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_search.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMin_price() != null && !(model.getResponse().getMin_price().equalsIgnoreCase("")) && model.getResponse().getMax_price().equalsIgnoreCase("")) {
                                Config.setSortMaxPrice("", getActivity());
                                tv_set_max_min.setText(model.getResponse().getMin_price() + " - " + "...");
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                all_select.setVisibility(View.VISIBLE);
                                lay_min_max.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMax_price() != null && !model.getResponse().getMax_price().equalsIgnoreCase("") && model.getResponse().getMin_price().equalsIgnoreCase("")) {

                                Config.setSortMaxPrice(model.getResponse().getMax_price(), getActivity());
                                tv_set_max_min.setText("0" + " - " + model.getResponse().getMax_price());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_min_max.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMin_price() != null && !(model.getResponse().getMin_price().equalsIgnoreCase("")) && model.getResponse().getMax_price() != null && !model.getResponse().getMax_price().equalsIgnoreCase("")) {
                                Config.setSortMaxPrice(model.getResponse().getMax_price(), getActivity());
                                tv_set_max_min.setText(model.getResponse().getMin_price() + " - " + model.getResponse().getMax_price());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_min_max.setVisibility(View.VISIBLE);
                            }

                            if (model.getResponse().getCountry() != null && model.getResponse().getCountry().equalsIgnoreCase("My country")) {
                                //   Config.setSortOwnCountry("yes", getActivity());
                                tv_set_courent.setText(model.getResponse().getCountry());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_courent.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getPosts().size() > 0) {
                                if (context instanceof ActivityHome) {
                                    ActivityHome.tv_discover_sort.setVisibility(View.VISIBLE);
                                    if (!model.getResponse().getDistance().equalsIgnoreCase("0KM")) {
                                        ActivityHome.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " " + model.getResponse().getDistance());
                                    } else {
                                        ActivityHome.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " 1KM");
                                    }
                                } else if (context instanceof SingleDatingActivity) {
                                    if (!model.getResponse().getDistance().equalsIgnoreCase("0KM")) {
                                        SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " " + model.getResponse().getDistance());
                                    } else {
                                        SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " 1KM");
                                    }
                                }


                                distall = model.getResponse().getDistance();
                                allpostList.addAll(model.getResponse().getPosts());

                                homeAdapter.additemList = allpostList;
                                homeAdapter.setLoaded();
                                homeAdapter.notifyDataSetChanged();


                                apiinfo = "N";
                                // homeAdapter.notifyDataSetChanged();

                            }
                            selectlist = new ArrayList<>();
                            if (model.getResponse().getCategory().size() > 0) {
                                all_select.setVisibility(View.VISIBLE);
                                recy_select_tag.setVisibility(View.VISIBLE);
                                selectTagAdapter = new SelectTagAdapter(getActivity(), model.getResponse().getCategory(), DiscoverHome.this);
                                recy_select_tag.setAdapter(selectTagAdapter);


                            }
                        }
                    } catch (Exception e) {
                        CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<HomeResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
                    swipeRefreshLayout.setRefreshing(false);
                    CommonUtils.getInstance().dismissLoadingDialog();
                }
            });


        } else {
            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitApiAgain(final String from) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
            try {
                lat = Config.getlatitude(getActivity());
                longi = Config.getlongitude(getActivity());
                Log.e("pageNo ", page_no + "");
                Log.e("lat ", lat + "");
                Log.e("Long ", longi + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (lat.equalsIgnoreCase("") && longi.equalsIgnoreCase("")) {
                lat = Config.getMainlatitude(getActivity());
                longi = Config.getMAinlongitude(getActivity());
            }

            if (sub_category.equalsIgnoreCase("0")) {
                sub_category = "";
            }
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<HomeResponseModel> call = apiInterface.PostHome(lat, longi, page_no, last_days, sorting, radius, own_country, special_category, category, sub_category, min_price, max_price, saechAlret, user_id, login_user_fid);
            call.enqueue(new Callback<HomeResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<HomeResponseModel> call, @NonNull Response<HomeResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        HomeResponseModel model = response.body();
                        if (allpostList != null) {
                            allpostList.clear();
                            homeAdapter.additemList.clear();
                            homeAdapter.notifyDataSetChanged();
                        }
                        all_select.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        if (model.isError() == false) {

                            if (model.getResponse().getNotification_count() == 0) {
                                ActivityHome.home_notification.setVisibility(View.GONE);
                            } else {
                                if (model.getResponse().getNotification_count() < 99) {
                                    ActivityHome.home_notification.setVisibility(View.VISIBLE);
                                    ActivityHome.home_notification.setText(String.valueOf(model.getResponse().getNotification_count()));
                                } else {
                                    ActivityHome.home_notification.setVisibility(View.VISIBLE);
                                    ActivityHome.home_notification.setText("99+");
                                }

                            }
                            model.getResponse().getPosts();
                            if (model.getResponse().getLast_day() != null && !model.getResponse().getLast_day().equalsIgnoreCase("")) {
                                Config.setSortDay(model.getResponse().getLast_day(), getActivity());
                                tv_set_calender_date.setText(model.getResponse().getLast_day());
                                all_select.setVisibility(View.VISIBLE);
                                lay_s_calender.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    if (from.equalsIgnoreCase("1")) {
                                        all_select.setVisibility(View.GONE);
                                    }
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }


                            }
                            if (model.getResponse().getRadius() != null && !model.getResponse().getRadius().equalsIgnoreCase("")) {
                                Config.setSortRadius(model.getResponse().getRadius(), getActivity());
                                tv_set_distance.setText(model.getResponse().getRadius());
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }

                                all_select.setVisibility(View.VISIBLE);
                                lay_distance.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getSorting() != null && !model.getResponse().getSorting().equalsIgnoreCase("")) {
                                Config.setSortBy(model.getResponse().getSorting(), getActivity());
                                tv_set_sort_type.setText(model.getResponse().getSorting());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_sort.setVisibility(View.VISIBLE);
                            }//nag search
                            if (model.getResponse().getKeyword() != null && !model.getResponse().getKeyword().equalsIgnoreCase("")) {
                                Config.setSearch(model.getResponse().getKeyword(), getActivity());
                                tv_search.setText(model.getResponse().getKeyword());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_search.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMin_price() != null && !(model.getResponse().getMin_price().equalsIgnoreCase("")) && model.getResponse().getMax_price().equalsIgnoreCase("")) {
                                Config.setSortMaxPrice("", getActivity());
                                tv_set_max_min.setText(model.getResponse().getMin_price() + " - " + "...");
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                all_select.setVisibility(View.VISIBLE);
                                lay_min_max.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMax_price() != null && !model.getResponse().getMax_price().equalsIgnoreCase("") && model.getResponse().getMin_price().equalsIgnoreCase("")) {

                                Config.setSortMaxPrice(model.getResponse().getMax_price(), getActivity());
                                tv_set_max_min.setText("0" + " - " + model.getResponse().getMax_price());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_min_max.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMin_price() != null && !(model.getResponse().getMin_price().equalsIgnoreCase("")) && model.getResponse().getMax_price() != null && !model.getResponse().getMax_price().equalsIgnoreCase("")) {
                                Config.setSortMaxPrice(model.getResponse().getMax_price(), getActivity());
                                tv_set_max_min.setText(model.getResponse().getMin_price() + " - " + model.getResponse().getMax_price());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_min_max.setVisibility(View.VISIBLE);
                            }

                            if (model.getResponse().getCountry() != null && model.getResponse().getCountry().equalsIgnoreCase("My country")) {
                                //  Config.setSortOwnCountry("yes", getActivity());
                                tv_set_courent.setText(model.getResponse().getCountry());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_courent.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getPosts().size() > 0) {
                                if (context instanceof ActivityHome) {
                                    ActivityHome.tv_discover_sort.setVisibility(View.VISIBLE);
                                    if (!model.getResponse().getRadius().equalsIgnoreCase("0KM")) {
                                        ActivityHome.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " " + model.getResponse().getRadius());
                                    } else {
                                        ActivityHome.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " 1KM");
                                    }
                                } else if (context instanceof SingleDatingActivity) {
                                    if (!model.getResponse().getRadius().equalsIgnoreCase("0KM")) {
                                        SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " " + model.getResponse().getRadius());
                                    } else {
                                        SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " 1KM");
                                    }
                                }


                                distall = model.getResponse().getDistance();
                                allpostList.addAll(model.getResponse().getPosts());

                                homeAdapter.additemList = allpostList;
                                homeAdapter.setLoaded();
                                homeAdapter.notifyDataSetChanged();


                                apiinfo = "N";
                                // homeAdapter.notifyDataSetChanged();

                            }
                            selectlist = new ArrayList<>();
                            if (model.getResponse().getCategory().size() > 0) {
                                all_select.setVisibility(View.VISIBLE);
                                recy_select_tag.setVisibility(View.VISIBLE);
                                selectTagAdapter = new SelectTagAdapter(getActivity(), model.getResponse().getCategory(), DiscoverHome.this);
                                recy_select_tag.setAdapter(selectTagAdapter);

                            }
                        }
                    } catch (Exception e) {
//                                CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<HomeResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
//                            CommonUtils.getInstance().dismissLoadingDialog();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });

        } else {
            swipeRefreshLayout.setRefreshing(false);
//            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }

    private void hitApiLoadMore(final String from) {

        if (CommonUtils.getInstance().isNetConnected(context)) {
//            CommonUtils.getInstance().displayLoadingDialog(false, context);

            try {
                lat = Config.getlatitude(getActivity());
                longi = Config.getlongitude(getActivity());
                Log.e("pageNo ", page_no + "");
                Log.e("lat ", lat + "");
                Log.e("Long ", longi + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (lat==null || lat.equalsIgnoreCase("") ) {
                lat = Config.getMainlatitude(getActivity());
                longi = Config.getMAinlongitude(getActivity());
            }

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<HomeResponseModel> call = apiInterface.PostHome(lat, longi, page_no, last_days, sorting, radius, own_country, special_category, category, sub_category, min_price, max_price, saechAlret, user_id, login_user_fid);
            call.enqueue(new Callback<HomeResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<HomeResponseModel> call, @NonNull Response<HomeResponseModel> response) {

                    try {
                        Log.d(TAG, "onResponse: " + response.body());
                        CommonUtils.getInstance().dismissLoadingDialog();
                        HomeResponseModel model = response.body();

                        all_select.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        if (model.isError() == false) {

                            if (model.getResponse().getNotification_count() == 0) {
                                ActivityHome.home_notification.setVisibility(View.GONE);
                            } else {
                                if (model.getResponse().getNotification_count() < 99) {
                                    ActivityHome.home_notification.setVisibility(View.VISIBLE);
                                    ActivityHome.home_notification.setText(String.valueOf(model.getResponse().getNotification_count()));
                                } else {
                                    ActivityHome.home_notification.setVisibility(View.VISIBLE);
                                    ActivityHome.home_notification.setText("99+");
                                }

                            }
                            model.getResponse().getPosts();
                            if (model.getResponse().getLast_day() != null && !model.getResponse().getLast_day().equalsIgnoreCase("")) {
                                Config.setSortDay(model.getResponse().getLast_day(), getActivity());
                                tv_set_calender_date.setText(model.getResponse().getLast_day());
                                all_select.setVisibility(View.VISIBLE);
                                lay_s_calender.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    if (from.equalsIgnoreCase("1")) {
                                        all_select.setVisibility(View.GONE);
                                    }
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }


                            }
                            if (model.getResponse().getRadius() != null && !model.getResponse().getRadius().equalsIgnoreCase("")) {
                                Config.setSortRadius(model.getResponse().getRadius(), getActivity());
                                tv_set_distance.setText(model.getResponse().getRadius());
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }

                                all_select.setVisibility(View.VISIBLE);
                                lay_distance.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getSorting() != null && !model.getResponse().getSorting().equalsIgnoreCase("")) {
                                Config.setSortBy(model.getResponse().getSorting(), getActivity());
                                tv_set_sort_type.setText(model.getResponse().getSorting());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_sort.setVisibility(View.VISIBLE);
                            }//nag search
                            if (model.getResponse().getKeyword() != null && !model.getResponse().getKeyword().equalsIgnoreCase("")) {
                                Config.setSearch(model.getResponse().getKeyword(), getActivity());
                                tv_search.setText(model.getResponse().getKeyword());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_search.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMin_price() != null && !(model.getResponse().getMin_price().equalsIgnoreCase("")) && model.getResponse().getMax_price().equalsIgnoreCase("")) {
                                Config.setSortMaxPrice("", getActivity());
                                tv_set_max_min.setText(model.getResponse().getMin_price() + " - " + "...");
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                all_select.setVisibility(View.VISIBLE);
                                lay_min_max.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMax_price() != null && !model.getResponse().getMax_price().equalsIgnoreCase("") && model.getResponse().getMin_price().equalsIgnoreCase("")) {

                                Config.setSortMaxPrice(model.getResponse().getMax_price(), getActivity());
                                tv_set_max_min.setText("0" + " - " + model.getResponse().getMax_price());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_min_max.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getMin_price() != null && !(model.getResponse().getMin_price().equalsIgnoreCase("")) && model.getResponse().getMax_price() != null && !model.getResponse().getMax_price().equalsIgnoreCase("")) {
                                Config.setSortMaxPrice(model.getResponse().getMax_price(), getActivity());
                                tv_set_max_min.setText(model.getResponse().getMin_price() + " - " + model.getResponse().getMax_price());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_min_max.setVisibility(View.VISIBLE);
                            }

                            if (model.getResponse().getCountry() != null && model.getResponse().getCountry().equalsIgnoreCase("My country")) {
                                //    Config.setSortOwnCountry("yes", getActivity());
                                tv_set_courent.setText(model.getResponse().getCountry());
                                all_select.setVisibility(View.VISIBLE);
                                if (model.getResponse().getLast_day().equalsIgnoreCase("Everywhere")) {
                                    //newly added by sunil
                                    lay_s_calender.setVisibility(View.GONE);
                                } else {
//                                    lay_s_calender.setVisibility(View.VISIBLE);
                                }
                                lay_courent.setVisibility(View.VISIBLE);
                            }
                            if (model.getResponse().getPosts().size() > 0) {
                                if (context instanceof ActivityHome) {
                                    ActivityHome.tv_discover_sort.setVisibility(View.VISIBLE);
                                    if (!model.getResponse().getRadius().equalsIgnoreCase("0KM")) {
                                        if(model.getResponse().getRadius().equalsIgnoreCase(""))
                                        {
                                            ActivityHome.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " 5511KM" );
                                        }
                                        else
                                        {
                                            ActivityHome.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " " + model.getResponse().getRadius());
                                        }

                                    } else {
                                        ActivityHome.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " 1KM");
                                    }
                                } else if (context instanceof SingleDatingActivity) {
                                    if (!model.getResponse().getRadius().equalsIgnoreCase("0KM")) {
                                        if(model.getResponse().getRadius().equalsIgnoreCase(""))
                                        {
                                            SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " 5511KM");
                                        }
                                        else
                                        {
                                            SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " " + model.getResponse().getRadius());
                                        }


                                    } else {
                                        SingleDatingActivity.tv_discover_sort.setText(getResources().getString(R.string.discover_offer) + " 1KM");
                                    }
                                }

                                distall = model.getResponse().getDistance();
                                allpostList.addAll(model.getResponse().getPosts());

                                homeAdapter.additemList = allpostList;
                                homeAdapter.setLoaded();
                                homeAdapter.notifyDataSetChanged();


                                apiinfo = "N";
                                // homeAdapter.notifyDataSetChanged();

                            }
                            selectlist = new ArrayList<>();
                            if (model.getResponse().getCategory().size() > 0) {
                                all_select.setVisibility(View.VISIBLE);
                                recy_select_tag.setVisibility(View.VISIBLE);
                                selectTagAdapter = new SelectTagAdapter(getActivity(), model.getResponse().getCategory(), DiscoverHome.this);
                                recy_select_tag.setAdapter(selectTagAdapter);
                                //selectTagAdapter.notifyDataSetChanged();

                            }
                        }
                    } catch (Exception e) {
//                                CommonUtils.getInstance().dismissLoadingDialog();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<HomeResponseModel> call, Throwable t) {
                    Log.e(TAG, "error: " + t.toString());
//                            CommonUtils.getInstance().dismissLoadingDialog();
                    swipeRefreshLayout.setRefreshing(false);
                }
            });


        } else {
            swipeRefreshLayout.setRefreshing(false);
//            CommonUtils.getInstance().dismissLoadingDialog();
            CommonUtils.getInstance().showAlertMessage(context, getString(R.string.please_ensure_internet_connectivity));
        }
    }


    @Override
    public void onLoadMore() {
        if (allpostList.size() > 5) {
            page_no = page_no + 1;
            hitApiLoadMore("");
            apiinfo = "Y";
        }

    }

}
